# 6.1.1
  * Update dependencies, fixing the following CVEs
    * CVE-2022-23514
    * CVE-2022-23515
    * CVE-2022-23516
    * CVE-2022-23517
    * CVE-2022-23518
    * CVE-2022-23519
    * CVE-2022-23520
    * CVE-2022-31163
    * CVE-2022-32209
    * CVE-2022-32224
    * CVE-2022-44566
    * CVE-2022-44570
    * CVE-2022-44571
    * CVE-2022-44572
    * CVE-2022-45442
    * CVE-2023-22792
    * CVE-2023-22795
    * CVE-2023-22796
# 6.1.0
  * Add `ape report detection` command to export artifact detection results

# 6.0.1
  * Update dependencies due to CVE-2022-30123

# 6.0.0
  * Fix documentation for recipe detector conditions
  * Add API endpoint for uploading artifact detection timetables
  * Add API endpoint for downloading artifact detection reference screenshots
  * Remove `images_path` from recipe parameters

# 5.0.1
  * Fix job `watch_arguments` encoding for compatibility with the windows-client

# 5.0.0
  * Jobs default `watch_arguments` value changed from `{}` to `nil`

# 4.1.0
  * Expose all infrastructure element parameters to the recipe scripts as environment variables

# 4.0.1
  * Remove the no longer used `Callback` class

# 4.0.0
  * Add `ape client` command that allows building a preconfigured client installer
  * Change `artifactomat_setup etc` to `artifactomat_setup app` to better communicate that not only `/etc` is affected

# 3.4.0
  * Add transparent-test recipe to be in the recipes directory by default in order to allow quickly testing the success of the artifactomat setup

# 3.3.0
  * Add recipe README to aid building your own recipes
  * Add start- and stop-detection jobs to trigger the start and stop of the detection of certain artifacts

# 3.2.0
  * Refactor to split up the ConfigurationManager into three parts
    - ConfigurationManager
    - RecipeManager
    - SubjectManager

# 3.1.1
  * Ape server returns a 500 with the error message on all errors

# 3.1.0
  * 🚀 public release
