<!--
Thank you for taking the time filing this merge request.
Let the comments guide you through each section.
You do not have to remove the comments.
-->

## PROBLEM TO SOLVE
<!--
Describe or link an issue
 **or**
What problem is to solve? Try to define the who/what/why. 
For example, "As a (who), I want (what), so I can (why/value)."
-->

## CHECKLIST
- [ ] The problem is described
- [ ] The version number is increased
- [ ] Documentation is adjusted
- [ ] Tests are written
- [ ] Changelog updated
