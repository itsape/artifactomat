<!--
Thank you for taking the time filing this issue.
Let the comments guide you through each section.
You do not have to remove the comments.
-->

## PROBLEM TO SOLVE
<!--
What were you trying to achieve?
What information was missing?
-->

## PLACE OF DOCUMENTATION
<!--
Where have you looked / would you look for information?
-->

## STATE OF DOCUMENTATION
<!--
What did you find? ("nothing" is a valid answer here)
-->

<!-- Do not edit below this line. -->
~documentation
