<!--
Thank you for taking the time proposing this feature.
Let the comments guide you through each section.
You do not have to remove the comments.
-->

## PROBLEM TO SOLVE
<!--
What is the problem to solve? Try to define the who/what/why. 
For example, "As a (who), I want (what), so I can (why/value)."
-->

## PROPOSAL
<!--
What would the solution look like, ideally?
-->

<!-- Do not edit below this line. -->
~feature
