#  THE ARTIFACT-O-MAT

## INSTALLATION
We tested the framework on *GNU Debian 11 "Bullseye"*.
You may use a distribution of your liking, but you will have to tweak the following instructions accordingly.
However, results may vary.

### PREREQUIREMENTS
Install the system requirements:
1. PostgreSQL (for persistence)
2. Ruby 2.6.3 (we rely on this version)
3. Redis v3 (for volatile memory)
4. libpq-dev (for the artifactomat gem)
5. ipset (for the artifactomat routing)
6. Docker (to build the windows-client)

#### 1. POSTGRES

The `artifactomat` gem requires postgresql:
```bash
sudo apt-get install postgresql postgresql-client
```
Connect to standard system database template1
```bash
sudo -u postgres psql template1
```
To set a password for postgres-admin , on template1=# prompt type (change xxxxxx accordingly):
```SQL
ALTER USER postgres with encrypted password 'xxxxxxx';
```
Exit psql with
```
\q
```

Edit config-file to allow for password login (Version number may be different),
and change "peer" to "md5" on the line concerning postgres.
These commands will automate the edit for you:
```bash
POSTGRES_MAJOR="$(psql --version | cut -d' ' -f3 | cut -d'.' -f1)" # the major version of your postgres install
sudo sed 's/^local\s*all\s*postgres\s*peer$/local\tall\tpostgres\tmd5/' -i.orig /etc/postgresql/$POSTGRES_MAJOR/main/pg_hba.conf
```

Restart Postgres:
```bash
sudo /etc/init.d/postgresql restart
```
or
```bash
sudo service postgresql restart
```
Create a user "its", on prompt set password to "ape"
```bash
createuser -U postgres -e -E -l -P its
```
If asked for a password enter the password of user *postgres*

Create the database "itsape" and set owner to "its"
```bash
createdb -U postgres -e -E UTF8 -O its itsape
```
If asked for a password enter the password of user *postgres*

#### 2. RUBY 2.6.3
The artifactomat gem is implemented relying on Ruby 2.6.3.
There are three options to use a specific version of Ruby.

1. **RVM** (https://rvm.io/)
2. **rbenv** (https://github.com/rbenv/rbenv)
3. **native** (build instructions for Debian below)

```bash
sudo apt update
sudo apt install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev libpq-dev checkinstall autoconf libgdbm-dev libgdbm-compat-dev

cd /tmp
wget http://ftp.ruby-lang.org/pub/ruby/2.6/ruby-2.6.3.tar.gz
tar -xzvf ruby-2.6.3.tar.gz
cd ruby-2.6.3/

autoconf
./configure --disable-install-doc
make -j "$(nproc)"

sudo checkinstall --install=no --fstrans=no -D make install
# see: https://wiki.debian.org/CheckInstall
# quick&dirty: Answer the first question with "y", press "return" on the second and "return" on the third question
# copy the deb where you want it
sudo dpkg -i /path/to/deb/ruby_2.6.3-1_amd64.deb
ruby -v
```

#### 3. REDIS
On a GNU Debian 11 "Bullseye" you may simply use the version from the repos:
```sh
sudo apt update
sudo apt install redis-server libsystemd-dev
# You may want to disable the default redis server:
# sudo systemctl disable redis-server
# sudo systemctl stop redis-server
```

#### 4. libpq-dev
On a GNU Debian 11 "Bullseye" you may simply install it from the repos:
```sh
sudo apt update
sudo apt install libpq-dev
```

#### 5. ipset
On a GNU Debian 11 "Bullseye" you may simply use the version from the repos:
```sh
sudo apt update
sudo apt install ipset
```

#### 6. Docker
The following is an excerpt of the current version of the [docker debian installation
instructions](https://docs.docker.com/engine/install/debian/#install-using-the-repository).
If this does not work, check out the link for the most up-to-date instructions on how to install
docker on debian.
```sh
sudo apt update
sudo apt install ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install docker-ce
```

### Use legacy IPtables
The framework relies on IPtables, however Debian 11 uses nftables by default.
To change the default firewall back to IPtables use the following commands:
```sh
sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
```

### INSTALL THE ARTIFACTOMAT GEM
This chapter explains the installation procedure of the gem itself.

Install bundler and the artifactomat dependencies (assuming the cloned repository is in your home directory):
```bash
cd ~/artifactomat/
sudo gem install bundler
sudo bundler install
```
Build the `artifactomat` gem
```bash
gem build artifactomat.gemspec
```
Then wait for the build process to finish (takes a few minutes). And install the newly created gem:
```bash
sudo gem install artifactomat-<version>-x86_64-linux.gem
```
Now setup the

- config in `/etc/artifactomat`
- data in `/var/lib/artifactomat`
- docker images to build the windows client

using the following command:
```bash
sudo artifactomat_setup app
```

Now configure the database details by entering the user etc. from the previous installation steps by editing the config and setting up the database:
```sh
nano /etc/artifactomat/conf.yml
artifactomat_setup db
```

Since we have the database and config files now, we can fire up the artifactomat services and set them to automatically run on system start:
```sh
systemctl enable --now artifactomat.service
```

Now everything is set up and the installation is complete.

### /etc/hosts

Please add following line to your `/etc/hosts`. Replace `<YourExternalIP>` with the host ip.

```bash
<YourExternalIP>     proxy.its.apt
```

## CONFIGURATION
Within the configuration folder (usually `/etc/artifactomat`) you are able to find multiple configuration files and `readme.md` files.
Just follow the instructions in these files.

You should at least have added subjects to `/etc/artifactomat/subjects` and recipes to `/etc/artifactomat/recipes`

All configuration changes require a restart via `systemctl restart artifactomat.service` to be applied.

### Subject Blacklist

The subject blacklist is a `csv` file located at `/etc/artifactomat/subject_blacklist`. In it, you
can define one subject per row that you want to exclude from the tests. Subjects matching these rows
will no longer take part in the tests, and all previously collected data about them will be removed.

In the subject blacklist, all fields except for the `blacklist_id` are optional. The `blacklist_id`
can be chosen freely by you. All the other fields should match the values of the subject you want to
exclude, or be left blank. A subject counts as matched, if all the given attributes in a blacklist
row are the same for *exactly one* subject in a groupfile.

Blacklist matches and errors will be shown in the service logs.

#### Examples

Assume the following groupfile named `group1.csv`

```csv
id,name,surname,userid,email,department
1,Max,Mustermann,maxi,max@muster.man,sektion7
2,Michaela,Musterfrau,micha,mic@muster.fra
3,Manuel,Mustermann,manu,manu@muster.man,sektion12
```

The following blacklist
```csv
blacklist_id,subject_id,name,surname,userid,email
1,,,Mustermann,,
```
will not match any subjects, because `surname: Mustermann` matches two subjects in the given
groupfile, which is not *exactly one*.

The following blacklist
```csv
blacklist_id,subject_id,name,surname,userid,email
1,group1.csv/1,Michaela,Musterfrau,,
```
will not match any subjects, because there is no subject with the `id: 1` that also has `name:
Michaela` and `surname: Musterfrau`.

The following blacklist
```csv
blacklist_id,subject_id,name,surname,userid,email
1,,Manuel,Mustermann,,
```
will match the subject with `id: 3`, because it is the only subject in the groupfile with `name:
Manuel` *and* `surname: Mustermann`.

## Test your setup

To test the setup of the artifactomat and windows-clients, there is a recipe called
`transparent_test` that can be found in `/etc/artifactomat/recipes`.
You will find instructions on how to use that recipe in the recipe's directory.
The following sections will give general instructions on how to use the artifactomat and how to set
up recipes.

## USAGE
Run the client `ape info --interval 2` to poll notifications from the framework in a 2 seconds interval (*tip: put it to background with '&' and don't change the shell*)

To interact with the server, you can use the `ape` command line tool. You can always get help by
issuing `ape help`. You can stop the artifactomat by issuing `sudo systemctl stop
artifactomat.service`. You can also restart any service by issuing `sudo systemctl restart
<servicename>`, where `<servicename>` is one of the following:

- `artifactomat-subject-tracker.service`
- `artifactomat-track-collector.service`
- `artifactomat-ape-scheduler.service`
- `artifactomat-ape-monitoring.service`
- `artifactomat-routing.service`
- `artifactomat-apenames.service`
- `artifactomat-ape-job-spooler.service`
- `artifactomat-ape-job-clerk.service`
- `artifactomat-aped.service`


## SET UP RECIPES
The tests are split into programs, series and seasons that you can configure
using the `ape` command.
You can find help for every command by providing the `--help` argument, e.g.
`ape season new --help` or `ape --help`.

At first you have to create a program with
```
ape program new -l <LABEL>
```
*  `<LABEL>`: some expressive label for your program

After you created at least one program, you can create a series that will be
part of that program like this:
```
ape series new -l <LABEL> -g <USERGROUP> -s <START_TIME> -e <END_TIME> <PROGRAM_ID>
```
*  `<LABEL>`: an expressive label for your series
*  `<USERGROUP>`: the filename of the group of users you defined in
`/etc/artifactomat/subjects/` that you want to assign to this series
*  `<START_TIME>`: the UTC time that you want the series to start rolling out
to users. This understands a time in natural language, for examples see [here](https://github.com/mojombo/chronic#examples)
*  `<END_TIME>`: the UTC time that you want the series to end. This understands
a time in natural language, for examples see [here](https://github.com/mojombo/chronic#examples)
*  `<PROGRAM_ID>`: the ID of the program that you want to assign the series to

You can find the ID to a program name in the output of
```
ape program list
```

After you created at least one series, you can create a season that will be
part of the series like this:
```
ape season new -l <LABEL> -r <RECIPE> -d <DURATION> -p <PARAMETERS> <SERIES_ID>
```
*  `<LABEL>`: an expressive label for your season
*  `<RECIPE>`: the name of the recipe that you want to associate with this season
*  `<DURATION>`: the duration of the season
*  `<PARAMETERS>`: the parameters that you need for your recipe
*  `<SERIES_ID>`: the ID of the series that you want to assign the season to

You can find the ID to a series label in the output of
```
ape series list
```

You will find a more detailed explanation of the parameters, as well as an
example command in the README.md file inside the recipe folder of the recipe
you want to add.

After you added all seasons you want to have in a series, you can schedule it with
```
ape series schedule <SERIES_ID>
```
*  `<SERIES_ID>`: the ID of the series that you want to schedule

After a series is scheduled, you **can not edit it anymore**.

Now you can prepare the series like this:
```
ape series prepare <SERIES_ID>
```
*  `<SERIES_ID>`: the ID of the series that you want to prepare

This will set up all the necessary infrastructure for the series, and inform
you (e.g. as the output of `ape info`) when everything is set up properly.

At last, you can approve the series with
```
ape series approve <SERIES_ID>
```
*  `<SERIES_ID>`: the ID of the series that you want to approve.

Once a series is approved, it will automatically be rolled out when the defined
rollout time is met.

## JOBS
This illustaretes the artifact-o-mat job management.

### ARTIFACT-O-MAT JOB MANAGEMENT:
```
BIN:-----------------------------------------------------------------------------------------------------
           ┌────────────────┐          ┌───────────────┐        ┌─────────────────┐
CLI/ENV ─> │   ape-job      │<─ HTTP ─>│  ape-jobclerk │<─ DB ─>│  ape-jobspooler │<─ HTTPS ─>|Clients
           └────────────────┘          └───────────────┘        └─────────────────┘
                  Ʌ                           Ʌ                         Ʌ
CLASSES:----------|---------------------------|-------------------------|--------------------------------
               JobTool                     JobClerk                 JobSpooler
```

### APE-JOB-TOOL CLI:
```
                          ,-> run -------------> ...
                         /
                        / ,-> copy ------------> ...
                       / /
ape-job ------> build {-{---> remove ----------> ...
       \               \ \
        \               \ `-> start_detection -> ...
         \               \
          \               `-> stop_detection --> ...
           \
            `-> delete --> all|waiting
```
