# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)
require 'artifactomat/version'
require 'date'

Gem::Specification.new do |gem|
  gem.name          = 'artifactomat'
  gem.version       = ARTIFACTOMAT::VERSION
  gem.date          = Date.today.to_s
  gem.summary       = 'artifact-o-mat for the IT Security Awareness Penetration Testing Framework'
  gem.authors       = ['Arnold Sykosch', 'Matthias Wübbeling',
                       'Christian Kollee', 'René Neff',
                       'Thomas Trimborn', 'Christian Doll',
                       'Pascua Theus', 'Timo Pohl']
  gem.email         = 'its.apt@uni-bonn.de'
  gem.files         = Dir['{bin,lib,db,dist,spec}/**/*', 'README*', 'LICENSE*']
  gem.test_files    = Dir['test/**/*']
  gem.platform      = Gem::Platform.local
  gem.executables   = Dir['bin/*'].map { |f| File.basename(f) }
  gem.require_paths = ['lib']
  gem.homepage      = 'https://itsec.cs.uni-bonn.de/itsapt/'
  gem.license       = 'MIT'
  gem.post_install_message = "Please run \'sudo artifactomat_setup\' after installation finishes.\n Edit /etc/artifactomat/conf.yml to your liking.\nHappy testing!"
  gem.description = <<DESC
  The artifactomat gem provides the tools to roll out an entire IT Security Awareness Penetration Testing Environment (itsape).
DESC

  gem.add_runtime_dependency 'activerecord', '6.1.7.2'
  gem.add_runtime_dependency 'async-dns', '1.2.6'
  gem.add_runtime_dependency 'biz', '1.8.2'
  gem.add_runtime_dependency 'bzip2-ffi', '1.1.0'
  gem.add_runtime_dependency 'chronic', '0.10.2'
  gem.add_runtime_dependency 'connection_pool', '2.2.5'
  gem.add_runtime_dependency 'ffi', '1.15.4'
  gem.add_runtime_dependency 'gli', '2.20.0'
  gem.add_runtime_dependency 'httpclient', '2.8.3'
  gem.add_runtime_dependency 'minitar', '0.9'
  gem.add_runtime_dependency 'pg', '1.2.3'
  gem.add_runtime_dependency 'railties', '6.1.7.2'
  gem.add_runtime_dependency 'redis', '3.3.5'
  gem.add_runtime_dependency 'redis-rpc', '1.2.0'
  gem.add_runtime_dependency 'rufus-scheduler', '3.7.0'
  gem.add_runtime_dependency 'simpleidn', '0.2.1'
  gem.add_runtime_dependency 'sinatra', '3.0.5'
  gem.add_runtime_dependency 'standalone_migrations', '6.1.0'
  gem.add_runtime_dependency 'tablomat', '1.2.1'
  gem.add_runtime_dependency 'tzinfo', '2.0.6'
  gem.add_runtime_dependency 'tzinfo-data', '1.2022.7'

  gem.add_development_dependency 'bullet', '~> 6.1.4'
  gem.add_development_dependency 'factory_bot', '~> 5.0'
  gem.add_development_dependency 'fakefs', '~> 0.11'
  gem.add_development_dependency 'fakeredis', '~> 0', '>= 0.6.0'
  gem.add_development_dependency 'listen', '~> 3', '>= 3.1.5'
  gem.add_development_dependency 'overcommit', '~> 0.34', '>= 0.34.2'
  gem.add_development_dependency 'pry', '~> 0.10.3'
  gem.add_development_dependency 'rack-test', '~> 0.6', '>= 0.6.3'
  gem.add_development_dependency 'reek', '~> 5.4'
  gem.add_development_dependency 'rspec', '~> 3.4'
  gem.add_development_dependency 'rspec-mocks', '~>3.4'
  gem.add_development_dependency 'rubocop', '0.76.0'
  gem.add_development_dependency 'rubocop-performance', '1.6.1' # fits rubocop version
  gem.add_development_dependency 'shoulda-matchers', '~> 3.1', '>= 3.1.1'
  gem.add_development_dependency 'simplecov', '~> 0.15', '>= 0.11.1'
  gem.add_development_dependency 'simplecov-cobertura', '~> 1.4', '>=1.4.1'
  gem.add_development_dependency 'sqlite3', '~> 1', '>= 1.4'
  gem.add_development_dependency 'webmock', '~> 3.7', '>= 3.7.6'
end
