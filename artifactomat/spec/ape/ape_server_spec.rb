# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require 'rspec'
require 'rack/test'
require 'chronic'

require 'artifactomat/rpc/clients/general_rpc_client'
require 'artifactomat/modules/ape/ape_server'

describe 'ApeServer' do
  include Rack::Test::Methods

  before(:each) do
    @an_ape = double('Ape')
    @conf_man = double('ConfigurationManager')
    @sub_man = double('SubjectManager')
    @recipe_man = double('RecipeManager')
    allow(Ape).to receive(:instance).and_return(@an_ape)
    allow(@an_ape).to receive(:configuration_manager).and_return(@conf_man)
    allow(@an_ape).to receive(:subject_manager).and_return(@sub_man)
    allow(@an_ape).to receive(:recipe_manager).and_return(@recipe_man)
    allow(ConfigurationManager).to receive(:new).and_return(@conf_man) # used inside recipes
  end

  def app
    Sinatra.new(ApeServer) { set :show_exceptions, :after_handler } # block allows error handler execution in dev environment
  end

  def clean_db
    TestProgram.destroy_all
    TestSeries.destroy_all
    TestSeason.destroy_all
    TestEpisode.destroy_all
  end

  let(:headers) { { 'HTTP_API_VERSION' => '1.0', 'Content-Type' => 'application/yaml' } }

  describe 'ApeServer Routes' do
    it 'checks the headers for correct api version' do
      get '/info', {}, 'api-version' => '0.9'
      expect(last_response.status).to eq(418)
    end

    it '/info' do
      info = 'some info'
      allow(@an_ape).to receive(:inform_user).with('some info')
      @an_ape.inform_user(info)

      allow(@an_ape).to receive(:fetch_infos).and_return(['some info'])
      get '/info', {}, headers
      expect(last_response).to be_ok
      expect(last_response.body).to eq([info].to_yaml)
    end

    it '/status' do
      status = {}
      status['APE'] = { 'Jobs' => [] }
      status['TrackCollector'] = {}
      status['TestScheduler'] = {}
      status['Monitoring'] = {}
      status['SubjectTracker'] = {}
      status['DeliveryManager'] = {}
      allow(@an_ape).to receive(:status).and_return(status)

      get '/status', {}, headers
      expect(last_response).to be_ok
      expect(last_response.body).to include('TrackCollector')
      expect(last_response.body).to include('TestScheduler')
      expect(last_response.body).to include('Monitoring')
      expect(last_response.body).to include('SubjectTracker')
      expect(last_response.body).to include('Jobs')
    end

    describe '/reset' do
      it 'with force' do
        expect(@an_ape).to receive(:reset_vstore)

        post '/reset', { 'force' => true }.to_yaml, headers
        expect(last_response).to be_ok
      end

      describe 'without force' do
        it 'no error' do
          expect(@an_ape).to receive(:safe_reset_vstore)

          post '/reset', { 'force' => false }.to_yaml, headers
          expect(last_response).to be_ok
        end

        it 'conflicting series' do
          expect(@an_ape).to receive(:safe_reset_vstore).and_raise(ApeError, 'The following series are prepared or approved: [Series 1, Series 4, Series 16]')

          post '/reset', { 'force' => false }.to_yaml, headers
          expect(last_response.status).to eq(422)
          expect(last_response.body).to include('The following series are prepared or approved: [Series 1, Series 4, Series 16]')
        end
      end
    end

    describe '/build/client' do
      before(:each) do
        @client_builder = double('ClientBuilder')
        allow(@an_ape).to receive(:client_builder).and_return(@client_builder)
      end

      it 'returns installer path if no error occured' do
        expect(@client_builder).to receive(:create_installer)
        expect(@client_builder).to receive(:installer_path).and_return('/path/to/installer.msi')
        get('/build/client', {}, headers)
        expect(last_response.status).to eq(200)
        expect(last_response.body).to include('/path/to/installer.msi')
      end
    end

    context '/query/:token' do
      it 'armed_episodes' do
        season = build(:test_season)
        armed_episodes_count = TestEpisode.where(armed: true).count
        expect(armed_episodes_count).to eq 0

        get '/query/armed_episodes', {}, headers
        expect(last_response.status).to eq 200
        season.test_episodes.each do |episode|
          expect(last_response.body).to include(episode.to_str)
        end
      end

      it 'locked_seasons' do
        series = build(:test_series)
        get '/query/locked_seasons', {}, headers
        expect(last_response.status).to eq 200
        series.test_seasons.each do |season|
          expect(last_response.body).to include(season.to_str)
        end
      end

      it 'infrastructure' do
        monitoring = double('Monitoring')
        allow(@an_ape).to receive(:monitoring).and_return(monitoring)
        expect(monitoring).to receive(:to_str).and_return({ 'ie1' => 'good' }.to_yaml)
        get '/query/infrastructure', {}, headers
        expect(last_response.status).to eq 200
        expect(YAML.safe_load(last_response.body)).to include('ie1' => 'good')
      end

      it 'tracked_subjects' do
        subject_tracker = double('SubjectTracker')
        allow(@an_ape).to receive(:subject_tracker).and_return(subject_tracker)
        subjects = FactoryBot.build_list(:subject, 3)
        expect(subject_tracker).to receive(:subject_ids).and_return(subjects.map(&:id))
        get '/query/tracked_subjects', {}, headers
        expect(last_response.status).to eq 200
        subjects.each do |subject|
          expect(last_response.body).to include(subject.id.to_s)
        end
      end

      it 'tracked_subjects if Subjecttracker is not available' do
        subject_tracker = double('SubjectTracker')
        allow(@an_ape).to receive(:subject_tracker).and_return(subject_tracker)
        expect(subject_tracker).to receive(:subject_ids).and_raise(GeneralRpcClient::RpcTimeoutError)
        get '/query/tracked_subjects', {}, headers
        expect(last_response.status).to eq 500
      end

      it 'active_subjects' do
        vstore = double('VolatileStore')
        allow(@an_ape).to receive(:vstore).and_return(vstore)
        s = ['heinz/1', 'kunz/2', 'sapper.lot/u']
        expect(vstore).to receive(:subjects_from_map).and_return(s)
        get '/query/active_subjects', {}, headers
        expect(last_response.status).to eq 200
        s.each do |subject|
          expect(last_response.body).to include(subject)
        end
      end

      it 'fails: on unknown token' do
        get '/query/asuklhjkldafhgjkldhfgjk', {}, headers
        expect(last_response.status).to eq 404
      end
    end

    it '/export' do
      data = { 'groupfile' => 'sub.csv', 'columns' => %w[name surname] }
      expect(SubjectExporter).to receive(:export).with(data, @conf_man, @sub_man).and_return('')
      post '/export', data.to_yaml, headers
      expect(last_response).to be_ok
      expect(YAML.safe_load(last_response.body)).to eq([''])
    end

    it '/pseudonymize' do
      data = { 'group1' => 'group1.csv', 'group2' => 'group2.csv' }
      expect(Pseudonymizer).to receive(:pseudonymize).with(data, @sub_man).and_return('')
      post '/pseudonymize', data.to_yaml, headers
      expect(last_response).to be_ok
      expect(YAML.safe_load(last_response.body)).to eq([''])
    end

    it '/import/trace' do
      track_collector = double('TrackCollector')
      allow(@an_ape).to receive(:track_collector).and_return track_collector
      file_contents = "one\ntwo\nthree"
      allow(track_collector).to receive(:parse_trace).and_return('')
      post '/import/trace', { 'trace' => file_contents }, headers
      expect(last_response).to be_ok
      expect(YAML.safe_load(last_response.body)).to eq([''])
    end

    it '/import/helpdesk' do
      track_collector = double('TrackCollector')
      allow(@an_ape).to receive(:track_collector).and_return track_collector
      file_contents = "one\ntwo\nthree"
      allow(track_collector).to receive(:parse_helpdesk).and_return('')
      post '/import/helpdesk', { 'trace' => file_contents }, headers
      expect(last_response).to be_ok
      expect(YAML.safe_load(last_response.body)).to eq([''])
    end

    it '404' do
      get '/404', {}, headers
      expect(last_response.status).to eq(404)
    end

    describe 'subjects' do
      it '/subject/list' do
        group_name = 'subjects.all'
        expect(@sub_man).to receive(:list_subject_groups).and_return([group_name])
        get '/subject/list', {}, headers
        expect(last_response).to be_ok
        expect(last_response.body).to eq([group_name].to_yaml)
      end

      it '/subject/list  - fails on ConfigError' do
        allow(@sub_man).to \
          receive(:list_subject_groups).and_raise(ConfigError)
        get '/subject/list', {}, headers
        expect(last_response.status).to eq(500)
      end
    end

    describe 'recipes' do
      let(:recipe) { build(:recipe) }

      it '/recipe/list' do
        allow(@recipe_man).to \
          receive(:list_recipes).and_return([recipe.id])
        get '/recipe/list', {}, headers
        expect(last_response).to be_ok
        expect(last_response.body).to eq([recipe.id].to_yaml)
      end

      it '/recipe/list - fails on ConfigError' do
        allow(@recipe_man).to \
          receive(:list_recipes).and_raise(ConfigError)
        get '/recipe/list', {}, headers
        expect(last_response.status).to eq(500)
      end

      it '/recipe/parameters/:id' do
        allow(@recipe_man).to \
          receive(:get_recipe).and_return(recipe)
        get '/recipe/parameters/1', {}, headers
        expect(last_response).to be_ok
        parameters = YAML.safe_load(last_response.body)
        expect(parameters.keys).to eq(recipe.parameters.keys)
      end

      it '/recipe/parameters/:id - fails on ConfigError' do
        allow(@recipe_man).to \
          receive(:get_recipe).and_raise(ConfigError)
        get '/recipe/parameters/1', {}, headers
        expect(last_response.status).to eq(500)
      end
    end
  end

  describe 'ResultController Routes' do
    before(:all) do
      clean_db
      group_file = 'disk'
      @recipe = build(:recipe)
      subject = build(:subject)
      @subject_id = subject.id
      @program = create(:test_program, series_count: 0)
      @series = create(:test_series,
                       season_count: 0,
                       group_file: group_file,
                       test_program: @program)
      @season = create(:test_season,
                       test_series_id: @series.id,
                       recipe_id: @recipe.id,
                       episode_count: 3)
      @episode = create(:test_episode, subject_id: @subject_id)
      @season.test_episodes << @episode
      create(:track_entry,
             subject_id: @subject_id,
             test_episode_id: @episode.id)
    end

    context 'series' do
      before(:each) do
        @result_generator = double('ResultGenerator')
      end

      it '/report/series/:id - 200' do
        data = { 'headers' => [['spec'], 'spec'], 'format' => 'yaml' }
        yml_data = data.to_yaml
        expect(@an_ape).to receive(:result_generator).and_return @result_generator
        expect(@result_generator).to receive(:report_series).with(@series.id.to_s,
                                                                  data['headers'],
                                                                  data['out'],
                                                                  data['format'])

        post "/report/series/#{@series.id}", yml_data, headers
        expect(last_response.status).to eq(200)
      end

      it '/report/series/:id - 404' do
        data = { 'headers' => [['spec'], 'spec'], 'format' => 'yaml' }
        yml_data = data.to_yaml
        expect(@an_ape).to receive(:result_generator).and_return @result_generator
        expect(@result_generator).to receive(:report_series).with('1234',
                                                                  data['headers'],
                                                                  data['out'],
                                                                  data['format']).and_raise(ResultError)

        post '/report/series/1234', yml_data, headers
        expect(last_response.status).to eq(404)
      end

      it '/report/series/:id (csv) - 200' do
        data = { 'headers' => [['spec'], 'spec'], 'format' => 'csv' }
        yml_data = data.to_yaml

        expect(@an_ape).to receive(:result_generator).and_return @result_generator
        expect(@result_generator).to receive(:report_series).with(@series.id.to_s,
                                                                  data['headers'],
                                                                  data['out'],
                                                                  data['format'])

        post "/report/series/#{@series.id}", yml_data, headers
        expect(last_response.status).to eq(200)
      end

      it '/report/online as yaml' do
        data = { 'headers' => [['spec'], 'spec'], 'format' => 'yml' }
        yml_data = data.to_yaml

        expect(@an_ape).to receive(:result_generator).and_return @result_generator
        expect(@result_generator).to receive(:report_online).with(data['format'])

        post '/report/online', yml_data, headers
        expect(last_response.status).to eq(200)
      end

      it '/report/online as csv' do
        data = { 'headers' => [['spec'], 'spec'], 'format' => 'csv' }
        yml_data = data.to_yaml

        expect(@an_ape).to receive(:result_generator).and_return @result_generator
        expect(@result_generator).to receive(:report_online).with(data['format'])

        post '/report/online', yml_data, headers
        expect(last_response.status).to eq(200)
      end

      it '/report/detection as yaml' do
        data = { 'format' => 'yaml' }
        yml_data = data.to_yaml

        expect(@an_ape).to receive(:result_generator).and_return(@result_generator)
        expect(@result_generator).to receive(:report_detection).with('yaml')

        post '/report/detection', yml_data, headers
        expect(last_response.status).to eq(200)
      end

      it '/report/detection as csv' do
        data = { 'format' => 'csv' }
        yml_data = data.to_yaml

        expect(@an_ape).to receive(:result_generator).and_return(@result_generator)
        expect(@result_generator).to receive(:report_detection).with('csv')

        post '/report/detection', yml_data, headers
        expect(last_response.status).to eq(200)
      end
    end
  end

  describe 'TestProgramController Routes' do
    context '/program/new' do
      before(:each) do
        clean_db
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        post '/program/new', { 'label' => 'some_label' }.to_yaml, headers
        expect(last_response.status).to eq(201)
        expect(TestProgram.count).to be(1)
      end

      it 'fails: on bad data' do
        expect(Logging).not_to receive(:info)
        post '/program/new', { 'not' => 'label' }.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestProgram.count).to be(0)
      end

      it 'fails: on db error' do
        expect(Logging).not_to receive(:info)
        allow_any_instance_of(TestProgram).to \
          receive(:save).and_return(false)
        post '/program/new', { label: 'some_label' }.to_yaml, headers
        expect(last_response.status).to eq(500)
        expect(TestProgram.count).to be(0)
      end
    end

    context '/program/edit/:id' do
      let(:program) { create(:test_program, series_count: 0) }

      before(:each) do
        clean_db
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        data = { 'label' => 'some_other_label' }
        post "/program/edit/#{program.id}", data.to_yaml, headers
        expect(last_response.status).to eq(204)
        expect(TestProgram.count).to be(1)
        tp_from_db = TestProgram.find(program.id)
        expect(tp_from_db.attributes).to include(data)
      end

      it 'fails: on bad id' do
        expect(Logging).not_to receive(:info)
        data = { 'label' => 'some_other_label' }
        post "/program/edit/#{program.id + 1}", data.to_yaml, headers
        expect(last_response.status).to eq(404)
        expect(TestProgram.count).to be(1)
        tp_from_db = TestProgram.find(program.id)
        expect(tp_from_db.attributes).to include(program.attributes)
      end

      it 'fails: on bad data' do
        expect(Logging).not_to receive(:info)
        data = { 'not_label' => 'some_other_label' }
        post "/program/edit/#{program.id}", data.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestProgram.count).to be(1)
        tp_from_db = TestProgram.find(program.id)
        expect(tp_from_db.attributes).to include(program.attributes)
      end

      it 'fails: on db error' do
        expect(Logging).not_to receive(:info)
        allow_any_instance_of(TestProgram).to \
          receive(:save).and_return(false)
        data = { 'label' => 'some_other_label' }
        post "/program/edit/#{program.id}", data.to_yaml, headers
        expect(last_response.status).to eq(500)
        expect(TestProgram.count).to be(1)
        tp_from_db = TestProgram.find(program.id)
        expect(tp_from_db.attributes).to include(program.attributes)
      end
    end

    context '/program/list' do
      before(:each) do
        clean_db
        @program = create(:test_program, series_count: 0)
      end

      it 'succeeds' do
        get '/program/list', {}, headers
        expect(last_response.status).to eq(200)
        expect(last_response.body).to eq([@program.attributes].to_yaml)
      end
    end

    context '/program/show/:id' do
      before(:each) do
        clean_db
        @program = create(:test_program, series_count: 0)
      end

      it 'succeeds' do
        response = @program.attributes
        response['series'] = []
        expect(@an_ape).to receive(:deflate_program).and_return(response)

        get "/program/show/#{@program.id}", {}, headers
        expect(last_response.status).to eq(200)
        expect(last_response.body).to eq(response.to_yaml)
      end

      it 'fails: on bad id' do
        get "/program/show/#{@program.id + 1}", {}, headers
        expect(last_response.status).to eq(404)
      end
    end

    context '/program/delete' do
      before(:each) do
        clean_db
        @program = create(:test_program, series_count: 0)
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        get "/program/delete/#{@program.id}", {}, headers
        expect(last_response.status).to eq(204)
        expect(TestProgram.count).to be(0)
      end

      it 'fails: on bad id' do
        expect(Logging).not_to receive(:info)
        get "/program/delete/#{@program.id + 1}", {}, headers
        expect(last_response.status).to eq(404)
        expect(TestProgram.count).to be(1)
      end

      it 'fails: on approved schedule' do
        expect(Logging).not_to receive(:info)
        ts = create(:test_series, test_program: @program, season_count: 1)
        ts.approved!
        get "/program/delete/#{@program.id}", {}, headers
        expect(last_response.status).to eq(409)
        expect(TestProgram.count).to be(1)
      end

      it 'fails: on db error' do
        allow_any_instance_of(TestProgram).to \
          receive(:destroy).and_return(false)
        expect(Logging).not_to receive(:info)
        get "/program/delete/#{@program.id}", {}, headers
        expect(last_response.status).to eq(500)
        expect(TestProgram.count).to be(1)
      end
    end
  end

  describe 'TestSeriesController Routes' do
    context '/series/new/:program_id' do
      let(:group_file) { 'some_file' }

      before(:each) do
        @test_scheduler = double('TestScheduler')
        allow(@an_ape).to receive(:test_scheduler).and_return(@test_scheduler)
        clean_db
        @program = create(:test_program, series_count: 0)
        allow(@sub_man).to receive(:list_subject_groups).and_return([group_file])
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        hash = { 'label' => 'somelabel',
                 'group_file' => group_file,
                 'start_date' => Time.now + (1.0 / 24) }
        post "/series/new/#{@program.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(201)
        expect(TestSeries.count).to be(1)
      end

      it 'fails: on db error' do
        allow_any_instance_of(TestSeries).to \
          receive(:save!).and_return(false)
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'group_file' => group_file,
                 'start_date' => Time.now + (1.0 / 24) }
        post "/series/new/#{@program.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(500)
        expect(TestSeries.count).to be(0)
      end

      it 'fails: on not existing group_file' do
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'group_file' => 'not group file',
                 'start_date' => Time.now + (1.0 / 24) }
        post "/series/new/#{@program.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeries.count).to be(0)
      end

      it 'fails: on unknown attributes' do
        expect(Logging).not_to receive(:info)
        hash = { 'love' => 'somelabel',
                 'group_file' => 'not group file',
                 'start_date' => Time.now + (1.0 / 24) }
        post "/series/new/#{@program.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeries.count).to be(0)
      end

      it 'fails: on non-existing program' do
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'group_file' => 'not group file',
                 'start_date' => Time.now + (1.0 / 24) }
        post "/series/new/#{@program.id + 1}", hash.to_yaml, headers
        expect(last_response.status).to eq(404)
        expect(TestSeries.count).to be(0)
      end
    end

    context '/series/edit/:id' do
      let(:group_file) { 'some_file' }

      matcher :be_equal_to_series do |expected|
        match do |actual|
          return false unless expected.id == actual.id
          return false unless expected.group_file == actual.group_file
          return false unless expected.label == actual.label
          return false unless expected.test_program_id == actual.test_program_id
          return false unless expected.start_date.strftime('%d-%m-%Y %H:%M:%S') == actual.start_date.strftime('%d-%m-%Y %H:%M:%S')

          true
        end
        failure_message do |actual|
          return "id match failed: #{expected.id} is not #{actual.id}" unless expected.id == actual.id
          return "group_file match failed: #{expected.group_file} is not #{actual.group_file}" unless expected.group_file == actual.group_file
          return "label match failed: #{expected.label} is not #{actual.label}" unless expected.label == actual.label
          return "test_program_id match failed: #{expected.test_program_id} is not #{actual.test_program_id}" unless expected.test_program_id == actual.test_program_id
          return "start_date match failed: #{expected.start_date} is not #{actual.start_date}" unless expected.start_date.strftime('%d-%m-%Y %H:%M:%S') == actual.start_date.strftime('%d-%m-%Y %H:%M:%S')
        end
      end

      before(:each) do
        clean_db
        @program = create(:test_program, series_count: 0)
        @series = create(:test_series,
                         season_count: 0,
                         group_file: group_file,
                         test_program: @program)

        allow(@sub_man).to receive(:list_subject_groups).and_return([group_file])
        @test_scheduler = double('TestScheduler')
        allow(@an_ape).to receive(:test_scheduler).and_return(@test_scheduler)
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        hash = { 'label' => 'somelabel_other' }
        post "/series/edit/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(200)
        expect(TestSeries.count).to be(1)
        series_from_db = TestSeries.find(@series.id)
        expect(series_from_db.attributes).to include(hash)
      end

      it 'fails: on db error' do
        allow_any_instance_of(TestSeries).to \
          receive(:save!).and_return(false)
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel_other' }
        post "/series/edit/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(500)
        expect(TestSeries.count).to be(1)
        series_from_db = TestSeries.find(@series.id)
        expect(series_from_db).to be_equal_to_series(@series)
      end

      it 'fails: if series approved' do
        @series.approved!
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel_other' }
        post "/series/edit/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeries.count).to be(1)
        series_from_db = TestSeries.find(@series.id)
        expect(series_from_db).to be_equal_to_series(@series)
      end

      it 'fails: on not existing group_file' do
        expect(Logging).not_to receive(:info)
        hash = { 'group_file' => 'somelabel_other' }
        post "/series/edit/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeries.count).to be(1)
        series_from_db = TestSeries.find(@series.id)
        expect(series_from_db).to be_equal_to_series(@series)
      end

      it 'fails: on unknown attributes' do
        expect(Logging).not_to receive(:info)
        hash = { 'love' => 'somelabel' }
        post "/series/edit/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeries.count).to be(1)
        series_from_db = TestSeries.find(@series.id)
        expect(series_from_db).to be_equal_to_series(@series)
      end

      it 'fails: on non-existing series' do
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'group_file' => 'not group file' }
        post "/series/edit/#{@series.id + 1}", hash.to_yaml, headers
        expect(last_response.status).to eq(404)
        expect(TestSeries.count).to be(1)
        series_from_db = TestSeries.find(@series.id)
        expect(series_from_db).to be_equal_to_series(@series)
      end
    end

    context '/series/list' do
      before(:each) do
        clean_db
        @series = create(:test_series, season_count: 0)
      end

      it 'succeeds' do
        get '/series/list', {}, headers
        expect(last_response.status).to eq(200)
        expect(last_response.body).to eq([@series.attributes].to_yaml)
      end
    end

    context '/series/show' do
      before(:each) do
        clean_db
        @series = create(:test_series, season_count: 0)
      end

      it 'succeeds' do
        allow(@an_ape).to receive(:deflate_series).and_return(@series.attributes)
        get "/series/show/#{@series.id}", {}, headers
        expect(last_response.status).to eq(200)
        expect(YAML.safe_load(last_response.body, [Time])).to include(@series.attributes)
      end

      it 'fails: on bad id' do
        get "/series/show/#{@series.id + 1}", {}, headers
        expect(last_response.status).to eq(404)
      end
    end

    context '/series/timetable' do
      before(:each) do
        clean_db
        @series = create(:test_series, season_count: 1)
      end

      it 'succeeds' do
        test_scheduler = double('TestScheduler')
        expect(@an_ape).to receive(:test_scheduler).and_return(test_scheduler)
        expect(test_scheduler).to receive(:generate_timetable).with(@series).and_return('')
        get "/series/timetable/#{@series.id}", {}, headers
        expect(last_response.status).to eq 200
        expect(last_response.body).to eq [''].to_yaml
      end

      it 'fails: on bad id' do
        get "/series/timetable/#{@series.id + 1}", {}, headers
        expect(last_response.status).to eq(404)
      end
    end

    context '/series/delete/:id' do
      before(:each) do
        clean_db
        @series = create(:test_series, season_count: 0)
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        get "/series/delete/#{@series.id}", {}, headers
        expect(last_response.status).to eq(204)
        expect(TestSeries.count).to be(0)
      end

      it 'fails: on db error' do
        allow_any_instance_of(TestSeries).to \
          receive(:destroy).and_return(false)
        expect(Logging).not_to receive(:info)
        get "/series/delete/#{@series.id}", {}, headers
        expect(last_response.status).to eq(500)
        expect(TestSeries.count).to be(1)
      end

      it 'fails: on approved schedule' do
        @series.approved!
        expect(Logging).not_to receive(:info)
        get "/series/delete/#{@series.id}", {}, headers
        expect(last_response.status).to eq(422)
        expect(TestSeries.count).to be(1)
      end

      it 'fails: on non-existing series' do
        expect(Logging).not_to receive(:info)
        get "/series/delete/#{@series.id + 1}", {}, headers
        expect(last_response.status).to eq(404)
        expect(TestSeries.count).to be(1)
      end
    end

    context '/series/schedule/:id' do
      let(:specimen) { build(:subject) }

      before(:each) do
        clean_db
        @test_scheduler = double('TestScheduler')
        allow(@an_ape).to receive(:test_scheduler).and_return(@test_scheduler)
        @series = create(:test_series, season_count: 0, start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc)
        @season = create(:test_season, episode_count: 0, test_series: @series, duration: 120)

        allow(@sub_man).to receive(:get_subjects).and_return([specimen])
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        expect(@test_scheduler).to receive(:schedule)
        post "/series/schedule/#{@series.id}", { 'force' => true }, headers
        expect(last_response.status).to eq(204)
        expect(TestEpisode.count).to be(1)
      end

      it 'fails: on approved schedule' do
        @series.approved!
        expect(Logging).not_to receive(:info)
        expect(@test_scheduler).not_to receive(:schedule)
        post "/series/schedule/#{@series.id}", { 'force' => true }, headers
        expect(last_response.status).to eq(422)
        expect(TestEpisode.count).to be(0)
      end

      it 'fails: on unknown series' do
        expect(Logging).not_to receive(:info)
        expect(@test_scheduler).not_to receive(:schedule)
        post "/series/schedule/#{@series.id + 1}", { 'force' => true }, headers
        expect(last_response.status).to eq(404)
        expect(TestEpisode.count).to be(0)
      end
    end

    context '/series/approve/:id' do
      before(:each) do
        clean_db
        @series = create(:test_series, season_count: 0)
        @season = create(:test_season, episode_count: 1, test_series: @series)
        subject_list = []
        (1..5).each do |i|
          subject_list << { id: i, name: "Subject#{i}", email: "user#{i}@example.com", surname: "Surname#{i}", userid: "User#{i}" }
        end

        @test_scheduler = double('TestScheduler')
        allow(@an_ape).to receive(:test_scheduler).and_return(@test_scheduler)
        @subject_tracker = double('SubjectTracker')
        allow(@an_ape).to receive(:subject_tracker).and_return(@subject_tracker)
        @track_collector = double('Trackcollector')
        allow(@an_ape).to receive(:track_collector).and_return(@track_collector)
        allow(@sub_man).to receive(:get_subjects).and_return(subject_list)
      end

      it 'succeeds' do
        @series.prepared!
        expect(Logging).to receive(:info)
        expect(@test_scheduler).to receive(:ack)
        expect(@subject_tracker).to receive(:activate_series)
        expect(@track_collector).to receive(:register_series)
        get "/series/approve/#{@series.id}", {}, headers
        expect(last_response.status).to eq(204)
        @series.reload
        expect(@series.approved?).to be_truthy
      end

      it 'fails: on approved schedule' do
        @series.approved!
        expect(Logging).not_to receive(:info)
        expect(@test_scheduler).not_to receive(:ack)
        get "/series/approve/#{@series.id}", {}, headers
        expect(last_response.status).to eq(422)
      end

      it 'fails: on completed series' do
        @series.completed!
        expect(Logging).not_to receive(:info)
        expect(@test_scheduler).not_to receive(:ack)
        get "/series/approve/#{@series.id}", {}, headers
        expect(last_response.status).to eq(422)
      end

      it 'fails: on unknown series' do
        expect(Logging).not_to receive(:info)
        expect(@test_scheduler).not_to receive(:ack)
        get "/series/approve/#{@series.id + 1}", {}, headers
        expect(last_response.status).to eq(404)
      end
    end

    context '/series/prepare/:id' do
      before(:each) do
        @routing = double('Routing')
        allow(@an_ape).to receive(:routing).and_return(@routing)
        @artifact_generator = double('ArtifactGenerator')
        allow(@an_ape).to receive(:artifact_generator).and_return(@artifact_generator)
        @infrastructure_generator = double('InfrastructureGenerator')
        allow(@an_ape).to receive(:infrastructure_generator).and_return(@infrastructure_generator)

        clean_db
        @series = create(:test_series, season_count: 0)
        @season = create(:test_season, episode_count: 0, test_series: @series)
        allow(Thread).to receive(:start).and_yield
        @series.scheduled!
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        expect(@routing).to receive(:create_routing).with(@series)
        expect(@an_ape).to receive(:update_job).twice
        expect(@an_ape).to receive(:inform_user)
        expect(@artifact_generator).to \
          receive(:generate).and_return([true, '', ''])
        expect(@infrastructure_generator).to \
          receive(:generate).and_return([true, '', ''])
        get "/series/prepare/#{@series.id}", {}, headers
        expect(last_response.status).to eq(204)
      end

      it 'fails: on unscheduled series' do
        @series.fresh!
        expect(Logging).not_to receive(:info)
        expect(@routing).not_to receive(:create_routing)
        expect(@artifact_generator).not_to \
          receive(:generate)
        expect(@infrastructure_generator).not_to \
          receive(:generate)
        get "/series/prepare/#{@series.id}", {}, headers
        expect(last_response.status).to eq(422)
      end

      it 'fails: on unknown series' do
        expect(Logging).not_to receive(:info)
        expect(@routing).not_to receive(:create_routing)
        expect(@artifact_generator).not_to \
          receive(:generate)
        expect(@infrastructure_generator).not_to \
          receive(:generate)
        get "/series/prepare/#{@series.id + 1}", {}, headers
        expect(last_response.status).to eq(404)
      end

      it 'fails: on failing artifact generation' do
        expect(Logging).not_to receive(:info)
        expect(Logging).to receive(:error)
        expect(@routing).to receive(:create_routing).with(@series)
        expect(@an_ape).to receive(:update_job)
        expect(@an_ape).to receive(:inform_user)
        expect(@artifact_generator).to \
          receive(:generate).and_return([false, '', ''])
        expect(@routing).to receive(:remove_routing).with(@series)
        expect(@artifact_generator).to \
          receive(:destroy_artifacts)
        expect(@infrastructure_generator).not_to \
          receive(:generate)
        get "/series/prepare/#{@series.id}", {}, headers
        expect(last_response.status).to eq(204)
      end

      it 'fails: on failing infrastructure generation' do
        depman = double('DeploymentManager')
        allow(@an_ape).to receive(:deployment_manager).and_return(depman)

        expect(Logging).not_to receive(:info)
        expect(Logging).to receive(:error)
        expect(@routing).to receive(:create_routing).with(@series)
        expect(@an_ape).to receive(:update_job).twice
        expect(@an_ape).to receive(:inform_user)
        expect(@artifact_generator).to \
          receive(:generate).and_return([true, '', ''])
        expect(@artifact_generator).to \
          receive(:destroy_artifacts)
        expect(@routing).to receive(:remove_routing).with(@series)
        expect(@infrastructure_generator).to \
          receive(:generate).and_raise(ScriptExecutionError)
        expect(depman).to receive(:revoke_artifact_deployment).with(@series).once
        get "/series/prepare/#{@series.id}", {}, headers
        expect(last_response.status).to eq(204)
      end
    end

    context '/series/abort/:id' do
      before(:each) do
        clean_db
        @series = create(:test_series, season_count: 0)
      end

      it 'succeeds' do
        allow(Logging).to receive(:debug)
        delivery_manager = double('DeliveryManager')
        allow(@an_ape).to receive(:delivery_manager).and_return(delivery_manager)
        deployment_manager = double('DeploymentManager')
        expect(@an_ape).to receive(:deployment_manager).and_return(deployment_manager)
        track_collector = double('Trackcollector')
        expect(@an_ape).to receive(:track_collector).and_return(track_collector)
        expect(@an_ape).to receive(:unschedule_jobs).with(@series)

        expect(Logging).to receive(:info)
        expect(delivery_manager).to receive(:lock_season).exactly(@series.test_seasons.count).times
        expect(deployment_manager).to receive(:withdraw).with(@series)
        expect(track_collector).to receive(:unregister_series).with(@series)
        get "/series/abort/#{@series.id}", {}, headers
        expect(last_response.status).to eq(204)
      end

      it 'fails: on unknown series' do
        expect(Logging).not_to receive(:info)
        get "/series/abort/#{@series.id + 1}", {}, headers
        expect(last_response.status).to eq(404)
      end
    end
  end

  describe 'TestSeasonController Routes' do
    context '/season/new' do
      let(:recipe) { build(:recipe, default_parameters: { 'some' => 'thing' }, parameters: { 'some' => 'otherthing' }) }

      before(:each) do
        clean_db
        @program = create(:test_program, series_count: 0)
        @series = create(:test_series, season_count: 0,
                                       start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                                       end_date: Chronic.parse('tomorrow 10:00am', ambiguous_time_range: :none).utc,
                                       test_program: @program)
        @artifact_generator = double('ArtifactGenerator')
        allow(@an_ape).to receive(:artifact_generator).and_return(@artifact_generator)
        @test_scheduler = double('TestScheduler')
        allow(@an_ape).to receive(:test_scheduler).and_return(@test_scheduler)

        allow(@recipe_man).to receive(:list_recipes).and_return([recipe.id])
        allow(@recipe_man).to receive(:get_recipe).and_return(recipe)
        allow(@artifact_generator).to \
          receive(:check_season).and_return([0, '', ''])

        allow(@conf_man).to receive(:cfg_path).and_return('/etc/artifactomat')
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        hash = { 'label' => 'somelabel',
                 'duration' => 120,
                 'recipe_id' => recipe.id,
                 'recipe_parameters' => {} }
        allow(@test_scheduler).to receive(:series_enough_runtime)
        allow(@test_scheduler).to receive(:format_duration)
        post "/season/new/#{@series.id}", hash.to_yaml, headers
        expect(@series.fresh?).to be_truthy
        expect(last_response.status).to eq(201)
        expect(TestSeason.count).to be(1)
      end

      it 'loads defaults' do
        expect(Logging).to receive(:info)
        hash = { 'label' => 'somelabel',
                 'duration' => 120,
                 'recipe_id' => recipe.id,
                 'recipe_parameters' => {} }
        allow(@test_scheduler).to receive(:series_enough_runtime)
        allow(@test_scheduler).to receive(:format_duration)
        post "/season/new/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(201)
        ts = TestSeason.last
        expect(ts.recipe_parameters).to include('some' => 'thing')
        expect(TestSeason.count).to be(1)
      end

      it 'fails: db error' do
        allow_any_instance_of(TestSeason).to \
          receive(:save).and_return(false)
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'duration' => 120,
                 'recipe_id' => recipe.id,
                 'recipe_parameters' => {} }
        allow(@test_scheduler).to receive(:series_enough_runtime)
        allow(@test_scheduler).to receive(:format_duration)
        post "/season/new/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(500)
        expect(TestSeason.count).to be(0)
      end

      it 'fails: on not existing recipe' do
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'duration' => 120,
                 'recipe_id' => 'not_a_recipe',
                 'recipe_parameters' => {} }
        post "/season/new/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(0)
      end

      it 'fails: on bad attributes' do
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'duration' => 120,
                 'recipe' => recipe.id,
                 'recipe_paramaters' => {} } # typo in param*e*ters
        post "/season/new/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(0)
      end

      it 'fails: on bad duration' do
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'duration' => 0,
                 'recipe_id' => recipe.id,
                 'recipe_parameters' => {} }
        post "/season/new/#{@series.id}", hash.to_yaml, headers
        expect(@series.fresh?).to be_truthy
        expect(last_response.status).to eq(500)
        expect(TestSeason.count).to be(0)
      end

      it 'fails: on bad recipe_prarameters' do
        expect(Logging).not_to receive(:info)
        recipe.parameters = { a: 'parameter' }
        hash = { 'label' => 'somelabel',
                 'duration' => 120,
                 'recipe_id' => 'not_a_recipe',
                 'recipe_parameters' => {} }
        post "/season/new/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(0)
      end

      it 'fails: on unknown series' do
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'duration' => 120,
                 'recipe_id' => recipe.id,
                 'recipe_parameters' => {} }
        post "/season/new/#{@series.id + 1}", hash.to_yaml, headers
        expect(last_response.status).to eq(404)
        expect(TestSeason.count).to be(0)
      end

      it 'fails: on not already approved series' do
        @series.approved!
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'duration' => 120,
                 'recipe_id' => recipe.id,
                 'recipe_parameters' => {} }
        post "/season/new/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(0)
      end

      it 'fails: on unmet recipe requirements' do
        allow(@artifact_generator).to \
          receive(:check_season).and_return([1, '', ''])
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel',
                 'duration' => 120,
                 'recipe_id' => recipe.id,
                 'recipe_parameters' => {} }
        post "/season/new/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(0)
      end

      it 'fails: on a duration exceeding the remaining runtime' do
        hash = { 'label' => 'somelabel',
                 'duration' => 130,
                 'recipe_id' => recipe.id,
                 'recipe_parameters' => {} }
        expect(@test_scheduler).to receive(:series_enough_runtime).and_raise(ApeError)
        post "/season/new/#{@series.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(0)
      end
    end

    context '/season/edit/:id' do
      let(:recipe) { build(:recipe, default_parameters: { 'some' => 'thing' }) }

      before(:each) do
        clean_db
        @program = create(:test_program, series_count: 0)
        @series = create(:test_series,
                         season_count: 0,
                         start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                         end_date: Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc,
                         test_program: @program)
        @season = create(:test_season, episode_count: 0,
                                       duration: 60,
                                       test_series: @series,
                                       recipe_id: recipe.id)
        allow(@recipe_man).to receive(:list_recipes).and_return([recipe.id])
        allow(@recipe_man).to receive(:get_recipe).and_return(recipe)

        @artifact_generator = double('ArtifactGenerator')
        allow(@an_ape).to receive(:artifact_generator).and_return(@artifact_generator)
        allow(@artifact_generator).to \
          receive(:check_season).and_return([0, '', ''])

        @test_scheduler = double('TestScheduler')
        allow(@an_ape).to receive(:test_scheduler).and_return(@test_scheduler)
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        hash = { 'label' => 'somelabel_other' }
        allow(@test_scheduler).to receive(:series_enough_runtime)
        allow(@test_scheduler).to receive(:format_duration)
        post "/season/edit/#{@season.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(200)
        expect(TestSeason.count).to be(1)
        season_from_db = TestSeason.find(@season.id)
        expect(season_from_db.attributes).to include(hash)
        expect(@series.fresh?).to be_truthy
      end

      it 'fails: on db error' do
        allow_any_instance_of(TestSeason).to \
          receive(:save).and_return(false)
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel_other' }
        allow(@test_scheduler).to receive(:series_enough_runtime)
        allow(@test_scheduler).to receive(:format_duration)
        post "/season/edit/#{@season.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(500)
        season_from_db = TestSeason.find(@season.id)
        expect(season_from_db.attributes).to include(@season.attributes)
      end

      it 'fails: on unmet recipe requirements' do
        expect(@artifact_generator).to \
          receive(:check_season).and_return([1, '', ''])
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel_other' }
        allow(@test_scheduler).to receive(:series_enough_runtime).and_raise(ApeError)
        post "/season/edit/#{@season.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        season_from_db = TestSeason.find(@season.id)
        expect(season_from_db.attributes).to include(@season.attributes)
      end

      it 'fails: if season approved' do
        @series.approved!
        expect(Logging).not_to receive(:info)
        hash = { 'label' => 'somelabel_other' }
        post "/season/edit/#{@season.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(1)
        season_from_db = TestSeason.find(@season.id)
        expect(season_from_db.attributes).to include(@season.attributes)
      end

      it 'fails: on not existing recipe' do
        expect(Logging).not_to receive(:info)
        hash = { 'recipe_id' => 'somelabel_other' }
        post "/season/edit/#{@season.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(1)
        season_from_db = TestSeason.find(@season.id)
        expect(season_from_db.attributes).to include(@season.attributes)
      end

      it 'fails: on unknown attributes' do
        expect(Logging).not_to receive(:info)
        hash = { love: 'somelabel' }
        post "/season/edit/#{@season.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(1)
        season_from_db = TestSeason.find(@season.id)
        expect(season_from_db.attributes).to include(@season.attributes)
      end

      it 'fails: on non-existing season' do
        expect(Logging).not_to receive(:info)
        hash = { label: 'somelabel' }
        post "/season/edit/#{@season.id + 1}", hash.to_yaml, headers
        expect(last_response.status).to eq(404)
        expect(TestSeason.count).to be(1)
        season_from_db = TestSeason.find(@season.id)
        expect(season_from_db.attributes).to include(@season.attributes)
      end

      it 'fails: on extending the seasons runtime too much' do
        hash = { 'duration' => 90 }
        allow(@test_scheduler).to receive(:series_enough_runtime).and_raise(ApeError)
        post "/season/edit/#{@season.id}", hash.to_yaml, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(1)
        season_from_db = TestSeason.find(@season.id)
        expect(season_from_db.attributes).to include(@season.attributes)
      end
    end

    context '/season/list' do
      before(:each) do
        clean_db
        @season = create(:test_season, episode_count: 0)
      end

      it 'succeeds' do
        get '/season/list', {}, headers
        expect(last_response.status).to eq(200)
        expect(last_response.body).to eq([@season.attributes].to_yaml)
      end
    end

    context '/season/show' do
      before(:each) do
        clean_db
        @season = create(:test_season, episode_count: 0)
      end

      it 'succeeds' do
        expect(@an_ape).to receive(:deflate_season).and_return(@season.attributes)
        get "/season/show/#{@season.id}", {}, headers
        expect(last_response.status).to eq(200)
        expect(YAML.safe_load(last_response.body)).to include(@season.attributes)
      end

      it 'fails: on bad id' do
        get "/season/show/#{@season.id + 1}", {}, headers
        expect(last_response.status).to eq(404)
      end
    end

    context '/season/delete' do
      before(:each) do
        clean_db
        @season = create(:test_season, episode_count: 0)
      end

      it 'succeeds' do
        expect(Logging).to receive(:info)
        get "/season/delete/#{@season.id}", {}, headers
        expect(last_response.status).to eq(204)
        expect(TestSeason.count).to be(0)
      end

      it 'fails: on db error' do
        allow_any_instance_of(TestSeason).to \
          receive(:destroy).and_return(false)
        expect(Logging).not_to receive(:info)
        get "/season/delete/#{@season.id}", {}, headers
        expect(last_response.status).to eq(500)
        expect(TestSeason.count).to be(1)
      end

      it 'fails: on approved schedule' do
        @season.test_series.approved!
        expect(Logging).not_to receive(:info)
        get "/season/delete/#{@season.id}", {}, headers
        expect(last_response.status).to eq(422)
        expect(TestSeason.count).to be(1)
      end

      it 'fails: on non-existing season' do
        expect(Logging).not_to receive(:info)
        get "/season/delete/#{@season.id + 1}", {}, headers
        expect(last_response.status).to eq(404)
        expect(TestSeason.count).to be(1)
      end
    end
  end

  describe 'Global error handling' do
    it 'Returns error message on unhandled exceptions' do
      track_collector = double('TrackCollector')
      allow(@an_ape).to receive(:track_collector).and_return track_collector

      allow(track_collector).to receive(:parse_trace).and_raise(GeneralRpcClient::RpcTimeoutError, 'TC timed out!')
      post '/import/trace', {}, headers
      expect(last_response.status).to eq(500)
      expect(last_response.body).to include('TC timed out!')

      allow(track_collector).to receive(:parse_trace).and_raise(RedisRpc::RemoteException.new('TC threw an exception!', ''))
      post '/import/trace', {}, headers
      expect(last_response.status).to eq(500)
      expect(last_response.body).to include('TC threw an exception!')

      allow(track_collector).to receive(:parse_trace).and_raise(Exception, 'TC exception detected!')
      post '/import/trace', {}, headers
      expect(last_response.status).to eq(500)
      expect(last_response.body).to include('TC exception detected!')
    end
  end
end
