# frozen_string_literal: true

require 'rspec'
require 'chronic'
require 'artifactomat/modules/ape/controller/test_season_controller'
require 'artifactomat/modules/ape'

require 'fakefs/spec_helpers'

describe 'TestSeasonController' do
  subject do
    described_class
    Class.new do
      include ApeController::TestSeasonController::Functions
    end.new
  end

  def clean_db
    without_bullet do
      TestProgram.destroy_all
      TestSeries.destroy_all
      TestSeason.destroy_all
      TestEpisode.destroy_all
    end
  end

  before(:each) do
    @an_ape = double('Ape')
    allow(Ape).to receive(:instance).and_return(@an_ape)

    @configuration_manager = double('ConfigurationManager')
    allow(@an_ape).to receive(:configuration_manager).and_return(@configuration_manager)
    allow(@configuration_manager).to receive(:cfg_path).and_return('/etc/artifactomat')

    @recipe_manager = double('RecipeManager')
    allow(@an_ape).to receive(:recipe_manager).and_return(@recipe_manager)

    @test_scheduler = double('TestScheduler')
    allow(@an_ape).to receive(:test_scheduler).and_return(@test_scheduler)

    @artifact_generator = double('ArtifactGenerator')
    allow(@an_ape).to receive(:artifact_generator).and_return(@artifact_generator)
  end

  after(:all) do
    clean_db
  end

  describe '::season_new' do
    let(:recipe) { build(:recipe) }

    before(:each) do
      clean_db
      @program = create(:test_program, series_count: 0)
      @series = create(:test_series, season_count: 0,
                                     start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                                     end_date: Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc,
                                     test_program: @program)
      allow(@recipe_manager).to receive(:list_recipes).and_return([recipe.id])
      allow(@recipe_manager).to receive(:get_recipe).and_return(recipe)
      allow(@artifact_generator).to \
        receive(:check_season).and_return([0, '', ''])
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      hash = { 'label' => 'somelabel',
               'duration' => 60,
               'recipe_id' => recipe.id }
      expect(@test_scheduler).to receive(:series_enough_runtime)
      expect(@test_scheduler).to receive(:format_duration)
      expect do
        subject.season_new(hash, @series.id)
      end.not_to raise_error
      expect(TestSeason.count).to be(1)
    end

    it 'fails: on not existing recipe' do
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel',
               'duration' => 60,
               'recipe_id' => 'not_a_recipe',
               'recipe_parameters' => {} }
      expect do
        subject.season_new(hash, @series.id)
      end.to raise_error(ApeError)
      expect(TestSeason.count).to be(0)
    end

    it 'fails: on recipe with unmet requirements' do
      allow(@artifact_generator).to \
        receive(:check_season).and_return([1, '', ''])
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel',
               'duration' => 60,
               'recipe_id' => recipe.id,
               'recipe_parameters' => {} }
      expect do
        subject.season_new(hash, @series.id)
      end.to raise_error(ApeError)
      expect(TestSeason.count).to be(0)
    end

    it 'fails: on bad attributes' do
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel',
               'duration' => 60,
               'recipe' => 'not_a_recipe',
               'recipe_paramaters' => {} }
      expect do
        subject.season_new(hash, @series.id)
      end.to raise_error(ActiveRecord::UnknownAttributeError)
      expect(TestSeason.count).to be(0)
    end

    it 'fails: on bad recipe_parameters' do
      expect(Logging).not_to receive(:info)
      recipe.parameters = { a: 'parameter' }
      hash = { 'label' => 'somelabel',
               'duration' => 60,
               'recipe_id' => 'not_a_recipe',
               'recipe_parameters' => {} }
      expect do
        subject.season_new(hash, @series.id)
      end.to raise_error(ApeError)
      expect(TestSeason.count).to be(0)
    end

    it 'fails: on unknown series' do
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel',
               'duration' => 60,
               'recipe_id' => recipe.id,
               'recipe_parameters' => {} }
      expect do
        subject.season_new(hash, @series.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
      expect(TestSeason.count).to be(0)
    end

    it 'fails: on not already approved series' do
      @series.approved!
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel',
               'duration' => 60,
               'recipe_id' => recipe.id,
               'recipe_parameters' => {} }
      expect do
        subject.season_new(hash, @series.id)
      end.to raise_error(ApeError)
      expect(TestSeason.count).to be(0)
    end

    it 'fails: on duration exceeding series runtime' do
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel',
               'duration' => 90,
               'recipe_id' => recipe.id }
      expect(@test_scheduler).to receive(:series_enough_runtime).and_raise(ApeError)
      expect do
        subject.season_new(hash, @series.id)
      end.to raise_error(ApeError)
      expect(TestSeason.count).to be(0)
    end

    it 'fails: on adding seasons with longer cumulative test time than series runtime' do
      allow(@test_scheduler).to receive(:format_duration)
      allow(Logging).to receive(:info)
      remaining_time = 60 # minutes
      (1..5).each do |i|
        hash = { 'label' => "somelabel#{i}",
                 'duration' => 10,
                 'recipe_id' => recipe.id,
                 'recipe_parameters' => {} }
        expect(@test_scheduler).to receive(:series_enough_runtime).with(@series).and_return(remaining_time -= 10)
        subject.season_new(hash, @series.id)
      end
      hash = { 'label' => 'overtime',
               'duration' => 30,
               'recipe_id' => recipe.id,
               'recipe_parameters' => {} }
      expect(@test_scheduler).to receive(:series_enough_runtime).with(@series).and_raise(ApeError)
      expect do
        subject.season_new(hash, @series.id)
      end.to raise_error(ApeError)
      expect(TestSeason.count).to eq(5)
    end

    describe 'set/check screenshots folder' do
      before(:all) do
        FakeFS.activate!
      end

      after(:all) do
        FakeFS.deactivate!
      end

      before(:each) do
        FakeFS.clear!
        @recipe_path = "/etc/artifactomat/recipes/#{recipe.id}"
        FileUtils.mkdir_p(@recipe_path)

        allow(Logging).to receive(:info)
        allow(@test_scheduler).to receive(:series_enough_runtime)
        allow(@test_scheduler).to receive(:format_duration)
      end

      context 'custom screenshots folder supplied' do
        before(:each) do
          @screenshots_location = 'screenshots/custom'
          @hash = { 'label' => 'somelabel',
                    'duration' => 60,
                    'recipe_id' => recipe.id,
                    'screenshots_folder' => @screenshots_location }
        end

        it 'sets the screenshots_folder to the supplied one, if it exists' do
          FileUtils.mkdir_p(File.join(@recipe_path, @screenshots_location))
          expect do
            subject.season_new(@hash, @series.id)
          end.not_to raise_error
          expect(TestSeason.count).to be(1)
          expect(TestSeason.last.screenshots_folder).to eq(@screenshots_location)
        end

        it 'raises error if the supplied folder does not exist' do
          expect do
            subject.season_new(@hash, @series.id)
          end.to raise_error(ApeError, /is not a valid directory/)
          expect(TestSeason.count).to be(0)
        end
      end

      context 'no custom screenshots folder supplied' do
        before(:each) do
          @hash = { 'label' => 'somelabel',
                    'duration' => 60,
                    'recipe_id' => recipe.id }
        end

        it 'sets the screenshots_folder to "" if the recipe does not have a `screenshots` subfolder' do
          expect do
            subject.season_new(@hash, @series.id)
          end.not_to raise_error
          expect(TestSeason.count).to be(1)
          expect(TestSeason.last.screenshots_folder).to eq('')
        end

        it 'sets the screenshots_folder to `screenshots` if the recipe has a `screenshots` subfolder' do
          FileUtils.mkdir_p(File.join(@recipe_path, 'screenshots'))
          expect do
            subject.season_new(@hash, @series.id)
          end.not_to raise_error
          expect(TestSeason.count).to be(1)
          expect(TestSeason.last.screenshots_folder).to eq('screenshots')
        end
      end
    end

    describe 'double booking recipe' do
      before :each do
        allow(Logging).to receive(:info)
        allow(@test_scheduler).to receive(:series_enough_runtime)
        allow(@test_scheduler).to receive(:format_duration)
        @hash = { 'label' => 'somelabel',
                  'duration' => 60,
                  'recipe_id' => recipe.id }
      end

      it 'does not warn if recipe is not double booked' do
        @series2 = create(:test_series, season_count: 0,
                                        start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                                        end_date: Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc,
                                        test_program: @program)
        output = subject.season_new(@hash, @series.id)
        expect(output).not_to match(/Warning/)
      end

      it 'does not warn if recipe is booked in same series' do
        @series.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => 'screenshots')
        @series.save!
        output = subject.season_new(@hash, @series.id)
        expect(output).not_to match(/Warning/)
      end

      it 'does not warn if double booked recipes will not overlap' do
        @series2 = create(:test_series, season_count: 0,
                                        start_date: Chronic.parse('tomorrow 10:00am', ambiguous_time_range: :none).utc,
                                        end_date: Chronic.parse('tomorrow 11:00am', ambiguous_time_range: :none).utc,
                                        test_program: @program)
        @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
        @series2.save!
        output = subject.season_new(@hash, @series.id)
        expect(output).not_to match(/Warning/)
      end

      context 'existing series has end date' do
        it 'warns if new series starts earlier than existing series and has no end' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_new(@hash, @series.id)
          expect(output).to match(/Warning/)
        end

        it 'warns if new series ends inside existing series' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc,
                                          end_date: Chronic.parse('tomorrow 8:01am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_new(@hash, @series.id)
          expect(output).to match(/Warning/)
        end

        it 'warns if new series starts inside existing series' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 8:01am', ambiguous_time_range: :none).utc,
                                          end_date: Chronic.parse('tomorrow 10:00am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_new(@hash, @series.id)
          expect(output).to match(/Warning/)
        end
      end

      context 'existing series has no end date' do
        before :each do
          @series = create(:test_series, season_count: 0,
                                         start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                                         test_program: @program)
          allow(@test_scheduler).to receive(:series_current_testtime)
        end

        it 'warns if new series has no end and starts earlier' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_new(@hash, @series.id)
          expect(output).to match(/Warning/)
        end

        it 'warns if new series has no end and starts later' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 10:00am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_new(@hash, @series.id)
          expect(output).to match(/Warning/)
        end

        it 'warns if new series ends after existing series start' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc,
                                          end_date: Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_new(@hash, @series.id)
          expect(output).to match(/Warning/)
        end

        it 'does not warn if new series ends before existing series start' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc,
                                          end_date: Chronic.parse('tomorrow 7:30am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_new(@hash, @series.id)
          expect(output).not_to match(/Warning/)
        end
      end
    end
  end

  describe '::season_edit' do
    let(:recipe) { build(:recipe) }

    before(:each) do
      clean_db
      @program = create(:test_program, series_count: 0)
      @series = create(:test_series,
                       season_count: 0,
                       start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                       test_program: @program)
      @season = create(:test_season, episode_count: 0,
                                     duration: 60,
                                     test_series: @series,
                                     recipe_id: recipe.id)
      allow(@recipe_manager).to receive(:list_recipes).and_return([recipe.id])
      allow(@recipe_manager).to receive(:get_recipe).and_return(recipe)
      allow(@artifact_generator).to \
        receive(:check_season).and_return([0, '', ''])
    end

    after(:each) do
      clean_db
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      hash = { 'label' => 'somelabel_other' }
      expect(@test_scheduler).to receive(:series_current_testtime)
      expect(@test_scheduler).to receive(:format_duration)
      expect do
        subject.season_edit(hash, @season.id)
      end.not_to raise_error
      expect(TestSeason.count).to be(1)
      season_from_db = TestSeason.find(@season.id)
      expect(season_from_db.attributes).to include(hash)
    end

    it 'fails: on unmet requirements' do
      allow(@artifact_generator).to \
        receive(:check_season).and_return([1, '', ''])
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel_other' }
      expect do
        subject.season_edit(hash, @season.id)
      end.to raise_error(ApeError)
      expect(TestSeason.count).to be(1)
      season_from_db = TestSeason.find(@season.id)
      expect(season_from_db.attributes).to include(@season.attributes)
    end

    it 'fails: if season approved' do
      @series.approved!
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel_other' }
      expect do
        subject.season_edit(hash, @season.id)
      end.to raise_error(ApeError)
      expect(TestSeason.count).to be(1)
      season_from_db = TestSeason.find(@season.id)
      expect(season_from_db.attributes).to include(@season.attributes)
    end

    it 'fails: on not existing recipe' do
      expect(Logging).not_to receive(:info)
      hash = { 'recipe_id' => 'somelabel_other' }
      expect do
        subject.season_edit(hash, @season.id)
      end.to raise_error(ApeError)
      expect(TestSeason.count).to be(1)
      season_from_db = TestSeason.find(@season.id)
      expect(season_from_db.attributes).to include(@season.attributes)
    end

    it 'fails: on unknown attributes' do
      expect(Logging).not_to receive(:info)
      hash = { 'love' => 'somelabel' }
      expect do
        subject.season_edit(hash, @season.id)
      end.to raise_error(ActiveRecord::UnknownAttributeError)
      expect(TestSeason.count).to be(1)
      season_from_db = TestSeason.find(@season.id)
      expect(season_from_db.attributes).to include(@season.attributes)
    end

    it 'fails: on non-existing season' do
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel' }
      expect do
        subject.season_edit(hash, @season.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
      expect(TestSeason.count).to be(1)
      season_from_db = TestSeason.find(@season.id)
      expect(season_from_db.attributes).to include(@season.attributes)
    end

    describe 'set/check screenshots folder' do
      let(:recipe2) { build(:recipe) }

      before(:all) do
        FakeFS.activate!
      end

      after(:all) do
        FakeFS.deactivate!
      end

      before(:each) do
        FakeFS.clear!
        @recipe_path = "/etc/artifactomat/recipes/#{recipe.id}"
        @recipe2_path = "/etc/artifactomat/recipes/#{recipe2.id}"
        @recipe_screenshots_folder = 'screenshots'
        @new_screenshots_folder = 'displayimages'
        FileUtils.mkdir_p(File.join(@recipe_path, @recipe_screenshots_folder))
        FileUtils.mkdir_p(@recipe2_path)

        allow(@recipe_manager).to receive(:list_recipes).and_return([recipe.id, recipe2.id])
        allow(@recipe_manager).to receive(:get_recipe) do |id|
          if id == recipe.id
            recipe
          elsif id == recipe2.id
            recipe2
          end
        end

        ts = TestSeason.last
        ts.screenshots_folder = 'screenshots'
        ts.save!

        allow(Logging).to receive(:info)
        allow(@test_scheduler).to receive(:series_current_testtime)
        allow(@test_scheduler).to receive(:format_duration)
      end

      it 'succeeds if neither recipe_id nor screenshots_folder were changed' do
        expect(TestSeason.count).to be(1)
        old_screenshots_folder = TestSeason.last.screenshots_folder

        hash = { 'label' => 'somelabel_other' }
        expect do
          subject.season_edit(hash, @season.id)
        end.not_to raise_error

        expect(TestSeason.count).to be(1)
        expect(TestSeason.last.screenshots_folder).to eq(old_screenshots_folder)
        expect(TestSeason.last.label).to eq('somelabel_other')
      end

      context 'only recipe_id changed' do
        before(:each) do
          @hash = { 'recipe_id' => recipe2.id }
        end

        it 'succeeds and does not change the screenshots_folder if it exists' do
          FileUtils.mkdir_p(File.join(@recipe2_path, @recipe_screenshots_folder))
          expect(TestSeason.count).to be(1)
          old_screenshots_folder = TestSeason.last.screenshots_folder

          expect do
            subject.season_edit(@hash, @season.id)
          end.not_to raise_error

          expect(TestSeason.count).to be(1)
          expect(TestSeason.last.screenshots_folder).to eq(old_screenshots_folder)
          expect(TestSeason.last.recipe_id).to eq(recipe2.id)
        end

        it 'raises error if screenshots_folder does not exist for new recipe' do
          expect do
            subject.season_edit(@hash, @season.id)
          end.to raise_error(ApeError, /is not a valid directory/)
        end
      end

      context 'only screenshots_folder changed' do
        before(:each) do
          @hash = { 'screenshots_folder' => @new_screenshots_folder }
        end

        it 'succeeds if new screenshots_folder exists' do
          FileUtils.mkdir_p(File.join(@recipe_path, @new_screenshots_folder))

          expect do
            subject.season_edit(@hash, @season.id)
          end.not_to raise_error

          expect(TestSeason.count).to be(1)
          expect(TestSeason.last.screenshots_folder).to eq(@new_screenshots_folder)
        end

        it 'succeeds for screenshots_folder ""' do
          @hash = { 'screenshots_folder' => '' }

          expect do
            subject.season_edit(@hash, @season.id)
          end.not_to raise_error

          expect(TestSeason.count).to be(1)
          expect(TestSeason.last.screenshots_folder).to eq('')
        end

        it 'raises error if new screenshots_folder does not exist' do
          expect do
            subject.season_edit(@hash, @season.id)
          end.to raise_error(ApeError, /is not a valid directory/)
        end
      end

      context 'recipe_id and screenshots_folder changed' do
        before(:each) do
          @hash = { 'screenshots_folder' => @new_screenshots_folder, 'recipe_id' => recipe2.id }
        end

        it 'succeeds if new screenshots_folder exists at new recipe' do
          FileUtils.mkdir_p(File.join(@recipe2_path, @new_screenshots_folder))

          expect do
            subject.season_edit(@hash, @season.id)
          end.not_to raise_error

          expect(TestSeason.count).to be(1)
          expect(TestSeason.last.screenshots_folder).to eq(@new_screenshots_folder)
          expect(TestSeason.last.recipe_id).to eq(recipe2.id)
        end

        it 'succeeds for screenshots_folder ""' do
          @hash['screenshots_folder'] = ''

          expect do
            subject.season_edit(@hash, @season.id)
          end.not_to raise_error

          expect(TestSeason.count).to be(1)
          expect(TestSeason.last.screenshots_folder).to eq('')
          expect(TestSeason.last.recipe_id).to eq(recipe2.id)
        end

        it 'raises error if new screenshots_folder does not exist at new recipe' do
          expect do
            subject.season_edit(@hash, @season.id)
          end.to raise_error(ApeError, /is not a valid directory/)
        end
      end
    end

    describe 'double booking recipe' do
      before :each do
        @series.end_date = Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc
        @series.save!
        allow(Logging).to receive(:info)
        allow(@test_scheduler).to receive(:series_enough_runtime)
        allow(@test_scheduler).to receive(:format_duration)
        @hash = { 'label' => 'somelabel',
                  'duration' => 60,
                  'recipe_id' => recipe.id }
      end

      it 'does not warn if recipe is not double booked' do
        @series2 = create(:test_series, season_count: 0,
                                        start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                                        end_date: Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc,
                                        test_program: @program)
        output = subject.season_edit(@hash, @season.id)
        expect(output).not_to match(/Warning/)
      end

      it 'does not warn if recipe is booked in same series' do
        @series.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
        @series.save!
        output = subject.season_edit(@hash, @season.id)
        expect(output).not_to match(/Warning/)
      end

      it 'does not warn if double booked recipes will not overlap' do
        @series2 = create(:test_series, season_count: 0,
                                        start_date: Chronic.parse('tomorrow 10:00am', ambiguous_time_range: :none).utc,
                                        end_date: Chronic.parse('tomorrow 11:00am', ambiguous_time_range: :none).utc,
                                        test_program: @program)
        @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
        @series2.save!
        output = subject.season_edit(@hash, @season.id)
        expect(output).not_to match(/Warning/)
      end

      context 'existing series has end date' do
        it 'warns if new series starts earlier than existing series and has no end' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_edit(@hash, @season.id)
          expect(output).to match(/Warning/)
        end

        it 'warns if new series ends inside existing series' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc,
                                          end_date: Chronic.parse('tomorrow 8:01am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_edit(@hash, @season.id)
          expect(output).to match(/Warning/)
        end

        it 'warns if new series starts inside existing series' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 8:01am', ambiguous_time_range: :none).utc,
                                          end_date: Chronic.parse('tomorrow 10:00am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_edit(@hash, @season.id)
          expect(output).to match(/Warning/)
        end
      end

      context 'existing series has no end date' do
        before :each do
          clean_db
          @series = create(:test_series, season_count: 0,
                                         start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                                         test_program: @program)
          @season = create(:test_season, episode_count: 0,
                                         duration: 60,
                                         test_series: @series,
                                         recipe_id: recipe.id)
          allow(@test_scheduler).to receive(:series_current_testtime)
        end

        it 'warns if new series has no end and starts earlier' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_edit(@hash, @season.id)
          expect(output).to match(/Warning/)
        end

        it 'warns if new series has no end and starts later' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 10:00am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_edit(@hash, @season.id)
          expect(output).to match(/Warning/)
        end

        it 'warns if new series ends after existing series start' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc,
                                          end_date: Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_edit(@hash, @season.id)
          expect(output).to match(/Warning/)
        end

        it 'does not warn if new series ends before existing series start' do
          @series2 = create(:test_series, season_count: 0,
                                          start_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc,
                                          end_date: Chronic.parse('tomorrow 7:30am', ambiguous_time_range: :none).utc,
                                          test_program: @program)
          @series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
          @series2.save!
          output = subject.season_edit(@hash, @season.id)
          expect(output).not_to match(/Warning/)
        end
      end
    end
  end

  describe '::season_list' do
    before(:each) do
      clean_db
      @season = create(:test_season, episode_count: 0)
    end

    it 'succeeds' do
      expect(subject.season_list).to contain_exactly(@season.attributes)
    end
  end

  describe '::season_show' do
    before(:each) do
      clean_db
      @season = create(:test_season, episode_count: 0)
    end

    it 'succeeds' do
      expect(@an_ape).to receive(:deflate_season).with(@season).and_return(@season.attributes)
      expect(subject.season_show(@season.id)).to include(@season.attributes)
    end

    it 'fails: on bad id' do
      expect do
        subject.season_show(@season.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe '::season_delete' do
    before(:each) do
      clean_db
      @season = create(:test_season, episode_count: 0)
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      expect do
        subject.season_delete(@season.id)
      end.not_to raise_error
      expect(TestSeason.count).to be(0)
    end

    it 'fails: on approved schedule' do
      @season.test_series.approved!
      expect(Logging).not_to receive(:info)
      expect do
        subject.season_delete(@season.id)
      end.to raise_error(ApeError)
      expect(TestSeason.count).to be(1)
    end

    it 'fails: on non-existing season' do
      expect(Logging).not_to receive(:info)
      expect do
        subject.season_delete(@season.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
      expect(TestSeason.count).to be(1)
    end
  end
end
