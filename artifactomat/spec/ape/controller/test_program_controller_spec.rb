# frozen_string_literal: true

require 'rspec'
require 'artifactomat/modules/ape'
require 'artifactomat/modules/ape/controller/test_program_controller'

describe 'TestProgramController' do
  before(:each) do
    @an_ape = double('Ape')
    allow(Ape).to receive(:instance).and_return(@an_ape)
  end

  subject do
    described_class
    Class.new do
      include ApeController::TestProgramController::Functions
    end.new
  end

  def clean_db
    TestProgram.destroy_all
    TestSeries.destroy_all
    TestSeason.destroy_all
    TestEpisode.destroy_all
  end

  describe '::program_new' do
    before(:each) do
      clean_db
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      expect do
        subject.program_new(label: 'some_label')
      end.not_to raise_error
      expect(TestProgram.count).to be(1)
    end

    it 'fails: on bad data' do
      expect(Logging).not_to receive(:info)
      expect do
        subject.program_new(not: 'label')
      end.to raise_error(ActiveRecord::UnknownAttributeError)
      expect(TestProgram.count).to be(0)
    end
  end

  describe '::program_edit' do
    before(:each) do
      clean_db
      @program = create(:test_program, series_count: 0)
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      data = { 'label' => 'some_other_label' }
      expect do
        subject.program_edit(data, @program.id)
      end.not_to raise_error
      expect(TestProgram.count).to be(1)
      tp_from_db = TestProgram.find(@program.id)
      expect(tp_from_db.attributes).to include(data)
    end

    it 'fails: on bad id' do
      expect(Logging).not_to receive(:info)
      data = { 'label' => 'some_other_label' }
      expect do
        subject.program_edit(data, @program.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
      expect(TestProgram.count).to be(1)
      tp_from_db = TestProgram.find(@program.id)
      expect(tp_from_db.attributes).to include(@program.attributes)
    end

    it 'fails: on bad data' do
      expect(Logging).not_to receive(:info)
      data = { 'not_label' => 'some_other_label' }
      expect do
        subject.program_edit(data, @program.id)
      end.to raise_error(ActiveRecord::UnknownAttributeError)
      expect(TestProgram.count).to be(1)
      tp_from_db = TestProgram.find(@program.id)
      expect(tp_from_db.attributes).to include(@program.attributes)
    end
  end

  describe '::program_list' do
    before(:each) do
      clean_db
      @program = create(:test_program, series_count: 0)
    end

    it 'succeeds' do
      expect(subject.program_list).to contain_exactly(@program.attributes)
    end
  end

  describe '::program_show' do
    before(:each) do
      clean_db
      @program = create(:test_program, series_count: 0)
    end

    it 'succeeds' do
      expect(@an_ape).to receive(:deflate_program).and_return(@program.attributes)
      expect(subject.program_show(@program.id)).to include(@program.attributes)
    end

    it 'fails: on bad id' do
      expect do
        subject.program_show(@program.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe '::program_delete' do
    before(:each) do
      clean_db
      @program = create(:test_program, series_count: 0)
    end

    context 'without series' do
      it 'succeeds without series' do
        expect(Logging).to receive(:info)
        expect do
          subject.program_delete(@program.id)
        end.not_to raise_error
        expect(TestProgram.count).to be(0)
      end

      it 'fails on bad id' do
        expect(Logging).not_to receive(:info)
        expect do
          subject.program_delete(@program.id + 1)
        end.to raise_error(ActiveRecord::RecordNotFound)
        expect(TestProgram.count).to be(1)
      end
    end

    context 'with series' do
      before(:each) do
        @series = create(:test_series, test_program: @program, season_count: 1)
      end

      it 'succeeds with completed series' do
        expect(Logging).to receive(:info)
        @series.completed!
        expect do
          subject.program_delete(@program.id)
        end.not_to raise_error
        expect(TestProgram.count).to be(0)
      end

      it 'succeeds with fresh series' do
        expect(Logging).to receive(:info)
        @series.fresh!
        expect do
          subject.program_delete(@program.id)
        end.not_to raise_error
        expect(TestProgram.count).to be(0)
      end

      it 'succeeds with scheduled series' do
        expect(Logging).to receive(:info)
        @series.scheduled!
        expect do
          subject.program_delete(@program.id)
        end.not_to raise_error
        expect(TestProgram.count).to be(0)
      end

      it 'fails with prepared series' do
        expect(Logging).not_to receive(:info)
        @series.prepared!
        expect do
          subject.program_delete(@program.id)
        end.to raise_error(ApeError)
        expect(TestProgram.count).to be(1)
      end

      it 'fails with approved series' do
        expect(Logging).not_to receive(:info)
        @series.approved!
        expect do
          subject.program_delete(@program.id)
        end.to raise_error(ApeError)
        expect(TestProgram.count).to be(1)
      end
    end
  end
end
