# frozen_string_literal: true

require 'rspec'
require 'chronic'
require 'artifactomat/modules/ape/controller/test_series_controller'
require 'artifactomat/modules/ape'

describe 'TestSeriesController' do
  subject do
    described_class
    Class.new do
      include ApeController::TestSeriesController::Functions
    end.new
  end

  def clean_db
    without_bullet do
      TestProgram.destroy_all
      TestSeries.destroy_all
      TestSeason.destroy_all
      TestEpisode.destroy_all
    end
  end

  before(:each) do
    @an_ape = double('Ape')
    allow(Ape).to receive(:instance).and_return(@an_ape)

    @subject_manager = double('SubjectManager')
    allow(@an_ape).to receive(:subject_manager).and_return(@subject_manager)

    @test_scheduler = double('TestScheduler')
    allow(@an_ape).to receive(:test_scheduler).and_return(@test_scheduler)

    @subject_tracker = double('SubjectTracker')
    allow(@an_ape).to receive(:subject_tracker).and_return(@subject_tracker)

    @track_collector = double('TrackCollector')
    allow(@an_ape).to receive(:track_collector).and_return(@track_collector)

    @routing = double('Routing')
    allow(@an_ape).to receive(:routing).and_return(@routing)

    @infrastructure_generator = double('InfrastructureGenerator')
    allow(@an_ape).to receive(:infrastructure_generator).and_return(@infrastructure_generator)

    @artifact_generator = double('ArtifactGenerator')
    allow(@an_ape).to receive(:artifact_generator).and_return(@artifact_generator)

    @delivery_manager = double('DeliveryManager')
    allow(@an_ape).to receive(:delivery_manager).and_return(@delivery_manager)

    @deployment_manager = double('DeploymentManager')
    allow(@an_ape).to receive(:deployment_manager).and_return(@deployment_manager)
  end

  describe '::series_new' do
    let(:group_file) { 'some_file' }

    before(:each) do
      clean_db
      @program = create(:test_program, series_count: 0)
      allow(@subject_manager).to receive(:list_subject_groups).and_return([group_file])
    end

    after(:each) do
      clean_db
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      hash = { 'label' => 'somelabel',
               'start_date' => Time.now,
               'end_date' => Time.now + (10 * 60),
               'group_file' => group_file }
      expect(@test_scheduler).to receive(:series_enough_runtime).and_return(10 * 60)
      expect(@test_scheduler).to receive(:format_duration)
      expect do
        subject.series_new(hash, @program.id)
      end.not_to raise_error
      expect(TestSeries.count).to be(1)
    end

    it 'succeeds without an end date' do
      expect(Logging).to receive(:info)
      hash = { 'label' => 'somelabel',
               'start_date' => Time.now,
               'group_file' => group_file }
      expect do
        subject.series_new(hash, @program.id)
      end.not_to raise_error
      expect(TestSeries.count).to be(1)
    end

    it 'fails: on not existing group_file' do
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel',
               'group_file' => 'not group file' }
      expect do
        subject.series_new(hash, @program.id)
      end.to raise_error(ApeError)
      expect(TestSeries.count).to be(0)
    end

    it 'fails: on unknown attributes' do
      expect(Logging).not_to receive(:info)
      hash = { 'love' => 'somelabel',
               'group_file' => 'not group file' }
      expect do
        subject.series_new(hash, @program.id)
      end.to raise_error(ActiveRecord::UnknownAttributeError)
      expect(TestSeries.count).to be(0)
    end

    it 'fails: on non-existing program' do
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel',
               'group_file' => 'not group file' }
      expect do
        subject.series_new(hash, @program.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
      expect(TestSeries.count).to be(0)
    end
  end

  describe '::series_edit' do
    let(:group_file) { 'some_file' }

    before(:each) do
      clean_db
      Ape.instance
      @program = create(:test_program, series_count: 0)
      @series = create(:test_series,
                       season_count: 0,
                       group_file: group_file,
                       test_program: @program)
      allow(@subject_manager).to receive(:list_subject_groups).and_return([group_file])
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      hash = { 'label' => 'somelabel_other' }
      expect do
        subject.series_edit(hash, @series.id)
      end.not_to raise_error
      expect(TestSeries.count).to be(1)
      series_from_db = TestSeries.find(@series.id)
      expect(series_from_db.attributes).to include(hash)
    end

    it 'fails: if series approved' do
      @series.approved!
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel_other' }
      expect do
        subject.series_edit(hash, @series.id)
      end.to raise_error(ApeError)
      expect(TestSeries.count).to be(1)
      series_from_db = TestSeries.find(@series.id)
      expect(series_from_db.attributes).to include(@series.attributes)
    end

    it 'fails: on not existing group_file' do
      expect(Logging).not_to receive(:info)
      hash = { 'group_file' => 'somelabel_other' }
      expect do
        subject.series_edit(hash, @series.id)
      end.to raise_error(ApeError)
      expect(TestSeries.count).to be(1)
      series_from_db = TestSeries.find(@series.id)
      expect(series_from_db.attributes).to include(@series.attributes)
    end

    it 'fails: on unknown attributes' do
      expect(Logging).not_to receive(:info)
      hash = { 'love' => 'somelabel' }
      expect do
        subject.series_edit(hash, @series.id)
      end.to raise_error(ActiveRecord::UnknownAttributeError)
      expect(TestSeries.count).to be(1)
      series_from_db = TestSeries.find(@series.id)
      expect(series_from_db.attributes).to include(@series.attributes)
    end

    it 'fails: on non-existing series' do
      expect(Logging).not_to receive(:info)
      hash = { 'label' => 'somelabel',
               'group_file' => 'not group file' }
      expect do
        subject.series_edit(hash, @series.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
      expect(TestSeries.count).to be(1)
      series_from_db = TestSeries.find(@series.id)
      expect(series_from_db.attributes).to include(@series.attributes)
    end
  end

  describe '::series_list' do
    before(:each) do
      clean_db
      @series = create(:test_series, season_count: 0)
    end

    it 'succeeds' do
      expect(subject.series_list).to contain_exactly(@series.attributes)
    end
  end

  describe '::series_show' do
    before(:each) do
      clean_db
      @series = create(:test_series, season_count: 0)
      allow(@an_ape).to receive(:deflate_series).and_return(@series.attributes)
    end

    it 'succeeds' do
      expect(subject.series_show(@series.id)).to include(@series.attributes)
    end

    it 'fails: on bad id' do
      expect do
        subject.series_show(@series.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe '::series_delete' do
    before(:each) do
      clean_db
      @series = create(:test_series, season_count: 0)
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      expect do
        subject.series_delete(@series.id)
      end.not_to raise_error
      expect(TestSeries.count).to be(0)
    end

    it 'fails: on approved schedule' do
      @series.approved!
      expect(Logging).not_to receive(:info)
      expect do
        subject.series_delete(@series.id)
      end.to raise_error(ApeError)
      expect(TestSeries.count).to be(1)
    end

    it 'fails: on non-existing series' do
      expect(Logging).not_to receive(:info)
      expect do
        subject.series_delete(@series.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
      expect(TestSeries.count).to be(1)
    end
  end

  describe '::series_schedule' do
    let(:specimen) { build(:subject) }

    before(:each) do
      clean_db
      @hash = { 'force' => true }
      @series = create(:test_series, season_count: 0, start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc, end_date: Chronic.parse('one week from now at 8:00am', ambiguous_time_range: :none).utc)
      @season = create(:test_season, episode_count: 0, duration: 60, test_series: @series)
      allow(@subject_manager).to receive(:get_subjects).and_return([specimen])
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      expect(@test_scheduler).to receive(:schedule)
      expect do
        subject.series_schedule(@hash, @series.id)
      end.not_to raise_error
      expect(TestEpisode.count).to be(1)
    end

    it 'fails: on approved schedule' do
      @series.approved!
      expect(Logging).not_to receive(:info)
      expect(@test_scheduler).not_to receive(:schedule)
      expect do
        subject.series_schedule(@hash, @series.id)
      end.to raise_error(ApeError)
      expect(TestEpisode.count).to be(0)
    end

    it 'fails: on unknown series' do
      expect(Logging).not_to receive(:info)
      expect(@test_scheduler).not_to receive(:schedule)
      expect do
        subject.series_schedule(@hash, @series.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
      expect(TestEpisode.count).to be(0)
    end

    describe 'double booking recipe' do
      before :each do
        @hash = { 'force' => false }
        allow(Logging).to receive(:info)
        allow(@test_scheduler).to receive(:schedule)
      end

      it 'does not check on forced schedule' do
        @hash = { 'force' => true }
        expect(subject).not_to receive(:check_conflicting_series)
        subject.series_schedule(@hash, @series.id)
      end

      it 'does not fail if overlapping series has no overlapping recipes' do
        recipe = build :recipe
        series2 = create(:test_series,
                         season_count: 0,
                         start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                         end_date: Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc)
        series2.scheduled!
        series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => recipe.id, 'screenshots_folder' => '')
        series2.save!

        expect { subject.series_schedule(@hash, @series.id) }.to_not raise_error
      end

      it 'does not fail if overlapping recipes are not in overlapping series' do
        series2 = create(:test_series,
                         season_count: 0,
                         start_date: Chronic.parse('tomorrow 6:00am', ambiguous_time_range: :none).utc,
                         end_date: Chronic.parse('tomorrow 7:00am', ambiguous_time_range: :none).utc)
        series2.scheduled!
        series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => @season.recipe_id, 'screenshots_folder' => '')
        series2.save!

        expect { subject.series_schedule(@hash, @series.id) }.to_not raise_error
      end

      it 'does not fail if overlapping series with overlapping recipes is fresh' do
        series2 = create(:test_series,
                         season_count: 0,
                         start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                         end_date: Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc)
        series2.fresh!
        series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => @season.recipe_id, 'screenshots_folder' => '')
        series2.save!

        expect { subject.series_schedule(@hash, @series.id) }.to_not raise_error
      end

      it 'fails on overlapping recipe in overlapping, non fresh series' do
        series2 = create(:test_series,
                         season_count: 0,
                         start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                         end_date: Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc)
        series2.scheduled!
        series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => @season.recipe_id, 'screenshots_folder' => '')
        series2.save!

        expect { subject.series_schedule(@hash, @series.id) }.to raise_error(ApeError)
      end

      it 'returns the correct conflicts' do
        series2 = create(:test_series,
                         season_count: 0,
                         start_date: Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc,
                         end_date: Chronic.parse('tomorrow 9:00am', ambiguous_time_range: :none).utc)
        series2.scheduled!
        series2.test_seasons.new('label' => 'double', 'duration' => 60, 'recipe_id' => @season.recipe_id, 'screenshots_folder' => '')
        series2.save!

        expect { subject.series_schedule(@hash, @series.id) }.to raise_error(ApeError, /Series '#{series2.id}' also uses recipe '#{@season.recipe_id}'/)
      end
    end
  end

  describe '::series_approve' do
    before(:each) do
      clean_db
      @series = create(:test_series, season_count: 0)
      @season = create(:test_season, episode_count: 0, test_series: @series)

      subject_list = []
      (1..5).each do |i|
        subject_list << { id: i, name: "Subject#{i}", email: "user#{i}@example.com", surname: "Surname#{i}", userid: "User#{i}" }
      end
      allow(@subject_manager).to receive(:get_subjects).and_return(subject_list)

      @series.prepared!
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      expect(@test_scheduler).to receive(:ack)
      expect(@subject_tracker).to receive(:activate_series)
      expect(@track_collector).to receive(:register_series)
      expect do
        subject.series_approve(@series.id)
      end.not_to raise_error
    end

    it 'fails: on approved schedule' do
      @series.approved!
      expect(Logging).not_to receive(:info)
      expect(@test_scheduler).not_to receive(:ack)
      expect(@subject_tracker).not_to receive(:activate_series)
      expect(@track_collector).not_to receive(:register_series)
      expect do
        subject.series_approve(@series.id)
      end.to raise_error(ApeError)
    end

    it 'fails: on unknown series' do
      expect(Logging).not_to receive(:info)
      expect(@test_scheduler).not_to receive(:ack)
      expect(@subject_tracker).not_to receive(:activate_series)
      expect(@track_collector).not_to receive(:register_series)
      expect do
        subject.series_approve(@series.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe '::series_prepare' do
    before(:each) do
      clean_db
      @series = create(:test_series, season_count: 0)
      @season = create(:test_season, episode_count: 0, test_series: @series)
      @series.approved!
      allow(Thread).to receive(:start).and_yield
      @series.scheduled!
    end

    it 'succeeds' do
      expect(@routing).to receive(:create_routing).with(@series)
      expect(@artifact_generator).to \
        receive(:generate).and_return([true, '', ''])
      expect(@infrastructure_generator).to \
        receive(:generate).and_return([true, '', ''])
      expect(@an_ape).to receive(:update_job).twice
      expect(@an_ape).to receive(:inform_user)
      expect(Logging).to receive(:info)
      expect do
        subject.series_prepare(@series.id)
      end.not_to raise_error
      sleep 0.2
      @series.reload
      expect(@series.prepared?).to be_truthy
    end

    it 'fails: on unknown series' do
      expect(@routing).not_to receive(:create_routing)
      expect(Logging).not_to receive(:info)
      expect(@artifact_generator).not_to \
        receive(:generate)
      expect(@infrastructure_generator).not_to \
        receive(:generate)
      expect do
        subject.series_prepare(@series.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
      expect(@series.prepared?).to be_falsy
    end

    it 'fails: on failing artifact generation' do
      expect(@routing).to receive(:create_routing).with(@series)
      expect(Logging).not_to receive(:info)
      expect(Logging).to receive(:error)
      expect(@an_ape).to receive(:inform_user)
      expect(@an_ape).to receive(:update_job)

      expect(@artifact_generator).to \
        receive(:generate).and_return([false, '', ''])
      expect(@routing).to receive(:remove_routing).with(@series)
      expect(@artifact_generator).to \
        receive(:destroy_artifacts)
      expect(@infrastructure_generator).not_to \
        receive(:generate)
      subject.series_prepare(@series.id)
      expect(@series.prepared?).to be_falsy
    end

    it 'fails: on failing infrastructure generation' do
      expect(@routing).to receive(:create_routing).with(@series)
      expect(Logging).not_to receive(:info)
      expect(Logging).to receive(:error)
      expect(@an_ape).to receive(:inform_user)
      expect(@an_ape).to receive(:update_job).twice

      expect(@artifact_generator).to \
        receive(:generate).and_return([true, '', ''])
      expect(@artifact_generator).to \
        receive(:destroy_artifacts)
      expect(@routing).to receive(:remove_routing).with(@series)
      expect(@infrastructure_generator).to \
        receive(:generate).and_raise(CommandExecutionError)
      expect(@deployment_manager).to receive(:revoke_artifact_deployment).with(@series).once
      subject.series_prepare(@series.id)
      expect(@series.prepared?).to be_falsy
    end

    it 'sets series status to "preparing" during the preparation' do
      # this is used to preemptively stop the series preparation
      allow(@an_ape).to receive(:routing).and_raise(Exception)
      expect { subject.series_prepare(@series.id) }.to raise_error
      @series.reload
      expect(@series.preparing?).to be(true)
    end
  end

  describe '::series_abort' do
    before(:each) do
      clean_db
      @series = create(:test_series, season_count: 0)
    end

    it 'succeeds' do
      expect(Logging).to receive(:info)
      expect(@delivery_manager).to receive(:lock_season).exactly(@series.test_seasons.count).times
      expect(@deployment_manager).to receive(:withdraw).with(@series)
      expect(@track_collector).to receive(:unregister_series).with(@series)
      expect(@an_ape).to receive(:unschedule_jobs).with(@series)
      expect do
        subject.series_abort(@series.id)
      end.not_to raise_error
    end

    it 'fails: on unknown series' do
      expect(Logging).not_to receive(:info)
      expect(@delivery_manager).not_to receive(:lock_season)
      expect(@deployment_manager).not_to receive(:withdraw)
      expect(@track_collector).not_to receive(:unregister_series).with(@series)
      expect do
        subject.series_abort(@series.id + 1)
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
