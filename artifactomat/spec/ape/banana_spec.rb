# frozen_string_literal: true

require 'rspec'
require 'artifactomat/modules/ape/banana'

describe 'Banana' do
  before do
    @banana = Banana.new('localhost', 0)
    @client = @banana.instance_variable_get(:@client)
    @response = instance_double('HTTP::Message',
                                status: 200, reason: 'OK', content: 'body')
  end

  it 'generated get request without id' do
    expect(@client).to receive(:get) do |uri|
      expect(uri.path).to match('/status')
      @response
    end
    status, message = @banana.status

    expect(status).to be_truthy
    expect(message).to match(@response.content)
  end

  it 'generates get request with id' do
    expect(@client).to receive(:get) do |uri|
      expect(uri.path).to match('/recipe/parameters/7')
      @response
    end
    status, message = @banana.recipe_parameters(id: '7')
    expect(status).to be_truthy
    expect(message).to match(@response.content)
  end

  it 'generates post request with data' do
    client = @banana.instance_variable_get(:@client)
    expect(client).to receive(:post) do |uri, body|
      expect(uri.path).to match('/program/new/4')
      expect(body[:body]).to match({ 'some' => 'data' }.to_yaml)
      @response
    end
    status, message = @banana.program_new(id: '4', some: 'data')
    expect(status).to be_truthy
    expect(message).to match(@response.content)
  end

  it 'detects version missmatch' do
    response = instance_double('HTTP::Message',
                               status: 418, reason: 'OK', content: 'a body')
    expect(@client).to receive(:get) do |_uri|
      response
    end
    status, = @banana.status
    expect(status).to be_falsy
  end

  it 'can report errors' do
    response = instance_double('HTTP::Message',
                               status: 422, reason: 'Not Found', content: 'a body')
    expect(@client).to receive(:get) do |_uri|
      response
    end
    status, = @banana.status
    expect(status).to be_falsy
  end

  it 'reports that it responds to all methods' do
    expect(@banana.respond_to?(:program_new)).to be_truthy
    expect(@banana.respond_to?(:something_that_makes_no_sense)).to be_truthy

    expect { @banana.method(:program_new) }.not_to raise_error
    expect { @banana.method(:something_that_makes_no_sense) }.not_to raise_error
  end

  describe 'build_client' do
    it 'sends a request to /build/client' do
      expect(@client).to receive(:get) do |uri|
        expect(uri.path).to eq('/build/client')
        @response
      end
      @banana.build_client
    end

    it 'increases the receive timeout for one request' do
      expect(@client).to receive(:get) do
        expect(@client.receive_timeout).to eq(140)
        @response
      end
      @banana.build_client
      expect(@client.receive_timeout).to eq(65)
    end
  end
end
