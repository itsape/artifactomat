# frozen_string_literal: true

require 'artifactomat/modules/ape/subject_exporter'
require 'artifactomat/models/subject'

describe SubjectExporter do
  describe '#export' do
    before(:each) do
      @conf_man = double('ConfigurationManager')
      @sub_man = double('SubjectManager')
      subjects = [Subject.new('sub.csv/1', 'Max', 'Mustermann', 'mam', 'mam@1&1.de'),
                  Subject.new('sub.csv/12', 'Melanie', 'Musterfrau', 'mem', 'mem@1&1.de')]
      allow(@sub_man).to receive(:list_subject_groups).and_return(['sub.csv'])
      allow(@sub_man).to receive(:get_subjects).and_return(subjects)
      allow(@conf_man).to receive(:get).with('ape.anonymity_level').and_return(1)
    end

    describe 'respects k anonymity' do
      before :each do
        subjects = [Subject.new('sub.csv/1', 'Max', 'Mustermann', 'mam', 'mam@1&1.de'),
                    Subject.new('sub.csv/2', 'Melanie', 'Musterfrau', 'mem', 'mem@1&1.de'),
                    Subject.new('sub.csv/3', 'Max', 'Siebrecht', 'mas', 'mas@1&1.de'),
                    Subject.new('sub.csv/4', 'Max', 'Siemens', 'mas', 'mas@1&1.de'),
                    Subject.new('sub.csv/5', 'Miriam', 'Musterfrau', 'mim', 'mem@1&1.de')]
        allow(@sub_man).to receive(:get_subjects).and_return(subjects)
      end

      it 'exports all subjects on k=1' do
        allow(@conf_man).to receive(:get).with('ape.anonymity_level').and_return(1)

        args = { 'groupfile' => 'sub.csv', 'columns' => %w[name] }
        out_csv = SubjectExporter.export(args, @conf_man, @sub_man)
        expected = "subject,name\nsub.csv/1,Max\nsub.csv/2,Melanie\nsub.csv/3,Max\nsub.csv/4,Max\nsub.csv/5,Miriam"
        expect(out_csv).to eq(expected)
      end

      it 'does not export subjects that would not be k-anonym' do
        allow(@conf_man).to receive(:get).with('ape.anonymity_level').and_return(2)

        args = { 'groupfile' => 'sub.csv', 'columns' => %w[name] }
        out_csv = SubjectExporter.export(args, @conf_man, @sub_man)
        expected = "subject,name\nsub.csv/1,Max\nsub.csv/3,Max\nsub.csv/4,Max"
        expect(out_csv).to eq(expected)
      end

      it 'works with multiple parameters' do
        allow(@conf_man).to receive(:get).with('ape.anonymity_level').and_return(2)

        args = { 'groupfile' => 'sub.csv', 'columns' => %w[name userid email] }
        out_csv = SubjectExporter.export(args, @conf_man, @sub_man)
        expected = "subject,name,userid,email\nsub.csv/3,Max,mas,mas@1&1.de\nsub.csv/4,Max,mas,mas@1&1.de"
        expect(out_csv).to eq(expected)
      end
    end

    it 'returns "that groupfile is unknown" on unknown groupfile' do
      args = { 'groupfile' => 'sub2.csv', 'columns' => %w[surname userid] }
      out_csv = SubjectExporter.export(args, @conf_man, @sub_man)
      expected = 'that groupfile is unknown'
      expect(out_csv).to eq(expected)
    end

    it 'exports correctly with good arguments' do
      args = { 'groupfile' => 'sub.csv', 'columns' => %w[surname userid] }
      out_csv = SubjectExporter.export(args, @conf_man, @sub_man)
      expected = "subject,surname,userid\nsub.csv/1,Mustermann,mam\nsub.csv/12,Musterfrau,mem"
      expect(out_csv).to eq(expected)
    end

    it 'exports correctly without any columns' do
      args = { 'groupfile' => 'sub.csv', 'columns' => [] }
      out_csv = SubjectExporter.export(args, @conf_man, @sub_man)
      expected = "subject\nsub.csv/1\nsub.csv/12"
      expect(out_csv).to eq(expected)
    end

    it 'leaves non existent columns blank' do
      args = { 'groupfile' => 'sub.csv', 'columns' => %w[surname username userid] }
      out_csv = SubjectExporter.export(args, @conf_man, @sub_man)
      expected = "subject,surname,username,userid\nsub.csv/1,Mustermann,,mam\nsub.csv/12,Musterfrau,,mem"
      expect(out_csv).to eq(expected)
    end
  end
end
