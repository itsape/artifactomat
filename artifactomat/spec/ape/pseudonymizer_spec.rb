# frozen_string_literal: true

require 'artifactomat/modules/ape/pseudonymizer'

describe Pseudonymizer do
  describe '#pseudonymize' do
    before :each do
      subs1 = [
        Subject.new('one.csv/1', 'Max', 'Mustermann', '001', 'm@1&1.de'),
        Subject.new('one.csv/2', 'Melanie', 'Musterfrau', '002', 'm@1&1.de'),
        Subject.new('one.csv/3', 'Max', 'Mustermann', '003', 'm@1&1.de'),
        Subject.new('one.csv/4', 'Marc', 'Kramp', '004', 'm@1&1.de')
      ]
      @conf_man = double('ConfigurationManager')
      allow(@conf_man).to receive(:list_subject_groups).and_return(%w[one.csv two.csv three.csv])
      allow(@conf_man).to receive(:get_subjects).with('one.csv').and_return(subs1)
    end

    describe 'error cases' do
      before :each do
        subs2 = []
        allow(@conf_man).to receive(:get_subjects).with('two.csv').and_return(subs2)
      end

      it 'returns an error if group1 is not loaded' do
        args = { 'group1' => 'missing.csv', 'group2' => 'two.csv' }
        out = Pseudonymizer.pseudonymize(args, @conf_man)
        expect(out).to eq('missing.csv is unknown')
      end

      it 'returns an error if group2 is not loaded' do
        args = { 'group1' => 'one.csv', 'group2' => 'missing.csv' }
        out = Pseudonymizer.pseudonymize(args, @conf_man)
        expect(out).to eq('missing.csv is unknown')
      end
    end

    describe 'good cases' do
      before :each do
        @args = { 'group1' => 'one.csv', 'group2' => 'two.csv' }
      end

      it 'matches all pseudonyms in a 1:1 mapping' do
        subs2 = [
          Subject.new('two.csv/4', 'Max', 'Mustermann', '001', 'm@1&1.de'),
          Subject.new('two.csv/3', 'Melanie', 'Musterfrau', '002', 'm@1&1.de'),
          Subject.new('two.csv/2', 'Max', 'Mustermann', '003', 'm@1&1.de'),
          Subject.new('two.csv/1', 'Marc', 'Kramp', '004', 'm@1&1.de')
        ]
        allow(@conf_man).to receive(:get_subjects).with('two.csv').and_return(subs2)
        expected_output = {
          'one.csv/1' => %w[two.csv/4],
          'one.csv/2' => %w[two.csv/3],
          'one.csv/3' => %w[two.csv/2],
          'one.csv/4' => %w[two.csv/1]
        }
        out = Pseudonymizer.pseudonymize(@args, @conf_man)
        expect(out).to eq(expected_output)
      end

      it 'leaves not matching pseudonyms out' do
        subs2 = [
          Subject.new('two.csv/4', 'Max', 'Mustermann', '001', 'm@1&1.de'),
          Subject.new('two.csv/3', 'Melanie', 'Musterfrau', '002', 'm@1&1.de'),
          Subject.new('two.csv/2', 'Anna-Lena', 'Schnell', '100', 'a@1&1.de'),
          Subject.new('two.csv/1', 'Carlotta', 'Walser', '101', 'c@1&1.de')
        ]
        allow(@conf_man).to receive(:get_subjects).with('two.csv').and_return(subs2)
        expected_output = {
          'one.csv/1' => %w[two.csv/4],
          'one.csv/2' => %w[two.csv/3],
          'one.csv/3' => [],
          'one.csv/4' => []
        }
        out = Pseudonymizer.pseudonymize(@args, @conf_man)
        expect(out).to eq(expected_output)
      end

      it 'matches all pseudonyms from group2 in a 1:n mapping' do
        subs2 = [
          Subject.new('two.csv/5', 'Max', 'Musterboy', '001', 'mm@1&1.de'),
          Subject.new('two.csv/4', 'Max', 'Mustermann', '001', 'm@1&1.de'),
          Subject.new('two.csv/3', 'Melanie', 'Musterfrau', '002', 'm@1&1.de'),
          Subject.new('two.csv/2', 'Max', 'Mustermann', '003', 'm@1&1.de'),
          Subject.new('two.csv/1', 'Marc', 'Kramp', '004', 'm@1&1.de')
        ]
        allow(@conf_man).to receive(:get_subjects).with('two.csv').and_return(subs2)
        expected_output = {
          'one.csv/1' => %w[two.csv/5 two.csv/4],
          'one.csv/2' => %w[two.csv/3],
          'one.csv/3' => %w[two.csv/2],
          'one.csv/4' => %w[two.csv/1]
        }
        out = Pseudonymizer.pseudonymize(@args, @conf_man)
        expect(out).to eq(expected_output)
      end

      it 'does not return matches, if there are no matches' do
        subs2 = [
          Subject.new('two.csv/4', 'Karsten', 'Stieglitz', '200', 'k@1&1.de'),
          Subject.new('two.csv/3', 'Lukas', 'Steinmeyer', '201', 'l@1&1.de'),
          Subject.new('two.csv/2', 'Ulrike', 'Häußler', '202', 'u@1&1.de'),
          Subject.new('two.csv/1', 'Saskia', 'Muth', '203', 's@1&1.de')
        ]
        allow(@conf_man).to receive(:get_subjects).with('two.csv').and_return(subs2)
        expected_output = {
          'one.csv/1' => [],
          'one.csv/2' => [],
          'one.csv/3' => [],
          'one.csv/4' => []
        }
        out = Pseudonymizer.pseudonymize(@args, @conf_man)
        expect(out).to eq(expected_output)
      end
    end
  end
end
