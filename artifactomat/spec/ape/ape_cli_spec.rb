# frozen_string_literal: false

require 'rspec'
require 'artifactomat/modules/ape/ape_cli'
require 'active_support/hash_with_indifferent_access'

describe 'ApeCLI' do
  before(:each) do
    allow($stdout).to receive(:write)
    @uri = URI::HTTP.build(host: 'localhost', port: 8765)
    @response = instance_double('HTTP::Message',
                                status: 200, reason: 'OK', content: [].to_yaml)
  end

  it 'ape status' do
    route = '/status'
    cmd_line = %w[status]
    @uri.path = route
    expect_any_instance_of(HTTPClient).to \
      receive(:get).with(@uri).and_return(@response)
    ApeCli.run(cmd_line)
  end

  describe 'reset' do
    it 'ape reset' do
      route = '/reset'
      cmd_line = %w[reset]
      @uri.path = route
      data = { 'force' => false }.to_yaml
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape reset -f' do
      route = '/reset'
      cmd_line = %w[reset -f]
      @uri.path = route
      data = { 'force' => true }.to_yaml
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape reset --force' do
      route = '/reset'
      cmd_line = %w[reset --force]
      @uri.path = route
      data = { 'force' => true }.to_yaml
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data).and_return(@response)
      ApeCli.run(cmd_line)
    end
  end

  it 'ape query TOKEN' do
    route = '/query/token'
    cmd_line = %w[query token]
    @uri.path = route
    expect_any_instance_of(HTTPClient).to \
      receive(:get).with(@uri).and_return(@response)
    ApeCli.run(cmd_line)
  end

  describe 'export' do
    it 'ape export groupfile columns' do
      route = '/export'
      cmd_line = %w[export sub.csv name surname]
      @uri.path = route
      data = { 'groupfile' => 'sub.csv', 'columns' => %w[name surname] }.to_yaml
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data).and_return(@response)
      expect(YAML).to receive(:safe_load).with([].to_yaml)
      ApeCli.run(cmd_line)
    end
  end

  it 'ape pseudonymize group1.csv group2.csv' do
    route = '/pseudonymize'
    cmd_line = %w[pseudonymize group1.csv group2.csv]
    @uri.path = route
    data = { 'group1' => 'group1.csv', 'group2' => 'group2.csv' }.to_yaml
    expect_any_instance_of(HTTPClient).to \
      receive(:post).with(@uri, body: data).and_return(@response)
    ApeCli.run(cmd_line)
  end

  context 'report' do
    it 'ape report series ID' do
      route = '/report/series/1'
      cmd_line = %w[report series 1]
      @uri.path = route
      data = { 'headers' => [%w[subject_id recipe score course_of_action online_time], nil], 'out' => nil, 'format' => 'yaml' }.to_yaml
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape report series -f csv ID' do
      route = '/report/series/1'
      cmd_line = %w[report series -f csv 1]
      @uri.path = route
      data = { 'headers' => [%w[subject_id recipe score course_of_action online_time], nil], 'out' => nil, 'format' => 'csv' }.to_yaml
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data).and_return(@response)
      expect(YAML).to receive(:safe_load).with([].to_yaml)
      ApeCli.run(cmd_line)
    end

    it 'ape report online' do
      route = '/report/online'
      cmd_line = %w[report online]
      @uri.path = route
      data = { 'format' => 'yaml' }.to_yaml
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape report online -f csv' do
      route = '/report/online'
      cmd_line = %w[report online -f csv]
      @uri.path = route
      data = { 'format' => 'csv' }.to_yaml
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data).and_return(@response)
      expect(YAML).to receive(:safe_load).with([].to_yaml)
      ApeCli.run(cmd_line)
    end

    it 'ape report detection' do
      route = '/report/detection'
      cmd_line = %w[report detection]
      @uri.path = route
      data = { 'format' => 'yaml' }.to_yaml
      expect_any_instance_of(HTTPClient).to receive(:post).with(@uri, body: data).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape report detection -f csv' do
      route = '/report/detection'
      cmd_line = %w[report detection -f csv]
      @uri.path = route
      data = { 'format' => 'csv' }.to_yaml
      expect_any_instance_of(HTTPClient).to receive(:post).with(@uri, body: data).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape report detection --format csv' do
      route = '/report/detection'
      cmd_line = %w[report detection --format csv]
      @uri.path = route
      data = { 'format' => 'csv' }.to_yaml
      expect_any_instance_of(HTTPClient).to receive(:post).with(@uri, body: data).and_return(@response)
      ApeCli.run(cmd_line)
    end
  end

  it 'ape info' do
    route = '/info'
    cmd_line = %w[info]
    @uri.path = route
    expect_any_instance_of(HTTPClient).to \
      receive(:get).with(@uri).and_return(@response)
    expect(ApeCli).to receive(:loop).and_yield
    ApeCli.run(cmd_line)
  end

  it 'ape info --interval X' do
    route = '/info'
    cmd_line = %w[info --interval 3]
    @uri.path = route
    expect_any_instance_of(HTTPClient).to \
      receive(:get).with(@uri).and_return(@response)
    expect(ApeCli).to receive(:loop).and_yield
    expect(ApeCli).to receive(:sleep).with(3)
    ApeCli.run(cmd_line)
  end

  it 'ape client' do
    route = '/build/client'
    cmd_line = %w[client]
    @uri.path = route
    @response = instance_double('HTTP::Message', status: 200, reason: 'OK', content: ['/tmp/client.msi'].to_yaml)
    expect_any_instance_of(HTTPClient).to receive(:get).with(@uri).and_return(@response)
    expect(FileUtils).to receive(:cp).with('/tmp/client.msi', './ITSAPE.Client.msi')
    ApeCli.run(cmd_line)
  end

  it 'ape client --outfile PATH' do
    route = '/build/client'
    cmd_line = %w[client --outfile /tmp/new_client.msi]
    @uri.path = route
    @response = instance_double('HTTP::Message', status: 200, reason: 'OK', content: ['/tmp/client.msi'].to_yaml)
    expect_any_instance_of(HTTPClient).to receive(:get).with(@uri).and_return(@response)
    expect(FileUtils).to receive(:cp).with('/tmp/client.msi', '/tmp/new_client.msi')
    ApeCli.run(cmd_line)
  end

  it 'ape import trace FILE' do
    route = '/import/trace'
    data = { 'trace' => '1', 'dry_run' => false, 'tz_offset' => '-4', 'series' => '2' }
    file = double('logfile')
    allow(file).to receive(:read).and_return('1')
    allow(File).to receive(:open).and_return(file)
    cmd_line = %w[import --tz_offset -4 --series 2 trace /some/file]
    @uri.path = route
    expect_any_instance_of(HTTPClient).to \
      receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
    expect($stderr).to receive(:write)
    ApeCli.run(cmd_line)
  end

  it 'ape import helpdesk FILE' do
    route = '/import/helpdesk'
    data = { 'trace' => '1', 'dry_run' => true, 'tz_offset' => 0, 'series' => nil, 'score' => '7', 'format' => '^some(?<format>.*)$' }
    file = double('logfile')
    allow(file).to receive(:read).and_return('1')
    allow(File).to receive(:open).and_return(file)
    cmd_line = %w[import --dry_run helpdesk --score 7 -f ^some(?<format>.*)$ /some/file]
    @uri.path = route
    expect_any_instance_of(HTTPClient).to \
      receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
    expect($stderr).to receive(:write)
    ApeCli.run(cmd_line)
  end

  it 'ape subject list' do
    route = '/subject/list'
    cmd_line = %w[subject list]

    @uri.path = route
    expect_any_instance_of(HTTPClient).to \
      receive(:get).with(@uri).and_return(@response)
    ApeCli.run(cmd_line)
  end

  context 'recipe' do
    it 'ape recipe list' do
      route = '/recipe/list'
      cmd_line = %w[recipe list]
      @uri.path = route

      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape recipe parameters ID' do
      route = '/recipe/parameters/1'
      cmd_line = %w[recipe parameters 1]
      @uri.path = route
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end
  end

  context 'program' do
    it 'ape program new LABEL' do
      route = '/program/new'
      @uri.path = route
      data = { 'label' => 'label1' }
      cmd_line = %w[program new --label label1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape program show ID' do
      route = '/program/show/1'
      cmd_line = %w[program show 1]
      @uri.path = route

      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape program show # fails with no ID' do
      cmd_line = %w[program show]
      expect($stderr).to receive(:write).at_least(:once)
      ApeCli.run(cmd_line)
    end

    it 'ape program edit ID --label SOME_LABEL' do
      route = '/program/edit/1'
      @uri.path = route
      data = { 'label' => 'label1' }
      cmd_line = %w[program edit --label label1 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape program edit ID # raises error' do
      cmd_line = %w[program edit 1]
      expect($stderr).to receive(:write).at_least(:once)
      ApeCli.run(cmd_line)
    end

    it 'ape program list' do
      route = '/program/list'
      cmd_line = %w[program list]
      @uri.path = route
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape program delete ID' do
      route = '/program/delete/7'
      cmd_line = %w[program delete 7]
      @uri.path = route
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end
  end

  context 'season' do
    it 'ape season list' do
      route = '/season/list'
      cmd_line = %w[season list]
      @uri.path = route
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape season new --label LABEL --duration DURATION --recipe RECIPE_ID SERIES_ID' do
      route = '/season/new/1'
      @uri.path = route
      data = { 'label' => 'label1',
               'duration' => 120,
               'recipe_id' => 'recipe_id1',
               'recipe_parameters' => ActiveSupport::HashWithIndifferentAccess.new({}) }
      cmd_line = %w[season new --label label1 --duration 2h --recipe recipe_id1 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape season new --label LABEL --duration DURATION --recipe RECIPE_ID --screenshots-folder SCREENSHOTS_FOLDER SERIES_ID' do
      route = '/season/new/1'
      @uri.path = route
      data = { 'label' => 'label1',
               'duration' => 120,
               'recipe_id' => 'recipe_id1',
               'recipe_parameters' => ActiveSupport::HashWithIndifferentAccess.new({}),
               'screenshots_folder' => 'screenshots' }
      cmd_line = %w[season new --label label1 --duration 2h --recipe recipe_id1 --screenshots-folder screenshots 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape season new --label LABEL --duration DURATION --recipe RECIPE_ID --parameters RECIPE_PARAMETER SERIES_ID' do
      route = '/season/new/1'
      @uri.path = route
      data = { 'label' => 'label1',
               'duration' => 120,
               'recipe_id' => 'recipe_id1',
               'recipe_parameters' =>
                ActiveSupport::HashWithIndifferentAccess.new(some: 'parameter') }
      cmd_line = %w[season new --label label1 --duration 2h --recipe recipe_id1 --parameters some:parameter 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape season new --label LABEL SERIES_ID # fails' do
      cmd_line = %w[season new --label label1 1]
      expect($stderr).to receive(:write).at_least(:once)
      expect_any_instance_of(HTTPClient).not_to receive(:post)
      ApeCli.run(cmd_line)
    end

    it 'ape season edit --label LABEL --recipe RECIPE_ID --parameters RECIPE_PARAMETERS  ID' do
      route = '/season/edit/1'
      @uri.path = route

      data = { 'label' => 'label1',
               'recipe_id' => 'recipe_id1',
               'recipe_parameters' => ActiveSupport::HashWithIndifferentAccess.new(some: 'value') }
      cmd_line = %w[season edit --label label1 --recipe recipe_id1 --parameters some:value 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape season edit ID --label LABEL --parameters RECIPE_PARAMETERS ID' do
      route = '/season/edit/1'
      @uri.path = route
      data = {
        'label' => 'label1',
        'recipe_parameters' => ActiveSupport::HashWithIndifferentAccess.new(some: 'value')
      }
      cmd_line = %w[season edit --label label1 --parameters some:value 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape season edit --screenshots-folder SCREENSHOTS_FOLDER ID' do
      route = '/season/edit/1'
      @uri.path = route
      data = {
        'screenshots_folder' => 'screenshots/firefox'
      }
      cmd_line = %w[season edit --screenshots-folder screenshots/firefox 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape season show ID' do
      route = '/season/show/season_id1'
      cmd_line = %w[season show season_id1]
      @uri.path = route
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape season delete ID' do
      route = '/season/delete/id1'
      cmd_line = %w[season delete id1]
      @uri.path = route
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end
  end

  context 'series' do
    it 'ape series new --label LABEL --group GROUP_FILE --start START_DATE PROGRAM_ID' do
      route = '/series/new/1'
      @uri.path = route

      data = { 'label' => 'label1', 'group_file' => 'group_file1', 'start_date' => Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc }
      cmd_line = %w[series new --label label1 --group group_file1 --start tomorrow\ 8:00am 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series new --label LABEL --group GROUP_FILE --start START_DATE --end END_DATE PROGRAM_ID' do
      route = '/series/new/1'
      @uri.path = route

      data = { 'label' => 'label1', 'group_file' => 'group_file1', 'start_date' => Chronic.parse('tomorrow 8:00am', ambiguous_time_range: :none).utc, 'end_date' => Chronic.parse('2 days from now at 0800 am', ambiguous_time_range: :none).utc }
      cmd_line = %w[series new --label label1 --group group_file1 --start tomorrow\ 8:00am --end 2\ days\ from\ now\ at\ 0800am 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series edit --label LABEL --group GROUP_FILE ID' do
      route = '/series/edit/1'
      @uri.path = route

      data = { 'label' => 'label1', 'group_file' => 'group_file1' }
      cmd_line = %w[series edit --label label1 --group group_file1 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series list' do
      route = '/series/list'
      cmd_line = %w[series list]
      @uri.path = route
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series show ID' do
      route = '/series/show/1'
      @uri.path = route
      cmd_line = %w[series show 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series delete ID' do
      route = '/series/delete/1'
      @uri.path = route
      cmd_line = %w[series delete 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series schedule ID' do
      route = '/series/schedule/1'
      @uri.path = route
      cmd_line = %w[series schedule 1]
      data = { 'force' => false }
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series schedule -f ID' do
      route = '/series/schedule/1'
      @uri.path = route
      cmd_line = %w[series schedule -f 1]
      data = { 'force' => true }
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series schedule --force ID' do
      route = '/series/schedule/1'
      @uri.path = route
      cmd_line = %w[series schedule --force 1]
      data = { 'force' => true }
      expect_any_instance_of(HTTPClient).to \
        receive(:post).with(@uri, body: data.to_yaml).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series approve ID' do
      route = '/series/approve/1'
      @uri.path = route
      cmd_line = %w[series approve 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series prepare ID' do
      route = '/series/prepare/1'
      @uri.path = route
      cmd_line = %w[series prepare 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series timetable ID' do
      route = '/series/timetable/1'
      @uri.path = route
      cmd_line = %w[series timetable 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end

    it 'ape series abort ID' do
      route = '/series/abort/1'
      @uri.path = route
      cmd_line = %w[series abort 1]
      expect_any_instance_of(HTTPClient).to \
        receive(:get).with(@uri).and_return(@response)
      ApeCli.run(cmd_line)
    end
  end
end
