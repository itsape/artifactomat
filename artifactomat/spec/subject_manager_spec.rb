# frozen_string_literal: true

require 'artifactomat/modules/configuration_manager'
require 'artifactomat/modules/subject_manager'
require 'artifactomat/modules/logging'

require 'fakefs/spec_helpers'
require 'fileutils'
require 'yaml'

describe SubjectManager do
  include FakeFS::SpecHelpers

  def write_subjects(location, subject_string, filename = 'good')
    subject_location = File.join(location, 'subjects')
    FileUtils.mkpath(subject_location)
    File.open(File.join(subject_location, filename), mode: 'w') do |f|
      f.write subject_string
    end
  end

  before(:all) do
    FakeFS.activate!

    file = File.join(File.dirname(__FILE__), '..', 'dist', 'etc', 'artifactomat')
    @artifactomat_path = File.expand_path(file)
    @good_subjects = "id,name,surname,userid,email,department\n" \
                     "1,Max,Mustermann,maxi,max@muster.man,sektion7\n" \
                     "2,Michaela,Musterfrau,micha,mic@muster.fra\n"
    @good_subjects2 = "id,name,surname,userid,email,roomno\n" \
                      "12,Max2,Mustermann2,maxi2,max2@muster.man,73\n" \
                      "22,Michaela2,Musterfrau2,micha2,mic2@muster.fra,\n"
    @changed_subjects = "id,name,surname,userid,email,department\n" \
                        "1,Max,Mustermann,maxi,max@mustermann.de,sektion7\n" \
                        "2,Michaela,Musterfrau,micha,mic@muster.fra\n"
    @blacklist_subjects = "id,name,surname,userid,email,department\n" \
                          "1,Max,Mustermann,maxi,max@muster.man,sektion7\n" \
                          "2,Michaela,Musterfrau,micha,mic@muster.fra\n" \
                          "3,Manuel,Mustermann,manu,manu@muster.man,sektion12\n"
    @bad_subjects = 'asdlöfkjsdaklö,asdöklfjasökldfj,.püqwejorpüj'
  end

  before(:each) do
    FakeFS::FileSystem.clear
    FakeFS::FileSystem.clone(@artifactomat_path)

    allow(Logging).to receive(:info)
    @cm = SubjectManager.new(@artifactomat_path)
  end

  after(:all) do
    FakeFS.deactivate!
  end

  describe '::initialize' do
    describe 'custom path' do
      it 'loads from a custom path' do
        FileUtils.mkdir '/other'
        FakeFS::FileSystem.clone(@artifactomat_path, '/other')
        expect { SubjectManager.new('/other') }.not_to raise_error
      end

      it 'raises ConfigError on not readable subjects directory' do
        allow(File).to receive(:readable?).and_return(false)
        expected = expect do
          SubjectManager.new(@artifactomat_path)
        end
        expected.to raise_error(ConfigError)
      end

      it 'raises error on non existant custom path' do
        bogus = 'sdökljfaasökldfjsdklöj'
        expect { SubjectManager.new(bogus) }.to raise_error(ConfigError)
      end
    end

    context 'subjects' do
      it 'logs error on missformated subjects' do
        expect(Logging).to receive(:error)
        write_subjects(@artifactomat_path, @bad_subjects)
        expect { SubjectManager.new(@artifactomat_path) }.to \
          raise_error(ConfigError)
      end

      it 'logs error if subject_folder is not a directory' do
        expect(Logging).to receive(:error)
        subjects_location = File.join(@artifactomat_path, 'subjects')
        FileUtils.rm(subjects_location)
        @cm = SubjectManager.new(@artifactomat_path)
      end

      describe 'blacklist' do
        before(:each) do
          write_subjects(@artifactomat_path, @blacklist_subjects, 'filter')
          @blacklist = File.join(@artifactomat_path, 'subject_blacklist')
        end

        it 'can filter subjects by blacklist' do
          expect(Logging).to receive(:info)
          File.open(@blacklist, 'a') do |f|
            f.puts "1,,Max,Mustermann,,\n2,,Michaela,Musterfrau,,\n"
          end
          expect { @cm = SubjectManager.new }.not_to raise_error
          expect(@cm.get_subjects('filter').map(&:id)).to eq(['filter/3'])
        end

        it 'can filter by subject id' do
          expect(Logging).to receive(:info)
          File.open(@blacklist, 'a') do |f|
            # blid 1 matches subjects 1 and 3, blid 2 matches subject 2, blid 3 matches subject 3
            # => only subject 1 will not get blacklisted
            f.puts "1,filter/1,,,,\n"
          end
          expect { @cm = SubjectManager.new }.not_to raise_error
          expect(@cm.get_subjects('filter').map(&:id)).to eq(['filter/2', 'filter/3'])
        end

        it 'ignores a blacklist row if multiple subjects of a group match its blacklist ID' do
          expect(Logging).to receive(:error)
          File.open(@blacklist, 'a') do |f|
            f.puts "1,,,Mustermann,,\n"
          end
          expect { @cm = SubjectManager.new }.not_to raise_error
          expect(@cm.get_subjects('filter').map(&:id)).to eq(['filter/1', 'filter/2', 'filter/3'])
        end

        it 'only ignores blacklist rows that matched multiple times' do
          expect(Logging).to receive(:error)
          expect(Logging).to receive(:info)
          File.open(@blacklist, 'a') do |f|
            # blid 1 matches subjects 1 and 3, blid 2 matches subject 2, blid 3 matches subject 3
            # => only subject 1 will not get blacklisted
            f.puts "1,,,Mustermann,,\n2,,Michaela,Musterfrau,,\n3,,Manuel,Mustermann,,\n"
          end
          expect { @cm = SubjectManager.new }.not_to raise_error
          expect(@cm.get_subjects('filter').map(&:id)).to eq(['filter/1'])
        end

        it 'does not crash on missing blacklist' do
          expect(Logging).to receive(:info).with('SubjectManager', 'Subject blacklist file is missing')
          FileUtils.rm_f(@blacklist)
          expect { @cm = SubjectManager.new }.not_to raise_error
          expect(@cm.get_subjects('filter').map(&:id)).to eq(['filter/1', 'filter/2', 'filter/3'])
        end
      end

      it 'does not crash when empty line in subject-file' do
        blank_subjects = "id,name,surname,userid,email,department\n" \
                         "1,Max,Mustermann,maxi,max@muster.man,sektion7\n" \
                         "\n"
        write_subjects(@artifactomat_path, blank_subjects)
        expect { @cm = SubjectManager.new }.not_to raise_error
      end
    end
  end

  describe '#list_subject_groups' do
    it 'succeeds' do
      write_subjects(@artifactomat_path, @good_subjects)
      write_subjects(@artifactomat_path, @good_subjects2, 'also_good')
      cm = SubjectManager.new
      expect(cm.list_subject_groups).to include('good', 'also_good')
    end
  end

  describe '#get_subjects' do
    before(:each) do
      write_subjects(@artifactomat_path, @good_subjects)
      write_subjects(@artifactomat_path, @good_subjects2, 'also_good')
      @cm = SubjectManager.new
    end

    it 'succeeds' do
      expect(@cm.get_subjects('good').length).to be == 2
    end

    it 'return subjects' do
      expect(@cm.get_subjects('good').first).to be_a_kind_of(Subject)
      expect(@cm.get_subjects('good').first.id).to match('good/1')
      expect(@cm.get_subjects('good').first.name).to match('Max')
      expect(@cm.get_subjects('good').first.email).to match('max@muster.man')
      expect(@cm.get_subjects('good').first.surname).to match('Mustermann')
      expect(@cm.get_subjects('good').first.userid).to match('maxi')
    end

    it 'raises ConfigError on unknown group' do
      expect(Logging).to receive(:error)
      expect { @cm.get_subjects('does_not_exist') }.to raise_error(ConfigError)
    end

    it 'reads arbitrary header fields' do
      expect(@cm.get_subjects('also_good').first.roomno).to match('73')
      expect(@cm.get_subjects('good').first.department).to match('sektion7')
    end
  end

  describe '#get_subject' do
    before(:each) do
      write_subjects(@artifactomat_path, @good_subjects)
      write_subjects(@artifactomat_path, @good_subjects2, 'also_good')
      @cm = SubjectManager.new
    end

    it 'succeeds' do
      expect(@cm.get_subject('good/1').userid).to match('maxi')
    end

    it 'delivers Subjects' do
      expect(@cm.get_subject('good/1')).to be_a_kind_of(Subject)
    end

    it 'raises ConfigError if subject is unknown' do
      expect do
        @cm.get_subject('good/7')
      end.to raise_error(ConfigError)
    end
  end

  describe '#get_subjects_for_userid' do
    before(:each) do
      double_subjects = "id,name,surname,userid,email,roomno\n" \
                        "1,Max,Mustermann,maxi,max@muster.man,73\n" \
                        "2,Michaela,Musterfrau,micha,mic@muster.fra,\n"
      write_subjects(@artifactomat_path, @good_subjects)
      write_subjects(@artifactomat_path, double_subjects, 'good2')
      @sm = SubjectManager.new
    end

    it 'succeeds' do
      expect(@sm.get_subjects_for_userid('maxi').length).to eq(2)
    end

    it 'delivers subjects' do
      expect(@sm.get_subjects_for_userid('maxi').first).to be_a_kind_of(Subject)
    end

    it 'delivers correct subjects' do
      @sm.get_subjects_for_userid('maxi').each do |sub|
        expect(sub.userid).to eq('maxi')
      end
    end
  end
end
