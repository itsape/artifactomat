# frozen_string_literal: true

require 'artifactomat/modules/test_scheduler'
require 'date'

describe TestScheduler do
  before(:all) do
    Logging.initialize
    @tardis = Time.parse('2017-10-18 15:30:13 +0200')
  end

  before(:each) do
    TestEpisode.destroy_all
    TestSeason.destroy_all
    TestSeries.destroy_all
    TestProgram.destroy_all
  end

  after(:each) do
    TestEpisode.destroy_all
    TestSeason.destroy_all
    TestSeries.destroy_all
    TestProgram.destroy_all
  end

  before(:each) do
    @configuration_manager = double('schedCM')
    @subject_manager = double('schedSM')

    biz = { 'hours' =>
      { mon:
        { '09:00' => '12:00', '13:00' => '17:00' },
        tue: { '09:00' => '12:00', '13:00' => '17:00' },
        wed: { '09:00' => '12:00', '13:00' => '17:00' },
        thu: { '09:00' => '12:00', '13:00' => '17:00' },
        fri: { '09:00' => '12:00', '13:00' => '17:00' } },
            'breaks' => {},
            'holidays' => [],
            'timezone' => 'Europe/Berlin' }
    @biz = Biz::Schedule.new do |config|
      config.hours = biz['hours']
      config.breaks = biz['breaks']
      config.holidays = biz['holidays']
      config.time_zone = biz['timezone']
    end
    allow(@configuration_manager).to receive(:get).with('conf.scheduler.biz').and_return(biz)
    @max_episode_delay_seconds = 30
    allow(@configuration_manager).to receive(:get).with('conf.scheduler.max_episode_delay_seconds').and_return(@max_episode_delay_seconds)

    @cooldown = 60
    allow(@configuration_manager).to receive(:get).with('conf.scheduler.cooldown').and_return(@cooldown)
    allow(@subject_manager).to receive(:blacklisted_subjects).and_return([])

    @delmgr = double('schedDelMgr')
  end

  describe 'new' do
    before(:each) do
      allow(Logging).to receive(:info)
      allow(@delmgr).to receive(:disarm)
    end

    it 'succeeds' do
      test_scheduler = TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
      expect(test_scheduler).to be_kind_of TestScheduler
    end

    it 'deletes blacklisted subjects episodes' do
      blacklist_subjects = ['blacklist/1', 'blacklist/2']
      allow(@subject_manager).to receive(:blacklisted_subjects).and_return(blacklist_subjects)
      running_ep = create(:test_episode, subject_id: blacklist_subjects[0])
      running_ep.running!
      prepared_ep = create(:test_episode, subject_id: blacklist_subjects[1])
      prepared_ep.prepared!
      TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)

      expect { running_ep.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { prepared_ep.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'disarms blacklisted subjects running episodes' do
      blacklist_subjects = ['blacklist/1', 'blacklist/2']
      allow(@subject_manager).to receive(:blacklisted_subjects).and_return(blacklist_subjects)
      eps = []
      blacklist_subjects.each do |sid|
        ep = create(:test_episode, subject_id: sid)
        ep.running!
        eps << ep
        expect(@delmgr).to receive(:disarm).with(ep)
      end
      expect(Logging).to receive(:info).exactly(eps.size).times

      TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
    end

    it 'does not disarm blacklisted subjects non-running episodes' do
      blacklist_subject = 'blacklist/1'
      allow(@subject_manager).to receive(:blacklisted_subjects).and_return([blacklist_subject])
      ep = create(:test_episode, subject_id: blacklist_subject)
      ep.prepared!

      expect(@delmgr).not_to receive(:disarm).with(ep)
      TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
    end

    it 'does not act on non-blacklisted subjects episodes' do
      blacklist_subject = 'blacklist/1'
      allow(@subject_manager).to receive(:blacklisted_subjects).and_return([blacklist_subject])
      blacklist_ep = create(:test_episode, subject_id: blacklist_subject)
      blacklist_ep.running!
      keep_ep = create(:test_episode)

      expect(@delmgr).not_to receive(:disarm).with(keep_ep)
      TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
      expect { keep_ep.reload }.not_to raise_error
      expect(keep_ep.failed?).to be_falsy
    end
  end

  describe '#running?' do
    let(:test_scheduler) { TestScheduler.new(@configuration_manager, @subject_manager, @delmgr) }

    it 'returns true after init' do
      expect(test_scheduler.running?).to be_truthy
    end
  end

  describe '#ack(series)' do
    let(:recipe1) { build(:recipe, duration: 1) }
    let(:recipe2) { build(:recipe, duration: 1) }

    before(:each) do
      @now = Time.new(2017, 10, 18, 16, 2, 2, '+02:00')
      allow(Time).to receive(:now).and_return(@now)

      @seasons = []
      @seasons[0] = create(:test_season, duration: 1, episode_count: 0, recipe_id: recipe1.id)
      @seasons[1] = create(:test_season, duration: 1, episode_count: 0, recipe_id: recipe2.id)

      @subjects = FactoryBot.build_list(:subject, 3)
      @subjects.each do |subject|
        create(:test_episode, subject_id: subject.id, test_season: @seasons[0])
        create(:test_episode, subject_id: subject.id, test_season: @seasons[1])
      end

      @series = create(:test_series, start_date: @now + 1, end_date: @now + (14 * 60), test_seasons: @seasons, season_count: 0)

      allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return(@subjects)
      allow(@configuration_manager).to receive(:get).with('conf.scheduler.cooldown').and_return(0)
      @test_scheduler = TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
      expect(Logging).to receive(:debug).with(/TestScheduler/i, /generated/i)
      @test_scheduler.schedule(@series)
    end

    it 'fails: if series is not prepared' do
      expect { @test_scheduler.ack(@series) }.to raise_error
    end

    it 'fails: if series if start_date is to old' do
      @series.prepared!
      @series.start_date = @now - 1
      expect { @test_scheduler.ack(@series) }.to raise_error
    end

    context 'works' do
      before(:each) do
        @series.prepared!
        expect(Logging).to receive(:info).with(/TestScheduler/i, /schedule.*acknowledged/i)
        @test_scheduler.ack(@series)
      end

      after(:each) do
        @test_scheduler.instance_variable_get(:@scheduler).jobs.each(&:unschedule)
      end

      it 'marks series as approved' do
        expect(@series.approved?).to be_truthy
      end

      it 'jobs are on #status' do
        expect(@test_scheduler.status['Schedules']).to be(@subjects.size * @seasons.size)
      end

      it 'runs all jobs' do
        expect(@delmgr).to receive(:arm).exactly(@subjects.size * @seasons.size).times
        expect(@delmgr).to receive(:disarm).exactly(@subjects.size * @seasons.size).times
        @series.test_episodes.each do |te|
          expect(te).to receive(:running!).exactly(:once)
          expect(te).to receive(:completed!).exactly(:once)
        end

        allow(Time).to receive(:now).and_return(@now + 14 * 60)

        sleep 1.0
      end

      it 'logs errors' do
        expect(Logging).to receive(:error).with(/Scheduler/i, /boom/i).exactly(@subjects.size * @seasons.size).times
        allow(@delmgr).to receive(:arm).and_raise('boom')
        sleep 0.5
        allow(Time).to receive(:now).and_return(@now + 14 * 60)
        expect(@delmgr).not_to receive(:disarm)
        sleep 0.5
      end
    end
  end

  describe '#generate_timetable(series)' do
    let(:recipe1) { build(:recipe) }
    let(:recipe2) { build(:recipe) }

    before(:each) do
      @now = Time.new(2017, 10, 18, 16, 2, 2, '+02:00')
      allow(Time).to receive(:now).and_return(@now)

      season1 = create(:test_season, duration: 10, episode_count: 0, recipe_id: recipe1.id)
      season2 = create(:test_season, duration: 10, episode_count: 0, recipe_id: recipe2.id)

      @subjects = FactoryBot.build_list(:subject, 3)
      @s = {}
      @subjects.each do |subject|
        @s[subject.id] = subject
        season1.test_episodes << create(:test_episode, subject_id: subject.id)
        season2.test_episodes << create(:test_episode, subject_id: subject.id)
      end

      @series = create(:test_series, start_date: @now + 1, end_date: @now + (300 * 60), test_seasons: [season1, season2], season_count: 0)

      allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return(@subjects)
      @s.each do |id, s|
        allow(@subject_manager).to receive(:get_subject).with(id).and_return(s)
      end

      @test_scheduler = TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
      expect(Logging).to receive(:debug).with(/TestScheduler/i, /generated/i)
      @test_scheduler.schedule(@series)
    end

    it 'lists all: schedule_start,schedule_end,subject.userid' do
      tt = @test_scheduler.generate_timetable(@series)
      @series.test_episodes.each do |e|
        expect(tt).to include e.schedule_start.to_s
        expect(tt).to include e.schedule_end.to_s
      end
      @s.each_value do |s|
        expect(tt).to include s.userid
      end
    end
  end

  describe '#schedule(series) with spare time' do
    let(:recipe1) { build(:recipe) }
    let(:recipe2) { build(:recipe) }

    before(:each) do
      @r = { recipe1.id => recipe1, recipe2.id => recipe2 }
      @now = Time.new(2017, 10, 18, 16, 2, 2, '+02:00')
      allow(Time).to receive(:now).and_return(@now)

      season1 = create(:test_season, duration: 10, episode_count: 0, recipe_id: recipe1.id)
      season2 = create(:test_season, duration: 10, episode_count: 0, recipe_id: recipe2.id)

      @subjects = FactoryBot.build_list(:subject, 3)
      @subjects.each do |subject|
        season1.test_episodes << create(:test_episode, subject_id: subject.id)
        season2.test_episodes << create(:test_episode, subject_id: subject.id)
      end

      @series = create(:test_series, start_date: @now + 1, end_date: @now + (120 * 60), test_seasons: [season1, season2], season_count: 0)

      allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return(@subjects)

      @test_scheduler = TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
    end

    it 'raises no error' do
      expect(Logging).to receive(:debug).with(/TestScheduler/i, /generated/i)
      expect { @test_scheduler.schedule(@series) }.not_to raise_error
    end

    context 'and the resulting schedule' do
      before(:each) do
        expect(Logging).to receive(:debug).with(/TestScheduler/i, /generated/i)
        @test_scheduler.schedule(@series)
      end

      it 'sets series.scheduled?' do
        expect(@series.scheduled?).to be_truthy
      end

      it 'all episodes have schedules now' do
        @series.test_episodes.reload.each do |episode|
          expect(episode.schedule_start).not_to be_nil
          expect(episode.schedule_end).not_to be_nil
        end
      end

      it 'all episodes have a runtime of at least season.duration outside of biz.hours' do
        @series.test_episodes.reload.each do |episode|
          duration = episode.test_season.duration
          actual_duration = @biz.within(episode.schedule_start, episode.schedule_end).in_minutes
          expect(actual_duration).to eq(duration), "duration should be #{duration}, but is #{actual_duration}"
        end
      end

      it 'all episodes are scheduled behind the series.start_date' do
        @series.test_episodes.reload.each do |episode|
          expect(episode.schedule_start).to be >= @series.start_date
        end
      end

      it 'all episodes grouped by subjects start at least $cooldown minutes apart' do
        @subjects.each do |subject|
          episodes = TestEpisode.where(subject_id: subject.id).order(:schedule_start).to_a
          episodes.combination(2) do |a, b|
            expect(a.schedule_end).to be <= (b.schedule_start - 60)
          end
        end
      end
    end

    it 'fails: on series.state == approved/prepared/completed' do
      expect(Logging).to receive(:error).with(/TestScheduler/i, /approved/i).once
      @series.approved!
      expect { @test_scheduler.schedule(@series) }.to raise_error
      expect(Logging).to receive(:error).with(/TestScheduler/i, /prepared/i).once
      @series.prepared!
      expect { @test_scheduler.schedule(@series) }.to raise_error
      expect(Logging).to receive(:error).with(/TestScheduler/i, /completed/i).once
      @series.completed!
      expect { @test_scheduler.schedule(@series) }.to raise_error
    end
  end

  describe '#schedule(series) without spare time' do
    let(:recipe1) { build(:recipe) }
    let(:recipe2) { build(:recipe) }

    before(:each) do
      @r = { recipe1.id => recipe1, recipe2.id => recipe2 }
      @now = Time.new(2017, 10, 18, 16, 2, 2, '+02:00')
      allow(Time).to receive(:now).and_return(@now)

      season1 = create(:test_season, duration: 10, episode_count: 0, recipe_id: recipe1.id)
      season2 = create(:test_season, duration: 10, episode_count: 0, recipe_id: recipe2.id)

      @subjects = FactoryBot.build_list(:subject, 3)
      @subjects.each do |subject|
        season1.test_episodes << create(:test_episode, subject_id: subject.id)
        season2.test_episodes << create(:test_episode, subject_id: subject.id)
      end

      @series = create(:test_series, start_date: @now + 1, end_date: @now + (22 * 60), test_seasons: [season1, season2], season_count: 0)

      allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return(@subjects)

      @test_scheduler = TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
    end

    it 'raises no error' do
      expect(Logging).to receive(:debug).with(/TestScheduler/i, /generated/i)
      expect { @test_scheduler.schedule(@series) }.not_to raise_error
    end

    context 'and the resulting schedule' do
      before(:each) do
        expect(Logging).to receive(:debug).with(/TestScheduler/i, /generated/i)
        @test_scheduler.schedule(@series)
      end

      it 'sets series.scheduled?' do
        expect(@series.scheduled?).to be_truthy
      end

      it 'all episodes have schedules now' do
        @series.test_episodes.reload.each do |episode|
          expect(episode.schedule_start).not_to be_nil
          expect(episode.schedule_end).not_to be_nil
        end
      end

      it 'all episodes have a runtime of at least season.duration outside of biz.hours' do
        @series.test_episodes.reload.each do |episode|
          duration = episode.test_season.duration
          actual_duration = @biz.within(episode.schedule_start, episode.schedule_end).in_minutes
          expect(actual_duration).to eq(duration), "duration should be #{duration}, but is #{actual_duration}"
        end
      end

      it 'all episodes are scheduled behind the series.start_date' do
        @series.test_episodes.reload.each do |episode|
          expect(episode.schedule_start).to be >= @series.start_date
        end
      end

      it 'all episodes grouped by subjects start exactly $cooldown minutes apart' do
        @subjects.each do |subject|
          episodes = TestEpisode.where(subject_id: subject.id).order(:schedule_start).to_a
          episodes.combination(2) do |a, b|
            expect(a.schedule_end).to eq(b.schedule_start - 60)
          end
        end
      end
    end

    it 'fails: on series.state == approved/prepared/completed' do
      expect(Logging).to receive(:error).with(/TestScheduler/i, /approved/i).once
      @series.approved!
      expect { @test_scheduler.schedule(@series) }.to raise_error
      expect(Logging).to receive(:error).with(/TestScheduler/i, /prepared/i).once
      @series.prepared!
      expect { @test_scheduler.schedule(@series) }.to raise_error
      expect(Logging).to receive(:error).with(/TestScheduler/i, /completed/i).once
      @series.completed!
      expect { @test_scheduler.schedule(@series) }.to raise_error
    end
  end

  describe 'restarting' do
    let(:recipe1) { build(:recipe) }
    let(:recipe2) { build(:recipe) }

    before(:each) do
      @scheduler = double('Scheduler')
      allow(Scheduler).to receive(:new).and_return(@scheduler)

      @now = Time.new(2017, 10, 18, 16, 2, 2, '+02:00')
      allow(Time).to receive(:now).and_return(@now)

      season1 = create(:test_season, duration: 1, episode_count: 0, recipe_id: recipe1.id)
      season2 = create(:test_season, duration: 1, episode_count: 0, recipe_id: recipe2.id)

      @subjects = FactoryBot.build_list(:subject, 3)
      @subjects.each do |subject|
        season1.test_episodes << create(:test_episode, subject_id: subject.id)
        season2.test_episodes << create(:test_episode, subject_id: subject.id)
      end

      @series = create(:test_series, start_date: @now + 1, end_date: @now + (14 * 60), test_seasons: [season1, season2], season_count: 0)

      allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return(@subjects)

      allow(@configuration_manager).to receive(:get).with('conf.scheduler.cooldown').and_return(0)
      @test_scheduler = TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
      expect(Logging).to receive(:debug).with(/TestScheduler/i, /generated/i)
      @test_scheduler.schedule(@series)
      @series.approved!
    end

    describe 'schedule recreation' do
      it 'adds jobs for episodes that are in time' do
        @series.test_seasons[0].test_episodes.each(&:running!)

        @series.test_seasons[0].test_episodes.each do |ep|
          expect(@scheduler).to(receive(:at).with(ep.schedule_end, tags: ['disarm episode', ep.to_str]))
        end

        @series.test_seasons[1].test_episodes.each do |ep|
          expect(@scheduler).to(receive(:at).with(ep.schedule_start, tags: ['arm episode', ep.to_str]))
        end

        TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
      end

      it 'adds disarm jobs for all running episodes that are past their end_date' do
        allow(Logging).to receive(:warning)
        @series.test_seasons[0].test_episodes.each(&:running!)
        @series.test_seasons[1].test_episodes.each(&:running!)
        @series.test_seasons[0].test_episodes.each do |ep|
          ep.schedule_start = @now - @max_episode_delay_seconds - 10 # start has to be before end
          ep.schedule_end = @now - @max_episode_delay_seconds # within conf.scheduler.max_episode_delay_seconds
          ep.save!
        end
        @series.test_seasons[1].test_episodes.each do |ep|
          ep.schedule_start = @now - @max_episode_delay_seconds - 10 # start has to be before end
          ep.schedule_end = @now - @max_episode_delay_seconds - 1 # before conf.scheduler.max_episode_delay_seconds
          ep.save!
        end

        (@series.test_seasons[0].test_episodes + @series.test_seasons[1].test_episodes).each do |ep|
          expect(@scheduler).to(receive(:at).with(ep.schedule_end, tags: ['disarm episode', ep.to_str]))
        end

        TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
      end

      it 'warns for running episodes with an end date more than conf.scheduler.max_episode_delay_seconds seconds in the past' do
        allow(@scheduler).to receive(:at)
        @series.test_seasons[0].test_episodes.each(&:running!)
        @series.test_seasons[1].test_episodes.each(&:running!)
        @series.test_seasons[0].test_episodes.each do |ep|
          ep.schedule_start = @now - @max_episode_delay_seconds - 10 # start has to be before end
          ep.schedule_end = @now - @max_episode_delay_seconds - 1
          ep.save!
        end

        @series.test_seasons[0].test_episodes.each do |ep|
          expect(Logging).to(receive(:warning).with(/TestScheduler/i, /#{Regexp.quote(ep.to_str)}/))
        end

        @series.test_seasons[1].test_episodes.each do |ep|
          expect(Logging).not_to(receive(:warning).with(/TestScheduler/i, /#{Regexp.quote(ep.to_str)}/))
        end

        TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
      end

      it 'does not arm prepared episodes with a start date more than 30 seconds in the past' do
        # all episodes are prepared
        @series.test_seasons[0].test_episodes.each do |ep|
          ep.schedule_start = @now - 31
          ep.save!
        end

        @series.test_seasons[0].test_episodes.each do |ep|
          expect(@scheduler).not_to(receive(:at).with(ep.schedule_start, tags: ['arm episode', ep.to_str]))
          expect(Logging).to(receive(:error).with(/TestScheduler/i, /#{Regexp.quote(ep.to_str)}/))
        end

        @series.test_seasons[1].test_episodes.each do |ep|
          expect(@scheduler).to(receive(:at).with(ep.schedule_start, tags: ['arm episode', ep.to_str]))
        end

        TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)

        @series.test_seasons[0].test_episodes.each do |ep|
          ep.reload
          expect(ep.failed?).to(be(true))
        end

        @series.test_seasons[1].test_episodes.each do |ep|
          ep.reload
          expect(ep.failed?).to(be(false))
        end
      end

      it 'allows prepared episodes to start up to 30 seconds late' do
        # all episodes are prepared
        @series.test_seasons[0].test_episodes.each do |ep|
          ep.schedule_start = @now - 30
          ep.save!
        end

        allow(@scheduler).to receive(:at)
        @series.test_seasons[0].test_episodes.each do |ep|
          expect(@scheduler).to(receive(:at).with(ep.schedule_start, tags: ['arm episode', ep.to_str]))
        end

        TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
      end
    end
  end

  describe '#unschedule_jobs(series)' do
    before(:each) do
      @scheduler = double('Scheduler')
      allow(Scheduler).to receive(:new).and_return(@scheduler)
      @test_scheduler = TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)
    end

    it 'unschedules jobs' do
      series = build(:test_series)
      jobs = [double('job1'), double('job2')]

      jobs.each do |job|
        expect(job).to receive(:unschedule)
      end
      expect(@scheduler).to receive(:jobs).with(tag: series.to_str).and_return(jobs)

      @test_scheduler.unschedule_jobs(series)
    end
  end

  it '#stop' do
    scheduler = double('Scheduler')
    allow(Scheduler).to receive(:new).and_return(scheduler)
    test_scheduler = TestScheduler.new(@configuration_manager, @subject_manager, @delmgr)

    expect(scheduler).to receive(:shutdown).with(:wait)
    test_scheduler.stop
  end
end
