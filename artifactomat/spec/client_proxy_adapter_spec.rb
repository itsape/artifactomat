# frozen_string_literal: true

require 'json'
require 'yaml'
require 'openssl'
require 'socket'

require_relative '../dist/etc/artifactomat/plugins/client_proxy_adapter'
require_relative 'support/openssl_helper'

describe ClientProxyAdapter do
  CPA_CONN_PORT = 8123
  CPA_CONN_IP = 'localhost'
  CPA_LOG_SIG = 'STP:ClientProxyAdapter'

  def get_content(path)
    # get content from path relative to the path of this file
    abs_path = File.join(File.dirname(__FILE__), path)
    File.open(abs_path)
  end

  def extend_local_cert_path(path)
    File.join(File.dirname(__FILE__), '../dist', path)
  end

  before(:each) do
    st_conf = YAML.safe_load(get_content('../dist/etc/artifactomat/conf.d/subjecttracker.yml'),
                             [Symbol])
    @cpa_conf = st_conf['plugins']['ClientProxyAdapter']
    @cpa_conf['cert_path'] = extend_local_cert_path(@cpa_conf['cert_path'])
    conf_man = double('ConfigurationManager')
    @sub_track = double('SubjectTracker')

    allow(conf_man).to receive(:get).with('subjecttracker.plugins.ClientProxyAdapter')\
                                    .and_return(@cpa_conf)

    allow(@sub_track).to receive(:configuration_manager).and_return(conf_man)

    @cpa = ClientProxyAdapter.new @sub_track, 'CPA'
  end

  describe '#run' do
    it 'starts two threads' do
      allow(@cpa).to receive(:start_server)

      allow(Thread).to receive(:new).twice

      expect { @cpa.run }.not_to raise_error
    end

    it 'starts an SSL server' do
      allow(Thread).to receive(:new)

      expect(OpenSSL::SSL::SSLServer).to receive(:new).and_call_original

      @cpa.run
      @cpa.stop
    end
  end

  context 'running' do
    before(:each) do
      allow(@cpa).to receive(:timeout_loop)
      allow(@cpa).to receive(:read_data)

      @cpa.run
    end

    after(:each) do
      @cpa.stop
    end

    it 'accepts a connection' do
      key = load_valid_key
      cert = load_valid_cert
      sock = build_ssl_sock key, cert, CPA_CONN_IP, CPA_CONN_PORT

      expect(@cpa).to receive(:read_data)
      expect(@cpa).to receive(:process_data)
      sock.connect
      sleep 0.2 # give the server some time to read and process the data
      expect { sock.gets }.not_to raise_error
      sock.close
    end

    it 'declines an invalid certificate' do
      # create new key and build a certificate from it
      key = OpenSSL::PKey::RSA.new(2048)
      cert = build_unsigned_cert(key)
      sock = build_ssl_sock key, cert, CPA_CONN_IP, CPA_CONN_PORT

      sock.connect
      expect { sock.gets }.to raise_error(OpenSSL::SSL::SSLError, /tlsv1 alert unknown ca/)
      sock.close
    end

    it 'declines an expired certificate' do
      ca_key = load_valid_ca_key
      ca = load_valid_ca
      # create a new key and create a new, signed cert that is expired
      key = OpenSSL::PKey::RSA.new(2048)
      cert = build_signed_cert(key, ca_key, ca, Time.now - 360.days, Time.now - 2.days)
      sock = build_ssl_sock key, cert, CPA_CONN_IP, CPA_CONN_PORT

      sock.connect
      expect { sock.gets }.to raise_error(OpenSSL::SSL::SSLError, /sslv3 alert certificate expired/)
      sock.close
    end

    it 'keeps accepting connections' do
      # valid connection
      key = load_valid_key
      cert = load_valid_cert
      sock = build_ssl_sock key, cert, CPA_CONN_IP, CPA_CONN_PORT

      expect(@cpa).to receive(:read_data)
      expect(@cpa).to receive(:process_data)
      sock.connect
      expect { sock.gets }.not_to raise_error
      sock.close

      # invalid cert
      key = OpenSSL::PKey::RSA.new(2048)
      cert = build_unsigned_cert(key)
      sock = build_ssl_sock key, cert, CPA_CONN_IP, CPA_CONN_PORT

      sock.connect
      expect { sock.gets }.to raise_error(OpenSSL::SSL::SSLError, /tlsv1 alert unknown ca/)
      sock.close

      # expired cert
      ca_key = load_valid_ca_key
      ca = load_valid_ca
      key = OpenSSL::PKey::RSA.new(2048)
      cert = build_signed_cert(key, ca_key, ca, Time.now - 360.days, Time.now - 2.days)
      sock = build_ssl_sock key, cert, CPA_CONN_IP, CPA_CONN_PORT

      sock.connect
      expect { sock.gets }.to raise_error(OpenSSL::SSL::SSLError, /sslv3 alert certificate expired/)
      sock.close

      # valid connection again
      key = load_valid_key
      cert = load_valid_cert
      sock = build_ssl_sock key, cert, CPA_CONN_IP, CPA_CONN_PORT

      expect(@cpa).to receive(:read_data)
      expect(@cpa).to receive(:process_data)
      sock.connect
      expect { sock.gets }.not_to raise_error
      sleep 0.2 # give the server some time to read and process the data
      sock.close
    end
  end

  describe '#timeout_loop' do
    before(:each) do
      @subject_timeout = @cpa_conf['subject_timeout']
      @now = Time.now

      allow(Time).to receive(:now).and_return(@now)
      allow(@cpa).to receive(:loop).and_yield
      allow(@cpa).to receive(:sleep)
    end

    it 'sleeps for the expected time' do
      sleep_time = ClientProxyAdapter.const_get(:SLEEP_TIME_TIMEOUT_LOOP)

      expect(@cpa).to receive(:sleep).with(sleep_time)

      @cpa.send(:timeout_loop)
    end

    it 'only removes subjects that have not been seen for longer than the threshold' do
      subjects = { 'timeout01' => @now - (@subject_timeout + 0.001),
                   'timeout02' => @now - (2 * @subject_timeout),
                   'stay01' => @now - @subject_timeout,
                   'stay02' => @now }

      allow(@cpa).to receive(:timeout_action)
      allow(@sub_track).to receive(:push_data)
      allow(Logging).to receive(:debug)

      @cpa.instance_variable_set(:@last_seen, subjects)
      @cpa.send(:timeout_loop)

      alive_subjects = @cpa.instance_variable_get(:@last_seen)
      expect(alive_subjects.keys).to eq(%w[stay01 stay02])
    end

    it 'sends a timeout notice to the subject tracker on timeout' do
      userid = 'timeout01'
      expected_action = Action.new(userid, '', Time.now)
      expected_action.session_end = @now
      subjects = { userid => @now - (2 * @subject_timeout) }

      # expect(expected_action).to eq(Action.from_json(JSON.generate(expected_action)))
      expect(@sub_track).to receive(:push_data).with(expected_action)
      expect(Logging).to receive(:debug).with(CPA_LOG_SIG, "User \"#{userid}\" timed out: ")

      @cpa.instance_variable_set(:@last_seen, subjects)
      @cpa.send(:timeout_loop)
    end

    it 'does not send a timeout notice on no timeout' do
      subjects = { 'stay01' => @now }

      expect(@sub_track).not_to receive(:push_data)
      expect(Logging).not_to receive(:debug)

      @cpa.instance_variable_set(:@last_seen, subjects)
      @cpa.send(:timeout_loop)
    end

    it 'deletes a subject on a subject tracker error' do
      subjects = { 'timeout01' => @now - (2 * @subject_timeout) }

      expect(@sub_track).to receive(:push_data).and_raise(StandardError)
      expect(Logging).to receive(:error).with(CPA_LOG_SIG, /TIMEOUT LOOP: StandardError .*/)

      @cpa.instance_variable_set(:@last_seen, subjects)
      @cpa.send(:timeout_loop)

      alive_subjects = @cpa.instance_variable_get(:@last_seen)
      expect(alive_subjects.keys).to eq([])
    end

    it 'keeps looping on StandardError' do
      subjects = { 'timeout01' => @now - (2 * @subject_timeout) }
      sleep_time = ClientProxyAdapter.const_get(:SLEEP_TIME_TIMEOUT_LOOP)

      allow(@cpa).to receive(:loop).and_call_original
      allow(Logging).to receive(:error).with(CPA_LOG_SIG, /TIMEOUT LOOP: StandardError .*/)
      allow(@sub_track).to receive(:push_data).and_raise(StandardError)

      @cpa.instance_variable_set(:@last_seen, subjects)
      expect { Timeout.timeout(2.1 * sleep_time) { @cpa.send(:timeout_loop) } }.to raise_error(Timeout::Error)
    end

    it 'keeps looping on no error' do
      subjects = { 'timeout01' => @now - (@subject_timeout + 0.001),
                   'timeout02' => @now - (2 * @subject_timeout),
                   'stay01' => @now - @subject_timeout,
                   'stay02' => @now }
      sleep_time = ClientProxyAdapter.const_get(:SLEEP_TIME_TIMEOUT_LOOP)

      allow(@cpa).to receive(:loop).and_call_original
      allow(@sub_track).to receive(:push_data)
      allow(Logging).to receive(:debug).with(CPA_LOG_SIG, /User .+ timed out./)

      @cpa.instance_variable_set(:@last_seen, subjects)
      expect { Timeout.timeout(2.1 * sleep_time) { @cpa.send(:timeout_loop) } }.to raise_error(Timeout::Error)
    end
  end

  describe '#read_data' do
    before(:each) do
      @conn = double('connection')
    end

    it 'reads all incoming data' do
      yaml_data = "---\nsubject01:\n  :ip: 42.69.66.6\n  :action: shutdown"

      allow(@conn).to receive(:gets).and_return("---\n", "subject01:\n", "  :ip: 42.69.66.6\n", '  :action: shutdown', nil)

      data = @cpa.send(:read_data, @conn)

      expect(data).to eq(yaml_data)
    end
  end

  describe '#process_data' do
    before(:each) do
      @now = Time.now

      allow(Time).to receive(:now).and_return(@now)
    end

    it 'fails if no data was sent' do
      expect(Logging).to receive(:error).with(CPA_LOG_SIG, /PROCESS DATA: .*/).once

      @cpa.send(:process_data, nil)
    end

    it 'fails if sent data is no valid yaml' do
      expect(Logging).to receive(:error).with(CPA_LOG_SIG, /PROCESS DATA: .*/).once

      @cpa.send(:process_data, 'this is not valid yaml')
    end

    it 'correctly builds an action from a given hash' do
      expected_action = Action.new('subject01', '42.69.66.6', @now)
      action_hash = { 'subject01' => { ip: '42.69.66.6', action: 'shutdown' } }

      allow(@cpa).to receive(:update_subjects_list).and_return(true)

      expect(@sub_track).to receive(:push_data).with(expected_action)
      expect(Logging).to receive(:debug).with(CPA_LOG_SIG, /PUSHING DATA: .*/)

      @cpa.send(:process_data, action_hash.to_yaml)
    end

    it 'properly parses a login action' do
      expected_action = Action.new('subject01', '42.69.66.6', @now)
      expected_action.session_start = @now
      action_hash = { 'subject01' => { ip: '42.69.66.6', action: 'login' } }

      allow(@cpa).to receive(:update_subjects_list).and_return(true)

      expect(@sub_track).to receive(:push_data).with(expected_action)
      expect(Logging).to receive(:debug).with(CPA_LOG_SIG, /PUSHING DATA: .*/)

      @cpa.send(:process_data, action_hash.to_yaml)
    end

    it 'properly parses a logout action' do
      expected_action = Action.new('subject01', '42.69.66.6', @now)
      expected_action.session_end = @now
      action_hash = { 'subject01' => { ip: '42.69.66.6', action: 'logout' } }

      allow(@cpa).to receive(:update_subjects_list).and_return(true)

      expect(@sub_track).to receive(:push_data).with(expected_action)
      expect(Logging).to receive(:debug).with(CPA_LOG_SIG, /PUSHING DATA: .*/)

      @cpa.send(:process_data, action_hash.to_yaml)
    end

    it 'updates the last seen time when the action is not logout' do
      expected_last_seen = { 'subject01' => @now }
      action_hash = { 'subject01' => { ip: '42.69.66.6', action: 'not_logout' } }

      allow(@sub_track).to receive(:push_data)
      allow(Logging).to receive(:debug).with(CPA_LOG_SIG, /PUSHING DATA: .*/)

      @cpa.send(:process_data, action_hash.to_yaml)
      expect(@cpa.instance_variable_get(:@last_seen)).to eq(expected_last_seen)
    end

    it 'removes the subject from the last seen list if the action is logout' do
      subjects = { 'subject01' => @now }
      action_hash = { 'subject01' => { ip: '42.69.66.6', action: 'logout' } }

      allow(@sub_track).to receive(:push_data)
      allow(Logging).to receive(:debug).with(CPA_LOG_SIG, /PUSHING DATA: .*/)

      @cpa.instance_variable_set(:@last_seen, subjects)
      @cpa.send(:process_data, action_hash.to_yaml)
      expect(@cpa.instance_variable_get(:@last_seen)).to eq({})
    end

    it 'does not break when an unknown subject logs out' do
      action_hash = { 'subject01' => { ip: '42.69.66.6', action: 'logout' } }

      allow(@sub_track).to receive(:push_data)
      allow(Logging).to receive(:debug).with(CPA_LOG_SIG, /PUSHING DATA: .*/)

      @cpa.send(:process_data, action_hash.to_yaml)
      expect(@cpa.instance_variable_get(:@last_seen)).to eq({})
    end
  end
end
