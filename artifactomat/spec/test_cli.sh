rake db:drop
rake db:migrate
docker rm -f $(docker ps -a -q)
#docker rmi -f $(docker images -q)
sleep 1
bundle exec bin/ape program new --label test
sleep 1
bundle exec bin/ape program list
bundle exec bin/ape series new --label test --group test.csv 1
sleep 1
bundle exec bin/ape series list
bundle exec bin/ape season new --label test --recipe testrecipe 1
sleep 1
bundle exec bin/ape season list
bundle exec bin/ape series schedule 1
bundle exec bin/ape series prepare 1
bundle exec bin/ape status
bundle exec bin/ape info --interval 2 &
INFOS_PID=$!
for i in `seq 1 6`;
do
  bundle exec bin/ape status
  docker ps
  sleep 10
done
bundle exec bin/ape series approve 1
bundle exec bin/ape series ready 1
kill $INFOS_PID
