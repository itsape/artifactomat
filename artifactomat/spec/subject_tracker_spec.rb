# frozen_string_literal: false

require 'artifactomat/modules/subject_tracker/subject_tracker'
require 'artifactomat/models/subject'
require 'json'
require 'yaml'

class ConfigError < StandardError
end

# Class to mock a thread
class ThreadMockSubTra
  attr_accessor :status
  def initialize
    @status = 'sleep'
  end

  def alive?
    @status != 'dead'
  end

  def kill
    @status = 'dead'
  end
end

# Test CollectorPlugin
class CollectorPlugin
  attr_accessor :name

  def initialize(subject_tracker, name)
    @st = subject_tracker
    @name = name
  end

  def collect_data; end
end

# Test RecieverPlugin
class ReceiverPlugin
  attr_accessor :name
  @stopped = false

  def initialize(subject_tracker, name)
    @st = subject_tracker
    @name = name
  end

  def run; end

  def stop; end
end

describe SubjectTracker do
  before(:each) do
    @subject_manager = double('SubjectManager_ST')
    @configuration_manager = double('ConfigManager_ST')
    allow(@configuration_manager).to receive(:cfg_path).and_return('/etc/artifactomat/')
    allow(@configuration_manager).to \
      receive(:get).with('conf.ape.redis_socket') { '/tmp/artifactomat-spec-routing' }
    allow(@configuration_manager).to receive(:get).with('subjecttracker') do
      YAML.safe_load('---
      plugins:
        CollectorPlugin:
          type: collector
          file_name: collector.rb
          class_name: CollectorPlugin
          subject_timeout: 70
          cert_path: /etc/artifactomat/cert/intermediate
        ReceiverPlugin:
          type: receiver
          file_name: receiver.rb
          class_name: ReceiverPlugin
          request_interval: 1
    ')
    end

    @series = create(:test_series, group_file: 'subjects.csv', season_count: 0)
    @season = create(:test_season, test_series: @series, episode_count: 0)
    @series.test_seasons << @season
    @series.save

    @subject_list = []
    (1..5).each do |i|
      subject = Subject.new("subjects.csv/#{i}",
                            "Subject#{i}", "Surname#{i}", "User#{i}",
                            "user#{i}@example.com")
      subject.add_field('same')
      subject.same = 'same'
      @subject_list << subject
      episode = create(:test_episode, test_season: @season, subject_id: subject.id)
      @season.test_episodes << episode
    end
    # create second pseudonym for User2 in a separate series
    @series2 = create(:test_series, group_file: 'subjects2.csv', season_count: 0)
    @season2 = create(:test_season, test_series: @series2, episode_count: 0)
    @series2.test_seasons << @season2
    @series2.save

    subject2 = Subject.new('subjects2.csv/1',
                           'Subject2', 'Surname2', 'User2',
                           'user2@example.com')
    subject2.add_field('same')
    @subject_list2 = [subject2]
    episode = create(:test_episode, test_season: @season2, subject_id: subject2.id)
    @season2.test_episodes << episode

    @season2.save

    allow(@subject_manager).to receive(:get_subjects) do |gid|
      case gid
      when 'subjects.csv'
        @subject_list
      when 'subjects2.csv'
        @subject_list2
      else
        []
      end
    end
    allow(@subject_manager).to receive(:get_subject) do |a|
      ret = nil
      @subject_list.each do |subject|
        ret = subject if subject.id == a
      end
      @subject_list2.each do |subject|
        ret = subject if subject.id == a
      end
      ret
    end
    allow(@subject_manager).to receive(:list_subject_groups).and_return(['subjects.csv', 'subjects2.csv'])

    expect(Logging).to receive(:info).at_least(:once)
    expect(Logging).to receive(:debug).at_least(:once)

    allow(SubjectTracker).to receive(:require)
    allow(CollectorPlugin).to receive(:new) do
      plugin = double('General_Collector', name: 'General_Collector Plugin', request_interval: 1)
      allow(plugin).to receive(:collect_data)
      plugin
    end
    allow(ReceiverPlugin).to receive(:new) do
      plugin = double('General_Receiver', name: 'General_Receiver Plugin', request_interval: 1)
      allow(plugin).to receive(:run)
      allow(plugin).to receive(:stop)
      plugin
    end
    @vstore = VolatileStore.new(@configuration_manager)
    @subject_tracker = SubjectTracker.new(@configuration_manager, @subject_manager)
  end

  after(:each) do
    @subject_tracker.stop
    sleep 1.1
    @subject_tracker = nil

    without_bullet do
      TestProgram.destroy_all
      TestSeries.destroy_all
      TestSeason.destroy_all
      TestEpisode.destroy_all
    end
  end

  describe '#status' do
    it 'returns a Hash' do
      expect(@subject_tracker.status).to be_a(Hash)
    end

    it 'tells the status of the main thread' do
      expect(@subject_tracker.status['Running?']).to match('false').or match('true')
    end

    it 'tells about the plugins loaded' do
      expect(@subject_tracker.status['Plugins']['collectors']).to match(['General_Collector Plugin'])
      expect(@subject_tracker.status['Plugins']['receiver']).to match(['General_Receiver Plugin'])
    end

    it 'tells about invalid plugin types' do
      allow(@configuration_manager).to receive(:get).with('subjecttracker') do
        YAML.safe_load('---
        plugins:
          DumboPlugin:
            type: invalid
            file_name: collector.rb
            class_name: CollectorPlugin
            subject_timeout: 70
            cert_path: /etc/artifactomat/cert/intermediate
        ')
      end
      expect(Logging).to receive(:error).with('SubjectTracker', 'Invalid plugin type: invalid').once
      SubjectTracker.new(@configuration_manager, @subject_manager)
    end

    it 'tells about subjects' do
      expect(@subject_tracker.status['Subjects']['active']).to match(0)
      expect(@subject_tracker.status['Subjects']['tracked']).to match(6)
      ip = '192.168.2.73'
      test_time = Time.new(2016, 2, 3, 4, 10, 6)
      action = Action.new('User3', ip, test_time)
      @subject_tracker.push_data(action)
      expect(@subject_tracker.status['Subjects']['active']).to match(1)
    end

    it 'activates all sessions for a user' do
      ip = '192.168.2.73'
      test_time = Time.new(2016, 2, 3, 4, 10, 6)
      action = Action.new('User2', ip, test_time)
      @subject_tracker.push_data(action)
      expect(@subject_tracker.status['Subjects']['active']).to match(2)
    end
  end

  context '#push_data' do
    before(:each) do
      @subject_tracker.instance_variable_get(:@stm).send(:initialize)
      SubjectHistory.destroy_all
      @action = Action.new('User1', '192.168.0.11', Time.now)
      @other_action = Action.new('User2', '192.168.0.222', Time.now)
      @subject_tracker.push_data(@action)
      @subject_tracker.push_data(@other_action)
    end

    it 'tracks subjects' do
      expect(@vstore.sids_by_ip(@action.ip)).to eq(['subjects.csv/1'])
      expect(Set.new(@vstore.sids_by_ip(@other_action.ip))).to eq(Set['subjects.csv/2', 'subjects2.csv/1'])
    end

    it 'updates tracking data' do
      action_updated = Action.new('User1', '192.168.0.22', Time.now)
      @subject_tracker.push_data(action_updated)
      expect(@vstore.sids_by_ip(action_updated.ip)).to eq(['subjects.csv/1'])
      expect(@vstore.sids_by_ip(@action.ip)).to eq([])
      expect(Set.new(@vstore.sids_by_ip(@other_action.ip))).to eq(Set['subjects.csv/2', 'subjects2.csv/1'])

      other_action_updated = Action.new('User2', '192.168.0.99', Time.now)
      @subject_tracker.push_data(other_action_updated)
      expect(@vstore.sids_by_ip(action_updated.ip)).to eq(['subjects.csv/1'])
      expect(@vstore.sids_by_ip(@other_action.ip)).to eq([])
      expect(Set.new(@vstore.sids_by_ip(other_action_updated.ip))).to eq(Set['subjects.csv/2', 'subjects2.csv/1'])
    end

    it 'removes subject from tracking' do
      action_updated = Action.new('User1', '192.168.0.11', Time.now)
      action_updated.session_end = Time.now
      @subject_tracker.push_data(action_updated)
      expect(@vstore.sids_by_ip(action_updated.ip)).to eq([])
      expect(@vstore.sids_by_ip(@action.ip)).to eq([])
      expect(Set.new(@vstore.sids_by_ip(@other_action.ip))).to eq(Set['subjects2.csv/1', 'subjects.csv/2'])

      other_action_updated = Action.new('User2', '192.168.0.222', Time.now)
      other_action_updated.session_end = Time.now
      @subject_tracker.push_data(other_action_updated)
      expect(@vstore.sids_by_ip(other_action_updated.ip)).to eq([])
    end
  end

  context 'LongTermMemory' do
    before(:each) do
      now = Time.now
      allow(Time).to receive(:now).and_return(now)
    end

    it 'creates SubjectHistory-entries of active Subjects when activating a series' do
      active_subject = Action.new('User5', '192.168.0.22', Time.now)
      active_subject.session_start = Time.now - 60
      other_active_subject = Action.new('User4', '192.168.0.24', Time.now)
      @subject_tracker.push_data(active_subject)
      @subject_tracker.push_data(other_active_subject)
      @subject_tracker.activate_series(@series.id)

      expect(SubjectHistory.all.size).to be(2)
      SubjectHistory.all.each do |entry|
        expect(entry.submit_time).to be_within(1.second).of Time.now.utc
        expect(entry.session_begin).to be_within(1.second).of Time.now
        expect(entry.session_end).to be(nil)
        expect(entry.test_series_id).to eq(@series.id)
      end
    end

    it 'creates multiple SubjectHistory-entries for the same user when theyre in different active series' do
      active_subject = Action.new('User2', '192.168.0.250', Time.now)
      active_subject.session_start = Time.now - 60
      @subject_tracker.push_data(active_subject)
      @subject_tracker.activate_series(@series.id)
      expect(SubjectHistory.all.size).to eq(3)
      @subject_tracker.activate_series(@series2.id)
      expect(SubjectHistory.all.size).to eq(4)
    end

    it 'creates SubjectHistory-entries when new data is pushed' do
      active_subject = Action.new('User3', '192.168.0.23', Time.now)
      active_subject.session_start = Time.now
      @subject_tracker.activate_series(@series.id)
      @subject_tracker.push_data(active_subject)

      entry = SubjectHistory.where(subject_id: 'subjects.csv/3').first
      expect(entry.submit_time).to be_within(1.second).of Time.now.utc
      expect(entry.session_begin).to be_within(1.second).of Time.now
      expect(entry.session_end).to be(nil)
      expect(entry.test_series_id).to eq(@series.id)

      expect(SubjectHistory.all.size).to eq(5)
    end

    it 'updates SubjectHistory-entries when session has ended' do
      inactive_subject = Action.new('User3', '192.168.0.23', Time.now)
      inactive_subject.session_end = Time.now - 10
      @subject_tracker.activate_series(@series.id)
      @subject_tracker.push_data(inactive_subject)

      entries = SubjectHistory.where(subject_id: 'subjects.csv/3')
      entries.each do |entry|
        expect(entry.session_end).to be_within(1.second).of Time.now - 10
      end

      expect(SubjectHistory.all.size).to eq(5)
    end

    it 'updates multiple SubjectHistory-entries when session for user in multiple active series has ended' do
      inactive_subject = Action.new('User2', '192.168.0.22', Time.now)
      inactive_subject.session_end = Time.now - 10
      @subject_tracker.activate_series(@series.id)
      @subject_tracker.activate_series(@series2.id)
      @subject_tracker.push_data(inactive_subject)

      entries = SubjectHistory.where(subject_id: 'subjects.csv/2')
      entries.each do |entry|
        expect(entry.session_end).to be_within(1.second).of Time.now - 10
      end
      entries = SubjectHistory.where(subject_id: 'subjects2.csv/1')
      entries.each do |entry|
        expect(entry.session_end).to be_within(1.second).of Time.now - 10
      end

      expect(SubjectHistory.all.size).to eq(5)
    end

    it 'adds subjects to whitelist on active seasons only' do
      @subject_tracker.activate_series(@series.id)
      ltm = @subject_tracker.instance_variable_get(:@ltm)
      expect(ltm.tracked?(@subject_list[1].id)).to be_truthy
      expect(ltm.tracked?(@subject_list[2].id)).to be_truthy
      expect(ltm.tracked?(@subject_list[3].id)).to be_truthy
    end

    it 'removes subjects from whitelist if series deactivated' do
      @subject_tracker.activate_series(@series.id)
      ltm = @subject_tracker.instance_variable_get(:@ltm)
      expect(ltm.instance_variable_get(:@subjects).count).to eq(5)
      @subject_tracker.deactivate_series(@series.id)
      expect(ltm.instance_variable_get(:@subjects).count).to eq(0)
    end

    it 'keeps subjects on whitelist if groupfile still in use' do
      series2 = create(:test_series, group_file: 'subjects.csv', season_count: 0)
      season2 = create(:test_season, test_series: series2, episode_count: 0)
      series2.test_seasons << season2
      series2.save
      @subject_list.each do |subject|
        episode = create(:test_episode, test_season: season2, subject_id: subject.id)
        season2.test_episodes << episode
      end
      season2.save

      ltm = @subject_tracker.instance_variable_get(:@ltm)
      @subject_tracker.activate_series(@series.id)

      expect(ltm.instance_variable_get(:@subjects).count).to eq(5)
      @subject_tracker.activate_series(series2.id)
      expect(ltm.instance_variable_get(:@subjects).count).to eq(5)
      expect(ltm.instance_variable_get(:@subjects).first.count).to eq(2)

      @subject_tracker.deactivate_series(@series.id)
      expect(ltm.instance_variable_get(:@subjects).count).to eq(5)
      expect(ltm.instance_variable_get(:@subjects).values.first.count).to eq(1)
      @subject_tracker.deactivate_series(series2.id)
      expect(ltm.instance_variable_get(:@subjects).count).to eq(0)
    end
  end

  context 'SubjectTracker plugins' do
    before(:each) do
      allow(Thread).to receive(:new) do |&block|
        block.call
        ThreadMockSubTra.new
      end
      allow(@configuration_manager).to receive(:cfg_path).and_return('/etc/artifactomat/')
      allow(@configuration_manager).to receive(:get).with('subjecttracker') do
        YAML.safe_load('---
        ')
      end
      allow(@configuration_manager).to receive(:list_subject_groups).and_return(['subjects.csv'])
    end

    it 'allows access to config manager for plugins' do
      expect(@subject_tracker.configuration_manager).not_to be_nil
    end

    it 'allows the registration of a collector plugin' do
      plugin = double('SubjectTracker_Collector', name: 'LogServer Plugin', request_interval: 1)
      allow(plugin).to receive(:collect_data)
      @subject_tracker.send(:register_collector, plugin)
      expect(@subject_tracker.collector_plugins.keys).to include(plugin.name)
    end

    it 'runs collector plugin in time' do
      now = Time.now
      allow(Time).to receive(:now).and_return(now)
      plugin = double('SubjectTracker_Collector', name: 'LogServer Plugin', request_interval: 1)
      expect(plugin).to receive(:collect_data).twice
      @subject_tracker.send(:register_collector, plugin)
      allow(Time).to receive(:now).and_return(now + 1)
      @subject_tracker.send(:main_loop_tick)
    end

    it 'allows the registration of a receiver plugin' do
      plugin = double('SubjectTracker_Receiver', name: 'Syslog Plugin')
      expect(plugin).to receive(:run).once
      expect(plugin).to receive(:stop)
      @subject_tracker.send(:register_receiver, plugin)
      expect(@subject_tracker.receiver_plugins.keys).to include(plugin.name)
    end

    it 'stops a receiver plugin at the end' do
      plugin = double('SubjectTracker_Receiver', name: 'Syslog Plugin', request_interval: 1)
      expect(plugin).to receive(:run).once
      expect(plugin).to receive(:stop)
      @subject_tracker.send(:register_receiver, plugin)
    end
  end

  context 'data provided by collector plugin' do
    before(:each) do
      SubjectHistory.destroy_all
      # allow long term memory access
      @subject_tracker.activate_series(@series.id)
    end

    it 'gets new subject-ip pairs' do
      subject_id = 'subjects.csv/1'
      ip = '192.168.2.1'
      test_time = Time.new(2016, 2, 3, 4, 5, 6)
      action = Action.new('User1', ip, test_time)
      action.session_start = test_time
      @subject_tracker.push_data(action)

      req = "subject_id = '#{subject_id}'"
      object = SubjectHistory.where(req)

      expect(object.length).to eq 1
      expect(object.first.subject_id).to eq(subject_id)
      expect(object.first.ip).to eq(ip)
      expect(object.first.session_begin).to eq(test_time.utc)
      expect(object.first.session_end).to be nil
    end

    it 'does not add a subject-ip pair for inactive series' do
      subject_id1 = 'subjects.csv/2'
      subject_id2 = 'subjects2.csv/1'
      ip = '192.168.2.2'
      test_time = Time.new(2016, 2, 3, 4, 5, 6)
      action = Action.new('User2', ip, test_time)
      action.session_start = test_time
      @subject_tracker.push_data(action)

      req = "subject_id = '#{subject_id1}'"
      object1 = SubjectHistory.where(req)
      expect(object1.length).to eq(1)

      req = "subject_id = '#{subject_id2}'"
      object2 = SubjectHistory.where(req)
      expect(object2.length).to eq(0)
    end

    it 'gets all new subject-ip pairs when multiple series are activated' do
      @subject_tracker.activate_series(@series2.id)
      subject_id1 = 'subjects.csv/2'
      subject_id2 = 'subjects2.csv/1'
      ip = '192.168.2.2'
      test_time = Time.new(2016, 2, 3, 4, 5, 6)
      action = Action.new('User2', ip, test_time)
      action.session_start = test_time
      @subject_tracker.push_data(action)

      req = "subject_id = '#{subject_id1}'"
      object = SubjectHistory.where(req)

      expect(object.length).to eq 1
      expect(object.first.subject_id).to eq(subject_id1)
      expect(object.first.ip).to eq(ip)
      expect(object.first.session_begin).to eq(test_time.utc)
      expect(object.first.session_end).to be nil

      req = "subject_id = '#{subject_id2}'"
      object = SubjectHistory.where(req)

      expect(object.length).to eq 1
      expect(object.first.subject_id).to eq(subject_id2)
      expect(object.first.ip).to eq(ip)
      expect(object.first.session_begin).to eq(test_time.utc)
      expect(object.first.session_end).to be nil
    end

    it 'gets all known subject-ip pairs for a user' do
      @subject_tracker.activate_series(@series2.id)
      subject_id = 'subjects.csv/2'
      subject_id2 = 'subjects2.csv/1'
      ip = '192.168.2.1'
      test_time = Time.new(2016, 2, 3, 4, 10, 6)
      action = Action.new('User2', ip, test_time)

      SubjectHistory.create(
        subject_id: subject_id,
        ip: ip,
        session_begin: Time.new(2016, 2, 3, 4, 5, 6),
        submit_time: Time.new(2016, 2, 3, 4, 5, 6)
      )
      SubjectHistory.create(
        subject_id: subject_id2,
        ip: ip,
        session_begin: Time.new(2016, 2, 3, 4, 5, 6),
        submit_time: Time.new(2016, 2, 3, 4, 5, 6)
      )

      @subject_tracker.push_data(action)

      req = "subject_id = '#{subject_id}'"
      object = SubjectHistory.where(req)

      expect(object.length).to eq 1
      expect(object.first.subject_id).to eq(subject_id)
      expect(object.first.ip).to eq(ip)
      expect(object.first.session_begin).to eq(Time.new(2016, 2, 3, 4, 5, 6).utc)
      expect(object.first.session_end).to be nil

      req = "subject_id = '#{subject_id2}'"
      object = SubjectHistory.where(req)

      expect(object.length).to eq 1
      expect(object.first.subject_id).to eq(subject_id2)
      expect(object.first.ip).to eq(ip)
      expect(object.first.session_begin).to eq(Time.new(2016, 2, 3, 4, 5, 6).utc)
      expect(object.first.session_end).to be nil
    end

    it 'provides a new ip for a known subject-ip pair' do
      subject_id = 'subjects.csv/1'
      old_ip = '192.168.2.1'
      new_ip = '192.168.2.42'
      test_time = Time.new(2016, 2, 3, 4, 10, 6)
      action = Action.new('User1', new_ip, test_time)
      action.session_start = test_time

      SubjectHistory.create(
        subject_id: subject_id,
        ip: old_ip,
        session_begin: Time.new(2016, 2, 3, 4, 5, 6),
        submit_time: Time.new(2016, 2, 3, 4, 5, 6)
      )
      # add to short term memory
      old_action = Action.new('', old_ip, '')
      old_action.subject_id = subject_id
      @vstore.update_ip_subject_map(old_action)

      @subject_tracker.push_data(action)
      req = "subject_id = '#{subject_id}'"
      object = SubjectHistory.where(req)

      expect(object.length).to eq 2
      expect(object.first.subject_id).to eq(subject_id)
      expect(object.first.ip).to eq(old_ip)
      expect(object.first.session_begin).to eq(Time.new(2016, 2, 3, 4, 5, 6).utc)
      expect(object.first.session_end).to eq(test_time.utc)
      expect(object.last.subject_id).to eq(subject_id)
      expect(object.last.ip).to eq(new_ip)
      expect(object.last.session_begin).to eq(test_time.utc)
      expect(object.last.session_end).to be nil
    end

    it 'provides the new ip for all active series\'' do
      @subject_tracker.activate_series(@series2.id)

      subject_id = 'subjects.csv/2'
      subject_id2 = 'subjects2.csv/1'
      old_ip = '192.168.2.1'
      new_ip = '192.168.2.42'
      test_time = Time.new(2016, 2, 3, 4, 10, 6)
      action = Action.new('User2', new_ip, test_time)
      action.session_start = test_time

      SubjectHistory.create(
        subject_id: subject_id,
        ip: old_ip,
        session_begin: Time.new(2016, 2, 3, 4, 5, 6),
        submit_time: Time.new(2016, 2, 3, 4, 5, 6)
      )
      SubjectHistory.create(
        subject_id: subject_id2,
        ip: old_ip,
        session_begin: Time.new(2016, 2, 3, 4, 5, 6),
        submit_time: Time.new(2016, 2, 3, 4, 5, 6)
      )

      # add to short term memory
      old_action = Action.new('', old_ip, '')
      old_action.subject_id = subject_id
      @vstore.update_ip_subject_map(old_action)
      old_action.subject_id = subject_id2
      @vstore.update_ip_subject_map(old_action)

      @subject_tracker.push_data(action)

      req = "subject_id = '#{subject_id}'"
      object = SubjectHistory.where(req)
      expect(object.length).to eq 2
      expect(object.last.ip).to eq(new_ip)

      req = "subject_id = '#{subject_id2}'"
      object = SubjectHistory.where(req)
      expect(object.length).to eq 2
      expect(object.last.ip).to eq(new_ip)
    end

    it 'gets session_end for a subject-ip pair' do
      subject_id = 'subjects.csv/1'
      ip = '192.168.2.1'
      test_time = Time.new(2016, 2, 3, 4, 10, 6)
      action = Action.new('User1', ip, test_time)
      action.session_end = test_time

      SubjectHistory.create(
        subject_id: subject_id,
        ip: ip,
        session_begin: Time.new(2016, 2, 3, 4, 5, 6),
        submit_time: Time.new(2016, 2, 3, 4, 5, 6)
      )

      @subject_tracker.push_data(action)
      req = "subject_id = '#{subject_id}'"
      object = SubjectHistory.where(req)

      expect(object.length).to eq 1
      expect(object.first.subject_id).to eq(subject_id)
      expect(object.first.ip).to eq(ip)
      expect(object.first.session_begin).to eq(Time.new(2016, 2, 3, 4, 5, 6).utc)
      expect(object.first.session_end).to eq(test_time.utc)
    end

    it 'gets session_end for all active subject-ip pairs' do
      @subject_tracker.activate_series(@series2.id)
      subject_id = 'subjects.csv/2'
      subject_id2 = 'subjects2.csv/1'
      ip = '192.168.2.1'
      test_time = Time.new(2016, 2, 3, 4, 10, 6)
      action = Action.new('User2', ip, test_time)
      action.session_end = test_time

      SubjectHistory.create(
        subject_id: subject_id,
        ip: ip,
        session_begin: Time.new(2016, 2, 3, 4, 5, 6),
        submit_time: Time.new(2016, 2, 3, 4, 5, 6)
      )
      SubjectHistory.create(
        subject_id: subject_id2,
        ip: ip,
        session_begin: Time.new(2016, 2, 3, 4, 5, 6),
        submit_time: Time.new(2016, 2, 3, 4, 5, 6)
      )

      @subject_tracker.push_data(action)
      req = "subject_id = '#{subject_id}'"
      object = SubjectHistory.where(req)

      expect(object.length).to eq 1
      expect(object.first.subject_id).to eq(subject_id)
      expect(object.first.ip).to eq(ip)
      expect(object.first.session_begin).to eq(Time.new(2016, 2, 3, 4, 5, 6).utc)
      expect(object.first.session_end).to eq(test_time.utc)

      req = "subject_id = '#{subject_id2}'"
      object = SubjectHistory.where(req)

      expect(object.length).to eq 1
      expect(object.first.subject_id).to eq(subject_id2)
      expect(object.first.ip).to eq(ip)
      expect(object.first.session_begin).to eq(Time.new(2016, 2, 3, 4, 5, 6).utc)
      expect(object.first.session_end).to eq(test_time.utc)
    end
  end

  context 'resolves subject-data' do
    before(:all) do
      @sub1 = SubjectHistory.create(
        subject_id: 'subjects.csv/3',
        ip: '127.0.0.42',
        session_begin: Time.new(2016, 6, 6, 10, 0, 0),
        submit_time: Time.new(2016, 6, 6, 11, 1, 30)
      )
      @sub2 = SubjectHistory.create(
        subject_id: 'subjects.csv/3',
        ip: '127.0.0.44',
        session_begin: Time.new(2016, 6, 6, 9, 0, 0),
        session_end: Time.new(2016, 6, 6, 11, 0, 0),
        submit_time: Time.new(2016, 6, 6, 9, 1, 30)
      )
    end

    context '#subject_by_ip (short term memory)' do
      it 'has a short term memory' do
        subject_id = 'subjects.csv/3'
        ip = '192.168.2.73'
        test_time = Time.new(2016, 2, 3, 4, 10, 6)
        action = Action.new('User3', ip, test_time)
        @subject_tracker.push_data(action)
        expect(@vstore.sids_by_ip(ip)).to eq([subject_id])
      end

      it 'resolves all subjects for an ip' do
        subject_ids = Set['subjects.csv/2', 'subjects2.csv/1']
        ip = '192.168.2.0'
        test_time = Time.new(2016, 2, 3, 4, 10, 6)
        action = Action.new('User2', ip, test_time)
        @subject_tracker.push_data(action)
        expect(Set.new(@vstore.sids_by_ip(ip))).to eq(subject_ids)
      end

      it 'updates short term memory' do
        subject_id = 'subjects.csv/3'
        ip = '192.168.2.72'
        test_time = Time.new(2016, 2, 3, 4, 10, 6)
        action = Action.new('User3', ip, test_time)
        @subject_tracker.push_data(action)
        expect(@vstore.sids_by_ip(ip)).to eq([subject_id])
      end

      it 'updates short term memory for multiple subjects on the same ip' do
        subject_ids = Set['subjects.csv/2', 'subjects2.csv/1']
        ip = '192.168.2.1'
        test_time = Time.new(2016, 2, 3, 4, 10, 6)
        action = Action.new('User2', ip, test_time)
        @subject_tracker.push_data(action)
        expect(Set.new(@vstore.sids_by_ip(ip))).to eq(subject_ids)
      end

      it 'removes subject from short term memory on session end' do
        ip = '192.168.2.71'
        test_time = Time.new(2016, 2, 3, 4, 10, 6)
        action = Action.new('User3', ip, test_time)
        action.session_end = test_time
        @subject_tracker.push_data(action)
        expect(@vstore.sids_by_ip(ip)).to eq([])
      end

      it 'removes all subjecs for an ip from short term memory on session end' do
        ip = '192.168.2.1'
        test_time = Time.new(2016, 2, 3, 4, 10, 6)
        action = Action.new('User2', ip, test_time)
        action.session_end = test_time
        @subject_tracker.push_data(action)
        expect(@vstore.sids_by_ip(ip)).to eq([])
      end
    end

    it 'returns a subject-id to a given ip-address' do
      expect(SubjectTracker.get_subjectid_from_ip('127.0.0.42')).to eq('subjects.csv/3')
    end

    it 'can not resolve multiple subjects on same ip' do
      sub3 = SubjectHistory.create(
        subject_id: 'subjects.csv/3',
        ip: '127.0.0.42',
        session_begin: Time.new(2016, 6, 6, 9, 0, 0),
        submit_time: Time.new(2016, 6, 6, 9, 1, 30)
      )
      expect(Logging).to receive(:warning)
      expect(SubjectTracker.get_subjectid_from_ip('127.0.0.42')).to eq(nil)
      sub3.destroy
    end

    it 'returns an ip-address to a given subject-id' do
      expect(SubjectTracker.get_ip_from_subjectid('subjects.csv/3')).to eq('127.0.0.42')
    end

    it 'can not resolve subjects on multiple ips' do
      sub4 = SubjectHistory.create(
        subject_id: 'subjects.csv/3',
        ip: '127.0.0.43',
        session_begin: Time.new(2016, 6, 6, 14, 0, 0),
        submit_time: Time.new(2016, 6, 6, 14, 1, 30)
      )
      expect(Logging).to receive(:warning)
      expect(SubjectTracker.get_ip_from_subjectid('subjects.csv/3')).to eq(nil)
      sub4.destroy
    end

    after(:all) do
      @sub1.destroy
      @sub2.destroy
    end
  end
end
