# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require 'date'
require 'rspec'
require 'rack/test'
require 'fileutils'
require 'digest'
require 'active_record_spec_helper'
require 'stringio'
require 'fakefs/spec_helpers'
require 'bzip2/ffi'
require 'minitar'

require 'artifactomat/services/jobspooler'
require 'artifactomat/models/job'
require 'artifactomat/models/detection_event'
require 'active_support'

describe 'Jobspooler', system: false do
  include Rack::Test::Methods

  def app
    @job_spooler = Jobspooler.new('cert_path' => File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'dist', 'etc', 'artifactomat', 'certs')), 'tracking' => true)
  end

  before(:all) do
    @sample_users = ['sample_user', 'f4ncy-u5er', 'spec!al_user']
    @host = 'example.org'
    @json_headers = {
      'ACCEPT' => 'application/json',
      'HTTP_ACCEPT' => 'application/json'
    }
    Job.delete_all
  end

  before(:each) do
    @fake_ssl_sock = double('SSLSocketMock')
    @fake_ssl_con = double('SSLConnectionMock')
    @fake_tcp_sock = double('TCPSocketMock')
    @configuration_manager = double('ConfigurationManager')
    @subject_manager = double('SubjectManager')
    @vstore = double('VolatileStore')
    TestSeason.instance_variable_set(:@configuration_manager, @configuration_manager)
    allow(ConfigurationManager).to receive(:new).and_return(@configuration_manager)
    allow(@configuration_manager).to receive(:cfg_path).and_return('/etc/artifactomat')
    allow(SubjectManager).to receive(:new).and_return(@subject_manager)
    allow(VolatileStore).to receive(:new).and_return(@vstore)
    allow(TCPSocket).to receive(:new).and_return(@fake_tcp_sock)
    allow(@fake_ssl_sock).to receive(:connect).and_return(@fake_ssl_con)
    allow(OpenSSL::SSL::SSLSocket).to receive(:new).and_return(@fake_ssl_sock)
    allow(Logging).to receive(:info)
  end

  after(:each) do
    Job.delete_all
  end

  context 'GET /jobs/:user' do
    let(:user) { @sample_users[rand(@sample_users.size)] }

    it 'returns "204 No content" if no jobs are available' do
      allow(Thread).to receive(:new).and_yield
      expect(@fake_ssl_sock).to receive(:sync_close=) # needs to be here

      track = Hash[user, { ip: '127.0.0.1', action: 'login' }].to_yaml
      expect(@fake_ssl_con).to receive(:puts).with(track)
      expect(@fake_ssl_con).to receive(:close)

      get "/jobs/#{user}"
      expect(last_response.status).to eql(204)
    end

    it 'returns "200" and a list of jobs in json' do
      job = create(:job, user: user, expiration_date: Time.now + 30)

      allow(Thread).to receive(:new).and_yield
      expect(@fake_ssl_sock).to receive(:sync_close=) # needs to be here

      track = Hash[user, { ip: '127.0.0.1', action: 'login' }].to_yaml
      expect(@fake_ssl_con).to receive(:puts).with(track)
      expect(@fake_ssl_con).to receive(:close)

      get "/jobs/#{user}"
      joblist = ["http://example.org:80/jobs/#{job.user}/#{job.id}"]
      expect(last_response.status).to eql(200)
      expect(last_response.body).to match(joblist.to_json)
    end

    it 'does always return job that does not expire' do
      job = create(:job, user: user, expiration_date: nil)

      allow(Thread).to receive(:new).and_yield
      expect(@fake_ssl_sock).to receive(:sync_close=) # needs to be here

      track = Hash[user, { ip: '127.0.0.1', action: 'login' }].to_yaml
      expect(@fake_ssl_con).to receive(:puts).with(track)
      expect(@fake_ssl_con).to receive(:close)

      get "/jobs/#{user}"
      joblist = ["http://example.org:80/jobs/#{job.user}/#{job.id}"]
      expect(last_response.status).to eql(200)
      expect(last_response.body).to match(joblist.to_json)
    end

    it 'does not return expired jobs' do
      create(:job, user: user, expiration_date: Time.now - 30)

      allow(Thread).to receive(:new).and_yield
      expect(@fake_ssl_sock).to receive(:sync_close=) # needs to be here

      track = Hash[user, { ip: '127.0.0.1', action: 'login' }].to_yaml
      expect(@fake_ssl_con).to receive(:puts).with(track)
      expect(@fake_ssl_con).to receive(:close)

      get "/jobs/#{user}"
      expect(last_response.status).to eql(204)
    end
  end

  context 'GET /jobs/:user/:id' do
    let(:user) { @sample_users[rand(@sample_users.size)] }

    it 'returns 200:json(job) if job is there' do
      job = create(:job, user: user)

      get "/jobs/#{user}/#{job.id}"
      expect(last_response.status).to eql(200)
      expect(last_response.body).to match(job.to_json)
    end

    it 'returns 404 if job is not there' do
      job = create(:job, user: user)

      get "/jobs/#{user}/#{job.id + 1}"
      expect(last_response.status).to eql(404)
    end
  end

  context 'GET /file/:id' do
    let(:user) { @sample_users[rand(@sample_users.size)] }

    it 'returns 404 if the job does not exist' do
      get '/file/42'
      expect(last_response.status).to eql(404)
    end

    it 'returns 404 if the file does not exist' do
      @file_location = '/tmp/itsapetestfile.txt'
      File.open(@file_location, mode: 'w') do |file|
        file.write('a string in a textfile')
      end
      job = create(:job, user: user, file_path: @file_location)
      File.delete(@file_location)
      get "/file/#{job.id}"
      expect(last_response.status).to eql(404)
    end

    it 'returns the file' do
      @file_location = '/tmp/itsapetestfile.txt'
      File.open(@file_location, mode: 'w') do |file|
        file.write('a string in a textfile')
      end
      file_md5 = Digest::MD5.hexdigest 'a string in a textfile'
      job = create(:job, user: user, file_path: @file_location)
      get "/file/#{job.id}"
      expect(last_response.status).to eql(200)
      the_file_md5 = Digest::MD5.hexdigest last_response.body
      expect(the_file_md5).to match(file_md5)
      File.delete(@file_location)
    end
  end

  context 'POST /jobs/:user/:id' do
    let(:job) { create(:job, user: @sample_users[rand(@sample_users.size)]) }

    it 'updates state if pushed' do
      data = { 'status' => 'completed' }

      allow(Thread).to receive(:new).and_yield
      expect(@fake_ssl_sock).to receive(:sync_close=) # needs to be here

      expect(@fake_ssl_con).to receive(:puts).twice
      expect(@fake_ssl_con).to receive(:gets)
      expect(@fake_ssl_con).to receive(:close)

      post "/jobs/#{job.user}/#{job.id}", data.to_json
      expect(last_response.status).to eql(204)
      job.reload
      expect(job.status).to match 'completed'
    end

    it 'adds collection_date on status change to completed' do
      expect(job.collection_date).to be_nil
      data = { 'status' => 'completed' }

      allow(Thread).to receive(:new).and_yield
      allow(@fake_ssl_sock).to receive(:sync_close=) # needs to be here

      allow(@fake_ssl_con).to receive(:puts)
      allow(@fake_ssl_con).to receive(:gets)
      allow(@fake_ssl_con).to receive(:close)

      now = Time.now
      post "/jobs/#{job.user}/#{job.id}", data.to_json
      job.reload
      expect(job.collection_date).to be_within(2).of(now)
    end

    it 'adds collection_date on status change to failed' do
      expect(job.collection_date).to be_nil
      data = { 'status' => 'failed' }

      allow(Thread).to receive(:new).and_yield
      allow(@fake_ssl_sock).to receive(:sync_close=) # needs to be here

      allow(@fake_ssl_con).to receive(:puts)
      allow(@fake_ssl_con).to receive(:gets)
      allow(@fake_ssl_con).to receive(:close)

      now = Time.now
      post "/jobs/#{job.user}/#{job.id}", data.to_json
      job.reload
      expect(job.collection_date).to be_within(2).of(now)
    end

    it 'returns 404 if job not found' do
      post "/jobs/#{job.user}/#{job.id.to_i + 1}"
      expect(last_response.status).to eql(404)
    end

    it 'returns 500 if pushing bad jobstate' do
      data = { 'status' => 'no_state' }
      post "/jobs/#{job.user}/#{job.id}", data.to_json
      expect(last_response.status).to eql(500)
    end
  end

  describe 'Artifact detection' do
    before(:all) do
      FakeFS.activate!
      FakeFS::FileSystem.clear
    end

    before(:each) do
      FakeFS::FileSystem.clear
      artifactomat_path = File.expand_path(File.join(File.dirname(__FILE__), '..', '..', 'dist', 'etc', 'artifactomat'))
      FakeFS::FileSystem.clone(artifactomat_path)
    end

    after(:all) do
      FakeFS.deactivate!
    end

    describe 'GET /screenshots/:season_id' do
      before(:each) do
        # create season and screenshots
        @season = create(:test_season, screenshots_folder: 'screenshots')
        FileUtils.mkdir('/tmp')
        FileUtils.mkdir_p("/etc/artifactomat/recipes/#{@season.recipe_id}/screenshots")

        %w[ss1 ss2 ss3].each do |ss|
          file_path = File.join("/etc/artifactomat/recipes/#{@season.recipe_id}/screenshots/#{ss}.jpg")
          File.open(file_path, 'w') do |file|
            file.write(ss)
          end
        end
        @expected_hash = Digest::MD5.new.update('ss1').update('ss2').update('ss3').hexdigest

        @rec_man = double('RecipeManager')
        allow(RecipeManager).to receive(:new).and_return(@rec_man)
      end

      it 'succeeds' do
        get "/screenshots/#{@season.id}"
        expect(last_response.status).to eql(200)
      end

      it 'returns the image hash as header' do
        get "/screenshots/#{@season.id}"
        expect(last_response.headers['MD5']).to eq(@expected_hash)
      end

      it 'returns an archive containing the images' do
        get "/screenshots/#{@season.id}"
        uncompressed = Bzip2::FFI::Reader.read(StringIO.new(last_response.body))
        images = {}
        Minitar::Reader.each_entry(StringIO.new(uncompressed)) do |entry|
          images[entry.full_name] = entry.read
        end
        expect(images['ss1.jpg']).to eq('ss1')
        expect(images['ss2.jpg']).to eq('ss2')
        expect(images['ss3.jpg']).to eq('ss3')
      end

      it 'caches the archive in the temp directory' do
        get "/screenshots/#{@season.id}"
        images_hash = last_response.headers['MD5']
        archive_path = "/tmp/artifactomat-#{images_hash}.tar.bz2"
        expect(File.exist?(archive_path)).to be_truthy
      end

      it 'uses the cached archive, if available' do
        expect(Minitar).to receive(:pack)
        get "/screenshots/#{@season.id}"
        expect(Minitar).not_to receive(:pack)
        get "/screenshots/#{@season.id}"
      end

      it 'returns 404 on invalid season id' do
        get '/screenshots/invalid'
        expect(last_response.status).to eql(404)
      end

      it 'returns 404 if season has no screenshots' do
        @season.screenshots_folder = ''
        @season.save!
        get "/screenshots/#{@season.id}"
        expect(last_response.status).to eql(404)
      end
    end

    describe 'POST /timetables' do
      before(:each) do
        FileUtils.mkdir_p('/var/lib/artifactomat/timetables')
        FileUtils.mkdir_p('/tmp')
        @schedule_end = DateTime.now.utc
        @schedule_start = @schedule_end - 2.minutes
        @event1 = @schedule_start + 1.seconds
        @event2 = @schedule_start + 1.minute
        event1_timestamp = @event1.strftime('%y%m%d%H%M%S%L')
        event2_timestamp = @event2.strftime('%y%m%d%H%M%S%L')
        File.open('/tmp/timetable.csv.bz2', 'w') do |file|
          Bzip2::FFI::Writer.write(file, "#{event1_timestamp},0\n#{event2_timestamp},2")
        end
        @subject = build(:subject)
        @te = create(:test_episode,
                     subject_id: @subject.id,
                     schedule_start: @schedule_start,
                     schedule_end: @schedule_end)
      end

      after(:each) do
        DetectionEvent.destroy_all
      end

      it 'creates one DetectionEvent per timetable row' do
        allow(@subject_manager).to receive(:get_subjects_for_userid).and_return([@subject])
        allow(@vstore).to receive(:sids_by_ip).and_return([@subject.id])

        filename = 'timetable.csv.bz2'
        File.open('/tmp/timetable.csv.bz2') do |file|
          post '/timetables/swag', 'file' => Rack::Test::UploadedFile.new(file, original_filename: filename)
        end
        des = DetectionEvent.where(test_episode_id: @te.id)
        expect(des.size).to eq(2)
        # DateTime can't be rounded, but DB only stores second granularity -> 1 second offset allowed
        expect(des[0].timestamp).to be_within(1.seconds).of(@event1)
        expect(des[0].impossible?).to be true
        expect(des[1].timestamp).to be_within(1.seconds).of(@event2)
        expect(des[1].certain?).to be true
      end

      describe 'episode inference' do
        before(:each) do
          # Same user as @subject, but from a different groupfile
          @subject_alt = Subject.new('group2.csv/1', @subject.name, @subject.surname, @subject.userid, @subject.email)
          @subject2 = build(:subject)
          allow(@subject_manager).to receive(:get_subjects_for_userid).and_return([@subject, @subject_alt])
          allow(@vstore).to receive(:sids_by_ip).and_return([@subject.id, @subject_alt.id])

          # Episode running in parallel to @te but for a different subject
          create(:test_episode,
                 subject_id: @subject2.id,
                 schedule_start: @schedule_start,
                 schedule_end: @schedule_end)
          # Episode running before @te for the same subject
          create(:test_episode,
                 subject_id: @subject.id,
                 schedule_start: @schedule_start - 121.seconds,
                 schedule_end: @schedule_start - 1.seconds)
          # Episode running after @te for same user in different group
          create(:test_episode,
                 subject_id: @subject_alt.id,
                 schedule_start: @schedule_end + 1.seconds,
                 schedule_end: @schedule_end + 121.seconds)
        end

        it 'correctly infers the episode' do
          filename = 'timetable.csv.bz2'
          File.open('/tmp/timetable.csv.bz2') do |file|
            post '/timetables/swag', 'file' => Rack::Test::UploadedFile.new(file, original_filename: filename)
          end
          des = DetectionEvent.where(test_episode_id: @te.id)
          expect(des.size).to eq(2)
        end
      end
    end
  end
end
