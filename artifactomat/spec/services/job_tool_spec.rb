# frozen_string_literal: true

require 'artifactomat/services/job_tool'
require 'artifactomat'
require 'webmock/rspec'

describe 'JobBuilder' do
  before(:all) do
    ENV['ARTIFACTOMAT_CONF_CONF_APE_JOB_CLERK_PORT'] = 51_648.to_s
    ENV['ARTIFACTOMAT_CONF_CONF_APE_JOB_CLERK_ADDRESS'] = 'localhost'
    File.open('/tmp/itsapetestfile.txt', mode: 'w') do |file|
      file.write('a string in a textfile')
    end
  end

  after(:all) do
    File.delete('/tmp/itsapetestfile.txt')
  end

  describe 'exit code' do
    it 'fails if unknown command' do
      cmd_line = %w[doanythingwrong]
      expect do
        JobTool.run(cmd_line)
      end.to(
        output(/error: Unknown command/).to_stderr_from_any_process & output(/COMMANDS/).to_stdout_from_any_process
      )
    end

    it 'does not fail if action is \'build run\'' do
      cmd_line = %w[build run]
      expect do
        JobTool.run(cmd_line)
      end.to_not output(/error: Unknown command/).to_stderr_from_any_process
    end

    it 'does not fail if action is \'build copy\'' do
      cmd_line = %w[build copy]
      expect do
        JobTool.run(cmd_line)
      end.to_not output(/error: Unknown command/).to_stderr_from_any_process
    end

    it 'does not fail if action is \'build remove\'' do
      cmd_line = %w[build remove]
      expect do
        JobTool.run(cmd_line)
      end.to_not output(/error: Unknown command/).to_stderr_from_any_process
    end

    it 'does not fail if action is \'delete\'' do
      cmd_line = %w[delete]
      expect do
        JobTool.run(cmd_line)
      end.to_not output(/error: Unknown command/).to_stderr_from_any_process
    end
  end

  describe 'flags' do
    before(:each) do
    end

    after(:each) do
      WebMock.reset!
    end

    it 'accepts --host flag' do
      uri = URI::HTTP.build(host: 'remotehost', path: '/delete/all', port: 51_648)
      stub_request(:post, uri).to_return(status: 201)
      cmd_line = %w[--host=remotehost delete all]
      JobTool.run(cmd_line)
      expect(WebMock).to have_requested(:post, uri)
    end

    it 'accepts --port flag' do
      uri = URI::HTTP.build(host: 'localhost', path: '/delete/all', port: 1337)
      stub_request(:post, uri).to_return(status: 201)
      cmd_line = %w[--port=1337 delete all]
      JobTool.run(cmd_line)
      expect(WebMock).to have_requested(:post, uri)
    end

    it 'falls back to environment variables if flags are ommitted' do
      uri = URI::HTTP.build(host: 'localhost', path: '/delete/all', port: 51_648)
      stub_request(:post, uri).to_return(status: 201)
      cmd_line = %w[delete all]
      JobTool.run(cmd_line)
      expect(WebMock).to have_requested(:post, uri)
    end
  end

  context 'build commands' do
    it 'fails on unknown sub command' do
      cmd_line = %w[build trash]

      expect do
        JobTool.run(cmd_line)
      end.to(
        output("error: Too many arguments for command\n\n").to_stderr_from_any_process & output(/COMMANDS/).to_stdout_from_any_process
      )
    end
    describe 'build run <user> <file>' do
      let(:cmd_line) { %w[build run user42 C:\Users\Mister42] }
      before(:each) do
        WebMock.reset!
        @uri = URI::HTTP.build(host: 'localhost', path: '/build/execute', port: 51_648)
        @now = Time.now
        allow(Time).to receive(:now).and_return(@now)
        @request = stub_request(:post, @uri).to_return(status: 201)
      end

      it 'does not print to stderr with valid command' do
        expect do
          JobTool.run(cmd_line)
        end.to_not output.to_stderr_from_any_process
      end

      it 'sends a request with the correct parameters' do
        expected = {
          action: 'execute',
          user: 'user42',
          file_path: 'C:\\Users\\Mister42',
          deploy_path: '',
          expiration_date: nil,
          creation_date: Time.now
        }
        JobTool.run(cmd_line)
        expect(WebMock).to have_requested(:post, @uri).with(body: expected.to_yaml)
      end

      it 'ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END is set, expiration date is set' do
        ENV['ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END'] = @now.to_s
        expected = {
          action: 'execute',
          user: 'user42',
          file_path: 'C:\\Users\\Mister42',
          deploy_path: '',
          # Apparently a parsed time *sometimes* gets a +- 1 second delay from the actual time
          # so we just parse it exactly like the client does
          expiration_date: Time.parse(Time.now.to_s).round.utc,
          creation_date: Time.now
        }
        JobTool.run(cmd_line)
        expect(WebMock).to have_requested(:post, @uri).with(body: expected.to_yaml)
        ENV['ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END'] = nil
      end
    end

    describe 'build copy <user> <file> <place>' do
      before(:each) do
        WebMock.reset!
        @uri = URI::HTTP.build(host: 'localhost', path: '/build/copy', port: 51_648)
        @now = Time.now
        allow(Time).to receive(:now).and_return(@now)
        @request = stub_request(:post, @uri).to_return(status: 201)
      end

      it 'validates filepath and fails if not a file' do
        cmd_line = %w[build copy user42 /tmp/itsnotafile.txt C:\Users\Mister42]
        expect do
          JobTool.run(cmd_line)
        end.to output("error: /tmp/itsnotafile.txt is not a valid file\n").to_stderr_from_any_process
      end

      it 'does not print any error to stderr with valid command' do
        cmd_line = %w[build copy user42 /tmp/itsapetestfile.txt C:\Users\Mister42]
        expect do
          JobTool.run(cmd_line)
        end.to_not output.to_stderr_from_any_process
      end

      it 'sends a request with the correct parameters' do
        cmd_line = %w[build copy user42 /tmp/itsapetestfile.txt C:\Users\Mister42]
        expected = {
          action: 'copy',
          user: 'user42',
          file_path: '/tmp/itsapetestfile.txt',
          deploy_path: 'C:\\Users\\Mister42',
          expiration_date: nil,
          creation_date: Time.now
        }

        JobTool.run(cmd_line)

        expect(WebMock).to have_requested(:post, @uri).with(body: expected.to_yaml)
      end

      it 'ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END is set, expiration date is set' do
        cmd_line = %w[build copy user42 /tmp/itsapetestfile.txt C:\Users\Mister42]
        expected = {
          action: 'copy',
          user: 'user42',
          file_path: '/tmp/itsapetestfile.txt',
          deploy_path: 'C:\\Users\\Mister42',
          # Apparently a parsed time *sometimes* gets a +- 1 second delay from the actual time
          # so we just parse it exactly like the client does
          expiration_date: Time.parse(Time.now.to_s).round.utc,
          creation_date: Time.now
        }

        ENV['ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END'] = Time.now.to_s

        JobTool.run(cmd_line)
        expect(WebMock).to have_requested(:post, @uri).with(body: expected.to_yaml)

        ENV['ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END'] = nil
      end
    end

    describe 'build remove <user> <file>' do
      it 'fails if removing a file that was not pushed' do
        @uri = URI::HTTP.build(host: 'localhost', path: '/build/remove', port: 51_648)
        @now = Time.now
        allow(Time).to receive(:now).and_return(@now)
        @request = stub_request(:post, @uri).to_return(status: 403, body: 'There is no job to get the file!')
        cmd_line = %w[build remove user42 C:\Users\Mister42]

        expect do
          JobTool.run(cmd_line)
        end.to(output("error: There is no job to get the file!\n").to_stderr_from_any_process)
      end

      context 'works only if the file was copied before' do
        before(:each) do
          WebMock.reset!
          @uri = URI::HTTP.build(host: 'localhost', path: '/build/remove', port: 51_648)
          @now = Time.now
          allow(Time).to receive(:now).and_return(@now)
          @request = stub_request(:post, @uri).to_return(status: 201)
        end

        it 'does not print any error to stderr with valid command' do
          cmd_line = %w[build remove user42 C:\Users\Mister42]
          expect do
            JobTool.run(cmd_line)
          end.to_not output.to_stderr_from_any_process
        end

        it 'sends a request with the correct parameters' do
          cmd_line = %w[build remove user42 C:\Users\Mister42]
          expected = {
            action: 'remove',
            user: 'user42',
            file_path: '',
            deploy_path: 'C:\\Users\\Mister42',
            expiration_date: nil,
            creation_date: Time.now
          }

          JobTool.run(cmd_line)

          expect(WebMock).to have_requested(:post, @uri).with(body: expected.to_yaml)
        end

        it 'it works with %-signs' do
          cmd_line = %w[build remove user_42 %USER_PROFILE\Desktop]
          expected = {
            action: 'remove',
            user: 'user_42',
            file_path: '',
            deploy_path: '%USER_PROFILE\\Desktop',
            expiration_date: nil,
            creation_date: Time.now
          }

          expect do
            JobTool.run(cmd_line)
          end.to_not output.to_stderr_from_any_process
          expect(WebMock).to have_requested(:post, @uri).with(body: expected.to_yaml)
        end

        it 'regardless wehter ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END is set, remove jobs do not expire' do
          cmd_line = %w[build remove user42 C:\Users\Mister42]
          ENV['ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END'] = Time.now.to_s
          expected = {
            action: 'remove',
            user: 'user42',
            file_path: '',
            deploy_path: 'C:\\Users\\Mister42',
            expiration_date: nil,
            creation_date: Time.now
          }

          JobTool.run(cmd_line)

          expect(WebMock).to have_requested(:post, @uri).with(body: expected.to_yaml)

          ENV['ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END'] = nil
        end
      end
    end
  end

  context 'delete <waiting|all>' do
    describe 'arguments' do
      before(:each) do
        WebMock.reset!
        @now = Time.now
        allow(Time).to receive(:now).and_return(@now)
      end

      it 'does not print to stderr on "delete all"' do
        uri = URI::HTTP.build(host: 'localhost', path: '/delete/all', port: 51_648)
        stub_request(:post, uri).to_return(status: 200)

        cmd_line = %w[delete all]
        allow(JobTool).to receive(:puts)
        expect do
          JobTool.run(cmd_line)
        end.to_not output.to_stderr_from_any_process
      end

      it 'does not print to stderr on "delete waiting"' do
        uri = URI::HTTP.build(host: 'localhost', path: '/delete/waiting', port: 51_648)
        stub_request(:post, uri).to_return(status: 200)

        cmd_line = %w[delete waiting]
        allow(JobTool).to receive(:puts)
        expect do
          JobTool.run(cmd_line)
        end.to_not output.to_stderr_from_any_process
      end

      it 'fails on invalid argument' do
        cmd_line = %w[delete invalid]
        uri = URI::HTTP.build(host: 'localhost', path: '/delete/invalid', port: 51_648)
        stub_request(:post, uri).to_return(status: 404)
        expect do
          JobTool.run(cmd_line)
        end.to(
          output(/error: Invalid argument/).to_stderr_from_any_process
        )
      end

      it 'fails on more than one argument' do
        cmd_line = %w[delete waiting toomuch]
        expect do
          JobTool.run(cmd_line)
        end.to(
          output(/error: Wrong number of arguments/).to_stderr_from_any_process
        )
      end
    end

    describe 'behaviour' do
      context '<waiting>' do
        before(:each) do
          @cmd_line = %w[delete waiting]
          WebMock.reset!
          @uri = URI::HTTP.build(host: 'localhost', path: '/delete/waiting', port: 51_648)
          @request = stub_request(:post, @uri).to_return(status: 200, body: 'This is the response')
        end

        it 'sends a request to /delete/waiting' do
          allow(JobTool).to receive(:puts)
          JobTool.run(@cmd_line)
          expect(WebMock).to have_requested(:post, @uri)
        end

        it 'outputs the response' do
          expect do
            JobTool.run(@cmd_line)
          end.to(
            output("The following jobs were deleted:\nThis is the response\n").to_stdout_from_any_process
          )
        end
      end

      context '<all>' do
        before(:each) do
          @cmd_line = %w[delete all]
          WebMock.reset!
          @uri = URI::HTTP.build(host: 'localhost', path: '/delete/all', port: 51_648)
          @request = stub_request(:post, @uri).to_return(status: 200, body: 'This is the response')
        end

        it 'sends a request to /delete/all' do
          allow(JobTool).to receive(:puts)
          JobTool.run(@cmd_line)
          expect(WebMock).to have_requested(:post, @uri)
        end

        it 'outputs the response' do
          expect do
            JobTool.run(@cmd_line)
          end.to(
            output("The following jobs were deleted:\nThis is the response\n").to_stdout_from_any_process
          )
        end
      end
    end
  end
end
