# frozen_string_literal: true

require 'artifactomat/services/nameserver'
require 'artifactomat/models/subject'
require 'artifactomat/modules/logging'
require 'artifactomat/volatile_store'

describe Nameserver do
  before(:each) do
    @route = '1.1.1.1'
    @upstream = '8.8.8.8'
    @upstream_port = 53
    @server_ip = '127.0.0.1'
    @server_port = 5252
    @vstore = double('VolatileStore')
    allow(VolatileStore).to receive(:new).and_return(@vstore)
    @cm = double('ConfigurationManager')
    allow(@cm).to \
      receive(:get).with('conf.nameserver.upstream.address') { @upstream }
    allow(@cm).to \
      receive(:get).with('conf.nameserver.address') { @server_ip }
    allow(@cm).to \
      receive(:get).with('conf.nameserver.port') { @server_port }
    allow(@cm).to \
      receive(:get).with('conf.nameserver.upstream.port') { @upstream_port }
    allow(@cm).to \
      receive(:get).with('conf.network.external_ip') { @route }
    allow(@cm).to \
      receive(:get).with('conf.nameserver.custom_urls.*') { { 'conf.nameserver.custom_urls.proxy.its.apt' => '6.7.8.9' } }
  end

  describe '#new' do
    before(:each) do
      @nameserver = Nameserver.new(@cm)
    end

    it 'builds server correctly' do
      servers = @nameserver.instance_variable_get(:@endpoints)
      expect(servers[0]).to be_eql([:udp, @server_ip, @server_port])
      expect(servers[1]).to be_eql([:tcp, @server_ip, @server_port])
    end

    it 'has the correct upstream' do
      resolver = @nameserver.instance_variable_get(:@upstream)
      endpoints = resolver.instance_variable_get(:@endpoints)
      expect(endpoints[0]).to be_eql([:udp, @upstream, @upstream_port])
      expect(endpoints[1]).to be_eql([:tcp, @upstream, @upstream_port])
    end
  end

  describe '#process' do
    before(:each) do
      @nameserver = Nameserver.new(@cm)

      @subject = build(:subject)
      @client_ip = '127.0.0.2'

      @transaction = double('NS_Transaction')
      @transaction_options = double('NS_Transaction_Options')
      @remote_adress = double('NS_remote_adress')
      allow(@transaction).to \
        receive(:options).and_return(remote_address: @remote_adress)
    end

    context 'IPv6' do
      it 'passes AAAA trough' do
        expect(@transaction).to receive(:passthrough!)
        @nameserver.process('google.com', Resolv::DNS::Resource::IN::AAAA, @transaction)
      end
    end

    context 'resolves magic' do
      it 'its.apt -> 127.0.0.1 for subjects' do
        allow(@remote_adress).to \
          receive(:ip_address).and_return(@client_ip)
        allow(@vstore).to receive(:sids_by_ip).with(@client_ip).and_return([@subject.id])
        name = 'its.apt'
        expect(@transaction).to receive(:respond!).with('127.0.0.1')
        @nameserver.process(name, nil, @transaction)
      end

      it 'its.apt -> 127.0.0.2 for non-subjects' do
        non_client_ip = '1.2.3.4'
        allow(@remote_adress).to \
          receive(:ip_address).and_return(non_client_ip)
        allow(@vstore).to receive(:sids_by_ip).with(non_client_ip).and_return([])
        name = 'its.apt'
        expect(@transaction).to receive(:respond!).with('127.0.0.2')
        @nameserver.process(name, nil, @transaction)
      end
    end

    context 'static routes from context' do
      it 'resolves configured static urls for everybody' do
        allow(@remote_adress).to \
          receive(:ip_address).and_return(@client_ip)
        name = 'proxy.its.apt'
        expect(@transaction).to receive(:respond!).with('6.7.8.9')
        @nameserver.process(name, nil, @transaction)
      end
    end

    context 'dynamic routing' do
      before(:each) do
        @non_subject_ip = '1.2.3.4'
        @active_name = 'google.de'
        @inactive_name = 'yahoo.com'
        allow(@vstore).to receive(:get_registered_subjects).with(@active_name).and_return([@subject.id])
        allow(@vstore).to receive(:get_registered_subjects).with(@inactive_name).and_return([])
        allow(@vstore).to receive(:sids_by_ip).with(@client_ip).and_return([@subject.id])
        allow(@vstore).to receive(:sids_by_ip).with(@non_subject_ip).and_return([])
      end

      it 'reroutes active subject on requesting redirected domain' do
        allow(@remote_adress).to \
          receive(:ip_address).and_return(@client_ip)

        expect(@transaction).to receive(:respond!).with(@route)
        @nameserver.process(@active_name, nil, @transaction)
      end

      it 'does not reroute for non-subjects' do
        allow(@remote_adress).to \
          receive(:ip_address).and_return(@non_subject_ip)

        expect(@transaction).to receive(:passthrough!)
        @nameserver.process(@active_name, nil, @transaction)
      end

      it 'does not reroute non redirectet domain for subjects' do
        allow(@remote_adress).to \
          receive(:ip_address).and_return(@client_ip)

        expect(@transaction).to receive(:passthrough!)
        @nameserver.process(@inactive_name, nil, @transaction)
      end
    end

    describe 'error handling' do
      before(:each) do
        allow(@transaction).to receive(:passthrough!).and_raise(StandardError)
      end

      it 'prints a backtrace' do
        allow(@transaction).to receive(:fail!)

        expect(Logging).to receive(:error).with('APENames', /Processing Failed: StandardError, Backtrace:/)

        @nameserver.process('google.com', Resolv::DNS::Resource::IN::AAAA, @transaction)
      end

      it 'marks the transaction as failed' do
        allow(Logging).to receive(:error)

        expect(@transaction).to receive(:fail!)

        @nameserver.process('google.com', Resolv::DNS::Resource::IN::AAAA, @transaction)
      end
    end
  end
end
