# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require 'rspec'
require 'rack/test'
require 'yaml'

require 'artifactomat/services/job_clerk'
require 'artifactomat'

describe 'JobClerk' do
  include Rack::Test::Methods

  def app
    JobClerk.new
  end

  before(:all) do
    Job.delete_all
  end

  before(:each) do
    @fake_tcp_sock = double('TCPSocketMock')
    allow(TCPSocket).to receive(:new).and_return(@fake_tcp_sock)
    @now = Time.now
  end

  after(:each) do
    Job.delete_all
  end

  describe 'POST /build/execute' do
    it 'returns status 406 on mismatching actions' do
      job = {
        action: 'copy',
        user: 'user42',
        file_path: 'C:\\Users\\Mister42',
        deploy_path: '',
        expiration_date: nil,
        creation_date: @now
      }
      post('/build/execute', job.to_yaml)
      expect(last_response.status).to eq(406)
      expect(Job.count).to eq 0
    end

    it 'creates run jobs with the given parameters' do
      job = {
        action: 'execute',
        user: 'user42',
        file_path: 'C:\\Users\\Mister42',
        deploy_path: '',
        expiration_date: nil,
        creation_date: @now
      }
      post('/build/execute', job.to_yaml)
      expect(last_response.status).to eq(201)
      expect(Job.count).to eq 1
      created_job = Job.last
      expect(created_job.action).to eq('execute')
      expect(created_job.user).to eq('user42')
      expect(created_job.file_path).to eq('C:\\Users\\Mister42')
      expect(created_job.deploy_path).to eq('')
      expect(created_job.expiration_date).to be_nil
      expect(created_job.creation_date.round).to eq(@now.utc.round)

      job = {
        action: 'execute',
        user: 'user420',
        file_path: 'C:\\Users\\Mister420',
        deploy_path: '',
        expiration_date: @now + 3600,
        creation_date: @now
      }
      post('/build/execute', job.to_yaml)
      expect(last_response.status).to eq(201)
      created_job = Job.last
      expect(created_job.action).to eq('execute')
      expect(created_job.user).to eq('user420')
      expect(created_job.file_path).to eq('C:\\Users\\Mister420')
      expect(created_job.deploy_path).to eq('')
      expect(created_job.expiration_date.round).to eq((@now + 3600).utc.round)
      expect(created_job.creation_date.round).to eq(@now.utc.round)
    end
  end

  describe 'POST /build/copy' do
    it 'returns status 406 on mismatching action' do
      job = {
        action: 'execute',
        user: 'user42',
        file_path: '/tmp/itsapetestfile.txt',
        deploy_path: 'C:\\Users\\Mister42',
        expiration_date: nil,
        creation_date: @now
      }
      post('/build/copy', job.to_yaml)
      expect(last_response.status).to eq(406)
      expect(Job.count).to eq 0
    end

    it 'creates copy jobs with the given parameters' do
      job = {
        action: 'copy',
        user: 'user42',
        file_path: '/tmp/itsapetestfile.txt',
        deploy_path: 'C:\\Users\\Mister42',
        expiration_date: nil,
        creation_date: @now
      }
      post('/build/copy', job.to_yaml)
      expect(last_response.status).to eq(201)
      expect(Job.count).to eq 1
      created_job = Job.last
      expect(created_job.action).to eq('copy')
      expect(created_job.user).to eq('user42')
      expect(created_job.file_path).to eq('/tmp/itsapetestfile.txt')
      expect(created_job.deploy_path).to eq('C:\\Users\\Mister42')
      expect(created_job.expiration_date).to be_nil
      expect(created_job.creation_date.round).to eq(@now.utc.round)

      job = {
        action: 'copy',
        user: 'user420',
        file_path: '/tmp/itsapetestfile.txt',
        deploy_path: 'C:\\Users\\Mister420',
        expiration_date: @now + 3600,
        creation_date: @now
      }
      post('/build/copy', job.to_yaml)
      expect(last_response.status).to eq(201)
      expect(Job.count).to eq 2
      created_job = Job.last
      expect(created_job.action).to eq('copy')
      expect(created_job.user).to eq('user420')
      expect(created_job.deploy_path).to eq('C:\\Users\\Mister420')
      expect(created_job.file_path).to eq('/tmp/itsapetestfile.txt')
      expect(created_job.expiration_date.round).to eq((@now + 3600).utc.round)
      expect(created_job.creation_date.round).to eq(@now.utc.round)
    end
  end

  describe 'POST /build/remove' do
    it 'fails if removing a file that was not pushed' do
      job = {
        action: 'remove',
        user: 'user42',
        file_path: '',
        deploy_path: 'C:\\Users\\Mister420',
        expiration_date: nil,
        creation_date: @now
      }
      post('/build/remove', job.to_yaml)
      expect(last_response.status).to eq(403)
      expect(last_response.body).to eq("There is no job to get the file '#{job[:deploy_path]}' to user '#{job[:user]}'")
      expect(Job.count).to eq(0)
    end

    context 'works only if the file was copied before' do
      it 'creates remove jobs with the given parameters' do
        # create copy job
        job = {
          action: 'copy',
          user: 'user42',
          file_path: '/tmp/itsapetestfile.txt',
          deploy_path: 'C:\\Users\\Mister42',
          expiration_date: nil,
          creation_date: @now
        }
        post('/build/copy', job.to_yaml)
        expect(last_response.status).to eq(201)

        # create remove job
        job = {
          action: 'remove',
          user: 'user42',
          file_path: '',
          deploy_path: 'C:\\Users\\Mister42',
          expiration_date: nil,
          creation_date: @now
        }
        post('/build/remove', job.to_yaml)
        expect(last_response.status).to eq(201)
        expect(Job.count).to eq 2
        created_job = Job.last
        expect(created_job.action).to eq('remove')
        expect(created_job.user).to eq('user42')
        expect(created_job.file_path).to eq('')
        expect(created_job.deploy_path).to eq('C:\\Users\\Mister42')
        expect(created_job.expiration_date).to be_nil
        expect(created_job.creation_date.round).to eq(@now.utc.round)
      end

      it 'marks the copy job as completed' do
        # create copy job
        job = {
          action: 'copy',
          user: 'user42',
          file_path: '/tmp/itsapetestfile.txt',
          deploy_path: 'C:\\Users\\Mister42',
          expiration_date: nil,
          creation_date: @now
        }
        post('/build/copy', job.to_yaml)
        expect(last_response.status).to eq(201)
        copy_job = Job.last

        # create remove job
        job = {
          action: 'remove',
          user: 'user42',
          file_path: '',
          deploy_path: 'C:\\Users\\Mister42',
          expiration_date: nil,
          creation_date: @now
        }
        post('/build/remove', job.to_yaml)
        expect(last_response.status).to eq(201)

        copy_job.reload
        expect(copy_job.completed?).to be_truthy
      end
    end
  end

  describe 'POST /delete/all' do
    before(:each) do
      create(:job, status: 0)
      create(:job, status: 1)
      create(:job, status: 2)
      create(:job, status: 0)
      create(:job, status: 1)
      create(:job, status: 2)
    end

    after(:each) do
      Job.delete_all
    end

    it 'deletes all jobs' do
      expect(Job.none?).to be(false)

      post('/delete/all')

      expect(last_response.status).to eq(200)
      expect(Job.none?).to be(true)
    end

    it 'responds with all deleted jobs' do
      resp = post('/delete/all')
      expected_str = +''
      6.times do
        expected_str << "id: .*user: .*status: .*action: .*file_path: .*deploy_path: .*creation_date: .*collection_date: .*expiration_date: .*\n"
      end
      expected = Regexp.new expected_str

      expect(last_response.status).to eq(200)
      expect(resp.body).to match(expected)
    end
  end

  describe 'POST /delete/waiting' do
    before(:each) do
      create(:job, status: 0)
      create(:job, status: 1)
      create(:job, status: 2)
      create(:job, status: 0)
      create(:job, status: 1)
      create(:job, status: 2)
    end

    after(:each) do
      Job.delete_all
    end

    it 'deletes waiting jobs' do
      post('/delete/waiting')

      waiting_jobs = Job.where('status = 0')

      expect(last_response.status).to eq(200)
      expect(waiting_jobs.count.zero?).to be(true)
    end

    it 'does not delete non-pending jobs' do
      post('/delete/waiting')

      non_waiting_jobs = Job.where('status != 0')

      expect(last_response.status).to eq(200)
      expect(non_waiting_jobs.count).to eq(4)
    end

    it 'responds with all deleted jobs' do
      resp = post('/delete/waiting')
      expected_str = +''
      2.times do
        expected_str << "id: .*user: .*status: waiting.*action: .*file_path: .*deploy_path: .*creation_date: .*collection_date: .*expiration_date: .*\n"
      end
      expected = Regexp.new expected_str

      expect(last_response.status).to eq(200)
      expect(resp.body).to match(expected)
    end
  end
end
