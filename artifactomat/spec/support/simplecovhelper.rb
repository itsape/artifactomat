# frozen_string_literal: true

require 'simplecov'

# Helper class to make coverage reports mergable
module SimpleCovHelper
  def self.configure_job
    # This should only be run as part of an RSpec pipeline
    return unless defined?(RSpec)

    SimpleCov.configure do
      if ENV['GITLAB_CI'] # Both these ENV variables are made available by Gitlab Runner
        job_name = ENV['CI_JOB_NAME']
        coverage_dir "coverage/#{job_name}"
        command_name job_name
        SimpleCov.at_exit { SimpleCov.result } # Don't generate formatted report; only the JSON with the results.
      end
    end
  end

  def self.configure_profile
    SimpleCov.configure do
      load_profile 'test_frameworks'
      track_files '{bin,lib}/**/*.rb'

      add_filter '/vendor/*'
      add_filter 'spec/'
      add_filter '/gems/'
      add_filter 'db/seeds.rb'
      add_filter 'lib/artifactomat.rb'
      add_filter 'lib/artifactomat/version.rb'
      add_filter 'lib/artifactomat/modules/ape/controller.rb'

      use_merging true
      merge_timeout 5.days
    end
  end

  def self.merge_all_results!
    resultset_files = Pathname.glob(
      File.join(SimpleCov.coverage_path, '**', '.resultset.json')
    )

    SimpleCov::ResultMerger.merge_results(*resultset_files).format!
  end

  def self.start!
    return unless ENV['COVERAGE'] == 'true'

    configure_profile
    configure_job # Make sure to call this when you start coverage

    SimpleCov.start
  end
end
