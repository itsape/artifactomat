# frozen_string_literal: true

def silence_stdouterr
  $stdout = File.new('/dev/null', 'w')
  $stderr = File.new('/dev/null', 'w')

  yield
ensure
  $stdout = STDOUT
  @stderr = STDERR
end
