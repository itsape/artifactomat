# frozen_string_literal: true

def build_unsigned_cert(key, valid_from = Time.now, valid_until = Time.now + 365.days)
  subject = '/C=BE/O=Test/OU=Test/CN=Test'
  cert = create_raw_cert key, subject, valid_from, valid_until

  ef = OpenSSL::X509::ExtensionFactory.new
  ef.subject_certificate = cert
  ef.issuer_certificate = cert
  cert = cert_add_extension(cert, ef)

  cert.sign key, OpenSSL::Digest::SHA256.new
  cert
end

def create_raw_cert(key, subject, valid_from, valid_until)
  cert = OpenSSL::X509::Certificate.new
  cert.subject = cert.issuer = OpenSSL::X509::Name.parse(subject)
  cert.not_before = valid_from
  cert.not_after = valid_until
  cert.public_key = key.public_key
  cert.version = 2
  cert.serial = rand(9999)
  cert
end

def cert_add_extension(cert, extension_factory, is_ca = true)
  caflag = is_ca ? 'CA:TRUE' : 'CA:FALSE'
  cert.extensions = [
    extension_factory.create_extension('basicConstraints', caflag, true),
    extension_factory.create_extension('subjectKeyIdentifier', 'hash')
  ]
  cert.add_extension extension_factory.create_extension('authorityKeyIdentifier',
                                                        'keyid:always,issuer:always')
  cert
end

def create_request(key, common_name, org)
  req = OpenSSL::X509::Request.new
  req.version = 0
  req.subject = OpenSSL::X509::Name.parse("CN=#{common_name}/#{org}")
  req.public_key = key.public_key
  req.sign(key, OpenSSL::Digest::SHA256.new)
end

def build_signed_cert(req_key, ca_key, ca_cert, not_before = Time.now, not_after = Time.now + 360.days)
  req = create_request(req_key, 'artifactomat.de', 'O=UniBonn/OU=ITSEC/L=Bonn/ST=NRW/C=DE')
  cert = create_raw_cert req, req.subject.to_s, not_before, not_after
  cert.issuer = ca_cert.subject

  ef = OpenSSL::X509::ExtensionFactory.new
  ef.subject_certificate = cert
  ef.issuer_certificate = ca_cert
  cert = cert_add_extension(cert, ef, false)
  cert.sign(ca_key, OpenSSL::Digest::SHA256.new)
end

def load_valid_cert
  cert_cont = get_cert_content('public/client.itsape.pem')
  OpenSSL::X509::Certificate.new(cert_cont)
end

def load_valid_key
  key_cont = get_cert_content('private/client.itsape.key')
  OpenSSL::PKey::RSA.new(key_cont)
end

def load_valid_ca_key
  ca_key_cont = get_cert_content('private/ca.key')
  OpenSSL::PKey::RSA.new(ca_key_cont)
end

def load_valid_ca
  ca_cont = get_cert_content('public/ca.pem')
  OpenSSL::X509::Certificate.new(ca_cont)
end

def build_ssl_sock(key, cert, ip, port)
  ctx = OpenSSL::SSL::SSLContext.new

  ctx.cert = cert
  ctx.key = key
  ctx.verify_mode = OpenSSL::SSL::VERIFY_NONE

  tcp_socket = TCPSocket.new(ip, port)
  OpenSSL::SSL::SSLSocket.new(tcp_socket, ctx)
end

def get_cert_content(path)
  abs_path = File.join(File.dirname(__FILE__), '../../dist/etc/artifactomat/certs/', path)
  File.open(abs_path)
end
