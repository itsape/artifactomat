# frozen_string_literal: true

require 'artifactomat/utils/bullet_loader'

def without_bullet
  raise 'No block given' unless block_given?

  begin
    enabled = Bullet.enable?
    Bullet.enable = false
    ret = yield
  ensure
    Bullet.enable = enabled
  end

  ret
end
