# frozen_string_literal: true

# Class to mock a thread
class ThreadMock
  attr_accessor :status
  def initialize(&block)
    @block = block
    @status = 'sleep'
  end

  attr_reader :block

  def join
    @status = false
  end
end
