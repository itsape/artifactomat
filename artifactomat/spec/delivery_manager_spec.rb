# frozen_string_literal: true

require 'artifactomat/modules/delivery_manager'
require 'artifactomat/models/job'

require 'tempfile'

require_relative 'mixins/script_executor_spec'

describe DeliveryManager do
  before(:all) do
    Logging.initialize
  end

  after(:each) do
    Job.destroy_all
  end

  # rubocop:disable Metrics/AbcSize
  def reset_database_env
    SubjectHistory.delete_all
    TestEpisode.delete_all
    TestSeason.delete_all
    @tempfile = ''
    @artifact = ''
    @arm_script = ''
    @disarm_script = ''
    @recipe = build(:recipe,
                    arm_script: @arm_script,
                    disarm_script: @disarm_script)
    @recipe.infrastructure = []
    @subject = build(:subject)
    @episodes = []
    @season = create(:test_season,
                     recipe_id: @recipe.id,
                     episode_count: 0)
    3.times do
      @episodes << create(:test_episode,
                          subject_id: @subject.id,
                          test_season: @season)
    end

    @history = SubjectHistory.create(
      subject_id: @subject.id,
      ip: '127.0.0.1',
      session_begin: Time.new(2016, 6, 6, 14, 0, 0),
      submit_time: Time.new(2016, 6, 6, 14, 1, 30)
    )
  end
  # rubocop:enable Metrics/AbcSize

  before(:each) do
    reset_database_env
    @tempfile = Tempfile.new('artifactomat.deliver')
    @artifact = 'DeliveryArtifact ' + @tempfile.path
    @arm_script = 'echo "$ARTIFACTOMAT_ARTIFACT armed" > ' + @tempfile.path
    @disarm_script = 'echo "$ARTIFACTOMAT_ARTIFACT disarmed" > ' + @tempfile.path
    @recipe.arm_script = @arm_script
    @recipe.disarm_script = @disarm_script
    @configuration_manager = double('ConfigurationManager')
    @recipe_manager = double('RecipeManager')
    @subject_manager = double('SubjectManager')
    @recipe.infrastructure = [build(:infrastructure_element)]

    allow(Logging).to receive(:warning)
    allow(@configuration_manager)
      .to receive(:get).with('conf.ape.tmp_dir') { '/tmp' }
    allow(@configuration_manager)
      .to receive(:cfg_path) { '/tmp' }
    allow(@recipe_manager)
      .to receive(:get_recipe).with(@recipe.id) { @recipe }
    allow(@configuration_manager)
      .to receive(:config_to_env) { { 'ARTIFACTOMAT_ARTIFACT' => @artifact } }
    allow(@subject_manager)
      .to receive(:get_subject).with(@subject.id) { @subject }

    TestSeason.instance_variable_set(:@recipe_manager, @recipe_manager)
    TestSeason.instance_variable_set(:@configuration_manager, @configuration_manager)

    @fake_router = double('routing_delmgr_spec')
    allow(@fake_router).to receive(:get_internal_ports).and_return([2, 3])
    allow(@fake_router).to receive(:get_external_address).and_return('1.2.3.4')
    allow(RoutingClient).to receive(:new).and_return(@fake_router)

    @delivery_manager = DeliveryManager.new(@configuration_manager,
                                            @recipe_manager,
                                            @subject_manager)
  end

  def expect_armed_episode_count_to_eq(num_episodes)
    c = TestEpisode.where(armed: true).count
    expect(c).to eq(num_episodes)
  end

  it_behaves_like 'a script executor' do
    let(:executor) do
      @delivery_manager
    end
  end

  describe '#arm' do
    it 'works and gives propper logs' do
      expect(@fake_router).to receive(:activate).with(@episodes[0])
      expect(Logging).to receive(:info).once do |sig, msg|
        expect(sig).to match('DeliveryManager')
        expect(msg).to match(@subject.id)
        expect(msg).to match(@episodes[0].id.to_s)
        expect(msg).to match(/episode/i)
        expect(msg).to match(/armed/i)
      end
      expect(Logging).to receive(:debug).twice.with('DeliveryManager', any_args)
      expect(@delivery_manager).to receive(:execute_script).with(@recipe.arm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, ''])
      @delivery_manager.arm(@episodes[0])
    end

    it 'creates start detection job' do
      now = Time.now.round
      allow(@fake_router).to receive(:activate)
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)
      allow(Time).to receive(:now).and_return(now)

      @delivery_manager.arm(@episodes[0])
      last_job = Job.last

      expect(last_job.user).to eq(@subject.userid)
      expect(last_job.action).to eq('start_detection')
      expect(last_job.file_path).to eq('')
      expect(last_job.deploy_path).to eq('')
      expect(last_job.creation_date).to eq(now)
      expect(last_job.expiration_date).to eq(@episodes[0].schedule_end)
      expect(last_job.watch_arguments).to eq(@episodes[0].test_season.json_detection_parameters)
    end

    it 'does not create start detection job if @detection is not defined' do
      @recipe.remove_instance_variable(:@detection)
      allow(@fake_router).to receive(:activate)
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)

      @delivery_manager.arm(@episodes[0])
      last_job = Job.last

      expect(last_job).to be_nil
    end

    it 'fails: if episodes gets armed twice' do
      expect(@fake_router).to receive(:activate).with(@episodes[0])
      expect(Logging).to receive(:info).once.with('DeliveryManager', any_args)
      expect(Logging).to receive(:debug).twice.with('DeliveryManager', any_args)
      expect(@delivery_manager).to receive(:execute_script).with(@recipe.arm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, ''])
      @delivery_manager.arm(@episodes[0])
      expect do
        @delivery_manager.arm(@episodes[0])
      end.to raise_error(DeliveryError)
    end

    it 'fails: does not run arm script if the routing fails' do
      expect(@fake_router).to receive(:activate).with(@episodes[0]).and_raise(RoutingRunningError)
      expect(Logging).to receive(:debug).once.with('DeliveryManager', any_args)
      expect(@delivery_manager).not_to receive(:execute_script)
      expect do
        @delivery_manager.arm(@episodes[0])
      end.to raise_error(DeliveryError)
    end
  end

  describe '#status' do
    it 'gives the number of armed episodes and the subjects currently tested' do
      expect(Logging).to receive(:info).twice.with('DeliveryManager', any_args)
      expect(Logging).to receive(:debug).at_least(:once) # we do not care here, speced elsewhere

      expect(@delivery_manager.status['Armed Episodes']).to be 0
      expect(@delivery_manager.status['Armed Subjects']).to be_empty

      # arm
      expect(@fake_router).to receive(:activate).with(@episodes[0])
      expect(@delivery_manager).to receive(:execute_script).with(@recipe.arm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, ''])
      @delivery_manager.arm(@episodes[0])

      expect(@delivery_manager.status['Armed Episodes']).to be 1
      expect(@delivery_manager.status['Armed Subjects']).to contain_exactly @episodes[0].subject_id

      # then disarm
      expect(@fake_router).to receive(:deactivate).with(@episodes[0])
      expect(@delivery_manager).to receive(:execute_script).with(@recipe.disarm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, ''])
      @delivery_manager.disarm(@episodes[0])

      expect(@delivery_manager.status['Armed Episodes']).to be 0
      expect(@delivery_manager.status['Armed Subjects']).to be_empty
    end
  end

  describe '#lock_season' do
    it 'does not arm episode on locked season' do
      expect(Logging).to receive(:info).once.with('DeliveryManager', any_args)
      @delivery_manager.lock_season(@season)
      expect(@fake_router).not_to receive(:activate).with(@episodes[0])
      expect(@delivery_manager).not_to receive(:execute_arm)
      expect(Logging).to receive(:warning).once.with('DeliveryManager', any_args)
      @delivery_manager.arm(@episodes[0])
    end

    it 'disarms running episodes when season gets locked' do
      # arm
      expect(@fake_router).to receive(:activate).with(@episodes[0])
      expect(Logging).to receive(:info).once.with('DeliveryManager', any_args)
      expect(Logging).to receive(:debug).twice.with('DeliveryManager', any_args)
      expect(@delivery_manager).to receive(:execute_script).and_return([0, ''])
      @delivery_manager.arm(@episodes[0])
      @episodes.each(&:reload)
      # lock
      expect(@fake_router).to receive(:deactivate).once
      expect(@delivery_manager).to receive(:execute_script).and_return([0, ''])
      expect(Logging).to receive(:info).twice.with('DeliveryManager', any_args)
      expect(Logging).to receive(:debug).twice.with('DeliveryManager', any_args)
      @delivery_manager.lock_season(@season)
      @episodes.each(&:reload)
      # should be disarmed
      expect do
        @delivery_manager.disarm(@episodes[0])
      end.to raise_error(DeliveryError)
    end

    it 'marks all incomplete episodes as failed' do
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:warning)
      allow(@fake_router).to receive(:deactivate)
      allow(@delivery_manager).to receive(:execute_script).and_return([0, ''])

      @delivery_manager.lock_season(@season)
      @episodes.each do |ep|
        ep.reload
        expect(ep.failed?).to be_truthy
      end
    end

    it 'does not mark completed episodes as failed' do
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:warning)
      allow(@fake_router).to receive(:deactivate)
      allow(@delivery_manager).to receive(:execute_script).and_return([0, ''])

      @episodes[0].completed!
      @delivery_manager.lock_season(@season)
      expect(@episodes[0].completed?).to be_truthy
      @episodes.delete(@episodes[0])
      @episodes.each do |ep|
        ep.reload
        expect(ep.failed?).to be_truthy
      end
    end
  end

  describe '#unlock_season' do
    it 'does allow arming only if season not locked' do
      # lock
      expect(Logging).to receive(:info).once.with('DeliveryManager', any_args)
      @delivery_manager.lock_season(@season)

      # disarmed but should not arm
      expect(Logging).to receive(:warning).once.with('DeliveryManager', any_args)
      @delivery_manager.arm(@episodes[0])

      # unlock
      expect_armed_episode_count_to_eq 0
      expect(Logging).to receive(:info).once.with('DeliveryManager', any_args)
      @delivery_manager.unlock_season(@season)

      # arm works
      expect(@fake_router).to receive(:activate).with(@episodes[0])
      expect(Logging).to receive(:info).once.with('DeliveryManager', any_args)
      expect(Logging).to receive(:debug).twice.with('DeliveryManager', any_args)
      expect(@delivery_manager).to receive(:execute_script).with(@recipe.arm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, ''])
      @delivery_manager.arm(@episodes[0])
    end
  end

  describe '#disarm' do
    it 'works' do
      # first arm
      expect(Logging).to receive(:info).twice.with('DeliveryManager', any_args)
      expect(Logging).to receive(:debug).at_least(:once)

      expect(@fake_router).to receive(:activate).with(@episodes[0])
      expect(@delivery_manager).to receive(:execute_script).with(@recipe.arm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, '', ''])
      @delivery_manager.arm(@episodes[0])

      # then disarm
      expect(@fake_router).to receive(:deactivate).with(@episodes[0])
      expect(@delivery_manager).to receive(:execute_script).with(@recipe.disarm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, ''])
      @delivery_manager.disarm(@episodes[0])

      expect_armed_episode_count_to_eq 0
    end

    it 'creates stop detection job, if detectiom is defined' do
      now = Time.now.round
      allow(@fake_router).to receive(:activate)
      allow(@fake_router).to receive(:deactivate)
      allow(@delivery_manager).to receive(:execute_script).with(@recipe.arm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, '', ''])
      allow(@delivery_manager).to receive(:execute_script).with(@recipe.disarm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, ''])
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)
      allow(Time).to receive(:now).and_return(now)

      @delivery_manager.arm(@episodes[0])
      start_detection_job = Job.last
      start_detection_job.completed!
      @delivery_manager.disarm(@episodes[0])
      last_job = Job.last

      expect(last_job.user).to eq(@subject.userid)
      expect(last_job.action).to eq('stop_detection')
      expect(last_job.file_path).to eq('')
      expect(last_job.deploy_path).to eq('')
      expect(last_job.creation_date).to eq(now)
      expect(last_job.expiration_date).to eq(nil)
      expect(last_job.watch_arguments).to eq(@episodes[0].test_season.json_detection_parameters)
    end

    it 'does not create stop detection job, if detection is not defined' do
      @recipe.remove_instance_variable(:@detection)
      allow(@fake_router).to receive(:activate)
      allow(@fake_router).to receive(:deactivate)
      allow(@delivery_manager).to receive(:execute_script).with(@recipe.arm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, '', ''])
      allow(@delivery_manager).to receive(:execute_script).with(@recipe.disarm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, ''])
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)

      @delivery_manager.arm(@episodes[0])
      @delivery_manager.disarm(@episodes[0])
      last_job = Job.last

      expect(last_job).to be_nil
    end

    it 'does not create stop detection job, if no matching start detection job exists' do
      allow(@fake_router).to receive(:activate)
      allow(@fake_router).to receive(:deactivate)
      allow(@delivery_manager).to receive(:execute_script).with(@recipe.arm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, '', ''])
      allow(@delivery_manager).to receive(:execute_script).with(@recipe.disarm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, ''])
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)
      expect(Logging).to receive(:warning).with('DeliveryManager', /Tried to create stop detection job for episode #{@episodes[0]}/)

      @delivery_manager.arm(@episodes[0])
      Job.destroy_all
      @delivery_manager.disarm(@episodes[0])
      last_job = Job.last

      expect(last_job).to be_nil
    end

    it 'fails: on unarmed episode' do
      expect(Logging).not_to receive(:info)
      expect(Logging).not_to receive(:debug)
      expect(@fake_router).not_to receive(:deactivate)
      expect(@delivery_manager).not_to receive(:execute_script)
      expect do
        @delivery_manager.disarm(@episodes[1])
      end.to raise_error(DeliveryError)
    end

    it 'logs an ERROR if routing fails' do
      # first arm
      expect(Logging).to receive(:info).once.with('DeliveryManager', any_args)
      expect(Logging).to receive(:debug).at_least(:once)

      expect(@fake_router).to receive(:activate).with(@episodes[0])
      expect(@delivery_manager).to receive(:execute_script).with(@recipe.arm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, '', ''])
      @delivery_manager.arm(@episodes[0])

      # then disarm
      expect(@delivery_manager).to receive(:execute_script).and_return([0, ''])
      expect(@fake_router).to receive(:deactivate).and_raise(RoutingRunningError)
      expect(Logging).to receive(:error) do |sig, msg|
        expect(sig).to match('DeliveryManager')
        expect(msg).to include(@episodes[0].to_str)
      end
      @delivery_manager.disarm(@episodes[0])
      expect_armed_episode_count_to_eq 0
    end
  end

  describe '#disarm_all' do
    it 'disarms all armed test episodes' do
      @episodes.each do |episode|
        episode.armed = true
        episode.save!
      end

      expect(Logging).to receive(:info).exactly(3).times
      expect(Logging).to receive(:debug).at_least(6).times
      allow(Logging).to receive(:warning)
      expect(@fake_router).to receive(:deactivate).exactly(3).times
      expect(@delivery_manager).to receive(:execute_script).with(@recipe.disarm_script, hash_including(DeliveryManager::SUBJECT_ENV_PREFIX + 'IP' => '127.0.0.1')).and_return([0, '']).exactly(3).times

      @delivery_manager.disarm_all
      expect_armed_episode_count_to_eq 0
    end
  end
end
