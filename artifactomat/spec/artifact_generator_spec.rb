# frozen_string_literal: true

require 'artifactomat/modules/artifact_generator'

require 'digest'
require 'fakefs/spec_helpers'

require_relative 'mixins/script_executor_spec'

TMP_DIR = '/tmp/artifactomat'
RECIPE_TEMP_FOLDER = 'artifactomat_recipe_temp_dir'

describe ArtifactGenerator do
  include FakeFS::SpecHelpers

  before(:all) do
    Logging.initialize
    FakeFS.activate!
  end

  after(:all) do
    FakeFS.deactivate!
  end

  before(:each) do
    cm = double('ConfigurationManager')
    allow(ConfigurationManager).to receive(:new).and_return(cm)
    @series = create(:test_series, season_count: 2)
    # the generated seasons have 3 episodes each... no way to change that
    # =>
    @season_count = 2
    @episode_count = 6
    @recipe = build(:recipe)

    @subject = build(:subject)
    @subject.id = 'somemagic'
    @subject.add_field('custom')
    @subject.send('custom='.to_sym, 'myvar')

    @deployment_manager = double('DeploymentManager')
    allow(@deployment_manager).to receive(:deploy_artifact).and_return(true)
    allow(@deployment_manager).to receive(:stop_deployment).and_return(true)

    @configuration_manager = double('CfgMgr-ArtifactGenerator')
    allow(@configuration_manager).to receive(:get).with('conf.ape.tmp_dir').and_return(TMP_DIR)
    allow(@configuration_manager).to receive(:cfg_path).and_return(TMP_DIR)
    allow(@configuration_manager).to receive(:config_to_env).and_return(
      'ARTIFACTOMAT_CONF_CONF_APE_TMP_DIR' => TMP_DIR
    )
    @recipe_manager = double('RecipeManager-ArtifactGenerator')
    allow(@recipe_manager).to receive(:get_recipe).and_return(@recipe)
    @subject_manager = double('SubjectManager')
    allow(@subject_manager).to receive(:get_subject).and_return(@subject)

    @scheduler = double('Scheduler')
    allow(Scheduler).to receive(:new).and_return(@scheduler)

    @an_ape = double('APEdouble')
    @fake_router = double('routing_ag_spec')
    allow(@fake_router).to receive(:get_internal_ports).and_return([2, 3])
    allow(@fake_router).to receive(:get_external_address).and_return('1.2.3.4')
    allow(@an_ape).to receive(:routing).and_return(@fake_router)
    allow(Ape).to receive(:instance).and_return(@an_ape)

    @artifact_generator = ArtifactGenerator.new(@deployment_manager,
                                                @configuration_manager,
                                                @recipe_manager,
                                                @subject_manager)
  end

  after(:each) do
    @series.destroy
  end

  it_behaves_like 'a script executor' do
    let(:executor) do
      @artifact_generator
    end
  end

  describe '#new' do
    before(:each) do
      @ts = create(:test_series, end_date: Time.now + 100)
      @ts.reload # timestamps are rounded in the DB, reload so this object gets the rounded stamp
    end

    after(:each) do
      @ts.destroy!
    end

    it 'creates destroy jobs for "prepared" series' do
      @ts.prepared!
      expect(@scheduler).to receive(:at).with(@ts.end_date, tags: ['destroy artifacts', @ts.to_str])

      ArtifactGenerator.new(@deployment_manager,
                            @configuration_manager,
                            @recipe_manager,
                            @subject_manager)
    end

    it 'creates destroy jobs for "approved" series' do
      @ts.approved!
      expect(@scheduler).to receive(:at).with(@ts.end_date, tags: ['destroy artifacts', @ts.to_str])

      ArtifactGenerator.new(@deployment_manager,
                            @configuration_manager,
                            @recipe_manager,
                            @subject_manager)
    end

    it 'does not create destroy jobs for series that are not prepared nor approved' do
      expect(@scheduler).not_to receive(:at)

      @ts.fresh!
      ArtifactGenerator.new(@deployment_manager,
                            @configuration_manager,
                            @recipe_manager,
                            @subject_manager)

      @ts.scheduled!
      ArtifactGenerator.new(@deployment_manager,
                            @configuration_manager,
                            @recipe_manager,
                            @subject_manager)

      @ts.completed!
      ArtifactGenerator.new(@deployment_manager,
                            @configuration_manager,
                            @recipe_manager,
                            @subject_manager)
    end
  end

  describe '#destroy_artifacts' do
    def get_artifact_paths(series)
      artifact_paths = []
      series.test_seasons.each do |season|
        artifact_paths.push File.join(TMP_DIR, season.id.to_s)
      end
      artifact_paths
    end

    it 'deletes the artifact folder if there' do
      artifact_paths = get_artifact_paths @series
      allow(File).to receive(:directory?).and_return(true)
      expect(FileUtils).to receive(:rm_rf).with(artifact_paths[0]).ordered
      expect(FileUtils).to receive(:rm_rf).with(artifact_paths[1]).ordered
      @artifact_generator.destroy_artifacts(@series)
    end

    it 'does not delete the artifact folder if its not there' do
      artifact_paths = get_artifact_paths @series
      allow(File).to receive(:directory?).and_return(false)
      expect(FileUtils).not_to receive(:rm_rf).with(artifact_paths[0])
      expect(FileUtils).not_to receive(:rm_rf).with(artifact_paths[1])
      @artifact_generator.destroy_artifacts(@series)
    end
  end

  describe '#check_season' do
    before(:each) do
      @season = create(:test_season, episode_count: 0, recipe_id: @recipe.id)
    end

    after(:each) do
      @season.delete
    end

    it 'has DEP_CHECK in ENV' do
      expect(Logging).to receive(:debug).twice.with('ArtifactGenerator', any_args)
      @recipe.artifact_script = 'echo $DEP_CHECK'
      status, output = @artifact_generator.check_season(@season)
      expect(status).to be_truthy
      expect(output).to match("true\n")
    end

    it 'understands status code' do
      expect(Logging).to receive(:debug).twice.with('ArtifactGenerator', any_args)
      @recipe.artifact_script = 'exit 1'
      expect(Logging).to receive(:error).once
      status, output = @artifact_generator.check_season(@season)
      expect(status).to be == 1
      expect(output).to be_empty
    end

    it 'returns error with exit code and error mesage' do
      expect(Logging).to receive(:debug).twice.with('ArtifactGenerator', any_args)
      @recipe.artifact_script = "#!/bin/bash\ndksflöjgöklsdfjgslödkf\n"
      expect(Logging).to receive(:error).once
      status, output = @artifact_generator.check_season(@season)
      expect(status).to be == 127
      expect(output).not_to be_empty
    end
  end

  describe '#generate' do
    before(:each) do
      @fake_thr = double('fake_thr')
      allow(@fake_thr).to receive(:value).and_return(0)
      @fake_io = double('fake_io')
      allow(@fake_io).to receive(:read).and_return('')

      @routing = double('RoutingDouble_AG')
      allow(@routing).to receive(:get_internal_ports).and_return([5, 6])
      allow(@routing).to receive(:get_external_address).and_return('127.0.0.1')

      @an_ape = double('APEdouble_AG')
      allow(Ape).to receive(:instance).and_return(@an_ape)
      allow(@an_ape).to receive(:routing).and_return(@routing)
      allow(@scheduler).to receive(:at)
    end

    it 'gets status 0' do
      @recipe.artifact_script = 'exit 0'
      expect(Logging).to receive(:debug).exactly(@episode_count).times
      expect(Logging).to receive(:info).exactly(@season_count + 1).times
      expect(@an_ape).to receive(:inform_user).once
      status = @artifact_generator.generate(@series)
      expect(status).to be(0)
    end

    it 'fails with exception on status != 0' do
      @recipe.artifact_script = "#!/bin/bash \ndksflöjg"
      expect(Logging).to receive(:debug).exactly(@episode_count / @season_count).times
      expect(Logging).to receive(:error).twice
      expect(@deployment_manager).not_to receive(:deploy_artifact)
      expect do
        @artifact_generator.generate(@series)
      end.to raise_error(ArtifactGeneratorError)
    end

    it 'generates a recipe specific id in ENV' do
      md5 = Digest::MD5.new
      md5 << @recipe.id
      @recipe.artifact_script = "#!/bin/bash \necho -n $ARTIFACTOMAT_RECIPE_TOKEN\n"
      expect(Logging).to receive(:debug).exactly(@episode_count).times
      expect(Logging).to receive(:info).exactly(@season_count + 1).times
      expect(@an_ape).to receive(:inform_user).once

      expect(Open3).to receive(:popen2e) do |env, script|
        expect(script).to match("#{@recipe.artifact_script}\n")
        expect(env['ARTIFACTOMAT_RECIPE_TOKEN']).to match(md5.hexdigest.to_s)
        [0, @fake_io, @fake_thr]
      end.exactly(@episode_count).times
      status = @artifact_generator.generate(@series)
      expect(status).to be(0)
    end

    it 'injects config to ENV' do
      @recipe.artifact_script = 'echo $ARTIFACTOMAT_CONF_CONF_APE_TMP_DIR'
      expect(Logging).to receive(:debug).exactly(@episode_count).times
      expect(Logging).to receive(:info).exactly(@season_count + 1).times
      expect(@an_ape).to receive(:inform_user).once

      expect(Open3).to receive(:popen2e) do |env, script|
        expect(script).to match("#{@recipe.artifact_script}\n")
        expect(env['ARTIFACTOMAT_CONF_CONF_APE_TMP_DIR']).to match('/tmp/artifactomat')
        [0, @fake_io, @fake_thr]
      end.exactly(@episode_count).times
      status = @artifact_generator.generate(@series)
      expect(status).to be(0)
    end

    it 'generates a subject specific id in ENV' do
      md5 = Digest::MD5.new
      md5 << @subject.id
      @recipe.artifact_script = 'echo -n $ARTIFACTOMAT_SUBJECT_TOKEN'
      expect(Logging).to receive(:debug).exactly(@episode_count).times
      expect(Logging).to receive(:info).exactly(@season_count + 1).times
      expect(@an_ape).to receive(:inform_user).once

      expect(Open3).to receive(:popen2e) do |env, script|
        expect(script).to match("#{@recipe.artifact_script}\n")
        expect(env['ARTIFACTOMAT_SUBJECT_TOKEN']).to match(md5.hexdigest.to_s)
        [0, @fake_io, @fake_thr]
      end.exactly(@episode_count).times
      status = @artifact_generator.generate(@series)
      expect(status).to be(0)
    end

    it 'injects epsiode parameters to ENV' do
      @recipe.artifact_script = 'echo -n $ARTIFACTOMAT_RECIPE_TESTEPISODE_ID'
      expect(Logging).to receive(:debug).exactly(@episode_count).times
      expect(Logging).to receive(:info).exactly(@season_count + 1).times
      expect(@an_ape).to receive(:inform_user).once

      expect(Open3).to receive(:popen2e) do |env, script|
        expect(script).to match("#{@recipe.artifact_script}\n")
        expect(TestEpisode.ids).to include env['ARTIFACTOMAT_RECIPE_TESTEPISODE_ID'].to_i
        expect(env['ARTIFACTOMAT_RECIPE_TESTEPISODE_ID'].to_i).to be > 1
        [0, @fake_io, @fake_thr]
      end.exactly(@episode_count).times
      status = @artifact_generator.generate(@series)
      expect(status).to be(0)
    end

    it 'holds subject infos in env' do
      @recipe.artifact_script = "
      echo $ARTIFACTOMAT_SUBJECT_ID
      echo $ARTIFACTOMAT_SUBJECT_NAME
      echo $ARTIFACTOMAT_SUBJECT_SURNAME
      echo $ARTIFACTOMAT_SUBJECT_USERID
      echo $ARTIFACTOMAT_SUBJECT_EMAIL
      echo $ARTIFACTOMAT_SUBJECT_CUSTOM\n"
      expect(Logging).to receive(:debug).exactly(@episode_count).times
      expect(Logging).to receive(:info).exactly(@season_count + 1).times
      expect(@an_ape).to receive(:inform_user).once

      expect(Open3).to receive(:popen2e) do |env, script|
        expect(script).to match("#{@recipe.artifact_script}\n")
        expect(env['ARTIFACTOMAT_SUBJECT_ID']).to match(@subject.id.to_s)
        expect(env['ARTIFACTOMAT_SUBJECT_NAME']).to match(@subject.name.to_s)
        expect(env['ARTIFACTOMAT_SUBJECT_SURNAME']).to match(@subject.surname.to_s)
        expect(env['ARTIFACTOMAT_SUBJECT_USERID']).to match(@subject.userid.to_s)
        expect(env['ARTIFACTOMAT_SUBJECT_EMAIL']).to match(@subject.email.to_s)
        expect(env['ARTIFACTOMAT_SUBJECT_CUSTOM']).to match(@subject.custom.to_s)
        [0, @fake_io, @fake_thr]
      end.exactly(@episode_count).times
      status = @artifact_generator.generate(@series)
      expect(status).to be(0)
    end

    it 'creates a tmp folder' do
      @recipe.artifact_script = "\ntrue"
      expect(Logging).to receive(:debug).exactly(@episode_count).times
      expect(Logging).to receive(:info).exactly(@season_count + 1).times
      expect(@an_ape).to receive(:inform_user).once

      expect(Open3).to receive(:popen2e) do |_env, _script|
        [0, @fake_io, @fake_thr]
      end.exactly(@episode_count).times

      expect(FileUtils).to receive(:mkdir_p).twice
      status = @artifact_generator.generate(@series)
      expect(status).to be(0)
    end

    it 'can run ruby scripts' do
      FakeFS.deactivate!
      tmp = Tempfile.new('itsape-test')
      tmp.write '#!/usr/bin/ruby'
      tmp.close
      @recipe.artifact_script = "#!/bin/bash\nruby #{tmp.path}"
      expect(Logging).to receive(:debug).exactly(@episode_count).times
      expect(Logging).to receive(:info).exactly(@season_count + 1).times
      expect(@an_ape).to receive(:inform_user).once

      status = @artifact_generator.generate(@series)
      expect(status).to be(0)
      tmp.unlink
      FakeFS.activate!
    end

    it 'can run with a shebang' do
      @recipe.artifact_script = "#!/bin/bash\necho 'message1'\necho 'message2'\n"
      expect(Logging).to receive(:debug).exactly(@episode_count).times
      expect(Logging).to receive(:info).exactly(@season_count + 1).times
      expect(@an_ape).to receive(:inform_user).once

      status = @artifact_generator.generate(@series)
      expect(status).to be(0)
    end

    it 'calls the deployment manager' do
      @recipe.artifact_script = 'true'
      expect(Logging).to receive(:debug).exactly(@episode_count).times
      expect(Logging).to receive(:info).exactly(@season_count + 1).times
      expect(@an_ape).to receive(:inform_user).once

      expect(@deployment_manager).to receive('deploy_artifact').with(@series.test_seasons[0])
      status = @artifact_generator.generate(@series)
      expect(status).to be(0)
    end

    it 'creates a destroy job for the series' do
      allow(Logging).to receive(:debug)
      allow(Logging).to receive(:info)
      allow(@an_ape).to receive(:inform_user)
      expect(@scheduler).to receive(:at).with(@series.end_date, tags: ['destroy artifacts', @series.to_str])
      @artifact_generator.generate(@series)
    end
  end

  it 'unschedules jobs' do
    series = build(:test_series)
    jobs = [double('job1'), double('job2')]

    jobs.each do |job|
      expect(job).to receive(:unschedule)
    end
    expect(@scheduler).to receive(:jobs).with(tag: series.to_str).and_return(jobs)

    @artifact_generator.unschedule_jobs(series)
  end
end
