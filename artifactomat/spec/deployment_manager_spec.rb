# frozen_string_literal: true

require 'artifactomat/modules/deployment_manager'
require 'artifactomat/modules/monitoring'
require 'artifactomat/modules/logging'

require_relative 'mixins/script_executor_spec'

require 'tempfile'

describe DeploymentManager do
  before(:all) do
    Logging.initialize
  end

  before(:each) do
    @monitoring = double('Monitoring')
    allow(@monitoring).to receive(:polling_rate).and_return(10)
    @subject_tracker = double 'SubjectTracker'
    @recipe_manager = double('RecipeManager')
    @subject_manager = double('SubjectManager')
    @configuration_manager = double 'ConfigurationMananger'
    allow(@configuration_manager).to receive(:get)
      .with(ScriptExecution::APE_TMP_DIR) { '/tmp' }
    allow(@configuration_manager).to receive(:cfg_path) { '/tmp' }

    @an_ape = double('APEdouble_deplmgr_spec')
    @fake_router = double('routing_deplmgr_spec')
    # allow(@fake_router).to receive(:get_internal_ports).and_return([2, 3])
    # allow(@fake_router).to receive(:get_external_address).and_return('1.2.3.4')
    allow(@an_ape).to receive(:routing).and_return(@fake_router)
    allow(Ape).to receive(:instance).and_return(@an_ape)

    @deployment_manager =
      DeploymentManager.new(@monitoring,
                            @configuration_manager,
                            @recipe_manager,
                            @subject_manager,
                            @subject_tracker)
  end

  it_behaves_like 'a script executor' do
    let(:executor) do
      @deployment_manager
    end
  end

  describe '.new' do
    it 'requires Monitoring and TestScheduler objects' do
      expect do
        DeploymentManager.new(@monitoring,
                              @configuration_manager,
                              @recipe_manager,
                              @subject_manager,
                              @subject_tracker)
      end.not_to raise_error
    end
  end

  describe '#deploy_artifact' do
    it 'receives an artifact to deploy' do
      season = create(:test_season)
      expect { @deployment_manager.deploy_artifact(season) }.not_to raise_error
    end

    it 'stores the season ids for deployment' do
      season_ids = []
      3.times do
        season = create(:test_season)
        season_ids << season.id
        @deployment_manager.deploy_artifact(season)
      end
      expect(@deployment_manager.seasons).to match_array(season_ids)
    end
  end

  describe '#deploy_infrastructure' do
    it 'receives an infrastructure element to deploy' do
      infrastructure = build(:infrastructure_element)
      expect { @deployment_manager.deploy_infrastructure(infrastructure) }
        .not_to raise_error
    end

    it 'stores the infrastructure for deployment' do
      3.times do
        elem = build(:infrastructure_element)
        @deployment_manager.deploy_infrastructure(elem)
        expect(@deployment_manager.infrastructures).to include(elem)
      end
    end
  end

  describe '#stop_deployment' do
    before(:each) do
      allow(@fake_router).to receive(:get_internal_ports).and_return([2, 3])
      allow(@fake_router).to receive(:get_external_address).and_return('1.2.3.4')
    end

    it 'receives a test series' do
      series = build(:test_series)
      expect { @deployment_manager.stop_deployment(series) }.not_to raise_error
    end

    it 'runs the deploy_script after stop_deployment' do
      expect(Logging).to receive(:info).exactly(2).times
      expect(Logging).to receive(:debug).once
      tmpfile = Tempfile.new('artifactomat.stop_deployment')
      begin
        recipe = build(:recipe)
        recipe.deploy_script =
          'echo "$ARTIFACTOMAT_RECIPE_TESTSEASON_ID" > ' + tmpfile.path
        season = create(:test_season, recipe_id: recipe.id)
        series = create(:test_series, test_seasons: [season])
        element = build(:infrastructure_element)
        element.season_id = season.id
        allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
        allow(@configuration_manager).to receive(:config_to_env) { {} }
        allow(@monitoring).to receive(:subscribe).with(element)
        allow(@monitoring).to receive(:get_status).with(element) { MonitoringState.new(0, '') }
        allow(@subject_tracker).to receive(:start_plugins).with(series)

        # deploy infrastructure
        @deployment_manager.infrastructure_ready(series)
        @deployment_manager.deploy_infrastructure(element)

        # deploy artifacts
        @deployment_manager.deploy_artifact(season)
        @deployment_manager.stop_deployment(series)

        expect(File.file?(tmpfile.path)).to be true
        expect(tmpfile.readlines[0].chomp!).to eql(season.id.to_s)
      ensure
        tmpfile.close
        tmpfile.unlink
      end
    end
  end

  describe '#stop_deployment?' do
    it 'returns true if artifact deployment is ready' do
      ts = double('TestSeries')
      @deployment_manager.stop_deployment(ts)
      expect(@deployment_manager.stop_deployment?(ts)).to be true
    end

    it 'returns false if artifact deployment is not ready' do
      ts = double('TestSeries')
      expect(@deployment_manager.stop_deployment?(ts)).to be false
    end
  end

  describe '#infrastructure_ready' do
    before(:each) do
      allow(@fake_router).to receive(:get_internal_ports).and_return([2, 3])
      allow(@fake_router).to receive(:get_external_address).and_return('1.2.3.4')
    end

    it 'receives a test_series' do
      ts = double('TestSeries')
      expect { @deployment_manager.infrastructure_ready(ts) }.not_to raise_error
    end

    it 'runs the deploy_script after infrastructure_ready' do
      expect(Logging).to receive(:info).exactly(2).times
      expect(Logging).to receive(:debug).once
      tmpfile = Tempfile.new('artifactomat.infrastructure_ready')
      begin
        recipe = build(:recipe)
        recipe.deploy_script =
          'echo "$ARTIFACTOMAT_RECIPE_TESTSEASON_ID" > ' + tmpfile.path
        season = create(:test_season, recipe_id: recipe.id)
        series = create(:test_series, test_seasons: [season])
        element = build(:infrastructure_element)
        element.season_id = season.id
        allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
        allow(@configuration_manager).to receive(:config_to_env) { {} }
        allow(@subject_tracker).to receive(:start_plugins).with(series)

        # deploy artifacts
        @deployment_manager.deploy_artifact(season)
        @deployment_manager.stop_deployment(series)

        # deploy infrastructure
        @deployment_manager.deploy_infrastructure(element)
        @deployment_manager.infrastructure_ready(series)

        expect(File.file?(tmpfile.path)).to be true
        expect(tmpfile.readlines[0].chomp!).to eql(season.id.to_s)
      ensure
        tmpfile.close
        tmpfile.unlink
      end
    end
  end

  describe '#infrastructure_ready?' do
    it 'returns true if artifact deployment is ready' do
      ts = double('TestSeries')
      @deployment_manager.infrastructure_ready(ts)
      expect(@deployment_manager.infrastructure_ready?(ts)).to be true
    end

    it 'returns false if artifact deployment is not ready' do
      ts = double('TestSeries')
      expect(@deployment_manager.infrastructure_ready?(ts)).to be false
    end
  end

  describe '#setup' do
    before(:each) do
      allow(@fake_router).to receive(:get_internal_ports).and_return([2, 3])
      allow(@fake_router).to receive(:get_external_address).and_return('1.2.3.4')
    end

    it 'runs the deploy_script to deploy artifacts to infrastructure' do
      expect(Logging).to receive(:info).exactly(2).times
      expect(Logging).to receive(:debug).once
      tmpfile = Tempfile.new('artifactomat.deployment')
      begin
        recipe = build(:recipe)
        recipe.deploy_script =
          'echo "$ARTIFACTOMAT_RECIPE_TESTSEASON_ID" > ' + tmpfile.path
        season = create(:test_season, recipe_id: recipe.id)
        series = create(:test_series, test_seasons: [season])
        element = build(:infrastructure_element)
        element.season_id = season.id
        allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
        allow(@configuration_manager).to receive(:config_to_env) { {} }

        allow(@subject_tracker).to receive(:start_plugins).with(series)

        # expect(Ape.instance.routing).to receive(:create_routing).with(series)

        @deployment_manager.deploy_artifact(season)
        @deployment_manager.deploy_infrastructure(element)
        @deployment_manager.setup(series)

        expect(File.file?(tmpfile.path)).to be true
        expect(tmpfile.readlines[0].chomp!).to eql(season.id.to_s)
      ensure
        tmpfile.close
        tmpfile.unlink
      end
    end

    it 'informs the Monitor about required infrastructure' do
      expect(Logging).to receive(:info).exactly(2).times
      expect(Logging).to receive(:debug).once
      element = build(:infrastructure_element)
      recipe = build(:recipe, infrastructure: [element])
      recipe.deploy_script = 'echo "$ITSAPE_RECIPE_TESTSEASON_ID"'
      season = create(:test_season, recipe_id: recipe.id)
      element.season_id = season.id
      series = create(:test_series, test_seasons: [season])
      allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
      allow(@configuration_manager).to receive(:config_to_env) { {} }
      allow(@monitoring).to receive(:subscribe).with(element)
      allow(@monitoring).to(receive(:get_status).with(element).twice { MonitoringState.new(0, '') })

      allow(@subject_tracker).to receive(:start_plugins).with(series)

      @deployment_manager.deploy_artifact(season)
      @deployment_manager.deploy_infrastructure(element)
      @deployment_manager.setup(series)

      expect(@monitoring).to have_received(:subscribe).with(element)
      expect(@monitoring).to have_received(:get_status).with(element).at_least(:once)
    end

    it 'raises an DeploymentError if season is not ready for deployment' do
      element = build(:infrastructure_element)
      recipe = build(:recipe, infrastructure: [element])
      recipe.deploy_script = 'echo "$ITSAPE_RECIPE_TESTSEASON_ID"'
      season = create(:test_season, recipe_id: recipe.id)
      element.season_id = season.id
      series = create(:test_series, test_seasons: [season])

      expect { @deployment_manager.setup(series) }
        .to raise_error(DeploymentError)
    end

    it 'cancels the season if there is an IE without a season' do
      element = build(:infrastructure_element)
      recipe = build(:recipe, infrastructure: [element])
      recipe.deploy_script = 'echo "$ITSAPE_RECIPE_TESTSEASON_ID"'
      season = create(:test_season, recipe_id: recipe.id)
      element.season_id = season.id
      series = create(:test_series, test_seasons: [season])

      allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
      allow(@configuration_manager).to receive(:config_to_env) { {} }
      allow(@monitoring).to receive(:subscribe).with(element).and_raise(MonitorError)
      allow(Logging).to receive(:info)
      expect(@monitoring).to receive(:unsubscribe).with(element)
      expect(FileUtils).to receive(:rm_rf)
      expect(Logging).to receive(:error)
        .with('DeploymentManager',
              "Could not subscribe #{element.to_str} to monitoring, element has no season.")

      @deployment_manager.deploy_artifact(season)
      @deployment_manager.deploy_infrastructure(element)
      @deployment_manager.setup(series)
    end

    it 'unsubscribe infrastructure if it is not available' do
      element = build(:infrastructure_element)
      recipe = build(:recipe, infrastructure: [element])
      recipe.deploy_script = 'echo "$ITSAPE_RECIPE_TESTSEASON_ID"'
      season = create(:test_season, recipe_id: recipe.id)
      element.season_id = season.id
      series = create(:test_series, test_seasons: [season])
      allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
      expect(Ape.instance.routing).not_to receive(:create_routing).with(series)

      @deployment_manager.deploy_artifact(season)
      ie_dep_manager = @deployment_manager.instance_variable_get(:@infrastructure_deployment_manager)
      expect(Logging).to receive(:error).once
      expect(Logging).to receive(:info).once
      expect(ie_dep_manager).to receive(:unsubscribe_monitoring)
      expect { @deployment_manager.setup(series) }
        .not_to raise_error(DeploymentError)
    end
  end

  describe '#withdraw' do
    it 'fails if the infrastructure generator is not set' do
      expect(Logging).to receive(:info).exactly(2)
      recipe = build(:recipe)
      season = create(:test_season, recipe_id: recipe.id)
      series = create(:test_series, test_seasons: [season])
      element = build(:infrastructure_element)
      element.season_id = season.id
      allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
      allow(@configuration_manager).to receive(:get).with(ScriptExecution::APE_TMP_DIR) { '/tmp' }
      allow(@configuration_manager).to receive(:cfg_path) { '/tmp' }
      expect(Ape.instance.routing).to receive(:remove_routing).with(series)

      expect { @deployment_manager.withdraw(series) }
        .to raise_error(NoMethodError)
    end

    it 'calls the infrastructure generator to destory the series' do
      expect(Logging).to receive(:info).exactly(2)
      recipe = build(:recipe)
      season = create(:test_season, recipe_id: recipe.id)
      series = create(:test_series, test_seasons: [season])
      allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
      allow(@configuration_manager).to receive(:get).with(ScriptExecution::APE_TMP_DIR) { '/tmp' }
      allow(@configuration_manager).to receive(:cfg_path) { '/tmp' }
      infragen = double('InfrastructureGenerator')
      allow(infragen).to receive(:destroy).with(series)
      allow(@subject_tracker).to receive(:deactivate_series).with(series.id)

      expect(Ape.instance.routing).to receive(:remove_routing).with(series)
      @deployment_manager.infrastructure_generator = infragen
      @deployment_manager.withdraw(series)

      expect(infragen).to have_received(:destroy).once.with(series)
    end
  end

  describe '#remove_folder' do
    it 'does not fail if the folder does not exist' do
      expect(Logging).to receive(:info).once
      season = create(:test_season)
      allow(@configuration_manager).to receive(:get).with(ScriptExecution::APE_TMP_DIR) { '/tmp' }
      allow(@configuration_manager).to receive(:cfg_path) { '/tmp' }

      expect { @deployment_manager.remove_folder(season) }.not_to raise_error
    end

    it 'removes the folder' do
      expect(Logging).to receive(:info).once
      artifactomat_recipe_tmp_dir = '/tmp/artifactomat_recipe_tmp_dir'
      season = create(:test_season)
      allow(@configuration_manager).to receive(:get)
        .with(ScriptExecution::APE_TMP_DIR) { artifactomat_recipe_tmp_dir }
      allow(@configuration_manager).to receive(:cfg_path) { '/tmp' }
      dirname = File.join(artifactomat_recipe_tmp_dir, season.id.to_s)

      FileUtils.mkdir_p(dirname)

      @deployment_manager.remove_folder(season)
      expect(File.directory?(dirname)).to be false
    end
  end
end
