# frozen_string_literal: true

require 'artifactomat/modules/client_builder'
require 'fakefs/spec_helpers'
require 'fileutils'

describe ClientBuilder do
  include FakeFS::SpecHelpers

  def create_windows_client_fake_dir(client_path)
    FileUtils.mkdir_p(File.join(client_path, '/tls/private'))
    FileUtils.mkdir_p(File.join(client_path, '/tls/public'))
    FileUtils.mkdir_p(File.join(client_path, 'ITSAPE-Client'))
    # mock config only containing the necessary elements for the spec
    cfg = {
      'uproxy' => { 'host' => '0.0.0.0', 'port' => 8443 },
      'dns' => { 'itsape' => '0.0.0.0' },
      # sha1sum of 'client' - the content of the client.itsape.cer
      'CertThumbprint' => 'd2a04d71301a8915217dd5faf81d12cffd6cd958'
    }
    File.write(File.join(client_path, '/ITSAPE-Client/config.example.json'), JSON.dump(cfg))
    File.write(File.join(client_path, '/tls/VARIABLES'), "SOMEVAR=SOMETEXT\nCLIENTPFXPASSWORD=\"password123\"\nOTHERVAR=OTHERVAL")
  end

  def create_artifactomat_fake_dir
    FileUtils.mkdir_p('/var/lib/artifactomat')
    FileUtils.mkdir_p('/certs/public')
    FileUtils.mkdir_p('/certs/private')
    File.write('/certs/public/ca.cer', 'ca')
    File.write('/certs/public/client.itsape.cer', 'client')
    File.write('/certs/public/server.itsape.cer', 'server')
    File.write('/certs/private/client.itsape.pfx', 'client_pfx')
    File.write('/var/lib/artifactomat/client_cert_password', 'cert_password')
  end

  def copy_artifactomat_certs_to_windows_client(client_path)
    FileUtils.copy('/certs/public/ca.cer', File.join(client_path, '/tls/public/ca.cer'))
    FileUtils.copy('/certs/public/client.itsape.cer',
                   File.join(client_path, '/tls/private/client.itsape.cer'))
    FileUtils.copy('/certs/public/server.itsape.cer',
                   File.join(client_path, '/tls/public/server.itsape.cer'))
    FileUtils.copy('/certs/private/client.itsape.pfx',
                   File.join(client_path, '/tls/private/client.itsape.pfx'))
  end

  before(:all) do
    FakeFS.activate!
  end

  before(:each) do
    FakeFS::FileSystem.clear

    @client_path = '/var/lib/artifactomat/windows-client-1.0.0.0'
    @conf_man = double('ConfigurationManager')
    @package = double('Gem::Package')
    allow(Gem::Package).to receive(:new).and_return(@package)
    allow(@package).to receive(:extract_tar_gz)
    allow(@conf_man).to receive(:cfg_path).and_return('/')
    allow(@conf_man).to receive(:get).with('conf.windows_client.version').and_return('1.0.0.0')
    allow(URI).to receive(:open)

    FileUtils.mkdir('/tmp')

    create_artifactomat_fake_dir
    create_windows_client_fake_dir(@client_path)

    @client_builder = ClientBuilder.new(@conf_man)
  end

  after(:all) do
    FakeFS.deactivate!
  end

  describe '#create_installer' do
    before(:each) do
      FileUtils.touch(File.join(@client_path, 'ITSAPE.Client.msi'))
      allow(@client_builder).to receive(:system)
      allow(@conf_man).to receive(:get).with('conf.network.external_ip').and_return('127.0.0.1')
      allow(@conf_man).to receive(:get).with('conf.ape_job_spooler.port').and_return(8443)
    end

    describe 'download' do
      before(:each) do
        allow(File).to receive(:chmod)
      end

      it 'downloads the client if it does not exist' do
        FileUtils.rm_rf(@client_path)
        stream = double('stream')
        expect(URI).to receive(:open) \
          .with('https://gitlab.com/itsape/windows-client/-/archive/1.0.0.0/windows-client-1.0.0.0.tar.gz') \
          .and_return(stream)
        expect(@package).to receive(:extract_tar_gz).with(stream, '/var/lib/artifactomat') do
          create_windows_client_fake_dir(@client_path)
        end
        @client_builder.create_installer
      end

      it 'downloads the correct version based on the config' do
        FileUtils.rm_rf(@client_path)
        allow(@conf_man).to receive(:get).with('conf.windows_client.version').and_return('2.3.4.5')
        @client_builder = ClientBuilder.new(@conf_man) # read new windows-client version
        allow(@client_builder).to receive(:system)
        client_path = '/var/lib/artifactomat/windows-client-2.3.4.5'
        expect(URI).to receive(:open) \
          .with('https://gitlab.com/itsape/windows-client/-/archive/2.3.4.5/windows-client-2.3.4.5.tar.gz') do
          end
        allow(@package).to receive(:extract_tar_gz) do
          create_windows_client_fake_dir(client_path)
        end
        @client_builder.create_installer
      end

      it 'raises a WindowsClientError if the download fails' do
        FileUtils.rm_rf(@client_path)
        allow(URI).to receive(:open).and_raise(SocketError, 'Connection failed')
        expect { @client_builder.create_installer }.to(
          raise_error(WindowsClientError, 'Failed to download client: Connection failed')
        )
      end

      it 'does not download the client if it exists' do
        expect(URI).not_to receive(:open)
        @client_builder.create_installer
      end
    end

    describe 'building' do
      before(:each) do
        FileUtils.mkdir_p('/windows-client/publish')
        FileUtils.mkdir_p(File.join(@client_path, 'publish'))
      end

      it 'rebuilds if "publish" directory does not exist' do
        FileUtils.rm_rf(File.join(@client_path, 'publish'))
        expect(@client_builder).to receive(:system).with(/docker run .* itsape-build/)
        @client_builder.create_installer
      end

      it 'does not rebuild else' do
        expect(@client_builder).not_to receive(:system).with(/docker run .* itsape-build/)
        @client_builder.create_installer
      end
    end

    describe 'packaging' do
      before(:each) do
        copy_artifactomat_certs_to_windows_client(@client_path)
        allow(@conf_man).to receive(:get).with('conf.network.external_ip').and_return('0.0.0.0')
      end

      it 'repacks if the installer is not present' do
        FileUtils.rm(File.join(@client_path, 'ITSAPE.Client.msi'))
        allow(File).to receive(:chmod)
        expect(@client_builder).to receive(:system).with(/docker run .* itsape-pack/)
        @client_builder.create_installer
      end

      it 'repacks if client config ip is different from artifactomat config ip' do
        allow(@conf_man).to receive(:get).with('conf.network.external_ip').and_return('127.0.0.1')
        expect(@client_builder).to receive(:system).with(/docker run .* itsape-pack/)
        @client_builder.create_installer
      end

      it 'repacks if artifactomat certificates are different from client certificates' do
        File.write('/certs/public/ca.cer', 'something different')
        expect(@client_builder).to receive(:system).with(/docker run .* itsape-pack/)
        @client_builder.create_installer
      end

      it 'repacks if client certificates do not exist' do
        FileUtils.rm(File.join(@client_path, '/tls/public/ca.cer'))
        expect(@client_builder).to receive(:system).with(/docker run .* itsape-pack/)
        @client_builder.create_installer
      end

      it 'does not repack else' do
        expect(@client_builder).not_to receive(:system).with(/docker run .* itsape-pack/)
        @client_builder.create_installer
      end

      it 'preconfigures the client' do
        FileUtils.rm(File.join(@client_path, 'ITSAPE.Client.msi'))
        allow(File).to receive(:chmod)
        allow(@client_builder).to receive(:system)
        @client_builder.create_installer

        client_config = JSON.parse(
          File.read(File.join(@client_path, '/ITSAPE-Client/config.json'))
        )
        tls_vars = File.read(File.join(@client_path, '/tls/VARIABLES'))

        expect(client_config['uproxy']['host']).to eq('server.itsape.zad.local')
        expect(client_config['uproxy']['port']).to eq(8443)
        expect(client_config['dns']['itsape']).to eq('0.0.0.0')
        expect(client_config['CertThumbprint']).to eq('d2a04d71301a8915217dd5faf81d12cffd6cd958')
        expect(tls_vars.include?('CLIENTPFXPASSWORD="cert_password"'))
      end

      it 'copies the necessary certificates' do
        FileUtils.rm(File.join(@client_path, 'ITSAPE.Client.msi'))
        allow(File).to receive(:chmod)
        allow(@client_builder).to receive(:system)
        @client_builder.create_installer

        expect(File.exist?(File.join(@client_path, 'tls', 'public',  'ca.cer'))).to be_truthy
        expect(File.exist?(File.join(@client_path, 'tls', 'public',  'server.itsape.cer'))).to be_truthy
        expect(File.exist?(File.join(@client_path, 'tls', 'private', 'client.itsape.cer'))).to be_truthy
        expect(File.exist?(File.join(@client_path, 'tls', 'private', 'client.itsape.pfx'))).to be_truthy
      end

      it 'raises error if password file is not present' do
        FileUtils.rm('/var/lib/artifactomat/client_cert_password')
        FileUtils.rm(File.join(@client_path, 'ITSAPE.Client.msi')) # necessary to start packing
        allow(File).to receive(:chmod)
        allow(@client_builder).to receive(:system)
        expect { @client_builder.create_installer }.to raise_error(WindowsClientError, /certificate password/)
      end
    end

    it 'makes the installer readable to everyone' do
      expect(File).to receive(:chmod).with(0o644, File.join(@client_path, 'ITSAPE.Client.msi'))
      @client_builder.create_installer
    end
  end
end
