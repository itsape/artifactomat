# frozen_string_literal: true

require 'artifactomat/modules/scheduler'

describe Scheduler do
  before(:all) do
    Logging.initialize
  end

  it 'logs an error in a job' do
    rs = Scheduler.new
    expect(Logging).to receive(:error).with('Scheduler', 'Failed to not raise an error: error')
    rs.at(Time.now, tag: 'not raise an error') do
      raise 'error'
    end
    sleep(0.5) # give the scheduler some time to start the job
  end
end
