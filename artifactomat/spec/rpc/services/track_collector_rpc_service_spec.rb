# frozen_string_literal: true

require 'artifactomat/rpc/services/track_collector_rpc_service'

describe TrackCollectorRpcService do
  before(:each) do
    conf_man = double('ConfigurationManager')
    @track_collector = double('TrackCollector')

    @tc_rpc_service = TrackCollectorRpcService.new(conf_man, @track_collector)
  end

  describe 'passthrough methods' do
    it '#status' do
      expect(@track_collector).to receive(:status)

      @tc_rpc_service.status
    end

    it '#parse_trace' do
      args = { 'trace' => "trace one\ntrace two\n", 'series' => 'test series' }
      expect(@track_collector).to receive(:parse_trace).with(args)

      @tc_rpc_service.parse_trace(args)
    end

    it '#parse_helpdesk' do
      args = { 'trace' => "trace one\ntrace two\n", 'series' => 'test series' }
      expect(@track_collector).to receive(:parse_helpdesk).with(args)

      @tc_rpc_service.parse_helpdesk(args)
    end
  end

  describe 'custom methods' do
    before(:each) do
      @series = create(:test_series)
    end

    after(:each) do
      TestSeries.destroy_all
    end

    it '#register_series gets series from ID' do
      expect(@track_collector).to receive(:register_series).with(@series)

      @tc_rpc_service.register_series(@series.id)
    end

    it '#unregister_series gets series from ID' do
      expect(@track_collector).to receive(:unregister_series).with(@series)

      @tc_rpc_service.unregister_series(@series.id)
    end
  end
end
