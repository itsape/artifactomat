# frozen_string_literal: true

require 'artifactomat/rpc/services/subject_tracker_rpc_service'

describe SubjectTrackerRpcService do
  describe 'passthrough methods' do
    before(:each) do
      conf_man = double('ConfigurationManager')
      @subject_tracker = double('SubjectTracker')

      @st_rpc_service = SubjectTrackerRpcService.new(conf_man, @subject_tracker)
    end

    it '#status' do
      expect(@subject_tracker).to receive(:status)

      @st_rpc_service.status
    end

    it '#subject_ids' do
      expect(@subject_tracker).to receive(:subject_ids)

      @st_rpc_service.subject_ids
    end

    it '#subjects_changed' do
      status = { new: %w[new1 new2], removed: %w[removed1 removed2], changed: %w[changed] }
      expect(@subject_tracker).to receive(:subjects_changed).with(status)

      @st_rpc_service.subjects_changed(status)
    end

    it '#activate_series' do
      series_id = 'series_1'
      expect(@subject_tracker).to receive(:activate_series).with(series_id)

      @st_rpc_service.activate_series(series_id)
    end

    it '#deactivate_series' do
      series_id = 'series_1'
      expect(@subject_tracker).to receive(:deactivate_series).with(series_id)

      @st_rpc_service.deactivate_series(series_id)
    end
  end
end
