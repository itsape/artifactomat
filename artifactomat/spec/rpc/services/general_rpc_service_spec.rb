# frozen_string_literal: true

require 'artifactomat/rpc/services/general_rpc_service'

class DummyRpcService
  include GeneralRpcService

  def initialize(conf_man, underlying_module)
    @service_name = 'dummy'
    @standard_proxy_methods = %i[foo bar?]
    @configuration_manager = conf_man
    @underlying_module = underlying_module
  end

  private

  attr_reader :service_name, :standard_proxy_methods, :underlying_module, :configuration_manager
end

describe GeneralRpcService do
  before(:each) do
    @conf_man = double('ConfigurationManager')
    @redis = double('Redis')
    @rpc_server = double('RedisRpc::Server')
    @underlying_module = double('UnderlyingModule')
    allow(Thread).to receive(:new).and_yield
    allow(Redis).to receive(:new).and_return(@redis)
    allow(RedisRpc::Server).to receive(:new).and_return(@rpc_server)
    allow(@conf_man).to receive(:get).with('conf.ape.redis_socket').and_return('/var/run/redis-artifactomat/redis-server.sock')

    @rpc_service = DummyRpcService.new(@conf_man, @underlying_module)
  end

  it '#start runs a new redis rpc server' do
    expect(Redis).to receive(:new).with(path: '/var/run/redis-artifactomat/redis-server.sock').and_return(@redis)
    expect(RedisRpc::Server).to receive(:new).with(@redis, 'dummy', @rpc_service).and_return(@rpc_server)
    expect(@rpc_server).to receive(:run)

    @rpc_service.start
  end

  it 'responds to methods defined in the standard proxy methods' do
    expect(@rpc_service.respond_to?(:foo)).to be_truthy
    expect(@rpc_service.respond_to?(:bar?)).to be_truthy
  end

  it 'does not respond to other missing methods' do
    expect(@rpc_service.respond_to?(:foo?)).to be_falsy
    expect(@rpc_service.respond_to?(:bar)).to be_falsy
    expect(@rpc_service.respond_to?(:other)).to be_falsy
  end

  it 'forwards standard proxy methods do the underlying module' do
    expect(@underlying_module).to receive(:foo).with('hello', 5)
    @rpc_service.foo('hello', 5)
    expect(@underlying_module).to receive(:bar?)
    @rpc_service.bar?
  end

  it 'does not forward methods that are not in standard proxy methods to the underlying module' do
    expect { @rpc_service.unavailable('hello', 5) }.to raise_error(NoMethodError)
  end
end
