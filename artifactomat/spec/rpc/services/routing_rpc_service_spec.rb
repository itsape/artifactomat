# frozen_string_literal: true

require 'artifactomat/rpc/services/routing_rpc_service'

describe RoutingService do
  before(:each) do
    conf_man = double('ConfigurationManager')
    recipe_man = double('RecipeManager')
    @routing = double('Routing')

    allow(ConfigurationManager).to receive(:new).and_return(conf_man)
    allow(RecipeManager).to receive(:new).and_return(recipe_man)
    allow(Routing).to receive(:new).and_return(@routing)
    expect(@routing).to receive(:run)

    @routing_rpc_service = RoutingService.new
  end

  describe 'passthrough methods' do
    it '#run' do
      expect(@routing).to receive(:run)

      @routing_rpc_service.run
    end

    it '#running?' do
      expect(@routing).to receive(:running?)

      @routing_rpc_service.running?
    end
  end

  describe 'custom methods' do
    after(:all) do
      TestSeries.destroy_all
      TestSeason.destroy_all
      TestEpisode.destroy_all
    end

    it '#create_routing gets series from ID' do
      series = create(:test_series)

      expect(@routing).to receive(:create_routing).with(series)

      @routing_rpc_service.create_routing(series.id)
    end

    it '#remove_routing gets series from ID' do
      series = create(:test_series)

      expect(@routing).to receive(:remove_routing).with(series)

      @routing_rpc_service.remove_routing(series.id)
    end

    it '#get_internal_ports gets season from ID' do
      season = create(:test_season)

      expect(@routing).to receive(:get_internal_ports).with(season)

      @routing_rpc_service.get_internal_ports(season.id)
    end

    it '#get_external_address gets season from ID' do
      season = create(:test_season)

      expect(@routing).to receive(:get_external_address).with(season)

      @routing_rpc_service.get_external_address(season.id)
    end

    it '#activate gets episode from ID' do
      episode = create(:test_episode)

      expect(@routing).to receive(:activate).with(episode)

      @routing_rpc_service.activate(episode.id)
    end

    it '#deactivate gets episode from ID' do
      episode = create(:test_episode)

      expect(@routing).to receive(:deactivate).with(episode)

      @routing_rpc_service.deactivate(episode.id)
    end

    it '#unschedule_jobs gets series from ID' do
      series = create(:test_series)

      expect(@routing).to receive(:unschedule_jobs).with(series)

      @routing_rpc_service.unschedule_jobs(series.id)
    end
  end
end
