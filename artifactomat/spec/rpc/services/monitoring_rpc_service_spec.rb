# frozen_string_literal: true

require 'artifactomat/rpc/services/monitoring_rpc_service'

describe MonitoringRpcService do
  before(:each) do
    conf_man = double('ConfigurationManager')
    del_man = double('DeliveryManager')
    recipe_man = double('RecipeManager')
    subject_man = double('SubjectManager')
    @monitoring = double('Monitoring')

    allow(ConfigurationManager).to receive(:new).and_return(conf_man)
    allow(DeliveryManager).to receive(:new).and_return(del_man)
    allow(Monitoring).to receive(:new).and_return(@monitoring)
    allow(RecipeManager).to receive(:new).and_return(recipe_man)
    allow(SubjectManager).to receive(:new).and_return(subject_man)
    expect(@monitoring).to receive(:run)

    @monitoring_rpc_service = MonitoringRpcService.new
  end

  describe 'passthrough methods' do
    it '#status' do
      expect(@monitoring).to receive(:status)

      @monitoring_rpc_service.status
    end

    it '#running?' do
      expect(@monitoring).to receive(:running?)

      @monitoring_rpc_service.running?
    end

    it '#stop' do
      expect(@monitoring).to receive(:stop)

      @monitoring_rpc_service.stop
    end

    it '#run' do
      expect(@monitoring).to receive(:run)

      @monitoring_rpc_service.run
    end

    it '#to_str' do
      expect(@monitoring).to receive(:to_str)

      @monitoring_rpc_service.to_str
    end

    it '#polling_rate' do
      expect(@monitoring).to receive(:polling_rate)

      @monitoring_rpc_service.polling_rate
    end
  end

  describe 'custom methods' do
    before(:each) do
      @ie = build(:infrastructure_element)
    end

    it '#get_status creates IE from yaml, returns hashmap' do
      status = double('status')
      allow(status).to receive(:to_i).and_return(1)
      allow(status).to receive(:error).and_return('no error')
      expect(@monitoring).to receive(:get_status).with(@ie).and_return(status)

      status_hash = @monitoring_rpc_service.get_status(@ie.to_yaml)
      expect(status_hash).to eq(state: 1, error: 'no error')
    end

    it '#subscribe creates IE from yaml' do
      expect(@monitoring).to receive(:subscribe).with(@ie)

      @monitoring_rpc_service.subscribe(@ie.to_yaml)
    end

    it '#unsubscribe creates IE from yaml' do
      expect(@monitoring).to receive(:unsubscribe).with(@ie)

      @monitoring_rpc_service.unsubscribe(@ie.to_yaml)
    end

    it '#infrastructure_running? creates IE from yaml' do
      expect(@monitoring).to receive(:infrastructure_running?).with(@ie)

      @monitoring_rpc_service.infrastructure_running?(@ie.to_yaml)
    end
  end
end
