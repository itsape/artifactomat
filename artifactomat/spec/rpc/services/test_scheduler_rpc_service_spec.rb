# frozen_string_literal: true

require 'artifactomat/rpc/services/test_scheduler_rpc_service'

describe TestSchedulerRpcService do
  before(:each) do
    conf_man = double('ConfigurationManager')
    del_man = double('DeliveryManager')
    recipe_man = double('RecipeManager')
    subject_man = double('SubjectManager')
    @test_scheduler = double('TestScheduler')

    allow(ConfigurationManager).to receive(:new).and_return(conf_man)
    allow(DeliveryManager).to receive(:new).and_return(del_man)
    allow(TestScheduler).to receive(:new).and_return(@test_scheduler)
    allow(RecipeManager).to receive(:new).and_return(recipe_man)
    allow(SubjectManager).to receive(:new).and_return(subject_man)

    @scheduler_rpc_service = TestSchedulerRpcService.new
  end

  describe 'passthrough methods' do
    it '#status' do
      expect(@test_scheduler).to receive(:status)

      @scheduler_rpc_service.status
    end

    it '#format_duration' do
      duration = 180
      expect(@test_scheduler).to receive(:format_duration).with(duration)

      @scheduler_rpc_service.format_duration(duration)
    end

    it '#running?' do
      expect(@test_scheduler).to receive(:running?)

      @scheduler_rpc_service.running?
    end

    it '#stop' do
      expect(@test_scheduler).to receive(:stop)

      @scheduler_rpc_service.stop
    end
  end

  describe 'custom methods' do
    after(:all) do
      TestSeries.destroy_all
      TestEpisode.destroy_all
    end

    it '#schedule gets series from ID' do
      series = create(:test_series)

      expect(@test_scheduler).to receive(:schedule).with(series)

      @scheduler_rpc_service.schedule(series.id)
    end

    it '#generate_timetable gets series from ID' do
      series = create(:test_series)

      expect(@test_scheduler).to receive(:generate_timetable).with(series)

      @scheduler_rpc_service.generate_timetable(series.id)
    end

    it '#ack gets series from ID' do
      series = create(:test_series)

      expect(@test_scheduler).to receive(:ack).with(series)

      @scheduler_rpc_service.ack(series.id)
    end

    it '#series_current_testtime gets series from ID' do
      testtime_delta = 5
      series = create(:test_series)

      expect(@test_scheduler).to receive(:series_current_testtime).with(series, testtime_delta)

      @scheduler_rpc_service.series_current_testtime(series.id, testtime_delta)
    end

    it '#series_enough_runtime gets series from ID' do
      testtime_delta = 5
      series = create(:test_series)

      expect(@test_scheduler).to receive(:series_enough_runtime).with(series, testtime_delta)

      @scheduler_rpc_service.series_enough_runtime(series.id, testtime_delta)
    end

    it '#add_jobs gets episode from ID' do
      episode = create(:test_episode)

      expect(@test_scheduler).to receive(:add_jobs).with(episode)

      @scheduler_rpc_service.add_jobs(episode.id)
    end

    it '#unschedule_jobs gets series from ID' do
      series = create(:test_series)

      expect(@test_scheduler).to receive(:unschedule_jobs).with(series)

      @scheduler_rpc_service.unschedule_jobs(series.id)
    end
  end
end
