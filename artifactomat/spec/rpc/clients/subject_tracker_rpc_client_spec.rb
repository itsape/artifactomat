# frozen_string_literal: true

require 'artifactomat/rpc/clients/subject_tracker_rpc_client'

describe SubjectTrackerRpcClient do
  before(:each) do
    @redis = double('Redis')
    conf_man = double('ConfigurationManager')
    sub_man = double('SubjectManager')

    allow(Redis).to receive(:new).and_return(@redis)
    allow(conf_man).to receive(:get)

    @st_rpc_client = SubjectTrackerRpcClient.new(conf_man, sub_man)
    @redis_client = @st_rpc_client.instance_variable_get(:@client)
  end

  describe 'passthrough methods' do
    it '#status' do
      expect(@redis_client).to receive(:send).with(:status)
      @st_rpc_client.status
    end

    it '#subject_ids' do
      expect(@redis_client).to receive(:send).with(:subject_ids)
      @st_rpc_client.subject_ids
    end

    it '#subjects_changed' do
      status = { new: %w[new1 new2], removed: %w[removed1 removed2], changed: %w[changed] }
      expect(@redis_client).to receive(:send).with(:subjects_changed, status)
      @st_rpc_client.subjects_changed(status)
    end

    it '#activate_series' do
      series_id = 'series_1'
      expect(@redis_client).to receive(:send).with(:activate_series, series_id)
      @st_rpc_client.activate_series(series_id)
    end

    it '#deactivate_series' do
      series_id = 'series_1'
      expect(@redis_client).to receive(:send).with(:deactivate_series, series_id)
      @st_rpc_client.deactivate_series(series_id)
    end
  end
end
