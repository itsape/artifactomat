# frozen_string_literal: true

require 'artifactomat/rpc/clients/monitoring_rpc_client'

describe MonitoringRpcClient do
  before(:each) do
    @redis = double('Redis')
    conf_man = double('ConfigurationManager')
    rec_man = double('RecipeManager')
    sub_man = double('SubjectManager')
    del_man = double('DeliveryManager')

    allow(Redis).to receive(:new).and_return(@redis)
    allow(conf_man).to receive(:get)

    @monitoring_rpc_client = MonitoringRpcClient.new(conf_man, rec_man, sub_man, del_man)
    @redis_client = @monitoring_rpc_client.instance_variable_get(:@client)
  end

  describe 'passthrough methods' do
    it '#status' do
      expect(@redis_client).to receive(:send).with(:status)
      @monitoring_rpc_client.status
    end

    it '#running?' do
      expect(@redis_client).to receive(:send).with(:running?)
      @monitoring_rpc_client.running?
    end

    it '#stop' do
      expect(@redis_client).to receive(:send).with(:stop)
      @monitoring_rpc_client.stop
    end

    it '#run' do
      expect(@redis_client).to receive(:send).with(:run)
      @monitoring_rpc_client.run
    end

    it '#to_str' do
      expect(@redis_client).to receive(:send).with(:to_str)
      @monitoring_rpc_client.to_str
    end

    it '#polling_rate' do
      expect(@redis_client).to receive(:send).with(:polling_rate)
      @monitoring_rpc_client.polling_rate
    end
  end

  describe 'custom methods' do
    before(:each) do
      @ie = build(:infrastructure_element)
    end

    it '#get_status converts to yaml and returns MonitoringState' do
      state_info = { 'state' => 'running', 'error' => '' }
      mon_state = double('MonitoringState')

      expect(@redis_client).to receive(:get_status).with(@ie.to_yaml).and_return(state_info)
      expect(MonitoringState).to receive(:new).with('running', '').and_return(mon_state)

      status = @monitoring_rpc_client.get_status(@ie)

      expect(status).to be(mon_state)
    end

    it '#subscribe converts to yaml' do
      expect(@redis_client).to receive(:subscribe).with(@ie.to_yaml)

      @monitoring_rpc_client.subscribe(@ie)
    end

    it '#unsubscribe converts to yaml' do
      expect(@redis_client).to receive(:unsubscribe).with(@ie.to_yaml)

      @monitoring_rpc_client.unsubscribe(@ie)
    end

    it '#infrastructure_running? converts to yaml' do
      expect(@redis_client).to receive(:infrastructure_running?).with(@ie.to_yaml)

      @monitoring_rpc_client.infrastructure_running?(@ie)
    end
  end
end
