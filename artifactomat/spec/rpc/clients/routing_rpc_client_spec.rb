# frozen_string_literal: true

require 'artifactomat/rpc/clients/routing_rpc_client'

describe RoutingClient do
  before(:each) do
    @redis = double('Redis')
    conf_man = double('ConfigurationManager')
    recipe_man = double('RecipeManager')

    allow(Redis).to receive(:new).and_return(@redis)
    allow(conf_man).to receive(:get)

    @routing_rpc_client = RoutingClient.new(conf_man, recipe_man)
    @redis_client = @routing_rpc_client.instance_variable_get(:@client)
  end

  describe 'passthrough methods' do
    it '#run' do
      expect(@redis_client).to receive(:send).with(:run)
      @routing_rpc_client.run
    end

    it '#running?' do
      expect(@redis_client).to receive(:send).with(:running?)
      @routing_rpc_client.running?
    end
  end

  describe 'custom methods' do
    it '#create_routing only passes id' do
      series = build(:test_series)
      expect(@redis_client).to receive(:create_routing).with(series.id)
      @routing_rpc_client.create_routing(series)
    end

    it '#remove_routing only passes id' do
      series = build(:test_series)
      expect(@redis_client).to receive(:remove_routing).with(series.id)
      @routing_rpc_client.remove_routing(series)
    end

    it '#get_internal_ports only passes id' do
      season = build(:test_season)
      expect(@redis_client).to receive(:get_internal_ports).with(season.id)
      @routing_rpc_client.get_internal_ports(season)
    end

    it '#get_external_address only passes id' do
      season = build(:test_season)
      expect(@redis_client).to receive(:get_external_address).with(season.id)
      @routing_rpc_client.get_external_address(season)
    end

    it '#activate only passes id' do
      episode = build(:test_episode)
      expect(@redis_client).to receive(:activate).with(episode.id)
      @routing_rpc_client.activate(episode)
    end

    it '#deactivate only passes id' do
      episode = build(:test_episode)
      expect(@redis_client).to receive(:deactivate).with(episode.id)
      @routing_rpc_client.deactivate(episode)
    end

    it '#unschedule_jobs only passes id' do
      series = build(:test_series)
      expect(@redis_client).to receive(:unschedule_jobs).with(series.id)
      @routing_rpc_client.unschedule_jobs(series)
    end
  end
end
