# frozen_string_literal: true

require 'artifactomat/rpc/clients/general_rpc_client'
require 'artifactomat/modules/logging'

class DummyRpcClient
  include GeneralRpcClient

  def initialize(conf_man)
    @service_name = 'dummy'
    @standard_proxy_methods = %i[foo bar?]
    @client = create_rpc_client(conf_man)
  end

  private

  attr_reader :client, :service_name, :standard_proxy_methods
end

describe GeneralRpcClient do
  before(:each) do
    @redis = double('Redis')
    @conf_man = double('ConfigurationManager')
    allow(Redis).to receive(:new).and_return(@redis)
    allow(@conf_man).to receive(:get).with('conf.ape.redis_socket').and_return('/var/run/redis-artifactomat/redis-server.sock')

    @client = DummyRpcClient.new(@conf_man)
    @rpc_client = @client.instance_variable_get(:@client)
  end

  it 'creates an rpc client on initialization' do
    expect(Redis).to receive(:new).with(path: '/var/run/redis-artifactomat/redis-server.sock').and_return(@redis)
    expect(RedisRpc::Client).to receive(:new).with(@redis, 'dummy', 10).and_return(@rpc_client)
    client = DummyRpcClient.new(@conf_man)
    expect(client.instance_variable_get(:@client)).to be(@rpc_client)
  end

  it 'responds to standard proxy methods' do
    expect(@client).to respond_to(:foo).with(2).arguments
    expect(@client).to respond_to(:bar?).with(0).arguments
  end

  it 'does not respond to other missing methods' do
    expect(@client).not_to respond_to(:nono)
  end

  it 'forwards standard proxy methods to the client' do
    expect(@rpc_client).to receive(:send).with(:foo, 'argument')
    @client.foo('argument')
  end

  it 'logs and raises errors if the forwarding times out' do
    allow(@rpc_client).to receive(:send).and_raise(RedisRpc::TimeoutException)
    expect(Logging).to receive(:error).with('dummy', /RedisRpcTimeout/)
    expect { @client.foo }.to raise_error(GeneralRpcClient::RpcTimeoutError, /dummy.*timeout.*running\?/i)
  end
end
