# frozen_string_literal: true

require 'artifactomat/rpc/clients/test_scheduler_rpc_client'

describe TestSchedulerRpcClient do
  before(:each) do
    @redis = double('Redis')
    conf_man = double('ConfigurationManager')
    del_man = double('DeliveryManager')
    sub_man = double('SubjectManager')

    allow(Redis).to receive(:new).and_return(@redis)
    allow(conf_man).to receive(:get)

    @scheduler_rpc_client = TestSchedulerRpcClient.new(conf_man, sub_man, del_man)
    @redis_client = @scheduler_rpc_client.instance_variable_get(:@client)
  end

  describe 'passthrough methods' do
    it '#status' do
      expect(@redis_client).to receive(:send).with(:status)
      @scheduler_rpc_client.status
    end

    it '#format_duration' do
      duration = 180
      expect(@redis_client).to receive(:send).with(:format_duration, duration)
      @scheduler_rpc_client.format_duration(duration)
    end

    it '#running?' do
      expect(@redis_client).to receive(:send).with(:running?)
      @scheduler_rpc_client.running?
    end
  end

  describe 'custom methods' do
    it '#schedule only passes id' do
      series = build(:test_series)

      expect(@redis_client).to receive(:schedule).with(series.id)

      @scheduler_rpc_client.schedule(series)
    end

    it '#generate_timetable only passes id' do
      series = build(:test_series)

      expect(@redis_client).to receive(:generate_timetable).with(series.id)

      @scheduler_rpc_client.generate_timetable(series)
    end

    it '#ack only passes id' do
      series = build(:test_series)

      expect(@redis_client).to receive(:ack).with(series.id)

      @scheduler_rpc_client.ack(series)
    end

    it '#series_current_testtime only passes id' do
      testtime_delta = 5
      series = build(:test_series)

      expect(@redis_client).to receive(:series_current_testtime).with(series.id, testtime_delta)

      @scheduler_rpc_client.series_current_testtime(series, testtime_delta)
    end

    it '#series_enough_runtime only passes id' do
      testtime_delta = 5
      series = build(:test_series)

      expect(@redis_client).to receive(:series_enough_runtime).with(series.id, testtime_delta)

      @scheduler_rpc_client.series_enough_runtime(series, testtime_delta)
    end

    it '#add_jobs only passes id' do
      episode = build(:test_episode)

      expect(@redis_client).to receive(:add_jobs).with(episode.id)

      @scheduler_rpc_client.add_jobs(episode)
    end

    it '#unschedule_jobs only passes id' do
      series = build(:test_series)

      expect(@redis_client).to receive(:unschedule_jobs).with(series.id)

      @scheduler_rpc_client.unschedule_jobs(series)
    end
  end
end
