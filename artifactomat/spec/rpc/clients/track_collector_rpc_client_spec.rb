# frozen_string_literal: true

require 'artifactomat/rpc/clients/track_collector_rpc_client'

describe TrackCollectorRpcClient do
  before(:each) do
    @redis = double('Redis')
    conf_man = double('ConfigurationManager')
    rec_man = double('RecipeManager')
    sub_man = double('SubjectManager')

    allow(Redis).to receive(:new).and_return(@redis)
    allow(conf_man).to receive(:get)

    @tc_rpc_client = TrackCollectorRpcClient.new(conf_man, rec_man, sub_man)
    @redis_client = @tc_rpc_client.instance_variable_get(:@client)
  end

  describe 'passthrough methods' do
    it '#status' do
      expect(@redis_client).to receive(:send).with(:status)
      @tc_rpc_client.status
    end

    it '#parse_trace' do
      args = { 'trace' => "trace one\ntrace two\n", 'series' => 'test series' }
      expect(@redis_client).to receive(:send).with(:parse_trace, args)
      @tc_rpc_client.parse_trace(args)
    end

    it '#parse_helpdesk' do
      args = { 'trace' => "trace one\ntrace two\n", 'series' => 'test series' }
      expect(@redis_client).to receive(:send).with(:parse_helpdesk, args)
      @tc_rpc_client.parse_helpdesk(args)
    end
  end

  describe 'custom methods' do
    before(:each) do
      @series = build(:test_series)
    end

    it '#register_series only passes id' do
      expect(@redis_client).to receive(:register_series).with(@series.id)

      @tc_rpc_client.register_series(@series)
    end

    it '#unregister_series only passes id' do
      expect(@redis_client).to receive(:unregister_series).with(@series.id)

      @tc_rpc_client.unregister_series(@series)
    end
  end
end
