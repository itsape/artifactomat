# frozen_string_literal: true

require_relative '../system/system_spec_helper'
require 'rspec'

describe 'GfPreperator' do
  describe 'works correctly' do
    before(:all) do
      @cmd = Command.new
      some_subjects = "id,name,surname,userid,email,department\n" \
                      "1,Max,Mustermann,maxi,max@muster.man,sektion7\n" \
                      '5,Mica,Musterfrau,mica,mica@muster.fra,'
      other_subjects = "id,name,surname,userid,email,department\n" \
                       ",Mica,Musterfrau,mica,mica@muster.fra,\n" \
                       ",Max2,Mustermann2,maxi2,max2@muster.man,\n" \
                       ',Mica2,Musterfrau2,mica2,mica2@muster.fra,'
      bad_subjects = "id,name,surname,userid,department\n" \
                     ",Mica,Musterfrau,mica,mica@muster.fra,\n" \
                     ",Max2,Mustermann2,maxi2,max2@muster.man,\n" \
                     ',Mica2,Musterfrau2,mica2,mica2@muster.fra,'
      noid_subjects = "name,surname,userid,email,department\n" \
                      "Mica,Musterfrau,mica,mica@muster.fra,one depp\n" \
                      "Max2,Mustermann2,maxi2,max2@muster.man,depp2\n" \
                      'Mica2,Musterfrau2,mica2,mica2@muster.fra,3depp'

      File.write('/tmp/some.csv', some_subjects)
      File.write('/tmp/other.csv', other_subjects)
      File.write('/tmp/bad.csv', bad_subjects)
      File.write('/tmp/noid.csv', noid_subjects)
    end

    it 'and has exit code 0 on sucess' do
      @cmd.run_and_wait('bin/gf-preperator combine /tmp/some.csv /tmp/other.csv')
      expect(@cmd.stderr).to be_empty
    end

    it 'and outputs the right thing' do
      @cmd.run_and_wait('bin/gf-preperator combine /tmp/some.csv /tmp/other.csv')
      expect(@cmd.stderr).to be_empty
      expected_result = "id,name,surname,userid,email,department\n" \
                        "5,Mica,Musterfrau,mica,mica@muster.fra,\n" \
                        "2,Max2,Mustermann2,maxi2,max2@muster.man,\n" \
                        "3,Mica2,Musterfrau2,mica2,mica2@muster.fra,\n"
      expect(@cmd.stdout.join('')).to eq(expected_result)
    end

    it 'and outputs the right thing with input file sans id col' do
      @cmd.run_and_wait('bin/gf-preperator combine /tmp/some.csv /tmp/noid.csv')
      expect(@cmd.stderr).to be_empty
      expected_result = "id,name,surname,userid,email,department\n" \
                        "5,Mica,Musterfrau,mica,mica@muster.fra,one depp\n" \
                        "2,Max2,Mustermann2,maxi2,max2@muster.man,depp2\n" \
                        "3,Mica2,Musterfrau2,mica2,mica2@muster.fra,3depp\n"
      expect(@cmd.stdout.join('')).to eq(expected_result)
    end

    it 'and fails if header do not contain id and the identifier' do
      @cmd.run_and_wait_unsafe('bin/gf-preperator combine /tmp/some.csv /tmp/bad.csv')
      err_msg = ["The header of your csv file does not include 'email'.\n", "error: exit\n"]
      expect(@cmd.stderr).to match err_msg
      expect(@cmd.exit_code).to eq 1
    end
  end
end
