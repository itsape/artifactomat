# frozen_string_literal: true

require 'artifactomat/modules/infrastructure_generator'
require 'artifactomat/modules/logging'
require 'yaml'

require_relative 'mixins/script_executor_spec'

describe InfrastructureGenerator do
  before(:all) do
    @recipes = {}
    @seasons = []
    (1..3).each do |i|
      recipe = build(:recipe, id: 'Recipe' + i.to_s)
      @recipes[recipe.id] = recipe
      @seasons << create(:test_season, recipe_id: recipe.id)
    end
    @ts = create(:test_series, test_seasons: @seasons)
  end

  after(:all) do
    TestSeason.destroy_all
  end

  before(:each) do
    allow(Ape).to receive(:instance).and_raise(RuntimeError)
    @configuration_manager = double('ConfigurationManager')
    @recipe_manager = double('RecipeManager')
    @subject_manager = double('SubjectManager')
    @deployment_manager = double('DeploymentManager')
    @monitoring = double('Monitoring')
    @scheduler = double('Scheduler')

    @seasons.each do |season|
      id = season.recipe_id
      allow(@recipe_manager).to receive(:get_recipe).with(id) { @recipes[id] }
    end
    allow(@configuration_manager).to receive(:config_to_env) { {} }
    allow(@configuration_manager)
      .to receive(:get)
      .with(ScriptExecution::APE_TMP_DIR) do
        '/tmp/apt-infrastructure-spec/tmp'
      end
    allow(@configuration_manager)
      .to receive(:cfg_path) { '/tmp/apt-infrastructure-spec' }

    allow(@deployment_manager).to receive(:infrastructure_ready)
    allow(@deployment_manager).to receive(:infrastructure_generator=)
    allow(@monitoring).to receive(:unsubscribe)
    allow(Scheduler).to receive(:new).and_return(@scheduler)
    allow(@scheduler).to receive(:at)
  end

  it_behaves_like 'a script executor' do
    let(:executor) do
      @ig = InfrastructureGenerator.new(@configuration_manager,
                                        @recipe_manager,
                                        @subject_manager,
                                        @deployment_manager,
                                        @monitoring)
    end
  end

  describe '.new' do
    before(:each) do
      @ts = create(:test_series, end_date: Time.now + 100)
      @ts.reload # timestamps are rounded in the DB, reload so this object gets the rounded stamp
    end

    after(:each) do
      @ts.destroy!
    end

    it 'requires ConfigurationManager, DeploymentManager, APE and Monitoring' do
      expect do
        InfrastructureGenerator
          .new(@configuration_manager,
               @recipe_manager,
               @subject_manager,
               @deployment_manager,
               @monitoring)
      end.not_to raise_error
    end

    it 'creates destroy jobs for "prepared" series' do
      @ts.prepared!
      expect(@scheduler).to receive(:at)
        .with(@ts.end_date, tags: ['destroy infrastructure', @ts.to_str])

      InfrastructureGenerator.new(@configuration_manager,
                                  @recipe_manager,
                                  @subject_manager,
                                  @deployment_manager,
                                  @monitoring)
    end

    it 'creates destroy jobs for "approved" series' do
      @ts.approved!
      expect(@scheduler).to receive(:at)
        .with(@ts.end_date, tags: ['destroy infrastructure', @ts.to_str])

      InfrastructureGenerator.new(@configuration_manager,
                                  @recipe_manager,
                                  @subject_manager,
                                  @deployment_manager,
                                  @monitoring)
    end

    it 'does not create destroy jobs for series that are not fresh nor approved' do
      expect(@scheduler).not_to receive(:at)

      @ts.fresh!
      InfrastructureGenerator.new(@configuration_manager,
                                  @recipe_manager,
                                  @subject_manager,
                                  @deployment_manager,
                                  @monitoring)

      @ts.scheduled!
      InfrastructureGenerator.new(@configuration_manager,
                                  @recipe_manager,
                                  @subject_manager,
                                  @deployment_manager,
                                  @monitoring)

      @ts.completed!
      InfrastructureGenerator.new(@configuration_manager,
                                  @recipe_manager,
                                  @subject_manager,
                                  @deployment_manager,
                                  @monitoring)
    end
  end

  describe '#generate' do
    it 'requires a TestSeries' do
      allow(@monitoring).to receive(:infrastructure_running?) { false }

      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      expect(Logging).to receive(:info).twice
      @an_ape = double('APEdouble')
      allow(Ape).to receive(:instance).and_return(@an_ape)
      expect(@an_ape).to receive(:inform_user).twice
      status = 1
      expect { status = ig.generate(@ts) }.not_to(raise_error)
      expect(status).to eq(0)
    end

    it 'requires to deploy InfrastructureElements correctly' do
      seasons = []
      elements = []
      (1..2).each do |_i|
        elem = []
        (1..2).each do |_j|
          el = build(:infrastructure_element, generate_script: 'true')
          elem << el
          elements << el
        end
        recipe = build(:recipe, infrastructure: elem)
        seasons << create(:test_season, recipe_id: recipe.id)
        allow(@recipe_manager)
          .to receive(:get_recipe).with(recipe.id) { recipe }
      end
      seasons.each do |season|
        allow(TestSeason).to receive(:find).with(season.id).and_return(season)
      end
      expect(Logging).to receive(:info).exactly(6).times
      @an_ape = double('APEdouble')
      allow(Ape).to receive(:instance).and_return(@an_ape)
      expect(@an_ape).to receive(:inform_user).twice
      allow(@monitoring).to receive(:infrastructure_running?) { false }
      allow(@deployment_manager).to receive(:deploy_infrastructure)
      ts = create(:test_series, test_seasons: seasons)
      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      expect(ig).to receive(:add_routing_information).exactly(4).times
      status = ig.generate(ts)
      expect(status).to eq(0)
      expect(@deployment_manager)
        .to have_received(:deploy_infrastructure).exactly(4).times
    end

    it 'creates a destroy job for the series' do
      without_bullet do
        TestSeries.destroy_all
        TestSeason.destroy_all
        TestSeason.destroy_all
        TestEpisode.destroy_all
      end

      elem = build(:infrastructure_element, generate_script: 'true')
      recipe = build(:recipe, infrastructure: [elem])
      season = create(:test_season, recipe_id: recipe.id)
      allow(@recipe_manager).to receive(:get_recipe).with(recipe.id).and_return(recipe)
      allow(TestSeason).to receive(:find).with(season.id).and_return(season)
      ts = create(:test_series, season_count: 0, test_seasons: [season], end_date: Time.now + 1000)
      ts.reload # get rounded timestamp from db

      allow(Logging).to receive(:info)
      @an_ape = double('APEdouble')
      allow(Ape).to receive(:instance).and_return(@an_ape)
      allow(@an_ape).to receive(:inform_user)
      allow(@monitoring).to receive(:infrastructure_running?).and_return(false)
      allow(@deployment_manager).to receive(:deploy_infrastructure)
      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      allow(ig).to receive(:add_routing_information)

      expect(@scheduler).to receive(:at).with(ts.end_date, tags: ['destroy infrastructure', ts.to_str])
      ig.generate(ts)
    end

    it 'raises an InfrastructureGenerationError if the command is unknown' do
      elem = build(:infrastructure_element, generate_script: 'NoSuchCommand')
      recipe = build(:recipe, infrastructure: [elem])
      season = create(:test_season, recipe_id: recipe.id)
      allow(TestSeason).to receive(:find).with(season.id).and_return(season)
      allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
      ts = create(:test_series, test_seasons: [season])
      allow(@monitoring).to receive(:infrastructure_running?) { false }
      expect(Logging).to receive(:info).once
      @an_ape = double('APEdouble')
      allow(Ape).to receive(:instance).and_return(@an_ape)
      expect(@an_ape).to receive(:inform_user)
      allow_any_instance_of(ScriptExecution).to receive(:execute_script).and_raise(InvalidCommandError)
      allow_any_instance_of(ScriptExecution).to receive(:season_to_env).and_return({})

      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      expect { ig.generate(ts) }.to raise_error(InfrastructureGenerationError)
    end

    it 'raises an InfrastructureGenerationError if the command returns != 0' do
      expect(Logging).to receive(:error).once
      elem = build(:infrastructure_element, generate_script: 'false')
      recipe = build(:recipe, infrastructure: [elem])
      season = create(:test_season, recipe_id: recipe.id)
      allow(TestSeason).to receive(:find).with(season.id).and_return(season)
      allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
      ts = create(:test_series, test_seasons: [season])
      allow(@monitoring).to receive(:infrastructure_running?) { false }
      expect(Logging).to receive(:info).once
      @an_ape = double('APEdouble')
      allow(Ape).to receive(:instance).and_return(@an_ape)
      expect(@an_ape).to receive(:inform_user)
      allow_any_instance_of(ScriptExecution).to receive(:execute_script).and_return(-1, nil)
      allow_any_instance_of(ScriptExecution).to receive(:season_to_env).and_return({})

      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      expect { ig.generate(ts) }.to raise_error(InfrastructureGenerationError)
    end

    it 'raises an InfrastructureRunningError if infrastructure is running' do
      expect(Logging).to receive(:error).once
      expect(Logging).to receive(:info).once
      ts = create(:test_series, season_count: 0)
      elem = build(:infrastructure_element, generate_script: 'false')
      recipe = build(:recipe, infrastructure: [elem])
      create(:test_season, recipe_id: recipe.id, test_series: ts)
      allow(@recipe_manager).to receive(:get_recipe).and_return(recipe)

      allow(@monitoring).to receive(:infrastructure_running?).and_return(true)

      @an_ape = double('APEdouble')
      allow(Ape).to receive(:instance).and_return(@an_ape)
      expect(@an_ape).to receive(:inform_user).twice

      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      expect { ig.generate(ts) }.to raise_error(InfrastructureRunningError)
    end
  end

  it 'rolls back already generated infrastructure in case of an error' do
    elements = []

    (1..3).each do |i|
      el = build(:infrastructure_element, generate_script: i == 3 ? 'false' : 'true')
      elements << el
    end
    recipe = build(:recipe, infrastructure: elements)
    season = create(:test_season, recipe_id: recipe.id)
    allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }

    expect(Logging).to receive(:info).exactly(3).times
    expect(Logging).to receive(:error).once
    @an_ape = double('APEdouble')
    allow(Ape).to receive(:instance).and_return(@an_ape)
    expect(@an_ape).to receive(:inform_user).once
    allow(@monitoring).to receive(:infrastructure_running?) { false }
    allow(@deployment_manager).to receive(:deploy_infrastructure)
    ts = create(:test_series, test_seasons: [season])
    ig = InfrastructureGenerator.new(@configuration_manager,
                                     @recipe_manager,
                                     @subject_manager,
                                     @deployment_manager,
                                     @monitoring)
    allow(TestSeason).to receive(:find).with(season.id).and_return(season)
    expect(ig).to receive(:add_routing_information).exactly(3).times
    expect(ig).to receive(:destroy_element).exactly(2).times
    expect { ig.generate(ts) }.to raise_error(InfrastructureGenerationError)
    expect(@deployment_manager)
      .to have_received(:deploy_infrastructure).exactly(2).times
  end

  describe '#execute_script' do
    it 'raises no error on correct execution' do
      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      expect { ig.execute_script('true') }.not_to raise_error
    end

    it 'status is 0 on correct execution' do
      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      status, = ig.execute_script('true')
      expect(status).to eql(0)
    end

    it 'raises InvalidCommandError if command is unknown' do
      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      expect { ig.execute_script('NoSuchCommand') }
        .to raise_error(InvalidCommandError)
    end

    it 'raises no error on script execution error' do
      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      expect { ig.execute_script('false') }.not_to raise_error
    end

    it 'status is not 0 on script execution error' do
      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      status, = ig.execute_script('false')
      expect(status).not_to eql(0)
    end
  end

  describe '#gather_elements' do
    it 'returns an array' do
      ts = create(:test_series, season_count: 0)
      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      elem = ig.gather_elements(ts)
      expect(elem).to be_a(Array)
    end

    it 'returns an empty array if no season exist' do
      ts = create(:test_series, season_count: 0)
      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)
      elem = ig.gather_elements(ts)
      expect(elem.empty?).to be true
    end

    it 'returns a flattend array' do
      seasons = []
      elements = []
      (1..2).each do |_i|
        elem = []
        (1..2).each do |_j|
          el = build(:infrastructure_element, generate_script: 'true')
          elem << el
          elements << el
        end
        recipe = build(:recipe, infrastructure: elem)
        seasons << create(:test_season, recipe_id: recipe.id)
        allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
      end

      ts = create(:test_series, test_seasons: seasons)
      ig = InfrastructureGenerator.new(@configuration_manager,
                                       @recipe_manager,
                                       @subject_manager,
                                       @deployment_manager,
                                       @monitoring)

      expect(ig.gather_elements(ts)).to match_array(elements)
    end
  end

  context '#destroy' do
    describe 'erronous destroy script' do
      before(:all) do
        elem = build(:infrastructure_element, destroy_script: 'false')
        @recipe = build(:recipe, infrastructure: [elem])
        @season = create(:test_season, recipe_id: @recipe.id)
        @ts_script_error = create(:test_series, test_seasons: [@season])
      end

      before(:each) do
        allow(@recipe_manager).to receive(:get_recipe).with(@recipe.id) { @recipe }
        allow(@monitoring).to receive(:infrastructure_running?) { true }

        @routing = double('RoutingDouble_IG')
        allow(@routing).to receive(:get_internal_ports).with(@season).and_return([5, 6])
        allow(@routing).to receive(:get_external_address).with(@season).and_return('127.0.0.1')

        @an_ape = double('APEdouble_IG')
        allow(Ape).to receive(:instance).and_return(@an_ape)
        allow(@an_ape).to receive(:routing).and_return(@routing)

        @ig = InfrastructureGenerator.new(@configuration_manager,
                                          @recipe_manager,
                                          @subject_manager,
                                          @deployment_manager,
                                          @monitoring)
      end

      it 'requires to log errors on script execution errors' do
        expect(Logging).to receive(:error).once
        @ig.destroy(@ts_script_error)
      end

      it 'does not raise an error erronous scripts' do
        expect(Logging).to receive(:error).once
        expect { @ig.destroy(@ts_script_error) }.not_to raise_error
      end

      it 'tells monitoring to unsubscribe the infrastructure elements' do
        allow(Logging).to receive(:error).once
        expect(@monitoring).to receive(:unsubscribe).once
        @ig.destroy(@ts_script_error)
      end
    end

    describe 'working destroy script' do
      before(:all) do
        elem = build(:infrastructure_element, destroy_script: 'true')
        @recipe = build(:recipe, infrastructure: [elem])
        season = create(:test_season, recipe_id: @recipe.id)
        @ts = create(:test_series, test_seasons: [season])
      end

      before(:each) do
        allow(@recipe_manager).to receive(:get_recipe).with(@recipe.id) { @recipe }
        allow(@monitoring).to receive(:infrastructure_running?) { false }

        @an_ape = double('APEdouble')
        allow(Ape).to receive(:instance).and_return(@an_ape)

        @ig = InfrastructureGenerator.new(@configuration_manager,
                                          @recipe_manager,
                                          @subject_manager,
                                          @deployment_manager,
                                          @monitoring)
      end

      it 'informs the user about not running infrastructure' do
        expect(Logging).to receive(:warning).once
        @ig.destroy(@ts)
      end

      it 'tells monitoring to unsubscribe the infrastructure elements' do
        allow(Logging).to receive(:warning).once
        expect(@monitoring).to receive(:unsubscribe).once
        @ig.destroy(@ts)
      end
    end
  end

  it 'unschedules jobs' do
    ig = InfrastructureGenerator.new(@configuration_manager,
                                     @recipe_manager,
                                     @subject_manager,
                                     @deployment_manager,
                                     @monitoring)
    series = build(:test_series)
    jobs = [double('job1'), double('job2')]

    jobs.each do |job|
      expect(job).to receive(:unschedule)
    end
    expect(@scheduler).to receive(:jobs).with(tag: series.to_str).and_return(jobs)

    ig.unschedule_jobs(series)
  end
end
