# frozen_string_literal: true

require 'artifactomat/volatile_store'

class MockAction
  attr_reader :subject_id, :ip

  def initialize(subject_id = '', ip = '')
    @subject_id = subject_id
    @ip = ip
  end
end

describe VolatileStore do
  before(:each) do
    @redis = double('Redis')
    @conn = double('Connection')
    @conf_man = double('ConfigurationManager')

    @db_active_names = 0
    @db_subject_ips = 1
    @db_local_variables = 2
    @db_news = 3
    @ch_subject_tracker_updates = 'subjecttracker_updates'

    allow(ConnectionPool).to receive(:new).and_return(@redis)
    allow(@redis).to receive(:with).and_yield(@conn)

    @vstore = VolatileStore.new(@conf_man)
  end

  it 'creates a redis connection on initialization' do
    allow(@conf_man).to receive(:get).and_return('/var/run/redis-artifactomat/redis-server.sock')
    expect(ConnectionPool).to receive(:new).with(size: 100, timeout: 2).and_yield
    expect(Redis).to receive(:new).with(path: '/var/run/redis-artifactomat/redis-server.sock')

    VolatileStore.new(@conf_man, 100, 2)
  end

  it '#with_state_saving_connection yields connection to local variable database' do
    expect(@conn).to receive(:select).with(@db_local_variables)
    @vstore.with_state_saving_connection do |conn|
      expect(conn).to be(@conn)
    end
  end

  describe '#init_news' do
    it 'creates empty news on redis if they don not already exist' do
      allow(@conn).to receive(:get).with('news').and_return(nil)

      expect(@conn).to receive(:select).with(@db_news)
      expect(@conn).to receive(:set).with('news', '[]')

      @vstore.init_news
    end

    it 'does not change news if they already exist' do
      allow(@conn).to receive(:get).with('news').and_return('["the news"]')
      expect(@conn).to receive(:select).with(@db_news)
      expect(@conn).not_to receive(:set)

      @vstore.init_news
    end
  end

  it '#push_news appends message to existing news' do
    allow(@conn).to receive(:get).with('news').and_return('["old news"]')

    expect(@conn).to receive(:select).with(@db_news)
    expect(@conn).to receive(:set).with('news', '["old news","new news"]')

    @vstore.push_news('new news')
  end

  describe '#fetch_and_clear_news' do
    it 'returns old news' do
      allow(@conn).to receive(:get).with('news').and_return('["old news"]')
      allow(@conn).to receive(:set)

      expect(@conn).to receive(:select).with(@db_news)
      expect(@vstore.fetch_and_clear_news).to eq(['old news'])
    end

    it 'sets empty news on redis' do
      allow(@conn).to receive(:get).with('news').and_return('["old news"]')

      expect(@conn).to receive(:select).with(@db_news)
      expect(@conn).to receive(:set).with('news', '[]')

      @vstore.fetch_and_clear_news
    end
  end

  describe '#ip_by_sid' do
    it 'returns the IP for a given SID' do
      expect(@conn).to receive(:select).with(@db_subject_ips)
      expect(@conn).to receive(:get).with('subjects.csv/1').and_return('192.168.0.4')

      expect(@vstore.ip_by_sid('subjects.csv/1')).to eq('192.168.0.4')
    end

    it 'returns nil on error' do
      allow(@conn).to receive(:select).with(@db_subject_ips)
      expect(@conn).to receive(:get).with('subjects.csv/1').and_raise(StandardError)

      expect(@vstore.ip_by_sid('subjects.csv/1')).to be_nil
    end
  end

  describe '#sids_by_ip' do
    it 'returns the SIDs for a given IP' do
      expect(@conn).to receive(:select).with(@db_subject_ips)
      expect(@conn).to receive(:get).with('192.168.0.5').and_return('["subjects.csv/2", "subjects.csv/3"]')

      expect(@vstore.sids_by_ip('192.168.0.5')).to eq(['subjects.csv/2', 'subjects.csv/3'])
    end

    it 'returns an empty array on error' do
      allow(@conn).to receive(:select).with(@db_subject_ips)
      expect(@conn).to receive(:get).with('192.168.0.5').and_raise(StandardError)

      expect(@vstore.sids_by_ip('192.168.0.5')).to eq([])
    end
  end

  describe '#remove_from_ip_subject_map' do
    before(:each) do
      @sid = 'subjects.csv/1'
      @ip = '192.168.0.42'
      @action = MockAction.new(@sid)
    end

    it 'logs on error' do
      allow(@conn).to receive(:select).and_raise(StandardError)

      expect(Logging).to receive(:error).with('SubjectTracker', %r{can not get ip by subject\.id subjects\.csv/1})

      @vstore.remove_from_ip_subject_map(@action)
    end

    context 'SID exists' do
      before(:each) do
        allow(@conn).to receive(:get).with(@sid).and_return(@ip)
      end

      it 'removes the IP for the given SID' do
        # the DB map has both, an `SID => IP` and an `IP => SID(s)` entry. This testcase tests that
        # the `SID => IP` entry gets deleted
        allow(@conn).to receive(:publish)
        allow(@conn).to receive(:get).with(@ip).and_return(nil)
        expect(@conn).to receive(:select).with(@db_subject_ips)
        expect(@conn).to receive(:del).with('subjects.csv/1')

        @vstore.remove_from_ip_subject_map(@action)
      end

      it 'announces the deletion of the IP <=> SID pair to the SUBJECT_TRACKER_UPDATES channel' do
        expected_msg = { action: 'del', data: { ip: @ip, sid: @sid } }.to_json

        allow(@conn).to receive(:get).with(@ip).and_return(nil)
        allow(@conn).to receive(:del).with('subjects.csv/1')
        allow(@conn).to receive(:select).with(@db_subject_ips)
        expect(@conn).to receive(:publish).with(@ch_subject_tracker_updates, expected_msg)

        @vstore.remove_from_ip_subject_map(@action)
      end

      describe 'removes the SID from the SID array for the subjects IP' do
        # the DB map has both, an `SID => IP` and an `IP => SID(s)` entry. These testcases test that
        # the `IP => SID(s)` entry gets deleted or updated
        before(:each) do
          allow(@conn).to receive(:get).with(@sid).and_return(@ip)
          allow(@conn).to receive(:del).with('subjects.csv/1')
          allow(@conn).to receive(:publish)
        end

        it 'deletes the array for the IP if it is empty after deletion' do
          expect(@conn).to receive(:select).with(@db_subject_ips)
          expect(@conn).to receive(:get).with(@ip).and_return('["subjects.csv/1"]')
          expect(@conn).to receive(:del).with(@ip)

          @vstore.remove_from_ip_subject_map(@action)
        end

        it 'updates the array for the IP if it is not empty after deletion' do
          expect(@conn).to receive(:select).with(@db_subject_ips)
          expect(@conn).to receive(:get).with(@ip).and_return('["subjects.csv/1","subjects.csv/2"]')
          expect(@conn).not_to receive(:del).with(@ip)
          expect(@conn).to receive(:set).with(@ip, '["subjects.csv/2"]')

          @vstore.remove_from_ip_subject_map(@action)
        end
      end
    end

    context 'SID does not exist' do
      before(:each) do
        allow(@conn).to receive(:get).with(@sid).and_return(nil)
      end

      it 'does not change anything in the DB' do
        allow(@conn).to receive(:select).with(@db_subject_ips)
        expect(@conn).not_to receive(:set)
        expect(@conn).not_to receive(:del)

        @vstore.remove_from_ip_subject_map(@action)
      end

      it 'does not publish any updates' do
        allow(@conn).to receive(:select).with(@db_subject_ips)
        expect(@conn).not_to receive(:publish)

        @vstore.remove_from_ip_subject_map(@action)
      end

      it 'does not crash' do
        allow(@conn).to receive(:select).with(@db_subject_ips)
        expect { @vstore.remove_from_ip_subject_map(@action) }.not_to raise_error
      end
    end
  end

  describe '#update_ip_subject_map' do
    before(:each) do
      @sid = 'subjects.csv/1'
      @old_ip = '192.168.0.42'
      @new_ip = '10.0.0.5'
      @action = MockAction.new(@sid, @new_ip)
    end

    describe 'removes the old entries' do
      before(:each) do
        # mock addition of new entries
        allow(@conn).to receive(:select).with(@db_subject_ips)
        allow(@conn).to receive(:set).with(@sid, @new_ip)
        allow(@conn).to receive(:get).with(@new_ip).and_return(nil)
        allow(@conn).to receive(:set).with(@new_ip, '["subjects.csv/1"]')
        allow(@conn).to receive(:publish)
        allow(Logging).to receive(:error)
      end

      it 'logs on error' do
        allow(@conn).to receive(:select).and_raise(StandardError)

        expect(Logging).to receive(:error).with('SubjectTracker', %r{can not get ip by subject\.id subjects\.csv/1})

        @vstore.update_ip_subject_map(@action)
      end

      context 'SID exists' do
        before(:each) do
          allow(@conn).to receive(:get).with(@sid).and_return(@old_ip)
        end

        it 'removes the IP for the given SID' do
          # the DB map has both, an `SID => IP` and an `IP => SID(s)` entry. This testcase tests that
          # the `SID => IP` entry gets deleted
          allow(@conn).to receive(:publish)
          allow(@conn).to receive(:get).with(@old_ip).and_return(nil)
          expect(@conn).to receive(:select).with(@db_subject_ips)
          expect(@conn).to receive(:del).with('subjects.csv/1')

          @vstore.update_ip_subject_map(@action)
        end

        it 'announces the deletion of the IP <=> SID pair to the SUBJECT_TRACKER_UPDATES channel' do
          expected_msg = { action: 'del', data: { ip: @old_ip, sid: @sid } }.to_json

          allow(@conn).to receive(:get).with(@old_ip).and_return(nil)
          allow(@conn).to receive(:del).with('subjects.csv/1')
          allow(@conn).to receive(:select).with(@db_subject_ips)
          expect(@conn).to receive(:publish).with(@ch_subject_tracker_updates, expected_msg)

          @vstore.update_ip_subject_map(@action)
        end

        describe 'removes the SID from the SID array for the subjects IP' do
          # the DB map has both, an `SID => IP` and an `IP => SID(s)` entry. These testcases test that
          # the `IP => SID(s)` entry gets deleted or updated
          before(:each) do
            allow(@conn).to receive(:get).with(@sid).and_return(@old_ip)
            allow(@conn).to receive(:del).with('subjects.csv/1')
            allow(@conn).to receive(:publish)
          end

          it 'deletes the array for the IP if it is empty after deletion' do
            expect(@conn).to receive(:select).with(@db_subject_ips)
            expect(@conn).to receive(:get).with(@old_ip).and_return('["subjects.csv/1"]')
            expect(@conn).to receive(:del).with(@old_ip)

            @vstore.update_ip_subject_map(@action)
          end

          it 'updates the array for the IP if it is not empty after deletion' do
            expect(@conn).to receive(:select).with(@db_subject_ips)
            expect(@conn).to receive(:get).with(@old_ip).and_return('["subjects.csv/1","subjects.csv/2"]')
            expect(@conn).not_to receive(:del).with(@old_ip)
            expect(@conn).to receive(:set).with(@old_ip, '["subjects.csv/2"]')

            @vstore.update_ip_subject_map(@action)
          end
        end
      end

      context 'SID does not exist' do
        before(:each) do
          allow(@conn).to receive(:get).with(@sid).and_return(nil)
        end

        it 'does not crash' do
          allow(@conn).to receive(:select).with(@db_subject_ips)
          expect { @vstore.update_ip_subject_map(@action) }.not_to raise_error
        end
      end
    end

    describe 'adds new entries' do
      before(:each) do
        # mock removal of old entries
        allow(@conn).to receive(:select)
        allow(@conn).to receive(:get)
        allow(@conn).to receive(:set)
        allow(@conn).to receive(:del)
        allow(@conn).to receive(:publish)
        allow(Logging).to receive(:error)
      end

      it 'logs on error' do
        allow(@conn).to receive(:select).and_raise(StandardError)

        expect(Logging).to receive(:error).with('SubjectTracker', /can not save/)

        @vstore.update_ip_subject_map(@action)
      end

      it 'adds the IP for the given SID' do
        expect(@conn).to receive(:select).with(@db_subject_ips)
        expect(@conn).to receive(:set).with(@sid, @new_ip)

        @vstore.update_ip_subject_map(@action)
      end

      it 'announces the addition of the new IP <=> SID pair to the SUBJECT_TRACKER_UPDATES channel' do
        expected = { action: 'add', data: { ip: @new_ip, sid: @sid } }.to_json
        expect(@conn).to receive(:publish).with(@ch_subject_tracker_updates, expected)
        expect(@conn).to receive(:select).with(@db_subject_ips)

        @vstore.update_ip_subject_map(@action)
      end

      it 'adds the SID to the SID array for the subjects IP' do
        expect(@conn).to receive(:select).with(@db_subject_ips)
        expect(@conn).to receive(:get).with(@new_ip).and_return('["subjects.csv/5"]')
        expect(@conn).to receive(:set).with(@new_ip, '["subjects.csv/5","subjects.csv/1"]')

        @vstore.update_ip_subject_map(@action)
      end
    end
  end

  describe '#count_subject_map' do
    it 'counts all the keys with a `/` in them' do
      expect(@conn).to receive(:select).with(@db_subject_ips)
      expect(@conn).to receive(:scan_each).with(match: '*/*').and_yield.and_yield.and_yield
      expect(@vstore.count_subject_map).to eq(3)
    end

    it 'returns intermediate count on error' do
      allow(@conn).to receive(:select).with(@db_subject_ips)
      allow(@conn).to receive(:scan_each).with(match: '*/*')
                                         .and_yield\
                                         .and_yield\
                                         .and_raise(StandardError)
      expect(@vstore.count_subject_map).to eq(2)
    end
  end

  describe '#subjects_from_map' do
    it 'returns all the subjects from the SID <=> IP map' do
      expect(@conn).to receive(:select).with(@db_subject_ips)
      expect(@conn).to receive(:scan_each).with(match: '*/*')\
                                          .and_yield('subjects.csv/1')\
                                          .and_yield('subjects.csv/2')\
                                          .and_yield('subjects.csv/3')
      expect(@vstore.subjects_from_map).to eq(['subjects.csv/1', 'subjects.csv/2', 'subjects.csv/3'])
    end

    it 'returns an empty array on error' do
      allow(@conn).to receive(:select).with(@db_subject_ips)
      allow(@conn).to receive(:scan_each).with(match: '*/*')\
                                         .and_yield('subjects.csv/1')\
                                         .and_yield('subjects.csv/2')\
                                         .and_raise(StandardError)
      expect(@vstore.subjects_from_map).to eq([])
    end
  end

  describe '#reset' do
    it 'deletes all records from redis' do
      allow(@conn).to receive(:select)
      allow(@conn).to receive(:set)
      allow(@conn).to receive(:get)
      expect(@conn).to receive(:flushall)

      @vstore.reset
    end

    it 'sets news to an empty array' do
      allow(@conn).to receive(:flushall)
      expect(@conn).to receive(:get).with('news').and_return(nil)
      expect(@conn).to receive(:select).with(@db_news)
      expect(@conn).to receive(:set).with('news', '[]')

      @vstore.reset
    end
  end

  describe '#subscribe_to_subjectracker_updates' do
    it 'yields messages from the SUBJECT_TRACKER_UPDATES channel' do
      @channel = double('Channel')
      allow(@channel).to receive(:message)\
        .and_yield(@channel, '{"data":"something"}').and_yield(@channel, '{"data":"something else"}')
      expect(@conn).to receive(:subscribe).with(@ch_subject_tracker_updates).and_yield(@channel)

      yielded_msgs = []
      @vstore.subscribe_to_subjectracker_updates do |msg|
        yielded_msgs << msg
      end

      expected = [{ 'data' => 'something' }, { 'data' => 'something else' }]
      expect(yielded_msgs).to eq(expected)
    end
  end

  describe '#get_registered_subjects' do
    it 'returns all subjects with active DNS redirections for a given name' do
      expect(@conn).to receive(:select).with(@db_active_names).twice
      allow(@conn).to receive(:smembers).with('google.de').and_return(['subjects.csv/1', 'subjects.csv/2'])
      allow(@conn).to receive(:smembers).with('duckduckgo.com').and_return(['subjects.csv/3', 'subjects.csv/4'])

      expect(@vstore.get_registered_subjects('google.de')).to eq(['subjects.csv/1', 'subjects.csv/2'])
      expect(@vstore.get_registered_subjects('duckduckgo.com')).to eq(['subjects.csv/3', 'subjects.csv/4'])
    end

    it 'returns empty array on error' do
      allow(@conn).to receive(:select).and_raise(StandardError)
      expect(@vstore.get_registered_subjects('google.de')).to eq([])
    end
  end

  describe '#register_subject' do
    it 'adds a subject to the set of active subjects for a given domain name' do
      expect(@conn).to receive(:select).with(@db_active_names)
      expect(@conn).to receive(:sadd).with('google.de', 'subjects.csv/1').and_return(true)

      @vstore.register_subject('google.de', 'subjects.csv/1')
    end

    it 'returns whether adding the subject was successful' do
      allow(@conn).to receive(:select).with(@db_active_names)
      allow(@conn).to receive(:sadd).with('google.de', 'subjects.csv/1').and_return(true)

      expect(@vstore.register_subject('google.de', 'subjects.csv/1')).to be_truthy

      allow(@conn).to receive(:select).with(@db_active_names)
      allow(@conn).to receive(:sadd).with('google.de', 'subjects.csv/1').and_return(false)

      expect(@vstore.register_subject('google.de', 'subjects.csv/1')).to be_falsy
    end

    it 'returns false on error' do
      allow(@conn).to receive(:select).and_raise(StandardError)

      expect(@vstore.register_subject('google.de', 'subjects.csv/1')).to be_falsy
    end
  end

  describe '#unregister_subject' do
    it 'adds a subject to the set of active subjects for a given domain name' do
      expect(@conn).to receive(:select).with(@db_active_names)
      expect(@conn).to receive(:srem).with('google.de', 'subjects.csv/1').and_return(true)

      @vstore.unregister_subject('google.de', 'subjects.csv/1')
    end

    it 'returns whether adding the subject was successful' do
      allow(@conn).to receive(:select).with(@db_active_names)
      allow(@conn).to receive(:srem).with('google.de', 'subjects.csv/1').and_return(true)

      expect(@vstore.unregister_subject('google.de', 'subjects.csv/1')).to be_truthy

      allow(@conn).to receive(:select).with(@db_active_names)
      allow(@conn).to receive(:srem).with('google.de', 'subjects.csv/1').and_return(false)

      expect(@vstore.unregister_subject('google.de', 'subjects.csv/1')).to be_falsy
    end

    it 'returns false on error' do
      allow(@conn).to receive(:select).and_raise(StandardError)

      expect(@vstore.unregister_subject('google.de', 'subjects.csv/1')).to be_falsy
    end
  end
end
