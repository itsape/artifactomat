# frozen_string_literal: true

require 'open3'
require 'socket'
require 'fileutils'
require 'timeout'

ETH0 = 'enp0s3'

class CommandError < RuntimeError
end

class SpecLogger
  def initialize(spec_name)
    @path = "log/#{spec_name}.log"
  end

  def log(text)
    File.open(@path, 'a') do |file|
      file.puts text
    end
  end
end

def build_tf(recipe)
  tf = TrackFilter.new
  tf.course_of_action = 'phishing link clicked'
  tf.extraction_pattern = [/\[(?<timestamp>[0-9]+)\] link clicked by (?<subject>.+) in (?<test_episode_id>[0-9]+)$/]
  tf.pattern = /(.+)/
  tf.recipe_id = recipe.id
  tf.score = 42
  tf2 = TrackFilter.new
  tf2.course_of_action = 'data filled'
  tf2.extraction_pattern = [/\[(?<timestamp>[0-9]+)\] data filled in by (?<subject>.+) in (?<test_episode_id>[0-9]+)$/]
  tf2.pattern = /(.+)/
  tf2.recipe_id = recipe.id
  tf2.score = 31
  [tf, tf2]
end

# rubocop:disable Metrics/AbcSize
def make_https(request, data = nil)
  cert_path = '/etc/artifactomat/certs'
  Net::HTTP.start(
    request.uri.hostname, request.uri.port,
    use_ssl: request.uri.scheme == 'https',
    cert: OpenSSL::X509::Certificate.new(
      File.open(File.join(cert_path, 'public/client.itsape.pem')).read
    ),
    key: OpenSSL::PKey::RSA.new(
      File.open(File.join(cert_path, 'private/client.itsape.key')).read
    ),
    ca_file: File.join(cert_path, 'public/ca.pem'),
    verify_depth: 5,
    verify_mode: OpenSSL::SSL::VERIFY_NONE # OpenSSL::SSL::VERIFY_PEER | OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT,
  ) do |http|
    request.body = data.to_json unless data.nil?
    http.request(request)
  end
end
# rubocop:enable Metrics/AbcSize

def tcp_port_open?(ip, port)
  Socket.tcp(ip.to_s, port, connect_timeout: 1) { true }
rescue StandardError
  false
end

def derive_ip
  cmd = Command.new
  cmd.run_and_wait("(ip addr | awk '/inet/ && /#{ETH0}/{sub(/\\/.*$/,\"\",$2); print $2}')")
  cmd.stdout.first.delete("\n").to_s
end

def kill_by_ppid(ppid, signal = nil)
  pid = `pgrep -P #{ppid}`.to_i
  return if pid.zero?

  signal_part = signal ? " using signal #{signal}" : ''
  puts("Terminate running child process on pid #{pid}#{signal_part}.")
  if signal
    @cmd.run_and_wait("sudo kill -#{signal} #{pid}")
  else
    @cmd.run_and_wait("sudo kill #{pid}")
  end
end

def sleep_until(time)
  sleep 0.1 until time < Time.now
end

# Class to make handling of commands easier
class Command
  attr_accessor :pid
  attr_reader :started, :stdout, :stderr

  def initialize(cmd = '')
    @wait_thread = (Thread.new {}).join
    @pid = 0
    @cmd = cmd
    @started = false
    @stdout_mutex = Mutex.new
    @stdout_news = [] # gets all new stdout lines, but does not guarantee to always hold all stdout of the command
    @stdout = []
    @stderr = []
    run @cmd unless @cmd.empty?
  end

  def run(cmd = '')
    @cmd = cmd
    return 0 if @cmd.empty?

    @stdout_news.clear
    @stdout.clear
    @stderr.clear

    stdin, stdout, stderr, wait_thread = Open3.popen3(cmd)
    stdin.close
    @wait_thread = wait_thread
    @pid = @wait_thread[:pid]
    @started = true
    monitor_streams(stdout, stderr)

    @pid
  end

  def run_and_wait(cmd, timeout = 20)
    exit_code = run_and_wait_unsafe(cmd, timeout)
    raise CommandError, "Command #{cmd} failed with exit code #{exit_code}.\n#{pretty_output}" unless exit_code == 0

    exit_code
  end

  def run_and_wait_unsafe(cmd, timeout = 20)
    error_msg = "TIMEOUT ARTER #{timeout} SECONDS: #{cmd}"
    Timeout.timeout(timeout, Timeout::Error, error_msg) do
      run(cmd)
      sleep 0.1 while exit_code.nil?
      return exit_code
    end
  end

  def each_new_stdout_line
    @stdout_mutex.synchronize do
      @stdout_news.each do |line|
        yield line
      end
      @stdout_news.clear
    end
  end

  def clear_stdout_news
    @stdout_mutex.synchronize do
      @stdout_news.clear
    end
  end

  def escalate_error
    # stderr match catches thread exceptions, even if the main thread is still alive
    return if @wait_thread.alive? && !@stderr.join('').match?(/terminated with exception/)

    puts pretty_output
    raise "FAILED TO RUN: #{@cmd}"
  end

  def wait_for_stdout_output(regexp, timeout = 5)
    wait_for_output(@stdout, regexp, timeout)
  end

  def wait_for_stderr_output(regexp, timeout = 5)
    wait_for_output(@stderr, regexp, timeout)
  end

  def pretty_stdout
    return "!! stdout of \"#{@cmd}\" empty.\n\n" if @stdout.empty?

    ret = 'stdout: ------- "' + @cmd + "\" ------ :start\n> "
    ret += @stdout.join('> ')
    ret + 'stdout: ------- "' + @cmd + "\" ------ :end\n\n"
  end

  def pretty_stderr
    return "!! stderr of \"#{@cmd}\" empty.\n\n" if @stderr.empty?

    ret = 'stderr: ------- ' + @cmd + " ------ :start\n! "
    ret += @stderr.join('! ')
    ret + 'stderr: ------- ' + @cmd + " ------ :end\n\n"
  end

  def pretty_output
    "#{pretty_stdout}#{pretty_stderr}"
  end

  def wait(max = nil)
    @wait_thread.join(max)
  end

  def kill
    return true unless @wait_thread.alive?

    Process.kill('SIGTERM', @wait_thread[:pid])
  end

  def sigint
    return true unless @wait_thread.alive?

    Process.kill('SIGINT', @wait_thread[:pid])
  end

  def exit_code
    return nil if @wait_thread.alive?

    # (@wait_thread.value.to_s).match(/\d+$/)[0].to_i
    /\d+$/.match(@wait_thread.value.to_s)[0].to_i
  end

  def running?
    @wait_thread.alive?
  end

  private

  def wait_for_output(output, regexp, timeout)
    error_msg = "TIMEOUT AFTER #{timeout} SECONDS: #{@cmd}, while waiting for #{regexp}"
    Timeout.timeout(timeout, Timeout::Error, error_msg) do
      loop do
        break unless output.filter { |l| l.match(regexp) }.empty?

        sleep(0.1)
      end
    end
  end

  def monitor_streams(stdout, stderr)
    { out: stdout, err: stderr }.each do |key, stream|
      Thread.new do
        until (raw_line = stream.gets).nil?
          @stdout_mutex.synchronize do
            @stdout_news.push(raw_line) if key == :out
          end
          @stdout.push(raw_line) if key == :out
          @stderr.push(raw_line) if key == :err
        end
        stream.close
      end
    end
  end
end

class SystemdService
  attr_reader :service_name
  def initialize(service_name)
    @service_name = service_name
    @cmd = Command.new
    @old_lines = []
  end

  def start
    @cmd.run_and_wait("sudo systemctl start #{@service_name}")
    clear_news
  end

  def stop
    @cmd.run_and_wait("sudo systemctl stop #{@service_name}")
  end

  def kill
    @cmd.run_and_wait("sudo systemctl kill -s SIGKILL #{@service_name}")
  end

  def restart
    @cmd.run_and_wait("sudo systemctl restart #{@service_name}")
    clear_news
  end

  def active?
    @cmd.run_and_wait_unsafe("sudo systemctl is-active #{@service_name}")
    @cmd.exit_code == 0
  end

  def status
    "systemctl status: ------- #{@service_name} ------- :start\n" \
    "#{journalctl_output}" \
    "systemctl status: ------- #{@service_name} ------- :end\n"
  end

  def pid
    @cmd.run_and_wait("systemctl show --value -p MainPID #{@service_name}")
    @cmd.stdout.join('')
  end

  def invocation_id
    @cmd.run_and_wait("systemctl show --value -p InvocationID #{@service_name}")
    @cmd.stdout.join('')
  end

  def wait_for_output(regexp, timeout = 5)
    error_msg = "TIMEOUT AFTER #{timeout} SECONDS: #{@service_name}, while waiting for #{regexp}"
    Timeout.timeout(timeout, Timeout::Error, error_msg) do
      loop do
        break if journalctl_output.match(regexp)

        sleep(0.1)
      end
    end
  end

  def each_new_line
    new_output.each do |line|
      yield line
    end
  end

  def escalate_error
    return if active?

    puts status
    raise "Failed to run #{@service_name}"
  end

  def clear_news
    cropped_output = journalctl_output.split("\n", 2)[1].strip
    all_lines = cropped_output.split("\n")
    @old_lines = all_lines
  end

  private

  # receives all the lines of output that have been emitted since the last time this function was
  # called
  def new_output
    # remove first line from journalctl output because it contains the time of the command, which
    # constantly changes and would always count as "new".
    cropped_output = journalctl_output.split("\n", 2)[1].strip
    all_lines = cropped_output.split("\n")
    new_lines = all_lines - @old_lines
    @old_lines = all_lines
    new_lines
  end

  def journalctl_output
    @cmd.run_and_wait("sudo journalctl -u #{@service_name} -b --no-pager")
    stdout = @cmd.stdout.join('')
    stderr = @cmd.stderr.join('')
    "#{stdout}\n#{stderr}\n"
  end
end

def write_subjects(file_name, subjects)
  cmd = Command.new
  tmp_location = '/tmp'

  FileUtils.mkpath(tmp_location)
  File.open(File.join(tmp_location, file_name), mode: 'w') do |f|
    f.write subjects
  end
  cmd.run_and_wait('sudo mkdir -p /etc/artifactomat/subjects')
  cmd.run_and_wait("sudo mv #{tmp_location}/#{file_name} /etc/artifactomat/subjects/#{file_name}")
end

def write_recipe(name, recipe)
  cmd = Command.new
  File.open("/tmp/#{name}", 'w') do |f|
    f.write YAML.dump(recipe)
  end
  cmd.run_and_wait("sudo mv /tmp/#{name} /etc/artifactomat/recipes/#{name}")
end

def build_helpdesk_tf(recipe)
  tf3 = TrackFilter.new
  tf3.course_of_action = 'called helpdesk'
  tf3.extraction_pattern = [/(?<phone>\d*);(?<timestamp>\d{2}.\d{2}.\d{4} \d{2}:\d{2}:\d{2});(?<waiting_time>\d*);(?<duration>\d*)/]
  tf3.pattern = /(.+)/
  tf3.recipe_id = recipe.id
  tf3.score = 9001
  tf3
end

def build_satellite_tf(recipe)
  tf4 = TrackFilter.new
  tf4.course_of_action = 'satellite trace'
  tf4.extraction_pattern = [/\[(?<timestamp>[0-9]+)\] satellite data from (?<subject>.+)/]
  tf4.pattern = /(.+)/
  tf4.recipe_id = recipe.id
  tf4.score = 21
  tf4
end

def build_link_tf(recipe)
  tf = TrackFilter.new
  tf.course_of_action = 'link clicked'
  tf.extraction_pattern = [/\[(?<timestamp>[0-9]+)\] link clicked by (?<subject>.+)$/]
  tf.pattern = /link clicked by/
  tf.recipe_id = recipe.id
  tf.score = 420
  tf
end

def set_log_lvl_debug
  cmd = Command.new
  conf = YAML.load_file('/etc/artifactomat/conf.yml')
  conf['ape']['log_level'] = 4
  File.open('/tmp/conf.yml', 'w') do |f|
    f.write YAML.dump(conf)
  end
  cmd.run('sudo mv /tmp/conf.yml /etc/artifactomat/conf.yml')
end

def write_plugin_conf
  conf = '---
  plugins:
    ClientProxyAdapter:
      type: receiver
      file_name: client_proxy_adapter.rb
      class_name: ClientProxyAdapter
      subject_timeout: 15
      cert_path: /etc/artifactomat/certs
'
  File.open('/tmp/subjecttracker.yml', 'w') do |f|
    f.write conf
  end
  @cmd.run_and_wait('sudo mv /tmp/subjecttracker.yml /etc/artifactomat/conf.d/subjecttracker.yml')
end

def build_ssl_context(cert_path)
  ssl_ctx = OpenSSL::SSL::SSLContext.new
  ssl_ctx.cert = OpenSSL::X509::Certificate.new File.read "#{cert_path}/public/client.itsape.pem"
  ssl_ctx.key = OpenSSL::PKey::RSA.new File.read "#{cert_path}/private/client.itsape.key"
  ssl_ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER | OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT
  ssl_ctx.ca_file = "#{cert_path}/public/ca.pem"
  ssl_ctx
end

def build_connection(cert_path, host = 'localhost', port = 8123)
  ssl_ctx = build_ssl_context(cert_path)
  socket = TCPSocket.new(host, port)
  ssl_socket = OpenSSL::SSL::SSLSocket.new(socket, ssl_ctx)
  ssl_socket.sync_close = true
  connection = ssl_socket.connect
  connection
rescue StandardError => e
  puts "Error on connect: #{e.message}"
end

def write_satellite_trace
  @cmd.run_and_wait("echo \"[#{Time.now.to_i}] satellite data from muster.csv/1\n[#{Time.now.to_i}] satellite data from muster.csv/2\" | sudo tee -a /tmp/artifactomat/satellite_trace")
end

def write_helpdesk_trace(timestamp)
  log = helpdesk_glue_log_pieces timestamp
  path = '/tmp/helpdesk_trace.csv'
  File.open(path, 'w') do |f|
    text = { 'trace' => log }.to_yaml
    f.write text
  end
  @cmd.run_and_wait('sudo mv /tmp/helpdesk_trace.csv /tmp/artifactomat/helpdesk_trace.csv')
end

# rubocop:disable Metrics/AbcSize
def write_real_life_example_helpdesk_logs_variant_1(time)
  log = '"Nummer ","Service ","Kunde ","Beschreibung ","Ticket erstellt am/um "
0815,"N/A - Service existiert nicht ","Unbekannte, Person ","HL - 00000 - Eine generische Beschreibung" ",' + (time + 10).strftime('%-m/%d/%Y %-k:%M:%S').to_s + '
93060,"AD-Arbeitsplatzverwaltung ","Mustermann, Max ","HL - 11111 Datei ""Top Secret"" ",' + (time + 20).strftime('%-m/%d/%Y %-k:%M:%S').to_s + '
987654321,"N/A - Service existiert nicht ","Person, Unbekannt ","HL - 33333 - Virusmeldung erhalten ",' + (time + 30).strftime('%-m/%d/%Y %-k:%M:%S').to_s + '
5557334,"AD-Arbeitsplatzverwaltung ","Simpson, Homer ","HL - 44444 Antivirus Meldung Meldung ",' + (time + 40).strftime('%-m/%d/%Y %-k:%M:%S').to_s + '
123456,"Email-Gateway ","Musterfrau, Terelanie ","HL - 22222 mysteriöse e-mail ",' + (time + 50).strftime('%-m/%d/%Y %-k:%M:%S').to_s

  path = '/tmp/helpdesk_trace_rl_1.csv'
  File.open(path, 'w') { |file| file.puts log }
  @cmd.run_and_wait("sudo mv #{path} /tmp/artifactomat/helpdesk_trace_rl_1.csv")
end

def write_real_life_example_helpdesk_logs_variant_2(time)
  log = '"origdestination ","origin         ","callstart               ","callend ","disconnecttime          ","termtype ","assignedagent ","answeringagent ","waittime "
55555,0815,' + (time + 10).strftime('%-m/%d/%Y %-k:%M').to_s + ',21,' + (time + 40).strftime('%-m/%d/%Y %-k:%M').to_s + ',21,0,0,0
55555,93060,' + (time + 20).strftime('%-m/%d/%Y %-k:%M').to_s + ',25,' + (time + 50).strftime('%-m/%d/%Y %-k:%M').to_s + ',22,0,0,0
55555,987654321,' + (time + 30).strftime('%-m/%d/%Y %-k:%M').to_s + ',57,' + (time + 60).strftime('%-m/%d/%Y %-k:%M').to_s + ',12,0,0,0
55555,5557334,' + (time + 40).strftime('%-m/%d/%Y %-k:%M').to_s + ',39,' + (time + 70).strftime('%-m/%d/%Y %-k:%M').to_s + ',42,0,0,0
55555,123456,' + (time + 50).strftime('%-m/%d/%Y %-k:%M').to_s + ',15,' + (time + 80).strftime('%-m/%d/%Y %-k:%M').to_s + ',84,0,0,0'

  path = '/tmp/helpdesk_trace_rl_2.csv'
  File.open(path, 'w') { |file| file.puts log }
  @cmd.run_and_wait("sudo mv #{path} /tmp/artifactomat/helpdesk_trace_rl_2.csv")
end

def write_real_life_example_helpdesk_logs_variant_3(time)
  log = 'Kunde,Beschreibung ,Artefakt,Quelle ,Ticket erstellt am/um:,Anmerkung:,Name aus Teilnehmerliste,Callstart (UTC),Disconnecttime (UTC),Wartezeit in Warteschlange (sek),Mail gesendet
Nonexistent.csv/1,HL -11111 Beschreibung ,artefakt,E-mail ,' + time.strftime('%-m/%d/%Y %-k:%M').to_s + ',,,,,,' + time.strftime('%a').to_s[0, 2] + ' ' + (time + 10).strftime('%d.%m.%Y %k:%M').to_s + '
Muster.csv/1,HL -11111 Beschreibung ,artefakt,E-mail ,' + time.strftime('%-m/%d/%Y %-k:%M').to_s + ',,,,,,' + time.strftime('%a').to_s[0, 2] + ' ' + (time + 20).strftime('%d.%m.%Y %k:%M').to_s + '
Nonexistent.csv/404,HL -11111 Beschreibung ,artefakt,E-mail ,' + time.strftime('%-m/%d/%Y %-k:%M').to_s + ',,,,,,' + time.strftime('%a').to_s[0, 2] + ' ' + (time + 30).strftime('%d.%m.%Y %k:%M').to_s + '
Nonexistent.csv/11,KI -11111 Beschreibung ,artefakt,Telefon ,' + time.strftime('%-m/%d/%Y %-k:%M').to_s + ',,,' + (time + 40).strftime('%-m/%d/%Y %-k:%M').to_s + ',' + (time + 40).strftime('%k:%M:%S').to_s + ',6,
Muster.csv/2,KI -11111 Beschreibung ,artefakt,Telefon ,' + time.strftime('%-m/%d/%Y %-k:%M').to_s + ',,,' + (time + 50).strftime('%-m/%d/%Y %-k:%M').to_s + ',' + (time + 50).strftime('%k:%M:%S').to_s + ',4,'

  path = '/tmp/helpdesk_trace_rl_3.csv'
  File.open(path, 'w') { |file| file.puts log }
  @cmd.run_and_wait("sudo mv #{path} /tmp/artifactomat/helpdesk_trace_rl_3.csv")
end

def write_real_life_example_helpdesk_logs_variant_4(time)
  log = 'Bemerkung,Name,Datum Callstart (UTC),Datum Disconnecttime (UTC),Wartezeit in Warteschlange (sek)
,Nonexistent/404,' + (time + 10).strftime('%d/%m/%Y %k:%M:%S').to_s + ',' + (time + 30).strftime('%d/%m/%Y %k:%M:%S').to_s + ',10
,Muster.csv/1,' + (time + 20).strftime('%d/%m/%Y %k:%M:%S').to_s + ',' + (time + 40).strftime('%d/%m/%Y %k:%M:%S').to_s + ',12
,Nonexistent/404,' + (time + 30).strftime('%d/%m/%Y %k:%M:%S').to_s + ',' + (time + 66).strftime('%d/%m/%Y %k:%M:%S').to_s + ',4
,Nonexistent/404,' + (time + 40).strftime('%d/%m/%Y %k:%M:%S').to_s + ',' + (time + 77).strftime('%d/%m/%Y %k:%M:%S').to_s + ',7
,Muster.csv/2,' + (time + 50).strftime('%d/%m/%Y %k:%M:%S').to_s + ',' + (time + 78).strftime('%d/%m/%Y %k:%M:%S').to_s + ',77'

  path = '/tmp/helpdesk_trace_rl_4.csv'
  File.open(path, 'w') { |file| file.puts log }
  @cmd.run_and_wait("sudo mv #{path} /tmp/artifactomat/helpdesk_trace_rl_4.csv")
end
# rubocop:enable Metrics/AbcSize

def helpdesk_glue_log_pieces(time)
  "ApparatNr;Anrufbeginn;Wartezeit;Anrufende
93080;#{(time + 60).strftime('%d.%m.%Y %H:%M:%S')};0;5
78288;#{(time + 65).strftime('%d.%m.%Y %H:%M:%S')};0;27
93060;#{(time + 100).strftime('%d.%m.%Y %H:%M:%S')};15;17
93057;#{(time + 120).strftime('%d.%m.%Y %H:%M:%S')};4;5"
end

def write_trace(episode, subject)
  log = trace_glue_log_pieces(episode, subject)
  puts log
  path = '/tmp/trace.yml'
  File.open(path, 'w') do |f|
    text = { 'trace' => log }.to_yaml
    f.write text
  end
  @cmd.run_and_wait('sudo mv /tmp/trace.yml /tmp/artifactomat/trace.yml')
end

def trace_glue_log_pieces(episode, subject)
  "[#{episode.schedule_start.to_i + 10}] link clicked by #{subject} in #{episode.id}\n[#{episode.schedule_start.to_i + 20}] data filled in by #{subject} in #{episode.id}\n"
end
