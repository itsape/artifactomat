# frozen_string_literal: true

require 'rspec'
require 'open3'
require 'socket'
require 'timeout'
require 'fileutils'
require 'yaml'
require 'active_record'
require 'support/factory_bot'
require 'listen'
require 'pathname'

require_relative 'system_spec_helper'

def during(ep1, ep2)
  return true if @armed[ep2] <= @armed[ep1] && @disarmed[ep2] > @armed[ep1]
  return true if @armed[ep1] <= @armed[ep2] && @disarmed[ep1] > @armed[ep2]

  false
end

describe 'SYSTEM SPEC – TestScheduler', system: true, order: :defined do
  before(:all) do
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']
    ActiveRecord::Base.establish_connection(db_config)

    @logger = SpecLogger.new('scheduling_system_spec')
    @redis = SystemdService.new('redis-server@artifactomat.service')
    @subject_tracker = SystemdService.new('artifactomat-subject-tracker.service')
    @track_collector = SystemdService.new('artifactomat-track-collector.service')
    @test_scheduler = SystemdService.new('artifactomat-ape-scheduler.service')
    @monitoring = SystemdService.new('artifactomat-ape-monitoring.service')
    @routing = SystemdService.new('artifactomat-routing.service')
    @apenames = SystemdService.new('artifactomat-apenames.service')
    @aped = SystemdService.new('artifactomat-aped.service')
    @ape = Command.new
    @cmd = Command.new
    @jobspooler = SystemdService.new('artifactomat-ape-job-spooler.service')

    @cert_path = '/etc/artifactomat/certs'

    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.ip_forward=1'
    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.conf.all.route_localnet=1'

    @cmd.run_and_wait 'sudo artifactomat_setup db'
    @cmd.run_and_wait 'sudo artifactomat_setup app --yes', 120

    # hashes keep track of armed subjects and time
    @armed_status = []
    @armed = {}
    @disarmed = {}
    @duration = 2

    # build and write a recipe
    ie = build(:infrastructure_element, generate_script: 'exit 0',
                                        monitor_script: 'exit 0',
                                        destroy_script: 'exit 0',
                                        ports: [443])
    ie2 = build(:infrastructure_element, generate_script: 'touch /tmp/artifactomat/ie2-generated',
                                         monitor_script: 'exit 0',
                                         destroy_script: 'rm /tmp/artifactomat/ie2-generated',
                                         ports: [443])
    recipe1 = build(:recipe, infrastructure: [ie],
                             deploy_script: 'exit 0',
                             artifact_script: 'exit 0',
                             arm_script: 'touch /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERID-minimal_1',
                             disarm_script: 'rm /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERID-minimal_1',
                             parameters: { 'parameter' => '21' },
                             duration: @duration)
    recipe1.trackfilters.push(build_helpdesk_tf(recipe1))
    recipe1.trackfilters.push(build_satellite_tf(recipe1))
    recipe1.trackfilters.concat(build_tf(recipe1))

    recipe2 = build(:recipe, infrastructure: [ie],
                             deploy_script: 'exit 0',
                             artifact_script: 'exit 0',
                             arm_script: 'touch /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERID-minimal_2',
                             disarm_script: 'rm /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERID-minimal_2',
                             parameters: { 'parameter' => '42' },
                             duration: @duration)
    recipe2.trackfilters.push(build_helpdesk_tf(recipe2))
    recipe2.trackfilters.push(build_satellite_tf(recipe2))
    recipe2.trackfilters.concat(build_tf(recipe2))

    recipe3 = build(:recipe, infrastructure: [ie],
                             deploy_script: 'exit 0',
                             artifact_script: 'exit 0',
                             arm_script: 'touch /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERID-minimal_3',
                             disarm_script: 'rm /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERID-minimal_3',
                             parameters: { 'parameter' => '42' },
                             duration: @duration)
    recipe3.trackfilters.push(build_helpdesk_tf(recipe3))
    recipe3.trackfilters.push(build_satellite_tf(recipe3))
    recipe3.trackfilters.concat(build_tf(recipe3))

    recipe4 = build(:recipe, infrastructure: [ie],
                             deploy_script: 'exit 0',
                             artifact_script: 'exit 0',
                             arm_script: 'touch /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERID-minimal_4',
                             disarm_script: 'rm /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERID-minimal_4',
                             parameters: { 'parameter' => '42' },
                             duration: @duration)
    recipe4.trackfilters.push(build_helpdesk_tf(recipe4))
    recipe4.trackfilters.push(build_satellite_tf(recipe4))
    recipe4.trackfilters.concat(build_tf(recipe4))

    recipe5 = build(:recipe, infrastructure: [ie2],
                             deploy_script: 'exit 0',
                             artifact_script: 'exit 0',
                             arm_script: 'exit 0',
                             disarm_script: 'exit 0',
                             parameters: { 'parameter' => '42' },
                             duration: 2)

    write_recipe('minimal_1.yml', recipe1)
    write_recipe('minimal_2.yml', recipe2)
    write_recipe('minimal_3.yml', recipe3)
    write_recipe('minimal_4.yml', recipe4)
    write_recipe('cleanup.yml', recipe5)

    subjects_alpha = "id,name,surname,userid,email\n" \
               "1,1,1,sub1,1@1&1.de\n" \
               "2,2,2,sub2,2@1&1.de\n" \
               "3,Non,Existent,no,dummy@example.net\n" \
               "4,Non1,Existent1,no1,dummy1@example.net\n" \
               "5,Non2,Existent2,no2,dummy2@example.net\n" \
               '6,Non3,Existent3,no3,dummy3@example.net'
    subjects_beta = "id,name,surname,userid,email\n" \
                "4,4,4,sub4,4@1&1.de\n" \
                '3,3,3,sub3,3@1&1.de'
    write_subjects('alpha.csv', subjects_alpha)
    write_subjects('beta.csv', subjects_beta)
    write_plugin_conf
    set_log_lvl_debug
  end

  after(:all) do
    @aped.kill
    @subject_tracker.kill
    @track_collector.kill
    @test_scheduler.kill
    @routing.kill
    @monitoring.kill
  end

  after(:each) do |example|
    if example.exception
      @aped.kill
      @logger.log(@aped.status)

      @subject_tracker.kill
      @logger.log(@subject_tracker.status)

      @track_collector.kill
      @logger.log(@track_collector.status)

      @routing.kill
      @logger.log(@routing.status)

      @test_scheduler.kill
      @logger.log(@test_scheduler.status)

      @monitoring.kill
      @logger.log(@monitoring.status)
    end
  end

  it 'starting aped' do
    @redis.start
    @routing.start
    @monitoring.start
    @apenames.start
    @jobspooler.start
    @subject_tracker.start
    @track_collector.start
    @test_scheduler.start
    @aped.start
    sleep 6

    @aped.escalate_error
    abort("\e[31m\nCAUTION!!\e[0m:\n#{@redis.status}") unless @redis.active?

    expect(@aped.active?).to be_truthy, 'Problem with starting aped!'
    expect(@apenames.active?).to be_truthy
    expect(@jobspooler.active?).to be_truthy
  end

  context 'schedules, prepares and approves two series' do
    it 'setup and run TestSeries' do
      @ape.run_and_wait 'ape program new -l "TestProgram"'

      @ape.run_and_wait 'ape series new -g "alpha.csv" -s "in 40 sec" -e "in 6 min" -l "TestSeries 1" 1'
      @ape.run_and_wait 'ape season new -d 2m -r "minimal_1" -l "TestSeason 1_1" 1'
      @ape.run_and_wait 'ape season new -d 2m -r "minimal_2" -l "TestSeason 1_2" 1'

      @ape.run_and_wait 'ape series new -g "beta.csv" -s "in 40 sec" -e "in 6 min" -l "TestSeries 2" 1'
      @ape.run_and_wait 'ape season new -d 2m -r "minimal_3" -l "TestSeason 2_1" 2'
      @ape.run_and_wait 'ape season new -d 2m -r "minimal_4" -l "TestSeason 2_2" 2'
      expect(TestProgram.count).to eq(1)
      expect(TestSeries.count).to eq(2)
      expect(TestSeason.count).to eq(4)

      @ape.run_and_wait 'ape series schedule 1'
      @ape.run_and_wait 'ape series schedule 2'

      @ape.run_and_wait 'ape series prepare 1'
      @ape.run_and_wait 'ape series prepare 2'

      begin
        prepared = []
        Timeout.timeout(30) do
          loop do
            @aped.each_new_line do |line|
              match = line.match(/<APE> \[Series "TestSeries (?<series>\d)" \(\d\) fully prepared, you may approve now\.\]/)
              prepared.push match[:series] if match
            end
            break if prepared.include?('1') && prepared.include?('2')

            sleep 1
          end
        end
      rescue Timeout::Error
        raise "Timeout while waiting for framework to allow approval. Prepared: #{prepared}"
      end

      @ape.run_and_wait 'ape series approve 1'
      @ape.run_and_wait 'ape series approve 2'
    end

    it 'arms and disarms 16 episodes' do
      # puts '-------------------- 1 --------------------'
      # @ape.run_and_wait 'ape series timetable 1'
      # puts @ape.stdout.join('')
      # puts '-------------------- 2 --------------------'
      # @ape.run_and_wait 'ape series timetable 2'
      # puts @ape.stdout.join('')
      # puts '-------------------- X --------------------'

      begin
        episode_to_arm = {}
        Timeout.timeout((2 * @duration * 60) + 90) do
          loop do
            # check log for finished arm/disarm events
            @test_scheduler.each_new_line do |line|
              match = line.match %r{<(?<ts>[\w\d: ]+)> <[^>]+> <\w+> \[(?<action>\w+) Episode \((?<episode>\d+)\) for (?<subject>\w+\.csv/\d+)}
              if match && (match[:action] == 'Armed')
                # check filesystem for arm-artifact
                te = TestEpisode.where("id = #{match[:episode]}").first
                @armed_status << te.status
                Dir.glob("/tmp/artifactomat/#{match[:episode]}*") do |f|
                  arm_artifact = f.match(%r{/tmp/artifactomat/#{match[:episode]}-(?<artifact>[\w\d]+-minimal_\d)})
                  @armed[arm_artifact[:artifact]] = Time.parse(match[:ts]) unless arm_artifact.nil?
                  episode_to_arm[match[:episode]] = arm_artifact[:artifact] unless arm_artifact.nil?
                end
              end

              if match && (match[:action] == 'Disarmed')
                # check filesystem for arm-artifact
                @disarmed[episode_to_arm[match[:episode]]] = Time.parse(match[:ts]) unless File.exist? "/tmp/artifactomat/#{match[:episode]}-#{episode_to_arm[match[:episode]]}}"
              end
            end
            break if (@armed.size == 16) && (@disarmed.size == 16)

            sleep 1
          end
        end
      rescue Timeout::Error
        raise "Timeout while waiting for framework to arm and disarm 16 episodes. Armed: #{@armed.keys.size} - #{@armed.keys},\n\nDisarmd: #{@disarmed.keys.size} - #{@disarmed.keys}"
      end

      episodes = %w[no-minimal_1 no1-minimal_1 no2-minimal_2
                    sub1-minimal_1 no3-minimal_2 sub2-minimal_2
                    sub4-minimal_3 sub3-minimal_3 no-minimal_2
                    sub1-minimal_2 sub2-minimal_1 no2-minimal_1
                    no1-minimal_2 no3-minimal_1 sub3-minimal_4
                    sub4-minimal_4]
      expect(@armed).to include(*episodes)
      expect(@disarmed).to include(*episodes)
    end

    it 'disarms episodes after their duration [+/- 3s]' do
      @armed.each_key do |key|
        expect(@disarmed[key] - @armed[key]).to be_within(3).of(@duration * 60)
      end
    end

    it 'arms multiple episodes for same subject subsequently [after at most 60s]' do
      # series 1
      expect((@armed['sub1-minimal_1'] - @armed['sub1-minimal_2']).abs).to be_within(60).of(@duration * 60)
      expect((@armed['sub2-minimal_1'] - @armed['sub2-minimal_2']).abs).to be_within(60).of(@duration * 60)
      # series 2
      expect((@armed['sub3-minimal_3'] - @armed['sub3-minimal_4']).abs).to be_within(60).of(@duration * 60)
      expect((@armed['sub4-minimal_3'] - @armed['sub4-minimal_4']).abs).to be_within(60).of(@duration * 60)
    end

    it 'runs two series in parallel' do
      accumulator = {}
      # they run parallel if at least one episode of series 2 will run during an episode from series 1
      accumulator['sub1 parallel sub3_3'] = 'true' if during('sub1-minimal_1', 'sub3-minimal_3')
      accumulator['sub1 parallel sub3_4'] = 'true' if during('sub1-minimal_1', 'sub3-minimal_4')
      accumulator['sub1 parallel sub4_3'] = 'true' if during('sub1-minimal_1', 'sub4-minimal_3')
      accumulator['sub1 parallel sub4_4'] = 'true' if during('sub1-minimal_1', 'sub4-minimal_4')
      expect(accumulator.length).to be_positive
    end

    it 'sets running episodes status to "running"' do
      @armed_status.each do |status|
        expect(status).to eq('running')
      end
    end

    it 'sets completed episodes status to "completed"' do
      tes = TestEpisode.where('status = 2')
      expect(tes.count).to eq(16)
    end
  end

  describe 'Cleans up the infrastructure' do
    it 'setup and run TestSeries' do
      old_tps = TestProgram.count
      old_tser = TestSeries.count
      old_tsea = TestSeason.count
      @ape.run_and_wait 'ape program new -l "TestProgram2"'

      @ape.run_and_wait "ape series new -g 'alpha.csv' -s 'in 40 sec' -e 'in 160 sec' -l 'CleanUpSeries' #{TestProgram.last.id}"
      @ape.run_and_wait "ape season new -d 2m -r 'cleanup' -l 'CleanUpSeason' #{TestSeries.last.id}"

      expect(TestProgram.count).to eq(old_tps + 1)
      expect(TestSeries.count).to eq(old_tser + 1)
      expect(TestSeason.count).to eq(old_tsea + 1)

      @ape.run_and_wait "ape series schedule #{TestSeries.last.id}"
      @ape.run_and_wait "ape series prepare #{TestSeries.last.id}"

      begin
        prepared = nil
        Timeout.timeout(30) do
          loop do
            @aped.each_new_line do |line|
              match = line.match(/<APE> \[Series "(?<series>[A-Za-z]+)" \(\d\) fully prepared, you may approve now\.\]/)
              prepared = match[:series] if match
            end
            break if prepared == 'CleanUpSeries'

            sleep 1
          end
        end
      rescue Timeout::Error
        raise 'Timeout while waiting for framework to allow approval.'
      end

      @ape.run_and_wait "ape series approve #{TestSeries.last.id}"
    end

    it 'creates infrastructure and routing' do
      sleep_until(TestSeries.last.start_date + 5)

      @cmd.run_and_wait('sudo ipset list')
      expect(@cmd.stdout).to include("Name: itsape-#{TestSeason.last.id}\n")

      if_file = Pathname.new('/tmp/artifactomat/ie2-generated')
      expect(if_file.exist?).to be_truthy
    end

    it 'removes infrastructure and routing' do
      sleep_until(TestSeries.last.end_date + 5)

      @cmd.run_and_wait('sudo ipset list')
      expect(@cmd.stdout).not_to include("Name: itsape-#{TestSeason.last.id}\n")

      if_file = Pathname.new('/tmp/artifactomat/ie2-generated')
      expect(if_file.exist?).to be_falsy
    end
  end
end
