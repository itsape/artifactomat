# frozen_string_literal: true

require 'rspec'
require 'active_record'

require_relative 'system_spec_helper'

describe 'CLIENT BUILD SYSTEM SPEC', system: true, order: :defined do
  before(:all) do
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']
    ActiveRecord::Base.establish_connection(db_config)

    @logger = SpecLogger.new('client_build_system_spec')
    @ape = Command.new
    @cmd = Command.new
    @aped = SystemdService.new('artifactomat-aped.service')
    @redis = SystemdService.new('redis-server@artifactomat.service')

    @cert_path = '/etc/artifactomat/certs'

    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.ip_forward=1'
    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.conf.all.route_localnet=1'

    @cmd.run_and_wait 'sudo artifactomat_setup db'
    @cmd.run_and_wait 'sudo artifactomat_setup app --yes', 120

    @redis.start
    @aped.start
    sleep(5) # wait for aped to be fully booted up

    set_log_lvl_debug
  end

  after(:all) do
    @aped.escalate_error
    @aped.kill
    @logger.log(@aped.status)
    @logger.log(@ape.pretty_output)
    @logger.log(@cmd.pretty_output)
  end

  it 'builds the client' do
    expect { @ape.run_and_wait('ape client', 140) }.not_to raise_error
    expect(File.exist?('ITSAPE.Client.msi')).to be_truthy
    expect { @cmd.run_and_wait('msiinfo suminfo ITSAPE.Client.msi') }.not_to raise_error
  end
end
