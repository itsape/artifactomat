# frozen_string_literal: true

require_relative 'system_spec_helper'
require 'uri'
require 'net/http'
require 'openssl'
require 'active_record'
require 'json'
require 'yaml'

def send_get(path)
  uri = URI::HTTPS.build(host: '127.0.0.1', port: 8443, path: path)
  request = Net::HTTP::Get.new(uri)
  make_https(request)
end

def send_post(path, data)
  uri = URI::HTTPS.build(host: '127.0.0.1', port: 8443, path: path)
  request = Net::HTTP::Post.new(uri)
  make_https(request, data)
end

def add_jobspooler_tf(recipe)
  tf = TrackFilter.new
  tf.course_of_action = 'artifact copied'
  tf.extraction_pattern = [/(?<timestamp>.[\d\+ :-]+).*(?<userid>(?<=user: )\w+).* status: completed, action: copy, file_path:.*/]
  tf.pattern = /.* status: completed, action: copy, file_path:.*/
  tf.recipe_id = recipe.id
  tf.score = 42
  [tf]
end

def write_trackfilter_conf
  cmd = Command.new
  conf = '---
  - !ruby/object:TrackFilter
    pattern: !ruby/regexp /(.+)/
    extraction_pattern:
    - !ruby/regexp /(?<phone>\d*);(?<timestamp>\d{2}.\d{2}.\d{4} \d{2}:\d{2}:\d{2});(?<waiting_time>\d*);(?<duration>\d*)/
    score: -1
    course_of_action: called helpdesk
  - !ruby/object:TrackFilter
    pattern: !ruby/regexp /(.+)/
    extraction_pattern:
    - !ruby/regexp /(?<timestamp>.*) \| \{.* user\:\ (?<userid>.*)\, status\:\ completed\, action\:\ copy.*/
    score: 0
    course_of_action: (STATUS) artifact successfully copied
'
  File.open('/tmp/trackfilter.yml', 'w') do |f|
    f.write conf
  end
  cmd.run_and_wait('sudo mv /tmp/trackfilter.yml /etc/artifactomat/conf.d/trackfilter.yml')
end

describe 'APEJOBS SYSTEM SPEC', system: true, order: :defined do
  before(:all) do
    # values should be the default values from the config
    # JobTool uses these environment variables that are exposed to recipe environments
    ENV['ARTIFACTOMAT_CONF_CONF_APE_JOB_CLERK_PORT'] = 51_648.to_s
    ENV['ARTIFACTOMAT_CONF_CONF_APE_JOB_CLERK_ADDRESS'] = 'localhost'
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']
    ActiveRecord::Base.establish_connection(db_config)

    @logger = SpecLogger.new('apejobs_system_spec')
    @redis = SystemdService.new('redis-server@artifactomat.service')
    @cmd = Command.new
    @jobspooler = SystemdService.new('artifactomat-ape-job-spooler.service')
    @job_clerk = SystemdService.new('artifactomat-ape-job-clerk.service')
    @job_tool = Command.new
    @cmd.run_and_wait('sudo artifactomat_setup db')
    @cmd.run_and_wait('sudo artifactomat_setup app --yes', 120)

    @jobspooler.start
    sleep 3
    @logger.log(@jobspooler.status)

    @job_clerk.start
  end

  after(:all) do
    @logger.log(@jobspooler.status)
    @jobspooler.stop

    @logger.log(@job_clerk.status) unless @job_clerk.active?
    @job_clerk.stop
  end

  context 'service' do
    it 'jobspooler listens' do
      expect(tcp_port_open?('127.0.0.1', 8443)).to be_truthy
    end

    it 'jobclerk listens' do
      expect(tcp_port_open?('127.0.0.1', 51_648)).to be_truthy
    end
  end

  context 'run jobs' do
    it 'joblist empty' do
      resp = send_get '/jobs/someuser'
      expect(resp.code.to_i).to eq 204
    end

    it 'create \'run\' job with ape-job' do
      # create a file
      file_location = '/tmp/itsapetestfile.txt'
      File.open(file_location, mode: 'w') do |file|
        file.write('a string in a textfile')
      end
      # create a job to run a file
      expect do
        @job_tool.run_and_wait('ape-job build run the_user /tmp/itsapetestfile.txt')
      end.not_to raise_error
    end

    it 'job appears in joblist' do
      resp = send_get '/jobs/the_user'
      expectation = ['https://127.0.0.1:8443/jobs/the_user/1'].to_json
      expect(resp.code.to_i).to eq 200
      expect(resp.body).to match expectation
    end

    it 'job details can be queried' do
      resp = send_get '/jobs/the_user/1'
      expect(resp.code.to_i).to eq 200
      body = JSON.parse(resp.body)
      expect(body['id']).to eq 1
      expect(body['user']).to match 'the_user'
      expect(body['status']).to match 'waiting'
      expect(body['action']).to match 'execute'
      expect(body['file_path']).to match '/tmp/itsapetestfile.txt'
      expect(body['deploy_path']).to be_empty
    end
  end

  context 'copy jobs' do
    it 'create \'copy\' job with ape-job' do
      # create a file
      file_location = '/tmp/itsapetestfile.txt'
      File.open(file_location, mode: 'w') do |file|
        file.write('a string in a textfile')
      end
      # create a job to run a file
      expect do
        @job_tool.run_and_wait("ape-job build copy another_user /tmp/itsapetestfile.txt '%USER_PROFILE\\DESKTOP'")
      end.not_to raise_error
    end

    it 'job appears in joblist' do
      resp = send_get '/jobs/another_user'
      expectation = ['https://127.0.0.1:8443/jobs/another_user/2'].to_json
      expect(resp.code.to_i).to eq 200
      expect(resp.body).to match expectation
    end

    it 'job details can be queried' do
      resp = send_get '/jobs/another_user/2'
      body = JSON.parse(resp.body)
      expect(body['id']).to eq 2
      expect(body['user']).to match 'another_user'
      expect(body['status']).to match 'waiting'
      expect(body['action']).to match 'copy'
      expect(body['file_path']).to match '/tmp/itsapetestfile.txt'
      expect(body['deploy_path']).to match '%USER_PROFILE\\DESKTOP'
      expect(resp.code.to_i).to eq 200
    end

    it 'job status can be updated' do
      post_res = send_post '/jobs/another_user/2', 'status' => 'failed'
      expect(post_res.code.to_i).to eq 204
      resp = send_get '/jobs/another_user/2'
      body = JSON.parse(resp.body)
      expect(body['id']).to eq 2
      expect(body['user']).to match 'another_user'
      expect(body['status']).to match 'failed'
      expect(body['action']).to match 'copy'
      expect(body['file_path']).to match '/tmp/itsapetestfile.txt'
      expect(body['deploy_path']).to match '%USER_PROFILE\\DESKTOP'
      expect(resp.code.to_i).to eq 200
    end

    it 'the file attached can be loaded' do
      resp = send_get '/file/2'
      body_md5 = Digest::MD5.hexdigest resp.body
      file_md5 = Digest::MD5.hexdigest 'a string in a textfile'
      expect(body_md5).to match(file_md5)
    end

    it 'create \'copy\' job that expires' do
      resp = send_get '/jobs/another_user'
      expect(resp.code.to_i).to eq 204

      # create a job to run a file
      expire = (Time.now + 5)
      @job_tool.run_and_wait("ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END='#{expire}' ape-job build copy another_user /tmp/itsapetestfile.txt '%USER_PROFILE\\DESKTOP'")

      resp = send_get '/jobs/another_user'
      expect(resp.code.to_i).to eq 200
      msg = JSON.parse(resp.body)
      expect(msg.count).to be == 1

      sleep_until(expire)
      sleep(1)

      resp = send_get '/jobs/another_user'
      expect(resp.code.to_i).to eq 204
    end
  end

  context 'remove jobs' do
    it 'create \'remove\' job with ape-job' do
      expect do
        @job_tool.run_and_wait('ape-job build copy the_user /tmp/itsapetestfile.txt \'%USER_PROFILE\DESKTOP\'')
        @job_tool.run_and_wait('ape-job build remove the_user \'%USER_PROFILE\DESKTOP\'')
      end.not_to raise_error
    end

    it 'job appears in joblist' do
      resp = send_get '/jobs/the_user'
      expectation = ['https://127.0.0.1:8443/jobs/the_user/3'].to_json
      expect(resp.code.to_i).to eq 200
      expect(resp.body).to match expectation
    end

    it 'job details can be queried' do
      resp = send_get '/jobs/the_user/5'
      body = JSON.parse(resp.body)
      expect(body['id']).to eq 5
      expect(body['user']).to match 'the_user'
      expect(body['status']).to match 'waiting'
      expect(body['action']).to match 'remove'
      expect(body['file_path']).to match ''
      expect(body['deploy_path']).to match '%USER_PROFILE\\DESKTOP'
      expect(resp.code.to_i).to eq 200
    end
  end

  context 'INTERFACES' do
    before(:all) do
      # build and write a recipe
      ie = build(:infrastructure_element, generate_script: 'touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/infrastructure',
                                          monitor_script: 'exit 0',
                                          destroy_script: 'rm -f $ARTIFACTOMAT_RECIPE_TEMP_DIR/infrastructure',
                                          ports: [443])
      recipe = build(:recipe, infrastructure: [ie],
                              deploy_script: 'touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/deploy',
                              artifact_script: 'if [ "$DEP_CHECK" != "" ] && ( [ "$DEP_CHECK" = 1 ] || [ "$DEP_CHECK" = "true" ] || [ "$DEP_CHECK" = "True" ]); then exit 0; fi; touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/artifact;',
                              arm_script: 'touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/arm',
                              disarm_script: 'touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/disarm',
                              parameters: { 'parameter' => '21' },
                              duration: 10)
      env_test_recipe = build(:recipe, infrastructure: [ie],
                                       deploy_script: 'touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/deploy',
                                       artifact_script: 'if [ "$DEP_CHECK" != "" ] && ( [ "$DEP_CHECK" = 1 ] || [ "$DEP_CHECK" = "true" ] || [ "$DEP_CHECK" = "True" ]); then exit 0; fi; touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/artifact;',
                                       arm_script: 'ape-job build run $ARTIFACTOMAT_SUBJECT_USERID /tmp/env_is_exposed.txt',
                                       disarm_script: 'exit 0',
                                       parameters: { 'parameter' => '21' },
                                       duration: 10)
      recipe.trackfilters.push(build_helpdesk_tf(recipe))
      recipe.trackfilters.push(build_satellite_tf(recipe))
      recipe.trackfilters.concat(add_jobspooler_tf(recipe))

      write_recipe('minimal.yml', recipe)
      write_recipe('env_test.yml', env_test_recipe)

      subjects = "id,name,surname,userid,email,phone\n" \
                 "1,Max,Mustermann,mustermann,max@muster.de,93060\n" \
                 "2,Terelanie,Musterfrau,terelanie,terelanie@muster.de,123456\n"
      write_subjects('muster.csv', subjects)
      write_plugin_conf
      write_trackfilter_conf
      set_log_lvl_debug
      @subject_tracker = SystemdService.new('artifactomat-subject-tracker.service')
      @track_collector = SystemdService.new('artifactomat-track-collector.service')
      @test_scheduler = SystemdService.new('artifactomat-ape-scheduler.service')
      @monitoring = SystemdService.new('artifactomat-ape-monitoring.service')
      @routing = SystemdService.new('artifactomat-routing.service')
      @aped = SystemdService.new('artifactomat-aped.service')
      @ape = Command.new
      @cmd = Command.new

      magic = "#{derive_ip}  proxy.its.apt"
      @cmd.run 'touch /tmp/artifact'
      @cmd.run "echo \"#{magic}\" | sudo tee -a /etc/hosts"
    end

    after(:all) do
      @aped.kill
      @logger.log(@aped.status)
      @jobspooler.kill
      @logger.log(@jobspooler.status)
      @subject_tracker.kill
      @logger.log(@subject_tracker.status)
      @track_collector.kill
      @logger.log(@track_collector.status)
    end

    it '[redis] start' do
      @redis.start
      abort("\e[31m\nCAUTION!!\e[0m:\n#{@redis.status}") unless @redis.active?
    end

    it '[routing] start' do
      @routing.start
      expect(@routing.active?).to be_truthy
    end

    it '[monitoring] start' do
      @monitoring.start
      expect(@monitoring.active?).to be_truthy
    end

    it '[subject_tracker] start' do
      @subject_tracker.start
      expect(@subject_tracker.active?).to be_truthy
    end

    it '[track_collector] start' do
      @track_collector.start
      expect(@track_collector.active?).to be_truthy
    end

    it '[test_scheduler] start' do
      @test_scheduler.start
      expect(@test_scheduler.active?).to be_truthy
    end

    it '[aped] start' do
      @aped.start
      expect(@aped.active?).to be_truthy
      sleep 4
      @aped.escalate_error
    end

    it '[aped] setup and run a series' do
      @ape.run_and_wait('ape program new -l TestProg')
      @ape.run_and_wait("ape series new -l 'TestSeri' -s 'in 40 sec' -e 'in 12 min' -g muster.csv #{TestProgram.last.id}")
      @ape.run_and_wait("ape season new -l TestSeas -d 10m -r 'minimal' -p 'parameter:12' #{TestSeries.last.id}")
      @ape.run_and_wait("ape series schedule #{TestSeries.last.id}")
      @next_up = Time.now + 40
      @ape.run_and_wait("ape series prepare #{TestSeries.last.id}")
      sleep 10
      @ape.run_and_wait("ape series approve #{TestSeries.last.id}")
      sleep 10
      sleep_until @next_up
    end

    it '[aped] is listening' do
      expect(tcp_port_open?('127.0.0.1', 8765)).to be_truthy
    end

    context 'jobspooler -> subjecttracker' do
      it '[subjecttracker] is listening' do
        expect(tcp_port_open?('127.0.0.1', 8123)).to be_truthy
      end

      it '[jobspooler] query jobs' do
        expect(SubjectHistory.where(subject_id: 'muster.csv/2', session_end: nil)).not_to exist
        resp = send_get '/jobs/terelanie'
        expect(resp.code.to_i).to eq 200
      end

      it '[subjecttracker] has received update' do
        sleep 1
        expect(SubjectHistory.where(subject_id: 'muster.csv/2', session_end: nil)).to exist
      end

      it '[ape-job] create job for user' do
        expect do
          @job_tool.run_and_wait('ape-job build copy terelanie /tmp/artifact \'C:\Users\terelanie\Desktop\'')
        end.not_to raise_error
      end
    end

    context 'jobspooler -> trackcollector' do
      it '[trackcollector] is listening' do
        expect(tcp_port_open?('127.0.0.1', 5555)).to be_truthy
      end

      it '[jobspooler] the job status is updated' do
        expect(TrackEntry.count).to eq 0
        copy_job = Job.where(user: 'terelanie', action: :copy).last
        post_res = send_post "/jobs/terelanie/#{copy_job.id}", 'status' => 'completed'
        expect(post_res.code.to_i).to eq 204
        resp = send_get "/jobs/terelanie/#{copy_job.id}"
        body = JSON.parse(resp.body)
        expect(body['status']).to match 'completed'
        expect(resp.code.to_i).to eq 200
        sleep 3
      end

      it '[trackcollector] to trackcollector recieved the trace' do
        expect(TrackEntry.count).to eq 2
        expect(TrackEntry.first.course_of_action).to match 'artifact copied'
        expect(TrackEntry.first.subject_id).to eq 'muster.csv/2'
        expect(TrackEntry.last.course_of_action).to match '(STATUS) artifact successfully copied'
        expect(TrackEntry.last.subject_id).to eq 'muster.csv/2'
      end
    end
  end
end
