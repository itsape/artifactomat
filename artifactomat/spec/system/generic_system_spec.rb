# frozen_string_literal: true

require 'rspec'
require 'open3'
require 'set'
require 'socket'
require 'timeout'
require 'fileutils'
require 'yaml'
require 'active_record'
require 'chronic'
require 'support/factory_bot'

require_relative 'system_spec_helper'

describe 'SYSTEM SPEC', system: true, order: :defined do
  before(:all) do
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']
    ActiveRecord::Base.establish_connection(db_config)

    @wait_until = Time.now

    @logger = SpecLogger.new('generic_system_spec')
    @ape = Command.new
    @cmd = Command.new
    @aped = SystemdService.new('artifactomat-aped.service')
    @apenames = SystemdService.new('artifactomat-apenames.service')
    @redis = SystemdService.new('redis-server@artifactomat.service')
    @subject_tracker = SystemdService.new('artifactomat-subject-tracker.service')
    @track_collector = SystemdService.new('artifactomat-track-collector.service')
    @test_scheduler = SystemdService.new('artifactomat-ape-scheduler.service')
    @monitoring = SystemdService.new('artifactomat-ape-monitoring.service')
    @routing = SystemdService.new('artifactomat-routing.service')

    @cert_path = '/etc/artifactomat/certs'

    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.ip_forward=1'
    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.conf.all.route_localnet=1'

    @cmd.run_and_wait 'sudo artifactomat_setup db'
    @cmd.run_and_wait 'sudo artifactomat_setup app --yes', 120

    # build and write a recipe
    ie = build(:infrastructure_element, generate_script: 'touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/infrastructure',
                                        monitor_script: 'exit 0',
                                        destroy_script: 'rm -f $ARTIFACTOMAT_RECIPE_TEMP_DIR/infrastructure',
                                        ports: [443])
    recipe = build(:recipe, infrastructure: [ie],
                            deploy_script: 'touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/deploy',
                            artifact_script: 'if [ "$DEP_CHECK" != "" ] && ( [ "$DEP_CHECK" = 1 ] || [ "$DEP_CHECK" = "true" ] || [ "$DEP_CHECK" = "True" ]); then exit 0; fi; touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/artifact;',
                            arm_script: 'touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/arm',
                            disarm_script: 'touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/disarm',
                            parameters: { 'parameter' => '21' },
                            duration: 10)
    recipe.trackfilters.push(build_helpdesk_tf(recipe))
    recipe.trackfilters.push(build_satellite_tf(recipe))
    recipe.trackfilters.concat(build_tf(recipe))

    write_recipe('minimal.yml', recipe)

    subjects = "id,name,surname,userid,email,phone\n" \
               "1,Max,Mustermann,mustermann,max@muster.de,93060\n" \
               "2,Terelanie,Musterfrau,terelanie,terelanie@muster.de,123456\n"
    write_subjects('muster.csv', subjects)
    write_plugin_conf
    set_log_lvl_debug
  end

  after(:all) do
    @aped.escalate_error
    @subject_tracker.escalate_error
    @track_collector.escalate_error
    @routing.escalate_error
    @test_scheduler.escalate_error
    @monitoring.escalate_error
    @aped.kill
    @logger.log(@aped.status)
    @subject_tracker.kill
    @logger.log(@subject_tracker.status)
    @track_collector.kill
    @logger.log(@track_collector.status)
    @routing.kill
    @logger.log(@routing.status)
    @test_scheduler.kill
    @logger.log(@test_scheduler.status)
    @monitoring.kill
    @logger.log(@monitoring.status)
    @logger.log(@ape.pretty_output)
    @logger.log(@cmd.pretty_output)
    @cmd.run_and_wait 'sudo rm -r /tmp/artifactomat'
    @cmd.run_and_wait 'sudo rm -r /etc/artifactomat'
  end

  def run(command, cmd)
    pid = command.run(cmd)
    expect(pid).not_to eq(0)
  end

  def restart_scheduler
    @test_scheduler.escalate_error
    return unless @redis.active?
    return unless @test_scheduler.active?

    @logger.log('restarting test scheduler')
    @test_scheduler.restart

    @test_scheduler.escalate_error
  end

  def restart_subject_tracker
    @subject_tracker.escalate_error
    return unless @redis.active?
    return unless @subject_tracker.active?

    @logger.log('restarting subject_tracker ...')
    @subject_tracker.restart

    @subject_tracker.escalate_error
  end

  def restart_track_collector
    @track_collector.escalate_error
    return unless @redis.active?
    return unless @track_collector.active?

    @logger.log('restarting track_collector ...')
    @track_collector.restart

    @track_collector.escalate_error
  end

  def restart_routing
    @routing.escalate_error
    return unless @redis.active?
    return unless @routing.active?

    @logger.log('restarting routing ...')
    @routing.restart

    @routing.escalate_error
  end

  def restart_monitoring
    @monitoring.escalate_error
    return unless @redis.active?
    return unless @monitoring.active?

    @logger.log('restarting monitoring ...')
    @monitoring.restart

    @monitoring.escalate_error
  end

  def restart_aped
    @aped.escalate_error
    return unless @redis.active?
    return unless @aped.active?

    @logger.log('restarting aped...')
    @aped.restart
    sleep(5)

    @aped.escalate_error
  end

  def restart_services
    restart_subject_tracker
    restart_track_collector
    restart_routing
    restart_scheduler
    restart_monitoring
    restart_aped
  end

  context 'Startup' do
    it 'starting redis' do
      @redis.start
      abort("\e[31m\nCAUTION!!\e[0m:\n#{@redis.status}") unless @redis.active?
    end

    it 'starting routing' do
      @routing.start
    end

    it 'starting monitoring' do
      @monitoring.start
    end

    it 'starting apenames' do
      @apenames.start
      sleep 5
      expect(tcp_port_open?('localhost', 5252)).to be_truthy
    end

    it 'starting track_collector' do
      @track_collector.start
    end

    it 'starting subject_tracker' do
      @subject_tracker.start
    end

    it 'starting test scheduler' do
      @test_scheduler.start
    end

    it 'starting aped' do
      @aped.start
      sleep(5)
      @aped.escalate_error
      expect(@aped.active?).to be_truthy, 'Problem with starting aped!'
      expect(@apenames.active?).to be_truthy
    end

    it 'aped is listening' do
      expect(tcp_port_open?('localhost', 8765)).to be_truthy
    end

    it 'trackcollector is listening' do
      expect(tcp_port_open?('localhost', 5555)).to be_truthy
    end

    it 'get status from aped' do
      expect(@aped.active?).to be_truthy
      @ape.run_and_wait 'ape status'
      output = @ape.stdout.join('')
      state = YAML.safe_load(output, [Symbol])
      expect(state['TrackCollector']['Server']['Running?']).to match('true')
      expect(state['TestScheduler']['Running?']).to match('true')
      expect(state['Monitoring']['Running?']).to match('true')
      expect(state['SubjectTracker']['Running?']).to match('true')
    end
  end

  context 'commands' do
    before(:context) do
      restart_services
    end

    it 'shows help with an unknown command' do
      @ape.run_and_wait_unsafe('ape unknown-command')
      expect(@ape.stdout.join('')).to match('--help         - Show this message')
      expect(@ape.stderr.join('')).to match("error: Unknown command 'unknown-command'")
      expect(@ape.exit_code).to_not be(0)
    end

    it 'gives bad exit code with known command and unknown resource' do
      @ape.run_and_wait_unsafe("ape season new -l SeasonX -r 'minimal' 2")
      expect(@ape.exit_code).to_not eq(0)
    end

    it 'help' do
      @ape.run_and_wait 'ape help'
      expect(@ape.stdout.join('')).to match('--help         - Show this message')
    end

    it 'info' do
      expect { @ape.run_and_wait('ape info') }.not_to raise_error
    end

    context 'program' do
      before(:context) do
        restart_services
      end

      it 'new' do
        @ape.run_and_wait "ape program new -l 'TestProgram'"

        expect(TestProgram.count).to eq(1)
        expect(TestProgram.first.label).to eq('TestProgram')
      end

      it 'list' do
        @ape.run_and_wait('ape program list')
        stdout = @ape.stdout.join('')
        # expect(stdout).to include('id: 1')
        expect(stdout).to include('label: TestProgram')
      end

      it 'show' do
        @ape.run_and_wait 'ape program show 1'
        stdout = @ape.stdout.join('')
        expect(stdout).to include('id: 1')
        expect(stdout).to include('label: TestProgram')
      end

      it 'edit' do
        @ape.run_and_wait 'ape program edit -l "Edited Program" 1'

        expect(@ape.stdout.join('')).to match('No Content')
        expect(TestProgram.count).to eq(1)
        expect(TestProgram.first.label).to eq('Edited Program')
      end

      it 'delete a program w/o series' do
        @ape.run_and_wait 'ape program delete 1'

        expect(@ape.stdout.join('')).to match('No Content')
        expect(TestProgram.count).to eq(0)
      end

      it 'delete a program with a series' do
        tp = TestProgram.create(label: 'Program with Series')
        tp.test_series.create(label: 'Series', group_file: 'muster.csv', start_date: Time.now + 20)

        @ape.run_and_wait "ape program delete #{tp.id}"

        expect(@ape.stdout.join('')).to match('No Content')
        expect(TestProgram.count).to eq(0)
        expect(TestSeries.count).to eq(0)
      end
    end

    context 'series' do
      before(:all) do
        TestProgram.delete_all
        TestSeries.delete_all
        restart_services
        @tp = TestProgram.create(label: 'TestProgram')
      end

      it 'new with valid group-file' do
        @ape.run_and_wait "ape series new -l 'TestSeries' -s 'in 20 sec' -e 'in 11 min' -g muster.csv #{@tp.id}"

        expect(@ape.stdout.join('')).to match('Series created. Remaining series testtime: 00h 10m')
        expect(TestSeries.count).to eq(1)
        expect(TestSeries.first.label).to eq('TestSeries')
        expect(TestSeries.first.group_file).to eq('muster.csv')
        expect(TestSeries.first.test_program.id).to eq(@tp.id)
      end

      it 'new with non-existant group-file' do
        @ape.run_and_wait_unsafe "ape series new -l 'TestSeries' -s 'in 20 sec' -g group.csv #{@tp.id}"

        expect(@ape.exit_code).not_to eq(0)
        expect(@ape.stdout.join('')).to match("Subject group 'group.csv' does not exist.")
        expect(TestSeries.count).to eq(1)
      end

      it 'list' do
        @ape.run_and_wait 'ape series list'

        stdout = @ape.stdout.join('')
        expect(stdout).to match("id: #{@tp.test_series.first.id}")
        expect(stdout).to match('label: TestSeries')
      end

      it 'edit' do
        @ape.run_and_wait "ape series edit -l 'Edited Series' #{@tp.test_series.first.id}"

        expect(@ape.stdout.join('')).to match('Series updated')
        expect(TestSeries.count).to eq(1)
        expect(TestSeries.first.label).to eq('Edited Series')
      end

      it 'show' do
        @ape.run_and_wait "ape series show #{@tp.test_series.first.id}"

        stdout = @ape.stdout.join('')
        expect(stdout).to match("id: #{@tp.test_series.first.id}")
        expect(stdout).to match('label: Edited Series')
        expect(stdout).to match('group_file: muster.csv')
      end

      it 'delete a series w/o a season' do
        @ape.run_and_wait "ape series delete #{@tp.test_series.first.id}"

        expect(@ape.stdout.join('')).to match('No Content')
        expect(TestSeries.count).to eq(0)
        expect(TestProgram.count).to eq(1)
      end

      it 'delete a series with a season' do
        ts = @tp.test_series.create(label: 'Series with Season', group_file: 'muster.csv', start_date: Time.now + 20)
        ts.test_seasons.create(label: 'Season', recipe_id: 'minimal', duration: 10, screenshots_folder: '')

        @ape.run_and_wait "ape series delete #{ts.id}"

        expect(@ape.stdout.join('')).to match('No Content')
        expect(TestProgram.count).to eq(1)
        expect(TestSeries.count).to eq(0)
        expect(TestSeason.count).to eq(0)
      end

      it 'schedule' do
        start = (Time.now + 30).utc
        ts = @tp.test_series.create(label: 'Series', group_file: 'muster.csv', start_date: start, end_date: start + 10 * 60)
        ts.test_seasons.create(label: 'Season', duration: 10, recipe_id: 'minimal', recipe_parameters: { 'parameter' => '21' }, screenshots_folder: '')
        @wait_until = Time.now + 30

        @ape.run_and_wait "ape series schedule #{ts.id}"

        expect(@ape.stdout.join('')).to match('No Content')
        expect(TestEpisode.count).to eq(2)
        expect(TestEpisode.first.schedule_start).not_to be_nil
        expect(TestEpisode.first.schedule_end).not_to be_nil
      end

      it 'schedule a second time' do
        ts = TestSeries.first
        @ape.run_and_wait "ape series schedule #{ts.id}"
        expect(@ape.stdout.join('')).to match('No Content')
        expect(TestEpisode.count).to eq(2)
        expect(TestEpisode.first.schedule_start).not_to be_nil
        expect(TestEpisode.first.schedule_end).not_to be_nil
      end

      it 'prepare' do
        ts = TestSeries.first
        @ape.run_and_wait "ape series prepare #{ts.id}"
        sleep 10
        expect(File.file?('/tmp/artifactomat/2/artifact')).to be_truthy
        expect(File.file?('/tmp/artifactomat/2/infrastructure')).to be_truthy
        expect(@ape.stdout.join('')).to match('No Content')
      end

      it 'approve' do
        data1 = Hash['mustermann', { ip: '127.0.0.199', action: 'login' }]
        connection = build_connection(@cert_path)
        connection.puts(data1.to_yaml)
        connection.close
        data2 = Hash['terelanie', { ip: '127.0.0.200', action: 'login' }]
        connection = build_connection(@cert_path)
        connection.puts(data2.to_yaml)
        connection.close

        ts = TestSeries.first
        @ape.run_and_wait "ape series approve #{ts.id}"
        expect(@ape.stdout.join('')).to match('No Content')
        sleep_until @wait_until # the series is sheduled then
        sleep 20
        expect(File.file?('/tmp/artifactomat/2/arm')).to be_truthy
      end

      it 'abort' do
        ts = TestSeries.first
        @ape.run_and_wait "ape series abort #{ts.id}"
        expect(@ape.stdout.join('')).to match('No Content')
      end
    end

    context 'season' do
      before(:all) do
        TestProgram.delete_all
        TestSeries.delete_all
        TestSeason.delete_all
        TestEpisode.delete_all
        restart_services
        @start = Chronic.parse('tomorrow at 08:00am').utc
        @end = Chronic.parse('tomorrow at 09:00am').utc
        @tp = TestProgram.create(label: 'TestProgram')
        @ts = @tp.test_series.create(label: 'TestSeries', group_file: 'muster.csv', start_date: @start, end_date: @end)
      end

      it 'new' do
        @ape.run_and_wait "ape season new -l 'TestSeason' -d '10m' -r 'minimal' -p 'parameter:12' #{@ts.id}"

        expect(@ape.stdout.join('')).to match('Season created. Remaining series testtime: 00h 50m')
        expect(TestSeason.count).to eq(1)
        expect(TestSeason.first.label).to eq('TestSeason')
        expect(TestSeason.first.duration).to eq(10)
        expect(TestSeason.first.recipe_id).to eq('minimal')
        expect(TestSeason.first.recipe_parameters).to eq('parameter' => '12')
        expect(TestSeason.first.test_series.id).to eq(@ts.id)
        expect(TestSeason.first.test_series.test_program.id).to eq(@tp.id)
      end

      it 'list' do
        @ape.run_and_wait 'ape season list'
        stdout = @ape.stdout.join('')

        expect(stdout).to match("id: #{@tp.test_series.first.test_seasons.first.id}")
        expect(stdout).to match('label: TestSeason')
        expect(stdout).to match('duration: 10')
        expect(stdout).to match('recipe_id: minimal')
        expect(stdout).to match("parameter: '12'")
      end

      it 'edit' do
        @ape.run_and_wait "ape season edit -l 'Edited Season' -d 15m -p 'parameter:42' #{@tp.test_series.first.test_seasons.first.id}"

        expect(@ape.stdout.join('')).to match('Season edited. Remaining series testtime: 00h 45m')
        expect(TestSeason.count).to eq(1)
        expect(TestSeason.first.label).to eq('Edited Season')
        expect(TestSeason.first.duration).to eq(15)
        expect(TestSeason.first.recipe_id).to eq('minimal')
        expect(TestSeason.first.recipe_parameters).to eq('parameter' => '42')
      end

      it 'show' do
        @ape.run_and_wait "ape season show #{@tp.test_series.first.test_seasons.first.id}"
        stdout = @ape.stdout.join('')

        expect(stdout).to match("id: #{@tp.test_series.first.test_seasons.first.id}")
        expect(stdout).to match('label: Edited Season')
        expect(stdout).to match('recipe_id: minimal')
        expect(stdout).to match("parameter: '42'")
      end

      it 'delete a season w/o an episode' do
        @ape.run_and_wait "ape season delete #{@tp.test_series.first.test_seasons.first.id}"

        expect(@ape.stdout.join('')).to match('No Content')
        expect(TestSeason.count).to eq(0)
        expect(TestSeries.count).to eq(1)
        expect(TestProgram.count).to eq(1)
      end

      it 'delete a season with an episode' do
        season = @ts.test_seasons.create(label: 'Season', recipe_id: 'minimal', duration: 10, screenshots_folder: '')
        season.test_episodes.create(subject_id: 1)

        @ape.run_and_wait "ape season delete #{season.id}"

        expect(@ape.stdout.join('')).to match('No Content')
        expect(TestProgram.count).to eq(1)
        expect(TestSeries.count).to eq(1)
        expect(TestSeason.count).to eq(0)
        expect(TestEpisode.count).to eq(0)
      end
    end

    describe 'info' do
      before :all do
        restart_services
      end

      it 'shows news about prepared series after restart' do
        @ape.run_and_wait('ape info')
        out = @ape.stdout.join('')
        expect(out).to match(/Series ".+" \(\d+\) fully prepared, you may approve now/)
      end
    end

    context 'recipe' do
      before :context do
        restart_services
      end

      it 'list' do
        dot_recipe = build(:recipe, name: '.dot')
        write_recipe('.dot.yml', dot_recipe)
        @ape.run_and_wait 'ape recipe list'
        stdout = @ape.stdout.join('')
        expect(stdout).to include('minimal')
        expect(stdout).not_to include('dot')
        @cmd.run_and_wait('sudo rm /etc/artifactomat/recipes/.dot.yml')
      end

      it 'param ID' do
        @ape.run_and_wait 'ape recipe param minimal'
        stdout = @ape.stdout.join('')
        expect(stdout).to match("parameter:\n  description: '21'\n")
      end
    end

    context 'result' do
      before(:all) do
        restart_services
        @ape.run_and_wait 'ape program new -l TestProgam'
        @ape.run_and_wait "ape series new -l 'TestSeries' -s 'in 20 sec' -e 'in 11 min' -g muster.csv #{TestProgram.last.id}"
        @ape.run_and_wait "ape season new -l TestSeason -d 10m -r 'minimal' -p 'parameter:12' #{TestSeries.last.id}"
        @ape.run_and_wait "ape series schedule #{TestSeries.last.id}"
      end

      after(:all) do
        @ape.run_and_wait "ape program delete #{TestProgram.last.id}"
      end

      it 'report series with existing ID' do
        @ape.run_and_wait "ape report series #{TestSeries.first.id} '+series_label'"
        stdout = @ape.stdout.join('')
        expect(stdout).to match('--- \[\]')
      end

      it 'report series w/o existing ID' do
        @ape.run_and_wait_unsafe 'ape report series 1000'
        stdout = @ape.stdout.join('')
        expect(stdout).to match('command not found')
      end
    end

    context 'subject' do
      it 'list' do
        @ape.run_and_wait 'ape subject list'
        stdout = @ape.stdout.join('')
        expect(stdout).to match('muster.csv')
      end
    end

    describe 'reset' do
      before(:all) do
        restart_services
        @ape.run_and_wait('ape program new -l ResetTestProg')
        @ape.run_and_wait("ape series new -l 'ResetTestSeri' -s 'in 90 sec' -e 'in 13 min' -g muster.csv #{TestProgram.last.id}")
        @ape.run_and_wait("ape series schedule #{TestSeries.last.id}")
        expect(TestSeries.last.status).to eq('scheduled')
        # Successful preparation creates an info message that is stored in the vstore
        @ape.run_and_wait("ape series prepare #{TestSeries.last.id}")
        begin
          Timeout.timeout(30) do
            @aped.each_new_line do |line|
              break if line.match?("<APE> [Series \"ResetTestSeri\" (#{TestSeries.last.id}) fully prepared, you may approve now.]")
            end

            sleep 1
          end
        rescue Timeout::Error
          raise 'Timeout while waiting for framework to allow approval.'
        end
        @ape.run_and_wait("ape series abort #{TestSeries.last.id}")
      end

      it 'resets the volatile store' do
        @ape.run_and_wait 'ape reset -f'
        @ape.run_and_wait 'ape info'
        # The preparation info message should not be present
        info = @ape.stdout.join('')
        expect(info).to eq('')
      end
    end

    context 'trace' do
      before(:all) do
        restart_services
        @ape.run_and_wait 'ape program new -l TestProg'
        @ape.run_and_wait "ape series new -l 'TestSeri' -s 'in 90 sec' -e 'in 13 min' -g muster.csv #{TestProgram.last.id}"
        @ape.run_and_wait "ape season new -l TestSeas -d 10m -r 'minimal' -p 'parameter:12' #{TestSeries.last.id}"
        @ape.run_and_wait "ape series schedule #{TestSeries.last.id}"
        expect(TestSeries.last.status).to eq('scheduled')
        @wait_until = Time.now + 90
        @ape.run_and_wait "ape series prepare #{TestSeries.last.id}"
        sleep 60
        data1 = Hash['mustermann', { ip: '127.0.0.199', action: 'login' }]
        connection = build_connection(@cert_path)
        connection.puts(data1.to_yaml)
        connection.close
        data2 = Hash['terelanie', { ip: '127.0.0.200', action: 'login' }]
        connection = build_connection(@cert_path)
        connection.puts(data2.to_yaml)
        connection.close
        expect(TestSeries.last.status).to eq('prepared')
        @ape.run_and_wait "ape series approve #{TestSeries.last.id}"
        expect(TestSeries.last.status).to eq('approved')
        sleep_until @wait_until
        sleep 3
      end

      after(:all) do
        @ape.run_and_wait "ape series abort #{TestSeries.last.id}"
        @ape.run_and_wait "ape program delete #{TestProgram.last.id}"
      end

      it 'import w/o existing file' do
        @ape.run_and_wait_unsafe('ape import trace notexistingfile.txt')
        stdout = @ape.stdout.join('')
        expect(@ape.exit_code).to eq(1)
        expect(stdout.inspect).to match('""')
      end

      it 'import with existing file' do
        write_trace(TestEpisode.last(2).first, 'muster.csv/1')
        @ape.run_and_wait 'ape import trace /tmp/artifactomat/trace.yml'
        stdout = @ape.stdout.join('')
        stderr = @ape.stderr.join('')
        expect(stderr).to include('Read 4 line(s), 2 trace(s) found.')
        expect(stdout).to include('phishing link clicked')
        expect(stdout).to include('data filled')
      end

      it 'import helpdesk file with trackfilter' do
        write_helpdesk_trace Time.now + 5 * 60
        @ape.run_and_wait 'ape import trace /tmp/artifactomat/helpdesk_trace.csv'
        stdout = @ape.stdout.join('')
        stderr = @ape.stderr.join('')
        expect(stderr).to include('Read 7 line(s), 2 trace(s) found.')
        expect(stdout).to include('helpdesk')
      end

      it 'import generic helpdesk file' do
        @ape.run_and_wait 'ape import helpdesk --format \'(?<subject_phone>\d+);(?<%d.%m.%Y %H:%M:%S>.+);\d+;\d+\' /tmp/artifactomat/helpdesk_trace.csv'
        stdout = @ape.stdout.join('')
        stderr = @ape.stderr.join('')
        expect(stderr).to include('Read 7 line(s), 1 trace(s) found.')
        expect(stdout).to include('helpdesk')
      end

      it 'imports a real life example variant 1' do
        write_real_life_example_helpdesk_logs_variant_1 Time.now + 5 * 60
        @ape.run_and_wait "ape import --series #{TestSeries.last.id} " + 'helpdesk --format \'(?<subject_phone>\d+),"[^"]+","(?<subject_surname>\w+), (?<subject_name>\w+) ",".*,(?<%m\/%d\/%Y %k:%M:%S>.+)\' /tmp/artifactomat/helpdesk_trace_rl_1.csv'
        stdout = @ape.stdout.join('')
        stderr = @ape.stderr.join('')
        expect(stderr).to include('Read 6 line(s), 2 trace(s) found.')
        expect(stdout).to include('helpdesk')
      end

      it 'imports a real life example variant 2' do
        write_real_life_example_helpdesk_logs_variant_2 Time.now + 5 * 60
        @ape.run_and_wait "ape import --series #{TestSeries.last.id} " + 'helpdesk --format \'\d+,(?<subject_phone>\d+),(?<%m\/%d\/%Y %k:%M>.+),\d+,.+,\d+,\d+,\d+,\d+\' /tmp/artifactomat/helpdesk_trace_rl_2.csv'
        stdout = @ape.stdout.join('')
        stderr = @ape.stderr.join('')
        expect(stderr).to include('Read 6 line(s), 2 trace(s) found.')
        expect(stdout).to include('helpdesk')
      end

      it 'imports a real life example variant 3' do
        write_real_life_example_helpdesk_logs_variant_3 Time.now + 5 * 60
        @ape.run_and_wait "ape import --series #{TestSeries.last.id} " + 'helpdesk --format \'(^(?<subject_id>[^,]+)(,[^,]*){9},(\w{2} (?<%d.%m.%Y %k:%M>[^,]+))$)|(^(?<subject_id>[^,]+)(,[^,]*){3},(?<%m\/%d\/%Y %k:%M>[^,]+)?(,[^,]*){6}$)\' /tmp/artifactomat/helpdesk_trace_rl_3.csv'
        stdout = @ape.stdout.join('')
        stderr = @ape.stderr.join('')
        expect(stderr).to include('Read 6 line(s), 2 trace(s) found.')
        expect(stdout).to include('helpdesk')
      end

      it 'imports a real life example variant 4' do
        write_real_life_example_helpdesk_logs_variant_4 Time.now + 5 * 60
        @ape.run_and_wait "ape import --series #{TestSeries.last.id} " + 'helpdesk --format \'^[^,]*,(?<subject_id>[^,]+),(?<%d\/%m\/%Y %k:%M:%S>[^,]+),[^,]*,[^,]*$\' /tmp/artifactomat/helpdesk_trace_rl_4.csv'
        stdout = @ape.stdout.join('')
        stderr = @ape.stderr.join('')
        expect(stderr).to include('Read 6 line(s), 2 trace(s) found.')
        expect(stdout).to include('helpdesk')
      end

      it 'get trace from satellite' do
        @cmd.run_and_wait 'sudo touch /tmp/artifactomat/satellite_trace'
        @cmd.run "ruby /etc/artifactomat/satellite.rb -f /tmp/artifactomat/satellite_trace --host 127.0.0.1 --port 5555 --ca #{@cert_path}/public/ca.pem --pkey #{@cert_path}/private/client.itsape.key --cert #{@cert_path}/public/client.itsape.pem"
        sleep 3
        expect(@cmd.running?).to be_truthy
        expect(@cmd.pid).to be
        expect(@cmd.exit_code).to be_nil
        target_te_count = TrackEntry.count + 2
        write_satellite_trace
        breakout = 7
        sleep 1 while TrackEntry.count <= target_te_count && (breakout -= 1) > 1

        te = TrackEntry.last(2)
        expect(te.length).to eq(2)

        course_of_actions = Set.new([te[0].course_of_action,
                                     te[1].course_of_action])
        subject_ids = Set.new([te[0].subject_id, te[1].subject_id])
        expect(course_of_actions).to eq(Set.new(['satellite trace',
                                                 'satellite trace']))
        expect(subject_ids).to eq(Set.new(['muster.csv/2', 'muster.csv/1']))

        @cmd.kill
        expect(@cmd.running?).to_not be_truthy
      end
    end

    context 'SubjectTracker' do
      before(:all) do
        TestProgram.delete_all
        TestSeries.delete_all
        TestSeason.delete_all
        restart_services
        @ape.run_and_wait 'ape program new -l TestProg'
        @ape.run_and_wait "ape series new -l 'TestSeri' -s 'in 40 sec' -e 'in 12 min' -g muster.csv #{TestProgram.last.id}"
        @ape.run_and_wait "ape season new -l TestSeas -d 10m -r 'minimal' -p 'parameter:12' #{TestSeries.last.id}"
        @ape.run_and_wait "ape series schedule #{TestSeries.last.id}"
        @wait_until = Time.now + 40
        @ape.run_and_wait "ape series prepare #{TestSeries.last.id}"
        sleep 20
        @ape.run_and_wait "ape series approve #{TestSeries.last.id}"
        sleep 20
        @logger.log(@ape.stdout.join(''))
        sleep_until @wait_until
        sleep 2
      end

      def sendto_st_plugin(cert_path, data)
        connection = build_connection(cert_path)
        connection.puts(data.to_yaml)
        connection.close
      end

      it 'receives logins and creates entries in DB' do
        sendto_st_plugin @cert_path, Hash['terelanie', { ip: '127.0.0.66', action: 'login' }]
        sendto_st_plugin @cert_path, Hash['mustermann', { ip: '127.0.0.33', action: 'login' }]

        sleep(1)

        expect(SubjectHistory.where(subject_id: 'muster.csv/2', ip: '127.0.0.66', session_end: nil)).to exist
      end

      it 'receives a changed ip and updates an entry in DB' do
        sendto_st_plugin @cert_path, Hash['terelanie', { ip: '127.0.0.166', action: 'login' }]

        sleep(1)

        expect(SubjectHistory.where(subject_id: 'muster.csv/2', ip: '127.0.0.66').where.not(session_end: nil)).to exist
        expect(SubjectHistory.where(subject_id: 'muster.csv/2', ip: '127.0.0.166', session_end: nil)).to exist
        expect(SubjectHistory.where(subject_id: 'muster.csv/1')).to exist
      end

      it 'receives logouts and updates entries in DB' do
        sendto_st_plugin @cert_path, Hash['terelanie', { ip: '127.0.0.166', action: 'logout' }]
        sendto_st_plugin @cert_path, Hash['mustermann', { ip: '127.0.0.33', action: 'login' }]

        sleep(1)

        expect(SubjectHistory.where(subject_id: 'muster.csv/1', ip: '127.0.0.33', session_end: nil)).to exist
        expect(SubjectHistory.where(subject_id: 'muster.csv/2', ip: '127.0.0.166').where.not(session_end: nil)).to exist
      end

      it 'updates entry in DB when subject times out' do
        sendto_st_plugin @cert_path, Hash['mustermann', { ip: '127.0.0.55', action: 'login' }]

        sleep(1)
        expect(SubjectHistory.where(subject_id: 'muster.csv/1', ip: '127.0.0.55', session_end: nil)).to exist

        sleep(17) # 15 Seconds is the timeout

        expect(SubjectHistory.where(subject_id: 'muster.csv/1', ip: '127.0.0.55').where.not(session_end: nil)).to exist
      end

      it 'does not have old data in short term memory' do
        data = Hash['terelanie', { ip: '127.0.0.33', action: 'login' }]
        sendto_st_plugin @cert_path, data
        sleep(10)
        data = Hash['mustermann', { ip: '127.0.0.33', action: 'login' }]
        sendto_st_plugin @cert_path, data
        sleep(10)

        @cmd.run_and_wait 'sudo redis-cli -n 1 -s /var/run/redis-artifactomat/redis-server.sock KEYS \*'
        redis_db = @cmd.stdout.join('')
        expect(redis_db).to include '127.0.0.33'
        expect(redis_db).to include 'muster.csv/1'
        expect(redis_db).not_to include 'muster.csv/2'

        @cmd.run_and_wait 'sudo redis-cli -n 1 -s /var/run/redis-artifactomat/redis-server.sock KEYS \* | xargs -n 1 sudo redis-cli -n 1 -s /var/run/redis-artifactomat/redis-server.sock get'
        redis_db = @cmd.stdout.join('')
        expect(redis_db).to include '127.0.0.33'
        expect(redis_db).to include 'muster.csv/1'
        expect(redis_db).not_to include 'muster.csv/2'
      end
    end
  end
end
