# frozen_string_literal: true

require_relative 'system_spec_helper'
require 'active_record'
require 'timeout'
require 'socket'

def write_plugin_conf_routing
  cmd = Command.new
  conf = '---
  plugins:
    ClientProxyAdapter:
      type: receiver
      file_name: client_proxy_adapter.rb
      class_name: ClientProxyAdapter
      subject_timeout: 60
      cert_path: /etc/artifactomat/certs
'
  File.open('/tmp/subjecttracker.yml', 'w') do |f|
    f.write conf
  end
  cmd.run_and_wait('sudo mv /tmp/subjecttracker.yml /etc/artifactomat/conf.d/subjecttracker.yml')
end

describe 'ROUTING SPEC', system: true, order: :defined do
  before(:all) do
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']

    @logger = SpecLogger.new('routing_system_spec')
    @logger.log(db_config)
    ActiveRecord::Base.establish_connection(db_config)

    @redis = SystemdService.new('redis-server@artifactomat.service')
    @subject_tracker = SystemdService.new('artifactomat-subject-tracker.service')
    @track_collector = SystemdService.new('artifactomat-track-collector.service')
    @test_scheduler = SystemdService.new('artifactomat-ape-scheduler.service')
    @monitoring = SystemdService.new('artifactomat-ape-monitoring.service')
    @routing = SystemdService.new('artifactomat-routing.service')
    @apenames = SystemdService.new('artifactomat-apenames.service')
    @aped = SystemdService.new('artifactomat-aped.service')
    @ape = Command.new
    @cmd = Command.new

    @cert_path = '/etc/artifactomat/certs'

    @cmd.run_and_wait('sudo sysctl -w net.ipv4.ip_forward=1')
    @cmd.run_and_wait('sudo sysctl -w net.ipv4.conf.all.route_localnet=1')

    @cmd.run_and_wait('sudo artifactomat_setup db')
    @cmd.run_and_wait('sudo artifactomat_setup app --yes', 120)

    ie1 = build(:infrastructure_element, generate_script: 'touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/infrastructure',
                                         monitor_script: 'exit 0',
                                         destroy_script: 'rm -f $ARTIFACTOMAT_RECIPE_TEMP_DIR/infrastructure',
                                         ports: [8081],
                                         names: ['its.ape1'])
    ie2 = build(:infrastructure_element, generate_script: 'exit 0',
                                         monitor_script: 'exit 0',
                                         destroy_script: 'exit 0',
                                         ports: [8082],
                                         names: ['its.ape2'])
    recipe = build(:recipe, infrastructure: [ie1, ie2],
                            deploy_script: 'exit 0',
                            artifact_script: 'exit 0',
                            arm_script: 'exit 0',
                            disarm_script: 'exit 0',
                            duration: 10)
    recipe.trackfilters.push(build_helpdesk_tf(recipe))
    recipe.trackfilters.push(build_satellite_tf(recipe))
    write_recipe('routing.yml', recipe)

    subjects = "id,name,surname,userid,email,phone\n" \
               "1,Max,Mustermann,mustermann,max@muster.de,93060\n" \
               "2,Terelanie,Musterfrau,terelanie,terelanie@muster.de,123456\n"
    write_subjects('muster.csv', subjects)
    write_plugin_conf
    set_log_lvl_debug
  end

  def pingable(host, port, debug = false)
    Thread.new do
      server = TCPServer.new(host, port)
      @logger.log('## listening') if debug
      # rubocop:disable Lint/AssignmentInCondition
      while client = server.accept
        # rubocop:enable Lint/AssignmentInCondition
        @logger.log('-- accepted connection') if debug
        msg = client.gets
        @logger.log("-- received: #{msg}") if debug
        client.puts 'pong' if msg.include? 'ping'
        client.close
        break
      end
      server.close
      msg
    end
  end

  def kill_aped
    @aped.kill
    @logger.log(@aped.status)
  end

  def kill_subject_tracker
    @subject_tracker.kill
    @logger.log(@subject_tracker.status)
  end

  def kill_track_collector
    @track_collector.kill
    @logger.log(@track_collector.status)
  end

  def kill_routing
    @routing.kill
    @logger.log(@routing.status)
  end

  def kill_scheduler
    @test_scheduler.kill
    @logger.log(@test_scheduler.status)
  end

  def kill_monitoring
    @monitoring.kill
    @logger.log(@monitoring.status)
  end

  after(:all) do
    kill_aped
    kill_subject_tracker
    kill_track_collector
    kill_scheduler
    kill_routing
    kill_monitoring
  end

  after(:each) do |example|
    if example.exception
      kill_aped
      kill_subject_tracker
      kill_track_collector
      kill_routing
      kill_monitoring
      @cmd.run_and_wait('dig @1.1.1.1 google.de +noall +answer')
      @logger.log('Manual DNS request:')
      @logger.log(@cmd.pretty_output)
    end
  end

  context 'Routing' do
    before(:all) do
      @ip = derive_ip

      @cmd.run_and_wait 'docker run --rm -it -d --name system_spec_docker debian:stable-slim /bin/bash', 60
      @cmd.run_and_wait 'docker exec system_spec_docker apt-get update', 60
      @cmd.run_and_wait 'docker exec system_spec_docker apt-get install -y netcat net-tools iputils-ping', 60
      @cmd.run_and_wait "docker exec system_spec_docker bash -c \"ifconfig eth0 | grep -Po \'(?<=inet) [0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\'\"", 60
      @docker_ip = @cmd.stdout.first.strip

      @cmd.run_and_wait('docker run --rm -it -d --name system_spec_docker_non_subject debian:stable-slim /bin/bash')
      @cmd.run_and_wait('docker exec system_spec_docker_non_subject apt-get update')
      @cmd.run_and_wait('docker exec system_spec_docker_non_subject apt-get install -y netcat net-tools')
      @cmd.run_and_wait('docker exec system_spec_docker_non_subject apt-get install -y net-tools')
      @cmd.run_and_wait("docker exec system_spec_docker_non_subject bash -c \"ifconfig eth0 | grep -Po \'(?<=inet) [0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\'\"")
      @docker_ip_non_subject = @cmd.stdout.first.strip

      @cmd.run_and_wait("sudo sed -i 's/external_ip: 192.168.1.101/external_ip: #{@ip}/g' /etc/artifactomat/conf.yml")

      write_plugin_conf_routing

      # start is up...
      @redis.start
      abort("\e[31m\nCAUTION!!\e[0m:\n#{@redis.status}") unless @redis.active?

      @routing.start
      @subject_tracker.start
      @track_collector.start
      @test_scheduler.start
      @monitoring.start

      @logger.log('STARTING APED!')
      @aped.start
      sleep(2)

      @apenames.start
      sleep 5
      apenames_running = tcp_port_open?('127.0.0.1', 5252)
      raise 'APENAMES NOT STARTED' unless apenames_running

      @logger.log('STARTED APENAMES!')

      @ape.run_and_wait('ape program new -l TestProgX')
      @ape.run_and_wait("ape series new -l 'TestSeriX' -g muster.csv -s 'in 40 sec' -e 'in 11 min' #{TestProgram.last.id}")
      @ape.run_and_wait("ape season new -l TestSeasX -d 10m -r 'routing' #{TestSeries.last.id}")
      @ape.run_and_wait("ape series schedule #{TestSeries.last.id}")
      @next_up = Time.now + 40
      @ape.run_and_wait("ape series prepare #{TestSeries.last.id}")

      sleep 30 # big ape is big slow and monitoring has to be waited on...
      # puts '---8<---- PARAMETERS -------->8---'
      # puts "VM IP:      #{@ip}"
      # puts "DOCKER IP:  #{@docker_ip}"
      # puts "DOCKER2 IP: #{@docker_ip_non_subject}"
      # puts '---8<---- PARAMETERS -------->8---'

      data = Hash['mustermann', { ip: '127.0.0.101', action: 'login' }]
      connection = build_connection(@cert_path)
      connection.puts(data.to_yaml)
      connection.close
      sleep(1)
      data = Hash['terelanie', { ip: '127.0.0.104', action: 'login' }]
      connection = build_connection(@cert_path)
      connection.puts(data.to_yaml)
      connection.close

      sleep(2)
      @ape.run_and_wait("ape series approve #{TestSeries.last.id}")
      sleep 30
      sleep_until @next_up
    end

    after(:all) do
      @apenames.start
      @logger.log(@apenames.status)
      @cmd.run_and_wait('docker rm -f system_spec_docker')
      @cmd.run_and_wait('docker rm -f system_spec_docker_non_subject')
    end

    it 'checks whether ips are set' do
      expect(@docker_ip).not_to be_empty
      expect(@docker_ip_non_subject).not_to be_empty
    end

    context 'Nameserver' do
      it 'changes specific names for a specific subject' do
        @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.101 its.ape1 +noall +answer')
        stdout = @cmd.stdout.join('')
        # puts stdout
        # @cmd.run_and_wait('sudo netstat -tulpn')
        # puts @cmd.stdout.join('')
        regex = Regexp.new('its.ape1\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
        expect(stdout).to match(regex)
        ip = regex.match(stdout)[:ip]
        expect(ip).to eql(@ip)

        @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.101 its.ape2 +noall +answer')
        stdout = @cmd.stdout.join('')
        regex = Regexp.new('its.ape2\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
        expect(stdout).to match(regex)
        ip = regex.match(stdout)[:ip]
        expect(ip).to eql(@ip)
      end

      it 'does not crash on à±<8a>#001a.t-mobile.com' do
        @cmd.run_and_wait_unsafe('dig @127.0.0.1 -p 5252 -b 127.0.0.101 "à±<8a>#001a.t-mobile.com" +noall +answer')
        @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.101 its.ape1 +noall +answer')
        stdout = @cmd.stdout.join('')
        regex = Regexp.new('its.ape1\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
        expect(stdout).to match(regex)
        ip = regex.match(stdout)[:ip]
        expect(ip).to eql(@ip)
      end

      context 'its.apt is magic' do
        it 'resolves its.apt for a subject to 127.0.0.1' do
          @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.101 its.apt +noall +answer')
          stdout = @cmd.stdout.join('')
          regex = Regexp.new('its.apt\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
          expect(stdout).to match(regex)
          ip = regex.match(stdout)[:ip]
          expect(ip).to eql('127.0.0.1')
        end

        it 'resolves its.apt for a non-subject to 127.0.0.2' do
          @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.102 its.apt +noall +answer')

          stdout = @cmd.stdout.join('')
          regex = Regexp.new('its.apt\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
          expect(stdout).to match(regex)
          ip = regex.match(stdout)[:ip]
          expect(ip).to eql('127.0.0.2')
        end
      end

      it 'passes through other requests to the real DNS' do
        @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.101 google.de +noall +answer')

        stdout = @cmd.stdout.join('')
        regex = Regexp.new('google.de\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
        expect(stdout).to match(regex)
        ip = regex.match(stdout)[:ip]
        expect(ip).not_to eql(@ip)
      end

      it 'does not change name for other subjects' do
        @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.102 its.ape1 +noall +answer')

        stdout = @cmd.stdout.join('')
        regex = Regexp.new('its.ape1\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
        expect(stdout).not_to match(regex)

        @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.102 its.ape2 +noall +answer')

        stdout = @cmd.stdout.join('')
        regex = Regexp.new('its.ape2\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
        expect(stdout).not_to match(regex)

        @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.102 google.de +noall +answer')

        stdout = @cmd.stdout.join('')
        regex = Regexp.new('google.de\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
        expect(stdout).to match(regex)
        ip = regex.match(stdout)[:ip]
        expect(ip).not_to eql(@ip)
      end
    end

    context 'Routing Module' do
      it 'reroutes for ie2 on subject scheduled' do
        data = Hash['mustermann', { ip: @docker_ip, action: 'login' }]
        connection = build_connection(@cert_path)
        connection.puts(data.to_yaml)
        connection.close

        # TODO: aped shall be fast enough, *always* fast enough
        # DONOT increase this value, only decrease it.
        sleep 0.5

        netcat = pingable('localhost', 9998)

        @cmd2 = Command.new
        @cmd2.run_and_wait("docker exec system_spec_docker bash -c \'echo \"ping\" | nc #{@ip} 8082\'")

        # puts '*** messenger output'
        # puts @cmd2.stdout.join('')

        # puts "IP ............... #{@ip}"
        # puts "IP non-subject ... #{@docker_ip_non_subject}"
        # puts "IP subject ....... #{@docker_ip}"
        # debug = Command.new
        # cmd = 'sudo /sbin/iptables -t nat -L -n -v '
        # cmd = 'sudo /sbin/iptables-save'
        # puts 'https://git.cs.uni-bonn.de/itsape/framework/issues/83'
        # puts "*** RUNNING: #{cmd}"
        # ex = debug.run_and_wait cmd, 60
        # puts "exitcode: #{ex} -------------------------------- "
        # puts debug.stdout.join('').to_str
        # puts debug.stderr.join('').to_str
        # puts '------------------------------------------------ '

        expect(netcat.join(5)).not_to be(nil)
        expect(netcat.value.to_str.strip).to eql('ping')
        expect(@cmd2.stdout.first.strip).to eql('pong')
      end

      it 'reroutes not for ie2 on non-subject' do
        @cmd2 = Command.new
        @cmd3 = Command.new

        netcat = pingable('localhost', 9081)
        # @cmd.run('[ `echo "pong" | nc -l -q 0 -s 127.0.0.1 -p 9081` = "ping" ]')
        sleep 0.5

        @cmd2.run_and_wait_unsafe("docker exec system_spec_docker_non_subject bash -c \"echo \"ping\" | nc -v #{@ip} 8082\"")
        error = @cmd2.stderr.join('')
        expect(error).to include('Connection refused')

        @cmd2.run_and_wait_unsafe("docker exec system_spec_docker_non_subject bash -c \"echo 'ping' | nc -v #{@ip} 9998\"")
        expect(@cmd2.stderr.join('')).to include('Connection refused')
        netcat.terminate
      end
    end

    context 'Only the Nameserver' do
      before(:all) do
        @ape.kill
      end

      it 'passes through other requests to the real DNS' do
        @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.101 google.de +noall +answer')

        stdout = @cmd.stdout.join('')
        regex = Regexp.new('google.de\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
        expect(stdout).to match(regex)
        ip = regex.match(stdout)[:ip]
        expect(ip).not_to eql(@ip)
      end

      it 'resolves its.apt for a non-subject to 127.0.0.2' do
        @cmd.run_and_wait('dig @127.0.0.1 -p 5252 -b 127.0.0.102 its.apt +noall +answer')

        stdout = @cmd.stdout.join('')
        regex = Regexp.new('its.apt\.\s*\d*\s*IN\s*A\s*(?<ip>\d+\.\d+\.\d+\.\d+)')
        expect(stdout).to match(regex)
        ip = regex.match(stdout)[:ip]
        expect(ip).to eql('127.0.0.2')
      end
    end
  end
end
