# frozen_string_literal: true

require 'rspec'
require 'pathname'
require_relative 'system_spec_helper'

def find_artifactomat_services
  artifactomat_services = []
  Pathname.new('/etc/systemd/system/').each_child do |service_path|
    service_name = service_path.basename
    # the make-tmpdir service is just a helper that ends after execution and is thus excluded here.
    # there is a separate testcase to ensure that it is executed.
    artifactomat_services << SystemdService.new(service_name) if service_name.fnmatch?('artifactomat-*.service') && !service_name.fnmatch('artifactomat-make-tmpdir.service')
  end
  artifactomat_services
end

describe 'Service System Spec', system: true, order: :defined do
  before(:all) do
    @cmd = Command.new

    # All systemd services
    @postgres = SystemdService.new('postgresql.service')
    @redis = SystemdService.new('redis-server@artifactomat.service')

    @artifactomat = SystemdService.new('artifactomat.service')

    @cmd.run_and_wait 'sudo artifactomat_setup db'
    @cmd.run_and_wait 'sudo artifactomat_setup app --yes', 120

    @services = find_artifactomat_services

    # in case postgres or redis are already running
    @postgres.stop
    @redis.stop
  end

  describe 'startup' do
    it 'artifactomat.service starts all necessary services' do
      @artifactomat.start

      # the bundled service doesn't wait for individual services to start, so we have to wait
      sleep(10)

      expect(@artifactomat.active?).to be_truthy
      expect(@postgres.active?).to be_truthy
      expect(@redis.active?).to be_truthy
      @services.each do |service|
        expect(service.active?).to be_truthy, "Service #{service.service_name} did not start"
      end
    end

    it 'artifactomat.service stops all artifactomat services' do
      @artifactomat.stop

      sleep(3)

      expect(@artifactomat.active?).to be_falsy
      @services.each do |service|
        expect(service.active?).to be_falsy, "Service #{service.service_name} did not stop"
      end
    end

    it 'creates the temp directory on start' do
      tmp_dir = '/tmp/artifactomat'
      @artifactomat.stop
      FileUtils.rm_rf(tmp_dir)
      @artifactomat.start
      sleep(1)
      expect(Pathname.new(tmp_dir).exist?).to be_truthy
    end
  end

  context 'services are running' do
    before(:all) do
      @artifactomat.start
      sleep(10)
    end

    it 'individual services can restart without influencing other services' do
      @services.each do |service|
        remaining_services = @services.clone
        remaining_services.delete(service)
        service_invocation_id = service.invocation_id
        remaining_invocation_ids = remaining_services.map(&:invocation_id)

        service.restart

        # different invocation id => service has restarted
        expect(service.invocation_id).not_to eq(service_invocation_id)
        # same invocation id => service did not restart
        expect(remaining_invocation_ids).to eq(remaining_services.map(&:invocation_id))
        # all other services are still active
        remaining_services.each do |r_service|
          expect(r_service.active?).to be_truthy
        end
      end
    end

    it 'failed services automatically restart' do
      @services.each do |service|
        @cmd.run_and_wait("sudo kill -9 #{service.pid}")
        expect(service.active?).to be_falsy, "#{service.service_name} has not been killed"
        Timeout.timeout(10) do
          loop do
            break if service.active?
          end
        rescue Timeout::Error
          raise "Timeout while waiting for #{service.service_name} to restart"
        end
      end
    end
  end
end
