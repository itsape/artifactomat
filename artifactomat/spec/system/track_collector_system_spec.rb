# frozen_string_literal: true

require_relative 'system_spec_helper'
require 'openssl'

describe 'TRACK COLLECTOR SYSTEM SPEC', system: true, order: :defined do
  before(:all) do
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']

    ActiveRecord::Base.establish_connection(db_config)

    @logger = SpecLogger.new('track_collector_system_spec')
    @redis = SystemdService.new('redis-server@artifactomat.service')
    @subject_tracker = SystemdService.new('artifactomat-subject-tracker.service')
    @track_collector = SystemdService.new('artifactomat-track-collector.service')
    @test_scheduler = SystemdService.new('artifactomat-ape-scheduler.service')
    @monitoring = SystemdService.new('artifactomat-ape-monitoring.service')
    @routing = SystemdService.new('artifactomat-routing.service')
    @aped = SystemdService.new('artifactomat-aped.service')
    @ape = Command.new
    @cmd = Command.new

    @cert_path = '/etc/artifactomat/certs'

    @cmd.run_and_wait('sudo artifactomat_setup db')
    @cmd.run_and_wait('sudo artifactomat_setup app --yes', 120)

    ie = build(:infrastructure_element, generate_script: 'exit 0',
                                        monitor_script: 'exit 0',
                                        destroy_script: 'exit 0',
                                        ports: [443])
    before_recipe = build(:recipe, infrastructure: [ie],
                                   deploy_script: 'exit 0',
                                   artifact_script: 'exit 0',
                                   arm_script: 'exit 0',
                                   disarm_script: 'exit 0',
                                   parameters: { 'parameter' => '21' },
                                   duration: 2)
    before_recipe.trackfilters.push(build_satellite_tf(before_recipe))
    write_recipe('before_recipe.yml', before_recipe)

    alpha_recipe = build(:recipe, infrastructure: [ie],
                                  deploy_script: 'exit 0',
                                  artifact_script: 'exit 0',
                                  arm_script: 'exit 0',
                                  disarm_script: 'exit 0',
                                  parameters: { 'parameter' => '21' },
                                  duration: 10)
    alpha_recipe.trackfilters.push(build_link_tf(alpha_recipe))
    write_recipe('alpha_recipe.yml', alpha_recipe)

    beta_recipe = build(:recipe, infrastructure: [ie],
                                 deploy_script: 'exit 0',
                                 artifact_script: 'exit 0',
                                 arm_script: 'exit 0',
                                 disarm_script: 'exit 0',
                                 parameters: { 'parameter' => '21' },
                                 duration: 10)
    beta_recipe.trackfilters.push(build_satellite_tf(before_recipe))
    write_recipe('beta_recipe.yml', beta_recipe)

    after_recipe = build(:recipe, infrastructure: [ie],
                                  deploy_script: 'exit 0',
                                  artifact_script: 'exit 0',
                                  arm_script: 'exit 0',
                                  disarm_script: 'exit 0',
                                  parameters: { 'parameter' => '21' },
                                  duration: 2)
    after_recipe.trackfilters.push(build_link_tf(after_recipe))
    write_recipe('after_recipe.yml', after_recipe)

    subjects_alpha = "id,name,surname,userid,email,phone\n" \
                     "1,Max,Mustermann,mustermann,max@muster.de,93060\n" \
                     "2,Terelanie,Musterfrau,terelanie,terelanie@muster.de,123456\n" \
                     "3,Peter,Retep,peter,peter@muster.de,101010\n"
    write_subjects('alpha.csv', subjects_alpha)

    subjects_beta = "id,name,surname,userid,email,phone\n" \
                    "1,Moritz,Mannmuster,mannmuster,moritz@mann.de,60093\n" \
                    "2,Hannah,Fraumuster,hannah,hannah@frau.de,654321\n" \
                    "3,Peter,Retep,peter,peter@muster.de,101010\n"
    write_subjects('beta.csv', subjects_beta)

    write_plugin_conf
    set_log_lvl_debug
  end

  after(:all) do
    kill_and_print(@aped)
    kill_and_print(@subject_tracker)
    kill_and_print(@track_collector)
    kill_and_print(@routing)
    kill_and_print(@test_scheduler)
    kill_and_print(@monitoring)
  end

  def complete_cert_path(path)
    File.join('/etc/artifactomat/certs/', path)
  end

  def kill_and_print(systemd_service)
    return unless systemd_service

    systemd_service.kill
    @logger.log(systemd_service.status)
  end

  def create_ssl_context
    # create ssl context
    ssl_ctx = OpenSSL::SSL::SSLContext.new

    # set client certificate and key
    ssl_ctx.cert = OpenSSL::X509::Certificate.new(File.open(complete_cert_path('public/server.itsape.pem')))
    ssl_ctx.key = OpenSSL::PKey::RSA.new(File.open(complete_cert_path('private/server.itsape.key')))

    # verify server certificate aswell
    ssl_ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER | OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT
    ssl_ctx.ca_file = complete_cert_path('public/ca.pem')
    ssl_ctx
  end

  def send_tracks(tracks)
    # taken from trackcollector.yml
    host = 'localhost'
    port = 5555

    ssl_ctx = create_ssl_context
    tcp_socket = TCPSocket.new(host, port)
    ssl_socket = OpenSSL::SSL::SSLSocket.new(tcp_socket, ssl_ctx)
    connection = ssl_socket.connect
    connection.puts(tracks.length.to_s)
    tracks.each do |track|
      connection.puts(track)
    end
    connection.sysclose
  end

  # tracks must be at least 60 seconds in the future to fail
  # configured in dist/etc/artifactomat/conf.d/trackcollector.yml
  def send_satellite_tracks(ids)
    send_tracks(ids.map { |id| "[#{Time.now.to_i + 60}] satellite data from #{id}" })
  end

  def send_link_tracks(ids)
    send_tracks(ids.map { |id| "[#{Time.now.to_i + 60}] link clicked by #{id}" })
  end

  def wait_for_preparation
    prepared = Set.new
    Timeout.timeout(30) do
      loop do
        @aped.each_new_line do |line|
          match = line.match(/<APE> \[Series "\w+ (?<series>\d)" \(\d\) fully prepared, you may approve now\.\]/)
          prepared.add(match[:series]) if match
        end
        break if prepared == Set.new((1..TestSeries.count).map(&:to_s))

        sleep 1
      end
    end
  rescue Timeout::Error
    raise "Timeout while waiting for framework to allow approval. Prepared: #{prepared}"
  end

  it 'starting infrastructure' do
    @redis.start
    @routing.start
    @test_scheduler.start
    @subject_tracker.start
    @track_collector.start
    @monitoring.start

    @aped.start
    sleep 6

    @aped.escalate_error
    abort("\e[31m\nCAUTION!!\e[0m:\n#{@redis.status}") unless @redis.active?

    expect(@aped.active?).to be_truthy, 'Problem with starting aped!'
    expect(@track_collector.active?).to be_truthy
    expect(@subject_tracker.active?).to be_truthy
    expect(@monitoring.active?).to be_truthy
  end

  context 'no series running' do
    it 'does not parse traces' do
      send_satellite_tracks(['alpha.csv/1', 'beta.csv/2'])
      send_link_tracks(['alpha.csv/3', 'beta.csv/3'])
      sleep(1)
      expect(TrackEntry.count).to eq(0)
    end
  end

  context 'running series' do
    before(:all) do
      @ape.run_and_wait('ape program new -l "TestProgram"')

      # series that we just let run through
      @ape.run_and_wait("ape series new -g 'alpha.csv' -s 'in 50 sec' -e 'in 170 sec' -l 'ended_series #{TestSeries.count + 1}' 1")
      @ape.run_and_wait("ape season new -d 2m -r 'before_recipe' -l 'ended season' #{TestSeries.last.id}")

      phase2_start = Time.now + 180
      @ape.run_and_wait("ape series new -g 'alpha.csv' -s 'in 180 sec' -e 'in 800 sec' -l 'alpha_series #{TestSeries.count + 1}' 1")
      @ape.run_and_wait("ape season new -d 10m -r 'alpha_recipe' -l 'alpha season' #{TestSeries.last.id}")
      @ape.run_and_wait("ape series new -g 'beta.csv' -s 'in 180 sec' -e 'in 800 sec' -l 'beta_series #{TestSeries.count + 1}' 1")
      @ape.run_and_wait("ape season new -d 10m -r 'beta_recipe' -l 'beta season' #{TestSeries.last.id}")

      @ape.run_and_wait("ape series new -g 'alpha.csv' -s 'in 1 day' -e 'in 2 days' -l 'never_runs_series #{TestSeries.count + 1}' 1")
      @ape.run_and_wait("ape season new -d 2m -r 'after_recipe' -l 'never runs season' #{TestSeries.last.id}")

      expect(TestProgram.count).to eq(1)
      expect(TestSeries.count).to eq(4)
      expect(TestSeason.count).to eq(4)
      (1..TestSeries.count).each do |i|
        @ape.run_and_wait("ape series schedule #{i}")
        @ape.run_and_wait("ape series prepare #{i}")
      end

      wait_for_preparation

      (1..TestSeries.count).each do |i|
        @ape.run_and_wait("ape series approve #{i}")
      end

      sleep_until(phase2_start + 3)
    end

    it 'does not detect tracks from ended series' do
      previous_te_cnt = TrackEntry.count
      send_satellite_tracks(['alpha.csv/1', 'alpha.csv/2', 'alpha.csv/3'])
      sleep(1)
      expect(TrackEntry.count).to eq(previous_te_cnt)
    end

    it 'detects tracks from the currently running series' do
      previous_te_cnt = TrackEntry.count
      send_link_tracks(['alpha.csv/1', 'alpha.csv/3'])
      send_satellite_tracks(['beta.csv/1', 'beta.csv/3'])
      sleep(1)
      expect(TrackEntry.count - previous_te_cnt).to eq(4)
    end

    it 'does not detect tracks for the wrong subjects' do
      previous_te_cnt = TrackEntry.count
      send_satellite_tracks(['alpha.csv/1', 'alpha.csv/2', 'alpha.csv/3'])
      send_link_tracks(['beta.csv/1', 'beta.csv/2', 'beta.csv/3'])
      sleep(1)
      expect(TrackEntry.count).to eq(previous_te_cnt)
    end

    it 'does not detect tracks from not yet started series' do
      previous_te_cnt = TrackEntry.count
      send_link_tracks(['beta.csv/1', 'beta.csv/2', 'beta.csv/3'])
      sleep(1)
      expect(TrackEntry.count).to eq(previous_te_cnt)
    end

    it 'links the TEs to the correct subjects' do
      expected = {
        'alpha.csv/1' => 'link clicked',
        'alpha.csv/3' => 'link clicked',
        'beta.csv/1' => 'satellite trace',
        'beta.csv/3' => 'satellite trace'
      }
      detected = {}
      tes = TrackEntry.last(4) # depends on the number of detected tracks
      tes.each do |te|
        detected[te.subject_id] = te.course_of_action
      end
      expect(expected).to eq(detected)
    end
  end
end
