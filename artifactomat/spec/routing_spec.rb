# frozen_string_literal: true

require 'artifactomat/modules/routing'
require 'artifactomat/models/routing_element'
require 'artifactomat/volatile_store'

describe Routing do
  before(:all) do
    Logging.initialize

    @recipes = {}
    @seasons = []
    @subjects = []

    TestSeries.destroy_all
    TestSeason.destroy_all
    TestEpisode.destroy_all
  end

  before(:all) do
    @test_series = create(:test_series, season_count: 0, test_seasons: [])
    (1..2).each do |i|
      recipe = build(:recipe, id: 'Recipe' + i.to_s, infrastructure: [])
      @recipes[recipe.id] = recipe
      subject = build(:subject)
      @subjects << subject
      @episode = build(:test_episode, subject_id: subject.id)
      season = create(:test_season, recipe_id: recipe.id, episode_count: 0, test_series: @test_series)
      season.test_episodes << @episode
      @seasons << season
    end
    @test_series.test_seasons = @seasons

    recipe = @recipes[@seasons[0].recipe_id]
    recipe.infrastructure << build(:infrastructure_element,
                                   generate_script: 'true', ports: ['8080'],
                                   names: ['its.ape1'])

    recipe = @recipes[@seasons[1].recipe_id]
    recipe.infrastructure << build(:infrastructure_element,
                                   generate_script: 'true',
                                   ports: %w[2525 4465 9993 9995],
                                   names: ['its.ape2'])
    recipe.infrastructure << build(:infrastructure_element,
                                   generate_script: 'true',
                                   ports: %w[8081 8082],
                                   names: ['its.ape3'])
  end

  before(:each) do
    @configuration_manager = double('ConfigurationManager')
    allow(@configuration_manager).to \
      receive(:get).with('conf.network.available_external_ports') { [2525, 4465, '8080-8089', 8091, '9900-9999'] }
    allow(@configuration_manager).to \
      receive(:get).with('conf.network.available_internal_ports') { ['9080-9087', 9091, '9099-9100'] }
    allow(@configuration_manager).to \
      receive(:get).with('conf.network.external_ip') { '192.168.1.101' }
    allow(@configuration_manager).to \
      receive(:get).with('conf.ape.redis_socket') { '/tmp/artifactomat-spec-routing' }
    @recipe_manager = double('RecipeManager')
    allow(@recipe_manager).to \
      receive(:get_recipe).with('Recipe1') { @recipes['Recipe1'] }
    allow(@recipe_manager).to \
      receive(:get_recipe).with('Recipe2') { @recipes['Recipe2'] }

    allow(IO).to receive(:read).with('/proc/sys/net/ipv4/ip_forward').and_return('1')
    allow(IO).to receive(:read).with('/proc/sys/net/ipv4/conf/all/route_localnet').and_return('1')
    allow(@iptables_mock).to receive(:get_active_rules).and_return([])

    @scheduler = double('Scheduler')
    allow(Scheduler).to receive(:new).and_return(@scheduler)
    allow(@scheduler).to receive(:at)
    @routing = Routing.new(@configuration_manager, @recipe_manager)
  end

  describe '#new' do
    before(:each) do
      @vstore = VolatileStore.new(@configuration_manager)
      allow(@iptables_mock).to receive(:append)
      allow(@ipset_mock).to receive(:create)
      allow(@ipset_mock).to receive(:matchset)
      allow(Logging).to receive(:debug)
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:warning)
    end

    it 'correctly recreates lost state after startup' do
      TestSeries.take.approved!
      action = Action.new('', '192.168.100.1', '')
      action.subject_id = @episode.subject_id
      @vstore.update_ip_subject_map action
      target_ports = [9086, 9087, 9091, 9099]
      target_res = []
      r = RoutingElement.new @test_series.id, @seasons[1].id
      r.internal_ip = '127.0.0.1'
      r.external_ip = '192.168.1.101'
      r.internal_ports = [9080, 9081, 9082, 9083]
      r.external_ports = [2525, 4465, 9993, 9995]
      target_res << r
      r = RoutingElement.new @test_series.id, @seasons[1].id
      r.internal_ip = '127.0.0.1'
      r.external_ip = '192.168.1.101'
      r.internal_ports = [9084, 9085]
      r.external_ports = [8081, 8082]
      target_res << r
      r = RoutingElement.new @test_series.id, @seasons[0].id
      r.internal_ip = '127.0.0.1'
      r.external_ip = '192.168.1.101'
      r.internal_ports = [9100]
      r.external_ports = [8080]
      target_res << r

      active_rules = [
        { match: "itsape-#{@seasons[1].id}", destination: '192.168.1.101', dport: '2525', jump: 'DNAT', to_dest: '127.0.0.1:9080' },
        { match: "itsape-#{@seasons[1].id}", destination: '192.168.1.101', dport: '4465', jump: 'DNAT', to_dest: '127.0.0.1:9081' },
        { match: "itsape-#{@seasons[1].id}", destination: '192.168.1.101', dport: '9993', jump: 'DNAT', to_dest: '127.0.0.1:9082' },
        { match: "itsape-#{@seasons[1].id}", destination: '192.168.1.101', dport: '9995', jump: 'DNAT', to_dest: '127.0.0.1:9083' },
        { match: "itsape-#{@seasons[1].id}", destination: '192.168.1.101', dport: '8081', jump: 'DNAT', to_dest: '127.0.0.1:9084' },
        { match: "itsape-#{@seasons[1].id}", destination: '192.168.1.101', dport: '8082', jump: 'DNAT', to_dest: '127.0.0.1:9085' }
      ]
      allow(@ipset_mock).to receive(:matchset) do |attrs|
        "--match-set #{attrs[:set_name]} #{attrs[:flags]}"
      end
      allow(@iptables_mock).to receive(:append)
      allow(@iptables_mock).to receive(:get_active_rules).and_return(active_rules)
      allow(@ipset_mock).to receive(:create)
      @episode.routing_active = true
      @episode.save!

      new_routing = Routing.new(@configuration_manager, @recipe_manager)
      int_ports = new_routing.instance_variable_get(:@available_internal_ports)
      expect(int_ports).to eq(target_ports)
      res = new_routing.instance_variable_get(:@routing_elements)
      expect(res).to eq(target_res)

      @recipes[@seasons[1].recipe_id].infrastructure.pop
      TestSeries.take.fresh!
      @episode.update routing_active: false
    end

    it 'creates routing remove jobs for prepared series' do
      old_status = @test_series.status
      @test_series.prepared!

      expect(@scheduler).to receive(:at)
        .with(@test_series.end_date, tags: ['remove routing', @test_series.to_str])

      Routing.new(@configuration_manager, @recipe_manager)
      @test_series.status = old_status
      @test_series.save!
    end

    it 'creates routing remove jobs for approved series' do
      old_status = @test_series.status
      @test_series.approved!

      expect(@scheduler).to receive(:at)
        .with(@test_series.end_date, tags: ['remove routing', @test_series.to_str])

      Routing.new(@configuration_manager, @recipe_manager)
      @test_series.status = old_status
      @test_series.save!
    end

    it 'does not create routing remove jobs for series that are not prepared nor approved' do
      old_status = @test_series.status
      @test_series.fresh!

      expect(@scheduler).not_to receive(:at)
      Routing.new(@configuration_manager, @recipe_manager)

      @test_series.scheduled!
      Routing.new(@configuration_manager, @recipe_manager)

      @test_series.completed!
      Routing.new(@configuration_manager, @recipe_manager)

      @test_series.status = old_status
      @test_series.save!
    end
  end

  describe '#run' do
    before(:each) do
      @vstore = VolatileStore.new(@configuration_manager)
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)
      allow(@ipset_mock).to receive(:matchset) do |attrs|
        "--match-set #{attrs[:set_name]} #{attrs[:flags]}"
      end
      allow(@ipset_mock).to receive(:create)
      allow(@iptables_mock).to receive(:append)
      allow(@iptables_mock).to receive(:get_active_rules).and_return([])
      @routing.create_routing(@test_series)
    end

    after(:each) do
      @episode.update routing_active: false
    end

    it 'can run' do
      @routing.run
      @routing.instance_variable_get(:@dyn_router).kill
    end

    it 'listens to additions to the ip_subject_map' do
      expect(@ipset_mock).to receive(:add).with(set_name: "itsape-#{@episode.test_season.id}", entry_data: '123.456.789')
      @routing.activate(@episode)
      action = Action.new('', '123.456.789', '')
      action.subject_id = @episode.subject_id
      @vstore.update_ip_subject_map(action)

      # https://github.com/guilleiguaran/fakeredis/issues/29
      # @routing.run
      @routing.send(:dyn_router)
    end

    it 'listens to removals to the ip_subject_map' do
      action = Action.new('', '123.456.789', '')
      action.subject_id = @episode.subject_id
      @vstore.update_ip_subject_map(action)
      allow(@ipset_mock).to receive(:add)
      @routing.activate(@episode)
      expect(@ipset_mock).to receive(:del).with(set_name: "itsape-#{@episode.test_season.id}", entry_data: '123.456.789')
      @vstore.remove_from_ip_subject_map(action)
      # https://github.com/guilleiguaran/fakeredis/issues/29
      # @routing.run
      @routing.send(:dyn_router)
    end
  end

  describe '#create_routing(series)' do
    before :each do
      allow(Logging).to receive(:debug)
      allow(Logging).to receive(:info)
      allow(@iptables_mock).to receive(:append)
      allow(@ipset_mock).to receive(:create)
      allow(@ipset_mock).to receive(:matchset) do |attrs|
        "--match-set #{attrs[:set_name]} #{attrs[:flags]}"
      end
    end

    it 'assigns internal ports and the number of them matches with IE-ports' do
      expect(Logging).to receive(:info).once
      expect(Logging).to receive(:debug).exactly(2)

      expect { @routing.create_routing(@test_series) }.not_to raise_error

      re = @routing.instance_variable_get(:@routing_elements)

      expect(re[0].internal_ports).to be_instance_of(Array)
      expect(re[1].internal_ports).to be_instance_of(Array)
      expect(re[0].internal_ports.size).to be(1)
      expect(re[1].internal_ports.size).to be(4)
    end

    it 'should not assign one internal port twice' do
      expect(Logging).to receive(:info).once
      expect(Logging).to receive(:debug).exactly(2)
      expect { @routing.create_routing(@test_series) }.not_to raise_error

      re = @routing.instance_variable_get(:@routing_elements)
      internal_ports = re[0].internal_ports + re[1].internal_ports

      expect(internal_ports.uniq!).to be(nil)
    end

    it 'should assign ports within the limits of the config' do
      expect(Logging).to receive(:info).once
      expect(Logging).to receive(:debug).exactly(2)
      expect { @routing.create_routing(@test_series) }.not_to raise_error

      re = @routing.instance_variable_get(:@routing_elements)
      internal_ports = re[0].internal_ports + re[1].internal_ports
      available_ports = [9080, 9081, 9082, 9083, 9084, 9085, 9086, 9087, 9091, 9099, 9100]

      internal_ports.each do |port|
        expect(available_ports).to include(port)
      end
    end

    it 'creates ipsets for each season' do
      @test_series.test_seasons.each do |season|
        expect(@ipset_mock).to receive(:create).with(set_name: "itsape-#{season.id}", type: 'hash:ip')
      end
      @routing.create_routing(@test_series)
    end

    it 'does not crash if an ipset already exists' do
      expect(Logging).to receive(:error).with('Routing', /Set cannot be created: set with the same name already exists/)
      expect(@ipset_mock).to receive(:create).and_raise(Tablomat::IPSetError.new('ipset v6.38: Set cannot be created: set with the same name already exists'))

      expect { @routing.create_routing(@test_series) }.not_to raise_error
    end

    it 'creates iptables rules' do
      int_ports = []
      (9080..9087).each { |x| int_ports << x }
      int_ports += [9091, 9099, 9100]

      @test_series.test_seasons.each do |season|
        recipe = @recipe_manager.get_recipe(season.recipe_id)
        recipe.infrastructure.each do |ie|
          no_ie_ports = ie.ports.length
          ie_ports = int_ports.pop no_ie_ports
          no_ie_ports.times do |i|
            expect(@iptables_mock).to receive(:append).with('nat',
                                                            'PREROUTING',
                                                            set: "--match-set itsape-#{season.id} src",
                                                            destination: '192.168.1.101',
                                                            protocol: :tcp,
                                                            dport: ie.ports[i].to_i,
                                                            jump: 'DNAT',
                                                            to: "127.0.0.1:#{ie_ports[i]}")
          end
        end
      end
      @routing.create_routing(@test_series)
    end

    it 'creates a destroy job' do
      expect(@scheduler).to receive(:at).with(@test_series.end_date,
                                              tags: ['remove routing', @test_series.to_str])
      @routing.create_routing(@test_series)
    end

    it 'should fail if external port is not available' do
      expect(Logging).to receive(:debug).exactly(2).times
      expect(Logging).to receive(:error)
      recipe = build(:recipe, id: 'Recipe3', infrastructure: [])
      recipe.infrastructure << build(:infrastructure_element,
                                     generate_script: 'true', ports: ['4242'],
                                     names: ['its.ape.broken'])
      @recipes[recipe.id] = recipe
      @seasons << create(:test_season, recipe_id: recipe.id)
      allow(@recipe_manager).to \
        receive(:get_recipe).with(recipe.id) { @recipes[recipe.id] }
      @test_series = create(:test_series, season_count: 0, test_seasons: @seasons)

      expect { @routing.create_routing(@test_series) }.to \
        raise_error(RoutingRunningError,
                    'Can\'t route 4242, not available.')

      # Cleanup: Remove season again
      @seasons.pop
    end

    it 'should fail if internal ports are exhausted' do
      expect(Logging).to receive(:debug).exactly(2).times
      expect(Logging).to receive(:error)
      recipe = build(:recipe, id: 'Recipe3', infrastructure: [])
      recipe.infrastructure << build(:infrastructure_element,
                                     generate_script: 'true',
                                     ports: %w[8080 8081 8082 8083 8084 8085 8086],
                                     names: ['no.more.ports'])
      @recipes[recipe.id] = recipe
      @seasons << create(:test_season, recipe_id: recipe.id)
      allow(@recipe_manager).to \
        receive(:get_recipe).with(recipe.id) { @recipes[recipe.id] }
      @test_series = create(:test_series, season_count: 0,
                                          test_seasons: @seasons)

      expect { @routing.create_routing(@test_series) }.to \
        raise_error(RoutingRunningError, 'Internal ports exhausted.')

      # Cleanup: Remove season again
      @seasons.pop
    end
  end

  describe '#remove_routing(series)' do
    before(:each) do
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)
      allow(@iptables_mock).to receive(:append)
      allow(@ipset_mock).to receive(:create)
      allow(@iptables_mock).to receive(:delete)
      allow(@ipset_mock).to receive(:destroy)
      allow(@ipset_mock).to receive(:matchset) do |attrs|
        "--match-set #{attrs[:set_name]} #{attrs[:flags]}"
      end
      @routing.create_routing(@test_series)
    end

    it 'removes ipsets for each season' do
      @test_series.test_seasons.each do |season|
        expect(@ipset_mock).to receive(:destroy).with(set_name: "itsape-#{season.id}")
      end
      @routing.remove_routing(@test_series)
    end

    it 'removes iptables rules' do
      int_ports = []
      (9080..9087).each { |x| int_ports << x }
      int_ports += [9091, 9099, 9100]

      @test_series.test_seasons.each do |season|
        recipe = @recipe_manager.get_recipe(season.recipe_id)
        recipe.infrastructure.each do |ie|
          no_ie_ports = ie.ports.length
          ie_ports = int_ports.pop no_ie_ports
          no_ie_ports.times do |i|
            expect(@iptables_mock).to receive(:delete).with('nat',
                                                            'PREROUTING',
                                                            set: "--match-set itsape-#{season.id} src",
                                                            destination: '192.168.1.101',
                                                            protocol: :tcp,
                                                            dport: ie.ports[i].to_i,
                                                            jump: 'DNAT',
                                                            to: "127.0.0.1:#{ie_ports[i]}")
          end
        end
      end
      @routing.remove_routing(@test_series)
    end

    it 'removes existing Routing Elements' do
      expect(Logging).to receive('info').exactly(1).times
      # expect(Logging).to receive('debug').exactly(2).times
      expect(Logging).to receive(:debug).twice
      re = @routing.instance_variable_get(:@routing_elements)
      expect(re.size).to be > 0
      @routing.remove_routing(@test_series)
      expect(re.size).to be(0)
    end

    it 'frees internal port-list' do
      expect(Logging).to receive(:info).exactly(1).times
      expect(Logging).to receive(:debug).exactly(2).times
      internal_port_list_before = @routing.instance_variable_get(:@available_internal_ports).size

      @routing.remove_routing(@test_series)
      internal_port_list_after = @routing.instance_variable_get(:@available_internal_ports).size

      expect(internal_port_list_after).to be > internal_port_list_before
    end

    it 'does not break if no routing is set up' do
      @routing.remove_routing(@test_series)
      expect { @routing.remove_routing(@test_series) }.not_to(raise_error)
    end
  end

  describe '#get_external_address(season)' do
    before :each do
      allow(@iptables_mock).to receive(:append)
      allow(@ipset_mock).to receive(:create)
      allow(@ipset_mock).to receive(:matchset) do |attrs|
        "--match-set #{attrs[:set_name]} #{attrs[:flags]}"
      end
    end

    it 'external adress returns external ip of a season' do
      allow(Logging).to receive(:info)
      expect(Logging).to receive(:debug).twice
      @routing.create_routing(@test_series)
      expect(@routing.get_external_address(@seasons[0])).to \
        match('192.168.1.101')
      expect(@routing.get_external_address(@seasons[1])).to \
        match('192.168.1.101')
    end
  end

  describe '#get_internal_ports(season)' do
    before :each do
      allow(@iptables_mock).to receive(:append)
      allow(@ipset_mock).to receive(:create)
      allow(@ipset_mock).to receive(:matchset) do |attrs|
        "--match-set #{attrs[:set_name]} #{attrs[:flags]}"
      end
    end

    it 'internal ports returns internal ports of a season' do
      allow(Logging).to receive(:info)
      expect(Logging).to receive(:debug).twice
      @routing.create_routing(@test_series)
      ports = @routing.get_internal_ports(@seasons[0])
      expect(ports).to match([9100])
      ports = @routing.get_internal_ports(@seasons[1])
      expect(ports).to match([9086, 9087, 9091, 9099])
    end
  end

  describe '#activate' do
    before(:each) do
      @vstore = VolatileStore.new(@configuration_manager)
      action = Action.new('', '123.456.789', '')
      action.subject_id = @episode.subject_id
      @vstore.update_ip_subject_map(action)
      allow(@iptables_mock).to receive(:append)
      allow(@ipset_mock).to receive(:create)
      allow(@ipset_mock).to receive(:matchset) do |attrs|
        "--match-set #{attrs[:set_name]} #{attrs[:flags]}"
      end
    end

    after(:each) do
      @episode.update routing_active: false
    end

    it 'adds the subjects ip to the season ipset' do
      expect(Logging).to receive(:info).exactly(2).times
      expect(Logging).to receive(:debug).exactly(2).times
      @routing.create_routing(@test_series)
      expect(@ipset_mock).to receive(:add).with(set_name: "itsape-#{@episode.test_season.id}",
                                                entry_data: '123.456.789')
      @routing.activate(@episode)
    end

    it 'does not crash if an ip is already on an ipset' do
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)
      expect(Logging).to receive(:error).with('Routing',
                                              /Could not add 123\.456\.789 to ipset itsape-#{@episode.test_season.id}/)
      @routing.create_routing(@test_series)
      allow(@ipset_mock).to receive(:add).and_raise(Tablomat::IPSetError.new("ipset v6.38: Element cannot be added to the set: it's already added"))
      expect { @routing.activate(@episode) }.not_to raise_error
    end

    it 'does crash on any other ipset error' do
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)
      @routing.create_routing(@test_series)
      allow(@ipset_mock).to receive(:add).and_raise(Tablomat::IPSetError)

      expect { @routing.activate(@episode) }.to raise_error(Tablomat::IPSetError)
    end

    it 'gives a warning if rule already exists' do
      skip('Tablomat does not support that yet.')
    end
  end

  describe '#deactivate' do
    before(:each) do
      @vstore = VolatileStore.new(@configuration_manager)
      action = Action.new('', '123.456.789', '')
      action.subject_id = @episode.subject_id
      @vstore.update_ip_subject_map(action)
      allow(@iptables_mock).to receive(:append)
      allow(@ipset_mock).to receive(:create)
      allow(@ipset_mock).to receive(:matchset) do |attrs|
        "--match-set #{attrs[:set_name]} #{attrs[:flags]}"
      end
    end

    it 'removes the subjects ip from the season ipset' do
      expect(Logging).to receive(:info).exactly(2).times
      expect(Logging).to receive(:debug).exactly(2).times
      @routing.create_routing(@test_series)
      expect(@ipset_mock).to receive(:del).with(set_name: "itsape-#{@episode.test_season.id}",
                                                entry_data: '123.456.789')
      @routing.deactivate(@episode)
    end

    it 'does not crash if an ip is already on an ipset' do
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)
      @routing.create_routing(@test_series)
      allow(@ipset_mock).to receive(:del).and_raise(Tablomat::IPSetError.new("ipset v6.38: Element cannot be deleted from the set: it's not added"))
      expect(Logging).to receive(:error).with('Routing',
                                              /Could not remove 123\.456\.789 from ipset itsape-#{@episode.test_season.id}/)

      expect { @routing.deactivate(@episode) }.not_to raise_error
    end

    it 'does not crash if an ipset does not exist anymore' do
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)
      @routing.create_routing(@test_series)
      allow(@ipset_mock).to receive(:del).and_raise(Tablomat::IPSetError.new('ipset v6.38: The set with the given name does not exist'))
      expect(Logging).to receive(:error).with('Routing',
                                              /Could not remove 123\.456\.789 from ipset itsape-#{@episode.test_season.id}/)

      expect { @routing.deactivate(@episode) }.not_to raise_error
    end

    it 'does crash on any other ipset error' do
      allow(Logging).to receive(:info)
      allow(Logging).to receive(:debug)
      @routing.create_routing(@test_series)
      allow(@ipset_mock).to receive(:del).and_raise(Tablomat::IPSetError)
      expect { @routing.deactivate(@episode) }.to raise_error(Tablomat::IPSetError)
    end

    it 'gives a warning if rule does not exist' do
      skip('Tablomat does not support that yet.')
    end
  end

  it 'checks routing flags' do
    expect(IO).to receive(:read).with('/proc/sys/net/ipv4/ip_forward').and_return('0')
    expect(IO).to receive(:read).with('/proc/sys/net/ipv4/conf/all/route_localnet').and_return('0')
    expect(Logging).to receive(:warning).with('Routing', "Local Routing is disabled! Please run 'sysctl -w net.ipv4.conf.all.route_localnet=1' in case you need to reroute traffic to local Docker containers.")
    expect(Logging).to receive(:warning).with('Routing', "IP Forwarding is disabled! Please run 'sysctl -w net.ipv4.ip_forward=1'")
    @routing.send(:check_routing_flags)
  end

  it 'unschedules jobs' do
    series = build(:test_series)
    jobs = [double('job1'), double('job2')]
    jobs.each do |job|
      expect(job).to receive(:unschedule)
    end
    expect(@scheduler).to receive(:jobs).with(tag: series.to_str).and_return(jobs)
    @routing.unschedule_jobs(series)
  end
end
