# frozen_string_literal: true

require 'artifactomat/modules/ape'
require 'artifactomat/mixins/script_execution.rb'

# dummy class for testing script execution
class MixinTester
  include ScriptExecution
  attr_accessor :configuration_manager
  attr_accessor :recipe_manager
  attr_accessor :subject_manager
end

APE_TMP_TEST = '/tmp/its.apt-tmp-test'
APE_CFG_TEST = '/tmp/its.apt-cfg-test'

describe ScriptExecution, type: :mixin do
  before(:each) do
    @an_ape = double('Ape')
    allow(Ape).to receive(:instance).and_return(@an_ape)
  end

  context 'Responses' do
    before(:each) do
      @mt = MixinTester.new
    end

    after(:each) do
      @mt = nil
    end

    it 'responds to execute_script' do
      expect(@mt.respond_to?(:execute_script)).to be true
    end

    it 'responds to token_from_object' do
      expect(@mt.respond_to?(:token_from_object)).to be true
    end

    it 'responds to subject_to_env' do
      expect(@mt.respond_to?(:subject_to_env)).to be true
    end

    it 'responds to recipe_tmp_folder' do
      expect(@mt.respond_to?(:recipe_tmp_folder)).to be true
    end

    it 'responds to season_to_env' do
      expect(@mt.respond_to?(:season_to_env)).to be true
    end

    it 'responds to episode_to_env' do
      expect(@mt.respond_to?(:episode_to_env)).to be true
    end
  end

  context 'execute_script' do
    before(:each) do
      @mt = MixinTester.new
    end

    after(:each) do
      @mt = nil
    end

    it 'runs a working script without error' do
      expect(@mt.execute_script('true')[0]).to eql(0)
    end

    it 'raises no Error on failing commands' do
      expect(@mt.execute_script('false')[0]).to eql(1)
    end

    it 'raises an InvalidCommandError on unknown commands' do
      expect { @mt.execute_script('NoSuchCommand') }
        .to raise_error(InvalidCommandError)
    end

    it 'accepts and forwards environments to commands' do
      _, stdout, = @mt.execute_script('echo -n $STUFF', 'STUFF' => 'Working')

      expect(stdout).to eql('Working')
    end
  end

  context 'token_from_object' do
    before(:each) do
      @mt = MixinTester.new
    end

    after(:each) do
      @mt = nil
    end

    # Test object
    class TestObject
      attr_accessor :id, :config_manager
    end
    it 'generates a MD5 sum from object.id' do
      obj = TestObject.new
      obj.id = 'Test'
      md5string = @mt.token_from_object(obj)
      expect(md5string).to eql('0cbc6611f5540bd0809a388dc95a615b')
    end

    it 'converts id to an String before computing the hash' do
      obj = TestObject.new
      obj.id = 12
      md5string = @mt.token_from_object(obj)
      expect(md5string).to eql('c20ad4d76fe97759aa27a0c99bff6710')
    end

    it 'raises a TokenError if object does not provide an attribute id' do
      expect { @mt.token_from_object('') }.to raise_error(TokenError)
    end
  end

  context 'recipe_tmp_folder' do
    before(:each) do
      @directory = '/tmp/artifactomat-tmp-dir'
      @mt = MixinTester.new
      @recipe = build(:recipe)
      @season = create(:test_season, recipe_id: @recipe.id)
      @configuration_manager = double('ConfigurationManager')
      allow(@configuration_manager)
        .to receive(:get).with(ScriptExecution::APE_TMP_DIR) { @directory }
      @mt.configuration_manager = @configuration_manager
    end

    after(:each) do
      @mt = nil
    end

    it 'raises an ScriptExecutionError, if provided argument ' \
       'is not a TestSeason' do
      expect { @mt.recipe_tmp_folder('') }.to raise_error(ScriptExecutionError)
    end

    it 'returns the correct tmp folder' do
      expect(@mt.recipe_tmp_folder(@season))
        .to eql(@directory + "/#{@season.id}")
    end
  end

  context 'subject_to_env' do
    before(:each) do
      @mt = MixinTester.new
      @env = { 'name' => 'Converter',
               'id' => 1,
               'surname' => 'Bob',
               'userid' => 1337,
               'email' => 'bob@converter.biz' }

      @subject = build(:subject,
                       name: @env['name'],
                       id: @env['id'],
                       surname: @env['surname'],
                       userid: @env['userid'],
                       email: @env['email'])
    end

    after(:each) do
      @mt = nil
    end

    it 'raises an EnvConversionError if provided argument is not a Subject' do
      expect { @mt.subject_to_env('') }.to raise_error(EnvConversionError)
    end

    it 'creates a subject token' do
      key = ScriptExecution::SUBJECT_TOKEN
      result_env = @mt.subject_to_env(@subject)
      expect(result_env.key?(key)).to be true
      expect(result_env[key]).to eql('c4ca4238a0b923820dcc509a6f75849b')
    end

    it 'converts the default subject to the correct environment' do
      result_env = @mt.subject_to_env(@subject)

      @env.each_key do |k|
        key = ScriptExecution::SUBJECT_ENV_PREFIX + k.upcase
        expect(result_env.key?(key)).to be true
        expect(result_env[key]).to eql(@env[k])
      end
    end
  end

  context 'season_to_env' do
    before(:each) do
      @mt = MixinTester.new
      @env = {}
      @ie = build(:infrastructure_element, generate_script: 'exit 0',
                                           monitor_script: 'exit 0',
                                           destroy_script: 'exit 0')
      @recipe = build(:recipe, infrastructure: [@ie])
      @season = create(:test_season, recipe_id: @recipe.id, recipe_parameters: { 'testparameter1': 'testparameter1', 'testparameter2': 'testparameter2' })
      @recipe_manager = double('RecipeManager')
      @configuration_manager = double('ConfigurationManager')

      allow(@configuration_manager)
        .to receive(:cfg_path) { APE_CFG_TEST }
      allow(@configuration_manager)
        .to receive(:get)
          .with(ScriptExecution::APE_TMP_DIR) { APE_TMP_TEST }
      allow(@recipe_manager).to receive(:get_recipe) { @recipe }
      @mt.configuration_manager = @configuration_manager
      @mt.recipe_manager = @recipe_manager

      routing = double('Routing')
      allow(@an_ape).to receive(:routing).and_return(routing)
      allow(routing).to receive(:get_internal_ports).with(@season).and_return([1])
      allow(routing).to receive(:get_external_address).with(@season).and_return('127.0.0.1')
    end

    after(:each) do
      @mt = nil
    end

    it 'raises an EnvConversionError if provided argument is ' \
       'not a TestSeason' do
      expect { @mt.season_to_env('') }.to raise_error(EnvConversionError)
    end

    it 'environment contains a recipe token' do
      key = ScriptExecution::RECIPE_TOKEN
      result_env = @mt.season_to_env(@season)
      expect(result_env.key?(key)).to be true
      expect(result_env[key]).to eql(@mt.token_from_object(@recipe))
    end

    it 'environment contains a seasion id' do
      key = ScriptExecution::SEASON_ID
      result_env = @mt.season_to_env(@season)
      expect(result_env.key?(key)).to be true
      expect(result_env[key]).to eql(@season.id.to_s)
    end

    it 'environment contains recipe parameters' do
      key1 = ScriptExecution::RECIPE_ENV_PREFIX + 'testparameter1'.upcase
      key2 = ScriptExecution::RECIPE_ENV_PREFIX + 'testparameter2'.upcase
      result_env = @mt.season_to_env(@season)
      expect(result_env.key?(key1)).to be true
      expect(result_env.key?(key2)).to be true
      expect(result_env[key1]).to eql('testparameter1')
      expect(result_env[key2]).to eql('testparameter2')
    end

    it 'environment contains IE parameters' do
      # parameter names and values are the IE factory defaults
      key1 = ScriptExecution::IE_ENV_PREFIX + 'ip'.upcase
      key2 = ScriptExecution::IE_ENV_PREFIX + 'port'.upcase
      result_env = @mt.season_to_env(@season)
      expect(result_env.key?(key1)).to be true
      expect(result_env.key?(key2)).to be true
      expect(result_env[key1]).to eql('192.168.12.2')
      expect(result_env[key2]).to eql('80')
    end
  end

  context 'episode_to_env' do
    before(:each) do
      @mt = MixinTester.new
      @sub_env = { 'name' => 'Converter',
                   'id' => 1,
                   'surname' => 'Bob',
                   'userid' => 1337,
                   'email' => 'bob@converter.biz',
                   'abilities' => 'tdd expert' }

      @subject = build(:subject,
                       name: @sub_env['name'],
                       id: @sub_env['id'],
                       surname: @sub_env['surname'],
                       userid: @sub_env['userid'],
                       email: @sub_env['email'])
      @subject.add_field('abilities')
      @subject.abilities = 'tdd expert'
      @episode = create(:test_episode, subject_id: @subject.id)
      @subject_manager = double('SubjectManager')
      allow(@subject_manager)
        .to receive(:get_subject).with(@subject.id.to_s) { @subject }
      @mt.subject_manager = @subject_manager
    end

    after(:each) do
      @mt = nil
    end

    it 'raises an EnvConversionError if provided argument' \
       'is not a TestEpisode' do
      expect { @mt.episode_to_env('') }.to raise_error(EnvConversionError)
    end

    it 'environment contains a episode id' do
      key = ScriptExecution::EPISODE_ID
      result_env = @mt.episode_to_env(@episode)

      expect(result_env.key?(key)).to be true
      expect(result_env[key]).to eql(@episode.id.to_s)
    end

    it 'environment contains the correct subject' do
      result_env = @mt.episode_to_env(@episode)

      @sub_env.each_key do |k|
        key = ScriptExecution::SUBJECT_ENV_PREFIX + k.upcase
        expect(result_env.key?(key)).to be true
        expect(result_env[key]).to eql(@sub_env[k])
      end
    end
  end

  it 'creates pretty errors' do
    @mt = MixinTester.new
    err = @mt.pp_error 'stdout', 'stderr'
    expect(err).to eq("============ STDOUT ==============\nstdout\n============ STDERR ==============\nstderr\n")
  end
end
