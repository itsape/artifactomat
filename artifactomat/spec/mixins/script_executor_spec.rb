# frozen_string_literal: true

require 'artifactomat/mixins/script_execution.rb'
require 'artifactomat/modules/ape.rb'

shared_examples 'a script executor' do
  describe '#execute_script' do
    it 'runs a script' do
      expect(executor.execute_script('true')[0]).to eql(0)
    end

    it 'accepts and forwards environments to commands' do
      _, stdout = executor.execute_script('echo -n $STUFF',
                                          'STUFF' => 'Working')
      expect(stdout).to eql('Working')
    end

    it 'raises no Error on failing commands' do
      expect(executor.execute_script('false')[0]).to eql(1)
    end

    it 'raises an InvalidCommandError if command does not exist' do
      expect do
        executor.execute_script('NoSuchCommand')
      end.to raise_error(InvalidCommandError)
    end

    it 'raises an InvalidCommandError if command is empty' do
      expect do
        executor.execute_script('')
      end.to raise_error(InvalidCommandError)
    end

    it 'raises an InvalidCommandError if command is nil' do
      expect do
        executor.execute_script(nil)
      end.to raise_error(InvalidCommandError)
    end
  end

  describe '#token_from_object' do
    # a dummy to extract tokens from
    class TestObject
      attr_accessor :id, :config_manager
    end
    it 'generates a MD5 sum from object.id' do
      obj = TestObject.new
      obj.id = 'Test'
      md5string = executor.token_from_object(obj)
      expect(md5string).to eql('0cbc6611f5540bd0809a388dc95a615b')
    end

    it 'converts id to an String before computing the hash' do
      obj = TestObject.new
      obj.id = 12
      md5string = executor.token_from_object(obj)
      expect(md5string).to eql('c20ad4d76fe97759aa27a0c99bff6710')
    end

    it 'raises a TokenError if object does not provide an attribute id' do
      expect { executor.token_from_object('') }.to raise_error(TokenError)
    end
  end

  describe '#recipe_tmp_folder' do
    before(:all) do
      @directory = '/tmp/artifactomat-tmp-dir'
      @recipe = build(:recipe)
      @season = create(:test_season, recipe_id: @recipe.id)
    end

    before(:each) do
      @configuration_manager = double('ConfigurationManager')
      allow(@configuration_manager)
        .to receive(:get).with(ScriptExecution::APE_TMP_DIR) { @directory }
      executor.instance_variable_set(:@configuration_manager, @configuration_manager)
    end

    it 'raises an ScriptExecutionError, if argument ' \
       'is not a TestSeason' do
      expect do
        executor.recipe_tmp_folder('')
      end.to raise_error(ScriptExecutionError)
    end

    it 'returns the correct tmp folder' do
      expect(executor.recipe_tmp_folder(@season))
        .to eql(@directory + "/#{@season.id}")
    end
  end

  describe '#subject_to_env' do
    before(:each) do
      @env = { 'name' => 'Converter',
               'id' => 1,
               'surname' => 'Bob',
               'userid' => 1337,
               'email' => 'bob@converter.biz' }

      @subject = build(:subject,
                       name: @env['name'],
                       id: @env['id'],
                       surname: @env['surname'],
                       userid: @env['userid'],
                       email: @env['email'])
    end

    it 'raises an EnvConversionError if provided argument is not a Subject' do
      expect do
        executor.subject_to_env('')
      end.to raise_error(EnvConversionError)
    end

    it 'creates a subject token' do
      key = ScriptExecution::SUBJECT_TOKEN
      result_env = executor.subject_to_env(@subject)
      expect(result_env.key?(key)).to be true
      expect(result_env[key]).to eql('c4ca4238a0b923820dcc509a6f75849b')
    end

    it 'converts the default subject to the correct environment' do
      result_env = executor.subject_to_env(@subject)

      @env.each_key do |k|
        key = ScriptExecution::SUBJECT_ENV_PREFIX + k.upcase
        expect(result_env.key?(key)).to be true
        expect(result_env[key]).to eql(@env[k])
      end
    end
  end

  context '#season_to_env' do
    before(:each) do
      @env = {}
      @recipe = build(:recipe)
      @season = create(:test_season, recipe_id: @recipe.id, recipe_parameters: { 'testparameter1': 'testparameter1', 'testparameter2': 'testparameter2' })

      @configuration_manager = double('ConfigurationManager')
      allow(@configuration_manager)
        .to receive(:cfg_path) { '/tmp/its.apt-cfg-test' }
      allow(@configuration_manager)
        .to receive(:get)
          .with(ScriptExecution::APE_TMP_DIR) { '/tmp/its.apt-tmp-test' }
      executor.instance_variable_set(:@configuration_manager, @configuration_manager)

      @recipe_manager = double('RecipeManager')
      allow(@recipe_manager).to receive(:get_recipe) { @recipe }
      executor.instance_variable_set(:@recipe_manager, @recipe_manager)

      @routing = double('RoutingDoubleScriptExecutor')
      allow(@routing).to receive(:get_internal_ports).with(@season).and_return([5, 6])
      allow(@routing).to receive(:get_external_address).with(@season).and_return('127.0.0.1')

      ape = double('st_ape_double')
      allow(Ape).to receive(:instance).and_return(ape)
      allow(ape).to receive(:routing).and_return(@routing)

      executor.instance_variable_set(:@routing, @routing) unless executor.instance_variable_get(:@routing).nil?
    end

    after(:each) do
      @season.destroy
    end

    it 'raises an EnvConversionError if provided argument is ' \
       'not a TestSeason' do
      expect { executor.season_to_env('') }.to raise_error(EnvConversionError)
    end

    context 'environment contains' do
      before(:each) do
        @result_env = executor.season_to_env(@season)
      end

      it 'a recipe token' do
        key = ScriptExecution::RECIPE_TOKEN
        expect(@result_env.key?(key)).to be true
        expect(@result_env[key]).to eql(executor.token_from_object(@recipe))
      end

      it 'a seasion id' do
        key = ScriptExecution::SEASON_ID
        expect(@result_env.key?(key)).to be true
        expect(@result_env[key]).to eql(@season.id.to_s)
      end

      it 'recipe parameters' do
        key1 = ScriptExecution::RECIPE_ENV_PREFIX + 'testparameter1'.upcase
        key2 = ScriptExecution::RECIPE_ENV_PREFIX + 'testparameter2'.upcase
        expect(@result_env.key?(key1)).to be true
        expect(@result_env.key?(key2)).to be true
        expect(@result_env[key1]).to eql('testparameter1')
        expect(@result_env[key2]).to eql('testparameter2')
      end

      it 'the external IP address' do
        key = ScriptExecution::ROUTING_EXT_ADDRESS
        expect(@result_env.key?(key)).to be true
        expect(@result_env[key]).to eql('127.0.0.1')
      end

      it 'internal ports' do
        key1 = ScriptExecution::ROUTING_PORT_PREFIX + '1'.upcase
        key2 = ScriptExecution::ROUTING_PORT_PREFIX + '2'.upcase
        expect(@result_env.key?(key1)).to be true
        expect(@result_env.key?(key2)).to be true
        expect(@result_env[key1]).to eql(5.to_s)
        expect(@result_env[key2]).to eql(6.to_s)
      end
    end
  end

  context '#episode_to_env' do
    before(:each) do
      @sub_env = { 'name' => 'Converter',
                   'id' => 1,
                   'surname' => 'Bob',
                   'userid' => 1337,
                   'email' => 'bob@converter.biz',
                   'abilities' => 'tdd expert' }

      @subject = build(:subject,
                       name: @sub_env['name'],
                       id: @sub_env['id'],
                       surname: @sub_env['surname'],
                       userid: @sub_env['userid'],
                       email: @sub_env['email'])
      @subject.add_field('abilities')
      @subject.abilities = 'tdd expert'
      @episode = create(:test_episode, subject_id: @subject.id)
      @subject_manager = double('SubjectManager')
      allow(@subject_manager)
        .to receive(:get_subject).with(@subject.id.to_s) { @subject }
      executor.instance_variable_set(:@subject_manager, @subject_manager)
    end

    after(:each) do
      @episode.destroy
    end

    it 'raises an EnvConversionError if provided argument' \
       'is not a TestEpisode' do
      expect { executor.episode_to_env('') }.to raise_error(EnvConversionError)
    end

    it 'environment contains a episode id' do
      key = ScriptExecution::EPISODE_ID
      result_env = executor.episode_to_env(@episode)

      expect(result_env.key?(key)).to be true
      expect(result_env[key]).to eql(@episode.id.to_s)
    end

    it 'environment contains scheduling information' do
      now = Time.now
      later = now + 300
      @episode.schedule_start = now
      @episode.schedule_end = later
      result_env = executor.episode_to_env(@episode)

      expect(result_env).to include('ARTIFACTOMAT_TESTEPISODE_SCHEDULE_START' => now.to_s)
      expect(result_env).to include('ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END' => later.to_s)
    end

    it 'environment contains the correct subject' do
      result_env = executor.episode_to_env(@episode)

      @sub_env.each_key do |k|
        key = ScriptExecution::SUBJECT_ENV_PREFIX + k.upcase
        expect(result_env.key?(key)).to be true
        expect(result_env[key]).to eql(@sub_env[k])
      end
    end
  end
end
