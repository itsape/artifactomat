# frozen_string_literal: true

require 'artifactomat/modules/logging'

describe Logging do
  before(:each) do
    @now = Time.now
    allow(Time).to receive(:now).and_return(@now)
  end

  it 'is always somewhat initialized' do
    expect do
      Logging.debug('test', '123')
    end.to output("<#{@now.strftime('%a %d %b %T %Y')}> <DEBUG> <test> [123]\n").to_stdout
  end

  it 'initializes Logging' do
    Logging.initialize(4)
    expect(Logging.instance_variable_get(:@log_level)).to eq(4)
  end

  describe 'log event' do
    before(:each) do
      Logging.initialize
      Logging.instance_variable_set(:@log_level, 4)
    end

    describe 'after initialization' do
      modulename = 'Testmodul'
      message = 'Test Message'

      it 'gets an error message' do
        level = 'ERROR'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.error(modulename, message) }
          .to output(log_entry).to_stdout
      end

      it 'gets a warning message' do
        level = 'WARNING'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.warning(modulename, message) }
          .to output(log_entry).to_stdout
      end

      it 'gets an info message' do
        level = 'INFO'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.info(modulename, message) }
          .to output(log_entry).to_stdout
      end

      it 'gets a debug message' do
        level = 'DEBUG'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.debug(modulename, message) }
          .to output(log_entry).to_stdout
      end
    end

    describe 'with different log levels after initialization' do
      modulename = 'Testmodul'
      message = 'Test Message'

      it 'gets an message at log level 3' do
        level = 'ERROR'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        Logging.instance_variable_set(:@log_level, 3)
        expect { Logging.error(modulename, message) }
          .to output(log_entry).to_stdout
        level = 'WARNING'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.warning(modulename, message) }
          .to output(log_entry).to_stdout
        level = 'INFO'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.info(modulename, message) }
          .to output(log_entry).to_stdout
        level = 'DEBUG'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.debug(modulename, message) }
          .to_not output(log_entry).to_stdout
      end

      it 'gets a message at log level 2' do
        level = 'ERROR'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        Logging.instance_variable_set(:@log_level, 2)
        expect { Logging.error(modulename, message) }
          .to output(log_entry).to_stdout
        level = 'WARNING'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.warning(modulename, message) }
          .to output(log_entry).to_stdout
        level = 'INFO'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.info(modulename, message) }
          .to_not output(log_entry).to_stdout
        level = 'DEBUG'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.debug(modulename, message) }
          .to_not output(log_entry).to_stdout
      end

      it 'gets an message at log level 1' do
        level = 'ERROR'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        Logging.instance_variable_set(:@log_level, 1)
        expect { Logging.error(modulename, message) }
          .to output(log_entry).to_stdout
        level = 'WARNING'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.warning(modulename, message) }
          .to_not output(log_entry).to_stdout
        level = 'INFO'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.info(modulename, message) }
          .to_not output(log_entry).to_stdout
        level = 'DEBUG'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.debug(modulename, message) }
          .to_not output(log_entry).to_stdout
      end

      it 'gets a message at log level 0' do
        level = 'ERROR'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        Logging.instance_variable_set(:@log_level, 0)
        expect { Logging.error(modulename, message) }
          .to_not output(log_entry).to_stdout
        level = 'WARNING'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.warning(modulename, message) }
          .to_not output(log_entry).to_stdout
        level = 'INFO'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.info(modulename, message) }
          .to_not output(log_entry).to_stdout
        level = 'DEBUG'
        log_entry = "<#{@now.strftime('%a %d %b %T %Y')}> " \
          "<#{level}> <#{modulename}> [#{message}]\n"
        expect { Logging.debug(modulename, message) }
          .to_not output(log_entry).to_stdout
      end
    end
  end

  context 'is able to use colored messages' do
    before(:each) do
      Logging.initialize
      Logging.instance_variable_set(:@log_colored, true)
    end

    describe 'on stdout' do
      modulename = 'Testmodul'
      message = 'Test Message'

      it 'gets an error message' do
        level = 'ERROR'
        log_entry = /<#{@now.strftime('%a %d %b %T %Y')}> <\e\[\d+m(\e\[\d+m)?#{level}\e\[\d+m(\e\[\d+m)?> <#{modulename}> \[#{message}\]/
        expect { Logging.error(modulename, message) }.to output(log_entry).to_stdout
      end

      it 'gets a warning message' do
        level = 'WARNING'
        log_entry = /<#{@now.strftime('%a %d %b %T %Y')}> <\e\[\d+m(\e\[\d+m)?#{level}\e\[\d+m(\e\[\d+m)?> <#{modulename}> \[#{message}\]/
        expect { Logging.warning(modulename, message) }.to output(log_entry).to_stdout
      end

      it 'gets an info message' do
        level = 'INFO'
        log_entry = /<#{@now.strftime('%a %d %b %T %Y')}> <\e\[\d+m(\e\[\d+m)?#{level}\e\[\d+m(\e\[\d+m)?> <#{modulename}> \[#{message}\]/
        expect { Logging.info(modulename, message) }.to output(log_entry).to_stdout
      end

      it 'gets a debug message' do
        level = 'DEBUG'
        log_entry = /<#{@now.strftime('%a %d %b %T %Y')}> <\e\[\d+m(\e\[\d+m)?#{level}\e\[\d+m(\e\[\d+m)?> <#{modulename}> \[#{message}\]/
        expect { Logging.debug(modulename, message) }.to output(log_entry).to_stdout
      end
    end
  end
end
