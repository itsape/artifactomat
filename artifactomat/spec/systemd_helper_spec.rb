# frozen_string_literal: true

require 'socket'
require 'fileutils'

require 'artifactomat/utils/systemd_helper'

describe Systemd do
  before(:all) do
    @old_notify_socket = ENV['NOTIFY_SOCKET']
    @notify_sock_addr = '/tmp/test_notify_sock'
    ENV['NOTIFY_SOCKET'] = @notify_sock_addr
    @socket = Socket.new(:UNIX, :DGRAM)
    @socket.bind(Addrinfo.unix(@notify_sock_addr, :DGRAM))
  end

  after(:all) do
    @socket.close
    FileUtils.rm(@notify_sock_addr)
    ENV['NOTIFY_SOCKET'] = @old_notify_socket
  end

  describe '#ready' do
    it 'sends a ready signal to the notify socket' do
      Systemd.ready
      resp, = @socket.recvfrom(7)
      expect(resp).to eq('READY=1')
    end

    it 'does not crash if NOTIFY_SOCKET is not set' do
      allow(Logging).to receive(:warning)

      ENV['NOTIFY_SOCKET'] = nil
      expect { Systemd.ready }.not_to raise_error
      ENV['NOTIFY_SOCKET'] = @notify_sock_addr
    end

    it 'logs if NOTIFY_SOCKET is not set' do
      expect(Logging).to receive(:warning).with('Systemd', /No systemd runtime detected/)

      ENV['NOTIFY_SOCKET'] = nil
      Systemd.ready
      ENV['NOTIFY_SOCKET'] = @notify_sock_addr
    end
  end

  it '#present? returns true iff NOTIFY_SOCKET is not nil' do
    expect(Systemd.present?).to be_truthy
    ENV['NOTIFY_SOCKET'] = 'some nonsensical string'
    expect(Systemd.present?).to be_truthy
    ENV['NOTIFY_SOCKET'] = nil
    expect(Systemd.present?).to be_falsy
    ENV['NOTIFY_SOCKET'] = @notify_sock_addr
  end
end
