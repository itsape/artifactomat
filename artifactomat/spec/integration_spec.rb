# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require 'cgi'
require 'rspec'
require 'rack/test'
require 'fileutils'
require 'fakefs/spec_helpers'
require 'artifactomat/modules/ape/ape_server'
require 'tzinfo'
require 'tzinfo/data' # has to be done before filesystem is mocked

#                 UI + BANANA
# ------------------------------------------- PORT (SYSTEM)
#                   APE                     |
#            ... OTHER MODULES ...          | INTERATION TEST
#               TRACK_COLLECTOR             |
# ------------------------------------------- PORT (SYSTEM)
#                  SATELITE

# rubocop:disable Style/GlobalVars
describe 'APE Integration Test:', integration: true, order: :defined do
  include Rack::Test::Methods
  include FakeFS::SpecHelpers::All

  def app
    ApeServer.new
  end

  def clean_db
    TestProgram.destroy_all
    TestSeries.destroy_all
    TestSeason.destroy_all
    TestEpisode.destroy_all

    TrackEntry.destroy_all
  end

  def build_two_trackfilters(recipe)
    tf = TrackFilter.new
    tf.course_of_action = 'phishing link clicked'
    tf.extraction_pattern = [/\[(?<timestamp>[0-9]+)\] link clicked by (?<subject>.+) in (?<test_episode_id>[0-9]+)$/]
    tf.pattern = /(.+)/
    tf.recipe_id = recipe.id
    tf.score = 42
    tf2 = TrackFilter.new
    tf2.course_of_action = 'data filled'
    tf2.extraction_pattern = [/\[(?<timestamp>[0-9]+)\] data filled in by (?<subject>.+) in (?<test_episode_id>[0-9]+)$/]
    tf2.pattern = /(.+)/
    tf2.recipe_id = recipe.id
    tf2.score = 31
    [tf, tf2]
  end

  def write_recipe
    ie = build(:infrastructure_element, generate_script: 'exit 0',
                                        monitor_script: 'exit 0',
                                        ports: [80, 443],
                                        names: ['only.me'])
    recipe = build(:recipe, infrastructure: [ie],
                            deploy_script: 'exit 0',
                            artifact_script: 'exit 0',
                            arm_script: 'exit 0',
                            duration: 3)

    recipe.trackfilters = build_two_trackfilters recipe
    # pp recipe
    File.open('/etc/artifactomat/recipes/minimal.yml', 'w') do |f|
      f.write YAML.dump(recipe)
    end
  end

  def write_subject
    subjects = "id,name,surname,userid,email,department\n" \
               "1,Max,Mustermann,maxi,max@muster.man,schablone\n"
    subject_location = '/etc/artifactomat/subjects'
    FileUtils.mkpath(subject_location)
    File.open(File.join(subject_location, 'muster.csv'), mode: 'w') do |f|
      f.write subjects
    end
  end

  let(:header) { { 'HTTP_API_VERSION' => '1.0', 'Content-Type' => 'application/yaml' } }

  before(:all) do
    # This is used to disable rpc, just replace rpc-client implementations
    # with real implementations.
    SubjectTrackerRpcClient = SubjectTracker
    TrackCollectorRpcClient = TrackCollector
    MonitoringRpcClient = Monitoring
    RoutingClient = Routing
    TestSchedulerRpcClient = TestScheduler

    FakeFS.activate!
    configs = File.join(File.dirname(__FILE__), '..', 'dist', 'etc', 'artifactomat')
    FakeFS::FileSystem.clone(File.expand_path(configs), '/etc/artifactomat')
    $LOAD_PATH.each do |path|
      FakeFS::FileSystem.clone(path) if path.include? 'active_support'
      FakeFS::FileSystem.clone(path) if path.include? 'tzinfo'
    end
    write_recipe
    write_subject
  end

  after(:all) do
    Ape.instance.monitoring&.stop
    FakeFS.deactivate!
    clean_db
    Object.send(:remove_const, 'SubjectTrackerRpcClient')
    Object.send(:remove_const, 'TrackCollectorRpcClient')
    Object.send(:remove_const, 'MonitoringRpcClient')
    Object.send(:remove_const, 'RoutingClient')
    Object.send(:remove_const, 'TestSchedulerRpcClient')
  end

  before(:each) do
    @iptables_mock = double('iptables-integration-spec-mock')
    allow(@iptables_mock).to receive(:activate)
    allow(@iptables_mock).to receive(:append)
    allow(@iptables_mock).to receive(:get_active_rules) { [] }
    @ipset_mock = double('ipset-integration-spec-mock')
    allow(@ipset_mock).to receive(:create)
    allow(@ipset_mock).to receive(:matchset) do |attrs|
      "--match-set #{attrs[:set_name]} #{attrs[:flags]}"
    end
    allow(Tablomat::IPTables).to receive(:new).and_return(@iptables_mock)
    allow(Tablomat::IPSet).to receive(:new).and_return(@ipset_mock)
    # Something is resetting the redis store after every test...
    Ape.instance.send(:init_news)
  end

  context 'PREFACE:' do
    context 'user can' do
      it 'see recipes' do
        get '/recipe/list', {}, header
        expect(last_response.status).to eq(200)
        recipe_list = YAML.safe_load(last_response.body)
        expect(recipe_list).to contain_exactly('minimal', 'transparent_test')
      end
    end
  end

  context 'CREATION:' do
    context 'user gets' do
      it 'a status' do
        get '/status', {}, header
        expect(last_response).to be_ok
        expect(last_response.body).to include('Monitoring')
        expect(last_response.body).to include('TestScheduler')
        expect(last_response.body).to include('TrackCollector')
        expect(last_response.body).to include('SubjectTracker')
        expect(last_response.body).to include('Jobs')
      end

      it 'a recipe list' do
        get '/recipe/list', {}, header
        expect(last_response.body).to include('minimal')
      end

      it 'a recipe parameters list' do
        get '/recipe/parameters/minimal', {}, header
        expect(last_response.body).to include('--- {}')
      end

      it 'a subject list' do
        get '/subject/list', {}, header
        expect(last_response.body).to include('muster.csv')
      end
    end

    context 'user creates' do
      it 'a new program' do
        expect(Logging).to receive(:info)
        post '/program/new', { 'label': 'muster_program' }.to_yaml, header
        expect(last_response.status).to eq(201)
        expect(TestProgram.count).to be(1)
      end

      it 'a new series with the information listed' do
        get '/program/list', {}, header
        expect(last_response.status).to eq(200)
        expect(last_response.body).to include('muster_program')
        answer = YAML.safe_load last_response.body, [Time, Symbol]
        expect(Logging).to receive(:info)
        $sleepy = Time.now + 20
        hash = { 'label' => 'muster_series',
                 'start_date' => $sleepy.utc,
                 'end_date' => ($sleepy + 6 * 60).utc,
                 'group_file' => 'muster.csv' }
        post "/series/new/#{answer.first['id']}", hash.to_yaml, header
        expect(last_response.status).to eq(201)
        expect(TestSeries.count).to be(1)
      end

      it 'a new season with the information listed' do
        expect(Logging).to receive(:debug).twice.with('ArtifactGenerator', any_args)
        get '/series/list', {}, header
        expect(last_response.status).to eq(200)
        expect(last_response.body).to include('muster_series')
        answer = YAML.safe_load last_response.body, [Time, Symbol]
        series_id = answer.first['id']
        expect(Logging).to receive(:info)
        allow(Logging).to receive(:debug)
        hash = { 'label' => 'somelabel',
                 'duration' => 5,
                 'recipe_id' => 'minimal',
                 'recipe_parameters' => {} }
        post "/season/new/#{series_id}", hash.to_yaml, header
        expect(last_response.status).to eq(201)
        expect(TestSeason.count).to be(1)
      end
    end

    context 'user starts and' do
      it 'schedules series' do
        get '/series/list', {}, header
        expect(last_response.status).to eq(200)
        expect(last_response.body).to include('muster_series')
        answer = YAML.safe_load last_response.body, [Time, Symbol]
        series_id = answer.first['id']

        expect(Logging).to receive(:info).with('APE', any_args)
        post "/series/schedule/#{series_id}", { 'force' => true }, header
        expect(last_response.status).to eq(204)
        expect(TestEpisode.count).to be(1)
      end

      it 'sees schedule' do
        get '/series/list', {}, header
        expect(last_response.status).to eq(200)
        expect(last_response.body).to include('muster_series')
        answer = YAML.safe_load last_response.body, [Time, Symbol]
        series_id = answer.first['id']
        get "/series/show/#{series_id}", {}, header
        expect(last_response.status).to eq(200)
        expect(last_response.body).to include('schedule_start: 20')
        expect(last_response.body).to include('schedule_end: 20')
      end

      it 'prepares series' do
        # replace the routing doubles with the freshly created ones, because doubles are only valid
        # for one example each!
        Ape.instance.routing.instance_variable_set(:@ipset, @ipset_mock)
        Ape.instance.routing.instance_variable_set(:@iptables, @iptables_mock)
        # capture the logs... since one cannot expect them anymore
        the_log = StringIO.new
        stdout = $stdout
        $stdout = the_log
        # if i don't stop the scheduler here it fails
        Ape.instance.monitoring.run

        # these are the messages that should be recieved.
        # they are thrown... but when i expect them they die
        # expect(Logging).to receive(:debug).at_least(:once).with('ArtifactGenerator', any_args)
        # expect(Logging).to receive(:debug).at_least(:once).with('Monitoring', any_args)
        # expect(Logging).to receive(:debug).twice.with('DeploymentManager', any_args)
        # expect(Logging).to receive(:info).twice.with('DeploymentManager', any_args)
        # expect(Logging).to receive(:debug).once.with('Routing', any_args)
        # expect(Logging).to receive(:info).once.with('Routing', any_args)
        # expect(Logging).to receive(:info).once.with('ArtifactGenerator', any_args)
        # expect(Logging).to receive(:info).thrice.with('InfrastructureGenerator', any_args)
        # expect(Logging).to receive(:info).once.with('APE', any_args)
        # expect(Logging).to receive(:info).at_least(3).times
        # expect(Logging).to receive(:debug).at_least(3).times

        get '/series/list', {}, header
        expect(last_response.status).to eq(200)
        expect(last_response.body).to include('muster_series')
        answer = YAML.safe_load last_response.body, [Time, Symbol]
        series_id = answer.first['id']

        get "/series/show/#{series_id}", {}, header
        expect(last_response.status).to eq(200)
        expect(last_response.body).to include('schedule_start: 20')
        expect(last_response.body).to include('schedule_end: 20')

        get "/series/prepare/#{series_id}", {}, header
        expect(last_response.status).to eq(204)

        series = TestSeries.find(series_id)
        begin
          Timeout.timeout(180) do
            sleep(1) until Ape.instance.deployment_manager.infrastructure_ready?(series)
          end
        rescue Timeout::Error
          STDOUT.puts the_log.string
          raise
        end
        Ape.instance.monitoring.stop
        expect(Ape.instance.infrastructure_generator.instance_variable_get(:@scheduler).jobs.size).to eq(1)
        expect(Ape.instance.routing.instance_variable_get(:@scheduler).jobs.size).to eq(1)

        expect(Ape.instance.deployment_manager.infrastructure_ready?(series)).to be_truthy
        expect(Ape.instance.deployment_manager.stop_deployment?(series)).to be_truthy

        get '/info', {}, header
        expect(last_response.status).to eq(200)
        answer = last_response.body
        expect(answer).to include('muster_series')
        expect(answer).to include('prepared')

        $stdout = stdout
        # AG does not report nil values
        expect(the_log.string).not_to match(/<ArtifactGenerator> <DEBUG> .* schedule_start: nil/)
        expect(the_log.string).not_to match(/<ArtifactGenerator> <DEBUG> .* schedule_stop: nil/)
      end

      it 'approves schedule' do
        get '/series/list', {}, header
        expect(last_response.status).to eq(200)
        expect(last_response.body).to include('muster_series')
        answer = YAML.safe_load last_response.body, [Time, Symbol]
        series_id = answer.first['id']
        expect(Logging).to receive(:info).at_least(:once)
        expect(Logging).to receive(:debug).once
        expect(Ape.instance.delivery_manager).to \
          receive(:arm)
        get "/series/approve/#{series_id}", {}, header
        expect(last_response.status).to eq(204)
        sleep 0.1 until $sleepy < Time.now
        sleep 5 # give it some time
      end
    end
  end

  context 'AFTERMATH:' do
    it 'user loads results' do
      expect(Logging).to receive(:info).twice
      episode = TestEpisode.last
      # log has to specify the episode id...
      log = "[1471445699] link clicked by muster.csv/1 in #{episode.id}\n[1471445701] data filled in by muster.csv/1 in #{episode.id}\n"
      data = { 'trace' => log }.to_yaml
      response = "test_episode,subject_id,score,course_of_action,timestamp\n#{episode.id},muster.csv/1,42"
      post '/import/trace', data, header

      body = YAML.safe_load(last_response.body)
      expect(last_response).to be_ok
      expect(body.first.lines.size).to eq(3) # 2 track_entries plus csv header
      expect(body.first).to include(response)
    end

    it 'user gets results by series' do
      get '/series/list', {}, header
      expect(last_response.status).to eq(200)
      expect(last_response.body).to include('muster_series')
      answer = YAML.safe_load last_response.body, [Time, Symbol]
      series_id = answer.first['id']
      expect(Logging).to receive(:info)
      data = { 'headers' => [['spec'], 'spec'], 'format' => 'csv' }.to_yaml
      post "/report/series/#{series_id}", data, header
      expect(last_response.status).to eq(200)
      msg = YAML.safe_load last_response.body, [Time, Symbol]

      expect(msg.first.lines.count).to be 3 # 2 track_entries plus csv header
    end
  end
end
# rubocop:enable Style/GlobalVars
