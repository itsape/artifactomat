# frozen_string_literal: true

require 'rspec'
require 'open3'
require 'socket'
require 'timeout'
require 'fileutils'
require 'yaml'
require 'active_record'
require 'support/factory_bot'
require 'net/http'

require_relative '../system/system_spec_helper'

SERIES_AMOUNT = 5
INF_ELEMENT_AMOUNT = 25

describe "MONITORING STRESS TEST – #{SERIES_AMOUNT * INF_ELEMENT_AMOUNT} inf elements", system: true, order: :defined do
  before(:all) do
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']
    ActiveRecord::Base.establish_connection(db_config)

    @logger = SpecLogger.new('monitoring_stress_spec')
    @ape = Command.new
    @cmd = Command.new
    @aped = SystemdService.new('artifactomat-aped.service')
    @apenames = SystemdService.new('artifactomat-apenames.service')
    @jobspooler = SystemdService.new('artifactomat-ape-job-spooler.service')
    @redis = SystemdService.new('redis-server@artifactomat.service')
    @subject_tracker = SystemdService.new('artifactomat-subject-tracker.service')
    @routing = SystemdService.new('artifactomat-routing.service')
    @test_scheduler = SystemdService.new('artifactomat-ape-scheduler.service')
    @monitoring = SystemdService.new('artifactomat-ape-monitoring.service')

    @cert_path = '/etc/artifactomat/certs'

    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.ip_forward=1'
    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.conf.all.route_localnet=1'

    @cmd.run_and_wait 'sudo artifactomat_setup db'
    @cmd.run_and_wait 'sudo artifactomat_setup app --yes', 120

    # hashes keep track of armed subjects and time
    @duration = 2

    # create an individual recipe for each series, because you cant safely have
    # the same recipe running multiple times at once
    SERIES_AMOUNT.times do |series|
      # build and write a recipe
      inf_elements = []
      INF_ELEMENT_AMOUNT.times do |ie_no|
        ie = build(:infrastructure_element, generate_script: 'mkdir -p /tmp/artifactomat/monitoring',
                                            monitor_script: "echo 'Hello World' >> /tmp/artifactomat/monitoring/ie-#{series}_#{ie_no}.mon",
                                            destroy_script: 'exit 0',
                                            ports: [443])
        inf_elements << ie
      end
      recipe = build(:recipe, infrastructure: inf_elements,
                              deploy_script: 'exit 0',
                              artifact_script: 'exit 0',
                              arm_script: 'exit 0',
                              disarm_script: 'exit 0',
                              parameters: { 'parameter' => '21' },
                              duration: @duration)

      write_recipe("minimal_#{series}.yml", recipe)
    end

    subjects_alpha = "id,name,surname,userid,email\n1,1,1,sub1,1@1&1.de\n"
    write_subjects('alpha.csv', subjects_alpha)
    set_log_lvl_debug
  end

  def kill_aped
    @aped.kill
    @logger.log(@aped.status)
  end

  def kill_subject_tracker
    @subject_tracker.kill
    @logger.log(@subject_tracker.status)
  end

  def kill_monitoring
    @monitoring.kill
    @logger.log(@monitoring.status)
  end

  after(:all) do
    kill_aped
    kill_subject_tracker
  end

  after(:each) do |example|
    if example.exception
      kill_aped
      kill_subject_tracker
      kill_monitoring
    end
  end

  it 'starting aped' do
    @redis.start
    @subject_tracker.start
    @apenames.start
    @routing.start
    @jobspooler.start
    @test_scheduler.start
    @monitoring.start
    @aped.start
    sleep 6

    @aped.escalate_error
    @subject_tracker.escalate_error
    abort("\e[31m\nCAUTION!!\e[0m:\n#{@redis.status}") unless @redis.active?

    expect(@subject_tracker.active?).to be_truthy
    expect(@apenames.active?).to be_truthy
    expect(@aped.active?).to be_truthy, 'Problem with starting aped!'
    expect(@jobspooler.active?).to be_truthy
    expect(@routing.active?).to be_truthy
    expect(@test_scheduler.active?).to be_truthy
    expect(@monitoring.active?).to be_truthy
  end

  describe "schedules, prepares and approves #{SERIES_AMOUNT} series" do
    it 'setup and run TestSeries' do
      expected_program_count = TestProgram.count + 1
      expected_series_count = TestSeries.count + SERIES_AMOUNT
      expected_season_count = TestSeason.count + SERIES_AMOUNT

      @ape.run_and_wait "ape program new -l 'MonitoringTestProgram'"

      series_ids = []
      SERIES_AMOUNT.times do |i|
        # start and end are irrelevant; monitoring runs after preparing!
        @ape.run_and_wait "ape series new -g 'alpha.csv' -s 'in 500 sec' -e 'in 1000 sec' -l 'MonitoringTestSeries#{i}' #{TestProgram.last.id}"
        series_id = TestSeries.last.id
        series_ids << series_id
        @ape.run_and_wait "ape season new -r 'minimal_#{i}' -d #{@duration}m -l 'MonitoringTestSeason_#{i}' #{series_id}"
      end

      expect(TestProgram.count).to eq(expected_program_count)
      expect(TestSeries.count).to eq(expected_series_count)
      expect(TestSeason.count).to eq(expected_season_count)

      series_ids.each do |series_id|
        @ape.run_and_wait "ape series schedule #{series_id}"
      end
      series_ids.each do |series_id|
        @ape.run_and_wait "ape series prepare #{series_id}"
      end

      begin
        prepared = []
        # TODO: This can be lowered once infrastructure building is parallelized in #113
        Timeout.timeout(INF_ELEMENT_AMOUNT * 10 + 230) do
          loop do
            @aped.each_new_line do |line|
              match = line.match(/<APE> \[Series "MonitoringTestSeries(?<series>\d+)" \(\d+\) fully prepared, you may approve now\.\]/)
              prepared.push(match[:series]) if match
            end
            breakage = true
            SERIES_AMOUNT.times do |i|
              breakage = false unless prepared.include?(i.to_s)
            end
            break if breakage

            sleep 1
          end
        end
      rescue Timeout::Error
        raise "Timeout while waiting for framework to allow approval. Prepared series: #{prepared}"
      end
    end

    it 'monitors' do
      wait_time = 120 # wait time in seconds
      @cmd.run_and_wait 'sudo rm /tmp/artifactomat/monitoring/*.mon'
      sleep wait_time
      SERIES_AMOUNT.times do |series|
        INF_ELEMENT_AMOUNT.times do |ie|
          content = File.read("/tmp/artifactomat/monitoring/ie-#{series}_#{ie}.mon").split("\n")
          # we expect 1 monitoring entry every 10 seconds
          expect(content.size).to be_within(1).of(wait_time / 10)
        end
      end
    end
  end
end
