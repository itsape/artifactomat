# frozen_string_literal: true

require 'rspec'
require 'open3'
require 'socket'
require 'timeout'
require 'fileutils'
require 'yaml'
require 'active_record'
require 'support/factory_bot'
require 'net/http'

require_relative '../system/system_spec_helper'

SUBJECT_AMOUNT = 1000

def calculate_ip(idx)
  "127.1.#{idx / 256}.#{idx % 256}"
end

describe "SUBJECT TRACKING STRESS TEST – #{SUBJECT_AMOUNT} subjects", system: true, order: :defined do
  before(:all) do
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']
    ActiveRecord::Base.establish_connection(db_config)

    @logger = SpecLogger.new('subject_tracking_stress_spec')
    @aped = SystemdService.new('artifactomat-aped.service')
    @ape = Command.new
    @cmd = Command.new
    @apenames = SystemdService.new('artifactomat-apenames.service')
    @jobspooler = SystemdService.new('artifactomat-ape-job-spooler.service')
    @redis = SystemdService.new('redis-server@artifactomat.service')
    @subject_tracker = SystemdService.new('artifactomat-subject-tracker.service')
    @test_scheduler = SystemdService.new('artifactomat-ape-scheduler.service')
    @routing = SystemdService.new('artifactomat-routing.service')
    @monitoring = SystemdService.new('artifactomat-ape-monitoring.service')

    @cert_path = '/etc/artifactomat/certs'

    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.ip_forward=1'
    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.conf.all.route_localnet=1'

    @cmd.run_and_wait 'sudo artifactomat_setup db'
    @cmd.run_and_wait 'sudo artifactomat_setup app --yes', 120

    subjects_alpha = +"id,name,surname,userid,email\n"
    (1..SUBJECT_AMOUNT).each do |i|
      subjects_alpha << "#{i},#{i},#{i},sub#{i},#{i}@1&1.de\n"
    end

    # increase subject timeout
    @st_conf = YAML.safe_load(File.read('/etc/artifactomat/conf.d/subjecttracker.yml'))
    @st_conf['plugins']['ClientProxyAdapter']['subject_timeout'] = 60
    File.open('/tmp/st.yml', 'w') do |fp|
      fp.write YAML.dump(@st_conf)
    end
    @cmd.run_and_wait 'sudo mv /tmp/st.yml /etc/artifactomat/conf.d/subjecttracker.yml'

    write_subjects('alpha.csv', subjects_alpha)
    set_log_lvl_debug
  end

  def send_action(subject, ip, action)
    data = Hash[subject, { ip: ip, action: action }]
    connection = build_connection('/etc/artifactomat/certs')
    connection.puts(data.to_yaml)
    connection.sysclose
  rescue StandardError => e
    @logger.log("Error while sending action: #{e}")
  end

  def threaded_send_actions(action, amount = SUBJECT_AMOUNT)
    threads = []
    subjects_per_thread = 10
    (amount / subjects_per_thread).times do |i|
      threads << Thread.new do
        (1..subjects_per_thread).each do |j|
          sub_no = subjects_per_thread * i + j
          send_action("sub#{sub_no}", calculate_ip(sub_no), action)
        end
      end
    end
    threads.each(&:join)
  end

  def kill_aped
    @aped.kill
    @logger.log(@aped.status)
  end

  def kill_subject_tracker
    @subject_tracker.kill
    @logger.log(@subject_tracker.status)
  end

  after(:all) do
    kill_aped
    kill_subject_tracker
  end

  after(:each) do |example|
    if example.exception
      kill_aped
      kill_subject_tracker
    end
  end

  it 'starting aped' do
    @redis.start
    @subject_tracker.start
    @apenames.start
    @routing.start
    @jobspooler.start
    @test_scheduler.start
    @monitoring.start
    @aped.start
    sleep 6

    @aped.escalate_error
    @subject_tracker.escalate_error
    abort("\e[31m\nCAUTION!!\e[0m:\n#{@redis.status}") unless @redis.active?

    expect(@subject_tracker.active?).to be_truthy
    expect(@test_scheduler.active?).to be_truthy
    expect(@monitoring.active?).to be_truthy
    expect(@routing.active?).to be_truthy
    expect(@apenames.active?).to be_truthy
    expect(@aped.active?).to be_truthy, 'Problem with starting aped!'
    expect(@jobspooler.active?).to be_truthy
  end

  describe 'Subject tracking' do
    it 'recognizes when subjects come online' do
      threaded_send_actions 'login'

      @cmd.run_and_wait 'ape report online'
      data = YAML.safe_load(@cmd.stdout.join(''))
      offline_subjects = []
      data.each do |subject|
        offline_subjects << subject['subject'] unless subject['online']
      end
      expect(offline_subjects).to be_empty
    end

    it 'recognizes when subjects go offline' do
      threaded_send_actions 'login'
      @cmd.run_and_wait 'ape report online'
      data = YAML.safe_load(@cmd.stdout.join(''))
      offline_subjects = []
      data.each do |subject|
        offline_subjects << subject['subject'] unless subject['online']
      end
      expect(offline_subjects).to be_empty

      threaded_send_actions 'logout'
      @cmd.run_and_wait 'ape report online'
      data = YAML.safe_load(@cmd.stdout.join(''))
      online_subjects = []
      data.each do |subject|
        online_subjects << subject['subject'] if subject['online']
      end
      expect(online_subjects).to be_empty
    end

    it 'shows the user as offline after the timeout' do
      # make sure subjects are online
      threaded_send_actions 'login'
      @cmd.run_and_wait 'ape report online'
      data = YAML.safe_load(@cmd.stdout.join(''))
      offline_subjects = []
      data.each do |subject|
        offline_subjects << subject['subject'] unless subject['online']
      end
      expect(offline_subjects).to be_empty

      # sleep 5 more seconds to give the framework time to process all the timeouts
      # 5 = 2 * sleep_time + 1 (see ClientProxyAdapter#timeout_loop)
      sleep_time = @st_conf['plugins']['ClientProxyAdapter']['subject_timeout'] + 5
      sleep(sleep_time / 2)
      # send keepalive for half the subjects
      threaded_send_actions 'ping', SUBJECT_AMOUNT / 2
      sleep(sleep_time / 2)

      @cmd.run_and_wait 'ape report online'
      data = YAML.safe_load(@cmd.stdout.join(''))

      expected_online_subjects = []
      (1..SUBJECT_AMOUNT / 2).each do |i|
        expected_online_subjects << "alpha.csv/#{i}"
      end

      online_subjects = []
      data.each do |subject|
        online_subjects << subject['subject'] if subject['online']
      end
      expect(online_subjects).to eq(expected_online_subjects)
    end
  end
end
