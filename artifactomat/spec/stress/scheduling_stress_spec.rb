# frozen_string_literal: true

require 'rspec'
require 'open3'
require 'socket'
require 'timeout'
require 'fileutils'
require 'yaml'
require 'active_record'
require 'support/factory_bot'
require 'net/http'

require_relative '../system/system_spec_helper'

SUBJECT_AMOUNT = 1000
SERIES_AMOUNT = 5 # MUST be a divisor of SUBJECT_AMOUNT

def during(ep1, ep2)
  (@armed[ep2] <= @armed[ep1] && @disarmed[ep2] > @armed[ep1]) ||
    (@armed[ep1] <= @armed[ep2] && @disarmed[ep1] > @armed[ep2])
end

describe "SCHEDULING STRESS TEST – #{SUBJECT_AMOUNT} subjects", system: true, order: :defined do
  before(:all) do
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']
    ActiveRecord::Base.establish_connection(db_config)

    @logger = SpecLogger.new('scheduling_stress_spec')
    @ape = Command.new
    @cmd = Command.new
    @aped = SystemdService.new('artifactomat-aped.service')
    @apenames = SystemdService.new('artifactomat-apenames.service')
    @jobspooler = SystemdService.new('artifactomat-ape-job-spooler.service')
    @redis = SystemdService.new('redis-server@artifactomat.service')
    @subject_tracker = SystemdService.new('artifactomat-subject-tracker.service')
    @track_collector = SystemdService.new('artifactomat-track-collector.service')
    @routing = SystemdService.new('artifactomat-routing.service')
    @test_scheduler = SystemdService.new('artifactomat-ape-scheduler.service')
    @monitoring = SystemdService.new('artifactomat-ape-monitoring.service')

    @cert_path = '/etc/artifactomat/certs'

    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.ip_forward=1'
    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.conf.all.route_localnet=1'

    @cmd.run_and_wait 'sudo artifactomat_setup db'
    @cmd.run_and_wait 'sudo artifactomat_setup app --yes', 120

    # hashes keep track of armed subjects and time
    @armed_status = []
    @armed = {}
    @disarmed = {}
    @duration = 2

    # create an individual recipe for each series, because you cant safely have
    # the same recipe running multiple times at once
    SERIES_AMOUNT.times do |series|
      # build and write a recipe
      ie = build(:infrastructure_element, generate_script: 'exit 0',
                                          monitor_script: 'exit 0',
                                          destroy_script: 'exit 0',
                                          ports: [443])
      recipe = build(:recipe, infrastructure: [ie],
                              deploy_script: 'exit 0',
                              artifact_script: 'exit 0',
                              arm_script: "touch /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERID-minimal_#{series}",
                              disarm_script: "rm /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERID-minimal_#{series}",
                              parameters: { 'parameter' => '21' },
                              duration: @duration)

      write_recipe("minimal_#{series}.yml", recipe)
    end

    subjects_alpha = +"id,name,surname,userid,email\n"
    (1..SUBJECT_AMOUNT).each do |i|
      subjects_alpha << "#{i},#{i},#{i},sub#{i},#{i}@1&1.de\n"
    end

    write_subjects('alpha.csv', subjects_alpha)
    set_log_lvl_debug
  end

  def kill_aped
    @aped.kill
    @logger.log(@aped.status)
  end

  def kill_subject_tracker
    @subject_tracker.kill
    @logger.log(@subject_tracker.status)
  end

  def kill_track_collector
    @track_collector.kill
    @logger.log(@track_collector.status)
  end

  def kill_routing
    @routing.kill
    @logger.log(@routing.status)
  end

  def kill_scheduler
    @test_scheduler.kill
    @logger.log(@test_scheduler.status)
  end

  def kill_monitoring
    @monitoring.kill
    @logger.log(@monitoring.status)
  end

  after(:all) do
    @aped.kill
  end

  after(:each) do |example|
    @aped.clear_news

    if example.exception
      kill_aped
      kill_subject_tracker
      kill_track_collector
      kill_routing
      kill_scheduler
      kill_monitoring
    end
  end

  it 'starting aped' do
    @redis.start
    @subject_tracker.start
    @track_collector.start
    @apenames.start
    @routing.start
    @monitoring.start
    @jobspooler.start
    @test_scheduler.start
    @aped.start
    sleep 6

    @aped.escalate_error
    @subject_tracker.escalate_error
    @track_collector.escalate_error
    abort("\e[31m\nCAUTION!!\e[0m:\n#{@redis.status}") unless @redis.active?

    expect(@subject_tracker.active?).to be_truthy, 'Problem with subject_tracker!'
    expect(@track_collector.active?).to be_truthy, 'Problem with track_collector!'
    expect(@test_scheduler.active?).to be_truthy
    expect(@apenames.active?).to be_truthy
    expect(@aped.active?).to be_truthy, 'Problem with starting aped!'
    expect(@jobspooler.active?).to be_truthy
    expect(@routing.active?).to be_truthy
    expect(@monitoring.active?).to be_truthy
  end

  describe "schedules, prepares and approves 1 series with #{SUBJECT_AMOUNT} subjects" do
    it 'setup and run TestSeries' do
      expected_program_count = TestProgram.count + 1
      expected_series_count = TestSeries.count + 1
      expected_season_count = TestSeason.count + 1

      @ape.run_and_wait 'ape program new -l "ScheduleTestProgram"'

      # when scaling up SUBJECT_AMOUNT, this time should be scaled up aswell to avoid
      # the series start time to be before the series is approved.
      @ape.run_and_wait "ape series new -g 'alpha.csv' -s 'in 60 sec' -e 'in 180 sec'  -l 'ScheduleTestSeries1' #{TestProgram.last.id}"
      @ape.run_and_wait "ape season new -r 'minimal_0' -d 2m -l 'ScheduleTestSeason1_1' #{TestSeries.last.id}"

      expect(TestProgram.count).to eq(expected_program_count)
      expect(TestSeries.count).to eq(expected_series_count)
      expect(TestSeason.count).to eq(expected_season_count)

      @ape.run_and_wait "ape series schedule #{TestSeries.last.id}"
      @ape.run_and_wait "ape series prepare #{TestSeries.last.id}"

      output = ''
      Timeout.timeout(60) do
        until output.include? 'fully prepared'
          @ape.run_and_wait 'ape info'
          output = @ape.stdout.join("\n")
        end
      end
      @ape.run_and_wait "ape series approve #{TestSeries.last.id}"
    end

    it "arms and disarms #{SUBJECT_AMOUNT} episodes" do
      wait_time = (@duration * 60) + 60
      begin
        episode_to_arm = {}
        Timeout.timeout(wait_time) do
          loop do
            armed_te_ids = []
            # check log for finished arm/disarm events
            @test_scheduler.each_new_line do |line|
              match = line.match %r{<(?<ts>[\w\d: ]+)> <[^>]+> <\w+> \[(?<action>\w+) Episode \((?<episode>\d+)\) for (?<subject>\w+\.csv/\d+)}
              if match && (match[:action] == 'Armed')
                # check filesystem for arm-artifact
                armed_te_ids << match[:episode]
                Dir.glob("/tmp/artifactomat/#{match[:episode]}*") do |f|
                  arm_artifact = f.match(%r{/tmp/artifactomat/#{match[:episode]}-(?<artifact>\w+\d+-minimal_\d+)})
                  @armed[arm_artifact[:artifact]] = Time.parse(match[:ts]) unless arm_artifact.nil?
                  episode_to_arm[match[:episode]] = arm_artifact[:artifact] unless arm_artifact.nil?
                end
              end

              if match && (match[:action] == 'Disarmed')
                # check filesystem for arm-artifact
                @disarmed[episode_to_arm[match[:episode]]] = Time.parse(match[:ts]) unless File.exist? "/tmp/artifactomat/#{match[:episode]}-#{episode_to_arm[match[:episode]]}}"
              end
            end
            break if (@armed.size == SUBJECT_AMOUNT) && (@disarmed.size == SUBJECT_AMOUNT)

            sleep 1
            armed_te_ids.each do |id|
              te = TestEpisode.where("id = #{id}").first
              @armed_status << te.status
            end
          end
        end
      rescue Timeout::Error
        msg = "Timeout while waiting for framework to arm and disarm episodes\nArmed: #{@armed.size}, Disarmed: #{@disarmed.size}"
        raise msg
      end

      expect(@armed.size).to eq(SUBJECT_AMOUNT)
      expect(@disarmed.size).to eq(SUBJECT_AMOUNT)
    end

    it 'disarms episodes after their duration [+/- 5s]' do
      @armed.each_key do |key|
        expect(@disarmed[key] - @armed[key]).to be_within(5).of(@duration * 60)
      end
    end

    it 'sets running episodes to status "running' do
      @armed_status.each do |status|
        expect(status).to eq('running')
      end
    end

    it 'sets completed episodes status do "completed"' do
      tes = TestEpisode.where('status = 2')
      expect(tes.count).to eq(SUBJECT_AMOUNT)
    end
  end

  describe "schedules, prepares and approves #{SERIES_AMOUNT} series with #{SUBJECT_AMOUNT / SERIES_AMOUNT} subjects each" do
    before(:all) do
      @armed = {}
      @disarmed = {}
      # write new subject groups
      SERIES_AMOUNT.times do |i|
        subjects = +"id,name,surname,userid,email\n"
        (1..SUBJECT_AMOUNT / SERIES_AMOUNT).each do |j|
          subjects << "#{j},#{i}-#{j},#{i}-#{j},sub#{i}-#{j},#{i}-#{j}@1&1.de\n"
        end
        write_subjects("subjects#{i}.csv", subjects)
      end
      @ape.run_and_wait 'sudo rm /etc/artifactomat/subjects/alpha.csv'
      @aped.restart
      @subject_tracker.restart
      @track_collector.restart
      @routing.restart
      @test_scheduler.restart
      @monitoring.restart
      @test_scheduler.clear_news
      sleep 2
    end

    it 'setup and run TestSeries' do
      expected_program_count = TestProgram.count + 1
      expected_series_count = TestSeries.count + SERIES_AMOUNT
      expected_season_count = TestSeason.count + SERIES_AMOUNT

      @ape.run_and_wait "ape program new -l 'ScheduleTestProgram2'"
      expect(TestProgram.count).to eq(expected_program_count)

      # when scaling up SERIES_AMOUNT, this time should be scaled up aswell to avoid
      # the series start time to be before the series is approved.
      schedule_start = Time.now + 75
      series_ids = []
      SERIES_AMOUNT.times do |i|
        @ape.run_and_wait "ape series new -g 'subjects#{i}.csv' -s 'in 75 sec' -e 'in 195 sec' -l 'ScheduleTestSeries#{i}' #{TestProgram.last.id}"
        series_id = TestSeries.last.id
        series_ids << series_id
        @ape.run_and_wait "ape season new -r 'minimal_#{i}' -d 2m -l 'ScheduleTestSeason_#{i}' #{series_id}"
      end

      expect(TestSeries.count).to eq(expected_series_count)
      expect(TestSeason.count).to eq(expected_season_count)

      series_ids.each do |series_id|
        @ape.run_and_wait "ape series schedule #{series_id}"
      end
      series_ids.each do |series_id|
        @ape.run_and_wait "ape series prepare #{series_id}"
      end

      begin
        prepared = []
        Timeout.timeout(60) do
          loop do
            @aped.each_new_line do |line|
              match = line.match(/<APE> \[Series "ScheduleTestSeries(?<series>\d+)" \(\d+\) fully prepared, you may approve now\.\]/)
              prepared.push match[:series] if match
            end
            breakage = true
            SERIES_AMOUNT.times do |i|
              breakage = false unless prepared.include?(i.to_s)
            end
            break if breakage

            sleep 1
          end
        end
      rescue Timeout::Error
        raise "Timeout while waiting for framework to allow approval. Prepared series: #{prepared}"
      end
      series_ids.each do |series_id|
        @ape.run_and_wait "ape series approve #{series_id}"
      end
      sleep_until schedule_start
    end

    it "arms and disarms #{SUBJECT_AMOUNT} episodes" do
      wait_time = (@duration * 60) + 60

      begin
        episode_to_arm = {}
        Timeout.timeout(wait_time) do
          loop do
            armed_te_ids = []
            # check log for finished arm/disarm events
            @test_scheduler.each_new_line do |line|
              match = line.match %r{<(?<ts>[\w\d: ]+)> <[^>]+> <\w+> \[(?<action>\w+) Episode \((?<episode>\d+)\) for (?<subject>\w+\.csv/\d+)}
              if match && (match[:action] == 'Armed')
                # check filesystem for arm-artifact
                armed_te_ids << match[:episode]
                Dir.glob("/tmp/artifactomat/#{match[:episode]}*") do |f|
                  arm_artifact = f.match(%r{/tmp/artifactomat/#{match[:episode]}-(?<artifact>\w+\d+-\d+-minimal_\d+)})
                  @armed[arm_artifact[:artifact]] = Time.parse(match[:ts]) unless arm_artifact.nil?
                  episode_to_arm[match[:episode]] = arm_artifact[:artifact] unless arm_artifact.nil?
                end
              end

              if match && (match[:action] == 'Disarmed')
                # check filesystem for arm-artifact
                @disarmed[episode_to_arm[match[:episode]]] = Time.parse(match[:ts]) unless File.exist? "/tmp/artifactomat/#{match[:episode]}-#{episode_to_arm[match[:episode]]}}"
              end
            end
            break if (@armed.size == SUBJECT_AMOUNT) && (@disarmed.size == SUBJECT_AMOUNT)

            sleep 1
            armed_te_ids.each do |id|
              te = TestEpisode.where("id = #{id}").first
              @armed_status << te.status
            end
          end
        end
      rescue Timeout::Error
        raise "Timeout while waiting for framework to arm and disarm episodes\nArmed: #{@armed.size}, Disarmed: #{@disarmed.size}"
      end

      expect(@armed.size).to eq(SUBJECT_AMOUNT)
      expect(@disarmed.size).to eq(SUBJECT_AMOUNT)
    end

    it 'disarms episodes after their duration [+/- 5s]' do
      @armed.each_key do |key|
        expect(@disarmed[key] - @armed[key]).to be_within(5).of(@duration * 60)
      end
    end

    it 'sets running episodes status to "running"' do
      @armed_status.each do |status|
        expect(status).to eq('running')
      end
    end

    it 'sets completed episodes status to "completed"' do
      tes = TestEpisode.where('status = 2')
      expect(tes.count).to eq(2 * SUBJECT_AMOUNT)
    end
  end
end
