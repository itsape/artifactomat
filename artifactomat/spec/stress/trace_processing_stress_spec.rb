# frozen_string_literal: true

require 'rspec'
require 'open3'
require 'socket'
require 'timeout'
require 'fileutils'
require 'yaml'
require 'active_record'
require 'support/factory_bot'
require 'net/http'

require_relative '../system/system_spec_helper'

SUBJECT_AMOUNT = 1000
ADDITIONAL_TFS = 50
SATELLITE_AMOUNT = 50 # has to be a divisor of SUBJECT_AMOUNT/2

def build_jobs_tf(recipe)
  tf = TrackFilter.new
  tf.course_of_action = 'job completed'
  tf.extraction_pattern = [/(?<timestamp>.[\d\+ :-]+).*(?<userid>(?<=user: )\w+).* status: completed, action: copy, file_path:.*/]
  tf.pattern = /.* status: completed, action: copy, file_path:.*/
  tf.recipe_id = recipe.id
  tf.score = 42
  tf
end

def build_ignored_tf(recipe, idx)
  tf = TrackFilter.new
  tf.course_of_action = "this should not be tracked #{idx}"
  tf.extraction_pattern = [/\[(?<timestamp>[0-9]+)\] #{idx} nothing has been cl1cked by (?<subject>.+)$/]
  tf.pattern = /this is never matched #{idx}/
  tf.recipe_id = recipe.id
  tf.score = 666
  tf
end

def write_satellite_trace(start_idx, end_idx, satellite_id)
  trace = +''
  (start_idx..end_idx).each do |i|
    trace << "[#{Time.now.to_i}] satellite data from alpha.csv/#{i}\n"
  end
  @cmd.run_and_wait("echo \"#{trace}\" | sudo tee -a /tmp/artifactomat/satellite_trace_#{satellite_id}")
end

def write_link_trace(start_idx, end_idx, satellite_id)
  trace = +''
  (start_idx..end_idx).each do |i|
    trace << "[#{Time.now.to_i}] link clicked by alpha.csv/#{i}\n"
  end
  @cmd.run_and_wait("echo \"#{trace}\" | sudo tee -a /tmp/artifactomat/satellite_trace_#{satellite_id}")
end

describe "TRACE PROCESSING STRESS TEST – #{SUBJECT_AMOUNT} subjects", system: true, order: :defined do
  before(:all) do
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']
    ActiveRecord::Base.establish_connection(db_config)

    @logger = SpecLogger.new('trace_processing_stress_spec')
    @ape = Command.new
    @cmd = Command.new
    @aped = SystemdService.new('artifactomat-aped.service')
    @subject_tracker = SystemdService.new('artifactomat-subject-tracker.service')
    @track_collector = SystemdService.new('artifactomat-track-collector.service')
    @redis = SystemdService.new('redis-server@artifactomat.service')
    @test_scheduler = SystemdService.new('artifactomat-ape-scheduler.service')
    @satellite = Command.new
    @routing = SystemdService.new('artifactomat-routing.service')
    @monitoring = SystemdService.new('artifactomat-ape-monitoring.service')

    @cert_path = '/etc/artifactomat/certs'

    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.ip_forward=1'
    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.conf.all.route_localnet=1'

    @cmd.run_and_wait 'sudo artifactomat_setup db'
    @cmd.run_and_wait 'sudo artifactomat_setup app --yes', 120

    @duration = 10

    ie = build(:infrastructure_element, generate_script: 'exit 0',
                                        monitor_script: 'exit 0',
                                        destroy_script: 'exit 0',
                                        ports: [443])
    recipe = build(:recipe, infrastructure: [ie],
                            deploy_script: 'exit 0',
                            artifact_script: 'exit 0',
                            arm_script: 'touch /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERNAME-minimal_1',
                            disarm_script: 'rm /tmp/artifactomat/$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID-$ARTIFACTOMAT_SUBJECT_USERNAME-minimal_1',
                            parameters: { 'parameter' => '21' },
                            duration: @duration)
    recipe.trackfilters.push(build_link_tf(recipe))
    recipe.trackfilters.push(build_satellite_tf(recipe))
    ADDITIONAL_TFS.times do |i|
      recipe.trackfilters.push(build_ignored_tf(recipe, i))
    end

    write_recipe('long.yml', recipe)

    subjects_alpha = +"id,name,surname,userid,email\n"
    (1..SUBJECT_AMOUNT).each do |i|
      subjects_alpha << "#{i},#{i},#{i},sub#{i},#{i}@1&1.de\n"
    end

    write_subjects('alpha.csv', subjects_alpha)
    set_log_lvl_debug
  end

  def kill_track_collector
    @track_collector.kill
    @logger.log(@track_collector.status)
  end

  def kill_subject_tracker
    @subject_tracker.kill
    @logger.log(@subject_tracker.status)
  end

  def kill_aped
    @aped.kill
    @logger.log(@aped.status)
  end

  def kill_monitoring
    @monitoring.kill
    @logger.log(@monitoring.status)
  end

  def kill_scheduler
    @test_scheduler.kill
    @logger.log(@test_scheduler.status)
  end

  after(:each) do |example|
    if example.exception
      kill_aped
      kill_subject_tracker
      kill_track_collector
      kill_scheduler
      kill_monitoring
    end
  end

  it 'starting aped' do
    @redis.start
    @subject_tracker.start
    @track_collector.start
    @routing.start
    @test_scheduler.start
    @monitoring.start
    @aped.start
    sleep 6
    @aped.escalate_error
    abort("\e[31m\nCAUTION!!\e[0m:\n#{@redis.status}") unless @redis.active?

    expect(@aped.active?).to be_truthy, 'Problem with starting aped!'
    expect(@routing.active?).to be_truthy
    expect(@monitoring.active?).to be_truthy
    expect(@subject_tracker.active?).to be_truthy
    expect(@track_collector.active?).to be_truthy
    expect(@test_scheduler.active?).to be_truthy
  end

  describe 'Trace processing' do
    before(:all) do
      expected_program_count = TestProgram.count + 1
      expected_series_count = TestSeries.count + 1
      expected_season_count = TestSeason.count + 1

      # create a program and series + season for the subjects
      @ape.run_and_wait 'ape program new -l "TraceTestProgram"'
      @ape.run_and_wait "ape series new -g 'alpha.csv' -s 'in 40 sec' -e 'in 640 sec' -l 'TraceTestSeries' #{TestProgram.last.id}"
      @ape.run_and_wait "ape season new -r 'long' -d 10m -l 'TraceTestSeason' #{TestSeries.last.id}"
      series_start = Time.now + 40
      expect(TestProgram.count).to eq(expected_program_count)
      expect(TestSeries.count).to eq(expected_series_count)
      expect(TestSeason.count).to eq(expected_season_count)
      @ape.run_and_wait "ape series schedule #{TestSeries.last.id}"
      @ape.run_and_wait "ape series prepare #{TestSeries.last.id}"
      output = ''
      Timeout.timeout(60) do
        until output.include? 'fully prepared'
          @ape.run_and_wait 'ape info'
          output = @ape.stdout.join("\n")
        end
      end
      @ape.run_and_wait "ape series approve #{TestSeries.last.id}"
      sleep_until series_start
      sleep 10 # wait for all episodes to be armed
    end

    it 'records all satellite traces' do
      satellites = []
      SATELLITE_AMOUNT.times do |i|
        @cmd.run_and_wait "sudo touch /tmp/artifactomat/satellite_trace_#{i}"
        sat = Command.new
        sat.run "ruby /etc/artifactomat/satellite.rb -f /tmp/artifactomat/satellite_trace_#{i} --host 127.0.0.1 --port 5555 --ca #{@cert_path}/public/ca.pem --pkey #{@cert_path}/private/client.itsape.key --cert #{@cert_path}/public/client.itsape.pem"
        satellites.push(sat)
      end
      sleep 3
      satellites.each do |sat|
        expect(sat.running?).to be_truthy
        expect(sat.pid).to be
        expect(sat.exit_code).to be_nil
      end

      target_te_count = TrackEntry.count + SUBJECT_AMOUNT

      sat_traces = (SUBJECT_AMOUNT / 2) / SATELLITE_AMOUNT
      SATELLITE_AMOUNT.times do |i|
        # sub0 - sub500 get satellite traces
        write_satellite_trace(i * sat_traces + 1, (i + 1) * sat_traces, i)
        # sub501 - sub1000 get link traces
        write_link_trace(i * sat_traces + 1 + (SUBJECT_AMOUNT / 2),
                         (i + 1) * sat_traces + (SUBJECT_AMOUNT / 2),
                         i)
      end
      breakout = 10
      sleep 5 while TrackEntry.count < target_te_count && (breakout -= 1).positive?

      expected_subject_ids = []
      (1..SUBJECT_AMOUNT).each do |i|
        expected_subject_ids << "alpha.csv/#{i}"
      end
      track_entries = TrackEntry.last(SUBJECT_AMOUNT)
      subject_ids = track_entries.map(&:subject_id)
      not_recorded = expected_subject_ids - subject_ids

      expect(not_recorded).to be_empty
    end

    it 'links the trace to the correct subject' do
      tes = TrackEntry.last(SUBJECT_AMOUNT)
      tes.each do |te|
        sid = te.subject_id.split('/')[1].to_i
        coa = te.course_of_action
        if sid <= (SUBJECT_AMOUNT / 2)
          expect(coa).to eq('satellite trace')
        else
          expect(coa).to eq('link clicked')
        end
      end
    end
  end
end
