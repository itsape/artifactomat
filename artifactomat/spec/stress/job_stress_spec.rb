# frozen_string_literal: true

require 'rspec'
require 'open3'
require 'socket'
require 'timeout'
require 'fileutils'
require 'yaml'
require 'active_record'
require 'support/factory_bot'
require 'net/http'

require_relative '../system/system_spec_helper'

SUBJECT_AMOUNT = 1000

# Class to start a new process from a pool of Command instances
class CommandPool
  def initialize(size)
    @size = size
    @workers = []
    @size.times do
      @workers << Command.new
    end
  end

  def exec(command)
    @workers[find_empty_worker].run command
  end

  def running?
    @workers.each do |worker|
      return false if worker.running?
    end
  end

  def wait_for_finish
    @workers.each do |cmd|
      sleep 0.1 while cmd.running?
    end
  end

  private

  def find_empty_worker
    idx = rand(@size)
    while @workers[idx].running?
      idx = rand(@size)
      sleep 0.01
    end
    idx
  end
end

def send_get(path)
  uri = URI::HTTPS.build(host: '127.0.0.1', port: 8443, path: path)
  request = Net::HTTP::Get.new(uri)
  make_https(request)
end

def send_post(path, data)
  uri = URI::HTTPS.build(host: '127.0.0.1', port: 8443, path: path)
  request = Net::HTTP::Post.new(uri)
  make_https(request, data)
end

describe "JOB STRESS TEST – #{SUBJECT_AMOUNT} subjects", system: true, order: :defined do
  before(:all) do
    conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'
    db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'production']
    ActiveRecord::Base.establish_connection(db_config)

    @logger = SpecLogger.new('job_stress_spec')
    @ape = Command.new
    @cmd = Command.new
    @aped = SystemdService.new('artifactomat-aped.service')
    @apenames = SystemdService.new('artifactomat-apenames.service')
    @jobspooler = SystemdService.new('artifactomat-ape-job-spooler.service')
    @jobclerk = SystemdService.new('artifactomat-ape-job-clerk.service')
    @redis = SystemdService.new('redis-server@artifactomat.service')
    @routing = SystemdService.new('artifactomat-routing.service')
    @test_scheduler = SystemdService.new('artifactomat-ape-scheduler.service')
    @monitoring = SystemdService.new('artifactomat-ape-monitoring.service')

    @cert_path = '/etc/artifactomat/certs'

    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.ip_forward=1'
    @cmd.run_and_wait 'sudo sysctl -w net.ipv4.conf.all.route_localnet=1'

    @cmd.run_and_wait 'sudo artifactomat_setup db'
    @cmd.run_and_wait 'sudo artifactomat_setup app --yes', 120

    subjects_alpha = +"id,name,surname,userid,email\n"
    (1..SUBJECT_AMOUNT).each do |i|
      subjects_alpha << "#{i},#{i},#{i},sub#{i},#{i}@1&1.de\n"
    end

    write_subjects('alpha.csv', subjects_alpha)
    set_log_lvl_debug
  end

  after(:all) do
    @aped.kill
  end

  after(:each) do |example|
    if example.exception
      @aped.kill
      @logger.log(@aped.status)
    end
  end

  it 'starting aped' do
    @redis.start
    @apenames.start
    @routing.start
    @test_scheduler.start
    @monitoring.start
    pid = @aped.start
    expect(pid).not_to eq(0)
    sleep 6
    @aped.escalate_error
    abort("\e[31m\nCAUTION!!\e[0m:\n#{@redis.status}") unless @redis.active?

    expect(@aped.active?).to be_truthy, 'Problem with starting aped!'
    expect(@apenames.active?).to be_truthy
    expect(@routing.active?).to be_truthy
    expect(@test_scheduler.active?).to be_truthy
    expect(@monitoring.active?).to be_truthy
  end

  describe 'jobs' do
    before(:all) do
      @cmd_pool = CommandPool.new(4)
      @job_ids = {}

      TrackEntry.destroy_all
      @jobspooler.start
      @jobclerk.start
      sleep 5
    end

    after(:all) do
      TrackEntry.destroy_all
      @jobspooler.stop
      @logger.log(@jobspooler.status)
    end

    it 'job-clerk runs' do
      expect(tcp_port_open?('127.0.0.1', 51_648)).to be_truthy
      expect(@jobclerk.active?).to be_truthy
    end

    it 'job-spooler runs' do
      expect(@jobspooler.active?).to be_truthy
      expect(tcp_port_open?('127.0.0.1', 8443)).to be_truthy
    end

    it 'accepts jobs' do
      # create a file
      file_location = '/tmp/itsapetestfile.txt'
      File.open(file_location, mode: 'w') do |file|
        file.write('a string in a textfile')
      end

      (1..SUBJECT_AMOUNT).each do |i|
        @cmd_pool.exec "ape-job --host=localhost --port=51648 build run sub#{i} /tmp/itsapetestfile.txt"
        @cmd_pool.exec "ape-job --host=localhost --port=51648 build copy sub#{i} /tmp/itsapetestfile.txt '%USER_PROFILE\\DESKTOP'"
      end
      sleep 5
    end

    it 'shows jobs in joblist' do
      (1..SUBJECT_AMOUNT).each do |i|
        resp = send_get "/jobs/sub#{i}"
        expect(resp.code.to_i).to eq 200
        jobs = JSON.parse(resp.body)
        expect(jobs.size).to eq(2)
      end
    end

    it 'receives correct details for job queries' do
      (1..SUBJECT_AMOUNT).each do |i|
        # get job ids
        resp = send_get "/jobs/sub#{i}"
        job_list = JSON.parse(resp.body)
        ids = job_list.map { |x| x.split('/')[-1].to_i }
        @job_ids[i] = ids # save job IDs for later tests
        jobs = []
        ids.each do |id|
          resp = send_get "/jobs/sub#{i}/#{id}"
          expect(resp.code.to_i).to eq 200
          job = JSON.parse(resp.body)
          job.delete('collection_date')
          job.delete('creation_date')
          job.delete('expiration_date')
          job.delete('id')
          jobs << job
        end

        job1 = {
          'user' => "sub#{i}",
          'status' => 'waiting',
          'action' => 'execute',
          'file_path' => '/tmp/itsapetestfile.txt',
          'deploy_path' => '',
          'watch_arguments' => nil
        }
        job2 = {
          'user' => "sub#{i}",
          'status' => 'waiting',
          'action' => 'copy',
          'file_path' => '/tmp/itsapetestfile.txt',
          'deploy_path' => '%USER_PROFILE\\DESKTOP',
          'watch_arguments' => nil
        }
        expect(jobs).to include(job1)
        expect(jobs).to include(job2)
      end
    end

    it "properly updates the job status of #{SUBJECT_AMOUNT} jobs" do
      update = { 'status' => 'completed' }
      (1..SUBJECT_AMOUNT).each do |i|
        # update status of one job to completed
        resp = send_post "/jobs/sub#{i}/#{@job_ids[i][0]}", update
        expect(resp.code.to_i).to eq(204)

        # query job to make sure it was updated
        resp = send_get "/jobs/sub#{i}/#{@job_ids[i][0]}"
        expect(resp.code.to_i).to eq(200)
        job = JSON.parse(resp.body)
        expect(job['status']).to eq('completed')

        # query second job to make sure it was not updated
        resp = send_get "/jobs/sub#{i}/#{@job_ids[i][1]}"
        expect(resp.code.to_i).to eq(200)
        job = JSON.parse(resp.body)
        expect(job['status']).to eq('waiting')
      end
    end
  end
end
