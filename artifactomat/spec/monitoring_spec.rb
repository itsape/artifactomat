# frozen_string_literal: true

require 'artifactomat/modules/monitoring'
require 'artifactomat/modules/configuration_manager'

require_relative 'mixins/script_executor_spec'

describe Monitoring do
  Logging.initialize

  TEST_DIR = '/tmp/artifactomat-monitoring-test'
  TEMP_DIR = TEST_DIR + '/tmp'

  def build_ie_with_season
    ie = build(:infrastructure_element)
    season = build(:test_season)
    season.id = 420
    season.save!
    ie.season_id = season.id
    ie
  end

  before(:each) do
    unless RSpec.current_example.metadata[:skip_before]
      TestSeason.delete_all
      routing_client = double('RoutingClient')
      allow(RoutingClient).to receive(:new) { routing_client }
      @subject_manager = double('SubjectManager_monitor_spec')
      @recipe_manager = double('RecipeManager_monitor_spec')
      @configuration_manager = double('ConfigManager_monitor_spec')
      allow(@configuration_manager).to receive(:config_to_env) { {} }
      allow(@configuration_manager).to receive(:get).with(ScriptExecution::APE_TMP_DIR) { TEMP_DIR }
      allow(@configuration_manager).to receive(:cfg_path) { TEST_DIR }
      allow(@configuration_manager).to receive(:get).with('conf.monitoring.polling_rate') { 10 }

      @delivery_manager = double('del_mgr_monitoring_spec')

      @ie = build(:infrastructure_element)

      @recipe = build(:recipe, infrastructure: [@ie])
      @season = create(:test_season, label: 'hello', recipe_id: @recipe.id)
      @season.id = 42
      @season.save!
      @ie.season_id = @season.id
      @ie.monitor_script = 'exit 0'

      allow(@recipe_manager).to receive(:get_recipe).with(@recipe.id) { @recipe }

      @monitoring = Monitoring.new(@configuration_manager,
                                   @recipe_manager,
                                   @subject_manager,
                                   @delivery_manager)
    end
  end

  it_behaves_like 'a script executor' do
    let(:executor) do
      @monitoring
    end
  end

  describe '#initialize' do
    it 'loads its polling rate' do
      @configuration_manager = double('ConfigManager')
      allow(@configuration_manager).to receive(:get).with('conf.monitoring.polling_rate') { 42 }
      @monitoring = Monitoring.new(@configuration_manager,
                                   @recipe_manager,
                                   @subject_manager,
                                   @delivery_manager)
      expect(@monitoring.polling_rate).to eq(42)
    end

    it 'uses a default polling_rate if conf.monitoring.polling_rate ' \
       'is not available' do
      @configuration_manager = double('ConfigManager')
      allow(@configuration_manager)
        .to receive(:get)
          .with('conf.monitoring.polling_rate') { raise(ConfigError) }
      @monitoring = Monitoring.new(@configuration_manager,
                                   @recipe_manager,
                                   @subject_manager,
                                   @delivery_manager)
      expect(@monitoring.polling_rate).to eq(10)
    end

    describe 'state recreation' do
      before(:each) do
        TestSeries.delete_all
        TestSeason.delete_all
        @series = []
        @series_info = {}
        (0..4).each do |i|
          ie = build(:infrastructure_element)
          ie2 = build(:infrastructure_element)
          recipe = build(:recipe, infrastructure: [ie, ie2])
          season = create(:test_season, label: "Normal #{i}", recipe_id: recipe.id)
          season.id = 43 + i
          season.save!
          ie.season_id = season.id
          ie2.season_id = season.id
          allow(@recipe_manager).to receive(:get_recipe).with(recipe.id) { recipe }
          @series << season.test_series
          @series_info[season.test_series.id] = [{ ies: Set[ie.to_yaml, ie2.to_yaml], locked: false }]
        end
        @series[0].fresh!
        @series[1].scheduled!
        @series[2].prepared!
        @series[3].approved!
        @series[4].completed!
        (2..3).each do |i|
          # the prepared and approved series are the ones that should actually get recreated, so we
          # create some additional testcases only for these series
          locked_ie = build(:infrastructure_element)
          unlocked_ie = build(:infrastructure_element)
          locked_recipe = build(:recipe, infrastructure: [locked_ie])
          unlocked_recipe = build(:recipe, infrastructure: [unlocked_ie])
          locked_season = create(:test_season, label: "Locked #{i}",
                                               recipe_id: locked_recipe.id,
                                               test_series: @series[i])
          locked_season.locked = true
          locked_season.id = 1 + i
          locked_season.save!
          unlocked_season = create(:test_season, label: "Unlocked #{i}",
                                                 recipe_id: unlocked_recipe.id,
                                                 test_series: @series[i])
          unlocked_season.id = 100 + i
          unlocked_season.save!
          locked_ie.season_id = locked_season.id
          unlocked_ie.season_id = unlocked_season.id
          allow(@recipe_manager).to receive(:get_recipe).with(unlocked_recipe.id) { unlocked_recipe }
          allow(@recipe_manager).to receive(:get_recipe).with(locked_recipe.id) { locked_recipe }
          @series_info[@series[i].id] << { ies: Set[locked_ie.to_yaml], locked: true }
          @series_info[@series[i].id] << { ies: Set[unlocked_ie.to_yaml], locked: false }
        end

        allow(Logging).to receive(:info).with('Monitoring', /Subscribed IE/)
      end

      it 'does not start monitoring IEs of series that are not prepared or approved' do
        do_not_monitor_ies = Set.new
        # fresh, scheduled and completed series
        [0, 1, 4].each do |i|
          @series_info[@series[i].id].each do |season_info|
            do_not_monitor_ies.merge(season_info[:ies])
          end
        end

        mon = Monitoring.new(@configuration_manager,
                             @recipe_manager,
                             @subject_manager,
                             @delivery_manager)

        subscribed_elements = mon.instance_variable_get(:@subscribed_elements).keys.to_set
        expect(subscribed_elements.disjoint?(do_not_monitor_ies)).to be(true)
      end

      it 'does not start monitoring IEs of locked seasons' do
        do_not_monitor_ies = Set.new
        (0..4).each do |i|
          @series_info[@series[i].id].each do |season_info|
            next unless season_info[:locked]

            do_not_monitor_ies.merge(season_info[:ies])
          end
        end

        mon = Monitoring.new(@configuration_manager,
                             @recipe_manager,
                             @subject_manager,
                             @delivery_manager)

        subscribed_elements = mon.instance_variable_get(:@subscribed_elements).keys.to_set
        expect(subscribed_elements.disjoint?(do_not_monitor_ies)).to be(true)
      end

      it 'starts monitoring all IEs of unlocked season of prepared or approved series' do
        do_monitor_ies = Set.new
        (2..3).each do |i|
          @series_info[@series[i].id].each do |season_info|
            next if season_info[:locked]

            do_monitor_ies.merge(season_info[:ies])
          end
        end

        mon = Monitoring.new(@configuration_manager,
                             @recipe_manager,
                             @subject_manager,
                             @delivery_manager)

        subscribed_elements = mon.instance_variable_get(:@subscribed_elements).keys.to_set
        expect(subscribed_elements).to eq(do_monitor_ies)
      end
    end
  end

  describe '#status' do
    it 'returns a hash' do
      expect(@monitoring.status).to be_a(Hash)
    end

    it 'shows thread running' do
      expect(Logging).to receive(:debug).twice
      expect(Logging).to receive(:info).twice
      expect(@monitoring.status['Running?']).to match('false')
      @monitoring.run
      expect(@monitoring.status['Running?']).to match('true')
      @monitoring.stop
      expect(@monitoring.status['Running?']).to match('false')
    end

    it 'returns some information on infrastructure elements' do
      expect(@monitoring.status['Infrastructure']).to be_a(Hash)
      ie = build(:infrastructure_element)
      season = build(:test_season)
      season.id = 360
      season.save!
      ie.season_id = season.id
      state = MonitoringState.new 0
      elements = { ie.to_yaml => { state: state, ie: ie } }
      @monitoring.instance_variable_set(:@subscribed_elements, elements)
      expect(Logging).to receive(:info).once
      @monitoring.subscribe(ie)
      expect(@monitoring.status['Infrastructure']['total']).to match(1)
      expect(@monitoring.status['Infrastructure']['unhealthy']).to match(0)
      expect(@monitoring.status['Infrastructure']['seasons']).to match([season.to_str])
    end
  end

  describe '#get_status' do
    it 'returns the status code for a given IE' do
      expect(Logging).to receive(:info).once
      @monitoring.subscribe(@ie)
      expect(@monitoring.get_status(@ie).to_i).to eq(-1)
    end

    it 'fails for not subscribed IE' do
      expect { @monitoring.get_status(@ie) }
        .to raise_error(MonitorError, match('InfrastructureElement .* is unknown.'))
    end
  end

  describe '#infrastructure_running?' do
    it 'returns false if infrastructure element is not monitored' do
      ie = build(:infrastructure_element)
      expect(@monitoring.infrastructure_running?(ie)).to be false
    end

    it 'returns true on good infrastructure element status' do
      ie = build(:infrastructure_element)
      state = MonitoringState.new 0
      elements = { ie.to_yaml => { ie: ie, state: state } }
      @monitoring.instance_variable_set(:@subscribed_elements, elements)
      expect(@monitoring.infrastructure_running?(ie)).to be true
    end
  end

  describe '#subscribe' do
    it 'newly subscribed elements have an unknown state' do
      ie = build_ie_with_season
      expect(Logging).to receive(:info).once
      @monitoring.subscribe(ie)
      expect(@monitoring.get_status(ie).good?).to be false
    end

    it 'does not change state on subscribing twice' do
      ie = build_ie_with_season
      state = MonitoringState.new 0
      elements = { ie.to_yaml => { ie: ie, state: state } }
      @monitoring.instance_variable_set(:@subscribed_elements, elements)
      expect(Logging).to receive(:info).once
      @monitoring.subscribe(ie)
      expect(@monitoring.get_status(ie).good?).to be true
    end

    it 'does not allow subscribing elements without a season' do
      ie = build(:infrastructure_element)
      expect { @monitoring.subscribe(ie) }.to raise_error(MonitorError)
    end
  end

  describe '#unsubscribe' do
    it 'does not fail on unknown elements' do
      expect { @monitoring.get_status(@ie) }
        .to raise_error(MonitorError, match('InfrastructureElement .* is unknown.'))
      expect(Logging).to receive(:info).once
      @monitoring.unsubscribe(@ie)
      expect { @monitoring.get_status(@ie) }
        .to raise_error(MonitorError, match('InfrastructureElement .* is unknown.'))
    end

    it 'reverses #subscribe' do
      expect(Logging).to receive(:info).twice
      @monitoring.subscribe(@ie)
      expect(@monitoring.get_status(@ie).good?).to be false
      @monitoring.unsubscribe(@ie)
      expect { @monitoring.get_status(@ie) }
        .to raise_error(MonitorError, match('InfrastructureElement .* is unknown.'))
    end
  end

  context 'the Monitoring Thread' do
    describe '#running?, #run, #stop' do
      it 'does not run by default' do
        expect(@monitoring.running?).to be false
      end

      it 'reports running if started' do
        expect(Thread).to receive(:new).and_return(ThreadMock.new)
        expect(Logging).to receive(:debug).once
        expect(Logging).to receive(:info).once
        @monitoring.run
        expect(@monitoring.running?).to be true
      end

      it 'reports not running if stopped' do
        expect(Thread).to receive(:new).and_return(ThreadMock.new)
        expect(Logging).to receive(:debug).once
        expect(Logging).to receive(:info).once
        @monitoring.run
        expect(@monitoring.running?).to be true
        expect(Logging).to receive(:debug).once
        expect(Logging).to receive(:info).once
        @monitoring.stop
        expect(@monitoring.running?).to be false
      end
    end

    describe '.run_loop' do
      before(:each) do
        allow(TestSeason).to receive(:find).with(@season.id).and_return(@season)
      end

      it 'updates the status (good) and unlocks' do
        expect(Logging).to receive(:debug).thrice
        expect(Logging).to receive(:info).thrice
        # expect(@monitoring).to receive(:sleep).with(10)
        expect(@monitoring).to receive(:season_to_env).and_return({})

        expect(@delivery_manager).to receive(:unlock_season).with(@ie.season)
        expect(Logging).to receive(:info).once
        @monitoring.subscribe(@ie)
        @monitoring.run
        sleep(0.3)
        @monitoring.stop
        expect(@monitoring.get_status(@ie).good?).to be true
      end

      it 'updates the status (bad) and locks' do
        expect(Logging).to receive(:debug).thrice
        expect(Logging).to receive(:info).twice
        # expect(@monitoring).to receive(:sleep).with(10)
        expect(@monitoring).to receive(:season_to_env).and_return({})
        @ie.monitor_script = 'exit 1'

        expect(Logging).to receive(:error).once
        expect(@delivery_manager).to receive(:lock_season).with(@ie.season)
        expect(Logging).to receive(:info).once
        @monitoring.subscribe(@ie)
        @monitoring.run
        sleep(0.3)
        @monitoring.stop
        expect(@monitoring.get_status(@ie).good?).to be false
      end

      it 'timeout results in bad status' do
        @monitoring.instance_variable_set(:@polling_rate, 1)
        @ie.monitor_script = 'sleep 3'
        expect(Logging).to receive(:debug).thrice
        expect(Logging).to receive(:info).thrice
        # expect(@monitoring).to receive(:sleep).with(10)
        expect(@monitoring).to receive(:season_to_env).and_return({})
        # expect(@monitoring).to receive(:execute_script).with(@ie.monitor_script, {}) do
        #   sleep(3)
        # end

        expect(Logging).to receive(:error).once
        expect(@delivery_manager).to receive(:lock_season).with(@ie.season)
        @monitoring.subscribe(@ie)
        @monitoring.run
        sleep(0.5)
        @monitoring.stop
        sleep(2)
        expect(@monitoring.get_status(@ie).good?).to be false
        expect(@monitoring.get_status(@ie).error).to match('Timeout')
      end

      it 'does not update state on unsubscribed elements' do
        allow(@monitoring).to receive(:season_to_env).and_return({})
        @monitoring.instance_variable_set(:@polling_rate, 2)
        @ie.monitor_script = 'sleep 3'
        expect(Logging).to receive(:info).at_least(4).times
        expect(Logging).to receive(:debug).at_least(2).times
        expect(@monitoring).not_to receive(:manage_states)
        @monitoring.subscribe(@ie)
        @monitoring.run
        sleep 1
        @monitoring.unsubscribe(@ie)
        sleep 3
        @monitoring.stop
        expect(@monitoring.instance_variable_get(:@subscribed_elements)).not_to include(@ie.to_yaml)
      end
    end
  end

  describe '#to_str' do
    it 'returns pretty yaml' do
      expect(Logging).to receive(:info).once
      @monitoring.subscribe(@ie)
      status = YAML.safe_load @monitoring.to_str
      expect(status.keys.first).to include(@ie.season.label)
      expect(status.values.first).to include('unknown')
    end
  end
end
