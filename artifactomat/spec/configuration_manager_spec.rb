# frozen_string_literal: true

require 'artifactomat/modules/configuration_manager'
require 'artifactomat/modules/logging'

require 'fakefs/spec_helpers'
require 'fileutils'
require 'yaml'

describe ConfigurationManager do
  include FakeFS::SpecHelpers

  def write_test_config(cfg)
    File.open(File.join(@conf_path, 'conf.d', 'test.yml'), 'w') do |f|
      YAML.dump(cfg, f)
    end
    @cm = ConfigurationManager.new
  end

  def write_infrastructure(location)
    file_location = File.join(location, 'conf.d', 'infrastructure.yml')
    infrastructure = { 'webserver' => { 'ip' => '127.0.0.1', 'port' => '8081' }, 'mailserver' => { 'name' => 'Mailserver' } }
    File.open(file_location, mode: 'w') do |file|
      file.write(YAML.dump(infrastructure))
    end
  end

  before(:all) do
    FakeFS.activate!

    file = File.join(File.dirname(__FILE__), '..', 'dist', 'etc', 'artifactomat')
    @conf_path = File.expand_path(file)
  end

  before(:each) do
    FakeFS::FileSystem.clear
    FakeFS::FileSystem.clone(@conf_path)

    allow(Logging).to receive(:info)
    @cm = ConfigurationManager.new(@conf_path)
  end

  after(:all) do
    FakeFS.deactivate!
  end

  describe '::initialize' do
    context 'conf.yml & conf.d' do
      it 'loads from a custom path' do
        FileUtils.mkdir '/other'
        FakeFS::FileSystem.clone(@conf_path, '/other')
        result = ConfigurationManager.new('/other').get('conf.ape.ip')
        expect(result).to match('.')
      end

      it 'raises ConfigError on not readable config' do
        allow(File).to receive(:readable?).and_return(false)
        expected = expect do
          ConfigurationManager.new(@conf_path)
        end
        expected.to raise_error(ConfigError)
      end

      it 'raises error on config load from bogus path' do
        bogus = 'sdökljfaasökldfjsdklöj'
        expect { ConfigurationManager.new(bogus) }.to raise_error(ConfigError)
      end
    end
  end

  describe '#config_to_env' do
    it 'creates a hash for ENV from conf.yml' do
      env = @cm.config_to_env
      expect(env).to include('ARTIFACTOMAT_CONF_CONF_APE_TMP_DIR' => '/tmp/artifactomat',
                             'ARTIFACTOMAT_CONF_CONF_SUBJECTS_COL_SEP' => ',')
    end

    it 'creates a hash from conf.d configs' do
      write_infrastructure(@conf_path)
      expect { @cm = ConfigurationManager.new }.not_to raise_error
      env = @cm.config_to_env
      expect(env).to include('ARTIFACTOMAT_CONF_TRACKCOLLECTOR_IP' => '0.0.0.0',
                             'ARTIFACTOMAT_CONF_TRACKCOLLECTOR_PORT' => '5555',
                             'ARTIFACTOMAT_CONF_INFRASTRUCTURE_MAILSERVER_NAME' => 'Mailserver',
                             'ARTIFACTOMAT_CONF_INFRASTRUCTURE_WEBSERVER_IP' => '127.0.0.1',
                             'ARTIFACTOMAT_CONF_INFRASTRUCTURE_WEBSERVER_PORT' => '8081')
    end

    it 'handles arrays in config' do
      write_test_config('array' => ['one', { 'another' => 'two' }])
      env = @cm.config_to_env
      expect(env).to include('ARTIFACTOMAT_CONF_TEST_ARRAY_0' => 'one',
                             'ARTIFACTOMAT_CONF_TEST_ARRAY_1_ANOTHER' => 'two')
    end
  end

  describe '#get' do
    it 'succeeds' do
      cfgmgr = ConfigurationManager.new
      expect { cfgmgr.get('conf.ape.ip') }.not_to raise_error
      expect(cfgmgr.get('conf.ape.ip')).to match('localhost')
    end

    it '? wildcard on on value' do
      write_test_config('log_path' => '/test/logpath')
      result = @cm.get('?.log_path', %w[test logging])
      expect(result).to match('/test/logpath')
    end

    it '? wildcard on multiple hits' do
      hash = { 'some' => { 'fix' => '1' }, 'value' => { 'fix' => '2' } }
      write_test_config(hash)
      result = @cm.get('test.?.fix', %w[some value])
      expect(result).to match('test.some.fix' => '1', 'test.value.fix' => '2')
    end

    it 'supports * wildcard' do
      write_test_config('log_path' => '/test/logpath')
      result = @cm.get('*.log_path')
      expect(result).to match('/test/logpath')
    end

    it '* wildcard on multiple hits' do
      hash = { 'some' => { 'fix' => '1' }, 'value' => { 'fix' => '2' } }
      write_test_config(hash)
      result = @cm.get('test.*.fix')
      expect(result).to match('test.some.fix' => '1', 'test.value.fix' => '2')
    end

    it 'raises ConfigError on missing config values for key' do
      bogus = 'klasdfgj'
      expect do
        ConfigurationManager.new.get(bogus)
      end.to raise_error(ConfigError)
    end
  end

  describe '#absolution' do
    it 'contains the config path' do
      allow_any_instance_of(Pathname).to receive(:absolute?).and_return(false)
      dummy = 'dummy'
      expect(@cm.absolution(dummy)).to match("#{@cfg_path}/#{dummy}")
    end

    it 'parameter untouched if already absolute' do
      dummy = '/dummy'
      expect(@cm.absolution(dummy)).to match(dummy)
    end
  end
end
