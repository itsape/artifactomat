# frozen_string_literal: true

require 'artifactomat/models/subject'
require 'artifactomat/modules/subject_tracker/subject_tracker_utils'
require 'artifactomat/volatile_store.rb'

describe SubjectTrackerUtils do
  context '#resolve_subject_ids' do
    before(:each) do
      @subject_manager = double('SubjectManager')
      @configuration_manager = double('ConfigurationManager')
      @series = create(:test_series, group_file: 'subjects.csv', season_count: 0)
      @season = create(:test_season, test_series: @series, episode_count: 0)
      @series.test_seasons << @season
      @series.save

      @subject_list = []
      (1..5).each do |i|
        subject = Subject.new("subjects.csv/#{i}",
                              "Subject#{i}", "Surname#{i}", "User#{i}",
                              "user#{i}@example.com")
        subject.add_field('username')
        subject.add_field('same')
        subject.same = 'same'
        subject.username = "User#{i}"
        @subject_list << subject
        episode = create(:test_episode, test_season: @season, subject_id: subject.id)
        @season.test_episodes << episode
      end
      # create second pseudonym for User2 in a separate series
      @series2 = create(:test_series, group_file: 'subjects2.csv', season_count: 0)
      @season2 = create(:test_season, test_series: @series2, episode_count: 0)
      @series2.test_seasons << @season2
      @series2.save

      subject2 = Subject.new('subjects2.csv/1',
                             'Subject2', 'Surname2', 'User2',
                             'user2@example.com')
      subject2.add_field('username')
      subject2.add_field('same')
      subject2.username = 'User2'
      @subject_list2 = [subject2]
      episode = create(:test_episode, test_season: @season2, subject_id: subject2.id)
      @season2.test_episodes << episode

      @season2.save

      @vstore = VolatileStore.new(@configuration_manager)

      allow(@subject_manager).to receive(:get_subjects) do |gid|
        case gid
        when 'subjects.csv'
          @subject_list
        when 'subjects2.csv'
          @subject_list2
        else
          []
        end
      end
      allow(@configuration_manager).to receive(:get).with('conf.ape.redis_socket') do
        '/tmp/artifactomat-spec-routing'
      end
    end

    it 'returns subjects' do
      desc = { username: 'User2' }
      ids = SubjectTrackerUtils.resolve_subject_ids(@subject_manager,
                                                    @vstore,
                                                    desc,
                                                    @season)
      expect(ids).to eq(['subjects.csv/2'])
      desc = { 'username' => 'User2' }
      ids = SubjectTrackerUtils.resolve_subject_ids(@subject_manager,
                                                    @vstore,
                                                    desc,
                                                    @season)
      expect(ids).to eq(['subjects.csv/2'])
    end

    it 'returns subjects for the correct season' do
      desc = { 'username' => 'User2' }
      ids = SubjectTrackerUtils.resolve_subject_ids(@subject_manager,
                                                    @vstore,
                                                    desc,
                                                    @season)
      expect(ids).to eq(['subjects.csv/2'])

      ids = SubjectTrackerUtils.resolve_subject_ids(@subject_manager,
                                                    @vstore,
                                                    desc,
                                                    @season2)
      expect(ids).to eq(['subjects2.csv/1'])
    end

    it 'returns subjects from ip' do
      # add to short term memory
      action = Action.new('', '127.0.0.1', '')
      action.subject_id = 'subjects.csv/2'
      @vstore.update_ip_subject_map(action)
      desc = { 'ip' => '127.0.0.1' }
      ids = SubjectTrackerUtils.resolve_subject_ids(@subject_manager,
                                                    @vstore,
                                                    desc,
                                                    @season)
      expect(ids).to eq(['subjects.csv/2'])
    end

    it 'returns empty array if no subject is found' do
      desc = { username: 'User7' }
      ids = SubjectTrackerUtils.resolve_subject_ids(@subject_manager,
                                                    @vstore,
                                                    desc,
                                                    @season)
      expect(ids).to eq([])
      desc = { jibber: 'jabber' }
      ids = SubjectTrackerUtils.resolve_subject_ids(@subject_manager,
                                                    @vstore,
                                                    desc,
                                                    @season)
      expect(ids).to eq([])
    end

    it 'returns multiple subjects if they match the description' do
      desc = { same: 'same' }
      ids = SubjectTrackerUtils.resolve_subject_ids(@subject_manager,
                                                    @vstore,
                                                    desc,
                                                    @season)
      expect(ids.length).to eq(5)
    end
  end
end
