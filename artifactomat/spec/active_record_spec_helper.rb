# frozen_string_literal: true

require 'shoulda-matchers'
require 'active_record'
require 'yaml'

# RSpec.configure do |config|
#  config.around do |example|
#    ActiveRecord::Base.transaction do
#      example.run
#      raise ActiveRecord::Rollback
#    end
#  end
# end

RSpec::Expectations.configuration.warn_about_potential_false_positives = false

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :active_record
    with.library :active_model
  end
end

conf_file = ENV['ARTIFACTOMATDB'] || Dir.pwd + '/db/config.yml'

db_config = YAML.safe_load(File.open(conf_file))[ENV['RAILS_ENV'] || 'test']
ActiveRecord::Base.establish_connection(db_config)
ActiveRecord::Migration.suppress_messages do
  load(File.dirname(__FILE__) + '/../db/schema.rb')
end
