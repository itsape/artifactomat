# frozen_string_literal: true

require 'date'
require 'artifactomat/models'

FactoryBot.define do
  factory :detection_event do
    test_episode { create(:test_episode) }
    timestamp { DateTime.now }
    artifact_present { 0 }
  end
end
