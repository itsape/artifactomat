# frozen_string_literal: true

require 'artifactomat/models'

FactoryBot.define do
  factory :test_series do
    test_program { create(:test_program, series_count: 0) }
    sequence(:group_file) { |n| "GroupFile #{n}" }
    sequence(:label) { |n| "TestSeries #{n}" }
    start_date { (Time.now + (1.0 / 24)).round(0).utc }

    transient do
      season_count { 3 }
    end

    after(:create) do |series, evaluator|
      create_list(:test_season,
                  evaluator.season_count,
                  test_series: series)
    end
  end
end
