# frozen_string_literal: true

require 'artifactomat/models'
FactoryBot.define do
  factory :subject_history do
    test_series { create(:test_series) }
    sequence(:subject_id, &:to_s)
    sequence(:ip) { |n| "192.168.12.#{n}" }
    submit_time { Time.now }
    session_begin { Time.now }
  end
end
