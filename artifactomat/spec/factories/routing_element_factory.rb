# frozen_string_literal: true

require 'artifactomat/models'

FactoryBot.define do
  factory :routing_element do
    series_id { build(:test_series).id }
    season_id { build(:test_season).id }

    initialize_with { new(series_id, season_id) }
  end
end
