# frozen_string_literal: true

require 'artifactomat/models'

FactoryBot.define do
  factory :infrastructure_element do
    sequence(:infrastructure_type) { |n| "dummy#{n}" }
    transient do
      armed { false }
    end

    sequence(:monitor_script) { |n| "MonitorScript#{n}" }
    # sequence(:infrastructure_type) { |n| "Type#{n}" }
    sequence(:generate_script) { |n| "GenerateScript#{n}" }
    sequence(:destroy_script) { |n| "DestroyScript#{n}" }
    sequence(:arm_script) { |n| "ArmScript#{n}" }
    sequence(:disarm_script) { |n| "DisarmScript#{n}" }
    parameters { { 'ip' => '192.168.12.2', 'port' => 80 } }
    ports { [rand(2..65_535), rand(2..65_535)] }
    names { ['not.me', 'my.little.pony'] }

    after(:build) do |elem, evaluator|
      elem.arm if evaluator.armed
    end
  end
end
