# frozen_string_literal: true

require 'artifactomat/models'

FactoryBot.define do
  factory :track_filter do
    sequence(:recipe_id) { |n| "recipe#{n}" }
    sequence(:course_of_action) { |n| "CourseOfAction#{n}" }
    sequence(:score) { |n| n }
    extraction_pattern { [/\[(?<timestamp>[0-9]+)\] some action (?<subject>.+)/] }
    pattern { %r{/.*/} }

    after(:build) do |track_filter, evaluator|
      track_filter.course_of_action = evaluator.course_of_action
      track_filter.score = evaluator.score
      track_filter.recipe_id = evaluator.recipe_id
    end
  end
end
