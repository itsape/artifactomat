# frozen_string_literal: true

require 'artifactomat/models'

FactoryBot.define do
  factory :test_season do
    test_series { create(:test_series, season_count: 0) }
    sequence(:label) { |n| "TestSeason #{n}" }
    recipe_id { build(:recipe).id }
    recipe_parameters { {} }
    duration { 11 }
    screenshots_folder { '' }

    transient do
      episode_count { 3 }
    end

    after(:create) do |season, evaluator|
      create_list(:test_episode,
                  evaluator.episode_count,
                  test_season: season)
    end
  end
end
