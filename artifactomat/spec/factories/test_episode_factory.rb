# frozen_string_literal: true

require 'artifactomat/models'

FactoryBot.define do
  factory :test_episode do
    transient do
      scheduled { false }
    end
    subject_id { build(:subject).id }

    after(:create) do |episode, evaluator|
      create(:schedule, test_episode: episode) if evaluator.scheduled
    end
  end
end
