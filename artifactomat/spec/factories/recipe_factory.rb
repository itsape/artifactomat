# frozen_string_literal: true

require 'artifactomat/models'

# rubocop:disable Metrics/BlockLength
FactoryBot.define do
  factory :recipe do
    sequence(:id) { |n| "recipe#{n}" }
    artifact_script { "echo 'im an artifact'" }
    infrastructure { [] }
    # rubocop:disable Style/EmptyLiteral
    parameters { Hash.new }
    default_parameters { Hash.new }
    # rubocop:enable Style/EmptyLiteral

    duration { 10 }
    arm_script { 'exit 0' }
    disarm_script { 'exit 0' }
    deploy_script { 'exit 0' }
    undeploy_script { 'exit 0' }
    initialize_with do
      new(id,
          Recipe::RecipeScripts.new(artifact_script, arm_script, disarm_script, deploy_script, undeploy_script),
          infrastructure,
          parameters)
    end

    after(:build) do |recipe, evaluator|
      recipe.trackfilters = [build(:track_filter, recipe_id: evaluator.id)]
      recipe.detection = {
        'detection_interval' => 300,
        'detectors' => [{ 'detector_class_name' => 'TestDetector',
                          'pre_conditions' => '',
                          'target_conditions' => '' }],
        'runtime_information' => {},
        'error_window_size' => 3
      }
    end
  end
end
# rubocop:enable Metrics/BlockLength
