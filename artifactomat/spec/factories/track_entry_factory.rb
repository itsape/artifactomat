# frozen_string_literal: true

require 'artifactomat/models'

FactoryBot.define do
  factory :track_entry do
    test_episode
    subject_id { build(:subject).id }
    course_of_action { 'coa' }
    score { 0 }
    timestamp { 2.days.ago }

    initialize_with do
      new(test_episode: test_episode,
          subject_id: subject_id,
          course_of_action: course_of_action,
          timestamp: timestamp)
    end
  end
end
