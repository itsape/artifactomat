# frozen_string_literal: true

require 'artifactomat/models'

FactoryBot.define do
  factory :job do
    sequence(:id) { |n| n.to_s.to_i }
    sequence(:user) { |n| "user/#{n}" }

    status { 0 }
    action { 0 }

    file_path { '/some/file/path' }
    deploy_path { 'a/deploy/path' }
    watch_arguments { '{}' }
    creation_date { Time.now }
  end
end
