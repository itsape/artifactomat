# frozen_string_literal: true

require 'artifactomat/models'

FactoryBot.define do
  factory :subject do
    sequence(:id) { |n| "group.csv/#{n}" }
    sequence(:name) { |n| "Name #{n}" }
    sequence(:surname) { |n| "Surname #{n}" }
    sequence(:userid) { |n| "UserID #{n}" }
    sequence(:email) { |n| "subject#{n}@example.org" }

    initialize_with { new(id, name, surname, userid, email) }
  end
end
