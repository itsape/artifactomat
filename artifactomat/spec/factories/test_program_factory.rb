# frozen_string_literal: true

require 'artifactomat/models'

FactoryBot.define do
  factory :test_program do
    sequence(:label) { |n| "TestProgram #{n}" }

    transient do
      series_count { 3 }
    end

    after(:create) do |season, evaluator|
      create_list(:test_series,
                  evaluator.series_count,
                  test_program: season)
    end
  end
end
