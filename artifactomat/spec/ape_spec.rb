# frozen_string_literal: true

require 'rspec'
require 'artifactomat/modules/ape'

describe 'Ape' do
  before(:each) do
    banner = '                         -:__`
                  \Q@B&qXZoooZXq&Q@Q|
              ;QQdj\>rrrrrrrr>>>rr>\jdQQ;
      :zt]zvQ@U/rrrrrrrrrrrrrrrrrrrrrr>LU@D/z]Jv;
   W@RozcP@Qu*rrrrrrrrrrrrrrrrrrrrrrrrrrr*mQ@XLzSD@g
 `@O*>r\Q@U=rrr>/zFFti?>rrrrrrrr=iz]Fz/=rrr=9@H\rr|9@:
`@g>>r\QQu>r|oN@De}JyOQQD}*r>]qQQKa]}aW@Qm|>>u@Q\rr>D@
v@orr=Q@i>>e@N:         rQ@9QBL         `D@U>rS@Q=r>]@`
-@R>>m@e>>P@=             ;@\             F@K>re@er>O@
 _@U\Q@*r=@g     ,|=`              ^\:     K@|r*@Q?X@r
   QQ@B>>*@D    Q@@@@:           ;@@@@Q    m@\rrQ@Q@
    t@#r>>K@:   cQ@@&             E@@@\   r@Nr>rB@,
     @@>rr>k@&,                          u@g|r>>@@
     |@urr>r\a#@KF*                  ,cO@Dirrr>u@:
      QQ|rrrrrr=X@O      .. ..      ;@NJ=rrr>rvQQ
       @#Lrrrrr>D@;      `` ``       @Q?>rrrrL@@
        Q@}r>rrrQ@`  |//////////\;   Q@i>rrr]@H
         ;QLrrr>Q@   Q@`       ,@u   QQ\rrr=&:
           .>rr>N@,  `gN       Qk    @Rrrr;
              :re@o    "9QgDQgy`    t@zr:
                 ;BQZ|;         :/oNQ|
                      -;vuKKKF7r`'
    allow(Logging).to receive(:info).with('APE', "[userinfo] #{banner}")
    @configuration_manager = double('ConfigurationManager')
    allow(ConfigurationManager).to receive(:new).with(no_args).and_return(@configuration_manager)
    @recipe_manager = double('RecipeManager')
    allow(RecipeManager).to receive(:new).with(no_args).and_return(@recipe_manager)
    @subject_manager = double('SubjectManager')
    allow(SubjectManager).to receive(:new).with(no_args).and_return(@subject_manager)
    @delivery_manager = double('DeliveryManager')
    allow(DeliveryManager).to receive(:new).with(@configuration_manager,
                                                 @recipe_manager,
                                                 @subject_manager).and_return(@delivery_manager)
    @monitoring = double('Monitoring')
    allow(MonitoringRpcClient).to receive(:new).with(@configuration_manager,
                                                     @recipe_manager,
                                                     @subject_manager,
                                                     @delivery_manager).and_return(@monitoring)
    @subject_tracker_rpc_client = double('SubjectTrackerRpcClient')
    allow(SubjectTrackerRpcClient).to receive(:new).with(@configuration_manager,
                                                         @subject_manager).and_return(@subject_tracker_rpc_client)
    @deployment_manager = double('DeploymentManager')
    allow(DeploymentManager).to receive(:new).with(@monitoring,
                                                   @configuration_manager,
                                                   @recipe_manager,
                                                   @subject_manager,
                                                   @subject_tracker_rpc_client).and_return(@deployment_manager)
    @artifact_generator = double('ArtifactGenerator')
    allow(ArtifactGenerator).to receive(:new).with(@deployment_manager,
                                                   @configuration_manager,
                                                   @recipe_manager,
                                                   @subject_manager).and_return(@artifact_generator)
    @track_collector_rpc_client = double('TrackCollectorRpcClient')
    allow(TrackCollectorRpcClient).to receive(:new).with(@configuration_manager,
                                                         @recipe_manager,
                                                         @subject_manager).and_return(@track_collector_rpc_client)
    @test_scheduler = double('TestScheduler')
    allow(TestSchedulerRpcClient).to receive(:new).with(@configuration_manager,
                                                        @subject_manager,
                                                        @delivery_manager).and_return(@test_scheduler)
    @routing = double('RoutingClient')
    allow(RoutingClient).to receive(:new).with(@configuration_manager,
                                               @recipe_manager).and_return(@routing)
    @vstore = double('VolatileStore')
    allow(VolatileStore).to receive(:new).with(@configuration_manager).and_return(@vstore)
    allow(@vstore).to receive(:init_news)
    allow(@vstore).to receive(:push_news)
    allow(@vstore).to receive(:fetch_and_clear_news).and_return([])
    @infrastructure_generator = double('InfrastructureGenerator')
    allow(InfrastructureGenerator).to receive(:new).with(@configuration_manager,
                                                         @recipe_manager,
                                                         @subject_manager,
                                                         @deployment_manager,
                                                         @monitoring).and_return(@infrastructure_generator)
    @client_builder = double('ClientBuilder')
    allow(ClientBuilder).to receive(:new).with(@configuration_manager).and_return(@client_builder)

    allow(@configuration_manager).to receive(:get).with('conf.ape.log_level').and_return(4)
    allow(@configuration_manager).to receive(:get).with('conf.ape.log_colored').and_return(false)
    allow(@configuration_manager).to receive(:cfg_path).and_return('/etc/artifactomat')
    allow(@subject_manager).to receive(:blacklisted_subjects).and_return([])

    # work around the crazy singleton ...
    @ape = Ape.send(:new)
    @ape.init
  end

  describe 'initialization' do
    it 'removes leftovers of series in preparation' do
      series = build(:test_series, season_count: 0)
      series.preparing!
      expect(@artifact_generator).to receive(:destroy_artifacts).with(series)
      expect(@infrastructure_generator).to receive(:destroy).with(series)
      expect(@routing).to receive(:remove_routing).with(series)

      ape = Ape.send(:new)
      ape.init
      series.reload
      expect(series.scheduled?).to be(true)
    end

    it 'purges blacklisted subjects from the db' do
      blacklisted_subject = 'blacklist/1'
      keep_subject = 'subject/1'
      allow(@subject_manager).to receive(:blacklisted_subjects).and_return([blacklisted_subject])
      create(:subject_history, subject_id: blacklisted_subject)
      create(:subject_history, subject_id: keep_subject)
      create(:track_entry, subject_id: blacklisted_subject)
      create(:track_entry, subject_id: keep_subject)

      ape = Ape.send(:new)
      ape.init

      expect(SubjectHistory.where(subject_id: blacklisted_subject)).to be_empty
      expect(TrackEntry.where(subject_id: blacklisted_subject)).to be_empty
      expect(SubjectHistory.where(subject_id: keep_subject)).not_to be_empty
      expect(TrackEntry.where(subject_id: keep_subject)).not_to be_empty
    end

    it 'initializes the news db' do
      expect(@vstore).to receive(:init_news)
      ape = Ape.send(:new)
      ape.init
    end

    it 'adds the banner to the news db' do
      banner = '                         -:__`
                  \Q@B&qXZoooZXq&Q@Q|
              ;QQdj\>rrrrrrrr>>>rr>\jdQQ;
      :zt]zvQ@U/rrrrrrrrrrrrrrrrrrrrrr>LU@D/z]Jv;
   W@RozcP@Qu*rrrrrrrrrrrrrrrrrrrrrrrrrrr*mQ@XLzSD@g
 `@O*>r\Q@U=rrr>/zFFti?>rrrrrrrr=iz]Fz/=rrr=9@H\rr|9@:
`@g>>r\QQu>r|oN@De}JyOQQD}*r>]qQQKa]}aW@Qm|>>u@Q\rr>D@
v@orr=Q@i>>e@N:         rQ@9QBL         `D@U>rS@Q=r>]@`
-@R>>m@e>>P@=             ;@\             F@K>re@er>O@
 _@U\Q@*r=@g     ,|=`              ^\:     K@|r*@Q?X@r
   QQ@B>>*@D    Q@@@@:           ;@@@@Q    m@\rrQ@Q@
    t@#r>>K@:   cQ@@&             E@@@\   r@Nr>rB@,
     @@>rr>k@&,                          u@g|r>>@@
     |@urr>r\a#@KF*                  ,cO@Dirrr>u@:
      QQ|rrrrrr=X@O      .. ..      ;@NJ=rrr>rvQQ
       @#Lrrrrr>D@;      `` ``       @Q?>rrrrL@@
        Q@}r>rrrQ@`  |//////////\;   Q@i>rrr]@H
         ;QLrrr>Q@   Q@`       ,@u   QQ\rrr=&:
           .>rr>N@,  `gN       Qk    @Rrrr;
              :re@o    "9QgDQgy`    t@zr:
                 ;BQZ|;         :/oNQ|
                      -;vuKKKF7r`'
      expect(@vstore).to receive(:push_news).with(banner)
      ape = Ape.send(:new)
      ape.init
    end
  end

  describe 'bootstrapping framework' do
    it 'initializes ConfigurationManager correctly' do
      config = @ape.instance_variable_get(:@configuration_manager)
      expect(config).to be(@configuration_manager)
    end

    it 'initializes RecipeManager correctly' do
      rm = @ape.instance_variable_get(:@recipe_manager)
      expect(rm).to be(@recipe_manager)
    end

    it 'initializes SubjectManager correctly' do
      sm = @ape.instance_variable_get(:@subject_manager)
      expect(sm).to be(@subject_manager)
    end

    it 'initializes Monitoring correctly' do
      monitor = @ape.instance_variable_get(:@monitoring)
      expect(monitor).to be(@monitoring)
    end

    it 'initializes DeploymentManager correctly' do
      deploy = @ape.instance_variable_get(:@deployment_manager)
      expect(deploy).to be(@deployment_manager)
    end

    it 'initializes SubjectTracker correctly' do
      tracker = @ape.instance_variable_get(:@subject_tracker)
      expect(tracker).to be(@subject_tracker_rpc_client)
    end

    context 'TrackCollector' do
      it 'initializes TrackCollector correctly' do
        track = @ape.instance_variable_get(:@track_collector)
        expect(track).to be(@track_collector_rpc_client)
      end
    end

    it 'initializes DeliveryManager correctly' do
      delivery = @ape.instance_variable_get(:@delivery_manager)
      expect(delivery).to be(@delivery_manager)
    end

    it 'initializes ArtifactGenerator correctly' do
      artifact = @ape.instance_variable_get(:@artifact_generator)
      expect(artifact).to be(@artifact_generator)
    end

    it 'initializes TestScheduler correctly' do
      test_scheduler = @ape.instance_variable_get(:@test_scheduler)
      expect(test_scheduler).to be(@test_scheduler)
    end
  end

  describe 'has message queue' do
    it 'can store messages' do
      expect(@vstore).to receive(:push_news)
      expect(Logging).to receive(:info).with('APE', '[userinfo] hello world')
      expect { @ape.inform_user('hello world') }.not_to raise_error
    end

    it 'can fetch messages' do
      expect(@vstore).to receive(:fetch_and_clear_news).and_return(['hello world'])
      expect(Logging).to receive(:debug).with('APE', '[userinfo] all messages fetched.')
      expect(@ape.fetch_infos).to eq(['hello world'])
    end
  end

  describe 'can handle worker threads' do
    it 'can register worker threads' do
      dummy = double('thread')
      allow(dummy).to receive(:status).and_return('run')
      @ape.update_job(dummy, 'dummy worker')
      expect(@ape.jobs).to include('dummy worker' => 'run')
      expect(@ape.unregister_job(dummy)).to match('dummy worker')
    end

    it 'can gives the status on a finnished job only once' do
      dummy = double('thread')
      allow(dummy).to receive(:status).and_return(false)
      @ape.update_job(dummy, 'dummy worker')
      expect(@ape.jobs).to include('dummy worker' => 'Finished')
      expect(@ape.jobs).not_to include('dummy worker' => 'Finished')
    end
  end

  describe '#safe_reset_vstore' do
    it 'raises if there are preparing series' do
      ts = create(:test_series)
      ts.preparing!
      expect { @ape.safe_reset_vstore }.to raise_error(ApeError, /The following series are prepared or approved/)
      ts.destroy!
    end

    it 'raises if there are prepared series' do
      ts = create(:test_series)
      ts.prepared!
      expect { @ape.safe_reset_vstore }.to raise_error(ApeError, /The following series are prepared or approved/)
      ts.destroy!
    end

    it 'raises if there are approved series' do
      ts = create(:test_series)
      ts.approved!
      expect { @ape.safe_reset_vstore }.to raise_error(ApeError, /The following series are prepared or approved/)
      ts.destroy!
    end

    it 'resets if no prepared or running series exist' do
      ts = create(:test_series)
      ts.fresh!
      expect(@vstore).to receive(:reset)
      @ape.safe_reset_vstore
      ts.destroy!
    end
  end

  describe '#unschedule_jobs' do
    it 'asks all necessary modules to unschedule jobs' do
      expect(@routing).to receive(:unschedule_jobs).with(@series)
      expect(@infrastructure_generator).to receive(:unschedule_jobs).with(@series)
      expect(@artifact_generator).to receive(:unschedule_jobs).with(@series)
      expect(@test_scheduler).to receive(:unschedule_jobs).with(@series)
      @ape.unschedule_jobs(@series)
    end
  end

  describe '#reset_vstore' do
    it 'resets the vstore' do
      expect(@vstore).to receive(:reset)
      @ape.reset_vstore
    end
  end

  describe '#status' do
    before(:each) do
      allow(@track_collector_rpc_client).to receive(:status).and_return true
      allow(@test_scheduler).to receive(:status).and_return true
      allow(@monitoring).to receive(:status).and_return true
      allow(@subject_tracker_rpc_client).to receive(:status).and_return true
      allow(@delivery_manager).to receive(:status).and_return true
    end

    it 'tells things on background threads' do
      expect(@ape.status['APE']['Jobs']).to be_a(Array)
    end

    it 'tells things on the TrackCollector' do
      expect(@ape.status['TrackCollector']).to be
    end

    it 'tells things on the TestScheduler' do
      expect(@ape.status['TestScheduler']).to be
    end

    it 'tells things on the Monitoring' do
      expect(@ape.status['Monitoring']).to be
    end

    it 'tells things on the SubjectTracker' do
      expect(@ape.status['SubjectTracker']).to be
    end

    it 'tells things on the DeliveryManager' do
      expect(@ape.status['DeliveryManager']).to be
    end
  end

  describe 'manipulates data structures' do
    before(:all) do
      @program = build(:test_program, series_count: 0)
      @series = build(:test_series, season_count: 0)
      @season = build(:test_season, episode_count: 0)
      @subject = build(:subject)

      @series.test_seasons << @season
      @program.test_series << @series
    end

    it 'creates episodes' do
      allow(@subject_manager).to(
        receive(:get_subjects).and_return([@subject])
      )
      expect do
        @ape.create_episodes(@series)
      end.to change(TestEpisode, :count).by(1)
    end

    it 'deflates episodes' do
      allow(@subject_manager).to(
        receive(:get_subject).and_return(@subject)
      )
      episode = @season.test_episodes.first
      @episode = episode

      hash = episode.attributes
      hash['subject'] = @subject.id
      hash['seconds_exposed'] = 0
      @hash = hash
      flat =  @ape.deflate_episode(episode)
      expect(flat).to include(@hash)
    end

    it 'deflates seasons' do
      allow(@subject_manager).to(
        receive(:get_subject).and_return(@subject)
      )
      @season.reload
      hash = @season.attributes
      flat = @ape.deflate_season(@season)
      expect(flat).to include(hash)
    end

    it 'deflates series' do
      allow(@subject_manager).to(
        receive(:get_subject).and_return(@subject)
      )
      @series.reload
      hash = @series.attributes
      flat = @ape.deflate_series(@series)
      expect(flat).to include(hash)
    end

    it 'deflates programs' do
      allow(@subject_manager).to(
        receive(:get_subject).and_return(@subject)
      )
      @program.reload
      hash = @program.attributes
      flat = @ape.deflate_program(@program)
      expect(flat).to include(hash)
    end
  end
end
