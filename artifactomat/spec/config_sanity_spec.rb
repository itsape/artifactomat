# frozen_string_literal: true

require 'yaml'

# rubocop:disable Metrics/AbcSize
def rec_key_diff(path, hash1, hash2)
  key_diff = hash1.keys - hash2.keys
  key_diff += hash2.keys - hash1.keys
  return [path, key_diff] unless key_diff.empty?

  hash1.each_key do |key|
    next unless hash1[key].is_a?(Hash) && hash2[key].is_a?(Hash)

    sub_path = path + '.' + key.to_s
    key_diff = rec_key_diff(sub_path, hash1[key], hash2[key])
    return key_diff unless key_diff[1].empty?
  end
  [path, []]
end

def rec_type_diff(path, hash1, hash2)
  hash1.each_key do |key|
    class1 = hash1[key].class
    class2 = hash2[key].class
    return [path, key, [class1, class2]] unless class1 == class2

    next unless hash1[key].is_a?(Hash)

    sub_path = path + '.' + key.to_s
    ret = rec_type_diff(sub_path, hash1[key], hash2[key])
    return ret unless ret[2].empty?
  end
  [path, '', []]
end
# rubocop:enable Metrics/AbcSize

describe 'Config', config: true do
  conf_path = 'dist/etc/artifactomat/conf.yml'
  test_conf_path = 'spec/test_conf.yml'
  conf_content = File.read(conf_path)
  test_conf_content = File.read(test_conf_path)

  it 'has the same fields as the test config' do
    conf = YAML.safe_load(conf_content, [Symbol])
    test_conf = YAML.safe_load(test_conf_content, [Symbol])
    expect(rec_key_diff('', conf, test_conf)).to eq(['', []])
  end

  it 'has the same types as the test config' do
    conf = YAML.safe_load(conf_content, [Symbol])
    test_conf = YAML.safe_load(test_conf_content, [Symbol])
    expect(rec_type_diff('', conf, test_conf)).to eq(['', '', []])
  end

  it 'fails when adding an additional key' do
    conf = YAML.safe_load(conf_content, [Symbol])
    test_conf = YAML.safe_load(test_conf_content, [Symbol])
    conf[:test_key] = 0
    expect(rec_key_diff('', conf, test_conf)).to eq(['', [:test_key]])
  end

  it 'fails when a key type does not match' do
    conf = YAML.safe_load(conf_content, [Symbol])
    test_conf = YAML.safe_load(test_conf_content, [Symbol])
    conf[:test_key] = 0
    test_conf[:test_key] = 'string'
    expect(rec_key_diff('', conf, test_conf)).to eq(['', []])
    expect(rec_type_diff('', conf, test_conf)).to eq(['', :test_key, [Integer, String]])
  end

  it 'has comments prepending every block' do
    last_line_comment = 0
    uncommented_blocks = []
    conf_content.each_line do |line|
      # match comments at the beginning of a line
      if /^#/.match?(line)
        last_line_comment = 1
      # match key at the root level
      elsif /^[^\s]+.*:/.match?(line)
        uncommented_blocks.append(line) unless last_line_comment == 1
        last_line_comment = 0
      # match everything but blank lines
      elsif !/^\s*$/.match?(line)
        last_line_comment = 0
      end
    end
    expect(uncommented_blocks).to be_empty
  end
end
