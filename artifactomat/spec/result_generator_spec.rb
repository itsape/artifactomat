# frozen_string_literal: true

require 'artifactomat/modules/ape'
require 'artifactomat/modules/result_generator'

require 'date'
require 'yaml'
require 'csv'
require 'fakefs/safe'

describe ResultGenerator do
  before(:all) do
    Logging.initialize
  end

  before(:each) do
    @result_generator = ResultGenerator.new
  end

  context 'Report' do
    before(:all) do
      episodes_count = 3
      @series_label = 'ResultGeneratorTestSeries'
      @series_group_file = '/somewhere/on/the/hard/disk'
      @recipes = {}
      @seasons = {}
      @serializable_tracks = []
      @series = create(:test_series,
                       label: @series_label,
                       group_file: @series_group_file,
                       season_count: 0)
      # Build three seasons with one recipe and three episodes each.
      (0..2).each do |_index|
        recipe = build(:recipe)
        @recipes[recipe.id] = recipe
        season = create(:test_season,
                        recipe_id: recipe.id,
                        episode_count: episodes_count)
        @seasons[season.id] = {}
        @seasons[season.id]['track_entries'] = []
        @series.test_seasons << season
        # Add two track entries to the first epsiode and ...
        2.times do
          episode = season.test_episodes[0]
          create(:track_entry,
                 subject_id: episode.subject_id,
                 test_episode_id: episode.id)
          serializable_track = TrackEntry.last.serializable_hash
          serializable_track['online_time'] = 0
          @seasons[season.id]['track_entries'] << serializable_track.clone
          serializable_track['season_id'] = season.id
          @serializable_tracks << serializable_track
        end
        # ... one track entry to the third.
        episode = season.test_episodes[2]
        create(:track_entry,
               subject_id: episode.subject_id,
               test_episode_id: episode.id)
        serializable_track = TrackEntry.last.serializable_hash
        serializable_track['online_time'] = 0
        @seasons[season.id]['track_entries'] << serializable_track.clone
        serializable_track['season_id'] = season.id
        @serializable_tracks << serializable_track
        @episode_without_track_entry = season.test_episodes[1]
      end

      first = true
      @csv_track_entries = CSV.generate(col_sep: ';') do |csv|
        @serializable_tracks.each do |track|
          if first
            csv << track.keys
            first = false
          end
          csv << track.values
        end
      end
    end

    before(:each) do
      @configuration_manager = double('ConfigurationManager')
      @recipes.each do |key, value|
        allow(@configuration_manager)
          .to receive(:get_recipe).with(key) { value }
      end
    end

    context '#report_series' do
      before(:all) do
        @headers = %w[subject_id recipe score course_of_action online_time season_label]
        @expected_subjects = []
        @seasons.each do |_, season|
          season['track_entries'].each do |entry|
            @expected_subjects.push entry['subject_id']
          end
        end
      end

      it 'has a method report_series' do
        expect(@result_generator).to respond_to(:report_series)
      end

      it 'returns the correct amount of track entries' do
        expect(Logging).to receive(:info)
        results = YAML.safe_load(@result_generator.report_series(@series.id, [@headers, nil], nil, 'yaml'))
        expect(results.size).to eql(9)
      end

      it 'returns the correct data as yaml' do
        expect(Logging).to receive(:info)
        results = YAML.safe_load(@result_generator.report_series(@series.id, [@headers, nil], nil, 'yaml'))

        subjects = results.map { |x| x['subject_id'] }
        expect(subjects).to eql(@expected_subjects)
      end

      it 'returns the correct data as csv' do
        expect(Logging).to receive(:info)
        results = CSV.parse(@result_generator.report_series(@series.id, [@headers, nil], nil, 'csv'), headers: true, col_sep: ';')

        expect(results.headers).to eql(@headers)
        expect(results['subject_id']).to eql(@expected_subjects)
      end

      it 'returns the correct data as yaml if invalid format is passed' do
        allow(Logging).to receive(:info)
        expect(Logging).to receive(:warning)
        results = YAML.safe_load(@result_generator.report_series(@series.id, [@headers, nil], nil, 'json'))

        subjects = results.map { |x| x['subject_id'] }
        expect(subjects).to eql(@expected_subjects)
      end

      it 'contains the tracked online time' do
        expect(Logging).to receive(:info)

        # create 5 minutes online time for one subject
        test_episode = without_bullet { @series.test_seasons[0].test_episodes[0] }
        subject_id = test_episode.subject_id
        now = Time.now
        test_episode.schedule_start = now - 900
        test_episode.schedule_end = now - 300
        test_episode.save

        sh = SubjectHistory.create(test_series_id: @series.id,
                                   subject_id: subject_id,
                                   ip: '127.0.0.43',
                                   session_begin: now - 800,
                                   session_end: now - 500, # 5 minutes
                                   submit_time: now)

        @headers = %w[subject_id online_time]
        track_entries = YAML.safe_load(@result_generator.report_series(@series.id, [@headers, nil], nil, 'yaml'))

        track_entries.each do |entry|
          expect(entry['online_time']).to be
          if entry['subject_id'] == subject_id
            expect(entry['online_time']).to eq(5)
          else
            expect(entry['online_time']).to eq(0)
          end
        end

        # clean up
        sh.destroy!
        test_episode.schedule_start = nil
        test_episode.schedule_end = nil
        test_episode.save
      end

      it 'correctly infers online time for episode without track entries' do
        expect(Logging).to receive(:info)

        # create 5 minutes online time for subject of ep without track entries
        now = Time.now
        subject_id = @episode_without_track_entry.subject_id
        @episode_without_track_entry.schedule_start = now - 900
        @episode_without_track_entry.schedule_end = now - 300
        @episode_without_track_entry.save
        sh = SubjectHistory.create(test_series_id: @series.id,
                                   subject_id: subject_id,
                                   ip: '127.0.0.42',
                                   session_begin: now - 800,
                                   session_end: now - 500, # 5 minutes
                                   submit_time: now)

        @headers = %w[subject_id online_time]
        track_entries = YAML.safe_load(@result_generator.report_series(@series.id, [@headers, nil], nil, 'yaml'))
        track_entries.each do |entry|
          expect(entry['online_time']).to be
          if entry['subject_id'] == subject_id
            expect(entry['online_time']).to eq(5)
          else
            expect(entry['online_time']).to eq(0)
          end
        end

        # clean up
        sh.destroy!
        @episode_without_track_entry.schedule_start = nil
        @episode_without_track_entry.schedule_end = nil
        @episode_without_track_entry.save
      end

      it 'writes to file if filename is given' do
        allow(Logging).to receive(:info)
        filename = 'subjects.yml'
        results = FakeFS.with_fresh do
          @result_generator.report_series(@series.id, [@headers, nil], filename, 'yaml')
          YAML.load_file(filename)
        end

        subjects = results.map { |x| x['subject_id'] }
        expect(subjects).to eql(@expected_subjects)
      end

      it 'fails if series is unknown' do
        expect(Logging).to receive(:info)
        expect { @result_generator.report_series(42, [@headers, nil], nil, 'yaml') }
          .to raise_error('Unknown series')
      end

      describe 'custom header parsing' do
        it 'supports addition of new headers with + prefix' do
          expect(Logging).to receive(:info)
          raw_results = @result_generator.report_series(@series.id,
                                                        [@headers, '+test_episode_id, +season_id, +recipe'],
                                                        nil,
                                                        'yaml')
          results = YAML.safe_load(raw_results)

          results.each do |result|
            @headers.each do |header|
              expect(result.key?(header)).to be_truthy
            end
            expect(result.key?('test_episode_id')).to be_truthy
            expect(result.key?('season_id')).to be_truthy
          end
        end

        it 'supports addition of new headers without prefix' do
          expect(Logging).to receive(:info)
          raw_results = @result_generator.report_series(@series.id,
                                                        [@headers, 'test_episode_id, season_id, recipe'],
                                                        nil,
                                                        'yaml')
          results = YAML.safe_load(raw_results)

          results.each do |result|
            @headers.each do |header|
              expect(result.key?(header)).to be_truthy
            end
            expect(result.key?('test_episode_id')).to be_truthy
            expect(result.key?('season_id')).to be_truthy
          end
        end

        it 'supports removal of default headers' do
          expect(Logging).to receive(:info)
          raw_results = @result_generator.report_series(@series.id,
                                                        [@headers, '-test_episode_id, -recipe, -score'],
                                                        nil,
                                                        'yaml')
          results = YAML.safe_load(raw_results)

          results.each do |result|
            expect(result.key?('test_episode_id')).to be_falsy
            expect(result.key?('recipe')).to be_falsy
            expect(result.key?('score')).to be_falsy
          end
        end

        it 'supports mixing removal and addition of headers' do
          expect(Logging).to receive(:info)
          raw_results = @result_generator.report_series(@series.id,
                                                        [@headers, 'test_episode_id, +season_id, -recipe'],
                                                        nil,
                                                        'yaml')
          results = YAML.safe_load(raw_results)

          results.each do |result|
            (@headers - ['recipe']).each do |header|
              expect(result.key?(header)).to be_truthy
            end
            expect(result.key?('test_episode_id')).to be_truthy
            expect(result.key?('season_id')).to be_truthy
            expect(result.key?('recipe')).to be_falsy
          end
        end
      end
    end
  end

  describe '#report_online' do
    before :each do
      @ape = double('Ape')
      @subject_tracker = double('SubjectTracker')
      @vstore = double('VStore')
      @subject = build(:subject)
      @subject2 = build(:subject)
      @subject3 = build(:subject)
      @now = Time.now.utc

      allow(Ape).to receive(:instance).and_return(@ape)
      allow(@ape).to receive(:subject_tracker).and_return(@subject_tracker)
      allow(@ape).to receive(:vstore).and_return(@vstore)
      allow(@vstore).to receive(:subjects_from_map).and_return([@subject.id]) # online subjects
      allow(@subject_tracker).to receive(:subject_ids).and_return([@subject.id, @subject2.id, @subject3.id]) # all registered subjects

      program = TestProgram.create(label: 'spec_prog')
      @series = program.test_series.create(label: 'spec_seri', group_file: 'spec', start_date: Time.now.utc - 900)
      @season = @series.test_seasons.create(label: 'spec_seas1', recipe_id: 'spec', duration: 10, screenshots_folder: '')
    end

    after :each do
      without_bullet do
        TestProgram.destroy_all
        TestEpisode.destroy_all
        SubjectHistory.destroy_all
      end
    end

    it 'shows offline subjects as offline' do
      results = YAML.safe_load(@result_generator.report_online('yaml'))
      offline_subjects = results.select { |elem| elem['online'] == false }.map { |elem| elem['subject'] }

      expect(offline_subjects.size).to eq(2)
      expect(offline_subjects.to_set).to eq(Set[@subject2.id, @subject3.id])
    end

    it 'shows online subjects as online' do
      results = YAML.safe_load(@result_generator.report_online('yaml'))
      online_subjects = results.select { |elem| elem['online'] == true }.map { |elem| elem['subject'] }

      expect(online_subjects.size).to eq(1)
      expect(online_subjects[0]).to eq(@subject.id)
    end

    it 'correctly shows armed status' do
      episode1 = @season.test_episodes.create(subject_id: @subject.id,
                                              schedule_start: @now - 900,
                                              schedule_end: @now)
      episode2 = @season.test_episodes.create(subject_id: @subject2.id,
                                              schedule_start: @now - 900,
                                              schedule_end: @now)
      episode3 = @season.test_episodes.create(subject_id: @subject2.id,
                                              schedule_start: @now - 900,
                                              schedule_end: @now)
      episode4 = @season.test_episodes.create(subject_id: @subject3.id,
                                              schedule_start: @now - 900,
                                              schedule_end: @now)
      episode1.running!  # -> @subject should be armed
      episode4.running!  # -> subject3 should be armed
      episode2.prepared!
      episode3.completed!

      results = YAML.safe_load(@result_generator.report_online('yaml'))
      armed_subjects = results.select { |elem| elem['armed'] == true }.map { |elem| elem['subject'] }

      expect(armed_subjects.size).to eq(2)
      expect(armed_subjects.to_set).to eq(Set[@subject.id, @subject3.id])
    end

    describe 'online time calculation' do
      it 'subject not online at all' do
        ep = @season.test_episodes.create(subject_id: @subject.id,
                                          schedule_start: @now - 900,
                                          schedule_end: @now - 200)
        ep.completed!

        results = YAML.safe_load(@result_generator.report_online('yaml'))
        subject_results = results.select { |elem| elem['subject'] == @subject.id }[0]
        expect(subject_results['tracked']).to eq('0 minutes')
      end

      it 'subject 3 + 2 minutes online during one episode' do
        ep = @season.test_episodes.create(subject_id: @subject.id,
                                          schedule_start: @now - 900,
                                          schedule_end: @now - 200)
        ep.completed!
        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject.id,
                              ip: '127.0.0.43',
                              session_begin: @now - 800,
                              session_end: @now - 620,  # 3 minutes
                              submit_time: @now)
        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject.id,
                              ip: '127.0.0.43',
                              session_begin: @now - 500,
                              session_end: @now - 380,  # 2 minutes
                              submit_time: @now)

        results = YAML.safe_load(@result_generator.report_online('yaml'))
        subject_results = results.select { |elem| elem['subject'] == @subject.id }[0]

        expect(subject_results['tracked']).to eq('5 minutes')
      end

      it 'subject online since before episode started' do
        ep = @season.test_episodes.create(subject_id: @subject.id,
                                          schedule_start: @now - 900,
                                          schedule_end: @now - 200)
        ep.completed!
        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject.id,
                              ip: '127.0.0.43',
                              session_begin: @now - 1000, # starts at @now - 900
                              session_end: @now - 720, # 3 minutes
                              submit_time: @now)

        results = YAML.safe_load(@result_generator.report_online('yaml'))
        subject_results = results.select { |elem| elem['subject'] == @subject.id }[0]

        expect(subject_results['tracked']).to eq('3 minutes')
      end

      it 'subject session not ended yet' do
        ep = @season.test_episodes.create(subject_id: @subject.id,
                                          schedule_start: @now - 900,
                                          schedule_end: @now - 200)
        ep.completed!
        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject.id,
                              ip: '127.0.0.43',
                              session_begin: @now - 380,
                              session_end: nil, # ends at @now - 200 -> 3 minutes
                              submit_time: @now)

        results = YAML.safe_load(@result_generator.report_online('yaml'))
        subject_results = results.select { |elem| elem['subject'] == @subject.id }[0]

        expect(subject_results['tracked']).to eq('3 minutes')
      end

      it 'subject 3 minutes online during episode and 2 + 1 minutes after episode end' do
        ep = @season.test_episodes.create(subject_id: @subject.id,
                                          schedule_start: @now - 900,
                                          schedule_end: @now - 200)
        ep.completed!
        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject.id,
                              ip: '127.0.0.43',
                              session_begin: @now - 380,
                              session_end: @now - 80, # episode ends at @now - 200
                              submit_time: @now)

        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject.id,
                              ip: '127.0.0.43',
                              session_begin: @now - 70,
                              session_end: @now - 5,
                              submit_time: @now)

        results = YAML.safe_load(@result_generator.report_online('yaml'))
        subject_results = results.select { |elem| elem['subject'] == @subject.id }[0]

        expect(subject_results['tracked']).to eq('3 minutes')
      end

      it 'online during multiple episodes' do
        ep1 = @season.test_episodes.create(subject_id: @subject.id,
                                           schedule_start: @now - 3000,
                                           schedule_end: @now - 1800)
        ep1.completed!
        ep2 = @season.test_episodes.create(subject_id: @subject.id,
                                           schedule_start: @now - 1500,
                                           schedule_end: @now - 300)
        ep2.completed!

        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject.id,
                              ip: '127.0.0.43',
                              session_begin: @now - 2900,
                              session_end: @now - 2600,  # 5 minutes during ep1
                              submit_time: @now)

        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject.id,
                              ip: '127.0.0.43',
                              session_begin: @now - 1700,
                              session_end: @now - 1580,  # 2 minutes between eps
                              submit_time: @now)

        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject.id,
                              ip: '127.0.0.43',
                              session_begin: @now - 1400,
                              session_end: @now - 1160,  # 4 minutes during ep2
                              submit_time: @now)

        results = YAML.safe_load(@result_generator.report_online('yaml'))
        subject_results = results.select { |elem| elem['subject'] == @subject.id }[0]

        expect(subject_results['tracked']).to eq('9 minutes')
      end

      it 'multiple subjects' do
        ep1 = @season.test_episodes.create(subject_id: @subject.id,
                                           schedule_start: @now - 3000,
                                           schedule_end: @now - 1800)
        ep1.completed!
        ep2 = @season.test_episodes.create(subject_id: @subject.id,
                                           schedule_start: @now - 1500,
                                           schedule_end: @now - 300)
        ep2.completed!
        ep3 = @season.test_episodes.create(subject_id: @subject2.id,
                                           schedule_start: @now - 1200,
                                           schedule_end: @now - 600)
        ep3.completed!

        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject.id,
                              ip: '127.0.0.43',
                              session_begin: @now - 2900,
                              session_end: @now - 2600, # 5 minutes during ep1
                              submit_time: @now)

        SubjectHistory.create(test_series_id: @series.id,
                              subject_id: @subject2.id,
                              ip: '127.0.0.44',
                              session_begin: @now - 900,
                              session_end: @now - 780, # 2 minutes during ep3
                              submit_time: @now)

        results = YAML.safe_load(@result_generator.report_online('yaml'))
        online_hash = {}
        results.each do |res|
          online_hash[res['subject']] = res['tracked']
        end

        expect(online_hash[@subject.id]).to eq('5 minutes')
        expect(online_hash[@subject2.id]).to eq('2 minutes')
        expect(online_hash[@subject3.id]).to eq('0 minutes')
      end
    end
  end

  describe '#report_detection' do
    before(:each) do
      @now = Time.now.round
      @dt_now = @now.to_datetime
      season1 = create(:test_season, label: 'SpecSeason1', recipe_id: 'SpecRecipe1')
      season2 = create(:test_season, label: 'SpecSeason2', recipe_id: 'SpecRecipe2')
      te11 = season1.test_episodes.create(subject_id: 'group1/1')
      te12 = season1.test_episodes.create(subject_id: 'group1/2')
      te21 = season2.test_episodes.create(subject_id: 'group2/1')
      te22 = season2.test_episodes.create(subject_id: 'group2/2')

      # artifact_present: 0 - impossible, 1 - possible, 2 - certain
      create(:detection_event, test_episode: te11, timestamp: @dt_now, artifact_present: 0)
      create(:detection_event, test_episode: te11, timestamp: @dt_now + 1.minute, artifact_present: 1)
      create(:detection_event, test_episode: te12, timestamp: @dt_now, artifact_present: 0)
      create(:detection_event, test_episode: te12, timestamp: @dt_now + 30.seconds, artifact_present: 2)
      create(:detection_event, test_episode: te21, timestamp: @dt_now, artifact_present: 0)
      create(:detection_event, test_episode: te21, timestamp: @dt_now + 1.seconds, artifact_present: 2)
      create(:detection_event, test_episode: te21, timestamp: @dt_now + 60.seconds, artifact_present: 0)
      create(:detection_event, test_episode: te22, timestamp: @dt_now, artifact_present: 0)
    end

    it 'succeeds' do
      # Database `datetime` objects are converted to ruby `Time` objects.
      results = YAML.safe_load(@result_generator.report_detection('yaml'), [Time])
      expected_results = [
        { 'subject' => 'group1/1', 'season' => 'SpecSeason1', 'recipe' => 'SpecRecipe1', 'timestamp' => @now, 'artifact_present' => 'impossible' },
        { 'subject' => 'group1/1', 'season' => 'SpecSeason1', 'recipe' => 'SpecRecipe1', 'timestamp' => @now + 1.minute, 'artifact_present' => 'possible' },
        { 'subject' => 'group1/2', 'season' => 'SpecSeason1', 'recipe' => 'SpecRecipe1', 'timestamp' => @now, 'artifact_present' => 'impossible' },
        { 'subject' => 'group1/2', 'season' => 'SpecSeason1', 'recipe' => 'SpecRecipe1', 'timestamp' => @now + 30.seconds, 'artifact_present' => 'certain' },
        { 'subject' => 'group2/1', 'season' => 'SpecSeason2', 'recipe' => 'SpecRecipe2', 'timestamp' => @now, 'artifact_present' => 'impossible' },
        { 'subject' => 'group2/1', 'season' => 'SpecSeason2', 'recipe' => 'SpecRecipe2', 'timestamp' => @now + 1.seconds, 'artifact_present' => 'certain' },
        { 'subject' => 'group2/1', 'season' => 'SpecSeason2', 'recipe' => 'SpecRecipe2', 'timestamp' => @now + 60.seconds, 'artifact_present' => 'impossible' },
        { 'subject' => 'group2/2', 'season' => 'SpecSeason2', 'recipe' => 'SpecRecipe2', 'timestamp' => @now, 'artifact_present' => 'impossible' }
      ]
      expect(results).to match(expected_results)
    end
  end
end
