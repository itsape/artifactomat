# frozen_string_literal: true

require 'artifactomat/modules/recipe_manager'
require 'artifactomat/modules/configuration_manager'
require 'artifactomat/modules/logging'
require 'artifactomat/models/recipe'

require 'fakefs/spec_helpers'
require 'fileutils'
require 'yaml'

describe RecipeManager do
  include FakeFS::SpecHelpers

  def write_test_recipe(location, recipe)
    file_location = File.join(location, 'recipes', "#{recipe.id}.yml")
    File.open(file_location, mode: 'w') do |file|
      file.write(YAML.dump(recipe))
    end
  end

  def clone_config_to_fakefs(target)
    FakeFS::FileSystem.clone(@artifactomat_path, target)
    FileUtils.rm_rf(File.join(target, '/recipes'))
    FileUtils.mkdir(File.join(target, '/recipes'))
  end

  let(:recipe) { build(:recipe) }
  let(:tf) { build(:track_filter) }

  before(:all) do
    FakeFS.activate!

    file = File.join(File.dirname(__FILE__), '..', 'dist', 'etc', 'artifactomat')
    @artifactomat_path = File.expand_path(file)
  end

  before(:each) do
    FakeFS::FileSystem.clear
    clone_config_to_fakefs('/')

    allow(Logging).to receive(:info)
    @recipe_manager = RecipeManager.new(@artifactomat_path)
  end

  after(:all) do
    FakeFS.deactivate!
  end

  describe '::initialize' do
    describe 'custom path' do
      it 'loads from a custom path' do
        FileUtils.mkdir '/other'
        clone_config_to_fakefs('/other')
        expect { RecipeManager.new('/other') }.not_to raise_error
      end

      it 'raises ConfigError on not readable recipe directory' do
        allow(File).to receive(:readable?).and_return(false)
        expected = expect do
          RecipeManager.new(@artifactomat_path)
        end
        expected.to raise_error(ConfigError)
      end

      it 'raises error on non existant custom path' do
        bogus = 'sdökljfaasökldfjsdklöj'
        expect { RecipeManager.new(bogus) }.to raise_error(ConfigError)
      end
    end

    context 'recipes' do
      it 'writes to log if trying to load invalid recipe' do
        expect(Logging).to receive(:warning)
        file_location = File.join(@artifactomat_path, 'recipes', 'testrecipe2.yml')
        File.open(file_location, mode: 'w') do |file|
          file.write(YAML.dump('unvalid'))
        end
        @cm = RecipeManager.new(@artifactomat_path)
      end

      it 'does not load recipes without duration' do
        recipe.duration = 0
        expect(Logging).to receive(:warning)
        write_test_recipe(@artifactomat_path, recipe)
        @cm = RecipeManager.new(@artifactomat_path)
        expect(@cm.list_recipes).to be_empty
      end

      it 'does not load recipes with leading dot in filename' do
        recipe.name = '.dot'
        write_test_recipe(@artifactomat_path, recipe)
        @cm = RecipeManager.new(@artifactomat_path)
        expect(@cm.list_recipes).to be_empty
      end

      it 'does not load recipes without trackfilters' do
        FakeFS::FileSystem.delete(File.join(@artifactomat_path, 'conf.d', 'trackfilter.yml'))
        recipe.trackfilters = []
        expect(Logging).to receive(:warning)
        write_test_recipe(@artifactomat_path, recipe)
        @cm = RecipeManager.new(@artifactomat_path)
        expect(@cm.list_recipes).to be_empty
      end

      it 'does inject trackfilters to recipes' do
        FakeFS::FileSystem.delete(File.join(@artifactomat_path, 'conf.d', 'trackfilter.yml'))
        File.open(File.join(@artifactomat_path, 'conf.d', 'trackfilter.yml'), 'w') do |f|
          YAML.dump([tf], f)
        end
        recipe.trackfilters = []
        write_test_recipe(@artifactomat_path, recipe)
        @cm = RecipeManager.new(@artifactomat_path)
        recipes = @cm.list_recipes
        expect(recipes).to_not be_empty
        r = @cm.get_recipe(recipes.first)
        expect(r.trackfilters.count).to eq 1
        tf1 = r.trackfilters.first
        expect(tf1.course_of_action == tf.course_of_action).to be_truthy
        expect(tf1.extraction_pattern == tf.extraction_pattern).to be_truthy
        # two different regexp objects!
        expect(tf1.pattern.to_s == tf.pattern.to_s).to be_truthy
        expect(tf1.score == tf.score).to be_truthy
      end

      it 'does set id on load' do
        recipe.id = 'someid'
        expect(Logging).not_to receive(:warning)
        file_location = File.join(@artifactomat_path, 'recipes', 'broken.yml')
        File.open(file_location, mode: 'w') do |file|
          file.write(YAML.dump(recipe))
        end
        @cm = RecipeManager.new(@artifactomat_path)
        expect(@cm.list_recipes).to contain_exactly 'broken'
      end

      it 'logs that it initialized the config' do
        expect(Logging).to receive(:info).with('RecipeManager', 'Initialized recipes.').once
        @cm = RecipeManager.new(@artifactomat_path)
      end
    end
  end

  describe '#list_recipes' do
    it 'succeeds' do
      write_test_recipe @artifactomat_path, recipe
      @cm = RecipeManager.new(@artifactomat_path)
      expect(@cm.list_recipes).to contain_exactly(recipe.id)
    end
  end

  describe '#get_recipe' do
    before(:each) do
      FakeFS::FileSystem.delete(File.join(@artifactomat_path, 'conf.d', 'trackfilter.yml'))
      write_test_recipe @artifactomat_path, recipe
      @cm = RecipeManager.new(@artifactomat_path)
    end

    it 'succeeds' do
      expect(@cm.get_recipe(recipe.id).id).to match(recipe.id)
    end

    it 'returns recipe model' do
      testrecipe = @cm.get_recipe(recipe.id)
      expect(testrecipe).to be_a_kind_of(Recipe)
    end

    it 'raises ConfigError on not loaded recipe' do
      expect do
        @cm.get_recipe('not_a_recipe')
      end.to raise_error(ConfigError)
    end

    it 'gets recipes with underscores' do
      recipe.id = 'test_recipe'
      write_test_recipe(@artifactomat_path, recipe)
      @cm = RecipeManager.new
      expect(@cm.get_recipe(recipe.id)).not_to be_nil
    end

    it 'gets recipes with hyphen' do
      recipe.id = 'test-recipe'
      write_test_recipe(@artifactomat_path, recipe)
      @cm = RecipeManager.new
      expect(@cm.get_recipe(recipe.id)).not_to be_nil
    end
  end

  describe '#get_recipes' do
    before(:each) do
      FakeFS::FileSystem.delete(File.join(@artifactomat_path, 'conf.d', 'trackfilter.yml'))
      write_test_recipe @artifactomat_path, recipe
      @cm = RecipeManager.new
    end

    it 'succeeds' do
      expect(@cm.get_recipes.first.id).to match(recipe.id)
    end

    it 'returns all recipes' do
      recipes = @cm.get_recipes
      expect(recipes.to_yaml).to eq([recipe].to_yaml)
    end
  end
end
