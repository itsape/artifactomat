# frozen_string_literal: true

require 'date'
require 'active_record_spec_helper'
require 'artifactomat/models/detection_event'

describe DetectionEvent, type: :model do
  it { should belong_to(:test_episode) }
  it { should validate_presence_of(:timestamp) }
  it { should have_db_column(:artifact_present) }

  describe '#from_detection_timetable_row' do
    before(:each) do
      DetectionEvent.destroy_all
      @now = DateTime.now.utc
      @timestamp = @now.strftime('%y%m%d%H%M%S%L')
    end

    after(:each) do
      DetectionEvent.destroy_all
    end

    it 'saves an event to the db' do
      row = [@timestamp, '0']
      DetectionEvent.from_detection_timetable_row(row, 1)
      expect(DetectionEvent.all.length).to eq(1)
    end

    it 'correctly parses timestamp' do
      row = [@timestamp, '0']
      DetectionEvent.from_detection_timetable_row(row, 1)
      expect(DetectionEvent.last.timestamp).to be_within(1.seconds).of(@now)
    end

    it 'correctly parses artifact_present=impossible' do
      row = [@timestamp, '0']
      DetectionEvent.from_detection_timetable_row(row, 1)
      expect(DetectionEvent.last.impossible?).to be true
    end

    it 'correctly parses artifact_present=possible' do
      row = [@timestamp, '1']
      DetectionEvent.from_detection_timetable_row(row, 1)
      expect(DetectionEvent.last.possible?).to be true
    end

    it 'correctly parses artifact_present=certain' do
      row = [@timestamp, '2']
      DetectionEvent.from_detection_timetable_row(row, 1)
      expect(DetectionEvent.last.certain?).to be true
    end
  end
end
