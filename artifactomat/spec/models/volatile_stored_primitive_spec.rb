# frozen_string_literal: true

require 'artifactomat/models/volatile_stored_primitive'

describe VolatileStoredPrimitive do
  before(:each) do
    @conn = double('Connection')
    @class_name = 'LongTermMemory'
    @variable_name = 'subjects'
    @redis_varname = 'LongTermMemory__subjects'
    @vstore = double('VStore')
    @default_value = 42
  end

  context 'no debug options' do
    before(:each) do
      @options = {}
      @redis_return_value = '3'
      @redis_return_int = 3

      allow(@vstore).to receive(:with_state_saving_connection).and_yield(@conn)
      allow(@conn).to receive(:get).with(@redis_varname).and_return(@redis_return_value)

      @vsp = VolatileStoredPrimitive.new(@class_name, @variable_name, @vstore,
                                         @default_value, @options)
    end

    it 'initializes from redis' do
      expect(@vstore).to receive(:with_state_saving_connection).and_yield(@conn)
      expect(@conn).to receive(:get).with(@redis_varname).and_return('3')

      vsp = VolatileStoredPrimitive.new(@class_name, @variable_name, @vstore,
                                        @default_value, @options)

      expect(vsp.instance_variable_get(:@value)).to eq(3)
    end

    it 'initializes with default value if redis returns no value' do
      allow(@conn).to receive(:get).with(@redis_varname).and_return(nil)

      vsp = VolatileStoredPrimitive.new(@class_name, @variable_name, @vstore,
                                        @default_value, @options)

      expect(vsp.instance_variable_get(:@value)).to eq(@default_value)
    end

    it '#get returns the saved value' do
      val = @vsp.get

      expect(val).to eq(@redis_return_int)
    end

    it '#get does not reload value from redis' do
      expect(@vstore).not_to receive(:with_state_saving_connection)
      expect(@conn).not_to receive(:get)

      @vsp.get
    end

    it '#set sets the value' do
      allow(@conn).to receive(:set)

      @vsp.set(12)

      expect(@vsp.get).to eq(12)
    end

    it '#set writes to redis' do
      expect(@conn).to receive(:set).with(@redis_varname, '12')

      @vsp.set(12)
    end
  end

  context 'debug reread from redis' do
    before(:each) do
      @options = { debug_reread_from_redis: true }

      allow(@vstore).to receive(:with_state_saving_connection).and_yield(@conn)
      # make redis return 6 as default
      allow(@conn).to receive(:get).with(@redis_varname).and_return('6')
      @vsp = VolatileStoredPrimitive.new(@class_name, @variable_name, @vstore,
                                         @default_value, @options)
    end

    it '#get loads from redis' do
      expect(@conn).to receive(:get).with(@redis_varname).and_return('7')
      val = @vsp.get
      expect(val).to eq(7)
    end

    it 'raises if debug_ignore_redis is set aswell' do
      @options[:debug_ignore_redis] = true

      expect { VolatileStoredPrimitive.new(@class_name, @variable_name, @vstore, @default_value, @options) }.to(
        raise_error('configuration error; you cannot ignore redis but reread from it')
      )
    end
  end

  context 'debug ignore redis' do
    before(:each) do
      @options = { debug_ignore_redis: true }

      @vsp = VolatileStoredPrimitive.new(@class_name, @variable_name, @vstore,
                                         @default_value, @options)
    end

    it 'does not initialize from redis' do
      expect(@vstore).not_to receive(:with_state_saving_connection)
      expect(@conn).not_to receive(:get)

      vsp = VolatileStoredPrimitive.new(@class_name, @variable_name, @vstore, @default_value, @options)

      expect(vsp.instance_variable_get(:@value)).to eq(@default_value)
    end

    it '#get does not load from redis' do
      expect(@vstore).not_to receive(:with_state_saving_connection)
      expect(@conn).not_to receive(:get)

      val = @vsp.get

      expect(val).to eq(@default_value)
    end

    it '#set does not write to redis' do
      expect(@vstore).not_to receive(:with_state_saving_connection)
      expect(@conn).not_to receive(:get)

      @vsp.set(33)

      expect(@vsp.get).to eq(33)
    end

    it 'raises if debug_reread_from_redis is set aswell' do
      @options[:debug_reread_from_redis] = true

      expect { VolatileStoredPrimitive.new(@class_name, @variable_name, @vstore, @default_value, @options) }.to(
        raise_error('configuration error; you cannot ignore redis but reread from it')
      )
    end
  end
end
