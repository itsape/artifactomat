# frozen_string_literal: true

require 'artifactomat/models/routing_element'

describe RoutingElement, type: :model do
  context 'check required attributes' do
    before(:all) do
      @re = build(:routing_element)
    end

    describe 'check attr_reader' do
      it 'has a series_id attribute reader' do
        expect { @re.series_id }.not_to raise_error
      end

      it 'has a season_id attribute reader' do
        expect { @re.season_id }.not_to raise_error
      end

      it 'has a external_ip attribute reader' do
        expect { @re.external_ip }.not_to raise_error
      end

      it 'has a internal_ip attribute reader' do
        expect { @re.internal_ip }.not_to raise_error
      end

      it 'has a external_ports attribute reader' do
        expect { @re.external_ports }.not_to raise_error
      end

      it 'has a internal_ports attribute reader' do
        expect { @re.internal_ports }.not_to raise_error
      end
    end

    describe 'check attr_writer' do
      it 'has a series_id attribute reader' do
        expect { @re.series_id = 1 }.not_to raise_error
      end

      it 'has a season_id attribute reader' do
        expect { @re.season_id = 1 }.not_to raise_error
      end

      it 'has a external_ip attribute reader' do
        expect { @re.external_ip = '192.168.1.101' }.not_to raise_error
      end

      it 'has a internal_ip attribute reader' do
        expect { @re.internal_ip = '192.168.1.1' }.not_to raise_error
      end

      it 'has a external_ports attribute reader' do
        expect { @re.external_ports = [8080] }.not_to raise_error
      end

      it 'has a internal_ports attribute reader' do
        expect { @re.internal_ports = [9090] }.not_to raise_error
      end
    end
  end
end
