# frozen_string_literal: true

require 'artifactomat/models/infrastructure_element'

describe InfrastructureElement, type: :model do
  context 'check required attributes' do
    let(:ie) { build(:infrastructure_element) }

    describe 'check attr_reader' do
      it 'has a monitor_script attribute reader' do
        expect { ie.monitor_script }.not_to raise_error
      end

      it 'has a generate_script attribute reader' do
        expect { ie.generate_script }.not_to raise_error
      end

      it 'has a destroy_script attribute reader' do
        expect { ie.destroy_script }.not_to raise_error
      end

      it 'has a disarm_script attribute reader' do
        expect { ie.disarm_script }.not_to raise_error
      end

      it 'has a infrastructure_type attribute reader' do
        expect { ie.infrastructure_type }.not_to raise_error
      end

      it 'has a parameters attribute reader' do
        expect { ie.parameters }.not_to raise_error
      end

      it 'has a season_id attribute reader' do
        expect { ie.season_id }.not_to raise_error
      end
    end

    describe 'check attr_writer' do
      it 'has a monitor_script attribute writer' do
        expect { ie.monitor_script = 'true' }.not_to raise_error
      end

      it 'has a generate_script attribute writer' do
        expect { ie.generate_script = 'true' }.not_to raise_error
      end

      it 'has a destroy_script attribute writer' do
        expect { ie.destroy_script = 'true' }.not_to raise_error
      end

      it 'has a disarm_script attribute writer' do
        expect { ie.disarm_script = 'true' }.not_to raise_error
      end

      it 'has a infrastructure_type attribute writer' do
        expect { ie.infrastructure_type = 'Type' }.not_to raise_error
      end

      it 'has a parameters attribute writer' do
        expect { ie.parameters = { 'a' => 'b' } }.not_to raise_error
      end

      it 'has a season_id attribute writer' do
        expect { ie.season_id = nil }.not_to raise_error
      end
    end

    describe '#season' do
      it 'infers the season from its season id' do
        season = build(:test_season)
        season.id = 128
        season.save!
        ie.season_id = season.id
        expect(ie.season).to eq(season)
      end
    end

    describe '#to_str' do
      it 'includes .type, .season_id and .season.label if season is set' do
        season = build(:test_season)
        season.id = 5
        season.save!
        ie.season_id = season.id
        expect(ie.to_str).to match(ie.infrastructure_type)
        expect(ie.to_str).to match(season.id.to_s)
        expect(ie.to_str).to match(season.label)
      end

      it 'works if season is not set' do
        expect(ie.to_str).to match(ie.infrastructure_type)
      end
    end

    describe '#errors' do
      it 'returns an Array' do
        expect(ie.errors).to be_a Array
      end

      it '.infrastructure_type exists' do
        ie.infrastructure_type = nil
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'type'
      end

      it '.ports exists' do
        ie.ports = nil
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'ports'
      end

      it '.ports is an array' do
        ie.ports = 80
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'ports'
        expect(ie.errors.first).to include 'array'
      end

      it '.ports[i] is Numeric' do
        ie.ports = ['port']
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'port'
        expect(ie.errors.first).to include 'Numeric'
      end

      it '.ports[i] is > 1' do
        ie.ports = [1]
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'port'
        expect(ie.errors.first).to include '< 2'
      end

      it '.ports[i] is < 65535' do
        ie.ports = [65_536]
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'port'
        expect(ie.errors.first).to include '> 65535'
      end

      it '.monitor_script exits' do
        ie.monitor_script = ''
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'monitor script'
      end

      it '.generate_script exits' do
        ie.generate_script = ''
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'generate script'
      end

      it '.destroy_script exits' do
        ie.destroy_script = ''
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'destroy script'
      end

      it '.arm_script exits' do
        ie.arm_script = ''
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'arm script'
      end

      it '.disarm_script exits' do
        ie.disarm_script = ''
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'disarm script'
      end

      it '.names exists' do
        ie.names = nil
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'names'
        expect(ie.errors.first).to include 'array'
      end

      it '.names is an array' do
        ie.names = 'not.me'
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'names'
        expect(ie.errors.first).to include 'array'
      end

      it '.names[i] is a string' do
        ie.names = [4]
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'name'
        expect(ie.errors.first).to include 'string'
      end

      it '.names[i] is not blank' do
        ie.names = ['']
        expect(ie.errors.count).to eq 1
        expect(ie.errors.first).to include 'name'
        expect(ie.errors.first).to include 'blank'
      end
    end
  end
end
