# frozen_string_literal: true

require 'artifactomat/utils/mutexed_array'

describe MutexedArray do
  before(:each) do
    @mutex = double('Mutex')
    allow(Mutex).to receive(:new).and_return(@mutex)
    @mutexed_array = MutexedArray.new([0])
    # rspec uses Mutex internally, so original behaviour has to be restored
    allow(Mutex).to receive(:new).and_call_original
  end

  it 'initializes with a given array' do
    mutexed_array = MutexedArray.new([1, 2, 3])

    expect(mutexed_array.instance_variable_get(:@array)).to eq([1, 2, 3])
  end

  it 'initializes with an empty array if none was given' do
    mutexed_array = MutexedArray.new

    expect(mutexed_array.instance_variable_get(:@array)).to be_empty
  end

  it 'supports array operations' do
    allow(@mutex).to receive(:synchronize).and_yield

    expect(@mutexed_array.include?(0)).to be_truthy
    @mutexed_array << 3
    expect(@mutexed_array.include?(3)).to be_truthy
    expect(@mutexed_array.include?(4)).to be_falsy
    expect(@mutexed_array.clear).to be_empty
  end

  it 'synchronizes array operations with mutex' do
    expect(@mutex).to receive(:synchronize).thrice.and_yield

    @mutexed_array << 3
    @mutexed_array.include?(3)
    @mutexed_array.clear
  end

  it 'responds to array operations' do
    expect(@mutexed_array.respond_to?(:include?)).to be_truthy
    expect(@mutexed_array.respond_to?(:clear)).to be_truthy
    expect(@mutexed_array.respond_to?(:nonsense)).to be_falsy
  end

  it 'converts to an array' do
    expect(@mutexed_array.to_a).to eq([0])
    expect(@mutexed_array.to_ary).to eq([0])
  end
end
