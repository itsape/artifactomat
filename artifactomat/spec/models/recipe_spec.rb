# frozen_string_literal: true

require 'artifactomat/models/recipe'
require 'active_record_spec_helper'
require 'artifactomat/models/track_filter'

require 'fakefs/spec_helpers'

describe Recipe do
  include FakeFS::SpecHelpers

  let(:recipe) { build(:recipe) }

  before(:all) do
    FakeFS.activate!
    FakeFS::FileSystem.clear
  end

  after(:each) do
    FakeFS::FileSystem.clear
  end

  after(:all) do
    FakeFS.deactivate!
  end

  it '.new' do
    expect(recipe).to be_instance_of(Recipe)
  end

  it 'responds to id' do
    expect(recipe).to respond_to(:id)
  end

  it 'responds to artifact_script' do
    expect(recipe).to respond_to(:artifact_script)
  end

  it 'responds to infrastructure' do
    expect(recipe).to respond_to(:infrastructure)
  end

  it 'responds to parameters' do
    expect(recipe).to respond_to(:parameters)
  end

  it 'responds to arm_script' do
    expect(recipe).to respond_to(:arm_script)
  end

  it 'responds to disarm_script' do
    expect(recipe).to respond_to(:disarm_script)
  end

  it 'responds to trackfilters' do
    expect(recipe).to respond_to(:trackfilters)
  end

  it 'responds to detection' do
    expect(recipe).to respond_to(:detection)
  end

  describe '::load' do
    let(:recipe) do
      recipe = build(:recipe)
      ie = build(:infrastructure_element)
      tf = build(:track_filter, recipe_id: recipe.id)
      recipe.infrastructure = [ie]
      recipe.trackfilters = [tf]
      recipe
    end

    it 'loads a recipe' do
      File.open('tmp.yml', mode: 'w') { |f| f.write(YAML.dump(recipe)) }
      tmp = Recipe.load('tmp.yml')
      expect(tmp).to be_a Recipe
    end

    it 'raises RuntimeError if no Recipe could be loaded' do
      recipe = 5
      File.open('tmp.yml', mode: 'w') { |f| f.write(YAML.dump(recipe)) }
      expect { Recipe.load('tmp.yml') }.to raise_error
    end

    it 'raises RuntimeError if no real file provided' do
      File.open('weqwerwqer', mode: 'w') { |f| f.write(YAML.dump(recipe)) }
      expect { Recipe.load('tmp.yml') }.to raise_error
    end

    it 'resets id correctly' do
      recipe.id = 'hello'
      File.open('tmp.yml', mode: 'w') { |f| f.write(YAML.dump(recipe)) }
      tmp = Recipe.load('tmp.yml')
      expect(tmp.id).to match('tmp')
    end

    it 'does not mess things up' do
      expect(recipe.errors).to be_empty
      File.open('tmp.yml', mode: 'w') { |f| f.write(YAML.dump(recipe)) }
      tmp = Recipe.load('tmp.yml')
      expect(tmp.errors).to be_empty
    end
  end

  describe '#describe_parameters' do
    it 'returns a Hash' do
      expect(recipe.describe_parameters).to be_a Hash
    end

    it 'includes all parameters' do
      recipe.parameters = { 'some' => 'might be something' }
      recipe.default_parameters = { 'some' => 'thing' }
      desc = recipe.describe_parameters
      expect(desc['some']).to be_a Hash
      expect(desc['some']['description']).to include 'something'
      expect(desc['some']['default']).to include 'thing'
    end
  end

  describe '#detection_parameters' do
    let(:parsed_detection_params) { recipe.detection_parameters }

    it 'keeps all detection entries except for detectors and runtime_information unaltered' do
      detection_params = recipe.instance_variable_get(:@detection)

      detection_params.each do |key, value|
        next if %w[detectors runtime_information].include?(key)

        expect(parsed_detection_params[key]).to eq(value), "params[#{key}] = #{parsed_detection_params[key]} != #{value}"
      end
    end

    it 'transforms detectors in semicolon separated format' do
      detection_params = recipe.instance_variable_get(:@detection)
      # add second detector
      detection_params['detectors'] << {
        'detector_class_name' => 'DetectorTwo',
        'pre_conditions' => 'cond = 1',
        'target_conditions' => 'cond = 2'
      }
      recipe.instance_variable_set(:@detection, detection_params)

      det0 = detection_params['detectors'][0]
      det1 = detection_params['detectors'][1]
      expect(parsed_detection_params['detectors']['0']).to(
        eq("#{det0['detector_class_name']};#{det0['pre_conditions']};#{det0['target_conditions']}")
      )
      expect(parsed_detection_params['detectors']['1']).to(
        eq("#{det1['detector_class_name']};#{det1['pre_conditions']};#{det1['target_conditions']}")
      )
    end

    it 'adds the recipe ID as "artifact_name"' do
      expect(parsed_detection_params['artifact_name']).to eq(recipe.id)
    end

    it 'adds the recipe ID as runtime_information->artifact_name' do
      expect(parsed_detection_params['runtime_information']['artifact_name']).to eq(recipe.id)
    end

    it 'keeps all other runtime_information entries unaltered' do
      detection_params = recipe.instance_variable_get(:@detection)
      detection_params['runtime_information'].each do |key, value|
        expect(parsed_detection_params['runtime_information'][key]).to eq(value)
      end
    end
  end

  describe '#errors' do
    it 'returns an array' do
      expect(recipe.errors).to be_a Array
    end

    it '.id: exist' do
      recipe.instance_variable_set(:@id, nil)
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'id'
    end

    it '.duration: to exist' do
      recipe.instance_variable_set(:@duration, nil)
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'duration'
      expect(recipe.errors.first).to include 'no '
    end

    it '.duration: to be a number' do
      recipe.instance_variable_set(:@duration, 'duration')
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'duration'
      expect(recipe.errors.first).to include 'numeric'
    end

    it '.duration: to be > 1' do
      recipe.instance_variable_set(:@duration, 0.5)
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'duration'
      expect(recipe.errors.first).to include '1'
      expect(recipe.errors.first).to include '>'
    end

    it '.infrastructure: to be an array' do
      recipe.instance_variable_set(:@infrastructure, 'infrastructure')
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'infrastructure'
      expect(recipe.errors.first).to include 'array'
    end

    it '.infrastructure: to be arrray of infrastructure elements' do
      recipe.instance_variable_set(:@infrastructure, ['infrastructure'])
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'infrastructure'
      expect(recipe.errors.first).to include 'InfrastructureElement'
    end

    it '.infrastructure: own errors' do
      ie = build(:infrastructure_element)
      recipe.instance_variable_set(:@infrastructure, [ie])
      expect(ie).to receive(:errors).and_return []
      expect(recipe.errors.count).to eq 0
    end

    it '.trackfilters: to be an array' do
      recipe.instance_variable_set(:@trackfilters, 'track filters')
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'track filter'
      expect(recipe.errors.first).to include 'array'
    end

    it '.trackfilters: to be arrray of track filters' do
      recipe.instance_variable_set(:@trackfilters, ['track filter'])
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'track filter'
      expect(recipe.errors.first).to include 'TrackFilter'
    end

    it '.trackfilters: own errors' do
      tf = build(:track_filter, recipe_id: recipe.id)
      recipe.instance_variable_set(:@trackfilters, [tf])
      expect(tf).to receive(:errors).and_return []
      expect(recipe.errors.count).to eq 0
    end

    it '.trackfilters: recipe id missing' do
      tf = build(:track_filter, recipe_id: nil)
      recipe.instance_variable_set(:@trackfilters, [tf])
      expect(tf).to receive(:errors).and_return []
      e = recipe.errors
      expect(e.count).to eq 1
      expect(e.first).to include 'recipe'
      expect(e.first).to include 'id'
    end

    it '.artifact_script: exist' do
      recipe.instance_variable_set(:@artifact_script, nil)
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'artifact script'
    end

    it '.arm_script: exist' do
      recipe.instance_variable_set(:@arm_script, nil)
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'arm script'
    end

    it '.disarm_script: exist' do
      recipe.instance_variable_set(:@disarm_script, nil)
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'disarm script'
    end

    it '.deploy_script: exist' do
      recipe.instance_variable_set(:@deploy_script, nil)
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'deploy script'
    end

    it '.undeploy_script: exist' do
      recipe.instance_variable_set(:@undeploy_script, nil)
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'undeploy script'
    end

    it '.default_parameters: should not provide default without description' do
      recipe.instance_variable_set(:@default_parameters, some: :thing)
      recipe.instance_variable_set(:@parameters, {})
      expect(recipe.errors.count).to eq 1
      expect(recipe.errors.first).to include 'default parameter'
      expect(recipe.errors.first).to include 'description'
    end

    describe '.detection' do
      it 'is optional' do
        recipe.remove_instance_variable(:@detection)
        expect(recipe.errors.count).to eq 0
      end

      it 'is a hash' do
        recipe.instance_variable_set(:@detection, 'detection')
        expect(recipe.errors.count).to eq 1
        expect(recipe.errors.first).to include 'detection entry \'detection\' is not a hash'
      end

      it 'includes all required keys' do
        detection_params = recipe.instance_variable_get(:@detection)
        keys = %w[detection_interval detectors runtime_information]

        keys.each do |key|
          detection_params.delete(key)
          recipe.instance_variable_set(:@detection, detection_params)
          expect(recipe.errors.count).to be >= 1
          expect(recipe.errors.first).to include "misses #{key}"
          detection_params[key] = ''
        end
      end

      it 'has a detectors array' do
        detection_params = recipe.instance_variable_get(:@detection)
        detection_params['detectors'] = 'not a hash'
        recipe.instance_variable_set(:@detection, detection_params)
        expect(recipe.errors.count).to eq 1
        expect(recipe.errors.first).to include 'detectors \'not a hash\' is not an array'
      end

      it 'includes at least one detector' do
        detection_params = recipe.instance_variable_get(:@detection)
        detection_params['detectors'] = []
        recipe.instance_variable_set(:@detection, detection_params)
        expect(recipe.errors.count).to eq 1
        expect(recipe.errors.first).to include 'detectors \'[]\' is empty'
      end

      it 'includes only detectors with all required keys' do
        detection_params = recipe.instance_variable_get(:@detection)
        %w[target_conditions pre_conditions detector_class_name].each do |key|
          detection_params['detectors'][0].delete(key)
          recipe.instance_variable_set(:@detection, detection_params)
          expect(recipe.errors.count).to eq 1
          expect(recipe.errors.first).to include "misses #{key}"
          expect(recipe.errors.first).to include 'index 0'
          detection_params['detectors'][0][key] = ''
        end
      end
    end
  end
end
