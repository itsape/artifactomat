# frozen_string_literal: true

require 'active_record_spec_helper'
require 'artifactomat/models/test_series'

describe TestSeries, type: :model do
  it { should belong_to(:test_program) }
  it { should have_many(:test_seasons).dependent(:destroy) }
  it { should have_many(:test_episodes) }

  it '#status returns the current status' do
    ts = create(:test_series)
    ts.fresh!
    expect(ts.status).to eq('fresh')
    ts.scheduled!
    expect(ts.status).to eq('scheduled')
    ts.preparing!
    expect(ts.status).to eq('preparing')
    ts.prepared!
    expect(ts.status).to eq('prepared')
    ts.approved!
    expect(ts.status).to eq('approved')
    ts.completed!
    expect(ts.status).to eq('completed')
  end

  it '#completed? correctly tells whether the status is completed' do
    ts = create(:test_series)
    ts.fresh!
    expect(ts.completed?).to be(false)
    ts.scheduled!
    expect(ts.completed?).to be(false)
    ts.preparing!
    expect(ts.completed?).to be(false)
    ts.prepared!
    expect(ts.completed?).to be(false)
    ts.approved!
    expect(ts.completed?).to be(false)
    ts.completed!
    expect(ts.completed?).to be(true)
  end

  it '#approved? correctly tells whether the status is approved' do
    ts = create(:test_series)
    ts.fresh!
    expect(ts.approved?).to be(false)
    ts.scheduled!
    expect(ts.approved?).to be(false)
    ts.preparing!
    expect(ts.approved?).to be(false)
    ts.prepared!
    expect(ts.approved?).to be(false)
    ts.approved!
    expect(ts.approved?).to be(true)
    ts.completed!
    expect(ts.approved?).to be(false)
  end

  describe 'validation' do
    before(:each) do
      @attrs = { id: 0 }
      @attrs['label'] = 'label'
      @attrs['start_date'] = Time.new(2017, 10, 18, 16, 2, 2, '+02:00')
      @attrs['group_file'] = 'group file'
    end

    it 'should only accept Time objects as :start_date' do
      expect { TestSeries.new(@attrs) }.not_to raise_error

      @attrs['start_date'] = 'Not a time object'
      expect { TestSeries.new(@attrs) }.to raise_error(RuntimeError, 'TestSeries#start_date must be a Time object!')
    end

    it 'should validate that :label cannot be empty/falsy' do
      expect(TestSeries.new(@attrs).valid?).to be true

      @attrs['label'] = ''
      expect(TestSeries.new(@attrs).valid?).to be false

      @attrs['label'] = nil
      expect(TestSeries.new(@attrs).valid?).to be false
    end

    it 'should validate that :group_file cannot be empty/falsy' do
      expect(TestSeries.new(@attrs).valid?).to be true

      @attrs['group_file'] = ''
      expect(TestSeries.new(@attrs).valid?).to be false

      @attrs['group_file'] = nil
      expect(TestSeries.new(@attrs).valid?).to be false
    end
  end

  describe 'without test seasons' do
    let(:ts) { create(:test_series, season_count: 0) }

    it '.new' do
      expect(ts).to be_a TestSeries
    end

    it '#test_seasons.count' do
      expect(ts.test_seasons.count).to be 0
    end
  end

  describe 'with test seasons' do
    let(:ts) { create(:test_series, season_count: 3) }

    it '.new' do
      expect(ts).to be_a TestSeries
    end

    it '#test_seasons.count' do
      expect(ts.test_seasons.count).to be 3
    end

    it '#test_seasons are destroyed' do
      seasons = ts.test_seasons
      ts.destroy
      expect(TestSeason.all).not_to include(seasons)
    end

    it '#test_episodes' do
      expect(ts.test_episodes.count).to be 9
    end

    context 'not approved' do
      before(:each) do
        ts.fresh!
      end

      it 'does not update status on #status' do
        expect(ts.status).to eq('fresh')
      end

      it 'does not update status on #completed?' do
        ts.completed?
        expect(ts.status).to eq('fresh')
      end

      it 'does not update status on #approved?' do
        ts.approved?
        expect(ts.status).to eq('fresh')
      end
    end

    context 'approved' do
      before(:each) do
        ts.approved!
      end

      context 'has prepared or running episodes' do
        before(:each) do
          ts.test_episodes.first.running!
        end

        it 'does not update status on #status' do
          expect(ts.status).to eq('approved')
        end

        it 'does not update status on #completed?' do
          ts.completed?
          expect(ts.status).to eq('approved')
        end

        it 'does not update status on #approved?' do
          ts.approved?
          expect(ts.status).to eq('approved')
        end
      end

      context 'all episodes are completed or failed' do
        before(:each) do
          ts.test_episodes.each(&:completed!)
          ts.test_episodes.first.failed!
        end

        it 'does update status on #status' do
          expect(ts.status).to eq('completed')
        end

        it 'does update status on #completed?' do
          ts.completed?
          expect(ts.status).to eq('completed')
        end

        it 'does update status on #approved?' do
          ts.approved?
          expect(ts.status).to eq('completed')
        end
      end
    end
  end

  describe '#scheduled?' do
    let(:ts) do
      test_season = create(:test_season, episode_count: 1)
      create(:test_series, test_seasons: [test_season])
    end

    it 'false on episodes without schedule' do
      expect(ts.scheduled?).to be_falsy
    end

    it 'false without episodes' do
      ts.test_seasons.first.test_episodes.destroy_all
      expect(ts.scheduled?).to be_falsy
    end
  end
end
