# frozen_string_literal: true

require 'active_record_spec_helper'
require 'fakefs/spec_helpers'
require 'artifactomat/models/test_season'

require 'json'
require 'digest'

describe TestSeason, type: :model do
  it { should belong_to(:test_series) }
  it { should have_many(:test_episodes).dependent(:destroy) }
  it { should validate_presence_of(:label) }
  it { should validate_presence_of(:recipe_id) }
  it { should_not allow_value(0).for(:duration) }
  it { should_not allow_value(nil).for(:duration) }
  it { should allow_value(1).for(:duration) }
  it { should allow_value('10').for(:duration) }

  before(:all) do
    @test_hash = { 'one' => 'two' }
  end

  describe 'without test episodes' do
    before(:each) do
      @count = 0
    end

    it '.new' do
      object = create(:test_season, episode_count: @count)
      expect(object).to be_a TestSeason
    end

    it '#test_episodes.count' do
      object = create(:test_season, episode_count: @count)
      episodes = object.test_episodes
      expect(episodes.length).to be @count
    end

    it 'has recipe_parameters' do
      object = create(:test_season, episode_count: @count)
      object.recipe_parameters = @test_hash
      expect { object.save }.to_not raise_error
      expect(object.recipe_parameters).to include(@test_hash)
      @id = object.id
    end

    it 'stores a duration' do
      object = create(:test_season, episode_count: @count, duration: 2)
      expect(object.duration).to be(2)
    end
  end

  describe 'with test episodes' do
    before(:each) do
      @count = 5
    end

    it '.new' do
      object = create(:test_season)
      expect(object).to be_a TestSeason
    end

    it '#test_episodes.count' do
      object = create(:test_season, episode_count: @count)
      episodes = object.test_episodes
      expect(episodes.count).to be @count
    end

    it '#test_episodes are destroyed' do
      object = create(:test_season, episode_count: @count)
      episodes = object.test_episodes
      object.destroy
      expect(TestEpisode.all).not_to include(episodes)
    end
  end

  describe 'detection parameters' do
    before(:each) do
      @recipe = build(:recipe)
      @season = create(:test_season, recipe_id: @recipe.id)

      rm = double('RecipeManager')
      allow(RecipeManager).to receive(:new).and_return(rm)
      allow(rm).to receive(:get_recipe).and_return(@recipe)
      TestSeason.instance_variable_set(:@recipe_manager, rm)
    end

    it 'returns empty object if recipe detection parameters are empty' do
      @recipe.remove_instance_variable(:@detection)
      expect(@season.json_detection_parameters).to eq('{}')
    end

    it 'adds the season id' do
      parsed_params = JSON.parse(@season.json_detection_parameters)
      expect(parsed_params['season_id']).to eq(@season.id)
    end

    describe 'screenshots hash' do
      before(:all) do
        FakeFS.activate!
      end

      after(:all) do
        FakeFS.deactivate!
      end

      before(:each) do
        cm = double('ConfigurationManager')
        allow(ConfigurationManager).to receive(:new).and_return(cm)
        allow(cm).to receive(:cfg_path).and_return('/etc/artifactomat')
        TestSeason.instance_variable_set(:@configuration_manager, cm)
        @screenshots_path = "/etc/artifactomat/recipes/#{@recipe.id}/screenshots"
        FileUtils.mkdir_p(@screenshots_path)
      end

      after(:each) do
        FakeFS::FileSystem.clear
      end

      it 'adds initial MD5 hash if screenshots_folder string is empty' do
        parsed_params = JSON.parse(@season.json_detection_parameters)
        expect(parsed_params['screenshots_hash']).to eq(Digest::MD5.new.hexdigest)
      end

      it 'adds initial MD5 hash if screenshots folder is empty' do
        @season.screenshots_folder = 'screenshots'
        parsed_params = JSON.parse(@season.json_detection_parameters)
        expect(parsed_params['screenshots_hash']).to eq(Digest::MD5.new.hexdigest)
      end

      it 'adds the combined hash of all files in the screenshots directory' do
        File.open(File.join(@screenshots_path, 'fileA'), mode: 'w') { |f| f.write('12345') }
        File.open(File.join(@screenshots_path, 'fileB'), mode: 'w') { |f| f.write('aabbcc') }
        expected = Digest::MD5.new.update('12345').update('aabbcc').hexdigest

        @season.screenshots_folder = 'screenshots'
        parsed_params = JSON.parse(@season.json_detection_parameters)
        expect(parsed_params['screenshots_hash']).to eq(expected)
      end
    end

    describe 'absolute screenshots folder' do
      before(:each) do
        cm = double('ConfigurationManager')
        allow(ConfigurationManager).to receive(:new).and_return(cm)
        allow(cm).to receive(:cfg_path).and_return('/etc/artifactomat')
        TestSeason.instance_variable_set(:@configuration_manager, cm)
      end

      it 'raises if no screenshots folder is set' do
        expect { @season.absolute_screenshots_folder }.to \
          raise_error("Season #{@season.id} has no screenshots")
      end

      it 'returns the absolute screenshots folder if screenshots folder is set' do
        @season.screenshots_folder = 'screenshots'
        expect(@season.absolute_screenshots_folder).to \
          eq("/etc/artifactomat/recipes/#{@recipe.id}/screenshots")
      end
    end

    describe 'screenshots filenames' do
      before(:all) do
        FakeFS.activate!
      end

      after(:all) do
        FakeFS.deactivate!
      end

      before(:each) do
        cm = double('ConfigurationManager')
        allow(ConfigurationManager).to receive(:new).and_return(cm)
        allow(cm).to receive(:cfg_path).and_return('/etc/artifactomat')
        TestSeason.instance_variable_set(:@configuration_manager, cm)
        @screenshots_path = "/etc/artifactomat/recipes/#{@recipe.id}/screenshots"
        @season.screenshots_folder = 'screenshots'
        FileUtils.mkdir_p(@screenshots_path)
        File.open(File.join(@screenshots_path, 'fileA'), mode: 'w') { |f| f.write('12345') }
        File.open(File.join(@screenshots_path, 'fileB'), mode: 'w') { |f| f.write('aabbcc') }
      end

      it 'returns all filenames of screenshots files' do
        expect(@season.screenshots_filenames).to eq(%w[fileA fileB])
      end

      it 'ignores all non-file entries' do
        FileUtils.mkdir_p(File.join(@screenshots_path, 'someDir'))
        expect(@season.screenshots_filenames).to eq(%w[fileA fileB])
      end
    end
  end
end
