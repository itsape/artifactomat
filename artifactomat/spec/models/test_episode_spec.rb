# frozen_string_literal: true

require 'active_record_spec_helper'
require 'artifactomat/models/test_episode'

describe TestEpisode, type: :model do
  it { should belong_to(:test_season) }
  it { should validate_presence_of(:subject_id) }
  it { should have_db_column(:status) }

  before(:all) do
    @test_hash = { 'one' => 'two' }
  end

  context 'status' do
    before(:each) do
      @status = %i[prepared running completed failed]
    end

    it { should define_enum_for(:status).with(@status) }
  end

  it '.new' do
    object = create(:test_episode)
    expect(object).to be_a TestEpisode
  end

  describe '#seconds_exposed' do
    before(:each) do
      # sets up everything such that the episode has an exposure time of 5 minutes if no changes to
      # the episode or subject history are made
      cm = double('ConfigurationManager')
      @subject = build(:subject)
      @now = Time.now
      @series = create(:test_series)
      season = create(:test_season, test_series_id: @series.id)
      @episode = build(:test_episode, subject_id: @subject.id, test_season_id: season.id)
      @episode.schedule_start = @now - 1000
      @episode.schedule_end = @now - 100 # 15 minute schedule length
      @episode.completed!
      @episode.save

      @session = SubjectHistory.create(test_series_id: @series.id,
                                       subject_id: @subject.id,
                                       ip: '127.0.0.42',
                                       session_begin: @now - 500,
                                       session_end: @now - 200, # fully contained inside ep for 5 minutes
                                       submit_time: @now)

      allow(cm).to receive(:get_subject).with(@subject.id).and_return(@subject)
    end

    after(:each) do
      SubjectHistory.destroy_all
      @episode.destroy
    end

    it 'correctly calculates exposure of fully contained session' do
      expect(@episode.seconds_exposed).to eq(5.minutes)
    end

    it 'correctly calculates exposure for whole schedule spanning session' do
      @session.session_begin = @episode.schedule_start - 5.minutes
      @session.session_end = @episode.schedule_end + 1.minute
      @session.save

      expect(@episode.seconds_exposed).to eq(15.minutes)
    end

    it 'correctly calculates exposure of session starting during schedule and ending after schedule' do
      @session.session_begin = @episode.schedule_end - 3.minutes
      @session.session_end = @episode.schedule_end + 1.minute
      @session.save

      expect(@episode.seconds_exposed).to eq(3.minutes)
    end

    it 'correctly calculates exposure of session starting before schedule and ending inside schedule' do
      @session.session_begin = @episode.schedule_start - 5.minutes
      @session.session_end = @episode.schedule_start + 3.minutes
      @session.save

      expect(@episode.seconds_exposed).to eq(3.minutes)
    end

    it 'correctly calculates exposure of multiple sessions' do
      @session.session_begin = @episode.schedule_start - 1.minute
      @session.session_end = @episode.schedule_start + 1.minute # 1 minute exposure
      @session.save

      SubjectHistory.create(test_series_id: @series.id,
                            subject_id: @subject.id,
                            ip: '127.0.0.42',
                            session_begin: @episode.schedule_start + 2.minutes,
                            session_end: @episode.schedule_start + 4.minutes, # 2 minutes exposure
                            submit_time: @now)

      SubjectHistory.create(test_series_id: @series.id,
                            subject_id: @subject.id,
                            ip: '127.0.0.42',
                            session_begin: @episode.schedule_end - 3.minutes,
                            session_end: @episode.schedule_end + 1.minutes, # 3 minutes exposure
                            submit_time: @now)

      expect(@episode.seconds_exposed).to eq(6.minutes)
    end

    it 'shows no exposure if episode is prepared' do
      @episode.prepared!

      expect(@episode.seconds_exposed).to eq(0)
    end

    it 'shows no exposure if episode is failed' do
      @episode.failed!

      expect(@episode.seconds_exposed).to eq(0)
    end

    it 'shows no exposure if episode has not been scheduled' do
      @episode.schedule_start = nil
      @episode.schedule_end = nil
      @episode.save

      expect(@episode.seconds_exposed).to eq(0)
    end

    it 'shows no exposure if there is no recorded SubjectHistory' do
      SubjectHistory.delete_all

      expect(@episode.seconds_exposed).to eq(0)
    end
  end
end
