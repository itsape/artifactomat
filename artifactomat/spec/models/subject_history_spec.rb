# frozen_string_literal: true

require 'active_record_spec_helper'
require 'artifactomat/models/subject_history'

describe SubjectHistory, type: :model do
  it { should belong_to(:test_series) }
  it { should validate_presence_of(:session_begin) }
  it { should validate_presence_of(:submit_time) }
  it { should validate_presence_of(:ip) }
  it { should validate_presence_of(:subject_id) }

  describe 'without SubjectHistory' do
    before(:each) do
      @count = 0
    end

    it '.new' do
      object = create(:subject_history)
      expect(object).to be_a SubjectHistory
    end
  end
end
