# frozen_string_literal: true

require 'active_record_spec_helper'
require 'artifactomat/models/test_program'

describe TestProgram, type: :model do
  it { should have_many(:test_series).dependent(:destroy) }
  it { should validate_presence_of(:label) }

  describe 'without test series' do
    before(:each) do
      @count = 0
    end

    it '.new' do
      object = create(:test_program, series_count: @count)
      expect(object).to be_a TestProgram
    end

    it '#test_series.count' do
      object = create(:test_program, series_count: @count)
      series = object.test_series
      expect(series.count).to be @count
    end

    describe 'with test series' do
      before(:each) do
        @count = 5
      end

      it '.new' do
        object = create(:test_program, series_count: @count)
        expect(object).to be_a TestProgram
      end

      it '#test_series.count' do
        object = create(:test_program, series_count: @count)
        series = object.test_series
        expect(series.count).to be @count
      end

      it '#test_series are destroyed' do
        object = create(:test_program, series_count: @count)
        series = object.test_series
        object.destroy
        expect(TestSeries.all).not_to include(series)
      end
    end
  end
end
