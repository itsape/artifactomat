# frozen_string_literal: true

require 'artifactomat/models/volatile_stored_set'
require 'json'

# Mock class for a redis stored object
class DataClass
  attr_reader :data

  def initialize(data)
    @data = data
  end

  # needed to make #eql? work properly
  def hash
    [@data].hash
  end

  def ==(other)
    @data == other.data
  end

  def eql?(other)
    @data == other.data
  end

  def size
    @data.size
  end

  def to_json(*)
    @data.to_json
  end

  def self.from_json(json)
    DataClass.new(JSON.parse(json))
  end
end

describe VolatileStoredSet do
  before(:each) do
    @conn = double('Connection')
    @vstore = double('VStore')
    @class_name = 'LongTermMemory'
    @variable_name = 'subjects'
    @redis_varname = 'LongTermMemory__subjects'
  end

  context 'no debug options' do
    before(:each) do
      @options = {}
      allow(@vstore).to receive(:with_state_saving_connection).and_yield(@conn)
      allow(@conn).to receive(:smembers).with(@redis_varname).and_return(['[1,2,3]', '[4,5,6,7]'])
      @vss = VolatileStoredSet.new(@class_name, @variable_name, @vstore, DataClass, @options)
    end

    it 'initializes from redis' do
      expect(@vstore).to receive(:with_state_saving_connection).and_yield(@conn)
      expect(@conn).to receive(:smembers).with(@redis_varname).and_return(['[1,2,3]', '[4,5,6,7]'])
      vss = VolatileStoredSet.new(@class_name, @variable_name, @vstore, DataClass, @options)

      expected = Set[DataClass.new([1, 2, 3]), DataClass.new([4, 5, 6, 7])]

      expect(vss.instance_variable_get(:@set)).to eq(expected)
    end

    describe '#count' do
      it 'returns number of elements in redis' do
        expect(@vss.count).to eq(2)
      end

      it 'does not read from redis' do
        expect(@conn).not_to receive(:smembers)
        @vss.count
      end
    end

    describe '#include?' do
      it 'returns true if the element is in the set' do
        expect(@vss.include?(DataClass.new([1, 2, 3]))).to be_truthy
      end

      it 'returns false if the element is not in the set' do
        expect(@vss.include?(DataClass.new([0, 0, 0]))).to be_falsy
      end

      it 'does not read from redis' do
        expect(@conn).not_to receive(:smembers)
        @vss.include?(DataClass.new([1, 2, 3]))
      end
    end

    describe '#<<' do
      before(:each) do
        @data = DataClass.new([8, 9, 10])

        allow(@conn).to receive(:sadd).with(@redis_varname, @data.to_json)
      end

      it 'adds the given element to redis' do
        expect(@conn).to receive(:sadd).with(@redis_varname, @data.to_json)

        @vss << @data
      end

      it 'adds the given element to the set' do
        @vss << @data

        expect(@vss.include?(@data)).to be_truthy
      end

      it 'does not read from redis' do
        expect(@conn).not_to receive(:smembers)

        @vss << @data
      end
    end

    describe '#any?' do
      it 'returns true if block returns true for any member of the set' do
        # the DataClass#size returns the size of their @data array!
        expect(@vss.any? { |elem| elem.size > 1 }).to be_truthy
      end

      it 'returns false if block does not return true for any member of the set' do
        # the DataClass#size returns the size of their @data array!
        expect(@vss.any? { |elem| elem.size > 10 }).to be_falsy
      end

      it 'does not read from redis' do
        expect(@conn).not_to receive(:smembers)

        @vss.any? { |elem| elem.size > 10 }
      end
    end

    describe '#delete' do
      before(:each) do
        @elem = DataClass.new([1, 2, 3])

        allow(@conn).to receive(:srem).with(@redis_varname, @elem.to_json)
      end

      it 'removes element from the set' do
        expect(@vss.include?(@elem)).to be_truthy

        @vss.delete(@elem)

        expect(@vss.include?(@elem)).not_to be_truthy
      end

      it 'removes element from redis' do
        expect(@conn).to receive(:srem).with(@redis_varname, @elem.to_json)

        @vss.delete(@elem)
      end

      it 'does not read from redis' do
        expect(@conn).not_to receive(:smembers)

        @vss.delete(@elem)
      end
    end

    describe '#select' do
      it 'correctly selects items from the set' do
        expected = [DataClass.new([4, 5, 6, 7])]
        expect(@vss.select { |elem| elem.size > 3 }).to eq(expected)

        expected = []
        expect(@vss.select { |elem| elem.size > 4 }).to eq(expected)

        expected = [DataClass.new([1, 2, 3]), DataClass.new([4, 5, 6, 7])]
        expect(@vss.select { |elem| elem.size > 2 }.to_set).to eq(expected.to_set)
      end

      it 'does not read from redis' do
        expect(@conn).not_to receive(:smembers)

        @vss.select { |elem| elem.size > 2 }
      end
    end

    describe '#map' do
      it 'returns array with mapped elements' do
        expect(@vss.map(&:size)).to eq([3, 4])
      end

      it 'does not read from redis' do
        expect(@conn).not_to receive(:smembers)

        @vss.map(&:size)
      end
    end

    describe '#each' do
      it 'executes block for each element' do
        executed_with = []
        @vss.each do |elem|
          executed_with << elem
        end

        expect(executed_with.include?(DataClass.new([1, 2, 3]))).to be_truthy
        expect(executed_with.include?(DataClass.new([4, 5, 6, 7]))).to be_truthy
      end

      it 'does not read from redis' do
        expect(@conn).not_to receive(:smembers)

        @vss.each(&:to_json)
      end
    end
  end

  context 'debug reread from redis' do
    before(:each) do
      @options = { debug_reread_from_redis: true }
      allow(@vstore).to receive(:with_state_saving_connection).and_yield(@conn)
      allow(@conn).to receive(:smembers).with(@redis_varname).and_return(['[1,2,3]', '[4,5,6,7]'])
      @vss = VolatileStoredSet.new(@class_name, @variable_name, @vstore, DataClass, @options)
    end

    it '#count reads from redis again' do
      expect(@conn).to receive(:smembers).with(@redis_varname).and_return(['[8,9,10]'])
      expect(@vss.count).to eq(1)
    end

    it '#<< reads from redis again' do
      data = DataClass.new([11, 12])
      allow(@conn).to receive(:sadd)
      expect(@conn).to receive(:smembers).with(@redis_varname).and_return(['[8,9,10]'])

      @vss << data

      expect(@vss.instance_variable_get(:@set)).to eq(Set[DataClass.new([8, 9, 10]), data])
    end

    it '#include? reads from redis again' do
      expect(@conn).to receive(:smembers).with(@redis_varname).and_return(['[8,9,10]']).twice

      expect(@vss.include?(DataClass.new([8, 9, 10]))).to be_truthy
      expect(@vss.include?(DataClass.new([1, 2, 3]))).to be_falsy
    end

    it '#delete reads from redis again' do
      allow(@conn).to receive(:srem)
      expect(@conn).to receive(:smembers).with(@redis_varname).and_return(['[8,9,10]'])

      @vss.delete(DataClass.new([8, 9, 10]))

      expect(@vss.instance_variable_get(:@set)).to be_empty
    end

    it '#any? reads from redis again' do
      expect(@conn).to receive(:smembers).with(@redis_varname).and_return(['[8,9]'])

      expect(@vss.any? { |elem| elem.size == 2 }).to be_truthy
    end

    it '#select reads from redis again' do
      expect(@conn).to receive(:smembers).with(@redis_varname).and_return(['[8,9]'])

      expect(@vss.select { |elem| elem.size == 2 }).to eq([DataClass.new([8, 9])])
    end

    it '#map reads from redis again' do
      expect(@conn).to receive(:smembers).with(@redis_varname).and_return(['[8,9]'])

      expect(@vss.map(&:size)).to eq([2])
    end

    it '#each reads from redis again' do
      expect(@conn).to receive(:smembers).with(@redis_varname).and_return(['[8,9]'])

      elems = []
      @vss.each do |elem|
        elems << elem
      end

      expect(elems).to eq([DataClass.new([8, 9])])
    end

    it 'raises if ignore redis is set as well' do
      @options[:debug_ignore_redis] = true
      expect do
        VolatileStoredSet.new(@class_name, @variable_name, @vstore, DataClass, @options)
      end.to raise_error('configuration error; you cannot ignore redis but reread from it')
    end
  end

  context 'debug ignore redis' do
    before(:each) do
      @options = { debug_ignore_redis: true }
      @vss = VolatileStoredSet.new(@class_name, @variable_name, @vstore, DataClass, @options)
    end

    it 'does not initialize from redis' do
      expect(@vstore).not_to receive(:with_state_saving_connection)
      expect(@conn).not_to receive(:smembers)

      VolatileStoredSet.new(@class_name, @variable_name, @vstore, DataClass, @options)
    end

    it '#<< does not write to redis' do
      expect(@vstore).not_to receive(:with_state_saving_connection)
      expect(@conn).not_to receive(:sadd)
      data = DataClass.new([1, 2, 3])

      @vss << data

      expect(@vss.include?(data)).to be_truthy
    end

    it '#delete does not write to redis' do
      expect(@vstore).not_to receive(:with_state_saving_connection)
      expect(@conn).not_to receive(:srem)
      data = DataClass.new([1, 2, 3])

      @vss << data
      @vss.delete(data)

      expect(@vss.include?(data)).to be_falsy
    end

    it 'raises if reread from redis is set as well' do
      @options[:debug_reread_from_redis] = true
      expect do
        VolatileStoredSet.new(@class_name, @variable_name, @vstore, DataClass, @options)
      end.to raise_error('configuration error; you cannot ignore redis but reread from it')
    end
  end
end
