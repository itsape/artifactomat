# frozen_string_literal: true

require 'active_record_spec_helper'
require 'artifactomat/models/job'

describe Job, type: :model do
  let(:job) { build(:job) }

  describe '.user' do
    it 'stores a string' do
      expect(job.user).not_to be_empty
      expect(job.user).to be_a String
    end
  end

  describe '#expired?' do
    it 'is not expired by default' do
      expect(job.expired?).to be_falsy
    end

    it 'is expired if expiration_date is old' do
      job.expiration_date = Time.now - 2
      expect(job.expired?).to be_truthy
    end
  end

  describe '#to_json' do
    let(:jsonjob) { JSON(job.to_json) }

    it 'returns json' do
      expect(jsonjob['id']).to eq(job.id)
    end

    it 'contains the user' do
      expect(jsonjob['user']).to eq(job.user)
    end

    it 'contains the status' do
      expect(jsonjob['status']).to eq(job.status)
    end

    it 'contains the action' do
      expect(jsonjob['action']).to eq(job.action)
    end

    it 'contains the file_path' do
      expect(jsonjob['file_path']).to eq(job.file_path)
    end

    it 'contains the deploy_path' do
      expect(jsonjob['deploy_path']).to eq(job.deploy_path)
    end

    it 'contains the parsed watch_arguments' do
      expect(jsonjob['watch_arguments']).to eq(JSON.parse(job.watch_arguments))
    end

    it 'contains the creation date' do
      jcd = job.creation_date
      expected_date = /#{jcd.strftime('%Y-%m-%d')}T#{jcd.strftime('%H:%M:%S')}/
      expect(jsonjob['creation_date']).to match(expected_date)
    end
  end
end
