# frozen_string_literal: true

require 'artifactomat/models/action'

describe Action do
  it 'initializes' do
    now = Time.now.round
    action = Action.new(4, '10.0.0.1', now)

    expect(action.userid).to eq(4)
    expect(action.ip).to eq('10.0.0.1')
    expect(action.submit_time).to eq(now)
    expect(action.end_session?).to be_falsy
    expect(action.session_end).to be_nil
  end

  it 'allows assigning a session end' do
    now = Time.now.round
    action = Action.new(4, '10.0.0.1', now)
    action.session_end = now - 180

    expect(action.session_end).to eq(now - 180)
  end

  it 'tells if session has an end' do
    now = Time.now.round
    action = Action.new(4, '10.0.0.1', now)

    expect(action.end_session?).to eq(false)

    action.session_end = now - 180
    expect(action.end_session?).to eq(true)
  end

  it 'converts to string' do
    now = Time.now.round
    action = Action.new(4, '10.0.0.1', now)

    expect(action.to_s).to eq("<Action: userid=4 subject_id=N/A ip=10.0.0.1 submit_time=#{now} session_start=N/A session_end=N/A>")

    action = Action.new(4, '10.0.0.1', now)
    action.subject_id = 'subject.csv/5'
    action.session_start = now - 360
    action.session_end = now - 180

    expected = "<Action: userid=4 subject_id=subject.csv/5 ip=10.0.0.1 submit_time=#{now} session_start=#{now - 360} session_end=#{now - 180}>"
    expect(action.to_s).to eq(expected)
  end

  it 'serializes to json' do
    now = Time.now.round
    session_end = now - 180
    session_start = now - 300
    userid = '4'
    ip = '10.0.1.13'
    subject_id = 'subjects.csv/1'

    action = Action.new(userid, ip, now)
    action.subject_id = subject_id
    action.session_start = session_start
    action.session_end = session_end

    expected = JSON.generate(userid: userid, ip: ip, submit_time: now,
                             session_end: session_end, session_start: session_start,
                             subject_id: subject_id, end_session: true)
    action_json = action.to_json

    # can't compare json directly because order of key:value pairs is not guaranteed.
    # => parse both again and compare those. If the parsed data is equal it's good enough.
    parsed_expected = JSON.parse(expected)
    parsed_action = JSON.parse(action_json)
    expect(parsed_expected).to eq(parsed_action)
  end

  describe 'create from datastructure' do
    it '#from_json' do
      now = Time.now.round
      session_end = now - 180
      session_start = now - 300
      userid = '4'
      ip = '10.0.1.13'
      subject_id = 'subjects.csv/1'
      end_session = false

      action_hash = { userid: userid, ip: ip, submit_time: now,
                      session_end: session_end, session_start: session_start,
                      subject_id: subject_id, end_session: end_session }
      action_json = action_hash.to_json

      expected = Action.new(userid, ip, now)
      expected.session_end = session_end
      expected.session_start = session_start
      expected.subject_id = subject_id
      expected.instance_variable_set(:@end_session, end_session)

      expect(Action.from_json(action_json)).to eq(expected)
    end

    it '#from_hash' do
      now = Time.now.round
      session_end = now - 180
      session_start = now - 300
      userid = '4'
      ip = '10.0.1.13'
      subject_id = 'subjects.csv/1'
      end_session = false

      action_hash = { userid: userid, ip: ip, submit_time: now,
                      session_end: session_end, session_start: session_start,
                      subject_id: subject_id, end_session: end_session }

      expected = Action.new(userid, ip, now)
      expected.session_end = session_end
      expected.session_start = session_start
      expected.subject_id = subject_id
      expected.instance_variable_set(:@end_session, end_session)

      expect(Action.from_hash(action_hash)).to eq(expected)
    end
  end
end
