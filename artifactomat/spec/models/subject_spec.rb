# frozen_string_literal: true

require 'artifactomat/models/subject'

describe 'Subject' do
  before(:all) do
    @subject = build(:subject)
    @subject.add_field('custom')
    @subject.send('custom='.to_sym, 'custom')
  end

  it '.new' do
    expect(@subject).to be_instance_of(Subject)
  end

  it 'responds to id' do
    expect(@subject).to respond_to(:id)
  end

  it 'responds to name' do
    expect(@subject).to respond_to(:name)
  end

  it 'responds to surname' do
    expect(@subject).to respond_to(:surname)
  end

  it 'responds to userid' do
    expect(@subject).to respond_to(:userid)
  end

  it 'responds to email' do
    expect(@subject).to respond_to(:email)
  end

  it 'responds to custom fields' do
    expect(@subject).to respond_to(:custom)
  end

  describe '#match?({key: value})' do
    it 'returns true on correct field' do
      expect(@subject.match?(name: @subject.name)).to be_truthy
      expect(@subject.match?('name' => @subject.name)).to be_truthy
    end

    it 'returns false on incorrect value' do
      expect(@subject.match?(name: 'jibber')).to be_falsy
      expect(@subject.match?('name' => 'jabber')).to be_falsy
    end

    it 'returns false on incorrect key' do
      expect(@subject.match?(jibber: @subject.name)).to be_falsy
      expect(@subject.match?('jabber' => @subject.name)).to be_falsy
    end
  end

  describe '#add_field' do
    it 'adds an attribute' do
      expect(@subject).not_to respond_to(:not_there)
      @subject.add_field('not_there')
      @subject.not_there = 'there now'
      expect(@subject.not_there).to match('there now')
    end
  end

  describe '.from_hash' do
    it 'casts a Subject from an array' do
      h = { 'id' => 'test/1',
            'name' => 'Max',
            'surname' => 'Muster',
            'email' => 'max@test.com',
            'userid' => 'maxi' }
      s = Subject.from_hash(h)
      expect(s).to be_a(Subject)
    end
  end

  describe '==' do
    it 'works on attributes only' do
      h = { 'id' => 'test/1',
            'name' => 'Max',
            'surname' => 'Muster',
            'email' => 'max@test.com',
            'userid' => 'maxi' }
      s = Subject.from_hash(h)
      s2 = Subject.from_hash(h)
      expect(s == s2).to be_truthy
    end
  end

  describe '#to_hash' do
    it 'returns all attributes' do
      sh = @subject.to_hash
      expect(sh['id']).to match(@subject.id)
      expect(sh['name']).to match(@subject.name)
      expect(sh['surname']).to match(@subject.surname)
      expect(sh['userid']).to match(@subject.userid)
      expect(sh['email']).to match(@subject.email)
      expect(sh['custom']).to match(@subject.custom)
    end
  end
end
