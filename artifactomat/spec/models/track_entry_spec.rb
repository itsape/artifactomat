# frozen_string_literal: true

require 'active_record_spec_helper'
require 'artifactomat/models/track_entry'

describe TrackEntry, type: :model do
  it { should validate_presence_of(:test_episode) }
  it { should validate_presence_of(:subject_id) }
  it { should validate_presence_of(:score) }
  it { should validate_presence_of(:course_of_action) }
  it { should validate_presence_of(:timestamp) }

  it '.new' do
    object = create(:track_entry)
    expect(object).to be_a TrackEntry
  end
end
