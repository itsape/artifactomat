# frozen_string_literal: true

require 'active_record_spec_helper'
require 'artifactomat/models/track_filter'

describe TrackFilter, type: :model do
  it '.new' do
    object = build(:track_filter)
    expect(object).to be_a TrackFilter
  end

  describe '==' do
    it 'excludes @recipe_id' do
      tf1 = build(:track_filter)
      tf2 = tf1.clone
      tf2.recipe_id = 'hello'
      expect(tf1 == tf2).to be_truthy
    end
  end

  describe '#errors' do
    let(:subject) { build(:track_filter) }

    it 'returns an array' do
      expect(subject.errors).to be_a Array
    end

    it 'checks for course_of_action' do
      subject.course_of_action = nil
      expect(subject.errors.count).to eq 1
      expect(subject.errors.first).to include 'action'
    end

    it 'checks for extraction_pattern set' do
      subject.extraction_pattern = nil
      expect(subject.errors.count).to eq 1
      expect(subject.errors.first).to include 'extraction'
    end

    it 'checks for extraction_pattern to be an array' do
      subject.extraction_pattern = /\[(?<timestamp>[0-9]+)\] some action (?<subject>.+)/
      expect(subject.errors.count).to eq 1
      expect(subject.errors.first).to include 'extraction'
      expect(subject.errors.first).to include 'array'
    end

    it 'checks for extraction_pattern parsing a timestamp' do
      subject.extraction_pattern = [/some action (?<subject>.+)/]
      expect(subject.errors.count).to eq 1
      expect(subject.errors.first).to include 'extraction'
      expect(subject.errors.first).to include 'timestamp'
    end

    it 'checks for extraction_pattern parsing a subject identifier' do
      subject.extraction_pattern = [/\[(?<timestamp>[0-9]+)\] some action/]
      expect(subject.errors.count).to eq 1
      expect(subject.errors.first).to include 'extraction'
      expect(subject.errors.first).to include 'identifier'
    end

    it 'checks for pattern to exist' do
      subject.pattern = nil
      expect(subject.errors.count).to eq 1
      expect(subject.errors.first).to include 'pattern'
    end

    it 'checks for pattern to be a rexexp' do
      subject.pattern = '.*'
      expect(subject.errors.count).to eq 1
      expect(subject.errors.first).to include 'pattern'
      expect(subject.errors.first).to include 'regexp'
    end
  end
end
