# frozen_string_literal: true

require 'artifactomat/modules/track_collector'
require_relative '../dist/etc/artifactomat/satellite'
require_relative 'support/openssl_helper'

describe TrackCollector do
  TC_CONN_IP = 'localhost'
  TC_CONN_PORT = 36_883

  def get_filename(path)
    File.join(File.dirname(__FILE__),
              '../dist/etc/artifactomat/certs/',
              path)
  end

  def get_content(path)
    File.open(get_filename(path))
  end

  before(:each) do
    @subject_manager = double('SubjectManager')
    @recipe_manager = double('RecipeManager')
    @configuration_manager = double('ConfigManager')
    allow(@configuration_manager).to receive(:get).with('trackcollector.ip') do
      'localhost'
    end
    allow(@configuration_manager).to receive(:get).with('trackcollector.offset') do
      '60'
    end
    allow(@configuration_manager).to receive(:get).with('trackcollector.port') do
      '36883'
    end
    allow(@configuration_manager).to receive(:get).with('trackcollector.certificate') do
      'public/server.itsape.pem'
    end
    allow(@configuration_manager).to receive(:get).with('trackcollector.private_key') do
      'private/server.itsape.key'
    end
    allow(@configuration_manager).to receive(:get).with('trackcollector.path_ca_certificate_chain') do
      'public/ca.pem'
    end
    allow(@configuration_manager).to receive(:get).with('trackcollector.client_ca_chain') do
      'public/ca.pem'
    end
    allow(@configuration_manager).to receive(:absolution) do |path|
      File.join(File.dirname(__FILE__),
                '../dist/etc/artifactomat/certs/', path)
    end

    @track_collector = TrackCollector.new(@configuration_manager, @recipe_manager, @subject_manager)
    sleep 0.1 until @track_collector.running?
    @satconf = { host: 'localhost', port: @track_collector.port,
                 cert: get_filename('public/server.itsape.pem'),
                 key: get_filename('private/server.itsape.key'),
                 ca: get_filename('public/ca.pem'), polling: 1,
                 file: Tempfile.new('track_collector.log'),
                 bsize: 5 }
  end

  after(:each) do
    @track_collector.stop_server if @track_collector.running?
    TrackEntry.delete_all
    TestEpisode.delete_all
    TestSeason.delete_all
    TestSeries.delete_all
    TestProgram.delete_all
  end

  describe '#status' do
    it 'returns a hash' do
      expect(@track_collector.status).to be_a(Hash)
    end

    it 'tells the status of the server' do
      expect(@track_collector.status['Server']['Running?']).to match('true')
      @track_collector.stop_server
      expect(@track_collector.status['Server']['Running?']).to match('false')
    end

    it 'tells the port of the server' do
      expect(@track_collector.status['Server']['Port']).to match(36_883)
    end

    it 'tells the number of seasons registered' do
      @recipe = build(:recipe)
      allow(@recipe_manager).to receive(:get_recipe).with(@recipe.id) { @recipe }
      @season = create(:test_season, episode_count: 0, recipe_id: @recipe.id)
      @series = create(:test_series, season_count: 0, test_seasons: [@season])
      expect(@track_collector.status['Registered Seasons']).to match(0)
      @track_collector.register_series(@series)
      expect(@track_collector.status['Registered Seasons']).to match(1)
    end
  end

  context 'server' do
    describe '#running?' do
      it 'returns true if the the server is running' do
        expect(@track_collector.running?).to be_truthy
      end

      it 'returns false if the server is stopped' do
        @track_collector.stop_server
        expect(@track_collector.running?).to be false
      end
    end

    it 'accepts connections' do
      @client = Satellite.new(@satconf)
      @client.build_connection
    end

    it 'declines invalid certificates' do
      # set server cert and key file
      key = OpenSSL::PKey::RSA.new(2048)
      cert = build_unsigned_cert(key)
      ssl_socket = build_ssl_sock key, cert, TC_CONN_IP, TC_CONN_PORT

      ssl_socket.connect
      expect { ssl_socket.gets }.to raise_error(OpenSSL::SSL::SSLError, /tlsv1 alert unknown ca/)
      ssl_socket.close
    end

    it 'declines expired certificate' do
      # read valid ca-key
      ca_key = load_valid_ca_key
      ca = load_valid_ca

      # create new keypair, request and sign with valid key not_after -2 days
      key = OpenSSL::PKey::RSA.new(2048)
      cert = build_signed_cert(key, ca_key, ca, Time.now - 360.days, Time.now - 2.days)
      ssl_socket = build_ssl_sock key, cert, TC_CONN_IP, TC_CONN_PORT

      ssl_socket.connect
      expect { ssl_socket.gets }.to raise_error(OpenSSL::SSL::SSLError, /sslv3 alert certificate expired/)
      ssl_socket.close
    end
  end

  context 'Satellite' do
    before(:each) do
      @logfile = @satconf[:file]
      @satellite = Satellite.new(@satconf)
    end

    it 'is startable' do
      satellite_path = File.join(File.dirname(__FILE__), '../dist/etc/artifactomat/satellite.rb')
      satellite_thread = Thread.new { `ruby #{satellite_path} -f "/dev/null" --host "localhost" --port #{@track_collector.port} --ca #{get_filename('public/ca.pem')} --pkey #{get_filename('private/server.itsape.key')} --cert #{get_filename('public/server.itsape.pem')}` }
      sleep 0.01
      expect(satellite_thread.status).to be_truthy
      satellite_thread.exit
    end

    it 'sends already existing lines from observed file at startup' do
      initial_lines = ["initial line 1\n", "another line\n"]
      File.open(@logfile, mode: 'a') do |file|
        file.write(initial_lines.join)
      end

      expect(@satellite).to receive(:send_batch).with(initial_lines).and_return(initial_lines.join.bytesize)
      allow(@satellite).to receive(:loop).and_yield
      allow(@satellite).to receive(:sleep).with(1)
      @satellite.watchlog
      expect(@satellite.instance_variable_get(:@last_logpos)).to eql(initial_lines.join.bytesize)
    end

    it 'sends newly added lines from observed file' do
      allow(@satellite).to receive(:loop).and_yield
      allow(@satellite).to receive(:sleep).with(1)
      @satellite.watchlog
      sleep 0.01

      new_line = "this is a new line\n"
      expect(@satellite).to receive(:send_batch).with([new_line]).and_return(new_line.bytesize)
      File.open(@logfile, mode: 'a') do |file|
        file.write(new_line)
      end
      @satellite.watchlog
      sleep 0.01

      another_line = "and another one\n"
      expect(@satellite).to receive(:send_batch).with([another_line]).and_return(another_line.bytesize)
      File.open(@logfile, mode: 'a') do |file|
        file.write(another_line)
      end
      @satellite.watchlog
      expect(@satellite.instance_variable_get(:@last_logpos)).to eql(@logfile.size)
    end

    it 'resends after failing to send' do
      allow(@satellite).to receive(:loop).and_yield
      allow(@satellite).to receive(:sleep).with(1)

      resend_line = "this should get resend\n"
      File.open(@logfile, mode: 'a') do |file|
        file.write(resend_line)
      end
      logpos = @logfile.size - resend_line.bytesize

      expect(@satellite).to receive(:puts).once
      expect(@satellite).to receive(:send_batch).with([resend_line]).and_return(0)
      expect(@satellite).to receive(:send_batch).with([resend_line]).and_return(resend_line.bytesize)
      @satellite.watchlog

      expect(@satellite.instance_variable_get(:@last_logpos)).to eql(logpos)
      @satellite.watchlog
      expect(@satellite.instance_variable_get(:@last_logpos)).to eql(@logfile.size)
    end

    it 'does not crash on a failed connection' do
      allow(@satellite).to receive(:loop).and_yield
      allow(@satellite).to receive(:sleep).with(1)
      allow(@satellite).to receive(:build_connection).and_return(nil)

      File.open(@logfile, mode: 'a') do |file|
        file.write("This should be sent\n")
      end

      expect(@satellite).to receive(:puts).with(/\[ERROR\] Could not send log entry:/).once
      expect(@satellite).to receive(:puts).with(/\[Satellite\] ERROR: Could not send all log entries./).once
      expect { @satellite.watchlog }.not_to raise_error
    end

    it 'respects the batch size' do
      allow(@satellite).to receive(:loop).and_yield
      allow(@satellite).to receive(:sleep).with(1)

      new_lines = (1..10).map { |n| "new line #{n}\n" }
      File.open(@logfile, mode: 'a') do |file|
        file.puts new_lines
      end
      expect(@satellite).to receive(:send_batch).with(new_lines[0..4]).and_return(new_lines[0..4].join.bytesize)
      expect(@satellite).to receive(:send_batch).with(new_lines[5..]).and_return(new_lines[5..].join.bytesize)
      @satellite.watchlog
    end

    it 'sends to TC' do
      new_line = "this is a new line\n"
      File.open(@logfile, mode: 'a') do |file|
        file.write(new_line)
      end

      allow(@satellite).to receive(:loop).and_yield
      allow(@satellite).to receive(:sleep).with(1)
      expect(@track_collector).to receive(:parse_trace).with('trace' => new_line.strip)
      @satellite.watchlog
    end
  end

  context 'parsing' do
    before(:each) do
      @recipe = build(:recipe)
      allow(@recipe_manager).to receive(:get_recipe).with(@recipe.id) { @recipe }
      @subject = build(:subject)
      # set schedule start date to past and end date to future
      @episode = create(:test_episode, subject_id: @subject.id, schedule_start: Time.now - 60, schedule_end: Time.now + 60)
      @season = create(:test_season, episode_count: 0, test_episodes: [@episode], recipe_id: @recipe.id)
      @series = create(:test_series, season_count: 0, test_seasons: [@season])

      @timestamp = Time.now
      @time = @timestamp.asctime

      @tf = TrackFilter.new
      @tf.pattern = /.* [a-zA-Z0-9]* sudo.* session opened for user .* by \(uid=[0-9]*\)/
      @tf.extraction_pattern = []
      @tf.score = 0.45
      @tf.course_of_action = 'detect message and cancel'
    end

    describe '#register' do
      it 'allows registration of series' do
        allow(@recipe_manager).to receive(:get_recipe)
        @track_collector.register_series(@series)
        expect(@track_collector.instance_variable_get(:@seasons).count).to eql(1)
      end
    end

    describe '#unregister' do
      it 'allows unregistration of series' do
        @track_collector.register_series @series
        expect(@track_collector.instance_variable_get(:@seasons).count).to eql(1)
        @track_collector.unregister_series @series
        expect(@track_collector.instance_variable_get(:@seasons).count).to eql(0)
      end
    end

    describe '#parse_trace' do
      before(:each) do
        TrackEntry.delete_all
        TestEpisode.delete_all
        TestSeason.delete_all
        TestSeries.delete_all
        TestProgram.delete_all
        allow(Logging).to receive(:info)
      end

      it 'returns empty string if no seasons are registered' do
        args = { 'trace' => 'Test' }
        expect(@track_collector.parse_trace(args)).to eql('')
      end

      it 'accepts more than one log entry (without client)' do
        track_entries_created = 0
        @tf.pattern = /.*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<subject>.*)/]
        @recipe.trackfilters = [@tf]
        program = create(:test_program, series_count: 0)
        series = create(:test_series, season_count: 0, test_program: program)
        season = create(:test_season, episode_count: 0, test_series: series, recipe_id: @recipe.id)

        subjects = []
        10.times do |_i|
          subject = build(:subject)
          create(:test_episode, subject_id: subject.id, test_season: season, schedule_start: @timestamp - 60, schedule_end: @timestamp + 60)
          subjects.push(subject)
        end
        @track_collector.register_series(series)
        5.times do
          subject = subjects.sample
          args = { 'trace' => "#{@time},#{subject.id}" }

          2.times do
            track_entries_created += @track_collector.parse_trace(args).lines.size - 1
          end
        end

        expect(track_entries_created).to eql(TrackEntry.count)
        expect(track_entries_created).to eql(10)
      end

      it 'filters timestamp,subject' do
        args = { 'trace' => "#{@time},#{@subject.id}" }
        @tf.pattern = /.*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<subject>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
      end

      it 'logs trace recording' do
        expect(Logging).to receive(:info).with('TrackCollector',
                                               %r{group.csv/\d+ issued `detect message and cancel` for `recipe\d+`})

        args = { 'trace' => "#{@time},#{@subject.id}" }

        @tf.pattern = /.*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<subject>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        @track_collector.parse_trace(args)
      end

      it 'resolves subjectid from userid' do
        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file) { [@subject] }
        allow(SubjectTrackerUtils).to receive(:resolve_subject_ids) { [@subject.id] }
        args = { 'trace' => "#{@time},#{@subject.userid}" }
        @tf.pattern = /.*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<userid>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
      end

      it 'resolves subjectid from optional field' do
        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file) { [@subject] }
        allow(SubjectTrackerUtils).to receive(:resolve_subject_ids) { [@subject.id] }

        @subject.add_field('optional')
        @subject.optional = 'some stuff'

        args = { 'trace' => "#{@time},#{@subject.optional}" }
        @tf.pattern = /.*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<optional>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
        expect(TrackEntry.where(test_episode: @episode.id,
                                timestamp: @track_collector.send(:parse_time, @time),
                                subject_id: @subject.id,
                                course_of_action: @tf.course_of_action)).to exist
      end

      it 'does not resolve subjectid if no subject is matched' do
        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file) { [@subject] }
        allow(SubjectTrackerUtils).to receive(:resolve_subject_ids) { [] }

        args = { 'trace' => "#{@time},no match" }
        @tf.pattern = /.*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<userid>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        # expect(Logging).to receive(:warning).with('TrackCollector',
        #                                          "Can not resolve subject_id: no matches found with #{logentry}!")
        expect(@track_collector.parse_trace(args)).to eql('')
        expect(TrackEntry.where(test_episode: @episode.id,
                                timestamp: @track_collector.send(:parse_time, @time),
                                subject_id: @subject.id,
                                course_of_action: @tf.course_of_action)).to_not exist
      end

      it 'does not resolve subjectid if multiple subjects are matched' do
        allow(SubjectTrackerUtils).to receive(:resolve_subject_ids) { [] }
        @subject.add_field('department')
        @subject.department = 'Dep1'
        subject2 = build(:subject)
        subject2.add_field('department')
        subject2.department = 'Dep1'
        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file) { [@subject, subject2] }

        args = { 'trace' => "#{@time},#{@subject.department}" }
        @tf.pattern = /.*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<department>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        # expect(Logging).to receive(:warning).with('TrackCollector',
        #                                          "Can not resolve subject_id: multiple matches found with #{logentry}!")
        expect(@track_collector.parse_trace(args)).to eql('')
        expect(TrackEntry.where(test_episode: @episode.id,
                                timestamp: @track_collector.send(:parse_time, @time),
                                subject_id: @subject.id,
                                course_of_action: @tf.course_of_action)).to_not exist
        expect(TrackEntry.where(test_episode: @episode.id,
                                timestamp: @track_collector.send(:parse_time, @time),
                                subject_id: subject2.id,
                                course_of_action: @tf.course_of_action)).to_not exist
      end

      it 'filters timestamp,test_episode_id,subject' do
        args = { 'trace' => "#{@time},#{@episode.id},#{@subject.id}" }
        @tf.pattern = /.*,[0-9]*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>[0-9]*),(?<subject>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
      end

      it 'creates the correct TrackEntry with episode' do
        args = { 'trace' => "#{@time},#{@episode.id},#{@subject.id}" }
        @tf.pattern = /.*,[0-9]*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>[0-9]*),(?<subject>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)

        te = TrackEntry.last
        expect(te.timestamp.localtime.asctime).to eq(@timestamp.asctime)
        expect(te.subject_id).to eq(@subject.id)
        expect(te.test_episode_id).to eq(@episode.id)
      end

      it 'does not associate TrackEntry with episode if timestamp is before schedule start' do
        # time before 10 mins
        time = @timestamp - 10 * 60
        args = { 'trace' => "#{time},#{@subject.id}" }
        @tf.pattern = /.*,.*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<subject>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args)).to eql('')
      end

      it 'does not associate TrackEntry with episode if timestamp is after schedule end' do
        # time in 10 mins
        time = @timestamp + 10 * 60
        args = { 'trace' => "#{time},#{@subject.id}" }
        @tf.pattern = /.*,.*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<subject>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args)).to eql('')
      end

      it 'does not associate TrackEntry with episode if timestamp is before schedule start - even if episode_id is given' do
        # time before 10 mins
        time = @timestamp - 10 * 60
        args = { 'trace' => "#{time},#{@episode.id},#{@subject.id}" }
        @tf.pattern = /.*,[0-9]*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>[0-9]*),(?<subject>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args)).to eql('')
      end

      it 'does not associate TrackEntry with episode if timestamp is after schedule end - even if episode_id is given' do
        # time in 10 mins
        time = @timestamp + 10 * 60
        args = { 'trace' => "#{time},#{@episode.id},#{@subject.id}" }
        @tf.pattern = /.*,[0-9]*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>[0-9]*),(?<subject>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args)).to eql('')
      end

      it 'dry_run does not create a TrackEntry' do
        args = { 'trace' => "#{@time},#{@episode.id},#{@subject.id}", 'dry_run' => true, 'tz_offset' => 0 }
        @tf.pattern = /.*,[0-9]*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>[0-9]*),(?<subject>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
        expect(TrackEntry.all).to be_empty
      end

      it 'adds a provided timezone offset' do
        offset = -5
        args = { 'trace' => "#{@time},#{@episode.id},#{@subject.id}", 'dry_run' => false, 'tz_offset' => offset }
        @tf.pattern = /.*,[0-9]*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>[0-9]*),(?<subject>.*)/]

        @episode.schedule_start = Time.now - 60 + offset * 3600
        @episode.schedule_end = Time.now + 60 + offset * 3600
        # episode is now scheduled 5 hours behind, logentry contains normal time

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
        expect(TrackEntry.last.timestamp).to eql(Time.parse(@time).utc + offset * 3600)
      end

      it 'creates NO entries if series does not match provided series in args' do
        args = { 'trace' => "#{@time},#{@episode.id},#{@subject.id}", 'series' => '1' }
        @tf.pattern = /.*,[0-9]*,.*/
        @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>[0-9]*),(?<subject>.*)/]

        @recipe.trackfilters = [@tf]
        @track_collector.register_series(@series)

        expect(@track_collector.parse_trace(args)).to eql('')
        expect(TrackEntry.all).to be_empty
      end

      context 'user with multiple pseudonyms exists' do
        before(:each) do
          @recipe2 = build(:recipe)
          allow(@recipe_manager).to receive(:get_recipe).with(@recipe2.id) { @recipe2 }
          @subject2 = Subject.new('group2.csv/1', @subject.name, @subject.surname,
                                  @subject.userid, @subject.email)
          @episode2 = create(:test_episode, subject_id: @subject2.id, schedule_start: Time.now - 60, schedule_end: Time.now + 60)
          @season2 = create(:test_season, episode_count: 0, test_episodes: [@episode2], recipe_id: @recipe2.id)
          @series2 = create(:test_series, season_count: 0, test_seasons: [@season2])

          @recipe3 = build(:recipe)
          allow(@recipe_manager).to receive(:get_recipe).with(@recipe3.id) { @recipe3 }
          @subject3 = Subject.new('group3.csv/1', @subject.name, @subject.surname,
                                  @subject.userid, @subject.email)
          @episode3 = create(:test_episode, subject_id: @subject3.id, schedule_start: Time.now - 60, schedule_end: Time.now + 60)
          @season3 = create(:test_season, episode_count: 0, test_episodes: [@episode3], recipe_id: @recipe2.id)
          @series3 = create(:test_series, season_count: 0, test_seasons: [@season3])

          @tf.pattern = /.*,.*/

          @tf2 = TrackFilter.new
          @tf2.pattern = /.* this wont match/
          @tf2.extraction_pattern = []
          @tf2.score = 0.45
          @tf2.course_of_action = 'detect message and cancel2'

          @tf3 = TrackFilter.new
          @tf3.pattern = /.* this wont match either/
          @tf3.extraction_pattern = []
          @tf3.score = 0.45
          @tf3.course_of_action = 'detect message and cancel3'
          allow(SubjectTrackerUtils).to receive(:resolve_subject_ids) do |_, _, _, season|
            season = [@season, @season2, @season3].filter { |s| s.id == season.id }.first
            [season.test_episodes[0].subject_id]
          end
        end

        context '3 times same trackfilter pattern' do
          # EP1 & EP2 are active, EP3 is inactive. All EPs match the trace.
          it 'creates a TrackEntry for the correct episode if episode_id is present' do
            args = { 'trace' => "#{@time},#{@episode.id}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf]
            @recipe3.trackfilters = [@tf]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
            expect(TrackEntry.count).to eq(1)
            te = TrackEntry.last
            expect(te.test_episode_id).to eq(@episode.id)
          end

          it 'creates a TrackEntry for the correct episode if subject_id is present' do
            args = { 'trace' => "#{@time},#{@subject2.id}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<subject>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf]
            @recipe3.trackfilters = [@tf]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
            expect(TrackEntry.count).to eq(1)
            te = TrackEntry.last
            expect(te.test_episode_id).to eq(@episode2.id)
          end

          it 'creates a TrackEntry for the active episodes if no identifier is present' do
            args = { 'trace' => "#{@time},#{@subject.userid}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<userid>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf]
            @recipe3.trackfilters = [@tf]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(2)
            expect(TrackEntry.count).to eq(2)
            tes = TrackEntry.where(course_of_action: @tf.course_of_action)
            recorded_episodes = []
            tes.each do |te|
              recorded_episodes << te.test_episode_id
            end
            expect(Set.new(recorded_episodes)).to eq(Set[@episode.id, @episode2.id])
          end
        end

        context '3 different trackfilter pattern' do
          # EP1 & EP2 are active, EP3 is inactive. Only EP1 matches the trace.
          it 'only creates a single TrackEntry if episodeid is present in the trace' do
            args = { 'trace' => "#{@time},#{@episode.id}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf2]
            @recipe3.trackfilters = [@tf3]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
            expect(TrackEntry.count).to eq(1)
            te = TrackEntry.last
            expect(te.test_episode_id).to eq(@episode.id)
          end

          it 'only creates a single TrackEntry if subjectid is present in the trace' do
            args = { 'trace' => "#{@time},#{@subject.id}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<subject>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf2]
            @recipe3.trackfilters = [@tf3]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
            expect(TrackEntry.count).to eq(1)
            te = TrackEntry.last
            expect(te.test_episode_id).to eq(@episode.id)
          end

          it 'only creates a TrackEntry for the episode with the matching pattern' do
            args = { 'trace' => "#{@time},#{@subject.userid}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<userid>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf2]
            @recipe3.trackfilters = [@tf3]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
            expect(TrackEntry.count).to eq(1)
            te = TrackEntry.last
            expect(te.test_episode_id).to eq(@episode.id)
          end
        end

        context 'same pattern on active episodes' do
          # EP1 & EP2 are active and have the same trackfilter pattern matching
          # the trace. EP3 is inactive and does not match the trace
          it 'only creates a single TrackEntry if episodeid is present in the trace' do
            args = { 'trace' => "#{@time},#{@episode.id}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf]
            @recipe3.trackfilters = [@tf3]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
            expect(TrackEntry.count).to eq(1)
            te = TrackEntry.last
            expect(te.test_episode_id).to eq(@episode.id)
          end

          it 'only creates a single TrackEntry if subjectid is present in the trace' do
            args = { 'trace' => "#{@time},#{@subject.id}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<subject>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf]
            @recipe3.trackfilters = [@tf3]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
            expect(TrackEntry.count).to eq(1)
            te = TrackEntry.last
            expect(te.test_episode_id).to eq(@episode.id)
          end

          it 'only creates TrackEntries for the active episodes' do
            args = { 'trace' => "#{@time},#{@subject.userid}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<userid>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf]
            @recipe3.trackfilters = [@tf3]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(2)
            expect(TrackEntry.count).to eq(2)
            tes = TrackEntry.where(course_of_action: @tf.course_of_action)
            recorded_episodes = []
            tes.each do |te|
              recorded_episodes << te.test_episode_id
            end
            expect(Set.new(recorded_episodes)).to eq(Set[@episode.id, @episode2.id])
          end
        end

        context 'same pattern on one active and one inactive episode' do
          # EP1 & EP2 are active, EP3 is inactive. EP1 & EP3 have the same
          # trackfilter pattern that matches the trace, EP2 does not match
          it 'only creates a single TrackEntry if episodeid is present in the trace' do
            args = { 'trace' => "#{@time},#{@episode.id}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<test_episode_id>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf2]
            @recipe3.trackfilters = [@tf]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
            expect(TrackEntry.count).to eq(1)
            te = TrackEntry.last
            expect(te.test_episode_id).to eq(@episode.id)
          end

          it 'only creates a single TrackEntry if subjectid is present in the trace' do
            args = { 'trace' => "#{@time},#{@subject.id}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<subject>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf2]
            @recipe3.trackfilters = [@tf]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
            expect(TrackEntry.count).to eq(1)
            te = TrackEntry.last
            expect(te.test_episode_id).to eq(@episode.id)
          end

          it 'only creates a single TrackEntry for the active episode with the matching pattern' do
            args = { 'trace' => "#{@time},#{@subject.userid}" }
            @tf.extraction_pattern = [/(?<timestamp>.*),(?<userid>.*)/]

            @recipe.trackfilters = [@tf]
            @recipe2.trackfilters = [@tf2]
            @recipe3.trackfilters = [@tf]
            @track_collector.register_series(@series)
            @track_collector.register_series(@series2)

            expect(@track_collector.parse_trace(args).lines.size - 1).to eql(1)
            expect(TrackEntry.count).to eq(1)
            te = TrackEntry.last
            expect(te.test_episode_id).to eq(@episode.id)
          end
        end
      end
    end

    describe '#parse_helpdesk' do
      before(:each) do
        TrackEntry.delete_all
        TestEpisode.delete_all
        TestSeason.delete_all
        TestSeries.delete_all
        TestProgram.delete_all
        allow(Logging).to receive(:info)
      end

      it 'creates an entry with subject_id and timestamp present in trace' do
        args = {
          'trace' => "#{@subject.id} called at #{@timestamp}",
          'dry_run' => false,
          'tz_offset' => 0
        }

        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return([@subject])
        @track_collector.register_series(@series)

        ret = @track_collector.parse_helpdesk(args)
        te = TrackEntry.last
        expect(te.timestamp.localtime.asctime).to eq(@timestamp.asctime)
        expect(te.subject_id).to eq(@subject.id)
        expect(te.test_episode_id).to eq(@episode.id)
        expect(ret.lines.last).to eq("#{te.test_episode_id},#{te.subject_id},#{te.score},#{te.course_of_action},#{te.timestamp.utc}\n")
      end

      it 'creates NO entry with no matching subject' do
        args = {
          'trace' => "Someone called at #{@timestamp}",
          'dry_run' => false,
          'tz_offset' => 0
        }

        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return([@subject])
        @track_collector.register_series(@series)

        ret = @track_collector.parse_helpdesk(args)
        expect(ret).to eq('')
        expect(TrackEntry.all).to be_empty
      end

      it 'creates NO entry with no matching timestamp' do
        args = {
          'trace' => "#{@subject.id} called at #{@timestamp + 3600}",
          'dry_run' => false,
          'tz_offset' => 0
        }

        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return([@subject])
        @track_collector.register_series(@series)

        ret = @track_collector.parse_helpdesk(args)
        expect(ret).to eq('')
        expect(TrackEntry.all).to be_empty
      end

      it 'parses helpdesk traces with provided format' do
        args = {
          'trace' => "some, header, thingy\n"\
                     "some nonsense here! #{@timestamp}, #{@subject.id}\n"\
                     "nonsense #{@timestamp}, #{@subject.id}, called for unknown reason\n"\
                     "apocalypse 2012-12-21 13:37:00 +0100, #{@subject.id}, called for unknown reason\n"\
                     "more nonsense #{@timestamp}, nonsubject\n",
          'format' => '[\D\s\!]+(?<time>\d+-\d+-\d+ \d+:\d+:\d+ \+\d+), (?<subject_id>[\w\.\/]+).*',
          'dry_run' => false,
          'tz_offset' => 0
        }

        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return([@subject])
        @track_collector.register_series(@series)

        ret = @track_collector.parse_helpdesk(args)
        expect(TrackEntry.all.length).to be(2)
        te = TrackEntry.last
        expect(te.timestamp.localtime.asctime).to eq(@timestamp.asctime)
        expect(te.subject_id).to eq(@subject.id)
        expect(te.test_episode_id).to eq(@episode.id)
        expect(ret.lines.last).to eq("#{te.test_episode_id},#{te.subject_id},#{te.score},#{te.course_of_action},#{te.timestamp.utc}\n")
      end

      it 'parses helpdesk traces with custom time format' do
        args = {
          'trace' => "#{@timestamp.asctime}, #{@subject.id}\n"\
                     "#{@timestamp.asctime}, nonsubject\n",
          'format' => '(?<%a %b %d %H:%M:%S %Y>[\w\s:]+), (?<subject_id>[\w\.\/]+)',
          'dry_run' => false,
          'tz_offset' => 0
        }

        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return([@subject])
        @track_collector.register_series(@series)

        ret = @track_collector.parse_helpdesk(args)
        te = TrackEntry.last
        expect(te.timestamp.localtime.asctime).to eq(@timestamp.asctime)
        expect(te.subject_id).to eq(@subject.id)
        expect(te.test_episode_id).to eq(@episode.id)
        expect(ret.lines.last).to eq("#{te.test_episode_id},#{te.subject_id},#{te.score},#{te.course_of_action},#{te.timestamp.utc}\n")
      end

      it 'creates NO entries if series does not match provided series in args' do
        args = {
          'trace' => "#{@subject.id} called at #{@timestamp} to report #{@recipe.id}",
          'dry_run' => false,
          'tz_offset' => 0,
          'series' => 1
        }

        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return([@subject])
        @track_collector.register_series(@series)

        ret = @track_collector.parse_helpdesk(args)
        expect(ret).to eq('')
        expect(TrackEntry.all).to be_empty
      end

      it 'creates NO entries if dry_run is set' do
        args = {
          'trace' => "#{@subject.id} called at #{@timestamp} to report #{@recipe.id}",
          'dry_run' => true,
          'tz_offset' => 0
        }

        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return([@subject])
        @track_collector.register_series(@series)

        @track_collector.parse_helpdesk(args)
        expect(TrackEntry.all).to be_empty
      end

      it 'adds a provided timezone offset' do
        offset = -5
        args = {
          'trace' => "#{@subject.id} called at #{@timestamp} to report #{@recipe.id}",
          'dry_run' => false,
          'tz_offset' => offset
        }

        @episode.schedule_start = Time.now - 60 + offset * 3600
        @episode.schedule_end = Time.now + 60 + offset * 3600
        # episode is now scheduled 5 hours behind, logentry contains normal time

        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return([@subject])
        @track_collector.register_series(@series)

        ret = @track_collector.parse_helpdesk(args)
        te = TrackEntry.last
        expect(te.timestamp).to eql(Time.parse(@time).utc + offset * 3600)
        expect(ret.lines.last).to eq("#{te.test_episode_id},#{te.subject_id},#{te.score},#{te.course_of_action},#{te.timestamp.utc}\n")
      end

      it 'uses provided score value' do
        args = {
          'trace' => "#{@subject.id} called at #{@timestamp} to report #{@recipe.id}",
          'dry_run' => false,
          'tz_offset' => 0,
          'score' => '9001'
        }

        allow(@subject_manager).to receive(:get_subjects).with(@series.group_file).and_return([@subject])
        @track_collector.register_series(@series)

        ret = @track_collector.parse_helpdesk(args)
        te = TrackEntry.last
        expect(te.timestamp.localtime.asctime).to eq(@timestamp.asctime)
        expect(te.subject_id).to eq(@subject.id)
        expect(te.test_episode_id).to eq(@episode.id)
        expect(te.score).to eq(9001)
        expect(ret.lines.last).to eq("#{te.test_episode_id},#{te.subject_id},#{te.score},#{te.course_of_action},#{te.timestamp.utc}\n")
      end
    end
  end
end
