# frozen_string_literal: true

require 'fileutils'

describe 'build the gem-file' do
  after :all do
    FileUtils.rm_f('artifactomat_test_build.gem')
  end

  it 'returns no error' do
    expect(system('gem build artifactomat.gemspec -o artifactomat_test_build.gem')).to eq(true)
  end
end
