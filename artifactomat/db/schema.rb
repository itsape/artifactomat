# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_05_19_092450) do

  create_table "detection_events", force: :cascade do |t|
    t.integer "test_episode_id"
    t.datetime "timestamp"
    t.integer "artifact_present"
    t.index ["test_episode_id"], name: "index_detection_events_on_test_episode_id"
  end

  create_table "infra_elements_recipies", id: false, force: :cascade do |t|
    t.integer "infrastructure_element_id"
    t.index ["infrastructure_element_id"], name: "index_infra_elements_recipies_on_infrastructure_element_id"
  end

  create_table "jobs", force: :cascade do |t|
    t.string "user", null: false
    t.integer "action", null: false
    t.integer "status", default: 0, null: false
    t.string "file_path", default: "", null: false
    t.string "deploy_path", default: "", null: false
    t.datetime "expiration_date"
    t.datetime "creation_date"
    t.datetime "collection_date"
    t.string "watch_arguments"
    t.index ["user"], name: "idx_jobs_user"
  end

  create_table "subject_histories", force: :cascade do |t|
    t.integer "test_series_id"
    t.datetime "submit_time"
    t.datetime "session_begin"
    t.datetime "session_end"
    t.string "ip"
    t.string "subject_id"
    t.index ["test_series_id"], name: "index_subject_histories_on_test_series_id"
  end

  create_table "test_episodes", force: :cascade do |t|
    t.integer "test_season_id"
    t.string "subject_id"
    t.integer "status", default: 0
    t.datetime "schedule_start"
    t.datetime "schedule_end"
    t.boolean "armed", default: false, null: false
    t.boolean "routing_active", default: false, null: false
    t.index ["test_season_id"], name: "index_test_episodes_on_test_season_id"
  end

  create_table "test_programs", force: :cascade do |t|
    t.string "label"
  end

  create_table "test_seasons", force: :cascade do |t|
    t.integer "test_series_id"
    t.string "label"
    t.string "recipe_id"
    t.text "recipe_parameters"
    t.integer "duration"
    t.boolean "locked", default: false, null: false
    t.string "screenshots_folder", null: false
    t.index ["test_series_id"], name: "index_test_seasons_on_test_series_id"
  end

  create_table "test_series", force: :cascade do |t|
    t.integer "test_program_id"
    t.string "label"
    t.text "group_file"
    t.datetime "start_date"
    t.integer "status", default: 0
    t.datetime "end_date"
    t.index ["test_program_id"], name: "index_test_series_on_test_program_id"
  end

  create_table "track_entries", force: :cascade do |t|
    t.integer "test_episode_id"
    t.float "score"
    t.datetime "timestamp"
    t.string "course_of_action"
    t.string "subject_id"
    t.index ["test_episode_id"], name: "index_track_entries_on_test_episode_id"
  end

end
