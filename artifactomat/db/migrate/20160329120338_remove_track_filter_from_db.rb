class RemoveTrackFilterFromDb < ActiveRecord::Migration[5.2]
  def change
    drop_table :track_filters
  end
end
