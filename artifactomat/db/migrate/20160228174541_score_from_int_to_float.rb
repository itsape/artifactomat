class ScoreFromIntToFloat < ActiveRecord::Migration[5.2]
  def change
    change_column :track_filters, :score, :float
    change_column :track_entries, :score, :float
  end
end
