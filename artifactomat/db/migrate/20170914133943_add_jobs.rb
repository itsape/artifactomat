class AddJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      t.string  "user", null: false
      t.integer "action", null: false
      t.integer "status", null: false, default: 0
      t.string  "file_path",  default: "", null: false
      t.string  "deploy_path",  default: "", null: false
    end
  end
end
