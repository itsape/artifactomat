class SubjectToSubjectId < ActiveRecord::Migration[5.2]
  def change
    rename_column :test_episodes, :subject, :subject_id
    rename_column :track_entries, :subject, :subject_id
    rename_column :track_filters, :subject, :subject_id
  end
end
