class RemoveRecipeParametersFromTestepisode < ActiveRecord::Migration[5.2]
  def change
    remove_column :test_episodes, :recipe_parameters
  end
end
