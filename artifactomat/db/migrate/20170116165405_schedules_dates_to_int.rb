class SchedulesDatesToInt < ActiveRecord::Migration[5.2]
  def up
     change_column :schedules, :start_date, :integer
     change_column :schedules, :end_date, :integer
  end

   def down
     change_column :schedules, :start_date, :datetime
     change_column :schedules, :end_date, :datetime
   end
end
