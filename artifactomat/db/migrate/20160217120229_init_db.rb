class InitDb < ActiveRecord::Migration[5.2]
  def up
    create_table :track_entries do |t|
      t.belongs_to :test_episode, index: true

      t.column :score, :int
      t.column :timestamp, :datetime
      t.column :course_of_action, :string
      t.column :subject, :string
    end

    create_table :test_programs do |t|
      t.column :label, :string
    end

    create_table :test_series do |t|
      t.belongs_to :test_program, index: true

      t.column :label, :string
      t.column :group_file, :text
    end

    create_table :test_seasons do |t|
      t.belongs_to :test_series, index: true

      t.column :label, :string
      t.column :status, :integer
    end

    create_table :test_episodes do |t|
      t.belongs_to :test_season, index: true

      t.column :label, :string
      t.column :subject, :string
      t.column :recipe, :string
      t.column :duration, :integer
      t.column :recipe_parameters, :text
    end

    create_table :schedules do |t|
      t.belongs_to :test_episode, index: true

      t.column :start_date, :datetime
      t.column :end_date, :datetime
    end

    create_table :infrastructure_elements do |t|
      t.column :infrastructure_type, :string
      t.column :monitor_script, :text
      t.column :infrastructure_element_parameters, :text
      t.column :generate_script, :text
      t.column :destroy_script, :text
      t.column :arm_script, :text
      t.column :disarm_script, :text
      t.column :parameters, :text
      t.column :status, :integer, default: 0
    end

    create_table :infra_elements_recipies, id: false do |t|
      t.belongs_to :infrastructure_element, index: true
    end

    create_table :track_filters do |t|

      t.column :extraction_pattern, :text
      t.column :score, :integer
      t.column :pattern, :text
      t.column :course_of_action, :string
      t.column :subject, :string
      t.column :recipe, :string
    end

  end

  def self.down
    drop_table :test_programs
    drop_table :test_series
    drop_table :test_seasons
    drop_table :test_episodes
    drop_table :schedules
    drop_table :track_entries
    drop_table :infrastructure_elements
    drop_table :track_filter
  end
end
