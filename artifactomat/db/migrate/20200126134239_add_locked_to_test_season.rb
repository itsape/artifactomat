class AddLockedToTestSeason < ActiveRecord::Migration[5.2]
  def change
    add_column :test_seasons, :locked, :boolean, null: false, default: false
  end
end
