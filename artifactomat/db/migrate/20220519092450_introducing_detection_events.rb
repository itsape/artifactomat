class IntroducingDetectionEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :detection_events do |t|
      t.belongs_to :test_episode, index: true
      t.column :timestamp, :datetime
      t.column :artifact_present, :integer
    end
  end
end
