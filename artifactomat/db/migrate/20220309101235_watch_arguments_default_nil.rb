class WatchArgumentsDefaultNil < ActiveRecord::Migration[5.2]
  def change
    change_column_null :jobs, :watch_arguments, true
    change_column_default :jobs, :watch_arguments, from: "{}", to: nil
  end
end

