class RecipeToRecipeId < ActiveRecord::Migration[5.2]
  def change
    rename_column :test_seasons, :recipe, :recipe_id
    rename_column :track_filters, :recipe, :recipe_id
  end
end
