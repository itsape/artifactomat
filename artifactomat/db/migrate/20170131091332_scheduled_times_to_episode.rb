class ScheduledTimesToEpisode < ActiveRecord::Migration[5.2]
  def change
    add_column :test_episodes, :schedule_start, :datetime
    add_column :test_episodes, :schedule_end, :datetime
  end
end
