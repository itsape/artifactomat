class AddDurationToTestSeries < ActiveRecord::Migration[5.2]
  def change
    add_column :test_series, :duration, :integer
  end
end