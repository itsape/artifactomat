class RemoveInfrastructureElementFromDb < ActiveRecord::Migration[5.2]
  def change
    drop_table :infrastructure_elements
  end
end
