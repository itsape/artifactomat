class AddDurationToSeason < ActiveRecord::Migration[5.2]
  def change
    add_column :test_seasons, :duration, :integer
  end
end
