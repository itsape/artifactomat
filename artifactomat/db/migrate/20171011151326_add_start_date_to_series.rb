class AddStartDateToSeries < ActiveRecord::Migration[5.2]
  def change
    add_column :test_series, :start_date, :datetime
  end
end
