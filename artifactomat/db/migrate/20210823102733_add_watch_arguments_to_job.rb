class AddWatchArgumentsToJob < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :watch_arguments, :string, null: false, default: '{}'
  end
end

