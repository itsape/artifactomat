class AddCollectionDateToJobs < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :collection_date, :datetime
  end
end
