class AddScreenshotsFolderToTestSeason < ActiveRecord::Migration[5.2]
  def change
    add_column :test_seasons, :screenshots_folder, :string, null: false
  end
end

