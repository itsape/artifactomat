class AddEndDateToSeries < ActiveRecord::Migration[5.2]
  def change
    add_column :test_series, :end_date, :datetime
  end
end
