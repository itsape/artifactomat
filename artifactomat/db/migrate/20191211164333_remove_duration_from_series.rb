class RemoveDurationFromSeries < ActiveRecord::Migration[5.2]
  def change
    remove_column :test_series, :duration, :integer
  end
end
