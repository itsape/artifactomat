class ChangeTestseasonTestepisode < ActiveRecord::Migration[5.2]
  def change
    remove_column :test_episodes, :recipe
    add_column :test_seasons, :recipe, :string
  end
end
