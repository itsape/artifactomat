class AddIndexJobsUser < ActiveRecord::Migration[5.2]
  def change
    add_index :jobs, :user, name: 'idx_jobs_user'
  end
end
