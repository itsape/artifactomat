class DropSchedules < ActiveRecord::Migration[5.2]
  def up
    drop_table :schedules
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
end
