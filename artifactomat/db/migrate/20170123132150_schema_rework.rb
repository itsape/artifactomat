class SchemaRework < ActiveRecord::Migration[5.2]
  def change
    change_column :test_episodes, :status, :integer, :default => 0
    remove_column :test_episodes, :label
    remove_column :test_episodes, :duration
  end
end
