class MoveStatusFromTsToTe < ActiveRecord::Migration[5.2]
  def change
    remove_column :test_seasons, :status
    add_column :test_episodes, :status, :integer
  end
end
