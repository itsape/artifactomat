class IntroducingSubjectHistory < ActiveRecord::Migration[5.2]
  def change
    create_table :subject_histories do |t|
      t.belongs_to :test_series, index: true
      t.column :submit_time, :datetime
      t.column :session_begin, :datetime
      t.column :session_end, :datetime
      t.column :ip, :string
      t.column :subject_id, :string
    end
  end
end
