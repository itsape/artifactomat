class AddStateToSeries < ActiveRecord::Migration[5.2]
  def change
    add_column :test_series, :status, :integer, default: 0
  end
end
