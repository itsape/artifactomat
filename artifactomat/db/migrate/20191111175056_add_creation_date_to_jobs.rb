class AddCreationDateToJobs < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :creation_date, :datetime
  end
end
