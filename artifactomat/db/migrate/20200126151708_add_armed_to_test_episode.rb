class AddArmedToTestEpisode < ActiveRecord::Migration[5.2]
  def change
    add_column :test_episodes, :armed, :boolean, null: false, default: false
  end
end
