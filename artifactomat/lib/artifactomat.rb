# frozen_string_literal: true

require 'active_record'
require 'artifactomat/models'
require 'artifactomat/modules'

env = ENV['ARTIFACTOMAT_ENV'] || 'test'

if env == 'production'
  cfg = ConfigurationManager.new
  ActiveRecord::Base.establish_connection(cfg.get('conf.database'))
elsif env == 'integration'
  db_config = YAML.safe_load(File.open(File.dirname(__FILE__) + '/../db/config.yml'))['integration']
  ActiveRecord::Base.establish_connection(db_config)
  load(File.dirname(__FILE__) + '/../db/schema.rb')
else
  db_config = YAML.safe_load(File.open(File.dirname(__FILE__) + '/../db/config.yml'))[env]
  ActiveRecord::Base.establish_connection(db_config)
end

load(File.dirname(__FILE__) + '/../db/schema.rb') if env == 'test'
