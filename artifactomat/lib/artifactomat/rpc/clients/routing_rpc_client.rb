# frozen_string_literal: true

require 'artifactomat/models/test_series'
require 'artifactomat/models/test_season'
require 'artifactomat/models/test_episode'
require 'artifactomat/modules/configuration_manager'
require 'artifactomat/modules/routing'
require 'artifactomat/rpc/clients/general_rpc_client'

# RPC client for the routing service
class RoutingClient
  include GeneralRpcClient

  # additional arguments to match Routing signature
  def initialize(configuration_manager, _)
    @service_name = 'routing'
    @standard_proxy_methods = %i[run running?]
    @client = create_rpc_client configuration_manager
    @call_mutex = Mutex.new
  end

  def create_routing(series)
    @call_mutex.synchronize do
      @client.create_routing series.id
    end
  end

  def remove_routing(series)
    @call_mutex.synchronize do
      @client.remove_routing series.id
    end
  end

  def get_internal_ports(season)
    @call_mutex.synchronize do
      @client.get_internal_ports season.id
    end
  end

  def get_external_address(season)
    @call_mutex.synchronize do
      @client.get_external_address season.id
    end
  end

  def activate(episode)
    @call_mutex.synchronize do
      @client.activate episode.id
    end
  end

  def deactivate(episode)
    @call_mutex.synchronize do
      @client.deactivate episode.id
    end
  end

  def unschedule_jobs(series)
    @call_mutex.synchronize do
      @client.unschedule_jobs series.id
    end
  end

  private

  attr_reader :client, :service_name, :standard_proxy_methods
end
