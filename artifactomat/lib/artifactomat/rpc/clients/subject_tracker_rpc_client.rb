# frozen_string_literal: true

require 'artifactomat/rpc/clients/general_rpc_client'

# The counterpart to SubjectTrackerRpcService.
class SubjectTrackerRpcClient
  include GeneralRpcClient

  # second argument to match SubjectTracker signature
  def initialize(configuration_manager, _)
    @service_name = 'subject_tracker'
    @standard_proxy_methods = %i[status subject_ids subjects_changed
                                 activate_series deactivate_series]
    @client = create_rpc_client configuration_manager
  end

  private

  attr_reader :client, :service_name, :standard_proxy_methods
end
