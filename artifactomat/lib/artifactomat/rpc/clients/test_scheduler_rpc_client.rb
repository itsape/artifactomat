# frozen_string_literal: true

require 'artifactomat/rpc/clients/general_rpc_client'

# RPC client connecting Ape to the ape-scheduler
class TestSchedulerRpcClient
  include GeneralRpcClient

  # additional arguments to match TestScheduler signature
  def initialize(configuration_manager, _, _)
    @service_name = 'test_scheduler'
    @standard_proxy_methods = %i[status format_duration running?]
    @client = create_rpc_client(configuration_manager)
    @call_mutex = Mutex.new
  end

  def schedule(series)
    @call_mutex.synchronize do
      @client.schedule(series.id)
    end
  end

  def generate_timetable(series)
    @call_mutex.synchronize do
      @client.generate_timetable(series.id)
    end
  end

  def ack(series)
    @call_mutex.synchronize do
      @client.ack(series.id)
    end
  end

  def series_current_testtime(series, testtime_delta = 0)
    @call_mutex.synchronize do
      @client.series_current_testtime(series.id, testtime_delta)
    end
  end

  def series_enough_runtime(series, testtime_delta = 0)
    @call_mutex.synchronize do
      @client.series_enough_runtime(series.id, testtime_delta)
    end
  end

  def add_jobs(episode)
    @call_mutex.synchronize do
      @client.add_jobs(episode.id)
    end
  end

  def unschedule_jobs(series)
    @call_mutex.synchronize do
      @client.unschedule_jobs(series.id)
    end
  end

  private

  attr_reader :client, :service_name, :standard_proxy_methods
end
