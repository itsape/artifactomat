# frozen_string_literal: true

require 'artifactomat/rpc/clients/general_rpc_client'

# The counterpart to TrackCollectorRpcService.
class TrackCollectorRpcClient
  include GeneralRpcClient

  # additional arguments to match TrackCollector signature
  def initialize(configuration_manager, _, _)
    @service_name = 'track_collector'
    @standard_proxy_methods = %i[status parse_trace parse_helpdesk]
    @client = create_rpc_client configuration_manager
  end

  def register_series(series)
    @client.register_series(series.id)
  end

  def unregister_series(series)
    @client.unregister_series(series.id)
  end

  private

  attr_reader :client, :service_name, :standard_proxy_methods
end
