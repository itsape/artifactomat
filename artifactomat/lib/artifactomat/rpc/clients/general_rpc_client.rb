# frozen_string_literal: true

require 'redis-rpc'

# A general rpc client.
module GeneralRpcClient
  # A timeout error.
  class RpcTimeoutError < StandardError
  end

  def create_rpc_client(configuration_manager)
    redis = Redis.new(path: configuration_manager.get('conf.ape.redis_socket').to_s)
    timeout = 10
    RpcClient.new redis, service_name, timeout
  end

  def respond_to_missing?(method_name, _)
    standard_proxy_methods.include? method_name
  end

  def method_missing(method, *args)
    # call the Object method_missing()
    return super unless standard_proxy_methods.include? method

    client.public_send(method, *args)
  end

  # A generic rpc client.
  class RpcClient < RedisRpc::Client
    # It sets of :reek:BooleanParameter but we need the signature of this method to be
    # compliant to the one form Object.
    def respond_to?(_method, _include_all = false)
      true
    end

    def send(method, *args)
      super
    end

    # rubocop:disable Style/MethodMissingSuper, Style/MissingRespondToMissing
    def method_missing(method_name, *args)
      send(method_name, *args)
    rescue RedisRpc::TimeoutException
      reporter = @message_queue ||= 'unknown'
      msg = 'Service not responding (RedisRpcTimeout). Is the service running?'
      Logging.error(reporter, msg)
      raise RpcTimeoutError.new, "#{reporter}: #{msg}"
    end
    # rubocop:enable Style/MethodMissingSuper, Style/MissingRespondToMissing
  end

  private_constant :RpcClient
end
