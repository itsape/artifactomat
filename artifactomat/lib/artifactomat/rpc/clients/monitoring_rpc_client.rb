# frozen_string_literal: true

require 'artifactomat/rpc/clients/general_rpc_client'
require 'artifactomat/modules/monitoring'

# RPC client for the monitoring service
class MonitoringRpcClient
  include GeneralRpcClient

  # additional arguments to match Monitoring signature
  def initialize(configuration_manager, _, _, _)
    @service_name = 'monitoring'
    @standard_proxy_methods = %i[status running? stop run to_str polling_rate]
    @client = create_rpc_client(configuration_manager)
    @call_mutex = Mutex.new
  end

  def get_status(inf_element)
    state_info = @call_mutex.synchronize do
      @client.get_status(inf_element.to_yaml)
    end
    MonitoringState.new(state_info['state'], state_info['error'])
  end

  def subscribe(inf_element)
    @call_mutex.synchronize do
      @client.subscribe(inf_element.to_yaml)
    end
  end

  def unsubscribe(inf_element)
    @call_mutex.synchronize do
      @client.unsubscribe(inf_element.to_yaml)
    end
  end

  def infrastructure_running?(inf_element)
    @call_mutex.synchronize do
      @client.infrastructure_running?(inf_element.to_yaml)
    end
  end

  private

  attr_reader :client, :service_name, :standard_proxy_methods
end
