# frozen_string_literal: true

require 'artifactomat/rpc/services/general_rpc_service'
require 'artifactomat/models/test_series'

# This is the bridge between rpc via redis and an instance of
# TrackCollector.
class TrackCollectorRpcService
  include GeneralRpcService

  def initialize(configuration_manager, track_collector)
    @configuration_manager = configuration_manager
    @service_name = 'track_collector'
    @standard_proxy_methods = %i[status parse_trace parse_helpdesk]
    @track_collector = track_collector
  end

  def register_series(series_id)
    series = TestSeries.find series_id
    @track_collector.register_series(series)
  end

  def unregister_series(series_id)
    series = TestSeries.find series_id
    @track_collector.unregister_series(series)
  end

  private

  attr_reader :configuration_manager, :service_name,
              :standard_proxy_methods

  def underlying_module
    @track_collector
  end
end
