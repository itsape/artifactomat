# frozen_string_literal: true

require 'redis-rpc'
require 'artifactomat/utils/bullet_loader'

# A general rpc-service.
module GeneralRpcService
  def start
    redis = Redis.new(path: configuration_manager.get('conf.ape.redis_socket').to_s)
    rpc_server = RedisRpc::Server.new redis, service_name, self
    Thread.new do
      rpc_server.run
    end
  end

  def respond_to_missing?(method_name, _)
    standard_proxy_methods.include? method_name
  end

  def method_missing(method, *args)
    return super unless standard_proxy_methods.include? method

    Bullet.profile do
      underlying_module.send method, *args
    end
  end
end
