# frozen_string_literal: true

require 'artifactomat/models/test_series'
require 'artifactomat/models/test_season'
require 'artifactomat/models/test_episode'
require 'artifactomat/modules/configuration_manager'
require 'artifactomat/modules/recipe_manager'
require 'artifactomat/modules/routing'
require 'artifactomat/rpc/services/general_rpc_service'

# RPC service providing its.apt's dynamic routing capabilities
class RoutingService
  include GeneralRpcService

  def initialize
    @service_name = 'routing'
    @standard_proxy_methods = %i[run running?]
    @configuration_manager = ConfigurationManager.new
    recipe_manager = RecipeManager.new
    @underlying_module = Routing.new(@configuration_manager, recipe_manager)
    @underlying_module.run
  end

  def create_routing(series_id)
    series = TestSeries.find series_id
    @underlying_module.create_routing series
  end

  def remove_routing(series_id)
    series = TestSeries.find series_id
    @underlying_module.remove_routing series
  end

  # :reek:NilCheck
  def get_internal_ports(season_id)
    return [] if season_id.nil?

    season = TestSeason.find season_id
    @underlying_module.get_internal_ports season
  end

  # :reek:NilCheck
  def get_external_address(season_id)
    season = TestSeason.find season_id unless season_id.nil?
    @underlying_module.get_external_address season
  end

  def activate(episode_id)
    episode = TestEpisode.find episode_id
    @underlying_module.activate episode
  end

  def deactivate(episode_id)
    episode = TestEpisode.find episode_id
    @underlying_module.deactivate episode
  end

  def unschedule_jobs(series_id)
    series = TestSeries.find series_id
    @underlying_module.unschedule_jobs series
  end

  private

  attr_reader :service_name, :standard_proxy_methods, :underlying_module, :configuration_manager
end
