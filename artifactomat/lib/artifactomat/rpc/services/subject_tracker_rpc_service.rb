# frozen_string_literal: true

require 'artifactomat/rpc/services/general_rpc_service'

# This is the bridge between rpc via redis and an instance of
# SubjectTracker.
class SubjectTrackerRpcService
  include GeneralRpcService

  def initialize(configuration_manager, subject_tracker)
    @configuration_manager = configuration_manager
    @service_name = 'subject_tracker'
    @standard_proxy_methods = %i[status subject_ids subjects_changed
                                 activate_series deactivate_series]
    @underlying_module = subject_tracker
  end

  private

  attr_reader :configuration_manager, :service_name,
              :standard_proxy_methods, :underlying_module
end
