# frozen_string_literal: true

require 'artifactomat/models/test_episode'
require 'artifactomat/models/test_series'
require 'artifactomat/modules/configuration_manager'
require 'artifactomat/modules/recipe_manager'
require 'artifactomat/modules/subject_manager'
require 'artifactomat/modules/test_scheduler'
require 'artifactomat/rpc/services/general_rpc_service'

# RPC service connecting Ape to the ape-scheduler
class TestSchedulerRpcService
  include GeneralRpcService

  def initialize
    @service_name = 'test_scheduler'
    @standard_proxy_methods = %i[status format_duration running? stop]
    @configuration_manager = ConfigurationManager.new
    recipe_manager = RecipeManager.new
    subject_manager = SubjectManager.new
    delivery_manager = DeliveryManager.new(@configuration_manager, recipe_manager, subject_manager)
    @underlying_module = TestScheduler.new(@configuration_manager, subject_manager, delivery_manager)
  end

  def schedule(series_id)
    series = TestSeries.find(series_id)
    @underlying_module.schedule(series)
  end

  def generate_timetable(series_id)
    series = TestSeries.find(series_id)
    @underlying_module.generate_timetable(series)
  end

  def ack(series_id)
    series = TestSeries.find(series_id)
    @underlying_module.ack(series)
  end

  def series_current_testtime(series_id, testtime_delta)
    series = TestSeries.find(series_id)
    @underlying_module.series_current_testtime(series, testtime_delta)
  end

  def series_enough_runtime(series_id, testtime_delta)
    series = TestSeries.find(series_id)
    @underlying_module.series_enough_runtime(series, testtime_delta)
  end

  def add_jobs(episode_id)
    episode = TestEpisode.find(episode_id)
    @underlying_module.add_jobs(episode)
  end

  def unschedule_jobs(series_id)
    series = TestSeries.find(series_id)
    @underlying_module.unschedule_jobs(series)
  end

  private

  attr_reader :service_name, :standard_proxy_methods, :underlying_module, :configuration_manager
end
