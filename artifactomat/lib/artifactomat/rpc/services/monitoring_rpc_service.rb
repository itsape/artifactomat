# frozen_string_literal: true

require 'artifactomat/models/infrastructure_element'
require 'artifactomat/modules/monitoring'
require 'artifactomat/modules/configuration_manager'
require 'artifactomat/modules/recipe_manager'
require 'artifactomat/modules/subject_manager'
require 'artifactomat/modules/delivery_manager'
require 'artifactomat/rpc/services/general_rpc_service'

# RPC service providing its.apt's infrastructure monitoring
class MonitoringRpcService
  include GeneralRpcService

  def initialize
    @service_name = 'monitoring'
    @standard_proxy_methods = %i[status running? stop run to_str polling_rate]
    @configuration_manager = ConfigurationManager.new
    recipe_manager = RecipeManager.new
    subject_manager = SubjectManager.new
    delivery_manager = DeliveryManager.new(@configuration_manager, recipe_manager, subject_manager)
    @underlying_module = Monitoring.new(@configuration_manager,
                                        recipe_manager,
                                        subject_manager,
                                        delivery_manager)
    @underlying_module.run # automatically start the monitoring when starting the service
  end

  def get_status(ie_yaml)
    ie = YAML.safe_load(ie_yaml, [Symbol, InfrastructureElement])
    status = @underlying_module.get_status(ie)
    { state: status.to_i, error: status.error }
  end

  def subscribe(ie_yaml)
    ie = YAML.safe_load(ie_yaml, [Symbol, InfrastructureElement])
    @underlying_module.subscribe(ie)
  end

  def unsubscribe(ie_yaml)
    ie = YAML.safe_load(ie_yaml, [Symbol, InfrastructureElement])
    @underlying_module.unsubscribe(ie)
  end

  def infrastructure_running?(ie_yaml)
    ie = YAML.safe_load(ie_yaml, [Symbol, InfrastructureElement])
    @underlying_module.infrastructure_running?(ie)
  end

  private

  attr_reader :service_name, :standard_proxy_methods, :underlying_module, :configuration_manager
end
