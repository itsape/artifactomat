# frozen_string_literal: true

# Dummy bullet module
module Bullet
  def self.enable?
    false
  end

  def self.profile
    return unless block_given?

    yield
  end
end

def configure_bullet_for_sinatra
  return unless Bullet.enable?

  configure do
    use Bullet::Rack if Bullet.enable?
  end
end

return if ENV['ARTIFACTOMAT_ENV'] == 'production'

### Here starts the bullet configuration

require 'active_record'
require 'bullet'

Bullet.enable = true
Bullet.bullet_logger = true # log file is log/bullet.log
Bullet.raise = false

puts '++++++ Bullet is enabled!'
