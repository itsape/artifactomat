# frozen_string_literal: true

# Helper. A mutex wrapper for an array.
class MutexedArray
  def initialize(array = [])
    @mutex = Mutex.new
    @array = array
  end

  # rubocop:disable Style/MethodMissingSuper
  def method_missing(method, *args, &block)
    @mutex.synchronize do
      @array.send(method, *args, &block)
    end
  end
  # rubocop:enable Style/MethodMissingSuper

  # :reek:BooleanParameter
  # :reek:ManualDispatch
  def respond_to_missing?(name, flag = true)
    @array.respond_to?(name, flag)
  end

  def to_ary
    @array
  end

  def to_a
    @array
  end
end
