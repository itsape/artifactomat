# frozen_string_literal: true

require 'socket'

require 'artifactomat/modules/logging'

# A helper class to interact with the systemd notification socket
class Systemd
  LOG_SIG = 'Systemd'

  def self.present?
    ENV.key?('NOTIFY_SOCKET')
  end

  def self.ready
    unless present?
      Logging.warning(LOG_SIG, 'No systemd runtime detected (NOTIFY_SOCKET is not set).')
      return
    end

    notify_socket_addr = ENV['NOTIFY_SOCKET']

    Addrinfo.unix(notify_socket_addr, :DGRAM).connect do |sock|
      sock.write('READY=1')
      sock.close
    end
  end
end
