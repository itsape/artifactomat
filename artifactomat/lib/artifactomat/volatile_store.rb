# frozen_string_literal: true

require 'redis'
require 'json'
require 'connection_pool'

require 'artifactomat/modules/logging'

# This Class is a manager for the volatile memory
# :reek:RepeatedConditional
class VolatileStore
  DB_ACTIVE_NAMES = 0 # name => [subjects]
  DB_SUBJECT_IPS = 1 # ip => subject.id OR subject.id => ip
  DB_LOCAL_VARIABLES = 2
  DB_NEWS = 3
  CH_SUBJECT_TRACKER_UPDATES = 'subjecttracker_updates'

  # connections are lazy created.... take some more!
  def initialize(conf_man, pool_size = 50, timeout = 10)
    @redis = ConnectionPool.new(size: pool_size, timeout: timeout) do
      Redis.new(path: conf_man.get('conf.ape.redis_socket').to_s)
    end
  end

  def with_state_saving_connection
    @redis.with do |connection|
      connection.select DB_LOCAL_VARIABLES
      yield connection
    end
  end

  # :reek:NilCheck
  def init_news
    @redis.with do |connection|
      connection.select DB_NEWS
      connection.set('news', JSON.dump([])) if connection.get('news').nil?
    end
  end

  def push_news(msg)
    @redis.with do |connection|
      connection.select DB_NEWS
      news = JSON.parse(connection.get('news'))
      news.push(msg)
      connection.set('news', JSON.dump(news))
    end
  end

  def fetch_and_clear_news
    @redis.with do |connection|
      connection.select DB_NEWS
      old_news = JSON.parse(connection.get('news'))
      connection.set('news', JSON.dump([]))
      old_news
    end
  end

  # called by routing, long_term_memory, subject_tracker
  def ip_by_sid(sid)
    @redis.with do |connection|
      connection.select DB_SUBJECT_IPS
      return connection.get(sid)
    rescue StandardError
      return nil
    end
  end

  # called by subject_tracker, nameserver
  # :reek:NilCheck
  def sids_by_ip(ip)
    @redis.with do |connection|
      connection.select DB_SUBJECT_IPS
      sid_list_str = connection.get(ip)
      sid_list_str = '[]' if sid_list_str.nil?

      return JSON.parse(sid_list_str)
    rescue StandardError
      return []
    end
  end

  # called form subject_tracker
  def remove_from_ip_subject_map(action)
    remove_by_sid(action.subject_id)
  end

  # called form subject_tracker
  def update_ip_subject_map(action)
    remove_by_sid(action.subject_id)
    add_to_ip_subject_map(action.ip, action.subject_id)
  end

  def count_subject_map
    count = 0
    @redis.with do |connection|
      connection.select DB_SUBJECT_IPS
      connection.scan_each(match: '*/*') do
        count += 1
      end
    rescue StandardError
      return count
    end
    count
  end

  def subjects_from_map
    subjects = []
    @redis.with do |connection|
      connection.select DB_SUBJECT_IPS
      connection.scan_each(match: '*/*') do |sid|
        subjects << sid
      end
    rescue StandardError
      return []
    end
    subjects
  end

  def reset
    @redis.with(&:flushall)
    init_news
  end

  # called by routing
  def subscribe_to_subjectracker_updates
    @redis.with do |connection|
      connection.subscribe(CH_SUBJECT_TRACKER_UPDATES) do |on|
        on.message do |_channel, msg|
          msg = JSON.parse(msg)
          yield msg
        end
      end
    end
  end

  # called by nameserver
  def get_registered_subjects(name)
    @redis.with do |connection|
      connection.select DB_ACTIVE_NAMES
      return connection.smembers(name)
    rescue StandardError
      return []
    end
  end

  # called by routing
  def register_subject(name, sid)
    @redis.with do |connection|
      connection.select DB_ACTIVE_NAMES
      return connection.sadd(name, sid)
    rescue StandardError
      return false
    end
  end

  # called by routing
  def unregister_subject(name, sid)
    @redis.with do |connection|
      connection.select DB_ACTIVE_NAMES
      return connection.srem(name, sid)
    rescue StandardError
      return false
    end
  end

  private

  # :reek:NilCheck
  def remove_from_ip_list(connection, ip, sid)
    sid_list_str = connection.get(ip)
    return if sid_list_str.nil?

    sid_list = JSON.parse(sid_list_str)
    sid_list.delete(sid)
    if sid_list.empty?
      connection.del(ip)
    else
      connection.set(ip, JSON.dump(sid_list))
    end
  end

  # called form subject tracker
  # :reek:NilCheck
  def remove_by_sid(sid)
    @redis.with do |connection|
      connection.select DB_SUBJECT_IPS
      ip = connection.get(sid)
      unless ip.nil?
        msg = { action: 'del',
                data: { ip: ip, sid: sid } }.to_json
        connection.del(sid)
        remove_from_ip_list(connection, ip, sid)
        connection.publish(CH_SUBJECT_TRACKER_UPDATES, msg)
      end
    rescue StandardError => e
      Logging.error('SubjectTracker', "can not get ip by subject.id #{sid}: #{e.message}")
    end
  end

  # :reek:NilCheck
  def add_to_ip_list(connection, ip, sid)
    sid_list_str = connection.get(ip)
    sid_list_str = '[]' if sid_list_str.nil?

    sid_list = JSON.parse(sid_list_str)
    sid_list.append(sid)
    connection.set(ip, JSON.dump(sid_list))
  end

  # called form subject tracker
  def add_to_ip_subject_map(ip, sid)
    @redis.with do |connection|
      connection.select DB_SUBJECT_IPS
      connection.set(sid, ip)
      add_to_ip_list(connection, ip, sid)
      msg = { action: 'add', data: { ip: ip, sid: sid } }.to_json
      connection.publish(CH_SUBJECT_TRACKER_UPDATES, msg)
    rescue StandardError => e
      Logging.error('SubjectTracker',
                    "can not save #{ip} => #{sid}: #{e.message}")
    end
  end
end
