# frozen_string_literal: true

require 'artifactomat/models/test_program'
require 'artifactomat/models/test_series'
require 'artifactomat/modules/ape'

# Controller for the ApeServer
module ApeController
  # Controller for TestSeries
  module TestSeriesController
    # Helpers for TestSeriesController
    module Functions
      # :reek:NilCheck
      def check_series(series)
        raise ApeError, "Subject group '#{series.group_file}' does not exist." unless Ape.instance.subject_manager.list_subject_groups.include? series.group_file
      end

      def create_episodes(series)
        tseasons = series.test_seasons
        sgroup = series.group_file
        subjects = Ape.instance.subject_manager.get_subjects(sgroup)
        tseasons.each do |tseason|
          subjects.each do |subject|
            te = tseason.test_episodes.build(subject_id: subject.id)
            te.save
          end
        end
      end

      def destroy_episodes(series)
        seasons = series.test_seasons
        seasons.each do |season|
          season.test_episodes.each(&:destroy)
        end
      end

      def generate_series(series)
        series.preparing!
        ape = Ape.instance
        series_str = series.to_str
        ape.routing.create_routing(series)
        ape.update_job(Thread.current, "Series #{series_str}: generate_artifacts")
        generate_artifacts(series)
        ape.update_job(Thread.current, "Series #{series_str}: generate_infrastructure")
        generate_infrastructure(series)
        msg = "Series #{series_str} fully prepared, you may approve now."
        ape.inform_user(msg)
        series.prepared!
        Logging.info('APE', msg)
      end

      def generate_artifacts(series)
        ape = Ape.instance
        status, out, err = ape.artifact_generator.generate(series)
        return true if status

        ape.routing.remove_routing(series)
        ape.artifact_generator.destroy_artifacts(series)
        Logging.error('APE', "AG: ========== STDOUT ========== #{out}" \
                              "========== STDERR ========== #{err}")
        raise ArtifactGeneratorError, \
              "Series '#{series.to_str}' failed in artifact generation."
      end

      def generate_infrastructure(series)
        ape = Ape.instance
        ape.infrastructure_generator.generate(series)
      rescue InfrastructureRunningError, ScriptExecutionError, InvalidCommandError, TokenError, EnvConversionError, InfrastructureGenerationError => e
        ape.routing.remove_routing(series)
        ape.artifact_generator.destroy_artifacts(series)
        ape.deployment_manager.revoke_artifact_deployment(series)
        Logging.error('APE', "IG: #{e.message}")
        raise
      end

      # rubocop:disable Metrics/AbcSize
      # :reek:NilCheck
      def series_new(attribute_hash, program_id)
        tp = TestProgram.find(program_id) # exception: RecordNotFound
        series = tp.test_series.new(attribute_hash)
        check_series(series)
        raise ActiveRecord::RecordNotSaved, series.errors.full_messages unless series.save!

        sched = Ape.instance.test_scheduler
        begin
          message = if series.end_date.nil?
                      'Series created'
                    else
                      rem_testtime = sched.series_enough_runtime(series)
                      "Series created. Remaining series testtime: #{sched.format_duration(rem_testtime)}"
                    end
        rescue StandardError
          series.destroy!
          raise
        end

        Logging.info('APE', "Series created: #{series.attributes}")
        message
      end

      # :reek:NilCheck
      def series_edit(attribute_hash, series_id)
        series = TestSeries.find series_id # exception RecordNotFound
        raise ApeError, 'series already approved' unless series.schedulable?

        series.assign_attributes(attribute_hash)
        check_series(series)
        changes = series.changes
        raise ActiveRecord::RecordNotSaved, series.errors.full_messages unless series.save!

        sched = Ape.instance.test_scheduler
        begin
          message = if series.end_date.nil?
                      'Series updated'
                    else
                      rem_testtime = sched.series_enough_runtime(series)
                      "Series updated. Remaining series testtime: #{sched.format_duration(rem_testtime)}"
                    end
        rescue StandardError
          rollback_series(series, changes)
          raise
        end

        Logging.info('APE', "Series updated: #{series.attributes}")
        message
      end
      # rubocop:enable Metrics/AbcSize

      def series_list
        ret = []
        TestSeries.find_each do |series|
          ret.push series.attributes
        end
        ret
      end

      def series_show(id)
        ts = TestSeries.find id
        Ape.instance.deflate_series(ts)
      end

      def series_delete(id)
        series = TestSeries.find id
        raise ApeError, 'series already approved' unless series.schedulable?
        raise ActiveRecord::RecordNotSaved, series.errors.full_messages unless series.destroy

        Logging.info('APE', "Series deleted: #{series.to_str}")
      end

      def series_schedule(attribute_hash, series_id)
        series = TestSeries.find series_id # throws RecordNotFound
        raise ApeError, "series #{series.to_str} not schedulable any more" unless series.schedulable?

        check_conflicting_series(series) unless attribute_hash['force']
        destroy_episodes(series)
        create_episodes(series)
        series.save!
        Ape.instance.test_scheduler.schedule(series)
        Logging.info('APE', "Series scheduled: #{series.to_str}")
      end

      def timetable(id)
        series = TestSeries.find id # throws RecordNotFound
        Ape.instance.test_scheduler.generate_timetable(series)
      end

      def series_approve(series_id)
        series = TestSeries.find series_id
        raise ApeError, "series #{series.to_str} not prepared" unless series.prepared?

        announce_series_approval(series)
        series.approved!
        Logging.info('APE', "Series acknowledged: #{series.to_str}")
      end

      def series_prepare(id)
        series = TestSeries.find(id) # throws RecordNotFound
        raise ApeError, "series #{series.to_str} not scheduled yet" unless series.scheduled?

        Thread.start do
          generate_series(series)
        rescue StandardError => e
          msg = "Generating series #{series.to_str} failed with: #{e.message}"
          Ape.instance.inform_user(msg)
        end
      end

      # rubocop:disable Metrics/AbcSize
      # :reek:DuplicateMethodCall
      def series_abort(series_id)
        series = TestSeries.find series_id
        Logging.debug('APE', "Aborting series #{series.to_str}.")
        series.test_seasons.each do |season|
          Ape.instance.delivery_manager.lock_season(season)
        end
        Ape.instance.deployment_manager.withdraw(series)
        Ape.instance.track_collector.unregister_series(series)
        Ape.instance.unschedule_jobs(series)
        Logging.info('APE', "Series #{series.to_str}: aborted")
      end

      private

      def announce_series_approval(series)
        ape = Ape.instance
        ape.subject_tracker.activate_series(series.id)
        ape.track_collector.register_series(series)
        series.save!
        ape.test_scheduler.ack(series)
      end

      def rollback_series(series, changes)
        attr_hash = {}
        changes.each do |attr, change|
          attr_hash[attr] = change[0]
        end
        series.assign_attributes(attr_hash)
        series.save
      end

      def get_conflicting_series(series)
        conflicting = []
        overlapping_series = TestSeries.where('start_date < ? AND end_date > ? AND ID != ?', series.end_date, series.start_date, series.id).to_a
        overlapping_series.reject!(&:fresh?)
        overlapping_series.each do |os|
          overlapping_recipes = find_overlapping_recipes series, os
          overlapping_recipes.each do |recipe|
            conflicting << [os.id, recipe]
          end
        end
        conflicting
      end

      def find_overlapping_recipes(series1, series2)
        recipes1 = series1.test_seasons.to_a.map(&:recipe_id)
        recipes2 = series2.test_seasons.to_a.map(&:recipe_id)
        recipes1 & recipes2
      end

      def check_conflicting_series(series)
        conflicting_series = get_conflicting_series series
        return unless conflicting_series.size.positive?

        msg = +"The following scheduled series are conflicting:\n"
        conflicting_series.each do |cs|
          msg << "Series '#{cs[0]}' also uses recipe '#{cs[1]}'\n"
        end
        msg << "Running the same recipe multiple times at once can lead to malfunctions!\nIf you really want to schedule, use `ape series schedule -f #{series.id}`"
        raise ApeError, msg
      end
    end
    # rubocop:enable Metrics/AbcSize

    # rubocop:disable Metrics/AbcSize, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity
    # This is ridiculous ... f-u@rubocop
    # :reek:DuplicateMethodCall
    def self.registered(ape_server)
      ape_server.helpers TestSeriesController::Functions
      ape_server.post '/series/new/:program_id' do
        message = series_new(@request_data, params[:program_id])
        [201, message] # created
      rescue ActiveRecord::UnknownAttributeError => e
        [422, e.message] # semantic_error
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      rescue ActiveRecord::RecordNotSaved => e
        [500, e.message] # server error
      rescue ApeError => e
        [422, e.message] # semantic_error
      end

      ape_server.post '/series/edit/:series_id' do
        message = series_edit(@request_data, params[:series_id])
        [200, message] # no_content
      rescue ActiveRecord::UnknownAttributeError => e
        [422, e.message] # semantic_error
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      rescue ActiveRecord::RecordNotSaved => e
        [500, e.message] # server error
      rescue ApeError => e
        [422, e.message] # semantic_error
      end

      ape_server.post '/series/schedule/:series_id' do
        series_schedule @request_data, params[:series_id]
        204 # no_content
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      rescue ApeError => e
        [422, e.message] # semantic_error
      end

      ape_server.get '/series/list' do
        [200, series_list]
      end

      ape_server.get '/series/show/:series_id' do
        [200, series_show(params[:series_id])]
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      end

      ape_server.get '/series/timetable/:series_id' do
        [200, timetable(params[:series_id])]
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      end

      ape_server.get '/series/delete/:series_id' do
        series_delete params[:series_id]
        204
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      rescue ApeError => e
        [422, e.message] # semantic_error
      rescue ActiveRecord::RecordNotSaved => e
        [500, e.message] # server_error
      end

      ape_server.get '/series/approve/:series_id' do
        series_approve params[:series_id]
        204 # no_content
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      rescue ApeError => e
        [422, e.message] # semantic_error
      rescue SchedulerError => e
        [500, e.message] # server_error
      end

      ape_server.get '/series/prepare/:series_id' do
        series_prepare params[:series_id]
        204 # no_content
      rescue ApeError => e
        [422, e.message] # semantic_error
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      end

      ape_server.get '/series/abort/:series_id' do
        series_abort params[:series_id]
        204 # no_content
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      rescue SchedulerError => e
        [500, e.message] # server_error
      end
    end
    # rubocop:enable Metrics/AbcSize, Metrics/PerceivedComplexity, Metrics/CyclomaticComplexity
  end
end
