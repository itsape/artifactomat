# frozen_string_literal: true

require 'artifactomat/modules/result_generator'
require 'artifactomat/modules/ape'

# Ape Controller
module ApeController
  # Controller for results
  module ResultController
    # rubocop:disable Metrics/AbcSize
    # :reek:DuplicateMethodCall
    def self.registered(ape_server)
      ape_server.post '/report/series/:id' do
        [200, Ape.instance.result_generator.report_series(params[:id], @request_data['headers'], @request_data['out'], @request_data['format'])]
      rescue ResultError => e
        [404, e.message]
      end

      ape_server.post '/report/online' do
        [200, Ape.instance.result_generator.report_online(@request_data['format'])]
      end

      ape_server.post '/report/detection' do
        [200, Ape.instance.result_generator.report_detection(@request_data['format'])]
      end
    end
    # rubocop:enable Metrics/AbcSize
  end
end
