# frozen_string_literal: true

# Controller for the ApeServer
module ApeController
  # Controller for TestSeasons
  module TestSeasonController
    # Helpers for TestSeasons
    module Functions
      def recipe_exists?(recipe_id)
        Ape.instance.recipe_manager.list_recipes.include? recipe_id
      end

      def recipe_requirements_met?(season)
        status, output = Ape.instance.artifact_generator.check_season(season)
        raise ApeError, "Requirements for recipe '#{season.recipe_id}' not met: #{output}" unless status.zero?
      end

      def get_params(season)
        r = Ape.instance.recipe_manager.get_recipe(season.recipe_id)
        parameters = r.default_parameters
        r.parameters.each_key do |key|
          parameters[key.to_s] = season.recipe_parameters[key.to_sym] if season.recipe_parameters.key? key.to_sym
        end
        parameters
      end

      # :reek:NilCheck
      def check_season(season)
        raise ApeError, "Recipe '#{season.recipe_id}' does not exist." unless recipe_exists? season.recipe_id

        season.recipe_parameters = get_params(season)
        recipe_requirements_met? season
      end

      def ensure_screenshot_folder_exists(screenshots_folder, recipe_directory)
        absolute_screenshots_path = File.absolute_path(File.join(recipe_directory, screenshots_folder))
        raise ApeError, "#{absolute_screenshots_path} is not a valid directory" unless File.directory?(absolute_screenshots_path)
      end

      def check_new_screenshots_folder(attribute_hash)
        return unless attribute_hash.key?('recipe_id') # will raise an AttributeError on model creation

        recipe_directory = File.join(Ape.instance.configuration_manager.cfg_path,
                                     'recipes',
                                     attribute_hash['recipe_id'])
        if attribute_hash.key?('screenshots_folder')
          return if attribute_hash['screenshots_folder'] == ''

          ensure_screenshot_folder_exists(attribute_hash['screenshots_folder'], recipe_directory)
        else
          full_screenshots_path = File.join(recipe_directory, 'screenshots')
          attribute_hash['screenshots_folder'] = if File.directory?(full_screenshots_path)
                                                   'screenshots'
                                                 else
                                                   ''
                                                 end
        end
      end

      def check_edited_screenshots_folder(attribute_hash, season)
        return unless attribute_hash.key?('recipe_id') || attribute_hash.key?('screenshots_folder')

        # We can not know whether the current value for `season.screenshots_folder` has been
        # explicitly set by the user, or inferred as a default value. Therefore we will keep the old
        # screenshots_folder unless explicitly overwritten, and won't try to infer it again.
        screenshots_folder = attribute_hash.fetch('screenshots_folder', season.screenshots_folder)
        return if screenshots_folder == ''

        recipe_id = attribute_hash.fetch('recipe_id', season.recipe_id)
        recipe_directory = File.absolute_path(File.join(
                                                Ape.instance.configuration_manager.cfg_path,
                                                'recipes',
                                                recipe_id
                                              ))
        ensure_screenshot_folder_exists(screenshots_folder, recipe_directory)
      end

      # :reek:NilCheck
      # rubocop:disable Metrics/AbcSize
      def season_new(attribute_hash, series_id)
        series = TestSeries.find series_id # exception RecordNotFound
        raise ApeError, 'series already approved' unless series.schedulable?

        check_new_screenshots_folder(attribute_hash)
        season = series.test_seasons.new(attribute_hash) # UnknownAttributeError
        check_season(season)
        sched = Ape.instance.test_scheduler
        raise ActiveRecord::RecordNotSaved, season.errors.full_messages unless season.save

        begin
          if series.end_date.nil?
            cur_testtime, end_date = sched.series_current_testtime(series)
            message = +"Season created. Current series testtime: #{sched.format_duration(cur_testtime)}. Earliest end date: #{end_date}"
          else
            rem_testtime = sched.series_enough_runtime(series)
            message = +"Season created. Remaining series testtime: #{sched.format_duration(rem_testtime)}"
          end
        rescue StandardError
          season.destroy!
          raise
        end

        series.fresh!
        Logging.info('APE', "Season created: #{season.attributes}")

        message << determine_conflicting_series(series, attribute_hash['recipe_id'])
        message
      end

      # :reek:DuplicateMethodCall
      # :reek:NilCheck
      def season_edit(attribute_hash, id)
        season = TestSeason.find id # exception RecordNotFound
        raise ApeError, 'series already approved' unless season.test_series.schedulable?

        check_edited_screenshots_folder(attribute_hash, season)
        season.assign_attributes attribute_hash # UnknownAttributeError
        check_season(season)
        sched = Ape.instance.test_scheduler
        series = season.test_series
        changes = season.changes
        raise ActiveRecord::RecordNotSaved, season.errors.full_messages unless season.save

        begin
          if series.end_date.nil?
            cur_testtime, end_date = sched.series_current_testtime(series)
            message = +"Season edited. Current series testtime: #{sched.format_duration(cur_testtime)}. Earliest end date: #{end_date}"
          else
            rem_testtime = sched.series_enough_runtime(series)
            message = +"Season edited. Remaining series testtime: #{sched.format_duration(rem_testtime)}"
          end
        rescue StandardError
          rollback_season(season, changes)
          raise
        end

        season.test_series.fresh!
        Logging.info('APE', "Season edited: #{season.attributes}")
        message << determine_conflicting_series(season.test_series, attribute_hash['recipe_id'])
        message
      end
      # rubocop:enable Metrics/AbcSize

      def season_delete(id)
        ts = TestSeason.find(id.to_i)
        raise ApeError, 'series already approved' unless ts.test_series.schedulable?
        raise ActiveRecord::RecordNotSaved, ts.errors.full_messages unless ts.destroy

        Logging.info('APE', "Season deleted #{ts.id}")
      end

      def season_list
        ret = []
        TestSeason.find_each do |ts|
          ret.push(ts.attributes)
        end
        ret
      end

      def season_show(id)
        ts = TestSeason.find(id)
        Ape.instance.deflate_season(ts)
      end

      private

      def determine_conflicting_series(series, recipe_id)
        message = +''
        conflicting_series = find_conflicting_series(recipe_id, series)
        if conflicting_series.size.positive?
          message << "\nWarning: The following series also include this recipe:"
          conflicting_series.each do |conf_series|
            message << "\n\t'#{conf_series.label}' with ID '#{conf_series.id}'"
          end
          message << "\nA recipe running at the same time in different series can cause problems!"
        end
        message
      end

      def rollback_season(season, changes)
        attr_hash = {}
        changes.each do |attr, change|
          attr_hash[attr] = if attr == 'recipe_parameters'
                              YAML.safe_load(change[0], [Symbol, ActiveSupport::HashWithIndifferentAccess])
                            else
                              change[0]
                            end
        end
        season.assign_attributes(attr_hash)
        season.save
      end

      # :reek:NilCheck
      def find_conflicting_series(recipe_id, test_series)
        start_date = test_series.start_date
        end_date = test_series.end_date
        candidates = if end_date.nil?
                       TestSeries.where('end_date IS NULL OR end_date > ?', start_date).to_a
                     else
                       TestSeries.where('start_date < ? AND (end_date IS NULL OR end_date > ?)', end_date, start_date).to_a
                     end
        candidates.select! { |candidate| includes_recipe?(candidate, recipe_id) }
        candidates.reject { |candidate| candidate.id == test_series.id }
      end

      # :reek:ControlParameter
      def includes_recipe?(test_series, recipe_id)
        test_series.test_seasons.each do |season|
          return true if season.recipe_id == recipe_id
        end
        false
      end
    end

    # rubocop:disable Metrics/AbcSize
    # This is ridiculous ... f-u@rubocop
    # :reek:DuplicateMethodCall
    def self.registered(ape_server)
      ape_server.helpers TestSeasonController::Functions
      ape_server.post '/season/new/:series_id' do
        message = season_new(@request_data, params[:series_id])
        [201, message] # created
      rescue ActiveRecord::UnknownAttributeError => e
        [422, e.message] # semantic_error
      rescue ActiveRecord::RecordNotSaved => e
        [500, e.message]
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      rescue ApeError => e
        [422, e.message] # semantic_error
      end

      ape_server.post '/season/edit/:season_id' do
        message = season_edit(@request_data, params[:season_id])
        [200, message]
      rescue ActiveRecord::UnknownAttributeError => e
        [422, e.message] # semantic_error
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      rescue ActiveRecord::RecordNotSaved => e
        [500, e.message]
      rescue ApeError => e
        [422, e.message] # semantic_error
      end

      ape_server.get '/season/list' do
        [200, season_list]
      end

      ape_server.get '/season/show/:season_id' do
        [200, season_show(params[:season_id])]
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      end

      ape_server.get '/season/delete/:season_id' do
        season_delete params[:season_id]
        204
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message] # not_found
      rescue ApeError => e
        [422, e.message] # semantic_error
      rescue ActiveRecord::RecordNotSaved => e
        [500, e.message] # server_error
      end
    end
    # rubocop:enable Metrics/AbcSize
  end
end
