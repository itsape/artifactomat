# frozen_string_literal: true

require 'artifactomat/modules/ape'
require 'artifactomat/modules/logging.rb'

require 'artifactomat/models/test_program'

# Controller for the ApeServer
module ApeController
  # Controller for TestPrograms
  module TestProgramController
    # Helper for TestProgramController
    module Functions
      def program_new(attribute_hash)
        tp = TestProgram.new(attribute_hash)
        raise ActiveRecord::RecordNotSaved, tp.errors.full_messages unless tp.save

        Logging.info('APE', "Program created: #{tp.attributes}")
      end

      def program_edit(attribute_hash, id)
        tp = TestProgram.find id
        tp.assign_attributes attribute_hash
        raise ActiveRecord::RecordNotSaved, tp.errors.full_messages unless tp.save

        Logging.info('APE', "Program changed: #{tp.attributes}")
      end

      def program_list
        ret = []
        TestProgram.find_each do |tp|
          ret.push tp.attributes
        end
        ret
      end

      def program_show(id)
        tp = TestProgram.find id
        Ape.instance.deflate_program(tp)
      end

      def program_delete(id)
        tp = TestProgram.find id
        tp.test_series.each do |ts|
          raise ApeError, "Unable to delete Program #{tp.to_str}, the program contains prepared but uncompleted series." unless ts.completed? || ts.schedulable?
        end
        raise ActiveRecord::RecordNotSaved, tp.errors.full_messages unless tp.destroy

        Logging.info('APE', "Program deleted: #{tp.attributes}")
      end
    end

    # rubocop:disable Metrics/AbcSize
    # This is ridiculous ... f-u@rubocop
    # :reek:DuplicateMethodCall
    def self.registered(ape_server)
      ape_server.helpers TestProgramController::Functions

      ape_server.post '/program/new' do
        program_new(@request_data)
        201
      rescue ActiveRecord::RecordNotSaved => e
        [500, e.message]
      rescue ActiveRecord::UnknownAttributeError => e
        [422, e.message]
      end

      ape_server.post '/program/edit/:id' do
        program_edit(@request_data, params[:id])
        204
      rescue ActiveRecord::RecordNotSaved => e
        [500, e.message]
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message]
      rescue ActiveRecord::UnknownAttributeError => e
        [422, e.message]
      end

      ape_server.get '/program/list' do
        [200, program_list]
      end

      ape_server.get '/program/show/:id' do
        [200, program_show(params[:id])]
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message]
      end

      ape_server.get '/program/delete/:id' do
        program_delete params[:id]
        204
      rescue ActiveRecord::RecordNotSaved => e
        [500, e.message]
      rescue ApeError => e
        [409, e.message]
      rescue ActiveRecord::RecordNotFound => e
        [404, e.message]
      end
    end
    # rubocop:enable Metrics/AbcSize
  end
end
