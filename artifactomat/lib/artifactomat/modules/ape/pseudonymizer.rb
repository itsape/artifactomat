# frozen_string_literal: true

# maps pseudonyms of one groupfile to pseudonyms of the other
class Pseudonymizer
  def self.pseudonymize(args, subject_manager)
    group1 = args['group1']
    group2 = args['group2']
    subject_groups = subject_manager.list_subject_groups

    return "#{group1} is unknown" unless subject_groups.include? group1
    return "#{group2} is unknown" unless subject_groups.include? group2

    match_pseudonyms(group1, group2, subject_manager)
  end

  def self.match_pseudonyms(group1, group2, subject_manager)
    subjects1 = subject_manager.get_subjects(group1)
    subjects2 = subject_manager.get_subjects(group2)
    subject_map = {}

    subjects1.each do |subject|
      candidates = subjects2.select { |subject2| subject2.userid == subject.userid }
      subject_map[subject.id] = candidates.map(&:id)
    end
    subject_map
  end
end
