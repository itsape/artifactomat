# frozen_string_literal: false

require 'gli'
require 'yaml'
require 'chronic'
require 'fugit'
require 'artifactomat/modules/ape/banana'
require 'artifactomat/modules/client_builder'
require 'active_support/hash_with_indifferent_access'

# rubocop:disable Metrics/BlockLength
# :reek:RepeatedConditional

# The Bash interface to Banana
class ApeCli
  extend GLI::App

  # :reek:NilCheck
  def self.get_time(time)
    real_time = Chronic.parse(time, ambiguous_time_range: :none)
    real_time&.utc
  end

  # :reek:NilCheck
  def self.get_duration(duration)
    dur = Fugit.parse_duration duration
    dur&.to_sec&.div 60
  end

  program_desc 'The cli to the its.ape framework'
  version Banana::API_VERSION

  subcommand_option_handling :normal
  arguments :strict

  accept(Hash) do |value|
    result = {}
    value.split(/,/).each do |pair|
      k, v = pair.split(/:/)
      result[k] = v
    end
    ActiveSupport::HashWithIndifferentAccess.new result
  end

  flag %i[i ip], desc: 'The IP of ape', default_value: 'localhost'
  flag %i[p port], desc: 'The port of ape', default_value: '8765'

  pre do |global_options, _command, _options, _args|
    @banana = Banana.new(global_options[:ip], global_options[:p].to_i)
  end

  desc 'Query status of the framework modules'
  command :status do |cmd|
    cmd.action do
      print @banana.status
    end
  end

  desc 'Reset the volatile store. This resets the news, the saved links between subjects and their IPs, and the dns redirects for specific subjects. Running this while you have prepared or approved series may break their execution.'
  command :reset do |cmd|
    cmd.switch :f, :force, desc: 'Force reset, even if there are prepared or approved series', default_value: false
    cmd.action do |_global_options, options, _args|
      print @banana.reset(force: options[:force])
    end
  end

  desc 'Query information from the framework modules'
  command :info do |cmd|
    cmd.flag %i[i interval],
             desc: 'The query interval in seconds'
    cmd.action do |_global_options, options, _args|
      loop do
        print_infos @banana.info
        break if options[:interval].nil?

        sleep options[:interval].to_i
      end
    end
  end

  desc 'Output an installer for the windows client, that is preconfigured with this installations certificates'
  command :client do |cmd|
    cmd.flag %i[o outfile],
             default_value: './ITSAPE.Client.msi',
             desc: 'The path where to place the installer.'
    cmd.action do |_global_options, options, _args|
      puts 'Rebuilding and repacking the windows-client, if necessary. This might take up to two minutes.'

      success, resp = @banana.build_client
      exit_now!(resp) unless success

      installer_path = YAML.safe_load(resp)[0]
      begin
        FileUtils.cp(installer_path, options['outfile'])
      rescue Errno::EACCES
        puts "Could not place the installer at your requested location, you can copy it from #{installer_path}"
      else
        puts "Successfully built the installer at #{options['outfile']}"
      end
    end
  end

  desc 'Query [armed_episodes|locked_seasons|infrastructure|tracked_subjects|active_subjects]'
  arg 'TOKEN'
  command :query do |cmd|
    cmd.action do |_global_options, _options, args|
      print @banana.query(id: args.first)
    end
  end

  desc 'Manage recipes'
  command :recipe do |recipe_cmd|
    recipe_cmd.desc 'List loaded recipes'
    recipe_cmd.command :list do |cmd_list|
      cmd_list.action do |_global_options, _options, _args|
        print @banana.recipe_list
      end
    end

    recipe_cmd.desc 'List parameters of recipe with ID'
    recipe_cmd.arg 'ID'
    recipe_cmd.command :parameters do |cmd_param|
      cmd_param.action do |_global_options, _options, args|
        print @banana.recipe_parameters(id: args.first)
      end
    end
  end

  desc 'Manage traces'
  command :import do |import_cmd|
    import_cmd.desc 'Import traces to the framework'
    import_cmd.switch %i[d dry_run], default_value: false, negatable: false, desc: 'Don\'t save results to database.'
    import_cmd.flag %i[t tz_offset], default_value: 0, desc: 'Set a timezone offset in hours, e.g. +1, 4, -5, etc. This will shift all timestamps found within FILE. Use this if your traces have no timezone value and do not match the locale of this machine.'
    import_cmd.flag %i[s series], desc: 'Only import for given Series (ID or label).'

    import_cmd.desc 'Import traces into the framework. Useful for inserting missed traces after runtime.'
    import_cmd.arg 'FILE'
    import_cmd.command :trace do |cmd_trace|
      cmd_trace.action do |_global_options, options, args|
        file = File.open(args.first, 'r')
        contents = file.read
        args_list = {
          'trace' => contents,
          'dry_run' => options[GLI::Command::PARENT][:dry_run],
          'tz_offset' => options[GLI::Command::PARENT][:tz_offset],
          'series' => options[GLI::Command::PARENT][:series]
        }
        res = @banana.import_trace(args_list)
        entries = YAML.safe_load(res[1]).first

        if res[0] # if positive status code
          STDERR.write "Read #{contents.lines.size} line(s), #{entries.lines.size - 1} trace(s) found.\n"
          STDOUT.write entries # TrackEntries as csv on STDOUT
        else
          print res
        end
      end
    end

    import_cmd.desc 'Use this function ONLY to import helpdesk escalations to the framework. '\
                    'Provide a RegEx which matches lines from FILE and map attributes using named capture groups with --format. A best-effort approach is performed when --format is not used.'
    import_cmd.arg 'FILE'
    import_cmd.command :helpdesk do |cmd_helpdesk|
      cmd_helpdesk.flag %i[s score], default_value: '-1', desc: 'Score of helpdesk interaction.'
      cmd_helpdesk.flag %i[f format],
                        desc: "Provide a detailed RegEx with named capture groups to define where all relevant attributes are present within a line in FILE. Supported attributes\n\n"\
                        "- to match subjects: attributes defined within groupfile, prefixed with 'subject_'\n\n"\
                        "- to match timestamp: 'time' - Uses Time.parse, best effort. [formatstring characters] - Define how a timestamp is represented using the usual formatstrings, e.g. '%H:%M:%S'. Best see example for how to use it.\n\n"\
                        'Example: "(?<%a %b %d %H:%M:%S %Y>[\w\s:]+), (?<subject_id>[\w\.\/]+), .+"'
      cmd_helpdesk.action do |_global_options, options, args|
        file = File.open(args.first, 'r')
        contents = file.read
        args_list = {
          'trace' => contents,
          'dry_run' => options[GLI::Command::PARENT][:dry_run],
          'tz_offset' => options[GLI::Command::PARENT][:tz_offset],
          'series' => options[GLI::Command::PARENT][:series],
          'score' => options['score'],
          'format' => options['format']
        }
        res = @banana.import_helpdesk(args_list)
        entries = YAML.safe_load(res[1]).first

        if res[0] # if positive status code
          STDERR.write "Read #{contents.lines.size} line(s), #{entries.lines.size - 1} trace(s) found.\n"
          STDOUT.write entries # TrackEntries as csv on STDOUT
        else
          print res
        end
      end
    end
  end

  desc 'Manage subject groups'
  command :subject do |subject_cmd|
    subject_cmd.desc 'List loaded subject groups'
    subject_cmd.command :list do |cmd_list|
      cmd_list.action do |_global_options, _options, _args|
        print @banana.subject_list
      end
    end
  end

  desc 'Manage results'
  command :report do |report_cmd|
    report_cmd.desc 'Get reports'
    report_cmd.arg 'SERIES_ID'
    report_cmd.arg 'HEADER', :optional
    report_cmd.command :series do |series_cmd|
      series_cmd.flag %i[f format], default_value: 'yaml', desc: 'yaml/csv'
      series_cmd.flag %i[o out-file], desc: 'filepath to write output to'

      available_headers = %w[id test_episode_id season_id season_label series_id series_label subject_id recipe score course_of_action online_time timestamp]
      default_headers = %w[subject_id recipe score course_of_action online_time]
      series_cmd.desc 'Get results by SERIES_ID. With HEADER, adjust output. Default headers are: ' \
                      "[#{default_headers.join(', ')}]. All available headers are: [#{available_headers.join(', ')}]"
      series_cmd.action do |_global_options, options, args|
        res = @banana.report_series(id: args.first, headers: [default_headers, args[1]], out: options[:o], format: options[:f])
        # The reports are strings that get converted to yaml, but we want the pure strings
        if res[0] # successful http status code
          report = YAML.safe_load(res[1])
          puts report
        else
          print res
        end
      end
    end

    report_cmd.desc 'Get online subjects'
    report_cmd.command :online do |online_cmd|
      online_cmd.flag %i[f format], default_value: 'yaml', desc: 'yaml/csv'

      online_cmd.desc 'ToDo'
      online_cmd.action do |_global_options, options, _args|
        res = @banana.report_online(format: options['format'])
        # The reports are strings that get converted to yaml, but we want the pure strings
        if res[0] # successful http status code
          report = YAML.safe_load(res[1])
          puts report
        else
          print res
        end
      end
    end

    report_cmd.desc 'Get detection events'
    report_cmd.command :detection do |detection_cmd|
      detection_cmd.flag %i[f format], default_value: 'yaml', desc: 'yaml/csv'
      detection_cmd.action do |_global_options, options, _args|
        res = @banana.report_detection(format: options['format'])
        # The reports are strings that get converted to yaml, but we want the pure strings
        if res[0] # successful http status code
          report = YAML.safe_load(res[1])
          puts report
        else
          print res
        end
      end
    end
  end

  desc 'Generate a csv of subjects and selected attributes (columns) from a groupfile. '\
       'GROUPFILE is the filename of the groupfile. Must be currently loaded by ape. '\
       'COLUMNS are the headers of additional columns you want to export. Pseudonyms are always exported.'
  arg 'GROUPFILE'
  arg 'COLUMNS', %i[multiple optional]
  command :export do |cmd|
    cmd.action do |_global_options, _options, args|
      groupfile = args[0]
      columns = args.drop(1)
      res = @banana.export(groupfile: groupfile, columns: columns)
      if res[0] # if positive status code
        exported = YAML.safe_load(res[1])
        puts exported # exported subjects as csv
      else
        print res
      end
    end
  end

  desc 'Manage test programs'
  command :program do |program|
    program.desc 'Edit a existing test program'
    program.arg 'ID'
    program.command :edit do |cmd_edit|
      cmd_edit.flag %i[l label], required: true,
                                 desc: 'The new label of the program with given ID'
      cmd_edit.action do |_global_options, options, args|
        print @banana.program_edit(label: options[:label], id: args.first)
      end
    end

    program.desc 'Create a new test program'
    program.command :new do |cmd_new|
      cmd_new.flag %i[l label], required: true,
                                desc: 'The label of the new program with given ID'
      cmd_new.action do |_global_options, options, _args|
        print @banana.program_new(label: options[:label])
      end
    end

    program.desc 'List all programs'
    program.command :list do |cmd_list|
      cmd_list.action do |_global_options, _options, _args|
        print @banana.program_list
      end
    end

    program.desc 'Show program with given ID'
    program.arg 'ID'
    program.command :show do |cmd_show|
      cmd_show.action do |_global_options, _options, args|
        print @banana.program_show(id: args.first)
      end
    end

    program.desc 'Delete program with given ID'
    program.arg 'ID'
    program.command :delete do |cmd_delete|
      cmd_delete.action do |_global_options, _options, args|
        print @banana.program_delete(id: args.first)
      end
    end
  end

  desc 'Manage test seasons'
  command :season do |season|
    season.desc 'Edit a existing test season'
    season.arg 'ID'
    season.command :edit do |cmd_edit|
      cmd_edit.flag %i[l label], desc: 'The new label of the season with given ID'
      cmd_edit.flag %i[d duration], desc: 'The new duration of the season with given ID'
      cmd_edit.flag %i[r recipe], desc: 'The new recipe of the season with given ID'
      cmd_edit.flag %i[s screenshots-folder], desc: 'The new screenshot folder location of the season with given ID, relative to the recipe directory'
      cmd_edit.flag %i[p parameters], desc: 'The new recipe parameters of the season with given ID like some:value,another:valu', type: Hash
      cmd_edit.action do |_global_options, options, args|
        data = { id: args.first }
        data['label'] = options[:label] unless options[:label].nil?
        data['recipe_id'] = options[:recipe] unless options[:recipe].nil?
        data['recipe_parameters'] = options[:parameters] unless options[:parameters].nil?
        data['duration'] = get_duration(options[:duration]) unless options[:duration].nil?
        data['screenshots_folder'] = options[:'screenshots-folder'] unless options[:'screenshots-folder'].nil?
        print @banana.season_edit(data)
      end
    end

    season.desc 'Create test season'
    season.arg 'SERIES_ID'
    season.command :new do |cmd_new|
      cmd_new.flag %i[l label], required: true, desc: 'The label of the new season'
      cmd_new.flag %i[d duration], required: true, desc: 'The duration of the new season, e.g. 2h'
      cmd_new.flag %i[r recipe], required: true, desc: 'The recipe of the new season'
      cmd_new.flag %i[s screenshots-folder], required: false, desc: 'The screenshots folder of the new season, given relative to the recipe directory.'
      cmd_new.flag %i[p parameters], desc: 'The recipe parameters of the new season like some:value,another:value',
                                     type: Hash,
                                     default_value: ActiveSupport::HashWithIndifferentAccess.new
      cmd_new.action do |_global_options, options, args|
        data = { id: args.first }
        data['label'] = options[:label]
        data['duration'] = get_duration options[:duration]
        data['recipe_id'] = options[:recipe]
        data['recipe_parameters'] = options[:parameters]
        data['screenshots_folder'] = options[:'screenshots-folder'] unless options[:'screenshots-folder'].nil?
        print @banana.season_new(data)
      end
    end

    season.desc 'List all existing test seasons'
    season.command :list do |cmd_list|
      cmd_list.action do
        print @banana.season_list
      end
    end

    season.desc 'Show season with given ID'
    season.arg 'ID'
    season.command :show do |cmd_show|
      cmd_show.action do |_global_options, _options, args|
        print @banana.season_show(id: args.first)
      end
    end

    season.desc 'Delete season with given ID'
    season.arg 'ID'
    season.command :delete do |cmd_delete|
      cmd_delete.action do |_global_options, _options, args|
        print @banana.season_delete(id: args.first)
      end
    end
  end

  desc 'Manage test series'
  command :series do |series|
    series.desc 'Create new test series'
    series.arg 'PROGRAM_ID'
    series.command :new do |cmd_new|
      cmd_new.flag %i[l label], required: true, desc: 'The label of the new series'
      cmd_new.flag %i[g group], required: true, desc: 'The group of the new series'
      cmd_new.flag %i[s start], required: true, desc: 'The start date of the new series'
      cmd_new.flag %i[e end], desc: 'The end date of the new series'
      cmd_new.action do |_global_options, options, args|
        data = { id: args.first }
        data['label'] = options[:label] unless options[:label].nil?
        data['group_file'] = options[:group] unless options[:group].nil?
        data['start_date'] = get_time options[:start]
        data['end_date'] = get_time(options[:end]) unless options[:end].nil?
        print @banana.series_new(data)
      end
    end

    series.desc 'Edit test series'
    series.arg 'ID'
    series.command :edit do |cmd_edit|
      cmd_edit.flag %i[l label], desc: 'The new label of the series with ID'
      cmd_edit.flag %i[g group], desc: 'The new group of the series with ID'
      cmd_edit.flag %i[s start], desc: 'The new start date of the series with ID'
      cmd_edit.flag %i[e end], desc: 'The new end date of the series with ID'
      cmd_edit.action do |_global_options, options, args|
        data = { id: args.first }
        data['label'] = options[:label] unless options[:label].nil?
        data['group_file'] = options[:group] unless options[:group].nil?
        data['start_date'] = get_time(options[:start]) unless options[:start].nil?
        data['end_date'] = get_time(options[:end]) unless options[:end].nil?
        print @banana.series_edit(data)
      end
    end

    series.desc 'List all existing test series'
    series.command :list do |cmd_list|
      cmd_list.action do
        print @banana.series_list
      end
    end

    series.desc 'Show series with given ID'
    series.arg 'ID'
    series.command :show do |cmd_show|
      cmd_show.action do |_global_options, _options, args|
        print @banana.series_show(id: args.first)
      end
    end

    series.desc 'Delete series with given ID'
    series.arg 'ID'
    series.command :delete do |cmd_delete|
      cmd_delete.action do |_global_options, _options, args|
        print @banana.series_delete(id: args.first)
      end
    end

    series.desc 'Schedule series with given ID'
    series.arg 'ID'
    series.command :schedule do |cmd_schedule|
      cmd_schedule.switch :f, :force, desc: 'Force scheduling even if it has warnings', default_value: false
      cmd_schedule.action do |_global_options, options, args|
        print @banana.series_schedule(id: args.first, force: options[:force])
      end
    end

    series.desc 'Approve series with given ID'
    series.arg 'ID'
    series.command :approve do |cmd|
      cmd.action do |_global_options, _options, args|
        print @banana.series_approve(id: args.first)
      end
    end

    series.desc 'Prepare series with given ID'
    series.arg 'ID'
    series.command :prepare do |cmd|
      cmd.action do |_global_options, _options, args|
        print @banana.series_prepare(id: args.first)
      end
    end

    series.desc 'Schow the timetable of series with given ID'
    series.arg 'ID'
    series.command :timetable do |cmd|
      cmd.action do |_global_options, _options, args|
        print @banana.series_timetable(id: args.first)
      end
    end

    series.desc 'Abort series with given ID'
    series.arg 'ID'
    series.command :abort do |cmd|
      cmd.action do |_global_options, _options, args|
        print @banana.series_abort(id: args.first)
      end
    end
  end

  desc 'For all subjects of the group GROUP1, find matching subjects in GROUP2.'\
       'Returns a mapping of the pseudonyms from GROUP1 subjects to matching '\
       'pseudonyms of GROUP2 subjects. GROUP1 and GROUP2 must be groupfiles '\
       'that are currently loaded by aped.'
  arg 'GROUP1'
  arg 'GROUP2'
  command :pseudonymize do |cmd|
    cmd.action do |_global_options, _options, args|
      print @banana.pseudonymize(group1: args[0], group2: args[1])
    end
  end

  def self.print(response)
    if response[0]
      if response[1].empty?
        puts 'okay'
      else
        puts response[1]
      end
    else
      puts response[1]
      exit_now! ''
    end
  end

  def self.print_infos(response)
    infos = YAML.safe_load(response[1])
    return if infos.empty?

    infos.each do |info|
      puts info
    end
  end

  class << self
    attr_reader :banana
  end

  def banana
    self.class.banana
  end
end

# rubocop:enable Metrics/BlockLength
