# frozen_string_literal: true

require 'artifactomat/modules/ape/controller/result_controller'
require 'artifactomat/modules/ape/controller/test_program_controller'
require 'artifactomat/modules/ape/controller/test_series_controller'
require 'artifactomat/modules/ape/controller/test_season_controller'
