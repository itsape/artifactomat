# frozen_string_literal: false

require 'net/http'
require 'httpclient'
require 'cgi'
require 'active_support/hash_with_indifferent_access'

# Client helper for the ApeServer... Server the Ape a Banana
class Banana
  API_VERSION = '1.0'.freeze

  def initialize(ip, port)
    @uri = URI::HTTP.build(host: ip, port: port)
    @client = HTTPClient.new
    @client.receive_timeout = 65
    @client.default_header = { 'Content-Type' => 'application/yaml',
                               'API_VERSION' => API_VERSION }
  end

  # rubocop:disable Style/MethodMissingSuper
  def method_missing(name, *args)
    cmd = "/#{name.to_s.split(/_/).join('/')}"
    hash = args_to_hash(args)
    send_request cmd, hash
  rescue ArgumentError
    [false, 'wrong arguments']
  end
  # rubocop:enable Style/MethodMissingSuper

  def respond_to_missing?(*)
    # every method is valid and will return the server response to the query
    true
  end

  def build_client
    @client.receive_timeout = 140 # building the windows-client can take a while
    resp = send_request('/build/client', ActiveSupport::HashWithIndifferentAccess.new)
    @client.receive_timeout = 65
    resp
  end

  private

  def args_to_hash(args)
    hash = ActiveSupport::HashWithIndifferentAccess.new
    return hash if args.empty?

    hash = args.first
    raise ArgumentError, 'wrong arguments' unless hash.is_a?(Hash)

    hash
  end

  def send_request(cmd, hash)
    # https://bugs.ruby-lang.org/issues/10674
    hash = ActiveSupport::HashWithIndifferentAccess.new hash
    cmd << "/#{hash.delete('id')}" if hash.key? 'id'
    @uri.path = cmd
    query hash
  end

  # :reek:NilCheck
  # this is the abstraction layer for the api
  def parse(response)
    return [false, "API version #{API_VERSION} not supported by server."] if response.status.to_i == '418'.to_i

    status = HTTP::Status.successful? response.status

    msg = response.reason
    resp_content = response.content
    msg = resp_content unless resp_content.nil? || resp_content.empty?
    [status, msg]
  end

  def query(hash)
    if hash.any?
      parse @client.post(@uri, body: hash.to_h.to_yaml)
    else
      parse @client.get(@uri)
    end
  end
end
