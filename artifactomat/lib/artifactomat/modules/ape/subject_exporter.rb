# frozen_string_literal: true

# export pseudonyms and selected columns for a groupfile
class SubjectExporter
  class << self
    def export(args, conf_man, sub_man)
      group = args['groupfile']
      subject_groups = sub_man.list_subject_groups
      anonymity_level = conf_man.get('ape.anonymity_level')
      return 'that groupfile is unknown' unless subject_groups.include? group

      subjects = sub_man.get_subjects(group)
      build_csv(subjects, args['columns'], anonymity_level)
    end

    private

    def build_csv(subjects, columns, anonymity_level)
      out_csv = (['subject'] + columns).join(',')
      subjects.each do |subject|
        hash_sub = subject.to_hash
        next unless k_unique?(hash_sub, subjects, columns, anonymity_level)

        out_csv << "\n#{subject.id}"
        columns.each do |col|
          out_csv << ",#{hash_sub[col]}"
        end
      end
      out_csv
    end

    def k_unique?(subject, subjects, columns, anonymity_level)
      hits = 0
      subjects.each do |candidate|
        hash_candidate = candidate.to_hash
        is_equal = true
        columns.each do |column|
          if hash_candidate[column] != subject[column]
            is_equal = false
            break
          end
        end
        hits += 1 if is_equal
      end
      hits >= anonymity_level
    end
  end
end
