# frozen_string_literal: true

require 'sinatra/base'
require 'yaml'
require 'json'
require 'active_support/hash_with_indifferent_access'

require 'artifactomat'
require 'artifactomat/utils/bullet_loader'
require 'artifactomat/modules/client_builder'
require_relative 'controller/result_controller'
require_relative 'controller/test_program_controller'
require_relative 'controller/test_series_controller'
require_relative 'controller/test_season_controller'
require_relative 'pseudonymizer'
require_relative 'subject_exporter'

# The server part of Ape
class ApeServer < Sinatra::Base
  API_VERSIONS = ['1.0'].freeze

  configure_bullet_for_sinatra

  before do
    unless API_VERSIONS.include? request.env['HTTP_API_VERSION']
      halt 418,
           { 'Content-Type' => 'text/plain' },
           'Api version not supported.'.to_yaml
    end
    @request_data = YAML.safe_load(request.body.read.to_s, [Symbol, Time, ActiveSupport::HashWithIndifferentAccess])
  end

  after do
    response.body = response.body.to_yaml
  end

  register ApeController::ResultController
  register ApeController::TestProgramController
  register ApeController::TestSeriesController
  register ApeController::TestSeasonController

  get '/info' do
    [200, Ape.instance.fetch_infos]
  end

  get '/build/client' do
    Ape.instance.client_builder.create_installer
    [200, Ape.instance.client_builder.installer_path]
  end

  get '/query/:token' do
    case params[:token]
    when 'armed_episodes'
      episodes = TestEpisode.where(armed: true).map(&:to_str)
      return [200, episodes]
    when 'locked_seasons'
      seasons = TestSeason.where(locked: true).map(&:to_str)
      return [200, seasons]
    when 'infrastructure'
      return [200, YAML.safe_load(Ape.instance.monitoring.to_str)]
    when 'tracked_subjects'
      subject_ids = Ape.instance.subject_tracker.subject_ids
      return [200, subject_ids]
    when 'active_subjects'
      return [200, Ape.instance.vstore.subjects_from_map]
    else
      return [404, 'Token unknown']
    end
  end

  get '/status' do
    [200, Ape.instance.status]
  end

  get '/recipe/list' do
    recipes = Ape.instance.recipe_manager.list_recipes
    [200, recipes]
  rescue ConfigError => e
    [500, e.message]
  end

  get '/recipe/parameters/:id' do
    recipe = Ape.instance.recipe_manager.get_recipe params[:id]
    [200, recipe.describe_parameters]
  rescue ConfigError => e
    [500, e.message]
  end

  get '/subject/list' do
    subjects = Ape.instance.subject_manager.list_subject_groups
    [200, subjects]
  rescue ConfigError => e
    [500, e.message]
  end

  post '/reset' do
    resp = if @request_data['force']
             Ape.instance.reset_vstore
           else
             Ape.instance.safe_reset_vstore
           end
    [200, resp]
  rescue ApeError => e
    [422, e.message]
  end

  post '/import/trace' do
    traces = Ape.instance.track_collector.parse_trace(@request_data)
    [200, traces]
  end

  post '/import/helpdesk' do
    traces = Ape.instance.track_collector.parse_helpdesk(@request_data)
    [200, traces]
  end

  post '/pseudonymize' do
    out = Pseudonymizer.pseudonymize(@request_data, Ape.instance.subject_manager)
    [200, out]
  end

  post '/export' do
    out = SubjectExporter.export(@request_data,
                                 Ape.instance.configuration_manager,
                                 Ape.instance.subject_manager)
    [200, out]
  end

  not_found do
    [404, 'command not found']
  end

  error Exception do
    [500, request.env['sinatra.error'].message]
  end
end
