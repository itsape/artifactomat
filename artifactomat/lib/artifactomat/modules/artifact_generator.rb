# frozen_string_literal: true

require 'artifactomat/modules/deployment_manager'
require 'artifactomat/modules/logging'
require 'artifactomat/modules/scheduler'
require 'artifactomat/models/test_season'
require 'artifactomat/models/test_series'
require 'artifactomat/mixins/script_execution'
require 'fileutils'
require 'active_record'

# Artifact Generator Error
class ArtifactGeneratorError < RuntimeError
end

# Creates Artifacts for all recipies of all episodes of a TestSeries
class ArtifactGenerator
  include ScriptExecution

  LOG_SIG = 'ArtifactGenerator'

  def initialize(dep_man, conf_man, recipe_man, sub_man)
    @deployment_manager = dep_man
    @configuration_manager = conf_man
    @recipe_manager = recipe_man
    @subject_manager = sub_man # for ScriptExecution
    @scheduler = Scheduler.new
    @ag_mutex = Mutex.new
    recreate_state
  end

  # should be called in a thread
  def generate(series)
    series.test_seasons.each do |season|
      next if call_generate_season(season).zero?

      msg = "Generation of artifacts for #{series.to_str} failed (see log)."
      Logging.error(LOG_SIG, msg)
      raise ArtifactGeneratorError, msg
    end
    stop_deployment(series)
    add_destroy_job(series)
    0
  end

  def check_season(tseason)
    recipe = @recipe_manager.get_recipe(tseason.recipe_id)
    artifact_script = recipe.artifact_script + "\n"
    env = create_season_environment tseason
    env['DEP_CHECK'] = 'true'

    logstring = 'dependency check to be called: '
    Logging.debug(LOG_SIG, logstring + tseason.attributes.to_s)

    status, output = execute_script(artifact_script, env)
    generate_log_for_requirement_check(tseason, status, output, env)
    [status, output]
  end

  def destroy_artifacts(series)
    series.test_seasons.each do |season|
      delete_artifact_folder(season)
    end
  end

  def unschedule_jobs(series)
    @scheduler.jobs(tag: series.to_str).each(&:unschedule)
  end

  private

  def recreate_state
    TestSeries.where(status: %i[prepared approved]).find_each do |series|
      add_destroy_job(series)
    end
  end

  def add_destroy_job(series)
    @scheduler.at(series.end_date, tags: ['destroy artifacts', series.to_str]) do
      destroy_artifacts(series)
    end
  end

  def call_generate_season(season)
    errors = generate_season(season)
    if errors.zero?
      Logging.info(LOG_SIG, "All artifact generation scripts for season #{season.to_str} called without errors.")
      deploy_artifacts(season)
    else
      Logging.error(LOG_SIG, "Generate for #{season.to_str} failed in #{errors} out of #{season.test_episodes.count} times. See debug.")
    end
    errors
  end

  def stop_deployment(series)
    @ag_mutex.synchronize do
      @deployment_manager.stop_deployment(series)
    end
    communicate_info "All artifacts for series #{series.to_str} generated."
  end

  def deploy_artifacts(season)
    @ag_mutex.synchronize do
      @deployment_manager.deploy_artifact(season)
    end
  end

  def generate_log_for_requirement_check(tseason, status, output, env)
    logstring = "Requirement check for #{tseason.to_str} - #{tseason.recipe_id} returned: "
    if status.zero?
      Logging.debug(LOG_SIG, "#{logstring} true")
    else
      Logging.error(LOG_SIG, logstring + 'false ' + "The error shows: #{output}")
      Logging.debug(LOG_SIG, logstring + 'false ' + "ENV: #{env}")
    end
  end

  def delete_artifact_folder(season)
    env = season_to_env(season)
    folder = env[ScriptExecution::RECIPE_TEMP_FOLDER]
    FileUtils.rm_rf(folder) if File.directory?(folder)
  end

  def create_season_environment(tseason)
    env = @configuration_manager.config_to_env.merge(ENV)
    env.merge!(season_to_env(tseason))
  end

  def generate_season(tseason)
    ts_env = create_season_environment tseason

    FileUtils.mkdir_p(ts_env[ScriptExecution::RECIPE_TEMP_FOLDER])

    recipe = @recipe_manager.get_recipe(tseason.recipe_id)
    art_chain = recipe.artifact_script + "\n"

    execute_episode_scripts(tseason, ts_env, art_chain)
  end

  def execute_episode_scripts(tseason, ts_env, art_chain)
    ret_status = true
    output = ''
    errors = 0

    tseason.test_episodes.each do |test_episode|
      te_env = build_episode_env(ts_env, test_episode)

      ret_status, output = execute_script(art_chain, te_env)
      if ret_status.zero?
        Logging.debug(LOG_SIG, "Script executed for episode #{test_episode.to_str} with result #{ret_status}")
      else
        Logging.debug(LOG_SIG, "Script executed for episode #{test_episode.to_str} with result #{ret_status}: #{output}")
        errors += 1
      end
    end
    errors
  end

  def build_episode_env(ts_env, episode)
    te_env = ts_env.clone
    te_env.merge!(episode_to_env(episode))
    te_env
  end

  def communicate_info(msg)
    Logging.info(LOG_SIG, msg)
    Ape.instance.inform_user(msg)
  end
end
