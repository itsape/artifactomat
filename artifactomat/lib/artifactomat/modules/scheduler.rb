# frozen_string_literal: true

require 'rufus-scheduler'

require 'artifactomat/modules/logging'

LOG_SIG = 'Scheduler'

# our version of the rufus scheduler to catch errors
class Scheduler < Rufus::Scheduler
  def on_error(job, error)
    Logging.error(LOG_SIG, "Failed to #{job.tags.first}: #{error.message}")
  end
end

class SchedulerError < RuntimeError
end
