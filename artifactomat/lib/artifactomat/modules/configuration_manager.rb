# frozen_string_literal: true

require 'yaml'

require 'artifactomat/modules/logging'
require 'artifactomat/models/track_filter'

# ConfigurationError for the ConfigurationManager
class ConfigError < StandardError
end

# :reek:InstanceVariableAssumption
# :reek:RepeatedConditional
# :reek:DataClump
# Configuration Manager
class ConfigurationManager
  ENV_PREFIX = 'ARTIFACTOMAT_CONF'
  CONF_PATH = '/etc/artifactomat'
  BASIC_CONF = 'conf.yml'
  CONF_D = 'conf.d'
  LOG_SIG = 'CM'

  attr_reader :cfg_path

  class << self
    attr_reader :default_path
  end

  @default_path = File.expand_path(File.join(
                                     File.dirname(__FILE__),
                                     '..',
                                     '..',
                                     '..',
                                     'dist',
                                     'etc',
                                     'artifactomat'
                                   ))

  # :reek:NilCheck
  def initialize(path = nil)
    @cfg_path = self.class.default_path
    @cfg_path = CONF_PATH if File.readable? File.join CONF_PATH, BASIC_CONF
    @cfg_path = path unless path.nil?

    # check if path exists
    raise ConfigError, "'#{@cfg_path}' is no directory" unless File.directory?(@cfg_path)

    # check if readable
    raise ConfigError, "'#{@cfg_path}' is not readable" unless File.readable?(@cfg_path)

    init_config
    Logging.info(LOG_SIG, 'Initialized config.')
  end

  def get(query, *substitutes)
    subkeys = query.split('.')
    subkeys.each_with_index do |key, index|
      subkeys[index] = if key == '?'
                         substitutes[index - 1]
                       else
                         [key]
                       end
    end

    result = get_config_by_key @cfg, subkeys, {}
    raise ConfigError, "Config key unknown: #{query}" if result.empty?

    return result.values[0] if result.size == 1

    result
  end

  def config_to_env
    env = {}
    env = flatten_config(@cfg, env)
    Hash[env.map { |k, v| [[ENV_PREFIX, k].join('_').upcase, v] }]
  end

  def absolution(path)
    path = File.join @cfg_path, path unless (Pathname.new path).absolute?
    path
  end

  private

  def init_config
    @cfg = {}
    @cfg['trackfilter'] = []

    # load conf.yml
    load_basic_conf

    # load conf.d
    conf_d = File.join(@cfg_path, CONF_D)
    load_conf_d conf_d if File.exist? conf_d
  end

  # load conf.yml
  def load_basic_conf
    cfg_file = File.join(@cfg_path, BASIC_CONF)
    raise ConfigError, "Can not load #{BASIC_CONF} from: #{@cfg_path}" unless File.exist? cfg_file

    cfg_file_contents = File.read(cfg_file)
    @cfg['conf'] = YAML.safe_load(cfg_file_contents, [Symbol])
  end

  def fiddle(parentkey, subkey)
    subkey.to_s.sub('.', '_')
    [parentkey, subkey].reject(&:empty?).join('_')
  end

  # :reek:ManualDispatch
  def de_array(hash)
    numbered = {}
    hash.each do |key, value|
      next unless value.respond_to?(:each_with_index)

      value.each_with_index do |item, index|
        # numbered.merge!("#{key}_#{index}" => item.to_s)
        numbered.merge!(flatten_config(item, numbered, fiddle(key, index.to_s)))
      end
    end
    numbered
  end

  # :reek:ManualDispatch
  def flatten_config(cfg, flat, parent_key = '')
    return { parent_key => cfg.to_s } unless cfg.respond_to?(:keys)

    cfg.each_key do |key|
      if cfg[key].is_a? Array
        flat.merge!(de_array(fiddle(parent_key, key) => cfg[key]))
      else
        flat.merge!(flatten_config(cfg[key], flat, fiddle(parent_key, key)))
      end
    end
    flat
  end

  # :reek:ManualDispatch
  def get_config_by_key(config, keys, cfg, parent = '')
    return nil if keys.empty?

    keys.each do |key|
      next unless config.respond_to?(:each)

      config.each do |k, v|
        next unless key.include?(k) || key.first == '*'

        path = build_config_path parent, k
        check = get_config_by_key(v, keys[1..-1], cfg, path)
        if check
          cfg = check
        else
          cfg[path] = v
        end
      end
    end
    cfg
  end

  def build_config_path(parent, key)
    parent != '' ? "#{parent}.#{key}" : key
  end

  # :reek:NilCheck
  def load_conf(file)
    key = key_from_filename file
    file_contents = File.read(file)
    conf = YAML.safe_load(file_contents, [Symbol])
    @cfg[key] = conf unless conf.nil?
  end

  def loadable_config?(file)
    return false unless File.readable?(file)
    return false unless File.extname(file) == '.yml'
    return false if File.basename(file).index('.').zero?

    true
  end

  def key_from_filename(file)
    File.basename(file, File.extname(file))
  end

  def load_conf_d(folder)
    Dir.entries(folder).each do |conf|
      do_not_compute = ['.', '..', BASIC_CONF]
      next if do_not_compute.include? conf
      next if /trackfilter\.yml/.match? conf

      file = File.join folder, conf
      load_conf file if loadable_config? file
    end
    load_trackfilters(folder)
  end

  def load_trackfilters(folder)
    tf_conf = File.join folder, 'trackfilter.yml'
    return unless loadable_config? tf_conf

    tf_file_contents = File.read(tf_conf)
    tfs = YAML.safe_load(tf_file_contents, [TrackFilter, Regexp])
    return unless tfs.is_a? Array

    tfs.each do |tf|
      @cfg['trackfilter'] << tf if tf.errors.empty?
    end
  end
end
