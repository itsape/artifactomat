# frozen_string_literal: true

require 'json'
require 'digest'
require 'fileutils'
require 'open-uri'
require 'rubygems/package'

class WindowsClientError < StandardError
end

# Build and pack the windows-client with the artifactomats certificates and appropriate
# configuration
class ClientBuilder
  attr_reader :installer_path
  LOG_SIG = 'ClientBuilder'

  def initialize(conf_man)
    @configuration_manager = conf_man
    @client_version = conf_man.get('conf.windows_client.version')
    @client_repo_path = "/var/lib/artifactomat/windows-client-#{@client_version}"
    @installer_path = File.join(@client_repo_path, 'ITSAPE.Client.msi')
  end

  def create_installer
    download_client unless Dir.exist?(@client_repo_path)
    build_client unless client_built?
    configure_and_pack if need_repack?
    File.chmod(0o644, @installer_path)
  end

  private

  def client_built?
    Dir.exist?(File.join(@client_repo_path, 'publish'))
  end

  def need_repack?
    return true unless File.exist?(@installer_path)

    config = read_client_config
    my_ip = @configuration_manager.get('conf.network.external_ip')
    return true unless config['uproxy']['host'] == my_ip

    certs_changed?
  end

  def configure_and_pack
    configure_client
    set_client_cert_password
    copy_certs
    pack_client
  end

  def download_client
    uri = "https://gitlab.com/itsape/windows-client/-/archive/#{@client_version}/windows-client-#{@client_version}.tar.gz"
    stream = URI.open(uri)
    Gem::Package.new('').extract_tar_gz(stream, File.dirname(@client_repo_path))
  rescue StandardError => e
    raise WindowsClientError, "Failed to download client: #{e.message}"
  end

  def certs_changed?
    artifactomat_cert_dir = File.join(@configuration_manager.cfg_path, 'certs')
    client_cert_dir = File.join(@client_repo_path, 'tls')

    [['public/ca.cer',             'public/ca.cer'],
     ['public/client.itsape.cer',  'private/client.itsape.cer'],
     ['private/client.itsape.pfx', 'private/client.itsape.pfx'],
     ['public/server.itsape.cer',  'public/server.itsape.cer']].each do |source, target|
      source_path = File.join(artifactomat_cert_dir, source)
      target_path = File.join(client_cert_dir, target)
      return true unless File.exist?(target_path)
      return true unless Digest::MD5.file(source_path) == Digest::MD5.file(target_path)
    end
    false
  end

  def set_client_cert_password
    client_cert_path = '/var/lib/artifactomat/client_cert_password'
    tls_var_path = File.join(@client_repo_path, 'tls', 'VARIABLES')
    begin
      password = File.read(client_cert_path).strip
    rescue Errno::ENOENT
      raise WindowsClientError, 'Could not find certificate password in /var/lib/artifactomat/password, please regenerate your certificates by executing /etc/artifactomat/certs/generate-ca.sh'
    end
    tls_vars = File.read(tls_var_path)
    new_tls_vars = tls_vars.sub('CLIENTPFXPASSWORD="password123"', "CLIENTPFXPASSWORD=\"#{password}\"")
    File.write(tls_var_path, new_tls_vars)
  end

  def configure_client
    client_cert = File.join(@configuration_manager.cfg_path, 'certs', 'public', 'client.itsape.cer')
    my_ip = @configuration_manager.get('conf.network.external_ip')
    job_spooler_port = @configuration_manager.get('conf.ape_job_spooler.port')

    config = read_client_config
    config['uproxy']['host'] = 'server.itsape.zad.local' # according to the certs CN
    config['uproxy']['port'] = job_spooler_port
    config['dns']['itsape'] = my_ip
    config['CertThumbprint'] = Digest::SHA1.file(client_cert).hexdigest

    write_client_config(config)
  end

  def read_client_config
    example_config_path = File.join(@client_repo_path, 'ITSAPE-Client', 'config.example.json')
    real_config_path = File.join(@client_repo_path, 'ITSAPE-Client', 'config.json')
    config_path = if File.exist?(real_config_path)
                    real_config_path
                  else
                    example_config_path
                  end
    JSON.parse(File.read(config_path))
  end

  def write_client_config(config)
    config_path = File.join(@client_repo_path, 'ITSAPE-Client', 'config.json')
    File.write(config_path, JSON.dump(config))
  end

  def copy_certs
    artifactomat_cert_dir = File.join(@configuration_manager.cfg_path, 'certs')

    client_cert_dir = File.join(@client_repo_path, 'tls')
    [['public/ca.cer',             'public/ca.cer'],
     ['public/client.itsape.cer',  'private/client.itsape.cer'],
     ['private/client.itsape.pfx', 'private/client.itsape.pfx'],
     ['public/server.itsape.cer',  'public/server.itsape.cer']].each do |source, target|
      FileUtils.copy(File.join(artifactomat_cert_dir, source), File.join(client_cert_dir, target))
    end
  end

  def build_client
    system("docker run -v #{@client_repo_path}:/windows-client itsape-build")
  end

  def pack_client
    system("docker run -v #{@client_repo_path}:/windows-client itsape-pack")
  end
end
