# frozen_string_literal: true

require 'json'
require 'socket'
require 'openssl'
require 'csv'
require 'artifactomat/models/track_entry'
require 'artifactomat/models/recipe'
require 'artifactomat/models/test_season'
require 'artifactomat/models/track_filter'
require 'artifactomat/modules/logging'
require 'artifactomat/utils/mutexed_array'
require 'artifactomat/modules/subject_tracker/subject_tracker_utils'

# Implements TrackCollector as defined in the ITS.APT conception
# :reek:RepeatedConditional
class TrackCollector
  attr_reader :port

  # starts TrackCollector Server depending on the ssl_config hashmap provided
  # rubocop:disable Metrics/AbcSize
  def initialize(conf_man, recipe_man, sub_man)
    @configuration_manager = conf_man
    @recipe_manager = recipe_man
    @subject_manager = sub_man
    @vstore = VolatileStore.new(conf_man)

    # gather all active and completed seasons at startup - this is necessary to enable adding in new traces after test runtime
    @seasons = MutexedArray.new
    TestSeason.all.each do |season|
      @seasons.push season if season.test_series.status == 'approved' || season.test_series.status == 'completed'
    end

    @time_offset = @configuration_manager.get('trackcollector.offset').to_i
    @port = 0
    @ssl_cfg = read_ssl_cfg_from_cm
    @ssl_server = create_server @ssl_cfg
    @accept_thread = start_server
  end
  # rubocop:enable Metrics/AbcSize

  def status
    status = {}
    status['Server'] = { 'Port' => @port }
    status['Server']['Running?'] = running?.to_s
    status['Registered Seasons'] = @seasons.count
    status
  end

  def stop_server
    @accept_thread.kill while running?
    @ssl_server.close until @ssl_server.closed?
  end

  def running?
    %w[sleep run].include?(@accept_thread.status)
  end

  def start_server
    Thread.start do
      loop do
        Thread.start(@ssl_server.accept) do |connection|
          traces = read_data(connection)
          connection.close
          traces.each do |trace|
            parse_trace('trace' => trace)
          end
        end
      # rubocop:disable Lint/HandleExceptions
      rescue OpenSSL::SSL::SSLError
      # rubocop:enable Lint/HandleExceptions
      rescue StandardError => e
        Logging.error('TrackCollector',
                      "Server error: #{e.message}")
      end
    end
  end

  def register_series(series)
    series.test_seasons.each do |season|
      register_season season
    end
  end

  def unregister_series(series)
    series.test_seasons.each do |season|
      unregister_season season
    end
  end

  # Collected traces have to be mapped to the TrackEntry object.
  # For each registered recipe, the patterns are checked and the
  # first matching pattern will be chosen to extract the data.
  # Returns new track entries as CSV (string), or empty string.
  # rubocop:disable Metrics/AbcSize
  def parse_trace(args)
    return '' unless @seasons.any?

    entries = []
    # args includes: 'trace'
    # args may include: 'dry_run', 'tz_offset', 'series'
    args['trace'].each_line do |trace|
      next if ['', "\n", "\r\n"].include? trace

      @seasons.each do |season|
        next if args['series'] && !season.test_series.matches_id_or_label?(args['series'])

        recipe = @recipe_manager.get_recipe(season.recipe_id)
        entries.concat apply_trackfilters(season, trace, args) unless recipe.trackfilters.empty?
      end
    end
    entries_to_csv(entries)
  end
  # rubocop:enable Metrics/AbcSize

  # Like parse_trace, but exclusively for importing helpdesk information.
  # Helpdesk logs may be formatless, we need to be able to read them without trackfilters.
  def parse_helpdesk(args)
    return '' unless @seasons.any?

    entries = []
    # args includes: 'trace'
    # args may include: 'dry_run', 'tz_offset', 'series', 'score', 'attributes', 'format'
    args['trace'].each_line do |line|
      next if ['', "\n", "\r\n"].include? line

      @seasons.each do |season|
        next if args['series'] && !season.test_series.matches_id_or_label?(args['series'])

        entries.concat evaluate_helpdesk_trace(line, season, args)
      rescue StandardError
        Logging.error('TrackCollector', "Could not handle line:\n#{line}")
      end
    end
    entries_to_csv(entries)
  end

  private

  # :reek:DuplicateMethodCall
  # :reek:NilCheck
  def read_data(connection)
    traces = []
    expected = connection.gets.to_i
    expected.times do
      trace = connection.gets
      traces << trace.strip
    end
    connection.puts 'OK'

    traces
  rescue EOFError => e
    Logging.error('TrackCollector',
                  "Connection closed by peer #{e.message}")
    traces
  rescue StandardError => e
    Logging.error('TrackCollector',
                  "Error in message handling #{e.message}")
    traces
  end

  def connection_valid?(connection)
    now = Time.now
    if connection.peer_cert.not_before > now
      Logging.error('TrackCollector',
                    'Client certificate is not valid yet.')
      return false
    elsif connection.peer_cert.not_after < now
      Logging.error('TrackCollector',
                    'Client certificate is not valid anymore.')
      return false
    end
    true
  end

  def entries_to_csv(entries)
    return '' if entries.empty?

    res = CSV.generate do |csv|
      csv << entries.first.keys
      entries.each do |entry|
        csv << entry.values
      end
    end
    res
  end

  def apply_trackfilters(season, trace, args)
    entries = []
    recipe = @recipe_manager.get_recipe(season.recipe_id)
    trackfilters = recipe.trackfilters
    trackfilters.each do |tf|
      next unless hit tf.pattern, trace

      elements = collect_elements(tf.extraction_pattern, trace)
      subject_ids = elements.key?('subject') ? [elements['subject']] : SubjectTrackerUtils.resolve_subject_ids(@subject_manager, @vstore, elements, season)
      entries.concat create_track_entry_for_subjects(tf, elements, season, subject_ids, args)
    end

    entries
  end

  # rubocop:disable Metrics/AbcSize
  def evaluate_helpdesk_trace(trace, season, args)
    entries = []
    subjects = @subject_manager.get_subjects(season.test_series.group_file)
    season.test_episodes.each do |episode|
      te = create_helpdesk_track_entry(episode, args['score'])
      subject = subjects.select { |s| s.id == episode.subject_id }

      next unless helpdesk_hit?(trace, subject.first, te, args)

      unless args['dry_run']
        next unless te.save!
      end

      entries.push(te.to_hash)
      log_track(te)
    end

    entries
  end
  # rubocop:enable Metrics/AbcSize

  def create_helpdesk_track_entry(episode, score)
    te = TrackEntry.new
    te.score = if score && score.match(/^(-|\+)?[0-9]+$/)
                 score.to_i
               else
                 -1
               end
    te.course_of_action = 'helpdesk'
    te.test_episode = episode
    te.subject_id = episode.subject_id
    te
  end

  # :reek:LongParameterList
  def helpdesk_hit?(trace, subject, track_entry, args)
    if args['format'] # format is given via regex and named captures
      return false unless (match = trace.match(args['format']))

      fits_subject = trace_fits_subject_with_format?(match, subject)
      fits_schedule = trace_within_schedule_with_format?(match, track_entry.test_episode, track_entry, args['tz_offset'])
    else # no format given, best effort to fit subject and timestamp
      fits_subject = trace_fits_subject?(trace, subject)
      fits_schedule = trace_within_schedule?(trace, track_entry.test_episode, track_entry, args['tz_offset'])
    end

    return true if fits_subject && fits_schedule

    false
  end

  # checks if trace fits to subject of an episode
  def trace_fits_subject?(trace, subject)
    trace_downcase = trace.downcase
    # check for unique subject identifiers
    return true if trace_downcase.include?(subject.id.downcase) || trace_downcase.include?(subject.userid.downcase) || trace_downcase.include?(subject.email.downcase)

    false
  end

  # checks if trace fits to subject of an episode, with a given format in regex and named captures
  def trace_fits_subject_with_format?(match, subject)
    subject_attributes = subject.instance_variables.map { |a| 'subject_' + a.to_s[1..] }
    attributes_to_check = match.named_captures.slice(*subject_attributes)
    return false if attributes_to_check.empty?

    attributes_to_check.each do |attribute, value|
      return false unless subject.instance_variable_get("@#{attribute.delete_prefix('subject_')}").casecmp(value).zero?
    end
    true
  end

  # checks if trace fits within time schedule of an episode and sets found timestamp in track_entry
  def trace_within_schedule?(trace, episode, track_entry, tz_offset)
    # Time.parse is our best bet without a given format, but may fail
    date = Time.parse(trace) + tz_offset.to_i * 3600
    if date.between? episode.schedule_start, episode.schedule_end
      track_entry.timestamp = date
      return true
    end
    false
  rescue ArgumentError
    # could not parse time from trace
    false
  end

  def trace_within_schedule_with_format?(match, episode, track_entry, tz_offset)
    match.named_captures.each do |attribute, value|
      next unless value

      date = parse_date_from_format(attribute, value)
      next unless date

      date += (tz_offset.to_i || 0) * 3600
      if date.between?(episode.schedule_start, episode.schedule_end)
        track_entry.timestamp = date
        return true
      end
    end
    false
  rescue ArgumentError
    # could not parse time from trace
    false
  end

  def parse_date_from_format(attribute, value)
    return Time.at(value) if attribute == 'unix_timestamp'

    return Time.parse(value) if attribute == 'time'

    # forward slashes must be escaped in RegEx, but here we need them unescaped
    return Time.strptime(value, attribute.gsub('\\/', '/')) if attribute.include? '%'

    nil
  end

  # :reek:NilCheck and :reek:LongParameterList
  def create_track_entry_for_subjects(trackfilter, elements, season, subject_ids, args)
    entries = []
    subject_ids.each do |subject_id|
      tz_offset = args['tz_offset'] || 0
      te = generate_track_entry trackfilter, elements, season, subject_id, tz_offset
      next if te.test_episode.nil?

      unless args['dry_run']
        next unless te.save!
      end

      entries.push(te.to_hash)
      log_track(te)
    end
    entries
  end

  def log_track(track_entry)
    Logging.info('TrackCollector',
                 "#{track_entry.subject_id} issued `#{track_entry.course_of_action}` "\
                 "for `#{track_entry.test_episode.test_season.recipe_id}`")
  end

  def collect_elements(extraction_patterns, trace)
    elements = {}
    extraction_patterns.each do |ep|
      match = hit(ep, trace)
      next unless match

      match.names.each do |name|
        elements[name] = match[name]
      end
    end
    elements
  end

  def hit(pattern, trace)
    regexp = Regexp.new pattern
    regexp.match trace
  end

  # :reek:LongParameterList
  def generate_track_entry(track_filter, elements, season, subject_id, tz_offset)
    te = create_track_entry_from_filter track_filter
    te = fill_track_entry_with_matches te, elements, subject_id, tz_offset
    te = if elements.key?('test_episode_id')
           resolve_test_episode te, season, elements['test_episode_id']
         else
           infer_test_episode te, season
         end
    te
  end

  # :reek:ControlParameter
  def resolve_test_episode(track_entry, season, id)
    season.test_episodes.each do |episode|
      track_entry.test_episode = episode if episode.id.to_s == id && in_time?(track_entry, episode)
    end
    track_entry
  end

  def infer_test_episode(track_entry, season)
    season.test_episodes.each do |episode|
      track_entry.test_episode = episode if in_time?(track_entry, episode) && episode.subject_id == track_entry.subject_id
    end
    track_entry
  end

  # :reek:NilCheck
  def in_time?(track_entry, episode)
    te_timestamp = track_entry.timestamp
    return false if te_timestamp.nil? || episode.schedule_start.nil?
    return false if te_timestamp < (episode.schedule_start - @time_offset)
    return true if episode.schedule_end.nil?
    return false if te_timestamp > (episode.schedule_end + @time_offset)

    true
  end

  def fill_track_entry_with_matches(track_entry, elements, subject_id, tz_offset)
    track_entry.test_episode_id = elements['test_episode_id'] if elements.key?('test_episode_id')
    track_entry.timestamp = parse_time(elements['timestamp'], tz_offset) if elements.key?('timestamp')
    track_entry.subject_id = subject_id
    track_entry
  end

  def parse_time(time, tz_offset = 0)
    # Timestamp:
    return Time.at(time.to_i).utc + tz_offset * 3600 if /^[0-9]+$/.match?(time)

    # Apache Date-Time Format:
    time = time.sub(/(\d{4}):(\d{2})/, '\1 \2') \
    if %r{\d{2}\/([a-zA-Z]{3})\/(\d{4}):(\d{2}):(\d{2}):(\d{2})\s[+-]\d{4}}.match?(time)
    # Else:
    Time.parse(time).utc + tz_offset * 3600
  end

  def create_track_entry_from_filter(trackfilter)
    te = TrackEntry.new
    te.score = trackfilter.score
    te.course_of_action = trackfilter.course_of_action
    te
  end

  def create_server(ssl_config)
    tcp_server = TCPServer.new(ssl_config['ip'], ssl_config['port'])
    @port = tcp_server.addr[1]
    ssl_ctx = build_ssl_context(ssl_config)
    @ssl_server = OpenSSL::SSL::SSLServer.new(tcp_server, ssl_ctx)
  end

  def build_ssl_context(ssl_config)
    # create ssl context
    ssl_ctx = OpenSSL::SSL::SSLContext.new
    ssl_ctx.min_version = OpenSSL::SSL::TLS1_3_VERSION
    # set server certificate, key and ca chain file
    ssl_ctx.cert = OpenSSL::X509::Certificate.new(ssl_config['key.certificate'])
    ssl_ctx.key = OpenSSL::PKey::RSA.new(ssl_config['key.private'])
    ssl_ctx.ca_file = ssl_config['path.ca-certificate-chain']
    # set client verification and user CA
    ssl_ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER | \
                          OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT
    ssl_ctx.client_ca = OpenSSL::X509::Certificate.new(ssl_config['client.ca-chain'])
    ssl_ctx
  end

  def register_season(season)
    @seasons << season unless @seasons.include?(season)
  end

  def unregister_season(season)
    if @seasons.include? season
      @seasons.delete(season)
      true
    else
      false
    end
  end

  def read_file(file)
    File.open(file, 'r', &:read)
  end

  def read_ssl_cfg_from_cm
    ssl_config = {}
    ssl_config['ip'] = @configuration_manager.get('trackcollector.ip')
    ssl_config['port'] = @configuration_manager.get('trackcollector.port').to_i
    ssl_config['key.certificate'] =
      retrieve_file_by_config_key('trackcollector.certificate')
    ssl_config['key.private'] =
      retrieve_file_by_config_key('trackcollector.private_key')
    ssl_config['path.ca-certificate-chain'] =
      @configuration_manager.absolution(@configuration_manager.get('trackcollector.path_ca_certificate_chain'))
    ssl_config['client.ca-chain'] =
      retrieve_file_by_config_key('trackcollector.client_ca_chain')
    ssl_config
  end

  def retrieve_file_by_config_key(key)
    read_file(
      @configuration_manager.absolution(@configuration_manager.get(key))
    )
  end
end
