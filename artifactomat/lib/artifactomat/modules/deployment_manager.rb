# frozen_string_literal: true

require 'artifactomat/mixins/script_execution'
require 'artifactomat/modules/logging'
require 'fileutils'

# This is a helper class for the deployment manager as defined in the its.ape
# framework.
class DeploymentError < StandardError
end

# This class models the deployment manager as defined in the its.ape framework.
class DeploymentManager
  include ScriptExecution

  def initialize(mon, conf_man, recipe_man, sub_man, sub_tracker)
    @monitoring = mon
    @configuration_manager = conf_man
    @recipe_manager = recipe_man
    @subject_manager = sub_man # for ScriptExecution
    @subject_tracker = sub_tracker

    @infrastructure_deployment_manager =
      InfrastructureDeploymentManager.new(mon, conf_man, recipe_man, sub_man)

    @artifact_deployment_manager =
      ArtifactDeploymentManager.new
  end

  def infrastructures
    @infrastructure_deployment_manager.infrastructures
  end

  def infrastructure_generator=(generator)
    @infrastructure_deployment_manager.generator = generator
  end

  def seasons
    @artifact_deployment_manager.seasons
  end

  # Store the artifact for deployment.
  def deploy_artifact(season)
    @artifact_deployment_manager.deploy(season)
  end

  # Store the infrastructure element for deployment.
  def deploy_infrastructure(elem)
    @infrastructure_deployment_manager.deploy(elem)
  end

  # Mark the infrastructure deployment for 'series' as done.
  def infrastructure_ready(series)
    setup(series) if stop_deployment?(series)
    @infrastructure_deployment_manager.ready(series)
  end

  def infrastructure_ready?(series)
    @infrastructure_deployment_manager.ready?(series)
  end

  # Mark the artifact deployment for 'series' as done.
  def stop_deployment(series)
    if infrastructure_ready?(series)
      setup(series)
    else
      @artifact_deployment_manager.deployed(series)
    end
  end

  # Returns true if the artifact deployment for 'series' is done;
  # false otherwise.
  def stop_deployment?(series)
    @artifact_deployment_manager.deployed?(series)
  end

  def revoke_artifact_deployment(series)
    @artifact_deployment_manager.revoke_deployment series
  end

  # Set up a test series.
  def setup(series)
    seasons = series.test_seasons
    seasons.each do |season|
      setup_season(season)
    end

    Logging.info('DeploymentManager', "Setup of Series #{series.to_str} completed.")
  end

  # before i start removing logging messages, disable ABC first...
  # rubocop:disable Metrics/AbcSize
  def setup_season(season)
    rdy_for_deploy? season
    recipe = @recipe_manager.get_recipe(season.recipe_id)
    begin
      @infrastructure_deployment_manager.subscribe_monitoring(recipe)
    rescue DeploymentError => e
      # if the initial monitoring attempt is unsuccessful, disable this season
      Logging.error('DeploymentManager', e.message.to_s)
      @infrastructure_deployment_manager.unsubscribe_monitoring(season)
      return
    end

    status, output =
      execute_script(recipe.deploy_script, create_env(season))
    raise(CommandExecutionError, output) unless status.zero?

    Logging.info('DeploymentManager', "Deployed Season #{season.to_str}")
    Logging.debug('DeploymentManager', "deploy_script of Season #{season.to_str} returned with:  #{output}")
  end
  # rubocop:enable Metrics/AbcSize

  def create_env(season)
    env = @configuration_manager.config_to_env
    env.merge(season_to_env(season))
  end

  def withdraw(series)
    Ape.instance.routing.remove_routing(series)
    @infrastructure_deployment_manager.withdraw(series)
    @subject_tracker.deactivate_series(series.id)
  end

  def remove_folder(season)
    @infrastructure_deployment_manager.remove_folder(season)
  end

  private

  def rdy_for_deploy?(season)
    raise(DeploymentError, "Season #{season.to_str} not ready for deployment") unless @artifact_deployment_manager.season?(season.id)

    true
  end
end

# This is a helper class for the deployment manager as defined in the its.ape
# framework. :reek:Attribute
class InfrastructureDeploymentManager
  include ScriptExecution

  attr_reader :infrastructures
  attr_writer :generator

  def initialize(mon, conf_man, recipe_man, sub_man)
    @monitoring = mon
    @configuration_manager = conf_man # for ScriptExecution
    @recipe_manager = recipe_man
    @subject_manager = sub_man # for ScriptExecution
    @generator = nil
    @infrastructures = []
    @ready = []
  end

  # Store the infrastructure element for deployment.
  def deploy(elem)
    @infrastructures << elem
  end

  # Mark the infrastructure deployment for 'series' as done.
  def ready(series)
    @ready << series
  end

  def ready?(series)
    @ready.include?(series)
  end

  # Withdraws a test series.
  def withdraw(series)
    Logging.info('DeploymentMananger', "Withdraw #{series.to_str}.")
    Logging.info('DeploymentMananger',
                 "Told infrastructure generator to tear down #{series.to_str}.")
    @generator.destroy(series)
  end

  def subscribe_monitoring(recipe)
    recipe.infrastructure.each do |element|
      raise(DeploymentError, "#{element.infrastructure_type} not deployed") unless @infrastructures.include?(element)

      begin
        @monitoring.subscribe(element)
      rescue MonitorError
        raise(DeploymentError, "Could not subscribe #{element.to_str} to monitoring, element has no season.")
      end
      check(element)
    end
  end

  def unsubscribe_monitoring(season)
    Logging.info('DeploymentMananger',
                 "Inform monitoring about shutdown of season #{season.to_str}.")
    recipe = @recipe_manager.get_recipe(season.recipe_id)
    recipe.infrastructure.each do |element|
      @monitoring.unsubscribe(element)
    end
    remove_folder(season)
  end

  def remove_folder(season)
    dirname = recipe_tmp_folder(season)

    Logging.info('DeploymentMananger',
                 "Remove environment directory for #{season.to_str}: #{dirname}")

    FileUtils.rm_rf(dirname, secure: true)
  end

  private

  def check(inf_element)
    # give the monitoring some leeway
    Timeout.timeout(@monitoring.polling_rate + 3) do
      sleep(1) while @monitoring.get_status(inf_element).unknown?
      raise(DeploymentError, "Infrastructure #{inf_element.to_str} not ready") unless @monitoring.get_status(inf_element).good?
    end
  rescue Timeout::Error
    raise(DeploymentError, "Infrastructure #{inf_element.to_str} not ready")
  end
end

# This is a helper class for the deployment manager as defined in the its.ape
# framework.
class ArtifactDeploymentManager
  attr_reader :seasons

  def initialize
    @seasons = []
    @deployed = []
  end

  def deploy(season)
    if @seasons.include?(season.id)
      raise(DeploymentError,
            "TestSeason #{season.to_str} already called for deployment")
    end
    @seasons << season.id
  end

  def deployed(series)
    @deployed << series
  end

  def deployed?(series)
    @deployed.include?(series)
  end

  def season?(season_id)
    @seasons.include?(season_id)
  end

  def revoke_deployment(series)
    @deployed.delete series
    series.test_seasons.each { |s| @seasons.delete s.id }
  end
end
