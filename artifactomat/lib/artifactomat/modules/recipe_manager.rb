# frozen_string_literal: true

require 'artifactomat/models/recipe'
require 'artifactomat/models/infrastructure_element'
require 'artifactomat/modules/logging'
require 'artifactomat/modules/configuration_manager'

# :reek:RepeatedConditional
# :reek:DataClump
# :reek:InstanceVariableAssumption | we set them in init_recipes but reek does not understand
# Recipe Manager
class RecipeManager
  RECIPE_FOLDER = 'recipes'
  REQUIRED_SUBJECT_HEADERS = %w[id name surname userid email].freeze
  CONF_PATH = '/etc/artifactomat'
  TRACKFILTER_CONF_PATH = 'conf.d/trackfilter.yml'
  LOG_SIG = 'RecipeManager'

  class << self
    attr_reader :default_path
  end

  @default_path = File.expand_path(File.join(
                                     File.dirname(__FILE__),
                                     '..',
                                     '..',
                                     '..',
                                     'dist',
                                     'etc',
                                     'artifactomat'
                                   ))

  # :reek:NilCheck
  def initialize(path = nil)
    @cfg_path = self.class.default_path
    @cfg_path = CONF_PATH if File.readable? File.join CONF_PATH, RECIPE_FOLDER
    @cfg_path = path unless path.nil?

    # check if path exists
    raise ConfigError, "'#{@cfg_path}' is no directory" unless File.directory?(@cfg_path)

    # check if readable
    raise ConfigError, "'#{@cfg_path}' is not readable" unless File.readable?(@cfg_path)

    init_recipes
    Logging.info(LOG_SIG, 'Initialized recipes.')
  end

  def list_recipes
    @recipes.keys
  end

  def get_recipe(id, recipes = @recipes)
    raise ConfigError, "Recipe \'#{id}\' unknown." unless recipes.key? id

    recipes[id]
  end

  def get_recipes(recipes = @recipes)
    recipes.values
  end

  private

  def loadable_config?(file)
    return false unless File.readable?(file)
    return false unless File.extname(file) == '.yml'
    return false if File.basename(file).index('.').zero?

    true
  end

  def init_recipes
    @recipes = {}
    @trackfilters = load_trackfilters
    recipe_path = File.join(@cfg_path, RECIPE_FOLDER)
    if File.directory? recipe_path
      parse_recipes(recipe_path)
    else
      Logging.warning(LOG_SIG, "Recipe path '#{recipe_path}' is not a directory.")
    end
  end

  def parse_recipes(recipe_path)
    Dir.foreach(recipe_path) do |recipe|
      recipe_full_path = File.join(recipe_path, recipe)
      next unless loadable_config?(recipe_full_path)

      begin
        candidate = Recipe.load(recipe_full_path)
        candidate = load_trackfilters_to_recipe(candidate)
        next unless valid_recipe?(candidate)

        @recipes[candidate.id] = candidate
      rescue StandardError => e
        Logging.warning(LOG_SIG, "Recipe '#{recipe_full_path}' did not get loadad: #{e.message}")
      end
    end
  end

  def load_trackfilters
    tf_conf = File.join(@cfg_path, TRACKFILTER_CONF_PATH)
    return [] unless loadable_config? tf_conf

    tf_file_contents = File.read(tf_conf)
    tfs = YAML.safe_load(tf_file_contents, [TrackFilter, Regexp])
    return [] unless tfs.is_a? Array

    tfs.filter { |tf| tf.errors.empty? }
  end

  def load_trackfilters_to_recipe(recipe)
    @trackfilters.each do |tf|
      tf_clone = tf.clone
      tf_clone.recipe_id = recipe.id
      recipe.trackfilters << tf_clone
    end
    recipe
  end

  def valid_recipe?(recipe)
    return true if recipe.errors.empty?

    recipe.errors.each do |e|
      Logging.warning(LOG_SIG, "Recipe '#{recipe.id}' #{e} and will not be loaded.")
    end
    false
  end
end
