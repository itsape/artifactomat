# frozen_string_literal: true

require 'artifactomat/mixins/script_execution'
require 'artifactomat/models/job'
require 'artifactomat/modules/subject_tracker/subject_tracker'
require 'artifactomat/modules/logging'
require 'artifactomat/rpc/clients/routing_rpc_client'
require 'artifactomat/utils/mutexed_array'

# This is a helper class for the  a delviery manager as defined in the its.ape
# framework.
class DeliveryError < StandardError
end

# This class models the delviery manager as defined in the its.ape framework.
# :reek:RepeatedConditional
class DeliveryManager
  include ScriptExecution

  LOG_SIG = 'DeliveryManager'

  def initialize(conf_man, recipe_man, sub_man)
    @configuration_manager = conf_man
    @recipe_manager = recipe_man
    @subject_manager = sub_man # for ScriptExecution
    @routing = RoutingClient.new(conf_man, recipe_man)
  end

  def status
    subject_ids = TestEpisode.where(armed: true).pluck(:subject_id)
    { 'Armed Episodes' => subject_ids.count,
      'Armed Subjects' => subject_ids.uniq }
  end

  # Arm the 'episode'.
  # :reek:DuplicateMethodCall
  def arm(episode)
    return if check_locked(episode)
    raise(DeliveryError, "TestEpisode #{episode.to_str} was already armed.") if episode.armed?

    Logging.debug(LOG_SIG, "Arming test episode #{episode.to_str}.")
    begin
      output = execute_arm(episode)
    rescue RoutingRunningError => e
      raise(DeliveryError, "Routing failed: #{e.message}")
    rescue CommandExecutionError, InvalidCommandError => e
      Logging.debug(LOG_SIG, "Episode #{episode.to_str} Arm-Script failed: #{output}")
      raise(DeliveryError, "Arm-Script failed: #{e.message}")
    end
    create_start_detection_job(episode)
  end

  # Disarm the 'episode'.
  # :reek:DuplicateMethodCall
  def disarm(episode)
    raise(DeliveryError, "Disarm of episode #{episode.to_str} failed, was not armed.") unless episode.armed?

    Logging.debug(LOG_SIG, "Disarming test episode #{episode.to_str}.")
    create_stop_detection_job(episode)
    begin
      output = execute_disarm(episode)
    rescue RoutingRunningError => e
      Logging.error(LOG_SIG, "Routing failed, disarming episode #{episode.to_str}: #{e.message}")
    rescue CommandExecutionError, InvalidCommandError => e
      Logging.debug(LOG_SIG, "Episode #{episode.id} Disarm-Script failed: #{output}")
      raise(DeliveryError, "Disarm-Script failed: #{e.message}")
    end
  end

  # Disarms all armed TestEpisodes.
  def disarm_all
    TestEpisode.where(armed: true).each do |e|
      disarm(e)
    end
  end

  def lock_season(season)
    season.reload
    return if season.locked?

    season.locked = true
    season.save!
    season.test_episodes.each do |episode|
      disarm(episode) if episode.armed?
      episode.failed! unless episode.completed?
    end
    Logging.info(LOG_SIG, "Locked season #{season.to_str}.")
  end

  def unlock_season(season)
    season.reload
    return unless season.locked?

    season.locked = false
    season.save!
    Logging.info(LOG_SIG, "Unlocked season #{season.to_str}.")
  end

  private

  def create_start_detection_job(episode)
    detection_parameters = episode.test_season.json_detection_parameters
    return if detection_parameters == '{}'

    job = {
      action: 'start_detection',
      user: @subject_manager.get_subject(episode.subject_id).userid,
      file_path: '',
      deploy_path: '',
      expiration_date: episode.schedule_end,
      creation_date: Time.now,
      watch_arguments: detection_parameters
    }
    Job.create!(job)
  end

  def create_stop_detection_job(episode)
    detection_parameters = episode.test_season.json_detection_parameters
    return if detection_parameters == '{}'

    userid = @subject_manager.get_subject(episode.subject_id).userid
    unless Job.exists?(user: userid, action: :start_detection, status: :completed)
      Logging.warning(LOG_SIG, "Tried to create stop detection job for episode #{episode}, but no matching start detection job was found.")
      return
    end

    job = {
      action: 'stop_detection',
      user: userid,
      file_path: '',
      deploy_path: '',
      expiration_date: nil,
      creation_date: Time.now,
      watch_arguments: detection_parameters
    }
    Job.create!(job)
  end

  def check_locked(episode)
    locked = episode.test_season.locked?
    Logging.warning(LOG_SIG, "Unable to start locked episode #{episode.to_str}") if locked
    locked
  end

  def execute_arm(episode)
    @routing.activate(episode)
    _, output = execute_arm_script(episode)
    Logging.info(LOG_SIG, "Armed Episode #{episode.to_str}.")
    Logging.debug(LOG_SIG, "Episode #{episode.id} Arm-Script output:#{output}")
    episode.armed = true
    episode.save!
    output
  end

  def execute_disarm(episode)
    _, output = execute_disarm_script(episode)
    episode.armed = false
    episode.save!
    @routing.deactivate(episode)
    Logging.info(LOG_SIG, "Disarmed Episode #{episode.to_str}.")
    Logging.debug(LOG_SIG, "Episode #{episode.id} Disarm-Script output:#{output}")
    output
  end

  def execute_arm_script(episode)
    season = episode.test_season
    recipe = @recipe_manager.get_recipe(season.recipe_id)
    status, output = execute_script(recipe.arm_script, create_env(episode, season))
    raise(CommandExecutionError, output) unless status.zero?

    [status, output]
  end

  def create_env(episode, season)
    env = @configuration_manager.config_to_env
    env = env.merge(season_to_env(season))
    env = env.merge(episode_to_env(episode))
    ip = SubjectTracker.get_ip_from_subjectid(episode.subject_id)
    env[SUBJECT_ENV_PREFIX + 'IP'] = ip
    env
  end

  def execute_disarm_script(episode)
    season = episode.test_season
    recipe = @recipe_manager.get_recipe(season.recipe_id)
    status, output = execute_script(recipe.disarm_script, create_env(episode, season))
    raise(CommandExecutionError, output) unless status.zero?

    [status, output]
  end
end
