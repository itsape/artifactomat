# frozen_string_literal: true

require 'artifactomat/modules/logging'
require 'artifactomat/modules/test_scheduler/test_schedule_generator'
require 'artifactomat/modules/scheduler'

# runs as thread and schedules all TestEpisodes
class TestScheduler
  LOG_SIG = 'TestScheduler'

  def initialize(conf_man, sub_man, delmgr)
    @scheduler = Scheduler.new
    @generator = TestScheduleGenerator.new conf_man, sub_man
    @delmgr = delmgr
    @subject_manager = sub_man
    @max_episode_delay_seconds = conf_man.get('conf.scheduler.max_episode_delay_seconds')
    handle_blacklisted_subjects_episodes
    recreate_schedule
  end

  def schedule(series)
    raise SchedulerError, "Can not schedule Series #{series.to_str}: Series #{series.status}" unless series.schedulable?

    @generator.generate_schedule(series)
    series.scheduled!
    Logging.debug(LOG_SIG, "Generated schedule for series #{series.to_str}.")
  rescue StandardError => e
    Logging.error(LOG_SIG, e.message)
    raise
  end

  def generate_timetable(series)
    timetable = +''
    series.test_episodes.order(:schedule_start).each do |e|
      subject = @subject_manager.get_subject(e.subject_id)
      userid = subject.userid
      name = subject.name
      surname = subject.surname
      timetable << "#{e.schedule_start},#{name},#{surname},#{userid},#{e.test_season.recipe_id},#{e.schedule_end}\n"
    end
    timetable
  end

  # :reek:DuplicateMethodCall
  def ack(series)
    raise SchedulerError, "Series #{series.to_str} not prepared." unless series.prepared?
    raise SchedulerError, "Series #{series.to_str} has old schedule." if series.start_date < Time.now

    series.approved!
    series.test_episodes.each do |episode|
      add_jobs episode
    end
    Logging.info(LOG_SIG, "Series #{series.to_str} schedule acknowledged, episodes queued.")
  end

  def status
    status = {}
    status['Running?'] = running?.to_s
    status['Schedules'] = @scheduler.jobs(tag: 'arm episode').count
    status
  end

  def running?
    !@scheduler.down?
  end

  def series_current_testtime(series, testtime_delta = 0)
    testtime = @generator.series_testtime(series) + testtime_delta
    earliest_end = @generator.biz.time(testtime, :minutes).after(series.start_date)
    [testtime, earliest_end]
  end

  # rubocop:disable Metrics/AbcSize
  def series_enough_runtime(series, testtime_delta = 0)
    start = series.start_date
    testtime = @generator.series_testtime(series) + testtime_delta
    runtime = @generator.biz.within(start, series.end_date).in_minutes
    raise ApeError, "Series end date must be after the start date, i.e. after #{start}" unless runtime.positive?
    raise ApeError, "Series testtime too long for currently alotted runtime by #{format_duration(testtime - runtime)}. Shorten testtime or set end date to at least #{@generator.biz.time(testtime, :minutes).after(start)}" if testtime > runtime

    runtime - (testtime + @generator.cooldown / 60)
  end
  # rubocop:enable Metrics/AbcSize

  def format_duration(dur)
    mins = dur % 60
    hours = dur / 60
    format('%<hours>02dh %<mins>02dm', hours: hours, mins: mins)
  end

  def unschedule_jobs(series)
    @scheduler.jobs(tag: series.to_str).each(&:unschedule)
    true # return something json serializable for redis-rpc
  end

  def stop
    # Shuts down the scheduler, waits (blocks) until all the jobs cease running.
    @scheduler.shutdown(:wait)
  end

  private

  def handle_blacklisted_subjects_episodes
    TestEpisode.where(subject_id: @subject_manager.blacklisted_subjects).find_each do |episode|
      if episode.running?
        Logging.info(LOG_SIG, "Subject #{episode.subject_id} is blacklisted, disarming #{episode.to_str}")
        @delmgr.disarm(episode)
      end
      episode.destroy!
    end
  end

  def recreate_schedule
    TestSeries.where(status: %i[approved]).find_each do |series|
      series.test_episodes.each do |episode|
        if episode.prepared?
          restore_prepared_episode(episode)
        elsif episode.running?
          restore_running_episode(episode)
        end
      end
    end
  end

  def restore_prepared_episode(episode)
    if episode.schedule_start < Time.now - @max_episode_delay_seconds # allow episodes to start late
      episode.failed!
      Logging.error(LOG_SIG, "start_date for prepared episode '#{episode.to_str}' is #{Time.now - episode.schedule_start} seconds in the past. This episode has failed.")
    else
      add_jobs(episode)
    end
  end

  def restore_running_episode(episode)
    if episode.schedule_end < Time.now - @max_episode_delay_seconds # allow episodes to run too long
      Logging.warning(LOG_SIG,
                      "end_date for running episode '#{episode.to_str}' is more than " \
                      "#{Time.now - episode.schedule_end} seconds in the past.")
    end
    add_disarm_job(episode)
  end

  def add_disarm_job(episode)
    @scheduler.at(episode.schedule_end, tags: ['disarm episode', episode.to_str]) do
      ActiveRecord::Base.connection_pool.with_connection do
        @delmgr.disarm(episode)
        episode.completed!
      end
    end
  end

  def add_jobs(episode)
    ActiveRecord::Base.clear_active_connections!
    @scheduler.at(episode.schedule_start, tags: ['arm episode', episode.to_str]) do
      ActiveRecord::Base.connection_pool.with_connection do
        @delmgr.arm(episode)
        episode.running!
      end
      add_disarm_job(episode)
    end
  end
end
