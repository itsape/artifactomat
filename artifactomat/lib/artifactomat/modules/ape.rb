# frozen_string_literal: true

require 'singleton'

require 'artifactomat/modules/logging'
require 'artifactomat/modules/configuration_manager'
require 'artifactomat/modules/client_builder'
require 'artifactomat/modules/recipe_manager'
require 'artifactomat/modules/subject_manager'
require 'artifactomat/modules/artifact_generator'
require 'artifactomat/modules/result_generator'
require 'artifactomat/modules/deployment_manager'
require 'artifactomat/modules/delivery_manager'
require 'artifactomat/modules/infrastructure_generator'
require 'artifactomat/models/test_program'
require 'artifactomat/models/subject_history'
require 'artifactomat/models/track_entry'
require 'artifactomat/rpc/clients/subject_tracker_rpc_client'
require 'artifactomat/rpc/clients/track_collector_rpc_client'
require 'artifactomat/rpc/clients/routing_rpc_client'
require 'artifactomat/rpc/clients/test_scheduler_rpc_client'
require 'artifactomat/rpc/clients/monitoring_rpc_client'
require 'artifactomat/volatile_store'

# The APE specific error...
class ApeError < StandardError
end

# This is the heart of its.ape. :reek:InstanceVariableAssumption
class Ape
  include Singleton

  LOG_SIG = 'APE'

  attr_reader :configuration_manager
  attr_reader :client_builder
  attr_reader :recipe_manager
  attr_reader :subject_manager
  attr_reader :monitoring
  attr_reader :deployment_manager
  attr_reader :track_collector
  attr_reader :delivery_manager
  attr_reader :artifact_generator
  attr_reader :result_generator
  attr_reader :test_scheduler
  attr_reader :infrastructure_generator
  attr_reader :logging
  attr_reader :subject_tracker
  attr_reader :routing
  attr_reader :vstore

  def initialize
    @initialized = false
    @callback_mutex = Mutex.new
    @configuration_manager = ConfigurationManager.new
    @recipe_manager = RecipeManager.new
    @subject_manager = SubjectManager.new
    initialize_logger
    return if @configuration_manager.cfg_path.eql?('/etc/artifactomat')

    Logging.warning(LOG_SIG,
                    "Configuration loadad from '#{@configuration_manager.cfg_path}'")
  end

  def init
    return if @initialized

    @initialized = true
    init_vstore
    bootstrap
    cleanup_preparation_series
    purge_blacklisted_subject_data
    init_news
    init_jobs
    show_banner
  end

  singleton_class.send(:alias_method, :__singleton_instance, :instance)

  def self.instance
    inst = __singleton_instance
    inst.init
    inst
  end

  def status
    status = {}
    status['APE'] = { 'Jobs' => jobs }
    status['TrackCollector'] = @track_collector.status
    status['TestScheduler'] = @test_scheduler.status
    status['Monitoring'] = @monitoring.status
    status['SubjectTracker'] = @subject_tracker.status
    status['DeliveryManager'] = @delivery_manager.status
    status
  end

  def safe_reset_vstore
    conflicting_series = TestSeries.where(status: %i[preparing prepared approved]).to_a
    return reset_vstore if conflicting_series.empty?

    msg = +"The following series are prepared or approved: #{conflicting_series} \n"
    msg << "Resetting the vstore while you have prepared or approved series can break the series execution!\nIf you really want to reset the vstore, use `ape reset --force`"
    raise ApeError, msg
  end

  def reset_vstore
    @vstore.reset
  end

  def inform_user(msg)
    @news_mutex.synchronize do
      @vstore.push_news(msg)
    end
    Logging.info(LOG_SIG, "[userinfo] #{msg}")
  end

  def update_job(worker, state_description)
    @jobs_mutex.synchronize do
      @jobs[worker] = state_description
    end
  end

  def jobs
    jobs = []
    @jobs_mutex.synchronize do
      @jobs.each do |worker, state_description|
        status = { state_description => thread_status(worker) }
        jobs.push status
        @jobs.delete worker if worker.status == false
      end
    end
    jobs
  end

  def unregister_job(worker)
    @jobs_mutex.synchronize do
      @jobs.delete worker
    end
  end

  def fetch_infos
    @news_mutex.synchronize do
      news = @vstore.fetch_and_clear_news
      Logging.debug(LOG_SIG, '[userinfo] all messages fetched.')
      news
    end
  end

  def deflate_program(test_program)
    program_hash = test_program.attributes
    program_hash['series'] = []
    test_program.test_series.each do |test_serie|
      program_hash['series'].push deflate_series(test_serie)
    end
    program_hash
  end

  def deflate_series(test_series)
    series_hash = test_series.attributes
    series_hash['seasons'] = []
    test_series.test_seasons.each do |test_season|
      series_hash['seasons'].push deflate_season(test_season)
    end
    series_hash
  end

  def deflate_season(test_season)
    season_hash = test_season.attributes
    season_hash['episodes'] = []
    test_season.test_episodes.each do |test_episode|
      season_hash['episodes'].push deflate_episode(test_episode)
    end
    season_hash
  end

  def deflate_episode(test_episode)
    episode_hash = test_episode.attributes
    episode_hash['subject'] = test_episode.subject_id
    episode_hash['seconds_exposed'] = test_episode.seconds_exposed
    episode_hash
  end

  def create_episodes(series)
    tseasons = series.test_seasons
    sgroup = series.group_file
    subjects = @subject_manager.get_subjects(sgroup)
    tseasons.each do |tseason|
      subjects.each do |subject|
        te = tseason.test_episodes.build(subject_id: subject.id)
        te.save
      end
    end
  end

  def unschedule_jobs(series)
    @routing.unschedule_jobs(series)
    @infrastructure_generator.unschedule_jobs(series)
    @artifact_generator.unschedule_jobs(series)
    @test_scheduler.unschedule_jobs(series)
  end

  private

  def show_banner
    msg = '                         -:__`
                  \Q@B&qXZoooZXq&Q@Q|
              ;QQdj\>rrrrrrrr>>>rr>\jdQQ;
      :zt]zvQ@U/rrrrrrrrrrrrrrrrrrrrrr>LU@D/z]Jv;
   W@RozcP@Qu*rrrrrrrrrrrrrrrrrrrrrrrrrrr*mQ@XLzSD@g
 `@O*>r\Q@U=rrr>/zFFti?>rrrrrrrr=iz]Fz/=rrr=9@H\rr|9@:
`@g>>r\QQu>r|oN@De}JyOQQD}*r>]qQQKa]}aW@Qm|>>u@Q\rr>D@
v@orr=Q@i>>e@N:         rQ@9QBL         `D@U>rS@Q=r>]@`
-@R>>m@e>>P@=             ;@\             F@K>re@er>O@
 _@U\Q@*r=@g     ,|=`              ^\:     K@|r*@Q?X@r
   QQ@B>>*@D    Q@@@@:           ;@@@@Q    m@\rrQ@Q@
    t@#r>>K@:   cQ@@&             E@@@\   r@Nr>rB@,
     @@>rr>k@&,                          u@g|r>>@@
     |@urr>r\a#@KF*                  ,cO@Dirrr>u@:
      QQ|rrrrrr=X@O      .. ..      ;@NJ=rrr>rvQQ
       @#Lrrrrr>D@;      `` ``       @Q?>rrrrL@@
        Q@}r>rrrQ@`  |//////////\;   Q@i>rrr]@H
         ;QLrrr>Q@   Q@`       ,@u   QQ\rrr=&:
           .>rr>N@,  `gN       Qk    @Rrrr;
              :re@o    "9QgDQgy`    t@zr:
                 ;BQZ|;         :/oNQ|
                      -;vuKKKF7r`'

    inform_user(msg)
  end

  def init_vstore
    @vstore = VolatileStore.new(@configuration_manager)
  end

  # rubocop:disable Metrics/AbcSize
  def bootstrap
    @subject_tracker = SubjectTrackerRpcClient.new(@configuration_manager, @subject_manager)
    @delivery_manager = DeliveryManager.new @configuration_manager, @recipe_manager, @subject_manager
    @monitoring = MonitoringRpcClient.new(@configuration_manager,
                                          @recipe_manager,
                                          @subject_manager,
                                          @delivery_manager)
    @deployment_manager = DeploymentManager.new @monitoring,
                                                @configuration_manager,
                                                @recipe_manager,
                                                @subject_manager,
                                                @subject_tracker
    @artifact_generator = ArtifactGenerator.new(@deployment_manager,
                                                @configuration_manager,
                                                @recipe_manager,
                                                @subject_manager)
    @track_collector = TrackCollectorRpcClient.new(@configuration_manager,
                                                   @recipe_manager,
                                                   @subject_manager)
    @result_generator = ResultGenerator.new
    @test_scheduler = TestSchedulerRpcClient.new(@configuration_manager,
                                                 @subject_manager,
                                                 @delivery_manager)
    @infrastructure_generator =
      InfrastructureGenerator.new(@configuration_manager,
                                  @recipe_manager,
                                  @subject_manager,
                                  @deployment_manager,
                                  @monitoring)
    @routing = RoutingClient.new(@configuration_manager, @recipe_manager)
    @client_builder = ClientBuilder.new(@configuration_manager)
  end
  # rubocop:enable Metrics/AbcSize

  def purge_blacklisted_subject_data
    SubjectHistory.where(subject_id: @subject_manager.blacklisted_subjects).destroy_all
    TrackEntry.where(subject_id: @subject_manager.blacklisted_subjects).destroy_all
  end

  def cleanup_preparation_series
    TestSeries.where(status: %i[preparing]).find_each do |series|
      cleanup_series(series)
    end
  end

  def cleanup_series(series)
    @artifact_generator.destroy_artifacts(series)
    @infrastructure_generator.destroy(series)
    @routing.remove_routing(series)
    series.scheduled!
  end

  def init_news
    @vstore.init_news
    @news_mutex = Mutex.new
  end

  def init_jobs
    @jobs = {}
    @jobs_mutex = Mutex.new
  end

  def initialize_logger
    log_level = @configuration_manager.get('conf.ape.log_level')
    log_colored = @configuration_manager.get('conf.ape.log_colored')
    Logging.initialize(log_level, log_colored)
  end

  # :reek:NilCheck
  def thread_status(thread)
    status = thread.status
    return 'Terminated with exception' if status.nil?
    return 'Finished' unless status

    status
  end
end
