# frozen_string_literal: true

require 'set'
require 'yaml'
require 'timeout'

require 'artifactomat/modules/logging'
require 'artifactomat/mixins/script_execution'

class MonitorError < RuntimeError
end

# This class reflect the monitoring states
class MonitoringState
  attr_reader :error

  def initialize(state = nil, err = nil)
    @state = -1 # unknown
    @state = state if state
    @error = err if err
  end

  def to_str
    return 'unknown' if @state == -1
    return 'good' if @state.zero?

    'failed'
  end

  def not_good?
    !good?
  end

  def good?
    @state.zero?
  end

  def unknown?
    @state == -1
  end

  def to_i
    @state.to_i
  end

  def ==(other)
    return false unless other.class == self.class
    return false unless @error == other.error
    return false unless @state.to_i == other.to_i

    true
  end
end

# :reek:RepeatedConditional
# :reek:InstanceVariableAssumption | we set them in the #init function but reek does not understand
# This class models the monitoring as definded in the its.ape framework.
class Monitoring
  include ScriptExecution

  DEFAULT_POLLING_RATE = 10
  LOG_SIG = 'Monitoring'

  attr_reader :polling_rate

  def initialize(conf_man, recipe_man, sub_man, del_man)
    @configuration_manager = conf_man
    @recipe_manager = recipe_man
    @subject_manager = sub_man # for ScriptExecution
    @delivery_mgr = del_man
    init
    recreate_state
  end

  def status
    status = {}
    status['Running?'] = running?.to_s
    status['Infrastructure'] = {}
    status['Infrastructure']['total'] = @subscribed_elements.count
    status['Infrastructure']['unhealthy'] = no_unhealthy_elements
    status['Infrastructure']['seasons'] = monitored_seasons
    status
  end

  # return MonitoringState
  def get_status(inf_element)
    raise MonitorError, "InfrastructureElement #{inf_element.to_str} is unknown." unless @subscribed_elements.key?(inf_element.to_yaml)

    @subscribed_elements[inf_element.to_yaml][:state]
  end

  # Subscribe to new IE. Initially set status code to -1 (status currently
  # unknown).
  # :reek:NilCheck
  def subscribe(inf_element)
    @subscription_mutex.synchronize do
      raise MonitorError, "InfrastructureElement #{inf_element.to_str} without Season can't be monitored" if inf_element.season.nil?

      unless @subscribed_elements.key?(inf_element.to_yaml)
        @subscribed_elements[inf_element.to_yaml] = {
          state: MonitoringState.new,
          ie: inf_element
        }
      end
    end
    Logging.info(LOG_SIG, "Subscribed IE: #{inf_element.to_str}")
  end

  # Unsubscribe infrastructure elements.
  def unsubscribe(inf_element)
    @subscription_mutex.synchronize do
      @subscribed_elements.delete(inf_element.to_yaml)
    end
    Logging.info(LOG_SIG, "Unsubscribed IE: #{inf_element.to_str}")
  end

  # Returns true if the status of element is 0 (running).
  def infrastructure_running?(inf_element)
    get_status(inf_element).good?
  rescue MonitorError
    false
  end

  def running?
    return true if %w[sleep run].include? @thread.status

    false
  end

  def stop
    Logging.debug(LOG_SIG, 'Stopping Monitoring.')
    @stopped_mutex.synchronize do
      @stopped = true
    end
    @thread.join
    Logging.info(LOG_SIG, 'Monitoring stopped.')
  end

  def run
    Logging.debug(LOG_SIG, 'Starting Monitoring.')
    @stopped_mutex.synchronize do
      @stopped = false
    end
    @thread = Thread.new { run_loop }
    Logging.info(LOG_SIG, 'Monitoring started.')
  end

  def to_str
    all_states = {}
    @subscription_mutex.synchronize do
      @subscribed_elements.each do |_ie_yaml, data|
        all_states[data[:ie].to_str] = data[:state].to_str
      end
    end
    all_states.to_yaml
  end

  private

  def init
    @delivery_mutex = Mutex.new
    @routing = RoutingClient.new(@configuration_manager, @recipe_manager)

    begin
      @polling_rate = @configuration_manager.get('conf.monitoring.polling_rate')
    rescue ConfigError
      # We can ignore this error and use the default one.
      @polling_rate = DEFAULT_POLLING_RATE
    end

    @subscribed_elements = {}
    @subscription_mutex = Mutex.new

    @stopped = false
    @stopped_mutex = Mutex.new

    # always have a Thread object in @thread
    @thread = Thread.new {}
    @thread.join
  end

  def no_unhealthy_elements
    @subscribed_elements.values.count { |data| !data[:state].good? }
  end

  def monitored_seasons
    @subscribed_elements.values.map { |data| data[:ie].season.to_str }.uniq
  end

  def recreate_state
    TestSeries.where(status: %i[prepared approved]).includes([:test_seasons]).find_each do |series|
      series.test_seasons.each do |season|
        next if season.locked?

        recipe = @recipe_manager.get_recipe(season.recipe_id)
        recipe.infrastructure.each do |inf_element|
          inf_element.season_id = season.id
          subscribe(inf_element)
        end
      end
    end
  end

  def run_loop
    loop do
      break if stop?

      @subscription_mutex.lock
      @subscribed_elements.each do |_ie_yaml, data|
        Thread.new do
          monitor(data[:ie], data[:state])
        end
      end
      @subscription_mutex.unlock
      @polling_rate.times do
        return if stop?

        sleep(1)
      end
    end
  end

  def stop?
    @stopped_mutex.lock
    stop = @stopped
    @stopped_mutex.unlock
    stop
  end

  # ran in worker threat
  def create_env(season)
    env = @configuration_manager.config_to_env
    env.merge(season_to_env(season))
  end

  # ran in worker threat
  def monitor(inf_element, old_state)
    status, output = nil
    env = create_env(inf_element.season)
    begin
      Timeout.timeout(@polling_rate - 0.1) do
        status, output = execute_script(inf_element.monitor_script, env)
      end
    rescue Timeout::Error
      status = 1
      output = 'Timeout'
    end
    new_state = MonitoringState.new status, output
    # only update state if IE is still subscribed, else the still running Thread may unintentionally resubscribe
    manage_states(new_state, old_state, inf_element) if @subscribed_elements.key?(inf_element.to_yaml)
  end

  # :reek:DuplicateMethodCall
  def manage_states(new_state, old_state, inf_element)
    Logging.debug(LOG_SIG, "Got state for IE #{inf_element.to_str}: #{new_state.to_str}")
    if new_state.good?
      return if old_state.good?

      Logging.info(LOG_SIG, "Monitor for IE #{inf_element.to_str}: good")
      unlock(inf_element)
    else
      return if old_state == new_state

      Logging.error(LOG_SIG, "Monitor for IE #{inf_element.to_str} failed: #{new_state.error}")
      lock(inf_element)
    end
    update_state(inf_element, new_state)
  end

  def update_state(inf_element, state)
    @subscription_mutex.lock
    @subscribed_elements[inf_element.to_yaml][:state] = state
    @subscription_mutex.unlock
  end

  def lock(inf_element)
    @delivery_mutex.lock
    @delivery_mgr.lock_season(inf_element.season)
    @delivery_mutex.unlock
  end

  def unlock(inf_element)
    @delivery_mutex.lock
    @delivery_mgr.unlock_season(inf_element.season)
    @delivery_mutex.unlock
  end
end
