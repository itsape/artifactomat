# frozen_string_literal: true

# The logger for itsape
# :reek:DataClump
module Logging
  @log_level = 4
  @stdout_mutex = Mutex.new

  class << self
    # :reek:BooleanParameter
    def initialize(log_level = 4, log_colored = false)
      @log_level = log_level
      @log_colored = log_colored
    end

    def error(modulename, message)
      print_message(modulename, message, 'error') if @log_level.positive?
    end

    def warning(modulename, message)
      print_message(modulename, message, 'warning') if @log_level > 1
    end

    def info(modulename, message)
      print_message(modulename, message, 'info') if @log_level > 2
    end

    def debug(modulename, message)
      print_message(modulename, message, 'debug') if @log_level > 3
    end

    private

    def print_message(modulename, message, message_level)
      @stdout_mutex.synchronize do
        $stdout.write "<#{Time.now.strftime('%a %d %b %T %Y')}> \
<#{get_colour message_level.upcase}> <#{modulename}> [#{message}]\n"
        $stdout.flush
      end
    end

    # :reek:ControlParameter
    # rubocop:disable Metrics/CyclomaticComplexity
    def get_colour(message_level)
      case message_level
      when 'ERROR'
        @log_colored ? "\e[1m\e[91mERROR\e[0m" : 'ERROR'
      when 'WARNING'
        @log_colored ? "\e[33mWARNING\e[0m" : 'WARNING'
      when 'INFO'
        @log_colored ? "\e[37mINFO\e[0m" : 'INFO'
      when 'DEBUG'
        @log_colored ? "\e[2m\e[94mDEBUG\e[0m" : 'DEBUG'
      end
    end
    # rubocop:enable Metrics/CyclomaticComplexity
  end
end
