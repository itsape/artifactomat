# frozen_string_literal: true

require 'yaml'
require 'csv'

require 'artifactomat/modules/logging'
require 'artifactomat/models/detection_event'

class ResultError < RuntimeError
end

# This class provides the result generation module as defined
# in the its.ape framework.
class ResultGenerator
  LOG_SIG = 'ResultGenerator'

  # This class is a helper to support the exporter part
  # of the its.ape ResultGenerator.
  class Exporter
    def initialize(format)
      @format = format
      @strategy = case format
                  when 'csv'
                    CSVExporter.new
                  when 'yaml'
                    YAMLExporter.new
                  else
                    FallbackExporter.new(format)
                  end
    end

    def export(entries)
      @strategy.export(entries)
    end
  end

  # This class is a helper to support the YAML exporter part
  # of the its.ape ResultGenerator.
  class YAMLExporter
    def export(entries)
      entries.to_yaml
    end
  end

  # This class is a helper to support the CSV exporter part
  # of the its.ape ResultGenerator.
  class CSVExporter
    def export(entries, seperator = ';')
      return if entries.empty?

      headers = entries.first.keys
      results = CSV.generate(col_sep: seperator) do |csv|
        csv << headers
        entries.each do |entry|
          csv << entry.values
        end
        results
      end
    end
  end

  # This class is a helper to support the YAML exporter part
  # of the its.ape ResultGenerator, if the specified format is
  # unknown.
  class FallbackExporter
    def initialize(wanted_format)
      @wanted_format = wanted_format
    end

    def export(entries)
      Logging.warning(LOG_SIG, "Asked for export series as non available #{@wanted_format}. Delivered YAML instead.")
      entries.to_yaml
    end
  end

  def report_series(series_id, headers, out, format_)
    Logging.info('ResultGenerator', "Reporting series #{series_id} data as #{format_}.")
    series = TestSeries.find_by(id: series_id)
    raise(ResultError, 'Unknown series') if series.blank?

    results = { 'id' => series_id, 'label' => series.label, 'group_file' => series.group_file }
    season_results = {}
    series.test_seasons.includes([:test_episodes]).each do |season|
      season_results[season.id] = serialized_entries_by_season(season)
    end
    results['seasons'] = season_results
    prepare_series_results(results, headers, out, format_)
  end

  def report_online(format_ = 'yaml')
    subject_ids = Ape.instance.subject_tracker.subject_ids
    online_subject_ids = Ape.instance.vstore.subjects_from_map
    entries = []

    subject_ids.each do |subject_id|
      entries.push get_subject_online_entry(subject_id, online_subject_ids)
    end

    exporter = Exporter.new(format_)
    exporter.export(entries)
  end

  def report_detection(format_)
    detection_events = DetectionEvent.all
    entries = detection_events.map { |e| detection_entry(e) }

    exporter = Exporter.new(format_)
    exporter.export(entries)
  end

  private

  def detection_entry(detection_event)
    episode = detection_event.test_episode
    season = episode.test_season
    {
      'subject' => episode.subject_id,
      'season' => season.label,
      'recipe' => season.recipe_id,
      'timestamp' => detection_event.timestamp,
      'artifact_present' => detection_event.artifact_present
    }
  end

  # rubocop:disable Metrics/AbcSize
  # :reek:NilCheck
  def infer_online_time(episode_id, entry_timestamp = nil)
    episode = TestEpisode.find_by(id: episode_id)
    series_id = TestEpisode.find_by(id: episode_id).test_season.test_series.id
    history = SubjectHistory.where(test_series_id: series_id, subject_id: episode.subject_id)
    total_online_time = 0
    history.each do |session|
      session_end = if session.session_end.nil?
                      entry_timestamp.nil? ? Time.now.utc : entry_timestamp
                    else
                      session.session_end
                    end
      total_online_time += get_minutes_online(episode.schedule_start, episode.schedule_end, session.session_begin, session_end) if entry_timestamp.nil? || (entry_timestamp <= session_end)
    end
    total_online_time
  end
  # rubocop:enable Metrics/AbcSize

  # rubocop:disable Metrics/AbcSize
  def get_track_entries(episode_ids)
    episodes_no_tracks = []
    track_entries = episode_ids.collect do |episode_id|
      entry = TrackEntry.where(test_episode_id: episode_id)
      episodes_no_tracks.push(episode_id) if entry.empty?
      entry
    end
    track_entries.flatten!
    track_entries = track_entries.map(&:serializable_hash)

    track_entries.each do |entry|
      entry['online_time'] = infer_online_time(entry['test_episode_id'], entry['timestamp'])
    end

    episodes_no_tracks.each do |episode_id|
      track_entries.push(get_dummy_track_entry(episode_id)) if infer_online_time(episode_id).positive?
    end

    track_entries
  end
  # rubocop:enable Metrics/AbcSize

  def serialized_entries_by_season(season)
    results = {}
    results['recipe'] = season.recipe_id
    results['label'] = season.label
    episode_ids = season.test_episodes.collect(&:id)
    results['track_entries'] = get_track_entries(episode_ids)
    results
  end

  # :reek:NilCheck
  def prepare_series_results(results, headers, out, format_)
    order = headers[1].nil? ? headers[0] : prepare_headers(headers[0], headers[1])
    entries = build_series_entries(results, order)

    exporter = Exporter.new(format_)
    output = exporter.export(entries)

    if out
      file = File.open(out, 'w')
      file.write output
      file.close
    else
      output
    end
  end

  # rubocop:disable Metrics/AbcSize
  def build_series_entries(results, order)
    entries = []
    results['seasons'].each do |key, value|
      value['track_entries'].each do |track_entry|
        entry = track_entry.clone
        entry['recipe'] = value['recipe']
        entry['series_id'] = results['id']
        entry['series_label'] = results['label']
        entry['season_label'] = value['label']
        entry['season_id'] = key.to_s
        entry = (order & entry.keys).map { |k| [k, entry[k]] }.to_h
        entries << entry
      end
    end
    entries
  end
  # rubocop:enable Metrics/AbcSize

  def prepare_headers(headers, mod_headers)
    sorted_headers = []
    mod_headers.split(/[ ,]+/).each do |mod|
      if mod.start_with? '+'
        mod.slice!(0)
        sorted_headers.push(mod) unless sorted_headers.include? mod
      elsif mod.start_with? '-'
        mod.slice!(0)
        headers.delete(mod) if headers.include? mod
      else
        sorted_headers.push(mod) unless sorted_headers.include? mod
      end
    end
    sorted_headers += headers - sorted_headers
    sorted_headers
  end

  def get_subject_online_entry(subject_id, online_subject_ids)
    episodes = TestEpisode.where(subject_id: subject_id)
    total_minutes = 0
    armed = false
    episodes.each do |episode|
      total_minutes += infer_online_time episode.id
      armed = true if episode.running?
    end

    {
      'subject' => subject_id,
      'online' => online_subject_ids.include?(subject_id) ? true : false,
      'armed' => armed,
      'tracked' => "#{total_minutes} minutes"
    }
  end

  # generate dummy track entry, which contains accumulated online time of subject
  def get_dummy_track_entry(episode_id)
    dummy_entry = {
      'test_episode_id' => episode_id,
      'subject_id' => TestEpisode.find_by(id: episode_id).subject_id,
      'online_time' => infer_online_time(episode_id)
    }
    dummy_entry
  end

  # get online time of subject in minutes
  def get_minutes_online(episode_start, episode_end, session_start, session_end)
    return 0 if episode_end < session_start

    return 0 if episode_start > session_end

    start = if session_start < episode_start
              episode_start
            else
              session_start
            end

    end_ = if episode_end < session_end
             episode_end
           else
             session_end
           end

    (end_ - start).to_i / 60
  end
end
