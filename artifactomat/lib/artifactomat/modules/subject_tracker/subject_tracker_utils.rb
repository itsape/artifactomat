# frozen_string_literal: true

# Code that was initially located in subject_tracker but is shared and
# could be extracted.
module SubjectTrackerUtils
  # given a Hash "description", containing the headers from a group_file
  # as keys, try to resolve a subject from the hash-values
  # used by: TrackCollector
  def self.resolve_subject_ids(subject_manager, vstore, description, season)
    subjects = subject_manager.get_subjects(season.test_series.group_file)
    subjects = subjects.select do |subject|
      subject.match? description
    end
    return subjects.map(&:id) unless subjects.empty?

    vstore.sids_by_ip(description['ip'])
  end
end
