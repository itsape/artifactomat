# frozen_string_literal: true

require 'artifactomat/modules/logging.rb'
require 'artifactomat/volatile_store.rb'
require 'artifactomat/modules/subject_tracker/long_term_memory.rb'
require 'artifactomat/models/action'
require 'artifactomat/models/subject_history'
require 'artifactomat/models/test_series'
require 'artifactomat/models/action'

# class for bookholding subject-ips
class SubjectTracker
  LOG_SIG = 'SubjectTracker'

  # interface for collector plugins
  class CollectorPlugin
    attr_reader :name
    # provide SubjectTracker
    def initialize(subject_tracker, name)
      @st = subject_tracker
      @st.register_collector(self)
      @name = name
    end
  end

  # interface for receiver plugins
  class ReceiverPlugin
    attr_reader :name
    # provide SubjectTracker
    def initialize(subject_tracker, name)
      @st = subject_tracker
      @st.register_receiver(self)
      @name = name
    end
  end

  attr_reader :collector_plugins, :receiver_plugins, :configuration_manager

  # lookup from DB
  def self.get_ip_from_subjectid(subjectid)
    entry = SubjectHistory.where("session_end IS NULL AND subject_id = '#{subjectid}'")
    return nil if entry.empty?

    if entry.length > 1
      Logging.warning(LOG_SIG, 'Subject is registered on multiple IPs! Can not resolve!')
      return nil
    end
    entry.first.ip
  end

  # lookup from DB
  def self.get_subjectid_from_ip(ip)
    entry = SubjectHistory.where("session_end IS NULL AND ip = '#{ip}'")
    return nil if entry.empty?

    if entry.length > 1
      Logging.warning(LOG_SIG, 'Multiple Subjects on the same IP! Can not resolve!')
      return nil
    end
    entry.first.subject_id
  end

  def initialize(configuration_manager, subject_manager)
    @collector_plugins = {}
    @receiver_plugins = {}
    @configuration_manager = configuration_manager
    @subject_manager = subject_manager
    @vstore = VolatileStore.new(configuration_manager)
    @ltm = LongTermMemory.new(@vstore, @subject_manager)
    @subjects = Set.new

    @subjects = find_subjects
    init_plugins
    @main_loop_thread = Thread.new { main_loop }
  end

  def status
    status = {}
    status['Running?'] = running?.to_s
    status['Plugins'] = {}
    status['Plugins']['collectors'] = @collector_plugins.keys
    status['Plugins']['receiver'] = @receiver_plugins.keys
    status['Subjects'] = {}
    status['Subjects']['tracked'] = @subjects.count
    status['Subjects']['active'] = @vstore.count_subject_map
    status
  end

  def running?
    %w[sleep run].include? @main_loop_thread.status
  end

  # TODO: spec that!
  def stop
    @receiver_plugins.each_value do |plug|
      plug['plugin'].stop
      plug['thread'].kill if plug['thread'].alive?
    end
    Logging.debug(LOG_SIG, 'Shut down signal sent to receiver plugins')
    @main_loop_thread.kill
  end

  # add series subjects to ltm whitelist
  # called by ape
  def activate_series(series_id)
    series = TestSeries.find series_id
    @ltm.add_series(series)
  end

  # remove series subjects from ltm whitelist
  # called at series undeplyment
  def deactivate_series(series_id)
    series = TestSeries.find series_id
    @ltm.remove_series(series)
  end

  # plugins call this method to add user information
  # MSG for the subject tracker
  # subject:
  #   userid: user_X
  # ip: 127.0.0.1
  # session_start: Time.now    OR
  # session_end: Time.now
  def push_data(action)
    referred_subjects = resolve_subjects(action.userid)
    return nil if referred_subjects.empty?

    referred_subjects.each do |referred_subject|
      action.subject_id = referred_subject.id
      process_data(action)
    end
    nil
  end

  def subject_ids
    @subjects.map(&:id)
  end

  private

  def main_loop_tick
    now = Time.now
    @collector_plugins.each_value do |hash|
      plugin = hash['plugin']
      last = hash['last_polled']
      plugin.collect_data if now.to_i >= last + plugin.request_interval
    end
  end

  def main_loop
    loop do
      main_loop_tick
      sleep(1)
    end
    Logging.debug(LOG_SIG, 'Shut down signal received, main loop quits now')
  end

  def process_data(action)
    Logging.debug(LOG_SIG, "Processing: #{action}")
    return end_session(action) if action.end_session?

    start_session(action)
  end

  # :reek:NilCheck
  def start_session(action)
    # there is only one ip! one can easily wait for the timeout if the
    # user really moved
    ip = @vstore.ip_by_sid(action.subject_id)
    return if action.ip.nil?
    return if action.ip == ip

    @vstore.update_ip_subject_map(action)
    @ltm.start_session(action.subject_id, action.session_start)
  end

  def end_session(action)
    @ltm.end_session(action.subject_id, action.session_end)
    @vstore.remove_from_ip_subject_map(action)
  end

  def find_subjects
    groups = @subject_manager.list_subject_groups
    groups.map { |group| @subject_manager.get_subjects(group) }
          .reduce(Set.new, :merge)
  rescue ConfigError
    Logging.error(LOG_SIG, "Error getting subjects from groupfile #{group}")
  end

  def init_plugins
    conf = nil
    begin
      conf = @configuration_manager.get('subjecttracker')
    rescue ConfigError => e
      Logging.error(LOG_SIG, "Configuration Error: #{e}")
      return
    end

    conf['plugins'].each do |plugin_data|
      plugin = build_plugin(plugin_data)
      case plugin_data[1]['type']
      when 'collector'
        register_collector(plugin)
      when 'receiver'
        register_receiver(plugin)
      else
        Logging.error(LOG_SIG, "Invalid plugin type: #{plugin_data[1]['type']}")
      end
    rescue LoadError, NameError => e
      Logging.error(LOG_SIG, "Can not load plugin: #{e}")
      next
    end
  end

  # register collector plugin and call immediately
  def register_collector(plugin)
    plugin_name = plugin.name
    return if @collector_plugins.key? plugin_name

    @collector_plugins[plugin_name] = { 'plugin' => plugin, 'last_polled' => Time.now.to_i }
    plugin.collect_data
    Logging.info(LOG_SIG, "Successfully registered collector #{plugin_name}.")
  end

  def register_receiver(plugin)
    plugin_name = plugin.name
    return if @receiver_plugins.key? plugin_name

    @receiver_plugins[plugin_name] = { 'plugin' => plugin, 'thread' => Thread.new { plugin.run } }
    Logging.info(LOG_SIG, "Successfully registered receiver #{plugin_name}.")
  end

  def build_plugin(plugin_config)
    file = File.join(@configuration_manager.cfg_path, 'plugins', plugin_config[1]['file_name'])
    self.class.send(:require, file)
    plugin = Object.const_get(plugin_config[1]['class_name']).new(self, plugin_config[0])
    plugin
  end

  def select_subjects(subjects, description)
    subjects.select do |subject|
      subject.match?(description)
    end
  end

  def resolve_subjects(userid)
    description = { 'userid' => userid }
    select_subjects(@subjects, description)
  end
end
