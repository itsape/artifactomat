# frozen_string_literal: true

require 'set'
require 'artifactomat/volatile_store'
require 'artifactomat/models/volatile_stored_hash'

# :reek:RepeatedConditional
# Persistent subject history manager
class LongTermMemory
  LOG_SIG = 'SubjectTracker'

  def initialize(vstore, sub_man)
    @subject_manager = sub_man
    @vstore = vstore
    # { subject_id => Set{ series_id, ... }, ... }
    @subjects = VolatileStoredHash.new('long_term_memory', 'subjects', @vstore)
  end

  def add_series(series)
    first_season = series.test_seasons.first
    first_season.test_episodes.each do |te|
      subject = @subject_manager.get_subject(te.subject_id)
      add subject, series
      start_sessions_from_stm te.subject_id
    end
    Logging.debug(LOG_SIG, "LTM added series #{series.id}")
  end

  def remove_series(series)
    subject_ids = @subjects.delete_val(series.id)
    subject_ids.each { |id| end_all_sessions(id, Time.now) }
    Logging.debug(LOG_SIG, "LTM removed series #{series.id}")
  end

  def tracked?(subject_id)
    @subjects.key? subject_id
  end

  def end_session(subject_id, timestamp)
    return unless tracked? subject_id

    end_all_sessions(subject_id, timestamp)
  end

  def start_session(subject_id, timestamp)
    return unless tracked? subject_id

    current_entry = get_current_entry subject_id
    update current_entry, timestamp
  rescue ActiveRecord::RecordNotFound
    start_new_session subject_id, timestamp
  end

  private

  def get_current_entry(subject_id)
    SubjectHistory.where('session_end IS NULL AND subject_id = ?', subject_id).take!
  end

  def get_all_current_entry(subject_id)
    SubjectHistory.where('session_end IS NULL AND subject_id = ?', subject_id)
  end

  def end_all_sessions(subject_id, timestamp)
    SubjectHistory.where('session_end IS NULL AND subject_id = ? ', subject_id).update_all("session_end = '#{timestamp.utc}', submit_time = '#{Time.now.utc}'")
    Logging.debug(LOG_SIG, "LTM ended all sessions for subject #{subject_id}.")
  rescue ActiveRecord::RecordNotFound => e
    Logging.debug('SubjectTracker', e.to_s)
  end

  def add(subject, series)
    @subjects.add(subject.id, series.id)
  end

  # :reek:NilCheck
  def start_new_session(subject_id, timestamp)
    ip = @vstore.ip_by_sid(subject_id)
    return 1 if ip.nil?

    @subjects[subject_id].each do |series_id|
      SubjectHistory.create!(
        subject_id: subject_id,
        ip: ip,
        test_series_id: series_id,
        session_begin: timestamp,
        submit_time: Time.now.utc
      )
      Logging.debug(LOG_SIG, "LTM started session for subject \"#{subject_id}\" in series \"#{series_id}\"")
    end
    0
  end

  # :reek:NilCheck
  def start_sessions_from_stm(subject_id)
    ip = @vstore.ip_by_sid(subject_id)
    start_session(subject_id, Time.now) unless ip.nil?
  end

  # :reek:NilCheck
  def update(current_entry, timestamp)
    subject_id = current_entry.subject_id
    ip = @vstore.ip_by_sid(subject_id)
    return 1 if ip.nil?

    current_entry.session_end = timestamp if current_entry.ip != ip
    current_entry.submit_time = Time.now.utc
    current_entry.save
    Logging.debug(LOG_SIG, "LTM updated (ended) session for: #{subject_id}")
    start_new_session(subject_id, timestamp) if current_entry.ip != ip
  end
end
