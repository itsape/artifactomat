# frozen_string_literal: true

require 'artifactomat/modules/logging'
require 'artifactomat/mixins/script_execution'
require 'artifactomat/modules/ape'
require 'artifactomat/modules/scheduler'

class InfrastructureRunningError < StandardError
end

class InfrastructureGenerationError < StandardError
end

# This class models the infrastructure generator as defined in the its.apt
# framework.
class InfrastructureGenerator
  include ScriptExecution

  LOG_SIG = 'InfrastructureGenerator'

  def initialize(conf_man, recipe_man, subject_man, dep_man, mon)
    @configuration_manager = conf_man
    @recipe_manager = recipe_man
    @subject_manager = subject_man # for ScriptExecution
    @deployment_manager = dep_man
    @monitoring = mon
    @scheduler = Scheduler.new

    # The deployment manager requires a infrastructure generator.
    @deployment_manager.infrastructure_generator = self
    recreate_state
  end

  # rubocop:disable Metrics/AbcSize
  def generate(series)
    series_str = series.to_str
    communicate_info "Starting generation of infrastructure of Series #{series_str}."
    elements = gather_elements(series)

    raise InfrastructureRunningError if check_and_inform(elements)

    threads = elements.map do |element|
      Thread.new do
        begin
          generate_element(element)
          @deployment_manager.deploy_infrastructure(element)
        rescue CommandExecutionError, InvalidCommandError => e
          next e
        end
        element
      end
    end

    results = threads.map(&:value)
    errors = results.filter { |r| r.is_a? StandardError }
    unless errors.empty?
      results.reject { |r| r.is_a? StandardError }.each { |elem| destroy_element elem }
      errors.map!(&:to_s)
      raise InfrastructureGenerationError, "Could not generate infrastructure for series #{series_str} due to the following errors: #{errors}"
    end

    communicate_info "Infrastructure of Series #{series_str} generated."
    @deployment_manager.infrastructure_ready(series)
    add_destroy_job(series)
    0
  end
  # rubocop:enable Metrics/AbcSize

  def destroy(series)
    elements = gather_elements(series)
    elements.each do |element|
      check_unsub_and_destroy(element)
    end
  end

  # Returns an array of InfrastructureElements.
  def gather_elements(series)
    seasons = series.test_seasons
    elements = []
    seasons.each do |season|
      gather_season_elements(season, elements)
    end
    elements.flatten
  end

  def unschedule_jobs(series)
    @scheduler.jobs(tag: series.to_str).each(&:unschedule)
  end

  private

  def recreate_state
    TestSeries.where(status: %i[prepared approved]).find_each do |series|
      add_destroy_job(series)
    end
  end

  def add_destroy_job(test_series)
    @scheduler.at(test_series.end_date, tags: ['destroy infrastructure', test_series.to_str]) do
      destroy(test_series)
    end
  end

  def check_and_inform(elements)
    cleanup = false
    elements.each do |element|
      # Inform the user about running infrastructure.
      next unless @monitoring.infrastructure_running?(element)

      communicate_error "#{element.to_str} already running. Manual cleanup required."
      cleanup = true
    end
    cleanup
  end

  def communicate_error(msg)
    Logging.error(LOG_SIG, msg)
    Ape.instance.inform_user(msg)
  end

  def communicate_info(msg)
    Logging.info(LOG_SIG, msg)
    Ape.instance.inform_user(msg)
  end

  def check_unsub_and_destroy(element)
    running = @monitoring.infrastructure_running?(element)
    @monitoring.unsubscribe(element)
    if running
      destroy_element(element)
    else
      Logging.warning(LOG_SIG, "#{element.to_str} is not running and will not be destroyed.")
    end
  end

  def destroy_element(element)
    execute_destroy_script(element)
  rescue ScriptExecutionError => e
    Logging.error(LOG_SIG, "#{element.to_str} could not be stopped: #{e.message}")
  end

  def execute_destroy_script(element)
    status, output =
      execute_script(element.destroy_script, create_env(element.season))
    raise(CommandExecutionError, output) if status != 0

    Logging.info(LOG_SIG, "Destroyed #{element.to_str}")
  end

  # Deploy the InfrastructureElement if neccessary.
  def generate_element(element)
    status, output =
      execute_script(element.generate_script, create_env(element.season))
    if status != 0
      Logging.error(LOG_SIG, "Failed to generate #{element.to_str}: #{output}.")
      raise(CommandExecutionError, output)
    end
    Logging.info(LOG_SIG, "Generated #{element.to_str}")
  end

  def create_env(season)
    environment = @configuration_manager.config_to_env
    environment.merge(season_to_env(season))
  end

  def gather_season_elements(season, elements)
    recipe = @recipe_manager.get_recipe(season.recipe_id)
    gathered_elements = recipe.infrastructure
    gathered_elements.each do |element|
      element.season_id = season.id
    end
    elements << gathered_elements
  end
end
