# frozen_string_literal: true

require 'tablomat'
require 'artifactomat/modules/logging'
require 'artifactomat/models/test_episode'
require 'artifactomat/models/action'
require 'artifactomat/volatile_store'
require 'artifactomat/modules/scheduler'
require 'active_support/core_ext/object/blank'

class RoutingRunningError < StandardError
end

# This class models the routing module used for ip
# and DNS based routing of subject requests
class Routing
  LOG_SIG = 'Routing'
  MIN_PORT = 1
  MAX_PORT = 65_535

  # rubocop:disable Metrics/AbcSize
  def initialize(conf_manager, recipe_manager)
    @configuration_manager = conf_manager
    @recipe_manager = recipe_manager
    @routing_elements = []
    @available_external_ports = []
    @available_internal_ports = []
    @ext_ip = '0.0.0.0'
    @int_ip = '127.0.0.1'

    @vstore = VolatileStore.new conf_manager
    load_config_data
    check_routing_flags

    @dyn_router = (Thread.new {}).join

    @scheduler = Scheduler.new
    @iptables = Tablomat::IPTables.new
    @ipset = Tablomat::IPSet.new
    @iptables_mutex = Mutex.new
    @iptables.activate

    recreate_state
  end
  # rubocop:enable Metrics/AbcSize

  def run
    @dyn_router = Thread.new { dyn_router }
  end

  def running?
    %w[sleep run].include? @dyn_router.status
  end

  # rubocop:disable Metrics/AbcSize
  # creates routing elements for each infrastructure element of a series
  def create_routing(test_series, exclude_seasons = [])
    create_series_ipsets test_series
    test_series.test_seasons.each do |season|
      next if exclude_seasons.include? season.id

      elements = gather_season_infrastucture_elements(season)
      elements.each do |element|
        re = create_routing_element(element.ports, test_series.id, season.id)
        create_re_iptables_rules re
        Logging.debug(LOG_SIG, "Added Routing Element with #{re.inspect}.")
        @routing_elements << re
      end
    end
    Logging.info(LOG_SIG, "Created routing for Series #{test_series.id}")
    add_destroy_job(test_series)
  end
  # rubocop:enable Metrics/AbcSize

  def remove_routing(test_series)
    deactivate_all_episodes test_series
    remove_routing_elements test_series
    remove_series_ipsets test_series
    Logging.info(LOG_SIG, "Removed routing for Series #{test_series.id}.")
  end

  # returns internal ports of a season
  def get_internal_ports(season)
    internal_ports = []
    @routing_elements.each do |re|
      internal_ports.concat re.internal_ports if re.season_id == season.id
    end
    internal_ports
  end

  # returns external ip of a season
  def get_external_address(_season)
    @ext_ip
  end

  # def update(subject, old_ip, ip)
  #   @iptables.switchSources(old_ip, ip)
  #   Logging.info(LOG_SIG, "Changed routing for subject #{subject} from #{old_ip} to #{ip}")
  # end

  def activate(episode)
    # push the names to the redis instance
    load_names(episode)
    episode.update routing_active: true
    activate_routing(episode)
  end

  def deactivate(episode)
    unload_names(episode)
    episode.update routing_active: false
    deactivate_routing(episode)
  end

  def unschedule_jobs(series)
    @scheduler.jobs(tag: series.to_str).each(&:unschedule)
    true # return something json serializable for the redis-rpc
  end

  private

  def recreate_destroy_jobs
    TestSeries.where(status: %i[prepared approved]).find_each do |test_series|
      add_destroy_job(test_series)
    end
  end

  def add_destroy_job(test_series)
    @scheduler.at(test_series.end_date, tags: ['remove routing', test_series.to_str]) do
      remove_routing(test_series)
    end
  end

  def deactivate_all_episodes(test_series)
    test_series.test_seasons.each do |season|
      season.test_episodes do |episode|
        deactivate(episode)
      end
    end
  end

  def remove_routing_elements(test_series)
    @routing_elements.delete_if do |re|
      if re.series_id == test_series.id
        remove_re_iptables_rules re
        free_internal_ports(re.internal_ports)
        Logging.debug(LOG_SIG,
                      "Removed Routing Element with #{re.inspect}.")
        true
      end
    end
  end

  # Recreates the state for all active and inactive routing elements, because this directly influences
  # which ports are still available
  def recreate_state
    # Recreate active routing elements
    active_season_ids = recreate_active_routing_elements.map(&:id)

    # Prepare inactive routing elements
    recreate_inactive_routing_elements active_season_ids
    recreate_destroy_jobs
  end

  def unify_chunk(source, chunk_data)
    internal_ip = chunk_data[0][:to_dest_ip]
    external_ip = chunk_data[0][:destination]
    # Map each external port to its internal counterpart
    port_map = chunk_data.map { |c| [c[:dport].to_i, c[:to_dest_port].to_i] }.to_h

    [source, { internal_ip: internal_ip, external_ip: external_ip, source: source,
               port_map: port_map }]
  end

  def load_relevant_iptables_rules
    key_set = %i[to_dest match destination dport].to_set
    @iptables.get_active_rules
             .filter { |r| key_set.subset? r.keys.to_set }
             .map do |r|
      ip, port = r[:to_dest].split ':'
      r[:to_dest_ip] = ip
      r[:to_dest_port] = port
      r
    end
  end

  def infer_state_from_iptables_rules(rules)
    rules.sort_by! { |r| r[:match] }
    rules.chunk { |r| r[:match] }.map { |source, chunk_data| unify_chunk(source, chunk_data) }.to_h
  end

  # rubocop:disable Metrics/AbcSize
  # :reek:NilCheck
  def recreate_active_routing_elements
    ipt_rules = load_relevant_iptables_rules
    old_state = infer_state_from_iptables_rules ipt_rules
    re_set = Set.new
    active_seasons = Set.new

    TestEpisode.where(routing_active: true).find_each do |te|
      active_seasons.add te.test_season
    end

    # For each active episode, get the needed external ports for each IE
    # Look up the corresponding internal ports, construct routing elements and remove the internal
    # ports from @available_internal_ports
    active_seasons.each do |season|
      season_id = season.id
      season_rules = old_state["itsape-#{season_id}"]
      series_id = season.test_series.id
      recipe = @recipe_manager.get_recipe(season.recipe_id)
      infra_elems = recipe.infrastructure

      infra_elems.each do |ie|
        iports = ie.ports.map { |p| season_rules[:port_map][p.to_i] }
        new_re = RoutingElement.new series_id, season_id
        new_re.external_ip = season_rules[:external_ip]
        new_re.internal_ip = season_rules[:internal_ip]
        new_re.external_ports = ie.ports.map(&:to_i)
        new_re.internal_ports = iports
        re_set.add new_re
      end
    end

    re_set.each do |re|
      re.internal_ports.each { |p| @available_internal_ports.delete p }
      @routing_elements << re
    end
    active_seasons
  end
  # rubocop:enable Metrics/AbcSize

  def recreate_inactive_routing_elements(active_seasons)
    TestSeries.where(status: %i[prepared approved]).find_each do |ts|
      create_routing ts, active_seasons
    end
  end

  def dyn_router
    @vstore.subscribe_to_subjectracker_updates do |msg|
      case msg['action']
      when 'add'
        e = get_active_episode(msg['data']['sid'])
        activate_routing(e, msg['data']['ip']) if e
      when 'del'
        e = get_active_episode(msg['data']['sid'])
        deactivate_routing(e, msg['data']['ip']) if e
      end
    end
  end

  def get_active_episode(sid)
    TestEpisode.find_by routing_active: true, subject_id: sid
  end

  def create_routing_element(ports, test_series_id, season_id)
    ext_ports = check_external_ports(ports)
    int_ports = create_internal_ports(ports.length)
    re = RoutingElement.new(test_series_id, season_id)
    re.external_ip = @ext_ip
    re.internal_ip = @int_ip
    re.external_ports = ext_ports
    re.internal_ports = int_ports
    re
  end

  def create_series_ipsets(series)
    series.test_seasons.each do |season|
      @iptables_mutex.synchronize do
        @ipset.create(set_name: "itsape-#{season.id}", type: 'hash:ip')
      rescue Tablomat::IPSetError => e
        raise unless e.message.include?('Set cannot be created: set with the same name already exists')

        Logging.error(LOG_SIG, "Could not create ipset 'itsape-#{season.id}': #{e.message}")
      end
    end
  end

  def remove_series_ipsets(series)
    series.test_seasons.each do |season|
      @iptables_mutex.synchronize do
        @ipset.destroy(set_name: "itsape-#{season.id}")
      end
    end
  end

  def matchset_for_routing_element(routing_element)
    @ipset.matchset(set_name: "itsape-#{routing_element.season_id}", flags: 'src')
  end

  def add_ip_to_season_ipset(subject_ip, season_id)
    @iptables_mutex.synchronize do
      @ipset.add(set_name: "itsape-#{season_id}", entry_data: subject_ip)
    rescue Tablomat::IPSetError => e
      raise unless e.message.include?("Element cannot be added to the set: it's already added")

      Logging.error(LOG_SIG, "Could not add #{subject_ip} to ipset itsape-#{season_id}: #{e.message}")
    end
  end

  def remove_ip_from_season_ipset(subject_ip, season_id)
    @iptables_mutex.synchronize do
      @ipset.del(set_name: "itsape-#{season_id}", entry_data: subject_ip)
    rescue Tablomat::IPSetError => e
      error_message = e.message
      raise unless error_message.include?("Element cannot be deleted from the set: it's not added") || error_message.include?('The set with the given name does not exist')

      Logging.error(LOG_SIG, "Could not remove #{subject_ip} from ipset itsape-#{season_id}: #{error_message}")
    end
  end

  def create_re_iptables_rules(routing_element)
    @iptables_mutex.synchronize do
      routing_element.external_ports.length.times do |port_number|
        @iptables.append('nat',
                         'PREROUTING',
                         set: matchset_for_routing_element(routing_element),
                         destination: routing_element.external_ip,
                         protocol: :tcp,
                         dport: routing_element.external_ports[port_number],
                         jump: 'DNAT',
                         to: "#{routing_element.internal_ip}:#{routing_element.internal_ports[port_number]}")
      end
    end
  end

  def remove_re_iptables_rules(routing_element)
    @iptables_mutex.synchronize do
      routing_element.external_ports.length.times do |port_number|
        @iptables.delete('nat',
                         'PREROUTING',
                         set: matchset_for_routing_element(routing_element),
                         destination: routing_element.external_ip,
                         protocol: :tcp,
                         dport: routing_element.external_ports[port_number],
                         jump: 'DNAT',
                         to: "#{routing_element.internal_ip}:#{routing_element.internal_ports[port_number]}")
      end
    end
  end

  def activate_routing(episode, ip = nil)
    subject_ip = ip
    subject_ip = @vstore.ip_by_sid(episode.subject_id) unless ip
    return if subject_ip.blank?

    add_ip_to_season_ipset(subject_ip, episode.test_season.id)
    Logging.info(LOG_SIG, "Enabled routing for episode #{episode.id}.")
  end

  def deactivate_routing(episode, ip = nil)
    subject_ip = ip
    subject_ip = @vstore.ip_by_sid(episode.subject_id) unless ip
    return if subject_ip.blank?

    remove_ip_from_season_ipset(subject_ip, episode.test_season.id)
    Logging.info(LOG_SIG, "Disabled routing for episode #{episode.id}.")
  end

  def load_config_data
    @available_internal_ports = cast_port_array @configuration_manager.get('conf.network.available_internal_ports')
    @available_external_ports = cast_port_array @configuration_manager.get('conf.network.available_external_ports')
    @ext_ip = @configuration_manager.get('conf.network.external_ip')
    @int_ip = '127.0.0.1' # for future use
  end

  def get_routing_elements(season_id)
    @routing_elements.filter { |re| re.season_id == season_id }
  end

  # returns an array of InfrastructureElements
  def gather_season_infrastucture_elements(season)
    recipe = @recipe_manager.get_recipe(season.recipe_id)
    element = recipe.infrastructure
    element.each do |elem|
      elem.season_id = season.id
    end
    element
  end

  # :reek:NilCheck
  # returns free internal ports
  def create_internal_ports(number)
    ports = @available_internal_ports.pop number

    # If there aren't enough ports, add the ones taken back to the pool before raising
    # to improve recoverability
    if ports.length != number
      @available_internal_ports.concat ports
      raise(RoutingRunningError, 'Internal ports exhausted.')
    end

    ports
  rescue RoutingRunningError => e
    Logging.error(LOG_SIG, e.message)
    raise
  end

  def free_internal_ports(ports)
    @available_internal_ports.concat ports
  end

  # returns array with port range [lowest_port, highest_port]
  def cutting_port_range(port_string)
    port_range = /([0-9]+)-?([0-9]*)/.match(port_string.to_s)
    port_range = port_range.to_a
    return [port_range[1].to_i] if port_range[2].to_i.zero?

    (port_range[1].to_i..port_range[2].to_i).to_a
  end

  # cast an array with given single ports and port ranges to an array with
  # integers for single ports and arrays [first_port, last_port] for port ranges
  def cast_port_array(port_array)
    formatted_port_array = []
    port_array.each do |port_string|
      port_range = cutting_port_range(port_string)
      formatted_port_array.concat port_range
    end
    sanitize_ports formatted_port_array.uniq
  end

  def sanitize_ports(ports)
    ports.delete_if do |port|
      unless port_in_range? port
        Logging.warning(LOG_SIG, "Removing port #{port}. Not a port.")
        true
      end
    end
    ports
  end

  def check_external_ports(ports)
    clean_ports = []
    ports.each do |port|
      port = port.to_i
      raise(RoutingRunningError, "Can't route #{port}, not a port.") unless port_in_range? port
      raise(RoutingRunningError, "Can't route #{port}, not available.") unless external_port_configured? port

      clean_ports.push port
    end
    clean_ports
  rescue RoutingRunningError => e
    Logging.error(LOG_SIG, e.message)
    raise
  end

  def port_in_range?(port)
    port.between?(MIN_PORT, MAX_PORT)
  end

  def external_port_configured?(port)
    @available_external_ports.include? port
  end

  def load_names(episode)
    recipe = @recipe_manager.get_recipe(episode.test_season.recipe_id)
    recipe.infrastructure.each do |infrastructure|
      next unless infrastructure.instance_variable_defined?(:@names)

      infrastructure.names.each do |name|
        @vstore.register_subject(name, episode.subject_id)
      end
    end
  rescue StandardError => e
    Logging.error(LOG_SIG, "Could not activate names for #{episode.subject_id}: #{e.message}")
  end

  def unload_names(episode)
    recipe = @recipe_manager.get_recipe(episode.test_season.recipe_id)
    recipe.infrastructure.each do |infrastructure|
      next unless infrastructure.instance_variable_defined?(:@names)

      infrastructure.names.each do |name|
        @vstore.unregister_subject(name, episode.subject_id)
      end
    end
  rescue StandardError => e
    Logging.error(LOG_SIG, "Could not deactivate names for #{episode.subject_id}: #{e.message}")
  end

  def check_routing_flags
    Logging.warning(LOG_SIG, "IP Forwarding is disabled! Please run 'sysctl -w net.ipv4.ip_forward=1'") unless IO.read('/proc/sys/net/ipv4/ip_forward').to_i == 1
    Logging.warning(LOG_SIG, "Local Routing is disabled! Please run 'sysctl -w net.ipv4.conf.all.route_localnet=1' in case you need to reroute traffic to local Docker containers.") unless IO.read('/proc/sys/net/ipv4/conf/all/route_localnet').to_i == 1
  end
end
