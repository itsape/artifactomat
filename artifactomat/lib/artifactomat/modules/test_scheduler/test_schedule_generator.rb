# frozen_string_literal: true

require 'biz'
require 'artifactomat/models'

# helper for the test scheduler, generates schedules
class TestScheduleGenerator
  attr_reader :biz, :cooldown

  def initialize(conf_man, sub_man)
    @biz = generate_biz conf_man.get('conf.scheduler.biz')
    @cooldown = conf_man.get('conf.scheduler.cooldown')
    @subject_manager = sub_man
  end

  def generate_schedule(series)
    testtime = series_testtime series
    time_avail = series_runtime series
    raise ApeError, "Unable to schedule series #{series.to_str}: Available time too short. End date needs to be at least #{@biz.time(testtime, :minutes).after(series.start_date)}." unless testtime <= time_avail

    extra_time = time_avail - testtime
    subjects(series).each do |subject|
      schedule_subjects_episodes(series, subject, series.start_date, extra_time)
    end
    series.scheduled!
    series.save!
  end

  def series_testtime(series)
    series.test_seasons.sum(:duration) + (series.test_seasons.length - 1) * (@cooldown / 60)
  end

  private

  def schedule_subjects_episodes(series, subject, start, extra_time)
    randgen = Random.new
    pad = 0

    episodes(series, subject).each do |episode|
      rand_max = extra_time - pad
      new_pad = rand_max.positive? ? randgen.rand(rand_max) : 0
      start = @biz.time(new_pad, :minutes).after(start)
      start = schedule_episode(start, episode)
      pad += new_pad + @cooldown / 60
    end
  end

  def schedule_episode(start, episode)
    episode.schedule_start = start
    episode.schedule_end = @biz.time(duration(episode), :minutes).after(episode.schedule_start)
    episode.save!
    @biz.time(@cooldown, :seconds).after(episode.schedule_end)
  end

  def subjects(series)
    @subject_manager.get_subjects(series.group_file)
  end

  def episodes(series, subject)
    series.test_episodes.includes(:test_season).where(subject_id: subject.id).order(Arel.sql('RANDOM()'))
  end

  def duration(episode)
    episode.test_season.duration
  end

  # :reek:NilCheck
  def series_runtime(series)
    raise ApeError, "Unable to schedule series #{series.id}. Series has no end date." if series.end_date.nil?

    @biz.within(series.start_date, series.end_date).in_minutes
  end

  def generate_biz(biz_cfg)
    Biz::Schedule.new do |config|
      config.hours = biz_cfg['hours']
      config.breaks = biz_cfg['breaks']
      config.holidays = biz_cfg['holidays']
      config.time_zone = biz_cfg['timezone']
    end
  end
end
