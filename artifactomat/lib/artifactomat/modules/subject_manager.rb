# frozen_string_literal: true

require 'csv'

require 'active_support'
require 'active_support/core_ext/object/blank'

require 'artifactomat/modules/logging'
require 'artifactomat/models/subject'
require 'artifactomat/modules/configuration_manager'

# :reek:InstanceVariableAssumption
# :reek:RepeatedConditional
# :reek:DataClump
# Subject Manager
class SubjectManager
  SUBJECT_FOLDER = 'subjects'
  SUBJECT_BLACKLIST = 'subject_blacklist'
  SUBJECT_GID_DELIMITER = '/'
  REQUIRED_SUBJECT_HEADERS = %w[id name surname userid email].freeze
  CONF_PATH = '/etc/artifactomat'
  LOG_SIG = 'SubjectManager'

  attr_reader :blacklisted_subjects

  class << self
    attr_reader :default_path
  end

  @default_path = File.expand_path(File.join(
                                     File.dirname(__FILE__),
                                     '..',
                                     '..',
                                     '..',
                                     'dist',
                                     'etc',
                                     'artifactomat'
                                   ))

  # :reek:NilCheck
  def initialize(path = nil)
    @cfg_path = self.class.default_path
    @cfg_path = CONF_PATH if File.readable? File.join CONF_PATH, SUBJECT_FOLDER
    @cfg_path = path unless path.nil?

    # check if path exists
    raise ConfigError, "'#{@cfg_path}' is no directory" unless File.directory?(@cfg_path)

    # check if readable
    raise ConfigError, "'#{@cfg_path}' is not readable" unless File.readable?(@cfg_path)

    init_subjects
    Logging.info(LOG_SIG, 'Initialized subjects.')
  end

  # list all available files in subjects-path
  def list_subject_groups
    @subject_groups.keys
  end

  # returns an array of Subjects for given group-file
  def get_subjects(gid, subject_groups = @subject_groups)
    unless subject_groups.key? gid
      Logging.error(LOG_SIG, "Unknown subject group '#{gid}'")
      raise ConfigError, "Unknown subject group '#{gid}'"
    end
    subject_groups[gid]
  end

  def get_subject(identifier, subject_groups = @subject_groups)
    gid, sid, = identifier.split(SUBJECT_GID_DELIMITER)
    candidates = get_subjects(gid, subject_groups)
    candidates.each do |subject|
      return subject if identifier == subject.id
    end
    raise ConfigError, "Unknown subject '#{sid}' in group '#{gid}'."
  end

  def get_subjects_for_userid(userid)
    @subject_groups.values.flatten.select { |subject| subject.userid == userid }
  end

  private

  # load group-files and create subjects:
  # Create empty subject_groups Hash; then,
  # for each group-file, create an Array with Subjects.
  def init_subjects
    @subject_groups = {}
    @blacklisted_subjects = []
    subject_blacklist = load_subject_blacklist
    # csv_options = {
    #   col_sep: @cfg['conf']['subjects']['col_sep'],
    #   quote_char: @cfg['conf']['subjects']['quote_char'],
    #   skip_blanks: true
    # }
    subject_folder = File.join @cfg_path, SUBJECT_FOLDER
    unless File.directory?(subject_folder)
      Logging.error(LOG_SIG, "#{subject_folder} not a directory")
      return
    end
    parse_subject_folder(subject_folder, subject_blacklist)
    @subject_groups
  end

  def parse_subject_folder(subject_folder, blacklist)
    Dir.entries(subject_folder).each do |group_name|
      next if ['.', '..'].include? group_name
      next if ['.md', '.MD'].include? File.extname(group_name)

      group_file = File.join @cfg_path, SUBJECT_FOLDER, group_name
      header, group_subjects = parse_data(group_file)
      if (header & REQUIRED_SUBJECT_HEADERS) != REQUIRED_SUBJECT_HEADERS
        Logging.error(LOG_SIG, "Could not load subject group: #{group_name}")
        raise ConfigError, ('Bad Group-File: ' + group_name)
      else
        add_group_to_subjects(group_subjects, header, group_name, blacklist)
      end
    end
    @subject_groups
  end

  def parse_data(file)
    data = CSV.read(file, skip_blanks: true)
    header = data.shift.map(&:to_s)
    string_data = data.map { |row| row.map(&:to_s) }
    [header, string_data.map { |row| Hash[*header.zip(row).flatten] }]
  end

  def load_subject_blacklist
    subject_blacklist_file = File.join @cfg_path, SUBJECT_BLACKLIST
    unless File.exist? subject_blacklist_file
      Logging.info(LOG_SIG, 'Subject blacklist file is missing')
      return []
    end

    blacklist_array = CSV.read(subject_blacklist_file, skip_blanks: true)
    blacklist_array_to_hash(blacklist_array)
  end

  # :reek:NilCheck
  def blacklist_array_to_hash(blacklist_array)
    header = blacklist_array.shift.map(&:to_s)
    # convert all csv entries to strings, except for `nil`/empty entry
    string_subjects = blacklist_array.map { |row| row.map { |e| e.nil? ? nil : e.to_s } }
    # return Hashes `header_key => value` for each blacklisted subject, removing nil values
    string_subjects.map { |row| Hash[*header.zip(row).flatten].compact }
  end

  def add_group_to_subjects(group_subjects, headers, group_name, blacklist)
    @subject_groups[group_name] = []
    blacklisted_subjects = blacklist_matches_for_group(group_subjects, group_name, blacklist)
    filtered_group_subjects = filter_group_subjects(group_subjects, blacklisted_subjects, group_name)

    filtered_group_subjects.each do |subject_data|
      subject = parse_subject(group_name, subject_data, headers)
      @subject_groups[group_name].push(subject)
    end
  end

  def blacklist_matches_for_group(group_subjects, group_name, blacklist)
    blacklisted_subjects = Hash.new { |h, k| h[k] = [] }
    group_subjects.each do |subject_hash|
      blacklist_ids = blacklist_filter(subject_hash, group_name, blacklist)
      blacklist_ids.each do |blacklist_id|
        blacklisted_subjects[blacklist_id] << subject_hash
      end
    end
    blacklisted_subjects
  end

  def blacklist_filter(subject_data, group_name, blacklist)
    matches = []
    blacklist.each do |blacklist_subject|
      match = true
      blacklist_subject.each do |key, value|
        next if key == 'blacklist_id'

        match &= if key == 'subject_id'
                   group_name + SUBJECT_GID_DELIMITER + subject_data['id'] == value
                 else
                   subject_data[key] == value
                 end
      end
      matches << blacklist_subject['blacklist_id'] if match
    end
    matches
  end

  def filter_group_subjects(group_subjects, blacklisted_subjects, group_name)
    blacklisted_subjects.each do |blacklist_id, matched_subjects|
      if matched_subjects.size > 1
        subject_ids = matched_subjects.map { |sub| sub['id'] }.join(', ')
        Logging.error(LOG_SIG, "Multiple blacklist matches for blacklist-id '#{blacklist_id}' " \
                               "in groupfile '#{group_name}': [#{subject_ids}]. None removed!")
      else
        subject_data = matched_subjects[0]
        Logging.info(LOG_SIG, "Blacklist match! group_file: '#{group_name}', subject-id: " \
                              "'#{subject_data['id']}', blacklist-id: '#{blacklist_id}'. " \
                              'Removing subject from DB!')
        group_subjects.delete(subject_data)
        @blacklisted_subjects << "#{group_name}#{SUBJECT_GID_DELIMITER}#{subject_data['id']}"
      end
    end
    group_subjects
  end

  def parse_subject(group_name, data, headers)
    additional_headers = (headers - REQUIRED_SUBJECT_HEADERS)
    subject = Subject.new(
      group_name + SUBJECT_GID_DELIMITER + data['id'],
      data['name'],
      data['surname'],
      data['userid'],
      data['email']
    )
    additional_headers.each do |a|
      subject.add_field(a)
      subject.send("#{a}=".to_sym, data[a])
    end
    subject
  end
end
