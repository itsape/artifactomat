# frozen_string_literal: true

require 'net/http'
require 'sinatra/base'
require 'artifactomat/models/job'
require 'artifactomat/models/detection_event'
require 'artifactomat/utils/bullet_loader'
require 'artifactomat/modules/configuration_manager'
require 'artifactomat/modules/subject_manager'
require 'artifactomat/volatile_store'
require 'csv'
require 'json'
require 'yaml'
require 'base64'
require 'cgi'
require 'minitar'
require 'bzip2/ffi'

def build_ssl_context(cert_path)
  ssl_ctx = OpenSSL::SSL::SSLContext.new
  ssl_ctx.cert = OpenSSL::X509::Certificate.new File.read "#{cert_path}/public/client.itsape.pem"
  ssl_ctx.key = OpenSSL::PKey::RSA.new File.read "#{cert_path}/private/client.itsape.key"
  ssl_ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER | OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT
  ssl_ctx.ca_file = "#{cert_path}/public/ca.pem"
  ssl_ctx
end

def build_connection(cert_path, host = 'localhost', port = 8123)
  ssl_ctx = build_ssl_context(cert_path)
  socket = TCPSocket.new(host, port)
  ssl_socket = OpenSSL::SSL::SSLSocket.new(socket, ssl_ctx)
  ssl_socket.sync_close = true
  connection = ssl_socket.connect
  connection
end

# The Jobspooler Webservice // requested by the clients
class Jobspooler < Sinatra::Base
  set :sessions, true

  configure_bullet_for_sinatra

  def initialize(options = {}, **_kwargs)
    super()
    @options = options
    @tracker = nil
    @configuration_manager = ConfigurationManager.new
    @subject_manager = SubjectManager.new
    @vstore = VolatileStore.new(@configuration_manager)
  end

  # :reek:NilCheck
  def send_data_to_st(hash)
    Thread.new do
      @tracker = build_connection(@options['cert_path'])
      @tracker.puts(hash.to_yaml)
    rescue StandardError => e
      puts "ERROR SUBJECT TRACKER: #{e.message}"
    ensure
      @tracker&.close
    end
  end

  def send_data_to_tc(payload, job)
    Thread.new do
      track = "#{Time.now} | #{payload} | #{job.to_str}"
      connection = build_connection(@options['cert_path'], 'localhost', @options['tc_port'])
      connection.puts '1'
      connection.puts(track)
      _ = connection.gets
      connection.close
    end
  end

  def job_url(req, job)
    "#{req.scheme}://#{req.host}:#{req.port}/jobs/#{job.user}/#{job.id}"
  end

  def create_job_list(jobs, _user, request)
    job_list = []
    jobs.each do |job|
      job_list << job_url(request, job)
    end
    job_list
  end

  def create_screenshots_archive_if_not_exist(season)
    archive_path = "/tmp/artifactomat-#{season.screenshots_hash}.tar.bz2"
    current_dir = Dir.pwd
    screenshots_dir_path = season.absolute_screenshots_folder
    screenshots_names = season.screenshots_filenames
    Dir.chdir(screenshots_dir_path) # chdir in order to not leak absolute paths in tar
    unless File.exist?(archive_path)
      Bzip2::FFI::Writer.open(archive_path) do |writer|
        Minitar.pack(screenshots_names, writer)
      end
    end
    Dir.chdir(current_dir)
    archive_path
  end

  def infer_episode_id(userid, timestamp, ip)
    sid_candidates = @subject_manager.get_subjects_for_userid(userid).map(&:id)
    sids_matching_ip = @vstore.sids_by_ip(ip)
    sid_candidates &= sids_matching_ip
    te_candidates = TestEpisode.where(subject_id: sid_candidates) \
                               .where('schedule_start <= :timestamp AND schedule_end >= :timestamp',
                                      timestamp: timestamp)
    raise "Could not identify test episode for user #{userid} and time #{timestamp}" if te_candidates.size != 1

    te_candidates[0].id
  end

  get '/', provides: ['html'] do
    # return html status page
    ['<!DOCTYPE html>' \
    '<html><head><meta charset="UTF-8"><title>ITS.APT Jobspooler</title></head>' \
    '<body><h1>ITS.APT Jobspooler</h1>' \
    '<p>Hello!</body></p>']
  end

  get '/', provides: ['json'] do
    # return json status page
    [200, "Hello, I\'m the ITS.APT Jobspooler!"]
  end

  # update status for a job
  post '/jobs/:user/:id' do |_user, id|
    job = Job.find(id)

    request.body.rewind
    payload = JSON.parse(request.body.read)

    if payload.key?('status')
      status = payload['status']
    elsif payload.key?('Status')
      status = payload['Status']
    end
    return [500, 'no status update'] if status.nil?

    job.collection_date = Time.now
    job.status = status
    job.save!

    send_data_to_tc payload, job

    return [204]
  rescue ActiveRecord::RecordNotSaved, ActiveRecord::RecordInvalid => e
    [500, e.message]
  rescue ActiveRecord::RecordNotFound
    not_found
  rescue ArgumentError => e
    [500, e.message]
  end

  # query all waiting jobs for a user
  get '/jobs/:user', provides: ['json'] do |user|
    send_data_to_st(Hash[user, { ip: request.ip, action: 'login' }]) if @options['tracking'] == true
    jobs = Job.where('jobs.user = ? and status = 0 and (expiration_date > ? or expiration_date is null)', user, Time.now)
    return [204, 'No jobs available.'] if jobs.count.zero?

    body_response = create_job_list(jobs, user, request)
    status 200
    content_type :json
    body body_response.to_json
  end

  get '/jobs/:user/:id', provides: ['json'] do |_user, id|
    job = Job.find(id)
    [200, job.to_json]
  rescue ActiveRecord::RecordNotFound
    not_found
  end

  get '/file/:id' do |id|
    job = Job.find(id)
    if File.exist?(job.file_path)
      content_type 'application/octet-stream'
      file = File.new(job.file_path)
      stream do |out|
        # Read 10 MiB and push to stream
        until (bytes = file.read(1024 * 1024 * 10)).nil?
          out << bytes
        end
      end
    else
      not_found
    end
  rescue ActiveRecord::RecordNotFound
    not_found
  end

  post '/timetables/:username' do |username|
    tmpfile = params[:file][:tempfile]
    timetable_rows = CSV.parse(Bzip2::FFI::Reader.read(tmpfile))
    # take the middle row for highest chance of being within episode start and end time
    inference_timestamp = DateTime.strptime(timetable_rows[timetable_rows.size / 2][0], '%y%m%d%H%M%S%L')
    episode_id = infer_episode_id(username, inference_timestamp, request.ip)
    timetable_rows.each do |row|
      DetectionEvent.from_detection_timetable_row(row, episode_id)
    end
    [201]
  end

  get '/screenshots/:season_id' do |season_id|
    season = TestSeason.find(season_id)
    archive_path = create_screenshots_archive_if_not_exist(season)
    hash_sum = File.basename(archive_path).split('.')[0].split('-')[1]
    content_type 'application/octet-stream'
    headers['MD5'] = hash_sum
    file = File.new(archive_path)
    stream do |out|
      until (bytes = file.read(1024 * 1024 * 10)).nil?
        out << bytes
      end
    end
  rescue ConfigError, ActiveRecord::RecordNotFound
    not_found
  rescue RuntimeError => e
    raise unless e.message.include?('has no screenshots')

    not_found
  end
end
