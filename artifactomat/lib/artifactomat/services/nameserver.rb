# frozen_string_literal: true

require 'simpleidn'
require 'async/dns'

require 'artifactomat/volatile_store'
require 'artifactomat/modules/logging'

# ITSAPE Nameserver that supports rerouting
# :reek:InstanceVariableAssumption
class Nameserver < Async::DNS::Server
  CUSTOM_URLS_CONFIG_KEY = 'conf.nameserver.custom_urls.'
  LOG_SIG = 'APENames'

  attr_reader :upstream_ip
  attr_reader :upstream_port
  attr_reader :address
  attr_reader :port

  def initialize(conf_man)
    load_config(conf_man)
    # this does not care for errors
    @vstore = VolatileStore.new(conf_man)
    @upstream = build_resolver
    @magic_url = 'its.apt'
    @custom_urls = build_custom_urls(conf_man)
    super([[:udp, @address, @port], [:tcp, @address, @port]])
  end

  #:reek:ControlParameter
  def process(name, resource_class, transaction)
    # transaction.passthrough!(@upstream)
    if resource_class == Resolv::DNS::Resource::IN::AAAA
      transaction.passthrough!(@upstream)
      return
    end
    if name == @magic_url
      magic_ip(transaction)
      return
    end
    return if custom_static(name, transaction)

    route(name, transaction)
  rescue StandardError => e
    Logging.error(LOG_SIG, "Processing Failed: #{e}, Backtrace:\n\t#{e.backtrace.join("\n\t")}")
    transaction.fail!(:NXDomain)
  end

  private

  def load_config(conf_man)
    @address = conf_man.get('conf.nameserver.address').to_s
    @port = conf_man.get('conf.nameserver.port').to_i
    @infrastructure = conf_man.get('conf.network.external_ip').to_s
    @upstream_ip = conf_man.get('conf.nameserver.upstream.address').to_s
    @upstream_port = conf_man.get('conf.nameserver.upstream.port').to_i
  end

  def route(name, transaction)
    if reroute? name, transaction
      transaction.respond!(@infrastructure)
    else
      transaction.passthrough!(@upstream)
    end
  end

  def build_custom_urls(conf_man)
    custom_urls = {}
    configs = conf_man.get("#{CUSTOM_URLS_CONFIG_KEY}*")
    configs.each do |url, ip|
      clean_url = url[(CUSTOM_URLS_CONFIG_KEY.size)..(url.size - 1)]
      custom_urls[clean_url] = ip
    end
    custom_urls
  end

  def custom_static(name, transaction)
    return false unless @custom_urls.key?(name)

    ip = @custom_urls[name]
    transaction.respond!(ip)
    true
  end

  def magic_ip(transaction)
    client_ip = transaction.options[:remote_address].ip_address
    magic_response = '127.0.0.2'
    magic_response = '127.0.0.1' unless @vstore.sids_by_ip(client_ip).empty?
    transaction.respond!(magic_response)
  end

  def reroute?(name, transaction)
    client_ip = transaction.options[:remote_address].ip_address
    registered_subjects = @vstore.get_registered_subjects(name)
    return false if registered_subjects.empty?

    client = @vstore.sids_by_ip(client_ip)
    return true unless (client & registered_subjects).empty?

    false
  end

  def build_resolver
    Async::DNS::Resolver.new(
      [
        [:udp, @upstream_ip, @upstream_port],
        [:tcp, @upstream_ip, @upstream_port]
      ], origin: nil, logger: nil
    )
  end
end
