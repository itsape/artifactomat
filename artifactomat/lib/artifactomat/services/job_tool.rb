# frozen_string_literal: true

require 'gli'
require 'fileutils'
require 'yaml'
require 'httpclient'
require 'net/http'

# Allows recipe scripts or the user to create or delete jobs
# :reek:RepeatedConditional
class JobTool
  extend GLI::App

  program_desc 'ape-job-tool allows recipe-scripts and the user to create or delete jobs for the ape-job-spooler'

  subcommand_option_handling :normal
  arguments :strict

  pre do |glob|
    glob[:expiration] = nil
    glob[:expiration] = Time.parse(ENV['ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END']).round.utc if ENV.key? 'ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END'
    true
  end

  flag :port, desc: 'Port that the ape-job-clerk runs on', type: Integer
  flag :host, desc: 'IP that the ape-job-clerk runs on', type: String

  # rubocop:disable Metrics/BlockLength
  desc 'Build jobs'
  command 'build' do |build|
    build.desc 'Create job to run a file'
    build.long_desc %(
    Create job to run a file to a users host. The file has to exist on this system. Set environment variable ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END if you want this job to expire.
    )
    build.arg_name '<user> <external_file>'
    build.command(:run) do |cmd|
      cmd.action do |glob, _options, args|
        exit_now!('Wrong number of arguments, needs: <user> <file>') unless args.length == 2

        resp = send_request(glob, '/build/execute', action: 'execute', user: args[0], file_path: args[1], deploy_path: '', expiration_date: glob[:expiration], creation_date: Time.now)
        exit_now!(resp.content) unless HTTP::Status.successful?(resp.status)
      end
    end

    build.desc 'Create job to copy a file'
    build.long_desc %(
    Create job to copy a file to a users host. The file has to exist on this system. Set environment variable ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END if you want this job to expire.
    )
    build.arg_name '<user> <file> <place>'
    build.command(:copy) do |cmd|
      cmd.action do |glob, _options, args|
        exit_now!('Wrong number of arguments, needs: <user> <file> <place>') unless args.length == 3
        exit_now!("#{args[1]} is not a valid file") unless File.file?(args[1])

        resp = send_request(glob, '/build/copy', action: 'copy', user: args[0], file_path: args[1], deploy_path: args[2], expiration_date: glob[:expiration], creation_date: Time.now)
        exit_now!(resp.content) unless HTTP::Status.successful?(resp.status)
      end
    end

    build.desc 'Create job to remove a file'
    build.long_desc %(
    Create job to remove a file on a clients host. This file has to be copied to the user first. Remove jobs never expire.
    )
    build.arg_name '<user> <external_file>'
    build.command(:remove) do |cmd|
      cmd.action do |glob, _options, args|
        exit_now!('Wrong number of arguments, needs: <user> <file>') unless
          args.length == 2

        resp = send_request(glob, '/build/remove', action: 'remove', user: args[0], file_path: '', deploy_path: args[1], expiration_date: nil, creation_date: Time.now)
        exit_now!(resp.content) unless HTTP::Status.successful?(resp.status)
      end
    end
  end
  # rubocop:enable Metrics/BlockLength

  desc 'Delete jobs'
  long_desc %(
    Delete all jobs, choose between either all waiting jobs, or all jobs in the database.
  )
  arg_name '<waiting|all>'
  command(:delete) do |cmd|
    cmd.action do |glob, _options, args|
      exit_now!('Wrong number of arguments, needs: <waiting|all>') unless args.length == 1

      resp = send_request(glob, "/delete/#{args[0]}", nil)
      exit_now!('Invalid argument, needs either "waiting" or "all"') unless HTTP::Status.successful?(resp.status)

      puts 'The following jobs were deleted:'
      puts resp.content
    end
  end

  def self.send_request(glob, path, data)
    host, port = get_address(glob)
    uri = URI::HTTP.build(host: host, port: port, path: path)
    client = HTTPClient.new
    client.receive_timeout = 5
    client.default_header = { 'Content-Type' => 'application/yaml' }
    client.post(uri, body: data.to_yaml)
  rescue StandardError => e
    exit_now!(e)
  end

  # :reek:NilCheck
  def self.get_address(glob)
    host = ENV['ARTIFACTOMAT_CONF_CONF_APE_JOB_CLERK_ADDRESS']
    host = glob[:host] unless glob[:host].nil?
    port = ENV['ARTIFACTOMAT_CONF_CONF_APE_JOB_CLERK_PORT'].to_i
    port = glob[:port] unless glob[:port].nil?
    [host, port]
  end
end
