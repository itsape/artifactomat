# frozen_string_literal: true

require 'sinatra/base'
require 'yaml'
require 'artifactomat'
require 'artifactomat/models/job'
require 'artifactomat/utils/bullet_loader'

# The JobClerk creates jobs sent from the JobTool
class JobClerk < Sinatra::Base
  set :sessions, true

  configure_bullet_for_sinatra

  def initialize(options = {}, **_kwargs)
    super()
    @options = options
  end

  get '/', provides: ['html'] do
    ['<!DOCTYPE html>' \
    '<html><head><meta charset="UTF-8"><title>ITS.APE job builder server</title></head>' \
    '<body><h1>ITS.APE job clerk<h1>' \
    '<p>Hello!</body></p>']
  end

  get '/', provides: ['yaml'] do
    [200, "Hello, I\'m the ITS.APE job clerk!"]
  end

  post '/build/execute' do
    job = YAML.safe_load(request.body.read, [Symbol, Time])
    return [406, 'Mismatching job action'] unless job[:action] == 'execute'

    Job.create!(job)
    [201]
  rescue StandardError => e
    [500, e.message]
  end

  post '/build/copy' do
    job = YAML.safe_load(request.body.read, [Symbol, Time])
    return [406, 'Mismatching job action'] unless job[:action] == 'copy'

    Job.create!(job)
    [201]
  rescue StandardError => e
    [500, e.message]
  end

  post '/build/remove' do
    job = YAML.safe_load(request.body.read, [Symbol, Time])
    return [406, 'Mismatching job action'] unless job[:action] == 'remove'

    return [403, "There is no job to get the file '#{job[:deploy_path]}' to user '#{job[:user]}'"] unless copy_job?(job)

    Job.create!(job)
    [201]
  rescue StandardError => e
    [500, e.message]
  end

  post '/delete/waiting' do
    jobs = Job.where('jobs.status = 0')
    resp = +''
    jobs.each do |job|
      resp << job.to_str << "\n"
    end
    jobs.destroy_all
    [200, resp]
  end

  post '/delete/all' do
    resp = +''
    Job.find_each do |job|
      resp << job.to_str << "\n"
    end
    Job.destroy_all
    [200, resp]
  end

  private

  # :reek:NilCheck
  def copy_job?(job)
    copy_job = Job.where(user: job[:user],
                         action: Job.actions[:copy],
                         deploy_path: job[:deploy_path]).last
    return false if copy_job.nil?

    if copy_job.waiting?
      copy_job.completed!
      copy_job.save!
    end
    true
  end
end
