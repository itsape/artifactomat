# frozen_string_literal: true

require 'artifactomat/models/infrastructure_element'
require 'artifactomat/models/recipe'
require 'artifactomat/models/routing_element'
require 'artifactomat/models/subject'
require 'artifactomat/models/test_episode'
require 'artifactomat/models/test_program'
require 'artifactomat/models/test_season'
require 'artifactomat/models/test_series'
require 'artifactomat/models/track_entry'
require 'artifactomat/models/track_filter'
require 'artifactomat/models/subject_history'
require 'artifactomat/models/job'
