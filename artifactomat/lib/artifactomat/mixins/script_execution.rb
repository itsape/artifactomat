# frozen_string_literal: true

require 'open3'
require 'openssl'

class ScriptExecutionError < StandardError
end

class InvalidCommandError < ScriptExecutionError
end

class CommandExecutionError < ScriptExecutionError
end

class TokenError < StandardError
end

class EnvConversionError < StandardError
end

# This is a helper module to execute scripts, compute tmp folder and more.
module ScriptExecution
  # Should the exposed environment variables change, then the recipe documentation @
  # dist/etc/artifactomat/recipes/readme.md should reflect these changes!
  APE_TMP_DIR = 'conf.ape.tmp_dir'
  ARTIFACTOMAT_PREFIX = 'ARTIFACTOMAT_'
  EPISODE_PREFIX = ARTIFACTOMAT_PREFIX + 'TESTEPISODE_'
  RECIPE_ENV_PREFIX = ARTIFACTOMAT_PREFIX + 'RECIPE_'
  IE_ENV_PREFIX = RECIPE_ENV_PREFIX + 'IE_'
  RECIPE_ID = RECIPE_ENV_PREFIX + 'ID'
  RECIPE_FOLDER = RECIPE_ENV_PREFIX + 'FOLDER'
  RECIPE_PATH = RECIPE_ENV_PREFIX + 'PATH'
  RECIPE_TOKEN = RECIPE_ENV_PREFIX + 'TOKEN'
  RECIPE_TEMP_FOLDER = RECIPE_ENV_PREFIX + 'TEMP_DIR'
  SUBJECT_ENV_PREFIX = ARTIFACTOMAT_PREFIX + 'SUBJECT_'
  SUBJECT_TOKEN = SUBJECT_ENV_PREFIX + 'TOKEN'
  EPISODE_ID = RECIPE_ENV_PREFIX + 'TESTEPISODE_ID'
  SEASON_ID = RECIPE_ENV_PREFIX + 'TESTSEASON_ID'
  ROUTING_PORT_PREFIX = ARTIFACTOMAT_PREFIX + 'ROUTING_PORT_'
  ROUTING_EXT_ADDRESS = ARTIFACTOMAT_PREFIX + 'ROUTING_EXT_ADDR'

  # Executes the script 'script'.
  # Returns a status code, stdout and stderr
  # :reek:NilCheck
  # rubocop:disable Metrics/AbcSize
  def execute_script(script, env = {})
    raise(InvalidCommandError, 'Script nil') if script.nil? || script.empty?

    _, stdout_stderr, wait_thr = Open3.popen2e(env, script)
    [/\d+$/.match(wait_thr.value.to_s)[0].to_i, stdout_stderr.read]
  rescue SystemCallError => e
    raise(InvalidCommandError, "Executing #{script} yield: #{e}")
  rescue Timeout::Error # make shit killable
    Process.kill('KILL', wait_thr.pid)
    raise
  end
  # rubocop:enable Metrics/AbcSize

  # :reek:ManualDispatch
  def recipe_tmp_folder(tseason)
    raise(ScriptExecutionError, "#{tseason.inspect} has no id!") \
      unless tseason.respond_to?(:id)

    File.join(@configuration_manager.get(APE_TMP_DIR), tseason.id.to_s)
  end

  # :reek:ManualDispatch
  def token_from_object(obj)
    raise(TokenError, "Object has no attribute id: #{obj}") unless obj.respond_to?(:id)

    id = obj.id
    id = id.to_s unless id.is_a?(String)
    md5 = OpenSSL::Digest::MD5.new
    md5 << id
    md5.hexdigest.to_s
  end

  # Converts a subject to an environment hash.
  # :reek:NilCheck
  def subject_to_env(subject)
    raise(EnvConversionError, "Argument is not a Subject: #{subject}") unless subject.is_a?(Subject)

    subject_hash = subject.to_hash
    env = { SUBJECT_TOKEN => token_from_object(subject) }
    subject_hash.each do |k, v|
      v = 'nil' if v.nil?
      env[SUBJECT_ENV_PREFIX + k.upcase] = v
    end

    env
  end

  # Converts recipe attributes from a TestSeries
  # to an environment hash.
  def season_to_env(season)
    raise(EnvConversionError, "Argument is not a TestSeason: #{season}") \
      unless season.is_a?(TestSeason)

    recipe = @recipe_manager.get_recipe(season.recipe_id)
    env = { RECIPE_TOKEN => token_from_object(recipe),
            SEASON_ID => season.id.to_s }

    add_parameters(env, RECIPE_ENV_PREFIX, season.recipe_parameters)
    add_ie_parameters(env, recipe)
    add_season_parameters(env, season)
    add_routing_information(env, season)

    env
  end

  def add_ie_parameters(env, recipe)
    recipe.infrastructure.each do |ie|
      add_parameters(env, IE_ENV_PREFIX, ie.parameters)
    end
  end

  def add_season_parameters(env, season)
    season_recipe_id = season.recipe_id
    env[RECIPE_TEMP_FOLDER] = recipe_tmp_folder(season)
    env[RECIPE_PATH] = File.join(@configuration_manager.cfg_path,
                                 'recipes', season_recipe_id)
    env[RECIPE_FOLDER] = File.join(@configuration_manager.cfg_path,
                                   'recipes', season_recipe_id)
    env[RECIPE_ID] = season_recipe_id
  end

  def add_routing_information(env, season)
    routing = @routing || Ape.instance.routing
    ports = routing.get_internal_ports season
    (1..ports.size).each do |i|
      env["#{ROUTING_PORT_PREFIX}#{i}"] = ports[i - 1].to_s
    end
    env[ROUTING_EXT_ADDRESS] = routing.get_external_address season
  end

  # :reek:NilCheck
  def add_parameters(env, prefix, parameters)
    return nil if parameters.nil?

    parameters.each do |k, v|
      v = 'nil' if v.nil?
      env[prefix + k.to_s.upcase] = v.to_s
    end
  end

  # Converts subject and parameters from a TestEpisode
  # to an environment hash.
  def episode_to_env(episode)
    raise(EnvConversionError, "Argument is not a TestEpisode: #{episode}") \
      unless episode.is_a?(TestEpisode)

    env = extract_episode_parameters(episode)
    subject = @subject_manager.get_subject(episode.subject_id)
    env.merge!(subject_to_env(subject))

    env
  end

  def extract_episode_parameters(episode)
    env = { EPISODE_ID => episode.id.to_s }

    env[EPISODE_PREFIX + 'SCHEDULE_START'] = episode.schedule_start.to_s
    env[EPISODE_PREFIX + 'SCHEDULE_END'] = episode.schedule_end.to_s
    env
  end

  def pp_error(stdout, stderr)
    error = +"============ STDOUT ==============\n"
    error << stdout << "\n"
    error << "============ STDERR ==============\n"
    error << stderr << "\n"
  end
end
