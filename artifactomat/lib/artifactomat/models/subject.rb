# frozen_string_literal: true

require 'json'

# This class models a subject for the its.ape framework.
# # @param [String] id is created by combining
#   groupfile name and id from within
#   this groupfile
# @param <all others> are provided from
#   the specific .csv groupfile
class Subject
  attr_accessor :id
  attr_accessor :name
  attr_accessor :surname
  attr_accessor :userid
  attr_accessor :email

  def initialize(id, name, surname, userid, email)
    @name = name
    @id = id
    @surname = surname
    @userid = userid
    @email = email
  end

  def add_field(name)
    self.class.send(:attr_accessor, name)
  end

  def to_hash
    hash = {}
    instance_variables.each do |var|
      hash[var.to_s.delete('@')] = instance_variable_get(var)
    end
    hash
  end

  def self.from_hash(hash)
    s = Subject.new(hash['id'], hash['name'], hash['surname'], hash['userid'], hash['email'])
    hash.each do |k, v|
      s.add_field(k)
      s.instance_variable_set("@#{k}".to_sym, v)
    end
    s
  end

  def ==(other)
    other.class == self.class && other.state == state
  end

  def state
    instance_variables.map { |var| instance_variable_get var }
  end

  def match?(description)
    description.each do |attribute, value|
      return true if send(attribute) == value
    rescue NoMethodError
      next
    end
    false
  end

  def self.from_json(json)
    from_hash(JSON.parse(json))
  end

  def to_json(*_args)
    JSON.generate(to_hash)
  end
end
