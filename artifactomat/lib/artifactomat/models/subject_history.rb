# frozen_string_literal: true

require 'artifactomat/models/test_series'
require 'active_record'

# This class models the session history of a subject and its ip addresses.
class SubjectHistory < ActiveRecord::Base
  belongs_to :test_series

  validates :subject_id, presence: true
  validates :ip, presence: true
  validates :session_begin, presence: true
  validates :submit_time, presence: true
end
