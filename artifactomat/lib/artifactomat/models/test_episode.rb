# frozen_string_literal: true

require 'artifactomat/models/test_season'
require 'artifactomat/models/recipe'
require 'artifactomat/models/subject_history'
require 'active_record'
require 'active_model'

require 'yaml'

# checks dates for the test episode
class EpisodeScheduleValidator < ActiveModel::Validator
  # :reek:NilCheck
  def validate(record)
    record_schedule_start = record.schedule_start
    record_schedule_end = record.schedule_end
    record_errors = record.errors
    return if record_schedule_start.nil?

    if !record_schedule_end.nil?
      record_errors[:schedule_start] << 'schedule_start not before _end' unless record_schedule_start < record_schedule_end
    else
      record_errors[:schedule_start] << 'schedule_start set, so _end has to be set'
    end
  end
end

# This class models a test episode as defined in the its.ape framework.
class TestEpisode < ActiveRecord::Base
  include ActiveModel::Validations
  validates_with EpisodeScheduleValidator

  belongs_to :test_season

  validates :subject_id, presence: true

  enum status: %i[prepared running completed failed]

  def to_str
    "(#{id}) for #{subject_id}"
  end

  def seconds_exposed
    return 0 unless running? || completed?
    return 0 unless schedule_start # existing schedule_start implies schedule_end

    total_exposure = 0
    test_series_id = test_season.test_series.id
    SubjectHistory.where(subject_id: subject_id, test_series_id: test_series_id).each do |session|
      total_exposure += session_exposure(session)
    end
    total_exposure.round
  end

  private

  # :reek:NilCheck
  def session_exposure(session)
    return 0 unless session.session_begin < schedule_end

    effective_session_end = session.session_end.nil? ? Time.now : session.session_end
    overlap_end = [schedule_end, effective_session_end].min
    overlap_start = [session.session_begin, schedule_start].max

    overlap_end - overlap_start
  end
end
