# frozen_string_literal: true

require 'json'

# A primitive data type that is backed by volatilestore to survive restarts.
class VolatileStoredPrimitive
  # :reek:NilCheck
  def initialize(class_name, variable_name, volatile_store, default_value, options = {})
    @debug_reread_from_redis = options[:debug_reread_from_redis]
    @debug_ignore_redis = options[:debug_ignore_redis]

    # rubocop:disable Style/IfUnlessModifier
    if @debug_reread_from_redis && @debug_ignore_redis
      raise 'configuration error; you cannot ignore redis but reread from it'
    end

    # rubocop:enable Style/IfUnlessModifier
    @varname = "#{class_name}__#{variable_name}"
    @vstore = volatile_store
    @mut = Mutex.new
    @value = from_redis unless @debug_ignore_redis
    @value = default_value if @value.nil?
  end

  def get
    @mut.synchronize do
      @value = from_redis if @debug_reread_from_redis
      @value
    end
  end

  def set(value)
    @mut.synchronize do
      @value = value
      to_redis(value) unless @debug_ignore_redis
    end
    nil
  end

  private

  def from_redis
    json = nil
    @vstore.with_state_saving_connection do |conn|
      json = conn.get @varname
    end
    json && JSON.parse(json)
  end

  def to_redis(value)
    json = JSON.generate(value)
    @vstore.with_state_saving_connection do |conn|
      conn.set @varname, json
    end
  end
end
