# frozen_string_literal: true

require 'json'

# VolatileStoredHash is a Hash backed by volatile-store. It is a bit
# limited but the only known use is in LongTermMemory. Keys are
# Integers and Values are Sets of Integers
# :reek:RepeatedConditional
class VolatileStoredHash
  def initialize(class_name, variable_name, volatile_store, options = {})
    @debug_reread_from_redis = options[:debug_reread_from_redis]
    @debug_ignore_redis = options[:debug_ignore_redis]

    # rubocop:disable Style/IfUnlessModifier
    if @debug_reread_from_redis && @debug_ignore_redis
      raise 'configuration error; you cannot ignore redis but reread from it'
    end

    # rubocop:enable Style/IfUnlessModifier
    @varname = "#{class_name}__#{variable_name}"
    @vstore = volatile_store
    @mut = Mutex.new
    @hash = @debug_ignore_redis ? {} : from_redis
  end

  def key?(key)
    @mut.synchronize do
      @hash = from_redis if @debug_reread_from_redis
      @hash.key?(key)
    end
  end

  def add(key, value)
    @mut.synchronize do
      @hash = from_redis if @debug_reread_from_redis
      result = if @hash.key?(key)
                 @hash[key].add(value)
               else
                 @hash[key] = Set.new([value])
               end
      to_redis(@hash)
      result
    end
  end

  def [](key)
    @mut.synchronize do
      @hash = from_redis if @debug_reread_from_redis
      @hash[key]
    end
  end

  # CLAIM: Removes value from all sets (values) of the hash and
  # returns a list of corresponding keys.
  def delete_val(value)
    @mut.synchronize do
      @hash = from_redis if @debug_reread_from_redis
      keys = []
      @hash.each do |k, v|
        keys << k if v.delete?(value)
      end
      @hash.filter! { |_, v| !v.empty? }
      to_redis(@hash)
      keys
    end
  end

  def count
    @mut.synchronize do
      @hash = from_redis if @debug_reread_from_redis
      @hash.count
    end
  end

  def first
    @mut.synchronize do
      @hash = from_redis if @debug_reread_from_redis
      @hash.first
    end
  end

  def values
    @mut.synchronize do
      @hash = from_redis if @debug_reread_from_redis
      @hash.values
    end
  end

  private

  def from_redis
    json = nil
    @vstore.with_state_saving_connection do |conn|
      json = conn.get @varname
    end
    return {} unless json

    JSON.parse(json).map { |k, v| [k, Set.new(v)] }.to_h
  end

  def to_redis(arg)
    arrayed_version = arg.map { |k, v| [k, v.to_a] }.to_h
    json = JSON.generate(arrayed_version)
    @vstore.with_state_saving_connection do |conn|
      conn.set @varname, json
    end
  end
end
