# frozen_string_literal: true

require 'artifactomat/models/test_series'
require 'artifactomat/models/test_episode'
require 'artifactomat/modules/configuration_manager'
require 'artifactomat/modules/recipe_manager'

require 'active_record'
require 'active_support/hash_with_indifferent_access'
require 'yaml'
require 'json'

# This class models a test season as defined in the its.ape framework.
class TestSeason < ActiveRecord::Base
  @configuration_manager = ConfigurationManager.new
  @recipe_manager = RecipeManager.new

  class << self
    attr_reader :configuration_manager
    attr_reader :recipe_manager
  end

  belongs_to :test_series
  has_many :test_episodes, dependent: :destroy

  validates :label, presence: true
  validates :recipe_id, presence: true
  validates :duration, numericality: { greater_than_or_equal_to: 1 }

  def duration=(value)
    super(value.to_i)
  end

  # needed because we have a setter
  def duration
    super
  end

  def init
    self.recipe_parameters ||= YAML.dump('')
  end

  def recipe_parameters
    YAML.safe_load(read_attribute(:recipe_parameters), [Symbol, ActiveSupport::HashWithIndifferentAccess])
  end

  def recipe_parameters=(parameters)
    write_attribute(:recipe_parameters, YAML.dump(parameters))
  end

  def json_detection_parameters
    recipe = self.class.recipe_manager.get_recipe(recipe_id)
    recipe_detection_parameters = recipe.detection_parameters
    return '{}' if recipe_detection_parameters.empty?

    recipe_detection_parameters['season_id'] = id
    recipe_detection_parameters['screenshots_hash'] = screenshots_hash
    recipe_detection_parameters.to_json
  end

  def absolute_screenshots_folder
    raise "Season #{id} has no screenshots" if screenshots_folder == ''

    File.join(self.class.configuration_manager.cfg_path, 'recipes', recipe_id, screenshots_folder)
  end

  # :reek:NilCheck
  def screenshots_filenames
    Dir.entries(absolute_screenshots_folder).select do |filename|
      File.file?(File.join(absolute_screenshots_folder, filename))
    end
  end

  def screenshots_hash
    hash = Digest::MD5.new
    if screenshots_folder != ''
      absolute_screenshots_folder = File.join(self.class.configuration_manager.cfg_path,
                                              'recipes',
                                              recipe_id,
                                              screenshots_folder)
      Dir.foreach(absolute_screenshots_folder) do |element|
        next if File.directory?(element)

        file_path = File.join(absolute_screenshots_folder, element)
        hash.file(file_path)
      end
    end
    hash.hexdigest
  end

  def to_str
    "\"#{label}\" (#{id}): recipe: '#{recipe_id}', duration: '#{duration}', episodes: '#{test_episodes.count}', TestSeries: #{test_series.to_str}"
  end
end
