# frozen_string_literal: true

require 'digest'
require 'active_support'
require 'active_support/core_ext/object/blank'

require 'artifactomat/models/infrastructure_element'
require 'artifactomat/models/track_filter'

# This class models recipe as defined within the its.apt framework.
# :reek:InstanceVariableAssumption | we load instance variables from YAML that reek doesn't know
class Recipe
  # To provide scripts for a recipe
  class RecipeScripts
    attr_accessor :artifact_script, :arm_script, :disarm_script, :deploy_script, :undeploy_script

    def initialize(artifact_script, arm_script, disarm_script, deploy_script, undeploy_script)
      @artifact_script = artifact_script
      @arm_script = arm_script
      @disarm_script = disarm_script
      @deploy_script = deploy_script
      @undeploy_script = undeploy_script
    end
  end

  attr_accessor :duration, :arm_script, :disarm_script,
                :artifact_script, :infrastructure, :parameters,
                :trackfilters, :deploy_script, :default_parameters,
                :detection
  attr_reader   :id

  def initialize(id, scripts, infrastructure = [], parameters = {}, default_parameters = {})
    @trackfilters = []
    @id = id
    adjust_trackfilter_recipe_id(id)
    @artifact_script = scripts.artifact_script
    @infrastructure = infrastructure
    @parameters = parameters
    @default_parameters = default_parameters
    @arm_script = scripts.arm_script
    @disarm_script = scripts.disarm_script
    @deploy_script = scripts.deploy_script
    @undeploy_script = scripts.undeploy_script
  end

  def errors
    e = []
    e << 'has no id/name' if @id.blank?
    e.concat errors_for_duration
    e.concat errors_for_infrastructure
    e.concat errors_for_trackfilters
    e.concat errors_for_scripts
    e.concat errors_for_parameters
    e.concat errors_for_detection if defined? @detection
    e
  end

  def name
    @id
  end

  def name=(name)
    @id = name
    adjust_trackfilter_recipe_id(name)
  end

  def id=(id)
    @id = id
    adjust_trackfilter_recipe_id(id)
  end

  def describe_parameters
    parameters = {}
    @parameters.each do |k, v|
      parameters[k] = { 'description' => v, 'default' => @default_parameters[k] }
    end
    parameters
  end

  def detection_parameters
    return {} unless defined?(@detection)

    params = Marshal.load(Marshal.dump(@detection)) # create deep copy of detection
    params['artifact_name'] = @id
    detector_map = {}
    params['detectors'].each_with_index do |detector_values, detector_pos|
      detector_map[detector_pos.to_s] =
        "#{detector_values['detector_class_name']};#{detector_values['pre_conditions']};#{detector_values['target_conditions']}"
    end
    params['detectors'] = detector_map
    params['runtime_information']['artifact_name'] = @id
    params
  end

  def self.load(filename)
    yaml = File.read(filename)
    candidate = YAML.safe_load(yaml, [Recipe, TrackFilter, Regexp, InfrastructureElement])
    raise "Could not load recipe: #{filename}" unless candidate.is_a? Recipe

    candidate.id = File.basename(filename, File.extname(filename))
    candidate
  end

  private

  def errors_for_parameters
    e = []
    shared_keys = @parameters.keys & @default_parameters.keys
    (@default_parameters.keys - shared_keys).each do |key|
      e << "has default parameter #{key} but no description"
    end
    e
  end

  def adjust_trackfilter_recipe_id(id)
    @trackfilters.each do |tf|
      tf.recipe_id = id
    end
  end

  def errors_for_scripts
    e = []
    e << 'has no artifact script' if @artifact_script.blank?
    e << 'has no arm script' if @arm_script.blank?
    e << 'has no disarm script' if @disarm_script.blank?
    e << 'has no deploy script' if @deploy_script.blank?
    e << 'has no undeploy script' if @undeploy_script.blank?
    e
  end

  def errors_for_duration
    return ['has no duration'] if @duration.blank?
    return ['duration not numeric'] unless @duration.is_a? Numeric
    return ['duration is not >1'] if @duration < 2

    []
  end

  def errors_for_infrastructure
    return ['has no array of infrastructures'] unless @infrastructure.is_a? Array

    e = []
    @infrastructure.each do |ie|
      e.concat validate_infrastructure(ie)
    end
    e
  end

  def validate_infrastructure(inf_element)
    return ['has a infrastructure which is no InfrastructureElement'] unless inf_element.is_a? InfrastructureElement

    e = []
    inf_element.errors.each do |ei_e|
      e << "has a infrastructure which #{ei_e}"
    end
    e
  end

  def errors_for_trackfilters
    return ['has no array of track filters'] unless @trackfilters.is_a? Array
    return ['has no track filters'] if @trackfilters.count < 1

    e = []
    @trackfilters.each do |tf|
      e.concat validate_trackfilter(tf)
    end
    e
  end

  def validate_trackfilter(track_filter)
    return ['has a track filter which is no TrackFilter'] unless track_filter.is_a? TrackFilter

    e = []
    e << 'has no recipe.id set' if track_filter.recipe_id.blank?
    track_filter.errors.each do |tf_e|
      e << "has a track filter which #{tf_e}"
    end
    e
  end

  def errors_for_detection
    return ["detection entry '#{@detection}' is not a hash"] unless @detection.is_a? Hash

    errors = errors_for_detection_keys
    return errors unless errors.empty?

    return ["detection->detectors '#{@detection['detectors']}' is not an array"] unless @detection['detectors'].is_a?(Array)
    return ["detection->detectors '#{@detection['detectors']}' is empty"] if @detection['detectors'].empty?

    # validate detectors
    @detection['detectors'].each_with_index do |detector, index|
      errors.concat(validate_detector(index, detector))
    end
    errors
  end

  def errors_for_detection_keys
    errors = []
    %w[detection_interval detectors runtime_information].each do |key|
      errors << "misses #{key} key in detection hash" unless @detection.key?(key)
    end
    errors
  end

  def validate_detector(index, detector)
    return ["has a detector '#{detector}' at index #{index} which is no hash"] unless detector.is_a? Hash

    errors = []
    %w[detector_class_name pre_conditions target_conditions].each do |key|
      errors << "misses #{key} key in detector with index #{index}" unless detector.key?(key)
    end

    errors
  end
end
