# frozen_string_literal: true

require 'date'
require 'active_record'
require 'artifactomat/models/test_episode'

# This class represents a detection event for the artifact detection
class DetectionEvent < ActiveRecord::Base
  belongs_to :test_episode

  validates :timestamp, presence: true

  enum artifact_present: %i[impossible possible certain]

  def self.from_detection_timetable_row(row, episode_id)
    presence_map = %i[impossible possible certain]
    timestamp = DateTime.strptime(row[0], '%y%m%d%H%M%S%L')
    artifact_presence = presence_map[row[1].to_i]
    DetectionEvent.create(
      timestamp: timestamp,
      artifact_present: artifact_presence,
      test_episode_id: episode_id
    )
  end

  def to_s
    "DetectionEvent timestamp: #{timestamp}, artifact_present: #{artifact_present}, episode_id: #{test_episode_id}"
  end
end
