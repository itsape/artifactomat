# frozen_string_literal: true

require 'active_support'
require 'active_support/core_ext/object/blank'

# This class models a track filter as defined in the its.ape framework.
class TrackFilter
  attr_accessor :course_of_action, :extraction_pattern,
                :pattern, :recipe_id, :score

  IDS = %w[subject userid email ip phone].freeze

  def initialize
    @course_of_action = ''
    @extraction_pattern = []
    @pattern = //
  end

  def errors
    e = []
    e << 'has no course of action' if @course_of_action.blank?
    e.concat extraction_pattern_errors
    e.concat pattern_errors
    e
  end

  def ==(other)
    other.class == self.class && other.state == state
  end

  def state
    instance_variables.map do |var|
      next if var == :@recipe_id

      instance_variable_get var
    end
  end

  private

  def extraction_pattern_errors
    e = []
    return ['has no extraction pattern'] if @extraction_pattern.blank?
    return ['extraction pattern is no array'] unless @extraction_pattern.is_a? Array

    labels = @extraction_pattern.map { |ep| Regexp.new(ep).names }.flatten
    e << 'extraction pattern does not parse the timestamp' unless labels.include? 'timestamp'
    e << 'extraction pattern does not parse an subject identifier' if (labels & IDS).empty?
    e
  end

  def pattern_errors
    return ['has no pattern'] if @pattern.blank?
    return ['pattern is not regexp'] unless @pattern.is_a? Regexp

    []
  end
end
