# frozen_string_literal: true

# This class models an routing element generated, monitored and operated
# from the RoutingModule of the its.ape framework.
class RoutingElement
  attr_accessor :series_id, :season_id,
                :external_ip, :internal_ip, :external_ports,
                :internal_ports

  def initialize(series_id, season_id)
    @series_id = series_id
    @season_id = season_id
    @external_ip = nil
    @internal_ip = nil
    @external_ports = nil
    @internal_ports = nil
  end

  def ==(other)
    @series_id == other.series_id && @season_id == other.season_id && @external_ip == other.external_ip &&
      @internal_ip == other.internal_ip && @external_ports == other.external_ports &&
      @internal_ports == other.internal_ports
  end
end
