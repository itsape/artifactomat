# frozen_string_literal: true

require 'json'

# Class for a SubjectTracker plugin action
# :reek:InstanceVariableAssumption
class Action
  attr_accessor :userid, :ip, :submit_time, :session_start, :subject_id
  attr_reader :session_end

  class << self
    def from_hash(hash)
      action = Action.new(hash[:userid], hash[:ip], hash[:submit_time])
      action.instance_variable_set :@session_end, hash[:session_end]

      h_session_start = hash[:session_start]
      action.session_start = h_session_start if h_session_start
      h_subject_id = hash[:subject_id]
      action.subject_id = h_subject_id if h_subject_id
      h_end_session = hash[:end_session]
      action.instance_variable_set :@end_session, h_end_session if h_end_session

      action
    end

    def from_json(json)
      j = JSON.parse(json)
      hash = {
        userid: j['userid'],
        ip: j['ip'],
        submit_time: nil_or_parsed_time(j['submit_time']),
        session_start: nil_or_parsed_time(j['session_start']),
        subject_id: j['subject_id'],
        session_end: nil_or_parsed_time(j['session_end']),
        end_session: j['end_session']
      }

      Action.from_hash(hash)
    end

    private

    def nil_or_parsed_time(nil_or_string)
      nil_or_string && Time.parse(nil_or_string)
    end
  end

  def initialize(userid, ip, submit_time)
    @userid = userid
    @ip = ip
    @submit_time = submit_time
    @end_session = false
    @session_end = nil
  end

  def to_s
    "<Action: userid=#{@userid} subject_id=#{@subject_id || 'N/A'} ip=#{@ip} submit_time=#{@submit_time} session_start=#{@session_start || 'N/A'} session_end=#{@session_end || 'N/A'}>"
  end

  def session_end=(value)
    @end_session = true
    @session_end = value
  end

  def end_session?
    @end_session
  end

  def ==(other)
    other.class == self.class && other.state == state
  end

  def to_json(*_args)
    h = { userid: @userid,
          ip: @ip,
          submit_time: @submit_time,
          session_start: session_start,
          subject_id: subject_id,
          session_end: @session_end,
          end_session: @end_session }
    JSON.generate h
  end

  def state
    instance_variables.map { |var| instance_variable_get var }
  end
end
