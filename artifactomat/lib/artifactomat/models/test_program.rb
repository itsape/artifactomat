# frozen_string_literal: true

require 'active_record'
require 'artifactomat/models/test_series'

# This class models a test program as defined in the its.ape framework.
class TestProgram < ActiveRecord::Base
  has_many :test_series, dependent: :destroy
  validates :label, presence: true

  def to_str
    "\"#{label}\" (#{id})"
  end
end
