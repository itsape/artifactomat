# frozen_string_literal: true

require 'json'

# VolatileStoredSet is a Set backed by volatile-store. It is a bit
# limited but the only known use is in SubjectTracker.
# :reek:RepeatedConditional
class VolatileStoredSet
  def initialize(class_name, variable_name, volatile_store, klass, options = {})
    @debug_reread_from_redis = options[:debug_reread_from_redis]
    @debug_ignore_redis = options[:debug_ignore_redis]

    # rubocop:disable Style/IfUnlessModifier
    if @debug_reread_from_redis && @debug_ignore_redis
      raise 'configuration error; you cannot ignore redis but reread from it'
    end

    # rubocop:enable Style/IfUnlessModifier
    @varname = "#{class_name}__#{variable_name}"
    @vstore = volatile_store
    @klass = klass
    @mut = Mutex.new
    @set = @debug_ignore_redis ? Set.new : all_from_redis
  end

  def count
    @mut.synchronize do
      @set = all_from_redis if @debug_reread_from_redis
      @set.count
    end
  end

  def <<(element)
    @mut.synchronize do
      @set = all_from_redis if @debug_reread_from_redis
      @set << element
      redis_add(element) unless @debug_ignore_redis
    end
  end

  def include?(element)
    @mut.synchronize do
      @set = all_from_redis if @debug_reread_from_redis
      @set.include? element
    end
  end

  def delete(element)
    @mut.synchronize do
      @set = all_from_redis if @debug_reread_from_redis
      @set.delete element
      redis_delete(element) unless @debug_ignore_redis
    end
  end

  def any?(&block)
    @mut.synchronize do
      @set = all_from_redis if @debug_reread_from_redis
      @set.any?(&block)
    end
  end

  def select(&block)
    @mut.synchronize do
      @set = all_from_redis if @debug_reread_from_redis
      @set.to_a.select(&block)
    end
  end

  def map(&block)
    @mut.synchronize do
      @set = all_from_redis if @debug_reread_from_redis
      @set.map(&block)
    end
  end

  def each(&block)
    @mut.synchronize do
      @set = all_from_redis if @debug_reread_from_redis
      @set.each(&block)
    end
  end

  private

  def all_from_redis
    jsons = nil
    @vstore.with_state_saving_connection do |conn|
      jsons = conn.smembers @varname
    end
    jsons.map { |json| @klass.from_json(json) }.to_set
  end

  def redis_add(element)
    @vstore.with_state_saving_connection do |conn|
      conn.sadd(@varname, element.to_json)
    end
  end

  def redis_delete(element)
    @vstore.with_state_saving_connection do |conn|
      conn.srem(@varname, element.to_json)
    end
  end
end
