# frozen_string_literal: true

require 'active_record'
require 'json'

# This class models a job for the jop spooler
class Job < ActiveRecord::Base
  validates :user, length: { in: 0..255 }, allow_blank: false,
                   allow_nil: false, presence: true
  validates :file_path,     length: { in: 0..255 },
                            presence: true,
                            allow_blank: true,
                            allow_nil: false
  validates :deploy_path,     length: { in: 0..255 },
                              presence: true,
                              allow_blank: true,
                              allow_nil: false
  validates :creation_date, presence: true
  validates :watch_arguments, presence: true, allow_blank: false, allow_nil: true

  enum status: %i[waiting completed failed]
  enum action: %i[copy execute remove start_detection stop_detection]

  # rubocop:disable Metrics/AbcSize
  # :reek:NilCheck
  def to_json(*_args)
    {
      id: id,
      user: user,
      status: status,
      action: action,
      file_path: file_path,
      deploy_path: deploy_path,
      watch_arguments: if watch_arguments.nil?
                         watch_arguments
                       else
                         JSON.parse(watch_arguments)
                       end,
      creation_date: creation_date,
      collection_date: collection_date,
      expiration_date: expiration_date
    }.to_json
  end
  # rubocop:enable Metrics/AbcSize

  def to_str
    result = "id: #{id}, " \
      "user: #{user}, " \
      "status: #{status}, " \
      "action: #{action}, " \
      "file_path: #{file_path}, " \
      "deploy_path: #{deploy_path}"\
      "watch_arguments: #{watch_arguments}" \
      "creation_date: #{creation_date}"\
      "collection_date: #{collection_date}"\
      "expiration_date: #{expiration_date}"
    result
  end

  # :reek:NilCheck
  def expired?
    return false if expiration_date.nil?
    return false if Time.now < expiration_date

    true
  end
end
