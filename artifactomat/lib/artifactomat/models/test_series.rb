# frozen_string_literal: true

require 'artifactomat/models/test_program'
require 'artifactomat/models/test_season'
require 'artifactomat/models/test_episode'

require 'active_record'
require 'biz'

# This class models a test series as defined in the its.ape framework.
class TestSeries < ActiveRecord::Base
  self.skip_time_zone_conversion_for_attributes = %i[start_date end_date]

  belongs_to :test_program
  has_many :test_seasons, dependent: :destroy
  has_many :test_episodes, through: :test_seasons

  enum status: %i[fresh scheduled preparing prepared approved completed]

  validates :label, presence: true
  validates :group_file, presence: true

  def to_str
    "\"#{label}\" (#{id})"
  end

  # :reek:NilCheck
  def start_date
    date = read_attribute(:start_date)
    date.change(usec: 0)
    date
  end

  def start_date=(date)
    raise 'TestSeries#start_date must be a Time object!' unless date.is_a? Time

    date.change(usec: 0)
    write_attribute(:start_date, date)
  end

  # :reek:NilCheck
  def end_date
    date = read_attribute(:end_date)
    date&.change(usec: 0)
    date
  end

  def end_date=(date)
    raise 'TestSeries#end_date must be a Time object!' unless date.is_a? Time

    date.change(usec: 0)
    write_attribute(:end_date, date)
  end

  def schedulable?
    %w[fresh scheduled].include? read_attribute('status')
  end

  def completed?
    update_status == 'completed'
  end

  def approved?
    update_status == 'approved'
  end

  def status
    update_status
  end

  def matches_id_or_label?(identifier)
    return true if identifier.to_i == read_attribute(:id) || identifier == read_attribute(:label)

    false
  end

  private

  def update_status
    return self[:status] unless self[:status] == 'approved'

    test_episodes.each do |ep|
      return self[:status] unless ep.completed? || ep.failed?
    end
    self[:status] = 'completed'
  end
end
