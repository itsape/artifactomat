# frozen_string_literal: true

require 'active_support'
require 'active_support/core_ext/object/blank'

# This class models an infrastructure element generated, monitored and operated
# from the its.apt framework.
class InfrastructureElement
  attr_accessor :monitor_script, :generate_script,
                :destroy_script, :arm_script, :disarm_script,
                :infrastructure_type, :parameters, :season_id, :ports,
                :names
  attr_reader :status

  DISARMED = 'disarmed'
  ARMED    = 'armed'

  def initialize
    @monitor_script = 'false'
    @generate_script = 'false'
    @destroy_script = 'false'
    @arm_script = 'false'
    @disarm_script = 'false'
    @infrastructure_type = ''
    @parameters = {}
    @season_id = nil
    @ports = []
    @names = []
  end

  def season
    require 'artifactomat/models/test_season'
    return nil unless @season_id

    TestSeason.find(@season_id)
  rescue ActiveRecord::RecordNotFound
    nil
  end

  def ==(other)
    self.class == other.class && state == other.state
  end

  def to_str
    if season
      "\"#{infrastructure_type}\" of season \"#{season.label}\" (#{season_id}) "
    else
      "\"#{infrastructure_type}\" of unknown origin"
    end
  end

  def errors
    e = []
    e << 'has no type' if @infrastructure_type.blank?
    e.concat port_errors
    e.concat script_errors
    e.concat name_errors
    e
  end

  def state
    instance_variables.map { |var| instance_variable_get var }
  end

  private

  def script_errors
    e = []
    e << 'has no monitor script' if @monitor_script.blank?
    e << 'has no generate script' if @generate_script.blank?
    e << 'has no destroy script' if @destroy_script.blank?
    e << 'has no arm script' if @arm_script.blank?
    e << 'has no disarm script' if @disarm_script.blank?
    e
  end

  def name_errors
    return ['names is not an array'] unless @names.is_a? Array

    e = []
    @names.each do |name|
      e << "has name #{name} which is not a string" unless name.is_a? String
      e << 'has a blank name' if name.blank?
    end
    e
  end

  def port_errors
    return ['has no array of ports'] unless @ports.is_a? Array

    e = []
    @ports.each do |port|
      e.concat validate_port(port)
    end
    e
  end

  def validate_port(port)
    return ["port #{port} is not Numeric"] unless port.is_a? Numeric
    return ["port #{port} is < 2"] if port.to_i < 2
    return ["port #{port}is > 65535"] if port.to_i > 65_535

    []
  end
end
