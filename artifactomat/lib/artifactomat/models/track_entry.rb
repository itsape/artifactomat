# frozen_string_literal: true

require 'artifactomat/models/test_episode'
require 'active_record'

# This class models a track entry as defined in the its.ape framework.
class TrackEntry < ActiveRecord::Base
  belongs_to :test_episode

  validates :test_episode, presence: true
  validates :score, presence: true
  validates :timestamp, presence: true
  validates :subject_id, presence: true
  validates :course_of_action, presence: true

  def to_hash
    hash = {
      test_episode: test_episode.id,
      subject_id: subject_id,
      score: score,
      course_of_action: course_of_action,
      timestamp: timestamp
    }

    hash
  end
end
