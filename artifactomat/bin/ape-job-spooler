#!/usr/bin/env ruby
# frozen_string_literal: true

ENV['ARTIFACTOMAT_ENV'] ||= 'production'
require 'artifactomat'

require 'logger'
require 'optparse'
require 'yaml'
require 'etc'
require 'webrick'
require 'webrick/https'
require 'fileutils'

require 'artifactomat/modules/configuration_manager'
require 'artifactomat/services/jobspooler'
require 'artifactomat/utils/systemd_helper'

# default values
options = {}
options[:logfile] = 'stdout'

OptionParser.new do |opts|
  opts.on('-c', '--config', 'Configuration file') do |c|
    options[:config] = c
  end

  opts.on('-l', '--log-file', 'Log file') do |l|
    options[:logfile] = l
  end
end

begin
  if options[:config].nil?
    cfg_mgr = ConfigurationManager.new
    options[:config] = cfg_mgr.cfg_path
  else
    cfg_mgr = ConfigurationManager.new options[:config]
  end
  config = {}
  config['ip'] = cfg_mgr.get 'conf.ape_job_spooler.ip'
  config['port'] = cfg_mgr.get 'conf.ape_job_spooler.port'
  config['tracking'] = cfg_mgr.get 'conf.ape_job_spooler.tracker.active'
  config['tracker_port'] = cfg_mgr.get 'conf.ape_job_spooler.tracker.port'
  config['tracker_ip'] = cfg_mgr.get 'conf.ape_job_spooler.tracker.ip'
  config['cert_path'] = File.join(options[:config], cfg_mgr.get('conf.ape_job_spooler.cert_path'))
  config['tc_port'] = cfg_mgr.get('trackcollector.port').to_i
rescue StandardError => e
  # loading default config
  puts "Cannot load config from #{options[:config]}: #{e.message}"
  puts e.backtrace
  exit 1
end

puts '=== CONFIGURATION ==='
puts config.to_yaml

config['logfile'] ||= STDOUT
logger = Logger.new(config['logfile'])
logger.level = Logger::FATAL

webrick_options = {
  Host: config['ip'],
  Port: config['port'],
  Logger: WEBrick::Log.new(config['logfile'], WEBrick::Log::DEBUG),
  SSLEnable: true,
  SSLVerifyClient: OpenSSL::SSL::VERIFY_PEER | OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT,
  SSLCertificate: OpenSSL::X509::Certificate.new(
    File.open(File.join(config['cert_path'], 'public/server.itsape.pem')).read
  ),
  SSLPrivateKey: OpenSSL::PKey::RSA.new(
    File.open(File.join(config['cert_path'], 'private/server.itsape.key')).read
  ),
  SSLCACertificateFile: File.join(config['cert_path'], 'public/ca.pem'),
  SSLCertName: [['CN', WEBrick::Utils.getservername]]
}

jobspooler = Jobspooler.new(config)

Systemd.ready

Rack::Handler::WEBrick.run(jobspooler, webrick_options)
