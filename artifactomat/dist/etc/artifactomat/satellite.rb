# frozen_string_literal: true

require 'optparse'
require 'socket'
require 'openssl'
require 'securerandom'

# Satellite class of ITSAPE Framework
class Satellite
  def initialize(config)
    @config = config
    @logfile = File.open(@config[:file], 'r')
    @last_logpos = 0
    @polling_rate = @config[:polling] || 5
    @batch_size = @config[:bsize] || 50
  end

  def build_connection(host = @config[:host], port = @config[:port])
    ssl_ctx = create_ssl_context
    tcp_socket = TCPSocket.new(host, port)
    ssl_socket = OpenSSL::SSL::SSLSocket.new(tcp_socket, ssl_ctx)
    connection = ssl_socket.connect
    connection
  rescue StandardError => e
    puts "[Satellite] Error on build_connection(#{host}, #{port}): #{e.message}"
  end

  def create_ssl_context
    # create ssl context
    ssl_ctx = OpenSSL::SSL::SSLContext.new

    # set client certificate and key
    ssl_ctx.cert = OpenSSL::X509::Certificate.new(File.open(@config[:cert]))
    ssl_ctx.key = OpenSSL::PKey::RSA.new(File.open(@config[:key]))

    # verify server certificate aswell
    ssl_ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER | OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT
    ssl_ctx.ca_file = @config[:ca]
    ssl_ctx
  end

  def watchlog
    loop do
      send_entries if @last_logpos != @logfile.size
      sleep @polling_rate
    end
  end

  # :reek:NilCheck
  def send_batch(entries)
    begin
      bytes_sent = 0
      conn = build_connection
      conn.puts entries.length.to_s
      entries.each do |entry|
        conn.puts entry
        bytes_sent += entry.bytesize
      end
      conn.gets # wait for the TC to acknowledge all lines
    rescue StandardError => e
      puts "[ERROR] Could not send log entry: #{e.message}"
    end
    conn&.sysclose
    bytes_sent
  end

  def send_entries
    until @logfile.eof?
      bytes_read = 0
      entries = []
      @logfile.each_line.take(@batch_size).each do |line|
        bytes_read += line.bytesize
        entries << line
      end

      bytes_sent = send_batch entries
      if bytes_sent != bytes_read
        @last_logpos += bytes_sent
        @logfile.pos = @last_logpos
        puts "[Satellite] ERROR: Could not send all log entries. Sent #{bytes_sent} / #{bytes_read} Bytes."
        return
      end
      @last_logpos += bytes_sent
    end
  end
end

# rubocop:disable Metrics/AbcSize, Metrics/BlockLength
# :reek:NilCheck
# :nocov:
def parse_args
  options = {}
  optparse = OptionParser.new do |opts|
    opts.banner = 'Usage: satellite.rb -f INPUT [--polling INTERVAL] [--batch BATCH_SIZE] --host HOST --port PORT --ca CA_CERT --pkey KEY --cert CERT'

    opts.on '-f FILE', '--file FILE', 'Input file' do |file|
      options[:file] = file
    end

    opts.on '--polling INTERVAL', Integer, 'Wait time between polls in seconds. Defaults to 5s' do |polling|
      options[:polling] = polling
    end

    opts.on '--batch BATCH_SIZE', Integer, 'Maximum number of lines sent as a batch. Defaults to 50' do |bsize|
      options[:bsize] = bsize
    end

    opts.on '--host HOST', 'Host to connect to' do |host|
      options[:host] = host
    end

    opts.on '--port PORT', 'Port to connect to' do |port|
      options[:port] = port
    end

    opts.on '--ca CA_CERT', 'CA certificate' do |ca|
      options[:ca] = ca
    end

    opts.on '--cert CLIENT_CERT', 'Client certificate' do |cert|
      options[:cert] = cert
    end

    opts.on '--pkey PRIV_KEY', 'Client certificate key' do |key|
      options[:key] = key
    end

    opts.on '-h', '--help', 'Display help' do
      puts opts
    end
  end

  begin
    args = optparse.parse!
    mandatory = %i[file host port ca cert key]
    missing = mandatory.select { |param| options[param].nil? }
    raise OptionParser::MissingArgument, missing.join(', ') unless missing.empty?
  rescue OptionParser::InvalidOption, OptionParser::MissingArgument => e
    puts "[ERROR] #{e.message}"
    puts optparse
    exit
  end

  [options, args]
end
# rubocop:enable Metrics/AbcSize, Metrics/BlockLength

# rubocop:disable Style/GuardClause
def files_avail?(opts)
  mapping = { ca: 'CA certificate', cert: 'Client certificate', key: 'Client key' }
  missing = opts.filter { |k, v| %i[ca cert key].include?(k) && !File.file?(v) }.map { |k, _| mapping[k] }.join(', ')
  unless missing.empty?
    puts "[ERROR] The following files couldn\'t be found: #{missing}"
    exit
  end
end
# rubocop:enable Style/GuardClause

if $PROGRAM_NAME == __FILE__
  begin
    opts, = parse_args
    files_avail? opts

    my_satellite = Satellite.new(opts)
    my_satellite.watchlog
  end
end
