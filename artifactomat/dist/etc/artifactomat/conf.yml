---
# Configuration of the aped server
ape:
  # Location for all temporary data
  tmp_dir: /tmp/artifactomat
  # The ip the ape control chanel binds to
  ip: localhost
  port: 8765
  # log level: error - 1, warning - 2, info - 3, debug - 4
  log_level: 4
  # use colored output on stdout for log messages
  log_colored: true
  # socket of the redis instance
  # change /etc/redis/redis-artifactomat.conf accordingly
  redis_socket: /var/run/redis-artifactomat/redis-server.sock
  # amount of subjects amongst which a subject must be unidentifiable when exported.
  # the k in k-anonymity
  anonymity_level: 3

# Configuration of the apenames nameserver
nameserver:
  # address the dns server will bind to, should be the external IP
  address: 192.168.1.101
  # the port apennames will bind to (client uses 53 by default)
  port: 53
  # the DNS that should be used to resolv for normal behaviour
  upstream:
    address: 131.220.51.34
    port: 53
  # build custom urls, apenames will reply accordingly.
  custom_urls:
    # Do not remove this line it is needed for the ape-job-spooler.
    # Change to your external IP
    proxy.its.apt: 192.168.1.101

# Configuration of ape-job-clerk that handles requests to create jobs
ape_job_clerk:
  # The address that ape-job-clerk runs on
  address: localhost
  # The port that ape-job-clerk binds to
  port: 51648

# configure the database
# for more information see
# http://edgeguides.rubyonrails.org/configuring.html#configuring-a-database
# we srongly advise postgress version > 9.4
database:
  adapter: postgresql
  encoding: utf8
  database: itsape
  username: its
  password: ape
  host: localhost
  schema_search_path: public
  pool: 200
  # Pool size has to be fixed in PRODUCTION!!!!!!
  # The default pool size is 5 in ActiveRecord 4 and 5, which is too low.
  # Set the pool size to a value at least 20% of your group file (more is better)
  # Do not set a value higher the 1,8 times then what your database can handle
  # For postgress have a look at /etc/postgresql/<version>/main/postgreql.conf
  # and look for the value of 'max_connections = 100' (100 is default).
  # Without increasing that value 45 might be a good idea.
  # I used 200 and set postgresql max_connections to 500, which was okay
  # pool: 25
  # pool: 200

# Configuration for the subject file parser
subjects:
  # Column separator. Has to be given in quotes.
  # default: ","
  col_sep: ","
  # Quotes are encapsulated with this character. Has to
  # given in quotes.
  # default: """
  quote_char: '"'

# The configuration for the dynamic routing
network:
  # Insert the IP of the computer the framework is set up on.
  external_ip: 192.168.1.101
  # the DNS that should be used to resolv for normal behaviour
  company_dns: 131.220.51.34
  # Make port available for the dynamic routing.
  # You probably need 80 and 443 for some landing pages.
  # However this depends on your recipes, they require ports.
  available_external_ports:
    - 80
    - 443
    - 8080-8089
    - 8091
  # Give a range of ports where the infrastucture of the recipes binds to.
  # Every infrastructure element will need at leat one port
  # So a good value is a port range of the size of at least two times the
  # number of recipes you will run simultaniously.
  available_internal_ports:
    - 9080-9089
    - 9091

# Configuration of the test scheduler
# Decides when series can run
scheduler:
  # Amount of seconds that episodes are allowed to start or end late (e.g. due to service downtime)
  max_episode_delay_seconds: 30
  # The scheluder will not (dis-)arm outside of business hours.
  # See: https://github.com/zendesk/biz#configuration
  # ATTENTION: configure OS clock and timezone correctly!
  biz:
    # The time the subject is expected to be testable
    hours:
      # :mon:
      #   '09:00': '12:00'
      #   '13:00': '17:00'
      # :tue:
      #   '09:00': '12:00'
      #   '13:00': '17:00'
      # :wed:
      #   '09:00': '12:00'
      #   '13:00': '17:00'
      # :thu:
      #   '09:00': '12:00'
      #   '13:00': '17:00'
      # :fri:
      #   '09:00': '12:00'
      #   '13:00': '17:00'
      :mon:
        '00:00': '24:00'
      :tue:
        '00:00': '24:00'
      :wed:
        '00:00': '24:00'
      :thu:
        '00:00': '24:00'
      :fri:
        '00:00': '24:00'
      :sat:
        '00:00': '24:00'
      :sun:
        '00:00': '24:00'

    # Timeintervals that shall be excluded
    breaks: {}
    # breaks:
    #   2017-10-16:
    #     '14:15': '14:30'
    #     '15:40': '15:50'

    # Holidays during testperiod
    holidays: []
    # holidays:
    #   - 2016-01-01
    #   - 2016-12-25

    # https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
    timezone: 'Europe/Berlin'
  # the minimal time between two episodes for a subject in seconds 60 is okay
  cooldown: 1


# configuration for the ape job spooler
ape_job_spooler:
  # ip adress the server binds to, a good choice is probably the external ip
  #  e.g. 192.168.1.101 (do not use 'localhost' here!)
  ip: 192.168.1.101
  # Thie port the jobspooler binds to, keep in mind it should be
  # intersection-free, with the other configured ports
  port: 8443
  # Location of server.crt, server.key and ca.crt, relative from the config_path e.g. 'certs/'
  cert_path: 'certs/'
  # The Jobspooler is able to push messages to the subject tracker by the
  # client_proxy_adapter plugin,  ref. /etc/artifactomat/conf.d/subjecttracker.yml
  tracker:
    # activate the tracking
    active: true
    # set the ip, where the plugin is bound on (do not use 'localhost' here!)
    ip: 192.168.1.101
    port: 8123

# configuration for the windows-client building
windows_client:
  # version of the windows-client that you want to build.
  # manually changing this version may lead to errors when trying to build the windows-client
  version: 2.0.0

# configuration to access a docker registry
# docker:
#   # the username for your docker registry
#   user: docker
#   # the password for your docker registry
#   pass: 123456
