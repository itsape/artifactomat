cd /windows-client

find ./publish/ArtifactDetectorProcess/ -type f | sort \
    | wixl-heat \
    -p ./publish/ \
    --var var.SRCDIR \
    --directory-ref APPDIR \
    --component-group ArtifactDetectorProcess \
    > Installer/ArtifactDetectorProcess.wxs

find ./publish/ITSAPE.Client/ -not -name ITSAPE.Client.exe -type f | sort \
    | wixl-heat \
    -p ./publish/ \
    --var var.SRCDIR \
    --directory-ref APPDIR \
    --component-group ITSAPE.Client \
    | sed -r 's/(.*<Directory Id=")[a-zA-Z0-9]*(" Name="ITSAPE.Client".*)/\1ITSAPEClient_Dir\2/' \
    > Installer/ITSAPE.Client.wxs

wixl \
    -v \
    -o ITSAPE.Client.msi \
    -D CERTSDIR=tls \
    -D CONFIGFILE=ITSAPE-Client/config.json \
    -D SRCDIR=publish \
    Installer/ArtifactDetectorProcess.wxs Installer/ITSAPE.Client.wxs Installer/Installer.wxs

. tls/VARIABLES
./Installer/patch-tables.sh ./ITSAPE.Client.msi
