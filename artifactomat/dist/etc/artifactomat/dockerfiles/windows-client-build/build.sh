#!/bin/sh

set -x

cd /windows-client
mkdir -p publish/ITSAPE.Client
mkdir -p publish/ArtifactDetectorProcess

dotnet build --configuration Release ITSAPE-Client/ITSAPE-Client.csproj
dotnet build --configuration Release ArtifactDetectorProcess/ArtifactDetectorProcess.csproj
dotnet publish --configuration Release --output publish/ITSAPE.Client ITSAPE-Client/ITSAPE-Client.csproj
dotnet publish --configuration Release --output publish/ArtifactDetectorProcess ArtifactDetectorProcess/ArtifactDetectorProcess.csproj
