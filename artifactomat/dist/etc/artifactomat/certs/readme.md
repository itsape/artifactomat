Execute 'generate-ca.sh' once to create:  
- CA (key and cert)  
- itsape-client (key and cert)  
- itsape-server (key and cert)  

Certs are stored in 'public', while keys get stored in 'private'.  

To generate further certificates and sign them with the CA, proceed as follows (e.g. in a recipe-script):  
```bash
openssl genrsa -out private/REPLACE_NAME.key 2048
cp cert.conf /tmp/cert.conf
# don't forget to modify cert.conf to your needs
openssl req -config /tmp/cert.conf -new -sha256 -key private/REPLACE_NAME.key -out REPLACE_NAME.csr -subj "/C=DE/ST=NRW/CN=REPLACE_COMMON_NAME"
openssl ca -batch -config conf/openssl.cnf -in REPLACE_NAME.csr -out public/REPLACE_NAME.pem
rm REPLACE_NAME.csr
```

