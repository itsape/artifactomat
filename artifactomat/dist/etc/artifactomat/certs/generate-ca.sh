#!/bin/bash

# create new random password
PASSWORD_FILE=/var/lib/artifactomat/client_cert_password
PFX_PASSWD=$(dd if=/dev/urandom count=1 bs=256 | sha256sum | cut -d' ' -f1 | tee "$PASSWORD_FILE")

# create backup if necessary
if [ -d "conf" ] || [ -d "private" ] || [ -d "public" ] || [ -d "signed-keys" ]; then
  BACKUP_DIR="cert-backup-$(date --iso-8601=seconds)"
  mkdir BACKUP_DIR
  mv conf BACKUP_DIR
  mv private BACKUP_DIR
  mv public BACKUP_DIR
  mv signed-keys BACKUP_DIR
  echo "backed up old keys and certs at $BACKUP_DIR"
fi

# clean environment
rm -rf conf private public signed-keys
mkdir -p {conf,private,public}

# gen CA
cp openssl.cnf conf/openssl.cnf
openssl req -nodes -sha256 -config conf/openssl.cnf -days 1825 -x509 -newkey rsa:4096 -out public/ca.pem -outform PEM
mkdir signed-keys
echo "01" > conf/serial
touch conf/index
echo "unique_subject = no" > conf/index.attr

# gen itsape certs
openssl genrsa -out private/server.itsape.key 4096
openssl req -new -sha256 -key private/server.itsape.key -out server.itsape.csr -subj "/C=DE/ST=NRW/CN=server.itsape.zad.local"
openssl ca -batch -config conf/openssl.cnf -in server.itsape.csr -out public/server.itsape.pem
rm server.itsape.csr

openssl genrsa -out private/client.itsape.key 4096
openssl req -new -sha256 -key private/client.itsape.key -out client.itsape.csr -subj "/C=DE/ST=NRW/CN=client.itsape.zad.local"
openssl ca -batch -config conf/openssl.cnf -in client.itsape.csr -out public/client.itsape.pem
rm client.itsape.csr

# convert certs for ItsApe Client
openssl pkcs12 -inkey private/client.itsape.key -in public/client.itsape.pem -export -out private/client.itsape.pfx -passout "pass:$PFX_PASSWD"
cd public
openssl x509 -in ca.pem -outform der -out ca.cer
openssl x509 -in client.itsape.pem -outform der -out client.itsape.cer
openssl x509 -in server.itsape.pem -outform der -out server.itsape.cer
