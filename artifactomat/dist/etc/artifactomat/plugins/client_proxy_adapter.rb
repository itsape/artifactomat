# frozen_string_literal: true

require 'openssl'
require 'json'
require 'yaml'
require 'set'
require 'artifactomat/modules/logging'
require 'artifactomat/models/action'

# ClientProxyAdapter (SubjectTracker Plugin)
class ClientProxyAdapter
  CFG_KEY = 'subjecttracker.plugins.ClientProxyAdapter'
  LOG_SIG = 'STP:ClientProxyAdapter'
  SLEEP_TIME_TIMEOUT_LOOP = 2

  attr_reader :name

  # MSG that will be recieved
  # userid:
  #  ip: X.X.X.X
  #  action: logout/login

  # MSG for the subject tracker
  # subject:
  #   userid: user_X
  # ip: 127.0.0.1
  # action: session_start/session_end

  def initialize(subject_tracker, name)
    @subject_tracker = subject_tracker
    @config = @subject_tracker.configuration_manager.get(CFG_KEY)
    @last_seen = {}
    @last_seen_mutex = Mutex.new
    @server = nil
    @name = name
  end

  def run
    @server = start_server(@config['cert_path'])
    @timeout_thread = Thread.new { timeout_loop }
    @accept_thread = Thread.new { accept_loop }
  end

  def stop
    Thread.list.each do |thread|
      thread.kill unless thread == Thread.current
    end
    @server.close until @server.closed?
  end

  private

  # thread which checks for timed out subjects
  def timeout_loop
    loop do
      sleep(SLEEP_TIME_TIMEOUT_LOOP)
      @last_seen_mutex.synchronize do
        @last_seen.delete_if do |userid, last_seen|
          delete = (Time.now - last_seen) > @config['subject_timeout']
          if delete
            begin
              action = timeout_action(userid)
              result = @subject_tracker.push_data(action)
              Logging.debug(LOG_SIG, "User \"#{userid}\" timed out: #{result}")
            rescue StandardError => e
              Logging.error(LOG_SIG, "TIMEOUT LOOP: #{e} #{e.backtrace}")
            end
          end
          delete
        end
      end
    end
  end

  def accept_loop
    while @server
      # rubocop:disable Lint/HandleExceptions
      begin
        # NOTE: Logging doesn't really work for some weird reason
        STDOUT.puts 'ClientProxyAdapter -- accepting connections'
        STDOUT.flush
        Thread.start(@server.accept) do |connection|
          data = read_data(connection)
          connection.close
          process_data(data)
        end
      rescue OpenSSL::SSL::SSLError; end
      # rubocop:enable Lint/HandleExceptions
    end
  end

  # :reek:NilCheck
  def read_data(connection)
    message = ''
    loop do
      data = connection.gets
      break if data.nil?

      message += data
    end
    message
  rescue StandardError => e
    Logging.error(LOG_SIG, "CONNECTION PROCESS: #{e}")
    message
  end

  # incoming data: Hash[user, { ip: ip, action: action }].to_yaml
  def process_data(data)
    action_hash = YAML.safe_load(data, [Symbol])
    action = build_action(action_hash)
    Logging.debug(LOG_SIG, "PUSHING DATA: #{action}")
    @subject_tracker.push_data(action) if action
  rescue StandardError => e
    Logging.error(LOG_SIG, "PROCESS DATA: #{e}")
  end

  def build_action(data)
    userid = data.keys.first
    now = Time.now
    action = Action.new(userid, data[userid][:ip], now)
    action.session_end = now if data[userid][:action] == 'logout'
    action.session_start = now if data[userid][:action] == 'login'
    update_subjects_list(action)
    action
  end

  # :reek:DuplicateMethodCall synchronize has to be called each time!
  def update_subjects_list(action)
    @last_seen_mutex.synchronize do
      if action.end_session?
        @last_seen.delete(action.userid)
      else
        @last_seen[action.userid] = Time.now
      end
    end
    true
  end

  def build_ssl_context(cert_path)
    ssl_ctx = OpenSSL::SSL::SSLContext.new
    ssl_ctx.min_version = OpenSSL::SSL::TLS1_3_VERSION
    ssl_ctx.cert = OpenSSL::X509::Certificate.new File.read "#{cert_path}/public/server.itsape.pem"
    ssl_ctx.key = OpenSSL::PKey::RSA.new File.read "#{cert_path}/private/server.itsape.key"
    ssl_ctx.ca_file = "#{cert_path}/public/ca.pem"
    ssl_ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER | OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT
    ssl_ctx
  end

  def start_server(cert_path)
    ssl_ctx = build_ssl_context(cert_path)
    tcp_server = TCPServer.new('localhost', 8123)
    ssl_server = OpenSSL::SSL::SSLServer.new(tcp_server, ssl_ctx)
    ssl_server
  end

  def timeout_action(userid)
    action = Action.new(userid, '', Time.now)
    action.session_end = Time.now
    action
  end
end
