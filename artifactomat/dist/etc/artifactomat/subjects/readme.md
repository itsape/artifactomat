# SUBJECT GROUPS
Within this folder you may place CSV files.
Each file represents one subject group.
Within each file there can be multiple subjects.

## HEADER
 - **id:** Freely chosen `id`, has to be unique within one file, should be alphanumeric. A subject, defined within a file called `example.csv` and the id `12` will be identified across the artifact-o-mat as `example.csv/12`.
 - **name:** The subjects first name.
 - **surname:** The subjects family name.
 - **userid:** The subjects user id on its computer (or on the directory service if the infrastructure implements it).
 - **email:** The email address of the subject.

*You can add more columns if you need to.*

## EXAMPLE
```csv
id,name,surname,userid,email,position
1,Max,Mustermann,mm,mm@example.net,CTO
```
