[[_TOC_]]

# Recipes
This is where the recipes for the artifactomat should reside.
Recipes are complete provisioning setups.
Each YAML file within this folder will be interpreted as a possible recipe, with the filename (without the file-extension) acting as the recipe-name and -id.
Additional files which are needed by a recipe (e.g. scripts or artifact-templates) are to be deposited in a folder next to the YAML-file, named after the recipe (e.g. for a recipe-file **awesome\_recipe.yml**, the folder-name has to be **awesome\_recipe**).

## Runtime Environment
Each script will be executed by the systems default shell.
Custom environment variables are injected by the framework into the runtime environment.

The whole artifactomat config (`conf.yml`) will be avaialbe, using environment variables with the prefix `ARTIFACTOMAT_CONF_CONF_`.

Example:
```bash
$ARTIFACTOMAT_CONF_CONF_APE_TMP_DIR           # default: /tmp/its.ape
$ARTIFACTOMAT_CONF_CONF_NETWORK_EXTERNAL_IP   # e.g. 10.10.10.160
```

Each other config file in the `conf-d`-directory can also be accessed via the prefix
`ARTIFACTOMAT_CONF_<filename>`.

Example:
```bash
$ARTIFACTOMAT_CONF_TRACKCOLLECTOR_IP                                     # from conf.d/trackcollector.yml; default: 0.0.0.0
$ARTIFACTOMAT_CONF_TRACKCOLLECTOR_PORT                                   # from conf.d/trackcollector.yml; default: 8765
$ARTIFACTOMAT_CONF_SUBJECTTRACKER_ACTIVE_PLUGINS_ACTIVETRACKER_CERT_PATH # from conf.d/subjecttracker.yml; default: /etc/artifactomat/certs
```

During runtime, the artifactomat also makes the following environment variables available to use in your scripts:

```bash
$ARTIFACTOMAT_TESTEPISODE_SCHEDULE_START    # The start of the episode schedule, e.g. "2021-10-12 12:14:46 +0200"
$ARTIFACTOMAT_TESTEPISODE_SCHEDULE_END      # The end of the episode schdule, e.g. "2021-10-12 12:16:46 +0200"

$ARTIFACTOMAT_ROUTING_PORT_I                # A variable for each defined routing port, where "I" is the index of the port in your configured ports
$ARTIFACTOMAT_ROUTING_EXT_ADDR              # The external ip configures in the artifactomat config

$ARTIFACTOMAT_RECIPE_ID                     # The ID of the recipe, e.g. "awesome_recipe"
$ARTIFACTOMAT_RECIPE_FOLDER                 # The folder in which the recipe resides, e.g. /etc/artifactomat/recipes/awesome_recipe
$ARTIFACTOMAT_RECIPE_PATH                   # The folder in which the recipe resides, e.g. /etc/artifactomat/recipes/awesome_recipe
$ARTIFACTOMAT_RECIPE_TOKEN                  # MD5 hashsum of the recipe id, e.g. "83ca91ab1b96ecd049b0e5775a68f845"
$ARTIFACTOMAT_RECIPE_TEMP_DIR               # The temporary directory for the recipe, e.g. /tmp/its.ape/1
$ARTIFACTOMAT_RECIPE_<PARAMETERNAME>          # Each defined recipe parameter and it's value
$ARTIFACTOMAT_RECIPE_IE_<PARAMETERNAME>       # Each defined infrastructure element parameter and it's value
$ARTIFACTOMAT_RECIPE_TESTEPISODE_ID         # The ID of the test episode, e.g. 1
$ARTIFACTOMAT_RECIPE_TESTSEASON_ID          # The ID of the test season, e.g. 1

$ARTIFACTOMAT_SUBJECT_TOKEN                 # MD5 hashsum of the subject id, e.g. "92a9496f55184d4f7cbad550e8c986a5"
$ARTIFACTOMAT_SUBJECT_ATTR                  # Each attribute defined in your subjects file gets it's own environment variable, where ATTR is replaces with the name of the attribute
```


## Structure
Each Recipe has to define the following fields:

- **`artifact_script`**: [String pointing to a script-file]
  This script has two purposes:

  1. Check if the required dependencies are available
  2. Generate artifact(s) for each subject

  To differentiate the two tasks, the script is ***required*** to check if the environment variable `$DEP_CHECK` is set to either `1` or `true`.
  If this is the case, the script is expected to return `0` if all dependencies are met, any non-zero number else.

  To generate artifacts for each subject, the script will be called once for each individual subject/each TestEpisode, since subject-specific fields are loaded into the environment.
  Generated artifacts need to be stored until they are delivered to the corresponding subject.
  Each TestSeason has a temporary directory (available to the script under `$ARTIFACTOMAT_RECIPE_TEMP_DIR`), it is recommended to create an `artifacts`-folder here and store artifacts at this location.
  The author of a recipe is free to choose the exact location, but needs to stick to his choice across all scripts of the recipe.

  Example:
  ```yaml
  artifact_script: "$ARTIFACTOMAT_RECIPE_PATH/artifact_script.sh"
  ```

- **`arm_script`**: [String pointing to a script-file]
  Script, which will deliver the generated artifacts to a subject/make infrastructure visible to a subject, and potentially start the artifact detection.
  The `ape-job` tool, which is part of the artifactomat, can be used in this script to create jobs like copying a file to the subjects computer, or executing some code.
  Once the artifactomat is installed, execute `ape-job --help` for more information about it's usage.
  Executed at the beginning of each TestEpisode.

  Example:
  ```yaml
  arm_script: "$ARTIFACTOMAT_RECIPE_PATH/arm.sh"
  ```

- **`deploy_script`**: [String pointing to a script-file]
  Deploy artifacts into the required InfrastructureElements.
  Example: the artifact\_script has generated a website, which will need to be deployed into a webserver.
  At this point in time the webserver is _not_ active for the subject until the arm\_script has been called.

  Example:
  ```yaml
  deploy_script: "$ARTIFACTOMAT_RECIPE_PATH/deploy.sh"
  ```

- **`undeploy_script`**: [String pointing to a script-file]
  Withdraw artifacts from InfrastructureElements which were deployed through the deploy\_script.

  Example:
  ```yaml
  undeploy_script: "$ARTIFACTOMAT_RECIPE_PATH/undeploy.sh"
  ```

- **`disarm_script`**: [String pointing to a script-file]
  Script, which will try to withdraw artifacts delivered through the arm\_script/make infrastructure transparent to a subject.
  Also responsible for stopping artifact detection.
  The `ape-job` tool, which is part of the artifactomat, can be used in this script to e.g. remove previously copied files from the subjects computer.
  Once the artifactomat is installed, execute `ape-job --help` for more information about it's usage.
  Executed at the end of each TestEpisode.

  Example:
  ```yaml
  disarm_script: "$ARTIFACTOMAT_RECIPE_PATH/disarm.sh"
  ```

- **`trackfilters`**: [array of [TrackFilter-objects](#trackfilter-object)]
  A TrackFilter is required to define, extract and register the actions a subject can perform on a given artifact.
  For example, if a subject gets a phishing e-mail, the possible actions may include clicking the link inside the e-mail.
  Another action may be to fill in and submit a fake login-form.
  The actions are required to generate log-files of some manner.

  Example (for an example of the TrackFilter structure, see [TrackFilter-objects](#trackfilter-object)):
  ```yaml
  - !ruby/object:TrackFilter
    pattern: !ruby/regexp /(.+)/
    [...]
  - !ruby/object:TrackFilter
    [...]
  ```

- **`infrastructure`**: [array of [InfrastructureElement-objects](#infrastructureelement-object)]
  Recipes may require additional services to be available.
  For example, to send a phishing e-mail, sendmail needs to be setup.
  If an e-mail contains an URL, a webserver needs to be configured to detect the connection.
  These services can be set up through [infrastructure elements](#infrastructureelement-object).

  Example (for an example of the InfrastructureElement structure, see [InfrastructureElement-objects](#infrastructureelement-object)):
  ```yaml
  - !ruby/object:InfrastructureElement
    monitor_script: "$ARTIFACTOMAT_RECIPE_PATH/infra/monitor.sh"
    [...]
  - !ruby/object:InfrastructureElement
    [...]
  ```

- **`duration`**: [Integer]
  Duration of one TestEpisode in minutes.

  Example:
  ```yaml
  duration: 120
  ```

- **`parameters`**: [Hashmap]
  The recipes author may define custom parameters during creation of a TestSeason.
  These parameters will be available to the scripts as environment variables in the form `$ARTIFACTOMAT_RECIPE_PARAMETERNAME`.
  This hashmap lists all available parameters with a short description.

  Example:
  ```yaml
  parameters:
    filename: 'the name of the file to be deployed'
    mail_amount: 'the amount of e-mails to send'
  ```

- **`default_parameters`**: [Hashmap]
  Defines the default values for the parameters.

  Example:
  ```yaml
  default_parameters:
    filename: 'TopSecret.pdf'
    mail_amount: 3
  ```

- **`detection`**: [Hashmap]
  Parameters used for artifact detection. For further information, see the [Artifact Detection](#artifact-detection) section.

  Example (for an example of the detection structure, see the [detection settings section](#detection-settings)):
  ```yaml
  detection:
    detection_interval: 2
    [...]
  ```


### TrackFilter-object
  - **`pattern`**: [Regular Expression]
    Detect and extract relevant lines from a log.

    Example:
    ```yaml
    pattern: !ruby/regexp /(.+)/
    ```
  - **`extraction_pattern`**: [Array of Regular Expressions]
    Given one such line, extract named variables to track the users actions.
    Every extraction\_pattern is **required** to include at least the _timestamp_ variable and one of the following
    unique identifiers of the subject defined in a group-file (if needed, a recipes author may include additional variables:
    the available named variables are the same as the .csv-header used in a group file where all subjects are defined):
      - *subject* : The subject-id
      - *userid* : The userid the subject identifies with inside his work environment (e.g. Windows login-name)
      - *email* : The subjects email-address
      - *ip* : The subjects ip-address

    Example:
    ```yaml
    extraction_pattern:
    - !ruby/regexp /\[(?<timestamp>.*)\] \| (?<ip>[0-9.]{7,15}) \| GET \/(?<userid>.*) HTTP.*/
    ```
  - **`course_of_action`**: [String]
    Short description of the user-action defined in this TrackFilter (e.g. "clicked link inside phishing e-mail").

    Example:
    ```yaml
    course_of_action: "Link opened in browser"
    ```
  - **`score`**: [Integer]
  - Value to weight the user-action defined in this TrackFilter. Metric as follows:
    Negative Value: "Good" action, e.g. **-1** for Quarantining a Virus.
    Positive Value: "Bad" action, e.g. **1** for running an unknown executable.

    Example:
    ```yaml
    score: 1
    ```
  - **`recipe_id`**: [String]
    To identify the recipe it belongs to, the recipe\_id needs to be present in each TrackFilter.

    Example:
    ```yaml
    recipe_id: awesome_recipe
    ```

### InfrastructureElement-object
  - **`generate_script`**: [String pointing to a script-file]
    Script, which will generate the infrastructure, e.g. pulling, running and configuring a docker image/container.

    Example:
    ```yaml
    generate_script: "$ARTIFACTOMAT_RECIPE_DIR/infra/generate.sh"
    ```
  - **`arm_script`**: [String pointing to a script-file]
    Script, which will make the infrastructure visible to subjects.
    This script will not be called from inside the framework, it has to be called from inside the recipes *arm\_script* described earlier.

    Example:
    ```yaml
    arm_script: "$ARTIFACTOMAT_RECIPE_DIR/infra/arm.sh"
    ```
  - **`monitor_script`**: [String pointing to a script-file]
    Script, which will be called regularly to check if the infrastructure is still up and running as intended.
    The following exit codes have been defined:
      - **0** : Everything is fine.
      - **1** : A non-critical error occurred.
      - **2** : An error occurred.
      - **3** : The infrastructure is unreachable.

    Example:
    ```yaml
    monitor_script: "$ARTIFACTOMAT_RECIPE_DIR/infra/monitor.sh"
    ```
  - **`disarm_script`**: [String pointing to a script-file]
    Script, which will make the infrastructure transparent to subjects.
    This script will not be called from inside the framework, it has to be called from inside the recipes *disarm\_script* described earlier.

    Example:
    ```yaml
    disarm_script: "$ARTIFACTOMAT_RECIPE_DIR/infra/disarm.sh"
    ```
  - **`destroy_script`**: [String pointing to a script-file]
    Script, which will remove the generated infrastructure, e.g. stopping and removing a docker container.

    Example:
    ```yaml
    destroy_script: "$ARTIFACTOMAT_RECIPE_DIR/infra/destroy.sh"
    ```
  - **`infrastructure_type`**: [String]
    Define the type of infrastructure, e.g. 'webserver', 'mailserver', etc.
    Used to identify the infrastructure element, e.g. in logs.

    Example:
    ```yaml
    infrastructure_type: 'webserver'
    ```
  - **`parameters`**: [Hashmap]
    Optional parameters that will be made available to the aforementioned scripts through environment variables.

    Example:
    ```yaml
    parameters:
      file_name: 'testfile.yml'
      route: '/certificates'
    ```

  - **`season`**: [Integer]
    The TestSeason-id from which the InfrastructureElement is used by.
    This field is **not** to be filled in, this will be handled by the framework.

    Example:
    ```yaml
    season:
    ```
  - **`ports`**: [Array of Integers]
  	Define needed ports for this infrastructure element.

    Example:
    ```yaml
    ports:
      - 8080
    ```
  - **`names`**: [Array of Strings]
  	Define DNS-names for this infrastructure element.

    Example:
    ```yaml
    names:
      - 'example.its.ape'
    ```

## Artifact Detection
The artifactomat comes with an artifact detector, which can be used to identify whether an artifact was visible to the user.
If this functionality should be available for a recipe, the recipe has to come with appropriate detection settings, as well as a set of reference screenshots.
In addition, start- and stop-detection jobs have to be present in the arm- and disarm-script respectively.
The following sections briefly explain how to set them up.

### Detection Settings
All detection settings are summarized under the `detection` key, featuring a Hashmap of the following fields:

- **`detection_interval`** [Integer]:
  The timespan in milliseconds between two consecutive detections.

  Example:
  ```yaml
  detection_interval: 1000
  ```

- **`error_window_size`** [Integer]:
  For error correction during detection a sliding window majority voting algorithm may be applied.
  The `error_window_size` is the size of the sliding window. The voting algorithm used was originally 
  described by Boyer, Moore & Strother: "MJRTY—A Fast Majority Vote Algorithm" in: Automated Reasoning: 
  Essays in Honor of Woody Bledsoe. In order to avoid split voting, `error_window_size` has to be an 
  odd number. If no error correction should be applied, the value has to be 1. In order to account for 
  small deviance in measurement a value of 3 or 5 is reasonable.

  Example:
  ```yaml
  error_window_size: 5
  ```

- **`detectors`** [List of [Detectors](#detector)]:
  The detectors that should be run, *in the order they should be run*. The order is important if one
  of the later detectors uses results from an earlier detector, e.g. in a [Condition](#condition).

  Example (for an example of the detector structure, see the [Detector section](#detector)):
  ```yaml
  detectors:
    - detector_class_name: RunningProcessDetector
    [...]
  ```
- **`match_conditions`** [String of [Conditions](#condition)]: A condition which has to be true, in order for the
  artifact to count as visible to the user.

  Example:
  ```yaml
  match_conditions: "CountOpenWindows > 0 & MaxWindowVisibilityPercentage > 1"
  ```
- **`runtime_information`** [[RuntimeInformation](#runtimeinformation)]: An object containing additional information
  about attributes of the artifact

  Example (for an example of the runtime information structure, see the [RuntimeInformation section](@runtimeinformation)):
  ```yaml
  runtime_information:
    process_names: "thunderbird|outlook"
    [...]
  ```

#### Detector
A detector is a Hashmap consisting of the following fields:

- **`detector_class_name`** [String]:
  Name of the detector class you want to use.
  Note: some detectors use parts of the runtime information.
  See the section about [RuntimeInformation](#runtimeinformation) for information about which detector uses what info.
  Available detector classes are:
  - `OpenWindowDetector`:
    Detect whether a window is open.
  - `InstalledProgramsDetector`:
    Detect whether an application is installed.
  - `VisualFeatureDetector`:
    Detect visual features given by reference images.
  - `RunningProcessDetector`:
    Detect whether a process is running.
  - `DesktopIconDetector`:
    Detect whether an icon is *visible* on the desktop.
  - `TrayIconDetector`:
    Detect whether an icon is *visible* in the system tray / notification area.

  Example:
  ```yaml
  detector_class_name: RunningProcessDetector
  ```

- **`pre_conditions`** [String of Conditions]:
  A condition which has to be true, in order for the detector to start detection.
  Pre conditions may apply to one or more of the following attributes:
  - `ArtifactName` [String]:
    Name of the artifact.
  - `CountDesktopIcons` [Integer]:
    Amount of desktop icons that have been found.
  - `CountInstalledPrograms` [Integer]:
    Amount of installed programs that have been found.
  - `CountOpenWindows` [Integer]:
    Amount of open windows that have been found.
  - `CountRunningProcesses` [Integer]:
    Amount of running processes that have been found.
  - `CountTrayIcons` [Integer]:
    Amount of tray icons that have been found.
  - `CountVisualFeatureMatches` [Integer]:
    Amount of reference images that could be matched.
  - `MaxWindowVisibilityPercentage` [Integer]:
    Visibility of the most visible window in percent.
  - `ProgramExecutables` [List of Strings]:
    Information about matched executables.j
  - `ProcessIds` [List of Integers]:
    Process IDs detected by the `RunningProcessDetector` that have matched the runtime information.

  Example:
  ```yaml
  pre_conditions: 'CountRunningProcesses > 0'
  ```

- **`target_conditions`** [String of Conditions]:
  A condition which has to be true, in order for the detector to stop it's work, and let the next detector in the list start.
  Target conditions may apply to the following attribute:
  - `ArtifactPresent` [Integer]: Indication whether the artifact is present.
    Can have the following three values:
    - `0`: Presence deemed impossible.
    - `1`: Presence deemed possible.
    - `2`: Presence deemed certain.

  Example:
  ```yaml
  target_conditions: 'ArtifactPresent >= 1'
  ```

  Example of one detector:
  ```yaml
  detector_class_name: OpenWindowDetector
  pre_conditions: CountRunningProcesses > 0
  target_conditions: ArtifactPresent == 2
  ```

#### Condition

A single condition is a string that contains of one of the matching attributes mentioned above, one of the following comparators `>`, `<`, `>=`, `<=`, `=`, and a static value that the attribute value is compared to.
An example condition would be `"CountDesktopIcons > 2"`.
Multiple conditions can be linked into a compound condition by using `|` for an OR link, or `&` for an AND link.
Link precedence can be made explicit by enclosing parts of the condition in brackets.
An example compound condition would be `"CountDesktopIcons = 2 & (CountInstalledPrograms >= 1 | CountTrayIcons >= 1)"`.

#### RuntimeInformation

The RuntimeInformation is a Hashmap where all fields contain a single String, representing a `|` separated list of strings.
The fields are:

- **`icon_titles`**:
  Strings that should either be substrings of tray icon tooltips that the `TrayIconDetector` should detect, or substrings of desktop icon names that the `DesktopIconDetector` should detect.

  Example:
  ```yaml
  icon_titles: "word|office"
  ```
- **`process_names`**:
  Strings that should be substrings of process names that the `RunningProcessDetector` should detect.

  Example:
  ```yaml
  process_names: "thunderbird|outlook"
  ```
- **`program_names`**:
  Strings that should be substrings of installed programs that the `InstalledProgramsDetector` should detect.
  ```yaml
  program_names: "office"
  ```
- **`window_titles`**:
  Strings that should be substrings of windows to be detected by the `OpenWindowDetector`.
  ```yaml
  window_titles: "Posteingang|Inbox"
  ```

### Refererence Screenshots
If visual recognition should be used for detecting the deployed artifacts, reference screenshots of the artifacts have to be present.
They have to be adjusted according to the deployed software and OS of the network and to the quality improvements below.

The screenshots provided were taken using Microsoft Windows 10 and
- Microsoft Office Professional Plus 2019
  - Microsoft Outlook, version 2004 (build 12730.20250)
  - Word, version 2004 (build 12730.20250)
- Google Chrome, version 81.0.4044.138 (Official Build) (64-bit)
- Adobe Acrobat Reader DC, version 2020.006.20042
- Java Runtime, version 8 update 251 (build 1.8.0\_251-b08)

Reference screenshots should be manipulated as listed below in order to achieve better recognition results:
- cropping to the relevant regions
- replacing irrelevant text/features with the background color (see below)
  - some "guiding edges" should be left, like window outlines
- replacing user-dependent text/features with the background color (e.g. email recipient name)

Especially feature-rich content that is present in objects similar to the artifacts should be covered.
For example, the email list of received emails in Outlook, at the left of the opened email, or software logos.

In general, the following rules apply for the reference screenshot collections
- corners/edges are the features recognized
- fewer screenshots lead to faster detections
- if the artifact can be resized, representative sizes should be used
  (e.g. 100% & 50% of screen size)
- small screenshots are useless
  (image recognition needs a resolution > 75x75 px, better > 250x250 px)
- blank space on the screenshots is useful if the artifact's features can't be present there
  ("featureless space"), e.g. for websites
- the more unique features an artifact has, the more successful the recognition will be

## Example of a recipe-file
For an example of a full recipe, see `/etc/artifactomat/test_recipe/`
