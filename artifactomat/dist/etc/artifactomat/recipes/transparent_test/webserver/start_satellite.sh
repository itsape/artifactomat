#!/bin/bash

status=`ps -efww | grep -w "satellite.rb" | grep -v "grep" | awk -vpid=$$ '$2 != pid { print $2 }'`
if [ ! -z "$status" ]; then
    echo "satellite is already running"
    exit 0;
fi

ruby ${0%/*}/satellite.rb -f /itsape/traces.log --host IP_ADDRESS_OF_TC --port PORTNUMBER --ca ${0%/*}/ca.pem --pkey ${0%/*}/client.itsape.key --cert ${0%/*}/client.itsape.pem
