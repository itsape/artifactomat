#!/bin/bash

scriptPath=${0%/*}

#if TC listens on all interfaces send tracks over docker interface, else use the configured IP
if [ "$ARTIFACTOMAT_CONF_TRACKCOLLECTOR_IP" = "0.0.0.0" ]; then
	#get ip from docker interface
	ARTIFACTOMAT_CONF_TRACKCOLLECTOR_IP=$(ip addr | awk '/inet/ && /docker0/{sub(/\/.*$/,"",$2); print $2}')
fi

mkdir -p $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/website
#create site for monitoring
touch $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/website/monitor.html

#copy and configure satellite
mkdir -p $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/satellite
cp $scriptPath/../../../satellite.rb $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/satellite/satellite.rb
cp $scriptPath/../../../certs/public/ca.pem $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/satellite/ca.pem
cp $scriptPath/../../../certs/public/client.itsape.pem $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/satellite/client.itsape.pem
cp $scriptPath/../../../certs/private/client.itsape.key $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/satellite/client.itsape.key
cp $scriptPath/start_satellite.sh $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/satellite/start_satellite.sh
sed -i "s/IP_ADDRESS_OF_TC/$ARTIFACTOMAT_CONF_TRACKCOLLECTOR_IP/g" $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/satellite/start_satellite.sh
sed -i "s/PORTNUMBER/$ARTIFACTOMAT_CONF_TRACKCOLLECTOR_PORT/g" $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/satellite/start_satellite.sh

#check if docker image already exists
if [ "$(docker images -q itsapeapache 2> /dev/null)" == "" ]; then
	docker login -u "$ARTIFACTOMAT_CONF_CONF_DOCKER_USER" -p "$ARTIFACTOMAT_CONF_CONF_DOCKER_PASS" registry.cs.uni-bonn.de:5005
	docker pull registry.cs.uni-bonn.de:5005/itsape/recipes/itsapeapache:latest
	docker tag registry.cs.uni-bonn.de:5005/itsape/recipes/itsapeapache:latest itsapeapache:latest
fi

docker run -d -p $ARTIFACTOMAT_ROUTING_PORT_1:80 --name $ARTIFACTOMAT_RECIPE_TESTSEASON_ID-apache -v $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/website:/itsape/website -v $ARTIFACTOMAT_RECIPE_TEMP_DIR/webserver/satellite:/itsape/satellite itsapeapache:latest
docker exec $ARTIFACTOMAT_RECIPE_TESTSEASON_ID-apache sed -i '11i\
    LogFormat "%t | %a | %r " traces\
    SetEnvIf Request_URI "^/monitor.html$" log_exclude=true\
    CustomLog /itsape/traces.log traces env=!log_exclude
' /etc/apache2/sites-available/website.conf
docker exec $ARTIFACTOMAT_RECIPE_TESTSEASON_ID-apache chgrp -R 33 /itsape/website
docker exec $ARTIFACTOMAT_RECIPE_TESTSEASON_ID-apache chmod -R 774 /itsape/website
docker exec $ARTIFACTOMAT_RECIPE_TESTSEASON_ID-apache a2dissite 000-default
docker exec $ARTIFACTOMAT_RECIPE_TESTSEASON_ID-apache a2ensite website
docker exec $ARTIFACTOMAT_RECIPE_TESTSEASON_ID-apache service apache2 reload
docker exec -d $ARTIFACTOMAT_RECIPE_TESTSEASON_ID-apache sh /itsape/satellite/start_satellite.sh
