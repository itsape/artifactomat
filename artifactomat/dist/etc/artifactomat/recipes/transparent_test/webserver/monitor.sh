#!/bin/bash

export PATH=$PATH:/bin:/usr/bin

STATUS=$(curl --write-out %{http_code} --silent --output /dev/null $ARTIFACTOMAT_ROUTING_EXT_ADDR:$ARTIFACTOMAT_ROUTING_PORT_1/monitor.html)

if [ -z $STATUS ]
then
  exit 3
fi

if [ $STATUS -ne 200 ]
then
  exit 2
fi
