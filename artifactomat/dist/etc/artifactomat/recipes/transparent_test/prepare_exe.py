import sys
#sys.argv[1] = input filename
#sys.argv[2] = output filename
#sys.argv[3] = "hostname" or "ip"
#sys.argv[4] = hostname or ip (e.g. "google.com" or "127.0.0.1")
#sys.argv[5] = port (e.g. "80")
#sys.argv[6] = (OPTIONAL) additional arguments for GET request (e.g. for "GET /subjectid=42" provide "subjectid=42")

#example: python prepare_exe.py selfremove.exe clickme.exe "ip" "192.168.0.19" "8085" "testuserid"
#example: python prepare_exe.py selfremove.exe clickme.exe "hostname" "google.de" "80"

if len(sys.argv) != 7 and len(sys.argv) != 6:
    print("wrong number of arguments")
    exit(1)

# Read the entire file as a single byte string
with open(sys.argv[1], 'rb') as f:
    data = f.read()

if sys.argv[3] == "hostname":
    placeholder = bytearray("HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER ", 'ascii')
    replace = bytearray(sys.argv[4], 'ascii')
    replace = replace.ljust(128, b'\x00')
    data = data.replace(placeholder, replace)
elif sys.argv[3] == "ip":
    placeholder = bytearray("255.255.255.255", 'ascii')
    replace = bytearray(sys.argv[4], 'ascii')
    replace = replace.ljust(15, b'\x00')
    data = data.replace(placeholder, replace)
    placeholder = bytearray("HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER HOSTPLACEHOLDER ", 'ascii')
    replace = bytearray(b'\x00')
    replace = replace.ljust(128, b'\x00')
    data = data.replace(placeholder, replace)
else:
    print("Error: please provide whether ip or hostname will be used!")

placeholder = bytearray("12345", 'ascii')
replace = bytearray(sys.argv[5], 'ascii')
replace = replace.ljust(5, b'\x00')
data = data.replace(placeholder, replace)

placeholder = bytearray("PARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDERPARAMPLACEHOLDER", 'ascii')
if len(sys.argv) == 7:
    replace = bytearray(sys.argv[6], 'ascii')
else:
    replace = bytearray("", 'ascii')
replace = replace.ljust(256, b'\x00')
data = data.replace(placeholder, replace)

# Write binary data to a file
with open(sys.argv[2], 'wb') as f:
    f.write(data)
