#!/bin/bash

# dependency check
if [ "$DEP_CHECK" != "" ] && ( [ "$DEP_CHECK" = 1 ] || [ "$DEP_CHECK" = 'true' ] || [ "$DEP_CHECK" = 'True' ]); then
	EXIT=0

	docker -v >/dev/null 2>&1 || { echo "Docker is not installed.  Aborting." >&2; EXIT=1; }
	python -V >/dev/null 2>&1 || { echo "Python is not installed.  Aborting." >&2; EXIT=1; }

	exit $EXIT
fi

SUBJECTID=`echo $ARTIFACTOMAT_SUBJECT_ID | sed 's|/|_|g'`

# working dir
cd $ARTIFACTOMAT_RECIPE_TEMP_DIR
mkdir -p artifacts
ARTIFACTS_TMPDIR=$ARTIFACTOMAT_RECIPE_TEMP_DIR/artifacts
cd $ARTIFACTS_TMPDIR

#generate artifact
python $ARTIFACTOMAT_RECIPE_PATH/prepare_exe.py "$ARTIFACTOMAT_RECIPE_PATH/send_get.exe" "$ARTIFACTS_TMPDIR/${SUBJECTID}" "hostname" "apachetest.its.apt" "$ARTIFACTOMAT_ROUTING_PORT_1"
