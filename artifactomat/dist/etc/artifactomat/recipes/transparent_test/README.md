# Test Recipe

This is a recipe made to test the connectivity to the clients.  
To run the test, create a
groupfile that includes all the users of the clients you want to test.  
Now create a season and series with this recipe and the groupfile you created,
and start it.  

After the series has ended, you should see the track entries with the following
three course of actions for each subject in your groupfile:

-  course_of_action: "(STATUS) artifact successfully copied"
-  course_of_action: "(STATUS) artifact successfully executed"
-  course_of_action: "(STATUS) GET request successfully sent to apachetest.its.apt"

Each subject that does not have these, means that their respective client did not successfully
connect to the artifact-o-mat.
